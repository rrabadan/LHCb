###############################################################################
# (c) Copyright 2000-2024 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
#[=======================================================================[.rst:
GaudiConf
---------
#]=======================================================================]

gaudi_install(PYTHON)
gaudi_generate_confuserdb(
    GaudiConf.SimConf
    GaudiConf.DigiConf
    GaudiConf.CaloPackingConf
    GaudiConf.DstConf
    GaudiConf.TurboConf
)
lhcb_add_confuser_dependencies(
    DAQ/RawEventCompat
    Kernel/LHCbKernel
    Packer/EventPacker:EventPacker
)

gaudi_install(SCRIPTS)

gaudi_add_tests(QMTest)

gaudi_add_pytest(python OPTIONS --doctest-modules -v)
add_dependencies(pytest-prefetch-GaudiConf-GaudiConf.pytest.python LHCb_MergeConfdb)
