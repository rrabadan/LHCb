###############################################################################
# (c) Copyright 2023 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
Options file for merging output Sprucing DST files
"""

from GaudiConf.LbExec import Options
from PyConf.application import configure_input, configure, root_copy_input_writer
from PyConf.control_flow import CompositeNode, NodeLogic


def dst(options: Options):
    """
    Merges output of Sprucing jobs in production
    """
    writer = root_copy_input_writer(options, options.output_file)
    node = CompositeNode(
        'DSTMerger',
        combine_logic=NodeLogic.LAZY_AND,
        children=[writer],
        force_order=True)

    config = configure_input(options)
    config.update(configure(options, node))
    return config
