#!/usr/bin/env python
###############################################################################
# (c) Copyright 2022-2023 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
import argparse

from .cli_utils import FunctionLoader, OptionsLoader
from . import main


def parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "function",
        type=FunctionLoader,
        help=
        "Function to call with the options that will return the configuration. "
        "Given in the form 'my_module:function_name'.",
    )
    parser.add_argument(
        "options",
        help="YAML data to populate the Application.Options object with. "
        "Multiple files can merged using 'file1.yaml+file2.yaml'.")
    parser.add_argument("extra_args", nargs="*")
    parser.add_argument(
        "--dry-run",
        action="store_true",
        help="Do not run the application, just generate the configuration.",
    )
    parser.add_argument(
        "--export",
        help='Write a file containing the full options (use "-" for stdout)',
    )
    parser.add_argument(
        "--with-defaults",
        action="store_true",
        help="Include options set to default values (for use with --export)",
    )
    parser.add_argument(
        "--app-type",
        default="Gaudi::Application",
        help=argparse.SUPPRESS
        # "Name of the Gaudi application to use, primarily needed for Online."
    )
    kwargs = vars(parser.parse_args())
    kwargs["options"] = OptionsLoader(kwargs["function"], kwargs["options"])
    return main(**kwargs)


if __name__ == "__main__":
    exit(parse_args())
