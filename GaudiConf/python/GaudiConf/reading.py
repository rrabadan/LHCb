##############################################################################
# (c) Copyright 2022 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""Helpers for configuring an application to read Moore HLT2 output."""
from __future__ import absolute_import
import os
from PyConf.application import (default_raw_event, default_raw_banks,
                                input_from_root_file, ComponentConfig)
from PyConf.dataflow import dataflow_config
from PyConf.components import force_location, setup_component
from PyConf.reading import get_mc_vertices, get_mc_particles
from PyConf.control_flow import CompositeNode, NodeLogic
from PyConf.Algorithms import HltDecReportsDecoder, HltPackedBufferDecoder, HltRoutingBitsFilter, UnpackDstDataBank

__all__ = ["do_unpacking", "type_map"]


# These are the known types that we can persist
def type_map():
    ## TODO: derive from the properties of SelectivePacker (which doesn't work trivially, as there is no DataHandleArray (and corresponding property) in Gaudi...)
    type_map = {
        "SharedObjectsContainer<LHCb::Particle>":
        "ParticlesSelection",
        "SharedObjectsContainer<LHCb::ProtoParticle>":
        "ProtoParticlesSelection",
        "SharedObjectsContainer<LHCb::Event::v1::Track>":
        "TracksSelection",
        "SharedObjectsContainer<LHCb::RichPID>":
        "RichPIDsSelection",
        "SharedObjectsContainer<LHCb::MuonPID>":
        "MuonPIDsSelection",
        "SharedObjectsContainer<LHCb::RecVertex>":
        "PVsSelection",
        "SharedObjectsContainer<LHCb::Vertex>":
        "VerticesSelection",
        "SharedObjectsContainer<LHCb::TwoProngVertex>":
        "TwoProngVerticesSelection",
        "SharedObjectsContainer<LHCb::CaloHypo>":
        "CaloHyposSelection",
        "SharedObjectsContainer<LHCb::CaloCluster>":
        "CaloClustersSelection",
        "SharedObjectsContainer<LHCb::FlavourTag>":
        "FlavourTagsSelection",
        "KeyedContainer<LHCb::Particle,Containers::KeyedObjectManager<Containers::hashmap> >":
        "Particles",
        "KeyedContainer<LHCb::ProtoParticle,Containers::KeyedObjectManager<Containers::hashmap> >":
        "ProtoParticles",
        "KeyedContainer<LHCb::Event::v1::Track,Containers::KeyedObjectManager<Containers::hashmap> >":
        "Tracks",
        "KeyedContainer<LHCb::RichPID,Containers::KeyedObjectManager<Containers::hashmap> >":
        "RichPIDs",
        "KeyedContainer<LHCb::CaloChargedPID,Containers::KeyedObjectManager<Containers::hashmap> >":
        "CaloChargedPIDs",
        "KeyedContainer<LHCb::BremInfo,Containers::KeyedObjectManager<Containers::hashmap> >":
        "BremInfos",
        "KeyedContainer<LHCb::MuonPID,Containers::KeyedObjectManager<Containers::hashmap> >":
        "MuonPIDs",
        "KeyedContainer<LHCb::GlobalChargedPID,Containers::KeyedObjectManager<Containers::hashmap> >":
        "GlobalChargedPIDs",
        "KeyedContainer<LHCb::NeutralPID,Containers::KeyedObjectManager<Containers::hashmap> >":
        "NeutralPIDs",
        "KeyedContainer<LHCb::RecVertex,Containers::KeyedObjectManager<Containers::hashmap> >":
        "PVs",
        "KeyedContainer<LHCb::Vertex,Containers::KeyedObjectManager<Containers::hashmap> >":
        "Vertices",
        "KeyedContainer<LHCb::TwoProngVertex,Containers::KeyedObjectManager<Containers::hashmap> >":
        "TwoProngVertices",
        "KeyedContainer<LHCb::CaloHypo,Containers::KeyedObjectManager<Containers::hashmap> >":
        "CaloHypos",
        "KeyedContainer<LHCb::CaloCluster,Containers::KeyedObjectManager<Containers::hashmap> >":
        "CaloClusters",
        "KeyedContainer<LHCb::CaloDigit,Containers::KeyedObjectManager<Containers::hashmap> >":
        "CaloDigits",
        "KeyedContainer<LHCb::CaloAdc,Containers::KeyedObjectManager<Containers::hashmap> >":
        "CaloAdcs",
        "KeyedContainer<LHCb::FlavourTag,Containers::KeyedObjectManager<Containers::hashmap> >":
        "FlavourTags",
        "KeyedContainer<LHCb::WeightsVector,Containers::KeyedObjectManager<Containers::hashmap> >":
        "WeightsVectors",
        "LHCb::Relation1D<LHCb::Particle,LHCb::VertexBase>":
        "P2VRelations",
        "LHCb::Relation1D<LHCb::Particle,LHCb::MCParticle>":
        "P2MCPRelations",
        "LHCb::Relation1D<LHCb::Particle,int>":
        "P2IntRelations",
        "LHCb::Relation1D<LHCb::Particle,LHCb::RelatedInfoMap>":
        "P2InfoRelations",
        "LHCb::RelationWeighted1D<LHCb::ProtoParticle,LHCb::MCParticle,double>":
        "PP2MCPRelations",
        "LHCb::RecSummary":
        "RecSummary",
        "LHCb::Event::PV::PrimaryVertexContainer":
        "ExtendedPVs",
    }

    return type_map


def __hlt_decisions(source, decreports):
    """Return a HltDecReportsDecoder instance for HLT1, HLT2, Spruce or Turbo decisions."""
    return HltDecReportsDecoder(
        name=source + "DecReportsDecoder",
        SourceID=source,
        RawBanks=decreports,
        outputs={
            'OutputHltDecReportsLocation':
            force_location(f'/Event/{source}/DecReports')
        },
    )


def do_unpacking(input_process='Hlt2', has_mc_data=True, raw_event_format=0.5):
    """Return CompositeNode that will do a full unpacking of data

    Args:
        input_process (str): 'Turbo' or 'Spruce' or 'Hlt2' - serves to determine `RawEventLocation` only.
        raw_event_format (float):
    """
    assert input_process in [
        "Spruce", "Turbo", "Hlt2"
    ], 'Unpacking helper only accepts Turbo, Spruce or Hlt2 processes'

    rawevent = default_raw_event("DstData")
    routingBits = default_raw_banks("HltRoutingBits")
    decreports = default_raw_banks("HltDecReports")
    dstdata = default_raw_banks("DstData")
    hlt_decision = __hlt_decisions("Hlt2", decreports)

    basealgs = [rawevent, dstdata, decreports, routingBits]
    algs = [hlt_decision]

    # if sprucing, get sprucing decisions
    if input_process == "Spruce" or input_process == "Turbo":
        algs += [__hlt_decisions("Spruce", decreports)]

    # decode raw banks to map of packed data buffers
    packed_decoder = HltPackedBufferDecoder(
        SourceID=input_process.replace("Turbo", "Hlt2"),
        DecReports=decreports,
        RawBanks=dstdata)
    algs += [packed_decoder]

    # get mc unpackers if needed
    if has_mc_data:
        assert input_process in [
            "Hlt2", "Turbo", "Spruce"
        ], 'MC unpacking only accepts Turbo, Spruce or Hlt2 process'
        prefix = 'Spruce' if input_process == "Spruce" else ''
        algs += [
            get_mc_vertices(
                os.path.join("/Event", prefix, "HLT2/MC/Vertices")).producer,
            get_mc_particles(
                os.path.join("/Event", prefix, "HLT2/MC/Particles")).producer
        ]

    # Finally unpack packed data buffers to objects
    algs += [UnpackDstDataBank(InputName=packed_decoder.OutputBuffers)]

    if input_process == "Hlt2":
        phys_filter = HltRoutingBitsFilter(
            name="PhysFilter",
            RawBanks=routingBits,
            RequireMask=(0, 0, 1 << (95 - 64)))
        algs = [
            phys_filter,
            CompositeNode(
                'filter',
                combine_logic=NodeLogic.LAZY_AND,
                children=algs,
                force_order=True)
        ]
    return CompositeNode(
        'unpacking',
        combine_logic=NodeLogic.LAZY_AND,
        children=basealgs + algs,
        force_order=True)
