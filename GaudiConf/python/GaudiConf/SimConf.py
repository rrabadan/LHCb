###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
Configurable for Gauss output
"""
__version__ = "v17r0"
__author__ = "Chris Jones <Christopher.Rob.Jones@cern.ch>"

__all__ = [
    'SimConf'  ## the configurable, configures SIM writing/packing/unpacking
]

from Gaudi.Configuration import *
from Configurables import LHCbConfigurableUser as _LHCbConfigurableUser
import GaudiKernel.ProcessJobOptions


class SimConf(_LHCbConfigurableUser):

    __slots__ = {
        "Writer":
        "NONE"  # Name of the SIM Writer
        ,
        "Phases":
        ["Generator",
         "Simulation"]  # The simulation phases to include in the SIM file
        ,
        "SpilloverPaths": []  # Paths to write out when spillover is enabled
        ,
        "EnablePack":
        True  # Turn on/off packing of the SIM data
        ,
        "EnableUnpack":
        True  # Configure the SIM unpacking via the Data On Demand Service
        ,
        "DataUnpackingSeq":
        None  # If set, the data unpacking will be run explicitly in the given sequence
        ,
        "Detectors": ['VP', 'UT', 'FT', 'Rich', 'Muon', 'Ecal',
                      'Hcal']  # Active sub-detectors
        ,
        "PackingSequencers": {
        }  # The packing sequence to fill for each spillover event
        ,
        "DataType":
        ""  # Flag for backward compatibility with old data
        ,
        "SaveHepMC":
        True  # If False, do not save HepMC on output file
        ,
        "pGun":
        False  # If using a PGUN configuration, write the "reco" vertex as well
    }

    def allEventLocations(self):
        crossingList = ['']
        spillOverList = self.getProp("SpilloverPaths")
        if '' in spillOverList: spillOverList.remove('')
        crossingList += spillOverList
        return crossingList

    def _doWrite(self):
        """
        Define the file content and write it out
        """
        if not self.isPropertySet("Writer"):
            log.debug("No Writer defined for SIM")
            return
        self._defineOutputData()

    def slot_(self, slot):
        if slot != '': return slot + '/'
        return slot

    def tapeLocation(self, slot, root, item):
        return '/Event/' + self.slot_(slot) + root + '/' + item + '#1'

    def dodLocation(self, slot, root, item):
        return '/Event/' + self.slot_(slot) + root + '/' + item

    def mcTESRoot(self):
        mcRoot = 'MC'
        if self.getProp('EnablePack'): mcRoot = 'pSim'
        return mcRoot

    def writer(self):
        tape = OutputStream(
            self.getProp("Writer"), Preload=False, OutputLevel=3)
        return tape

    def _doPacking(self):

        from Configurables import (PackMCParticle, PackMCVertex)

        # Active Detectors
        dets = self.getProp("Detectors")

        # Packing sequences
        packingSeqs = self.getProp("PackingSequencers")

        # output content
        for slot in self.allEventLocations():

            if slot in packingSeqs:

                packing = packingSeqs[slot]

                if slot != '': packing.RootInTES = slot

                packMCP = PackMCParticle("PackMCParticle" + slot)
                packMCV = PackMCVertex("PackMCVertex" + slot)
                packing.Members += [packMCP, packMCV]

                if 'Velo' in dets:
                    from Configurables import MCVeloHitPacker
                    packing.Members += [
                        MCVeloHitPacker("MCVeloHitPacker" + slot)
                    ]

                if 'PuVeto' in dets:
                    from Configurables import MCPuVetoHitPacker
                    packing.Members += [
                        MCPuVetoHitPacker("MCPuVetoHitPacker" + slot)
                    ]

                if 'VP' in dets:
                    from Configurables import MCVPHitPacker
                    packing.Members += [MCVPHitPacker("MCVPHitPacker" + slot)]

                if 'TV' in dets:
                    from Configurables import MCTVHitPacker
                    packing.Members += [MCTVHitPacker("MCTVHitPacker" + slot)]

                if 'Plume' in dets:
                    from Configurables import MCPlumeHitPacker
                    packing.Members += [
                        MCPlumeHitPacker("MCPlumeHitPacker" + slot)
                    ]

                if 'TT' in dets:
                    from Configurables import MCTTHitPacker
                    packing.Members += [MCTTHitPacker("MCTTHitPacker" + slot)]

                if 'UT' in dets:
                    from Configurables import MCUTHitPacker
                    packing.Members += [MCUTHitPacker("MCUTHitPacker" + slot)]

                if 'UP' in dets:
                    from Configurables import MCUPHitPacker
                    packing.Members += [MCUPHitPacker("MCUPHitPacker" + slot)]

                if 'IT' in dets:
                    from Configurables import MCITHitPacker
                    packing.Members += [MCITHitPacker("MCITHitPacker" + slot)]

                if 'SL' in dets:
                    from Configurables import MCSLHitPacker
                    packing.Members += [MCSLHitPacker("MCSLHitPacker" + slot)]

                if 'OT' in dets:
                    from Configurables import MCOTHitPacker
                    packing.Members += [MCOTHitPacker("MCOTHitPacker" + slot)]

                if 'FT' in dets:
                    from Configurables import MCFTHitPacker
                    packing.Members += [MCFTHitPacker("MCFTHitPacker" + slot)]

                if 'MP' in dets:
                    from Configurables import MCMPHitPacker
                    packing.Members += [MCMPHitPacker("MCMPHitPacker" + slot)]

                if 'Muon' in dets:
                    from Configurables import MCMuonHitPacker
                    packing.Members += [
                        MCMuonHitPacker("MCMuonHitPacker" + slot)
                    ]

                if 'Prs' in dets:
                    from Configurables import MCPrsHitPacker
                    packing.Members += [
                        MCPrsHitPacker("MCPrsHitPacker" + slot)
                    ]

                if 'Spd' in dets:
                    from Configurables import MCSpdHitPacker
                    packing.Members += [
                        MCSpdHitPacker("MCSpdHitPacker" + slot)
                    ]

                if 'Ecal' in dets:
                    from Configurables import MCEcalHitPacker
                    packing.Members += [
                        MCEcalHitPacker("MCEcalHitPacker" + slot)
                    ]

                if 'Hcal' in dets:
                    from Configurables import MCHcalHitPacker
                    packing.Members += [
                        MCHcalHitPacker("MCHcalHitPacker" + slot)
                    ]

                if 'Rich' in dets:
                    from Configurables import MCRichHitPacker
                    from Configurables import MCRichOpticalPhotonPacker
                    from Configurables import MCRichSegmentPacker
                    from Configurables import MCRichTrackPacker
                    packing.Members += [
                        MCRichHitPacker("MCRichHitPacker" + slot)
                    ]
                    packing.Members += [
                        MCRichOpticalPhotonPacker("MCRichOpPhotPacker" + slot)
                    ]
                    packing.Members += [
                        MCRichSegmentPacker("MCRichSegmentPacker" + slot)
                    ]
                    packing.Members += [
                        MCRichTrackPacker("MCRichTrackPacker" + slot)
                    ]

                if 'HC' in dets:
                    from Configurables import MCHCHitPacker
                    packing.Members += [MCHCHitPacker("MCHCHitPacker" + slot)]

                if 'Bcm' in dets:
                    from Configurables import MCBcmHitPacker
                    packing.Members += [
                        MCBcmHitPacker("MCBcmHitPacker" + slot)
                    ]

                if 'Bls' in dets:
                    from Configurables import MCBlsHitPacker
                    packing.Members += [
                        MCBlsHitPacker("MCBlsHitPacker" + slot)
                    ]

        # print "SimConf.py: _doPacking(): packing.Members =", packing.Members

    def _makeUnpacker(self, type, name, slot, tesLoc):

        unp = type(name + slot)
        if slot != '': unp.RootInTES = slot
        if self.isPropertySet("DataUnpackingSeq"):
            self.getProp("DataUnpackingSeq").Members += [unp]
        else:
            output = self.dodLocation(slot, 'MC', tesLoc)
            DataOnDemandSvc().AlgMap[output] = unp

    def _doUnpacking(self):

        from Configurables import DataOnDemandSvc
        from Configurables import UnpackMCParticle, UnpackMCVertex

        # Define the event locations
        crossingList = self.allEventLocations()

        # Active Detectors
        dets = self.getProp("Detectors")

        ## event locations
        for slot in crossingList:

            self._makeUnpacker(UnpackMCParticle, "UnpackMCParticles", slot,
                               'Particles')
            self._makeUnpacker(UnpackMCVertex, "UnpackMCVertices", slot,
                               'Vertices')

            if 'Velo' in dets:
                from Configurables import MCVeloHitUnpacker
                self._makeUnpacker(MCVeloHitUnpacker, "UnpackMCVeloHits", slot,
                                   'Velo/Hits')

            if 'PuVeto' in dets:
                from Configurables import MCPuVetoHitUnpacker
                self._makeUnpacker(MCPuVetoHitUnpacker, "UnpackMCPuVetoHits",
                                   slot, 'PuVeto/Hits')

            if 'VP' in dets:
                from Configurables import MCVPHitUnpacker
                self._makeUnpacker(MCVPHitUnpacker, "UnpackMCVPHits", slot,
                                   'VP/Hits')

            if 'TV' in dets:
                from Configurables import MCTVHitUnpacker
                self._makeUnpacker(MCTVHitUnpacker, "UnpackMCTVHits", slot,
                                   'TV/Hits')

            if 'Plume' in dets:
                from Configurables import MCPlumeHitUnpacker
                self._makeUnpacker(MCPlumeHitUnpacker, "UnpackMCPlumeHits",
                                   slot, 'Plume/Hits')

            if 'IT' in dets:
                from Configurables import MCITHitUnpacker
                self._makeUnpacker(MCITHitUnpacker, "UnpackMCITHits", slot,
                                   'IT/Hits')

            if 'SL' in dets:
                from Configurables import MCSLHitUnpacker
                self._makeUnpacker(MCSLHitUnpacker, "UnpackMCSLHits", slot,
                                   'SL/Hits')

            if 'OT' in dets:
                from Configurables import MCOTHitUnpacker
                self._makeUnpacker(MCOTHitUnpacker, "UnpackMCOTHits", slot,
                                   'OT/Hits')

            if 'FT' in dets:
                from Configurables import MCFTHitUnpacker
                self._makeUnpacker(MCFTHitUnpacker, "UnpackMCFTHits", slot,
                                   'FT/Hits')

            if 'MP' in dets:
                from Configurables import MCMPHitUnpacker
                self._makeUnpacker(MCMPHitUnpacker, "UnpackMCMPHits", slot,
                                   'MP/Hits')

            if 'TT' in dets:
                from Configurables import MCTTHitUnpacker
                self._makeUnpacker(MCTTHitUnpacker, "UnpackMCTTHits", slot,
                                   'TT/Hits')

            if 'UT' in dets:
                from Configurables import MCUTHitUnpacker
                self._makeUnpacker(MCUTHitUnpacker, "UnpackMCUTHits", slot,
                                   'UT/Hits')

            if 'UP' in dets:
                from Configurables import MCUPHitUnpacker
                self._makeUnpacker(MCUPHitUnpacker, "UnpackMCUPHits", slot,
                                   'UP/Hits')

            if 'Muon' in dets:
                from Configurables import MCMuonHitUnpacker
                self._makeUnpacker(MCMuonHitUnpacker, "UnpackMCMuonHits", slot,
                                   'Muon/Hits')

            if 'Spd' in dets:
                from Configurables import MCSpdHitUnpacker
                self._makeUnpacker(MCSpdHitUnpacker, "UnpackMCSpdHits", slot,
                                   'Spd/Hits')

            if 'Prs' in dets:
                from Configurables import MCPrsHitUnpacker
                self._makeUnpacker(MCPrsHitUnpacker, "UnpackMCPrsHits", slot,
                                   'Prs/Hits')

            if 'Ecal' in dets:
                from Configurables import MCEcalHitUnpacker
                self._makeUnpacker(MCEcalHitUnpacker, "UnpackMCEcalHits", slot,
                                   'Ecal/Hits')

            if 'Hcal' in dets:
                from Configurables import MCHcalHitUnpacker
                self._makeUnpacker(MCHcalHitUnpacker, "UnpackMCHcalHits", slot,
                                   'Hcal/Hits')

            if 'Rich' in dets:
                from Configurables import MCRichHitUnpacker
                from Configurables import MCRichOpticalPhotonUnpacker
                from Configurables import MCRichSegmentUnpacker
                from Configurables import MCRichTrackUnpacker
                self._makeUnpacker(MCRichHitUnpacker, "UnpackMCRichHits", slot,
                                   'Rich/Hits')
                self._makeUnpacker(MCRichOpticalPhotonUnpacker,
                                   "UnpackMCRichPhots", slot,
                                   'Rich/OpticalPhotons')
                self._makeUnpacker(MCRichSegmentUnpacker,
                                   "UnpackMCRichSegments", slot,
                                   'Rich/Segments')
                self._makeUnpacker(MCRichTrackUnpacker, "UnpackMCRichTracks",
                                   slot, 'Rich/Tracks')

            if 'HC' in dets:
                from Configurables import MCHCHitUnpacker
                self._makeUnpacker(MCHCHitUnpacker, "UnpackMCHCHits", slot,
                                   'HC/Hits')

            if 'Bcm' in dets:
                from Configurables import MCBcmHitUnpacker
                self._makeUnpacker(MCHCHitUnpacker, "UnpackMCBmcHits", slot,
                                   'Bcm/Hits')

            if 'Bls' in dets:
                from Configurables import MCBlsHitUnpacker
                self._makeUnpacker(MCHCHitUnpacker, "UnpackMCBlsHits", slot,
                                   'Bls/Hits')

    def addHeaders(self, tape):

        # Event locations
        for slot in self.allEventLocations():

            list = []

            if "Generator" in self.getProp("Phases"):
                list += [self.tapeLocation(slot, 'Gen', 'Header')]

            if "Simulation" in self.getProp(
                    "Phases") or "GenToMCTree" in self.getProp("Phases"):
                list += [self.tapeLocation(slot, 'MC', 'Header')]

            # main event is mandatory, spillover events optional.
            if slot != '':
                tape.OptItemList += list
            else:
                tape.ItemList += list

    def addGenInfo(self, tape):

        # Add Generator level information
        if "Generator" in self.getProp("Phases"):

            # Save HepMC and BeamParameters only for main event
            if self.getProp("DataType") not in ['2009', '2010', 'MC09']:
                tape.ItemList += ['/Event/Gen/BeamParameters#1']
                # OptItem so that, when used by applications other than Gauss, they are saved if saved by Gauss
                if self.getProp('SaveHepMC'):
                    tape.OptItemList += ['/Event/Gen/HepMCEvents#1']

            for slot in self.allEventLocations():
                # main event is mandatory, spillover events optional.
                if slot != '':
                    tape.OptItemList += [
                        self.tapeLocation(slot, 'Gen', 'Collisions')
                    ]
                else:
                    tape.ItemList += [
                        self.tapeLocation(slot, 'Gen', 'Collisions')
                    ]

    def addMCParticles(self, tape, eventLocations):

        if "Simulation" in self.getProp(
                "Phases") or "GenToMCTree" in self.getProp("Phases"):

            # Event locations
            for slot in eventLocations:

                # Annoyingly (MC)Particles and (MC)Vertices change their names when packed ...
                if not self.getProp('EnablePack'):
                    simList = [self.tapeLocation(slot, 'MC', 'Particles')]
                else:
                    simList = [self.tapeLocation(slot, 'pSim', 'MCParticles')]

                # main event is manditory, spillover events optional.
                if slot != '':
                    tape.OptItemList += simList
                else:
                    tape.ItemList += simList

    def addMCVertices(self, tape, eventLocations):

        if "Simulation" in self.getProp(
                "Phases") or "GenToMCTree" in self.getProp("Phases"):

            # Event locations
            for slot in eventLocations:

                # Annoyingly (MC)Particles and (MC)Vertices change their names when packed ...
                if not self.getProp('EnablePack'):
                    simList = [self.tapeLocation(slot, 'MC', 'Vertices')]
                else:
                    simList = [self.tapeLocation(slot, 'pSim', 'MCVertices')]
                #add PGun primary vertex writing
                if self.getProp('pGun'):
                    simList += [
                        self.tapeLocation(slot, 'Rec/Vertex', 'Primary')
                    ]
                else:
                    pass
                # main event is manditory, spillover events optional.
                if slot != '':
                    tape.OptItemList += simList
                else:
                    tape.ItemList += simList

    def addGeneralSimInfo(self, tape, eventLocations):

        self.addMCVertices(tape, eventLocations)
        self.addMCParticles(tape, eventLocations)

    def addSubDetSimInfo(self, tape):

        if "Simulation" in self.getProp("Phases"):

            # Active Detectors
            dets = self.getProp("Detectors")

            # Event locations
            for slot in self.allEventLocations():

                mcRoot = self.mcTESRoot()

                simList = []

                if 'Velo' in dets:
                    simList += [self.tapeLocation(slot, mcRoot, 'Velo/Hits')]

                if 'PuVeto' in dets:
                    simList += [self.tapeLocation(slot, mcRoot, 'PuVeto/Hits')]

                if 'VP' in dets:
                    simList += [self.tapeLocation(slot, mcRoot, 'VP/Hits')]

                if 'TV' in dets:
                    simList += [self.tapeLocation(slot, mcRoot, 'TV/Hits')]

                if 'Plume' in dets:
                    simList += [self.tapeLocation(slot, mcRoot, 'Plume/Hits')]

                if 'IT' in dets:
                    simList += [self.tapeLocation(slot, mcRoot, 'IT/Hits')]

                if 'OT' in dets:
                    simList += [self.tapeLocation(slot, mcRoot, 'OT/Hits')]

                if 'FT' in dets:
                    simList += [self.tapeLocation(slot, mcRoot, 'FT/Hits')]

                if 'MP' in dets:
                    simList += [self.tapeLocation(slot, mcRoot, 'MP/Hits')]

                if 'TT' in dets:
                    simList += [self.tapeLocation(slot, mcRoot, 'TT/Hits')]

                if 'UT' in dets:
                    simList += [self.tapeLocation(slot, mcRoot, 'UT/Hits')]

                if 'UP' in dets:
                    simList += [self.tapeLocation(slot, mcRoot, 'UP/Hits')]

                if 'Muon' in dets:
                    simList += [self.tapeLocation(slot, mcRoot, 'Muon/Hits')]

                if 'Spd' in dets:
                    simList += [self.tapeLocation(slot, mcRoot, 'Spd/Hits')]

                if 'Prs' in dets:
                    simList += [self.tapeLocation(slot, mcRoot, 'Prs/Hits')]

                if 'Ecal' in dets:
                    simList += [self.tapeLocation(slot, mcRoot, 'Ecal/Hits')]

                if 'Hcal' in dets:
                    simList += [self.tapeLocation(slot, mcRoot, 'Hcal/Hits')]

                if 'SL' in dets:
                    simList += [self.tapeLocation(slot, mcRoot, 'SL/Hits')]

                if 'Rich' in dets:
                    simList += [
                        self.tapeLocation(slot, mcRoot, 'Rich/Hits'),
                        self.tapeLocation(slot, mcRoot, 'Rich/OpticalPhotons'),
                        self.tapeLocation(slot, mcRoot, 'Rich/Tracks'),
                        self.tapeLocation(slot, mcRoot, 'Rich/Segments')
                    ]
                    # Linkers annoyingly put things in slightly different places ...
                    if slot != '':
                        simList += [
                            '/Event/Link/' + slot +
                            '/MC/Particles2MCRichTracks#1', '/Event/Link/' +
                            slot + '/MC/Rich/Hits2MCRichOpticalPhotons#1'
                        ]
                    else:
                        simList += [
                            '/Event/Link/MC/Particles2MCRichTracks#1',
                            '/Event/Link/MC/Rich/Hits2MCRichOpticalPhotons#1'
                        ]

                if 'HC' in dets:
                    simList += [self.tapeLocation(slot, mcRoot, 'HC/Hits')]

                if 'Bcm' in dets:
                    simList += [self.tapeLocation(slot, mcRoot, 'Bcm/Hits')]

                if 'Bls' in dets:
                    simList += [self.tapeLocation(slot, mcRoot, 'Bls/Hits')]

                # main event is manditory, spillover events optional.
                if slot != '':
                    tape.OptItemList += simList
                else:
                    tape.ItemList += simList

        # print "addSubDetSimInfo(", tape, "): simList =", simList

    def _defineOutputData(self):
        """
        Define content of the output dataset
        """

        # Get the Writer instance
        tape = self.writer()

        # Empty lists to start
        tape.ItemList = []
        tape.OptItemList = []

        # Headers
        self.addHeaders(tape)

        # Add generator level information
        self.addGenInfo(tape)

        # Add general simulation information
        self.addGeneralSimInfo(tape, self.allEventLocations())

        # Add Sub-detector information
        self.addSubDetSimInfo(tape)

        # Some printout ...
        log.info(
            "%s.ItemList    = %s" % (self.getProp("Writer"), tape.ItemList))
        log.info(
            "%s.OptItemList = %s" % (self.getProp("Writer"), tape.OptItemList))
        tape.ExtraInputs = [t.replace('#1', '') for t in \
                            tape.ItemList + tape.OptItemList]

    def __apply_configuration__(self):
        GaudiKernel.ProcessJobOptions.PrintOn(force=True)

        self._doWrite()

        if self.getProp("EnablePack"): self._doPacking()
        if self.getProp("EnableUnpack"): self._doUnpacking()

        GaudiKernel.ProcessJobOptions.PrintOff()
