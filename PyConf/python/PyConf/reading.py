###############################################################################
# (c) Copyright 2020-2022 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from PyConf.packing import reco_locations, pp2mcp_locations
from PyConf.components import force_location
from PyConf.dataflow import DataHandle
from PyConf.tonic import configurable
from PyConf.Algorithms import (
    UnpackMCParticle,
    UnpackMCVertex,
    HltSelReportsDecoder,
    RawBankSizeFilter,
    Gaudi__DataLink,
)
from PyConf.application import (default_raw_banks, make_odin,
                                input_from_root_file)
from PyConf.packing import persistreco_version, default_persistable_location as persistable_location
from GaudiConf.LbExec import InputProcessTypes, HltSourceID
import os


def _unpackers_map():

    from PyConf.Algorithms import (
        RecVertexUnpacker, PrimaryVertexUnpacker, TrackUnpacker,
        TrackSelectionUnpacker, RichPIDUnpacker, MuonPIDUnpacker,
        GlobalChargedPIDUnpacker, NeutralPIDUnpacker, CaloChargedPIDUnpacker,
        BremInfoUnpacker, CaloHypoUnpacker, CaloClusterUnpacker,
        CaloDigitUnpacker, CaloAdcUnpacker, ProtoParticleUnpacker,
        ProtoParticleSelectionUnpacker, ParticleUnpacker,
        ParticleSelectionUnpacker, VertexUnpacker, FlavourTagUnpacker,
        FlavourTagSelectionUnpacker, P2VRelationUnpacker,
        P2MCPRelationUnpacker, PP2MCPRelationUnpacker, P2IntRelationUnpacker,
        P2InfoRelationUnpacker, RecSummaryUnpacker, SOATrackUnpacker,
        SOACaloClusterUnpacker, SOACaloHypoUnpacker, SOAPVUnpacker)

    return {
        "Tracks": TrackUnpacker,
        "TracksSelection": TrackSelectionUnpacker,
        "RichPIDs": RichPIDUnpacker,
        "CaloChargedPIDs": CaloChargedPIDUnpacker,
        "BremInfos": BremInfoUnpacker,
        "MuonPIDs": MuonPIDUnpacker,
        "GlobalChargedPID": GlobalChargedPIDUnpacker,
        "NeutralPIDs": NeutralPIDUnpacker,
        "CaloHypos": CaloHypoUnpacker,
        "CaloClusters": CaloClusterUnpacker,
        "CaloDigits": CaloDigitUnpacker,
        "CaloAdcs": CaloAdcUnpacker,
        "LightPVs": PrimaryVertexUnpacker,
        "PVs": RecVertexUnpacker,
        "ProtoParticles": ProtoParticleUnpacker,
        "ProtoParticlesSelection": ProtoParticleSelectionUnpacker,
        "Particles": ParticleUnpacker,
        "ParticlesSelection": ParticleSelectionUnpacker,
        "Vertices": VertexUnpacker,
        "FlavourTags": FlavourTagUnpacker,
        "FlavourTagsSelection": FlavourTagSelectionUnpacker,
        "P2VRelations": P2VRelationUnpacker,
        "P2MCPRelations": P2MCPRelationUnpacker,
        "P2IntRelations": P2IntRelationUnpacker,
        "P2InfoRelations": P2InfoRelationUnpacker,
        "PP2MCPRelations": PP2MCPRelationUnpacker,
        "RecSummary": RecSummaryUnpacker,
        "Tracks_v2": SOATrackUnpacker,
        "CaloClusters_v2": SOACaloClusterUnpacker,
        "CaloHypos_v2": SOACaloHypoUnpacker,
        "ExtendedPVs": SOAPVUnpacker
    }


@configurable
def tes_root(*, input_process: InputProcessTypes = InputProcessTypes.Hlt2):
    """
  Get the ROOT_TES location from the input_process type
  """
    if input_process == InputProcessTypes.Spruce:
        return '/Event/Spruce/HLT2'
    elif input_process in [
            InputProcessTypes.Hlt2, InputProcessTypes.TurboPass
    ]:
        return '/Event/HLT2'
    else:
        raise NotImplementedError(
            f"The specified 'input_process' {input_process} is not recognised. Can only be 'Hlt2' or 'Spruce' or 'TurboPass'. Please check!"
        )


@configurable
def source_id(*, input_process: InputProcessTypes = InputProcessTypes.Hlt2):
    """
  Get the ROOT_TES location from the input_process type
  """
    if input_process == InputProcessTypes.Spruce:
        return HltSourceID.Spruce
    elif input_process in [
            InputProcessTypes.Hlt2, InputProcessTypes.TurboPass
    ]:
        return HltSourceID.Hlt2
    else:
        raise NotImplementedError(
            f"The specified 'input_process' {input_process} is not recognised. Can only be 'Hlt2' or 'Spruce' or 'TurboPass'. Please check!"
        )


@configurable
def upfront_reconstruction(
        simulation=False,
        input_process: InputProcessTypes = InputProcessTypes.Hlt2):
    """Return a list DataHandles that define the upfront reconstruction output.

    This differs from `reconstruction` as it should not be used as inputs to
    other algorithms, but only to define the control flow, i.e. the return
    value of this function should be ran before all HLT2 lines.

    """

    stream = tes_root()

    reco_loc = reco_locations(stream)

    if simulation:
        reco_loc |= pp2mcp_locations(stream)

    unpackers = [_get_unpacked(v[1], v[0]) for v in reco_loc.values()]

    mc_algs = []
    if simulation:
        mc_algs = [
            get_mc_vertices(os.path.join(stream, 'MC/Vertices')).producer,
            get_mc_particles(os.path.join(stream, 'MC/Particles')).producer
        ]

    ### TODO:FIXME take advantage of the fact that the above have datahandles...
    # i.e. should _not_ have to return decoder here, and should just return the _output handles_ and not the algorithms
    # i.e. `upfront_reconstruction` should be a drop-in replacement for `reconstruction()`, with the same return type
    return [
        dstdata_filter(input_process.sourceID()),
        upfront_decoder(input_process.sourceID()).producer
    ] + mc_algs + unpackers


@configurable
def dstdata_filter(source: HltSourceID = InputProcessTypes.Hlt2,
                   raw_banks=default_raw_banks):
    """
    Setting a `RawBankSizeFilter` filter on the DstData bank size.

    This filter will skip events with empty DstData but a positive line decision.
    This may occur in events where DstData is a-priori huge (>16MB), and cannot be saved.

    Returns:
    The `RawBankSizeFilter` algorithm instance.
    """
    # TODO: should be able to specify the SourceID of the DstData raw bank...
    # and thus require a HltSourceID as arg to this function -- augment the Filter to check the
    #  sourceID...
    # FIXME: since we cannot filter on the sourceID, we should _not_ fold `source` into the name
    #        Once we can distinguish, the name can become more specific (again)
    return RawBankSizeFilter(
        # name='FilterDstDataSize_{}'.format(source),
        name='FilterDstDataSize',
        RawBankLocation=default_raw_banks("DstData"))


@configurable
def upfront_decoder(*,
                    source: HltSourceID = "Hlt2",
                    raw_banks=default_raw_banks):
    """Return a DataHandle for  HltPackedBufferDecoder output
       This is input to all unpacking algorithms except MC and old unpacker
    """
    from PyConf.Algorithms import HltPackedBufferDecoder
    source = HltSourceID(source)
    return HltPackedBufferDecoder(
        name="PackedBufferDecoder_{}".format(source.name),
        SourceID=source.name,
        DecReports=raw_banks("HltDecReports"),
        RawBanks=raw_banks("DstData")).OutputBuffers


def location2source(location):
    # deal with the fact that sourceID is eg. {"Spruce","Hlt1","Hlt2",...},
    # but the prefixes are "Spruce", "HLT1", "HLT2",... sigh...
    prefix_map = {"Hlt1": "HLT1", "Hlt2": "HLT2"}
    prefix_for = lambda sid: f"/Event/{prefix_map.get(sid.value,sid.value)}"
    for sid in HltSourceID:
        if location.startswith(prefix_for(sid)): return sid
    raise KeyError(f"cannot determine source id for location: '{location}'")


def _get_unpacked(type_name, location, **kwargs):
    sourceID = kwargs.get("source", location2source(location))
    if 'name' not in kwargs:
        # TODO: sourceID in name is redundant if location2source was used
        #       but removing it at this time would require *lots* of ref-updates...
        kwargs['name'] = 'Unpack_{}_{}'.format(sourceID,
                                               location.replace('/', '_'))
    return _unpackers_map()[type_name](
        InputName=upfront_decoder(sourceID),
        outputs={
            "OutputName": force_location(location)
        },
        **kwargs).OutputName


def get_particles(location, **kwargs):
    """
    Unpacking of particles. This will also unpack the decay vertices associated with these particles implicitely
    """
    return _get_unpacked("ParticlesSelection", location, **kwargs)


def get_flavourtags(location, **kwargs):
    """
    Unpacking of flavor tags. This will also unpack the associated tagged B particles implicitely.
    """
    return _get_unpacked("FlavourTagsSelection", location, **kwargs)


def get_pp2mcp_relations(location):
    """
    Unpacking of ProtoParticle to MCParticle relations. These are needed for truth matching
    MC Particles need to be unpacked first, proto particles are unpacked within relation unpacking
    Assume relations and MC particles are under same TES root
    """

    mc_location = location.replace(
        "Relations/ChargedPP2MCP" if "Charged" in location else
        "Relations/NeutralPP2MCP", "MC/Particles")
    mc_parts = get_mc_particles(mc_location)

    return _get_unpacked("PP2MCPRelations", location, ExtraInputs=[mc_parts])


def get_p2v_relations(location):
    """
    Unpacking of P2V relations. Make sure particles are unpacked
    """
    return _get_unpacked("P2VRelations", location)


def get_mc_particles(location, mc_vertices: DataHandle = None):
    """
    Unpacking of MCParticles.
    Chainning MCVertices as dependency is needed for truth matching, if mc_vertices is None, it will first unpack MCVertices

    Args:
        location (str): Location of the LHCb::MCParticles
        mc_vertices (DataHandle, optional): unpacked `LHCb::MCVertices`, defaults to None.

    Returns:
        DataHandle: DataHandle of `LHCb::MCParticles`.
    """

    sim_location = location.replace("MC/Particles", "pSim/MCParticles")

    if mc_vertices is None:
        mc_vertices = get_mc_vertices(
            location.replace("MC/Particles", "MC/Vertices"))

    return UnpackMCParticle(
        name='Unpack_{}'.format(location.replace('/', '_')),
        InputName=input_from_root_file(
            sim_location, forced_type='LHCb::PackedMCParticles'),
        outputs={
            "OutputName": force_location(location)
        },
        ExtraInputs=[mc_vertices],
    ).OutputName


def get_mc_vertices(location):
    """
    Unpacking of MCVertices.

    Args:
        location (str): Location of the LHCb::MCParticles

    Returns:
        mc_vertices (DataHandle): DataHandle of `LHCb::MCVertices`.
    """
    sim_location = location.replace("MC/Vertices", "pSim/MCVertices")

    return UnpackMCVertex(
        name='Unpack_{}'.format(location.replace('/', '_')),
        InputName=input_from_root_file(
            sim_location, forced_type='LHCb::PackedMCVertices'),
        outputs={
            "OutputName": force_location(location)
        },
    ).OutputName


@configurable
def reconstruction(*,
                   input_process: InputProcessTypes = InputProcessTypes.Hlt2,
                   simulation: bool = False,
                   packable: bool = True):
    stream = tes_root(input_process=input_process)
    reco_locs = reco_locations(stream=stream)
    if simulation:
        reco_locs |= pp2mcp_locations(stream)

    recos = {k: _get_unpacked(v[1], v[0]) for k, v in reco_locs.items()}

    postprocess_unpacked_data(recos, packable=packable)

    return recos


def get_charged_protoparticles(track_type):
    return reconstruction()[f"{track_type}Protos"]


def get_neutral_protoparticles():
    return reconstruction()["NeutralProtos"]


def get_extended_pvs():
    return reconstruction()["ExtendedPVs"]


def get_pvs():
    return reconstruction()["PVs"]


def get_rec_summary():
    return reconstruction()["RecSummary"]


def get_mc_track_info():
    return input_from_root_file(
        '/Event/MC/TrackInfo', forced_type='LHCb::MCProperty')


def get_mc_header():
    """Fetch LHCb::MCHeader object from file, this object contains:
        * Pointer to primary vertices
        * Event time
        * Event number
    Returns:
        DataHandle: DataHandle of the `LHCb::MCHeader`.
    """
    location = "/Event/MC/Header"
    mc_header = input_from_root_file(location, forced_type='LHCb::MCHeader')

    # LHCb::MCHeader contains SmartRefs to the container of LHCb::MCVertices.
    # TODO Ideally, here we need an algorithm that
    # - depends on (has as input) LHCb::MCVertices to ensure that they are unpacked
    # - validates that the location of the LHCb::MCVertices container is indeed
    #   where the SmartRefs point to,
    # - or, create a new LHCb::MCHeader that points to the given MCVertices
    # - has as output a link to or a copy of LHCb::MCHeader
    #
    # Here we take a shortcut and simply link the original MCHeader into the output
    # and we add the dependency on MCParticles (which depends on MCVertices) via ExtraInputs.

    # It is important to get the MCParticles/MCVertices linked to from MCHeader,
    # therefore the location is hardcoded.
    mc_particles = get_mc_particles("/Event/MC/Particles")

    def output_transform(Output):
        return {"Target": Output, "ExtraOutputs": [Output]}

    def input_transform(Input, MCParticles):
        return {"What": Input, "ExtraInputs": [MCParticles]}

    output = Gaudi__DataLink(
        name="LinkMCHeader_{hash}",
        Input=mc_header,
        MCParticles=mc_particles,
        input_transform=input_transform,
        output_transform=output_transform,
        outputs={
            "Output": force_location(location + "/Link")
        },
    ).Output
    output.force_type(mc_header.type)
    return output


def get_generator_header(*, location='/Event/Gen/Header'):
    """Fetch LHCb::GenHeader object from file
    Returns:
        DataHandle: DataHandle of the `LHCb::GenHeader`.
    """
    return input_from_root_file(location, forced_type='LHCb::GenHeader')


def get_odin():
    """
    Function to get the LHCb::ODIN location

    Returns:
        odin_loc: Location of the LHCb::ODIN
    """
    return make_odin()


@configurable
def get_hlt_reports(*, source: HltSourceID, raw_banks=default_raw_banks):
    """
    Set the Hlt service and algorithms.

    Args:
      source (HltSourceID): source ID or selection stage. It can be "Hlt1" or "Hlt2" or "Spruce".

    Returns:
      HltDecReportsDecoder containing the configuration for Hlt1, Hlt2 and Spruce lines.
    """

    source = HltSourceID(source)
    output_loc = "/Event/%s/DecReports" % source.name

    from PyConf.Algorithms import HltDecReportsDecoder
    return HltDecReportsDecoder(
        name=source.name + "DecReportsDecoder_{hash}",
        SourceID=source.name,
        RawBanks=raw_banks("HltDecReports"),
        outputs={'OutputHltDecReportsLocation': force_location(output_loc)},
    )


def get_decreports(source: HltSourceID):
    """
    Function to get the LHCb::DecReports for HLT1, HLT2 or Sprucing.

    Args:
        source (str): Selection stage can be "Hlt1" or "Hlt2" or "Spruce"
    Returns:
        dec_loc: Location of the LHCb::DecReports for HLT1 or Hlt2 or Spruce
    """
    return get_hlt_reports(source).OutputHltDecReportsLocation


@configurable
def get_hlt1_selreports(raw_banks=default_raw_banks):
    return HltSelReportsDecoder(
        DecReports=raw_banks("HltDecReports"),
        RawBanks=raw_banks("HltSelReports"),
        SourceID=HltSourceID.Hlt1.name).OutputHltSelReportsLocation


def postprocess_unpacked_data(data,
                              persistreco_version=persistreco_version,
                              packable=False):
    """ Needed to unpack older persistency versions into latest persistency style:
         - for PVs only in v1 format
         - for merged 'Tracks' and 'ChargedProtos' containers (to split them per track type)
         - for non-existant RecSummary

    """

    # PV v1 only persisted for now
    if "PVs" in data and "ExtendedPVs" not in data:
        from PyConf.Algorithms import RecV1ToPVConverter
        data["ExtendedPVs"] = RecV1ToPVConverter(
            InputVertices=data["PVs"]).OutputVertices
    elif "ExtendedPVs" in data and "PVs" not in data:
        from PyConf.Algorithms import PVToRecConverterV1WithoutTracks
        data["PVs"] = PVToRecConverterV1WithoutTracks(
            InputVertices=data["ExtendedPVs"],
            AddTrackWeights=False).OutputVertices

    # per track type splitter (to convert 0.0 persistency to latest)
    if persistreco_version() == 0.0:
        # use Selections (SharedObjectsContainers, so pointer remain valid to underlying objects, but not packable YET)
        # effectively front end only changed in terms of persistency locations
        from PyConf.Algorithms import TracksSharedSplitterPerType, ProtosSharedSplitterPerTrackType
        tracks_splitter = TracksSharedSplitterPerType(
            name='TrackContainerSharedSplitterPerType_{hash}',
            InputTracks=data['Tracks'],
            outputs={
                "LongTracks": persistable_location('LongTracks'),
                "DownstreamTracks": persistable_location('DownstreamTracks'),
                "UpstreamTracks": persistable_location('UpstreamTracks'),
                "Ttracks": persistable_location('Ttracks'),
                "VeloTracks": persistable_location('VeloTracks')
            })
        data['LongTracks'] = tracks_splitter.LongTracks
        data['DownstreamTracks'] = tracks_splitter.DownstreamTracks
        data['UpstreamTracks'] = tracks_splitter.UpstreamTracks
        data['Ttracks'] = tracks_splitter.Ttracks
        data['VeloTracks'] = tracks_splitter.VeloTracks
        # for protoparticles
        protos_splitter = ProtosSharedSplitterPerTrackType(
            name='ChargedProtoParticleSharedSplitterPerType_{hash}',
            InputProtos=data['ChargedProtos'],
            outputs={
                "LongProtos": persistable_location('LongProtos'),
                "DownstreamProtos": persistable_location('DownstreamProtos'),
                "UpstreamProtos": persistable_location('UpstreamProtos'),
                "TtrackProtos": None,
            })
        data['LongProtos'] = protos_splitter.LongProtos
        data['DownstreamProtos'] = protos_splitter.DownstreamProtos
        data['UpstreamProtos'] = protos_splitter.UpstreamProtos


@configurable
def tes_root_for_tistos(*, input_process: InputProcessTypes):
    """
    Get the ROOT_TES location from the input_process type used to get trigger particles for TISTOS.
    Only input processes Spruce and TurboPass are supported.
    Note that this might not be the correct solution see https://gitlab.cern.ch/lhcb/Moore/-/issues/663#note_7295503.
    """
    if input_process == InputProcessTypes.Spruce:
        return '/Event/Spruce/HLT2/TISTOS'
    elif input_process == InputProcessTypes.TurboPass:
        return '/Event/HLT2'
    else:
        raise ValueError(
            f"Requested ROOT_TES for TISTOS for input process {input_process}. This is not supported. Please check!"
        )
