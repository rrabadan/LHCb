###############################################################################
# (c) Copyright 2022 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
import os


def load_file(fname):
    if not fname: return None

    if fname.startswith("root:"):
        import XRootD.client
        with XRootD.client.File() as f:
            status, _ = f.open(str(fname))
            if not status.ok:
                raise RuntimeError(f"could not open {fname}: {status.message}")
            status, data = f.read()
            if not status.ok:
                raise RuntimeError(f"could not read {fname}: {status.message}")
            return data.decode('utf-8')
    else:
        with open(os.path.expandvars(fname), 'r', encoding='utf-8') as f:
            return f.read()
