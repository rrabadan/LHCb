###############################################################################
# (c) Copyright 2023 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from PyConf.control_flow import (CompositeNode, NodeLogic,
                                 traverse_node_and_children)
from PyConf.Algorithms import (Gaudi__Examples__IntDataProducer as
                               IntDataProducer)


def test_composite_node_hashing():
    """Two composite nodes created with the same name and children equal.

    This behaviour is required for implicit CF node de-duplication, ensuring
    that a set of CF nodes passed to HltControlFlowMgr contains exactly one
    representation of each node, which is a requirement of the algorithm.
    """
    child = IntDataProducer()
    n1 = CompositeNode(
        "Node1", children=[child], combine_logic=NodeLogic.LAZY_AND)
    n2 = CompositeNode(
        "Node1", children=[child], combine_logic=NodeLogic.LAZY_AND)
    n3 = CompositeNode(
        "Node1", children=[child], combine_logic=NodeLogic.LAZY_OR)
    assert n1 == n2
    assert hash(n1) == hash(n2)
    assert n1 != n3
    assert hash(n1) != hash(n3)


def test_traverse_node_and_children():
    child1 = IntDataProducer(name="Child1", Value=1)
    child2 = IntDataProducer(name="Child2", Value=2)
    bottom = CompositeNode("Bottom", children=[child1])
    mid = CompositeNode("Mid", children=[bottom, child2])
    top = CompositeNode("Top", children=[mid])
    nodes = ["Child1", "Child2", "Bottom", "Mid", "Top"]
    for node in traverse_node_and_children(top):
        assert node.name in nodes
        nodes.remove(node.name)
    assert len(nodes) == 0
