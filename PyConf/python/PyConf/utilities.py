###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
import difflib
import errno
import hashlib
import json
import multiprocessing
import os
import pickle
import re
import shutil
import subprocess
import tempfile


def DISABLE_TOOL(*args, **kwargs):
    """ Disables tool in the configuration by setting its (location) name to `None`.

        Returns:
            `None`

        Example::

            def make_PrForwardTracking_tracks(input_tracks, ut_hits_tool=DISABLE_TOOL):
                return PrForwardTracking(InputTracks=input_tracks["Pr"],
                                         AddUTHitsToolName=ut_hits_tool()).OutputTracks

        Note:
            This is merely a descriptive way of disabling tools, introduced because
            the configuration expects functions handed to the makers.
    """
    return None


class ConfigurationError(Exception):
    pass


def hash_bytes(*args):
    m = hashlib.blake2b(digest_size=8, usedforsecurity=False)
    for arg in args:
        m.update(arg + b"\0")
    # '{:0x}'.format(int.from_bytes(m.digest(), "big"))) == m.hexdigest()
    return int.from_bytes(m.digest(), "big")


def to_json(obj):
    """Return the value of `obj.to_json()`.

    The typical use-case is for dumping `BoundFunctor` and derived objects.

    Args:
        obj: Object with a `to_json` method member.

    Raises:
        TypeError: If `obj.to_json` raises an AttributeError.
    """
    try:
        to_json = obj.to_json  # call later to avoid catching other exceptions
    except AttributeError:
        raise TypeError(repr(obj) + " is not JSON serializable")
    return to_json()


def hash_object(x):
    """Return the hash of an object that we know how to serialize.

    Args:
        x (object supported by `_json_dump`): Object to be serialised.
    """
    s = json.dumps(x, default=to_json, sort_keys=True).encode()
    return hash_bytes(s)


def _read_options_pkl_impl(filename, all_opts=True):
    """Read a .pkl options file produced by gaudirun.py."""

    def all_props(c):
        d = c.getDefaultProperties()
        d.update(c.getValuedProperties())
        return d

    with open(filename, 'rb') as f:
        d = pickle.load(f)
    props = all_props if all_opts else (lambda c: c.getValuedProperties())
    return {
        '{} ({})'.format(c.getName(), c.getType()):
        {k: v
         for k, v in props(c).items()}
        for c in d.values()
    }


class _PropEncoder(json.JSONEncoder):
    def default(self, obj):
        if isinstance(obj, set):
            return sorted(obj)
        elif hasattr(obj, "toStringProperty"):
            return obj.toStringProperty()
        return obj


def read_options_pkl(filename):
    """Read a .pkl options file produced by gaudirun.py.

    A new process is started, which reads the options and passes them
    back via shared memory. This is unfortunately necessary as the side
    effects of loading a pickle with options occasionally prevents
    loading another one in the same process.

    """

    def worker(filename, ret):
        opts = _read_options_pkl_impl(filename)
        ret['data'] = json.dumps(opts, cls=_PropEncoder)

    with multiprocessing.Manager() as manager:
        ret = manager.dict()
        p = multiprocessing.Process(target=worker, args=(filename, ret))
        p.start()
        p.join()
        try:
            data = ret['data']
        except KeyError:
            raise RuntimeError('Could not open/parse ' + filename)
        return json.loads(data)


def read_options(filename):
    """Read and parse a Gaudi options file.

    Args:
        filename (str): Path to Gaudi options (.pkl, .py, or .opts).

    Returns:
        A dict containing the properties of all components.

    """
    try:
        from GaudiKernel.Configurable import (  # noqa
            DataHandle, PrivateToolHandle)
    except ImportError:  # backward compatibility with Gaudi < v34r0
        from GaudiKernel.Configurable import (  # noqa
            DataObjectHandleBase as DataHandle, PrivateToolHandle)
    if filename.endswith('.py'):
        with open(filename) as f:
            return eval(f.read())
    elif filename.endswith('.pkl'):
        with open(filename) as f:
            return read_options_pkl(filename)
    elif filename.endswith('.opts'):
        with open(filename) as f:
            import collections
            matches = re.finditer(r'^([^=]+)\.([^=.]+) = (.+);$', f.read(),
                                  re.MULTILINE)
            opts = collections.defaultdict(dict)
            for m in matches:
                value = m.group(3)
                if re.match(r'[0-9]+L', value):
                    value = value[:-1]
                opts[m.group(1)][m.group(2)] = eval(value)
            return opts
    else:
        raise RuntimeError('{} not supported!'.format(filename))


def color_diff_line(line, colored, old='red', new='green', meta='cyan'):
    if line.startswith('-'):
        return colored(line, old)
    elif line.startswith('+'):
        return colored(line, new)
    elif line.startswith(('@', '?')):
        return colored(line, meta)
    else:
        return line


def ndiff(old_formatted, new_formatted, old_name, new_name, color):
    """Diff formatted options with difflib.ndiff."""
    if color:
        from termcolor import colored
    else:
        colored = lambda x, *args, **kwargs: x

    out = []
    if old_name or new_name:
        out.append(
            colored(
                '--- {}\n+++ {}'.format(old_name, new_name), attrs=['bold']))

    for k in sorted(set(old_formatted) | set(new_formatted)):
        old = old_formatted.get(k, [])
        new = new_formatted.get(k, [])
        if old == new:
            continue
        out.append('@@ {} @@'.format(k))
        out.extend(
            color_diff_line(x, colored)
            for x in difflib.ndiff(old, new, None, None))
        out.append('')
    return '\n'.join(out)


def mkdir_p(path):
    try:
        os.makedirs(path)
    except OSError as exc:
        if exc.errno == errno.EEXIST and os.path.isdir(path):
            pass
        else:
            raise


def gitdiff(old_formatted, new_formatted, old_name, new_name, color):
    """Diff formatted options with git diff --no-index."""

    # create temporary directories
    tmpdir = tempfile.mkdtemp()
    try:
        old_dir = os.path.join(tmpdir, old_name)
        mkdir_p(old_dir)
        new_dir = os.path.join(tmpdir, new_name)
        mkdir_p(new_dir)

        # write out properties, one file per component
        for k, v in old_formatted.items():
            with open(os.path.join(old_dir, k), 'w') as f:
                f.write('\n'.join(v) + '\n')
        for k, v in new_formatted.items():
            with open(os.path.join(new_dir, k), 'w') as f:
                f.write('\n'.join(v) + '\n')

        cmd = [
            'git', 'diff', '--no-prefix', '--ignore-space-at-eol',
            '--color=' + ['never', 'always'][color], '--no-index', old_name,
            new_name
        ]
        p = subprocess.Popen(cmd, stdout=subprocess.PIPE, cwd=tmpdir)
        out = p.communicate()[0].decode('utf-8')
    finally:
        shutil.rmtree(tmpdir)
    return out


def diff_options(old_options,
                 new_options,
                 old_name='a',
                 new_name='b',
                 exclude=[],
                 method='git',
                 color=True):
    """Return the diff between two sets of Gaudi options.

    Takes two sets of Gaudi options as returned by `read_options` and
    computes a diff.

    Args:
        old_options (dict): The old Gaudi options.
        new_options (dict): The new Gaudi options.
        old_name (str): Name of the old options.
        new_name (str): Name of the new options.
        exclude (list): (component_regex, property_regex) tuples. If for
            any tuple both regexes match, the property is excluded.
        method (str): Method for computing the diff.
            One of "git" and "ndiff".
        color (str): Whether the output should be colorized.

    Returns:
        str: The entire diff.

    """
    exclude = [(re.compile(alg), re.compile(prop)) for alg, prop in exclude]

    def excluded(alg, prop):
        return any(
            ralg.match(alg) and rprop.match(prop) for ralg, rprop in exclude)

    def format_prop(name, value):
        s = json.dumps(
            {
                name: value
            },
            indent=4,
            sort_keys=True,
            cls=_PropEncoder,
            separators=(',', ': '),  # remove trailing whitespace for Python 2
        )
        return [line[4:] for line in s.splitlines()[1:-1]]

    def format_props(props):
        return sum((format_prop(name, props[name]) for name in sorted(props)),
                   [])

    def filter_props(component, props):
        if not exclude:
            return props
        return {k: v for k, v in props.items() if not excluded(component, k)}

    def filter_all_props(options):
        filtered = ((k, filter_props(k, v)) for k, v in options.items())
        return {k: v for k, v in filtered if v}

    def normalize_components(pkl_options, other_options):
        short_names = {}
        for k in pkl_options:
            m = re.match(r'^(.*) \(.*\)$', k)
            if not m:
                raise ValueError('unexpected name in pkl options')
            short_names[m.group(1)] = k
        assert len(short_names) == len(pkl_options)
        return {short_names.get(k, k): v for k, v in other_options.items()}

    try:
        new_options = normalize_components(old_options, new_options)
    except ValueError:
        try:
            old_options = normalize_components(new_options, old_options)
        except ValueError:
            pass

    old_formatted = {
        k: format_props(props)
        for k, props in filter_all_props(old_options).items()
    }
    new_formatted = {
        k: format_props(props)
        for k, props in filter_all_props(new_options).items()
    }

    if method == 'ndiff':
        return ndiff(
            old_formatted, new_formatted, old_name, new_name, color=color)
    elif method == 'git':
        return gitdiff(
            old_formatted, new_formatted, old_name, new_name, color=color)
    else:
        raise ValueError('method {} not recognized'.format(method))
