/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

/** @file RichMirrorSegPosition.h
 *
 *  Header file for utility class : Rich::MirrorSegPosition
 *
 *  @author Antonis Papanestis a.papanestis@rl.ac.uk
 *  @date   2004-06-30
 */

#pragma once

#include <cstdint>
#include <iostream>

namespace Rich {

  /** @class MirrorSegPosition RichDet/RichMirrorSegPosition.h
   *
   *  Helper class to pack together row/column information for the mirror
   *  segment position.
   *
   *  @author Antonis Papanestis a.papanestis@rl.ac.uk
   *  @date   2004-06-30
   */
  struct MirrorSegPosition {
    /// Type for dataword
    using DataType = std::uint8_t;
    /// Row number
    DataType row{0};
    /// Column number
    DataType column{0};
  };

} // namespace Rich

/// overloaded output to MsgStream
inline std::ostream& operator<<( std::ostream& os, const Rich::MirrorSegPosition& pos ) {
  return os << "Mirror Segment row:" << pos.row << " column:" << pos.column;
}
