/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

// local
#include "RichFutureDAQ/RichPDMDBEncodeMapping.h"

// RICH
#include "RichUtils/RichException.h"
#include "RichUtils/ToArray.h"
#include "RichUtils/ZipRange.h"

// Gaudi
#include "Gaudi/Algorithm.h"

// Messaging
#include "RichFutureUtils/RichMessaging.h"
#define debug( ... )                                                                                                   \
  if ( messenger() ) { ri_debug( __VA_ARGS__ ); }
#define verbo( ... )                                                                                                   \
  if ( messenger() ) { ri_verbo( __VA_ARGS__ ); }

// boost
#include "boost/container/static_vector.hpp"

// STL
#include <algorithm>
#include <vector>

using namespace Rich::Future::DAQ;
using namespace Rich::Detector;
using namespace Rich::DAQ;

void PDMDBEncodeMapping::checkVersion( const EncodingConds& C ) {

  // check status is (still) OK before continuing
  if ( !isInitialised() ) { return; }

  // mapping version
  boost::container::static_vector<int, 3> condVers;

  auto checkV = [&]( const auto& cond ) {
    // Access parameter, with default value 0 if missing
    condVers.push_back( condition_param<int>( cond, "MappingVersion", 0 ) );
  };

  // extract version from all conditions
  for ( const auto rich : Rich::detectors() ) { checkV( C.rTypeConds[rich] ); }
  checkV( C.hTypeCond );

  // Should all be the same for all panels.
  if ( !condVers.empty() && std::all_of( condVers.begin(), condVers.end(),
                                         [first = condVers.front()]( const auto& e ) { return e == first; } ) ) {
    m_mappingVer = condVers.front();
  } else {
    setIsInitialised( false );
    throw Rich::Exception( "Inconsistent mapping versions" );
  }
  debug( " -> Mapping Version ", m_mappingVer, endmsg );
}

void PDMDBEncodeMapping::fillRType( const EncodingConds& C ) {

  // check status is (still) OK before continuing
  if ( !isInitialised() ) { return; }

  // Loop over RICHes
  for ( const auto rich : Rich::detectors() ) {

    // load the condition
    const auto cond = C.rTypeConds[rich];

    // get the active ECs
    const auto active_ecs = condition_param<std::vector<int>>( cond, "ActiveECs" );
    debug( " -> Found ", active_ecs.size(), " active ECs", endmsg );

    // Loop over active ECs
    for ( const auto ec : active_ecs ) {

      // Load the active PMTs for this EC
      const auto ecS         = "EC" + std::to_string( ec ) + "_";
      const auto active_pmts = condition_param<std::vector<int>>( cond, ecS + "ActivePMTs" );
      debug( "  -> Loading ", active_pmts.size(), "ActivePMTs ", endmsg );

      // data shortcuts
      auto& rich_data = m_rTypeData.at( rich );
      auto& ec_data   = rich_data.at( ec );

      // loop over PMTs ..
      for ( const auto pmt : active_pmts ) {
        verbo( "   -> PMT ", pmt, endmsg );

        // Load the PMT data
        const std::string pmtS = ecS + "PMT" + std::to_string( pmt ) + "_";
        const auto        pdmdbs =
            Rich::toArray<PDMDBID::Type, NumAnodes>( condition_param<std::vector<int>>( cond, pmtS + "PDMDB" ) );
        const auto frames =
            Rich::toArray<PDMDBFrame::Type, NumAnodes>( condition_param<std::vector<int>>( cond, pmtS + "Frame" ) );
        const auto bits =
            Rich::toArray<FrameBitIndex::Type, NumAnodes>( condition_param<std::vector<int>>( cond, pmtS + "Bit" ) );

        // Fill data structure for each anode
        auto&         pmt_data = ec_data.at( pmt );
        std::uint16_t anode{0};
        for ( const auto&& [pdmdb, frame, bit] : Ranges::ConstZip( pdmdbs, frames, bits ) ) {
          auto& d = pmt_data.at( anode );
          assert( !d.isValid() ); // check not yet initialised
          d = AnodeData( PDMDBID( pdmdb ), PDMDBFrame( frame ), FrameBitIndex( bit ) );
          verbo( "    -> Anode ", anode, " ", d, endmsg );
          ++anode;
        } // conditions data loop

      } // active PMT loop

    } // ECs loop

  } // RICH loop
}

void PDMDBEncodeMapping::fillHType( const EncodingConds& C ) {

  // check status is (still) OK before continuing
  if ( !isInitialised() ) { return; }

  // Load the H Type encoding condition for this RICH
  const auto cond = C.hTypeCond;

  // get the active ECs
  const auto active_ecs = condition_param<std::vector<int>>( cond, "ActiveECs" );
  debug( " -> Found ", active_ecs.size(), " active ECs", endmsg );

  // Loop over active ECs
  for ( const auto ec : active_ecs ) {

    // Load the active PMTs for this EC
    const auto ecS         = "EC" + std::to_string( ec ) + "_";
    const auto active_pmts = condition_param<std::vector<int>>( cond, ecS + "ActivePMTs" );
    debug( "  -> Loading ", active_pmts.size(), "ActivePMTs ", endmsg );

    // data shortcuts
    auto& ec_data = m_hTypeData.at( ec );

    // loop over PMTs ..
    for ( const auto pmt : active_pmts ) {
      verbo( "   -> PMT ", pmt, endmsg );

      // Load the PMT data
      const std::string pmtS = ecS + "PMT" + std::to_string( pmt ) + "_";
      const auto        pdmdbs =
          Rich::toArray<PDMDBID::Type, NumAnodes>( condition_param<std::vector<int>>( cond, pmtS + "PDMDB" ) );
      const auto frames =
          Rich::toArray<PDMDBFrame::Type, NumAnodes>( condition_param<std::vector<int>>( cond, pmtS + "Frame" ) );
      const auto bits =
          Rich::toArray<FrameBitIndex::Type, NumAnodes>( condition_param<std::vector<int>>( cond, pmtS + "Bit" ) );

      // Fill data structure for each anode
      auto&         pmt_data = ec_data.at( pmt );
      std::uint16_t anode{0};
      for ( const auto&& [pdmdb, frame, bit] : Ranges::ConstZip( pdmdbs, frames, bits ) ) {
        auto& d = pmt_data.at( anode );
        assert( !d.isValid() ); // check not yet initialised
        d = AnodeData( PDMDBID( pdmdb ), PDMDBFrame( frame ), FrameBitIndex( bit ) );
        verbo( "    -> Anode ", anode, " ", d, endmsg );
        ++anode;
      } // conditions data loop

    } // active PMT loop

  } // ECs loop
}
