###############################################################################
# (c) Copyright 2000-2021 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
#[=======================================================================[.rst:
Rich/RichDetectors
------------------
#]=======================================================================]

gaudi_add_library(RichDetectorsLib
    SOURCES
        src/lib/Rich1.cpp
        src/lib/Rich2.cpp
        src/lib/RichX.cpp
        src/lib/RichPDInfo.cpp
        src/lib/RichPDPanel.cpp
    LINK
        PUBLIC
            LHCb::DetDescLib
            LHCb::LHCbKernel
            LHCb::LHCbMathLib
            LHCb::RichDetLib
            LHCb::RichUtils
            nlohmann_json::nlohmann_json
)

if(USE_DD4HEP)
    target_link_libraries(RichDetectorsLib PUBLIC Detector::DetectorLib)
endif()

gaudi_add_module(RichDetectors
    SOURCES
        src/component/RichDetectorTests.cpp
    LINK
        Boost::headers
        Gaudi::GaudiKernel
        LHCb::DetDescLib
        LHCb::LHCbAlgsLib
        LHCb::LHCbKernel
        LHCb::RichDetLib
        LHCb::RichFutureDAQLib
        LHCb::RichFutureKernel
        LHCb::RichUtils
        LHCb::RichDetectorsLib
)

gaudi_add_tests(QMTest)

if(NOT USE_DD4HEP)
    set_property(
        TEST
            RichDetectors.test-run-change-2022-data
        PROPERTY
            DISABLED TRUE
    )
endif()
