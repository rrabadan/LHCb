/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#pragma once

#include <cassert>
#include <optional>
#include <ostream>
#include <string>
#include <type_traits>
#include <utility>

#include "DetDesc/DetectorElement.h"

class ParamValidDataObject;

namespace Rich::Detector {

  //-----------------------------------------------------------------------------
  /** @class Handle handle.h
   *
   *  Rich helper class encapsulating differences between DetDesc and DD4HEP
   *
   *  @author Chris Jones
   *  @date   2020-10-05
   */
  //-----------------------------------------------------------------------------

  template <typename T>
  class Handle {

  private:
#ifdef USE_DD4HEP
    using H = std::optional<T>;
#else
    using H = const T*;
#endif
    // the handle
    H h;

  public:
    /// DD4HEP needs default constructor :(
    Handle() = default;
    /// Constructor from object
    Handle( const T& t ) {
      // internal handle type
#ifdef USE_DD4HEP
      h = t;
#else
      h = &t;
#endif
    }
    /// Access to handled object
    const T* access() const noexcept {
#ifdef USE_DD4HEP
      return ( h.has_value() ? &( h.value() ) : nullptr );
#else
      return h;
#endif
    }
    /// Overload arrow operator
    const T* operator->() const noexcept { return access(); }
    /// print to ostream
    friend inline std::ostream& operator<<( std::ostream& s, const Handle<T>& h ) { return s << h.access(); }
  };

} // namespace Rich::Detector
