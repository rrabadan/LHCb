/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#pragma once

// Local
#include "RichDetectors/Rich1Gas.h"
#include "RichDetectors/RichX.h"

// detector element
#ifdef USE_DD4HEP
#  include "Detector/Rich1/DetElemAccess/DeRich1.h"
#else
#  include "RichDet/DeRich1.h"
#endif

namespace Rich::Detector {

#ifdef USE_DD4HEP
  using Rich1 = details::RichX<Rich::Rich1, LHCb::Detector::DeRich1, LHCb::Detector::DeRich, Rich::Detector::Rich1Gas>;
#else
  using Rich1 = details::RichX<Rich::Rich1, DeRich1, DeRich, Rich::Detector::Rich1Gas>;
#endif

} // namespace Rich::Detector
