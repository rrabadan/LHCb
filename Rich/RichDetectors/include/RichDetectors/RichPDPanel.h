/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#pragma once

// Vc
#include <Vc/vector>

// Gaudi
#include "GaudiKernel/Plane3DTypes.h"
#include "GaudiKernel/Transform3DTypes.h"

// LHCbKernel
#include "Kernel/RichSide.h"
#include "Kernel/RichSmartID.h"

// LHCbMath
#include "LHCbMath/FastMaths.h"
#include "LHCbMath/SIMDTypes.h"

// Local
#include "RichDetectors/RichPD.h"
#include "RichDetectors/Utilities.h"

// Detector description
#ifdef USE_DD4HEP
#  include "Detector/Rich/DeRichPhDetPanel.h"
#else
#  include "RichDet/DeRichPDPanel.h"
#endif

// RichUtils
#include "RichUtils/RichDAQDefinitions.h"
#include "RichUtils/RichSIMDRayTracing.h"

// STL
#include <algorithm>
#include <array>
#include <cassert>
#include <cmath>
#include <cstdint>
#include <limits>
#include <memory>
#include <ostream>
#include <tuple>
#include <type_traits>

namespace Rich::Detector {

  //-----------------------------------------------------------------------------
  /** @class RichPDPanel.h
   *
   *  RICH PD panel class
   *
   *  @author Chris Jones
   *  @date   2020-10-05
   */
  //-----------------------------------------------------------------------------

  class alignas( LHCb::SIMD::VectorAlignment ) PDPanel final
      : public LHCb::SIMD::AlignedBase<LHCb::SIMD::VectorAlignment> {

  public:
    // types

    /// The underlying detector description object (DetDesc or DD4HEP)
#ifdef USE_DD4HEP
    using DetElem = LHCb::Detector::detail::DeRichPhDetPanelObject;
#else
    using DetElem = DeRichPDPanel;
#endif

    // inherit some types from PD

    using SIMDRayTResult = PD::SIMDRayTResult;
    using FP             = PD::FP;
    using SIMDFP         = PD::SIMDFP;
    using SIMDINT32      = PD::SIMDINT32;
    using SIMDPoint      = PD::SIMDPoint;
    using SIMDVector     = PD::SIMDVector;
    using SIMDPDs        = PD::SIMDPDs;
    using SIMDSmartIDs   = PD::SIMDSmartIDs;
    using XYArraySIMD    = std::array<SIMDFP, 2>;

    /// Return types for ray-tracing methods
    using RayTStruct = std::tuple<Gaudi::XYZPoint, LHCb::RichSmartID, const PD*, LHCb::RichTraceMode::RayTraceResult>;
    using RayTStructSIMD = std::tuple<SIMDPoint, SIMDSmartIDs, SIMDPDs, SIMDRayTResult::Results>;

  private:
    // types

    // types for PD lookup storage
    using PDsArray    = std::array<std::unique_ptr<const PD>, LHCb::RichSmartID::MaPMT::MaxPDsPerModule>;
    using ModuleArray = std::array<PDsArray, LHCb::RichSmartID::MaPMT::MaxModulesPerPanel>;

  private:
    /// Get access to the underlying object
    inline const DetElem* get() const noexcept { return m_panel; }

    /// Panel local module number
    inline auto panelLocalModNum( const LHCb::RichSmartID pdID ) const noexcept {
      // for now retain our own offset to support classic PMTs
      // in future can just use RichSmartID::panelLocalModuleNum()
      return ( pdID.pdMod() - m_modNumOffset );
    }

    /// Gets the intersection with the panel (SIMD) in global panel coordinates
    inline SIMDPoint getPanelInterSection( const SIMDPoint&  pGlobal, //
                                           const SIMDVector& vGlobal ) const noexcept {

      // find the intersection with the detection plane
      const auto scalar = vGlobal.Dot( m_detectionPlaneNormalSIMD );

      // return panel intersection point
      const auto distance = m_detectionPlaneSIMD.Distance( pGlobal ) / scalar;
      return ( pGlobal - ( distance * vGlobal ) );
    }

  public:
    // accessors

    /// Access all owned PD Modules
    const auto& pdModules() const noexcept { return m_PDs; }

    /// Access the RICH detector type
    inline auto rich() const noexcept { return m_rich; }

    /// Access the PD panel side
    inline auto side() const noexcept { return m_side; }

    /// Access the Smart ID for this panel
    inline auto panelSmartID() const noexcept { return m_panelID; }

    /// Get the PD for given ID
    auto dePD( const LHCb::RichSmartID pdID ) const
#ifdef NDEBUG
        noexcept
#endif
    {
      const PD* dePD = nullptr;
      if ( pdID.pdIsSet() ) {
        const auto localModNum = panelLocalModNum( pdID );
        // Note, whilst comissioning dd4hep, but still using old DetDesc MC samples with
        // different PMTs, we cannot apply asserts here by instead must using runtime size checks.
        // to be reverted once dd4hep is fully comissioned and DetDesc completely dropped.
        // assert( localModNum < m_PDs.size() );
        if ( localModNum < pdModules().size() ) {
          const auto& mod = pdModules()[localModNum];
          // assert( pdID.pdNumInMod() < mod.size() );
          if ( pdID.pdNumInMod() < mod.size() ) {
            const auto& pd = mod[pdID.pdNumInMod()];
            /** As long as we need to support 'classic' PMTs cannot apply this assert.
             *  @todo put assert back once old DetDesc is dropped. */
            // assert( pd.get() ); // should always have an object
            dePD = pd.get();
          }
        }
      }
      // assert( dePD ); // cannot do this as long as 'classic' PMT support is required (see above).
      return dePD;
    }

    /// Check if a given PD ID is a 'large' H-Type PMT
    inline bool isLargePD( const LHCb::RichSmartID pdID ) const noexcept {
      const auto pd = dePD( pdID );
      return ( pd ? pd->isHType() : false );
    }

    /// Access the local to global transform
    inline const auto& pdPanelToGlobal() const noexcept { return m_pdPanelToGloM; }

    /// Access the global to local transform
    inline const auto& globalToPDPanel() const noexcept { return m_gloToPDPanelM; }

    /// Access the global to local transform (SIMD)
    inline const auto& globalToPDPanelSIMD() const noexcept { return m_gloToPDPanelMSIMD; }

    /// Access the global to local transform
    inline const auto& globalToLocal() const noexcept { return m_gloToLocM; }

    /// Access the local to global transform
    inline const auto& localToGlobal() const noexcept { return m_locToGloM; }

    /// Access the global to local transform (SIMD)
    inline const auto& globalToLocalSIMD() const noexcept { return m_gloToLocMSIMD; }

    /// Fill the given RichSmartID for the given point
#ifdef USE_DD4HEP
    inline bool smartID( const Gaudi::XYZPoint&, LHCb::RichSmartID& ) const {
      // TODO: Implement for DD4HEP (e.g. if needed by Boole)
      throw LHCb::Detector::detail::RichNotImplemented( "PDPanel::smartID" );
    }
#else
    inline auto smartID( const Gaudi::XYZPoint& globalPoint, LHCb::RichSmartID& id ) const noexcept {
      return get()->smartID( globalPoint, id );
    }
#endif

    /// Compute the detection point for a given RichSmartID
    inline auto detectionPoint( const LHCb::RichSmartID id ) const noexcept {
      const auto pd = dePD( id );
      return ( pd ? pd->detectionPoint( id ) : PD::DetPtn{} );
    }

    /// Check if a given (SIMD) point is in the acceptance of the panel
    inline auto isInAcceptance( const SIMDPoint& aPointInPanel ) const noexcept {
      return ( abs( aPointInPanel.x() ) < m_xyHalfSizeSIMD[0] && //
               abs( aPointInPanel.y() ) < m_xyHalfSizeSIMD[1] );
    }

    /// Shift of panel plane in z
    inline auto zShift() const noexcept { return m_zShift; }

  public:
    // constructors

#ifdef USE_DD4HEP
    /// Templated constructor for dd4hep types
    /// For now also take DetDesc object as well
    template <typename PANEL>
    PDPanel( const Rich::DetectorType rich, //
             const Rich::Side         side, //
             const PANEL&             panel )
        : m_rich( rich ) //
        , m_side( side ) //
        , m_panelID( rich, side, LHCb::RichSmartID::MaPMTID )
        , m_panel( panel.access() )
        , m_gloToLocM( panel.toGlobalMatrix().Inverse() )
        , m_locToGloM( panel.toGlobalMatrix() ) {

      using namespace LHCb::Detector::detail;
      using GP = Gaudi::XYZPoint;

      // panel shift in Z
      init_param( m_zShift, ( Rich::Rich1 == m_rich ? "Rich1PmtDetPlaneZInPmtPanel" : "Rich2PmtDetPlaneZInPmtPanel" ) );

      // Form the detection plane Plane3D object, from three points in its plane.
      {
        const Gaudi::Plane3D p( localToGlobal() * GP{0, 0, m_zShift},   //
                                localToGlobal() * GP{0, 100, m_zShift}, //
                                localToGlobal() * GP{50, 50, m_zShift} );
        using PLANE                = decltype( m_detectionPlaneSIMD );
        m_detectionPlaneSIMD       = PLANE( SIMDFP( p.A() ), SIMDFP( p.B() ), SIMDFP( p.C() ), SIMDFP( p.D() ) );
        m_detectionPlaneNormalSIMD = m_detectionPlaneSIMD.Normal();
      }

      // global <-> local transform with additional offset translation for each panel
      // Note logic here is cloned from DeRichPMTPanel class in RichDet
      {
        const auto aPon = localToGlobal() * GP{0, 0, m_zShift};
        const auto sign = ( ( m_rich == Rich::Rich1 && m_side == Rich::top ) || //
                                    ( m_rich == Rich::Rich2 && m_side == Rich::left )
                                ? 1
                                : -1 );
        const auto panelSizes =
            ( m_rich == Rich::Rich1                                                                         // DetDesc
                  ? std::array{FP( 2 ) * fabs( dd4hep_param<double>( "Rh1PMTModulePlaneXEdgeTop" ) ),       // 1344.0
                               FP( 2 ) * fabs( dd4hep_param<double>( "Rh1PMTModulePlaneYEdgeTop" ) )}       // 621.5
                  : std::array{FP( 2 ) * fabs( dd4hep_param<double>( "Rh2MixedPMTModulePlaneXEdgeLeft" ) ), // 678.0
                               FP( 2 ) * fabs( dd4hep_param<double>( "Rh2MixedPMTModulePlaneYEdgeLeft" ) )} // 1344.0
            );
        const auto aOffset   = fabs( FP( 0.5 ) * panelSizes[Rich::Rich1 == m_rich ? 1 : 0] );
        const auto detPlaneZ = m_zShift;
        const auto localTranslation =
            ( m_rich == Rich::Rich1 ? ROOT::Math::Translation3D( aPon.x(), sign * aOffset, -detPlaneZ )
                                    : ROOT::Math::Translation3D( sign * aOffset, aPon.y(), -detPlaneZ ) );
        m_gloToPDPanelM = localTranslation * globalToLocal();
        m_pdPanelToGloM = m_gloToPDPanelM.Inverse();

        // Panel half x/y sizes
        m_xyHalfSizeSIMD[0] = fabs( FP( 0.5 ) * panelSizes[0] );
        m_xyHalfSizeSIMD[1] = fabs( FP( 0.5 ) * panelSizes[1] );
      }

      // create SIMD transforms
      toSIMDTrans( m_gloToLocM, m_gloToLocMSIMD );
      toSIMDTrans( m_gloToPDPanelM, m_gloToPDPanelMSIMD );

      using UINT = decltype( m_modNumOffset );
      UINT maxlocalModNum{0}, maxPDNumInMod{0};
      m_modNumOffset = std::numeric_limits<UINT>::max();

      // compute the SmartID for a given PMT
      auto getSmartID = [&rich, &side]( const auto& pmt ) {
        // PMT number in module
        const LHCb::RichSmartID::DataType nInMod = pmt.numInModule();
        // local module number
        const LHCb::RichSmartID::DataType locModN = pmt.panelLocalModuleN();
        // global module number
        const auto gloModN = LHCb::RichSmartID::MaPMT::panelLocalToGlobalModN( rich, side, locModN );
        assert( gloModN == ( LHCb::RichSmartID::DataType )( pmt.moduleCopyNum() ) );
        // build the PMT ID
        // FIXME : Note we are forcing the type to PMTs below. At some point this will need fixing.
        const LHCb::RichSmartID id( rich, side, nInMod, gloModN, LHCb::RichSmartID::MaPMTID, pmt.isHType() );
        assert( id.rich() == rich );
        assert( id.panel() == side );
        assert( id.pdMod() == gloModN );
        assert( id.panelLocalModuleNum() == locModN );
        assert( id.pdNumInMod() == nInMod );
        return id;
      };

      // pre-loop over IDs to determine min/max module numbers for this panel
      auto checkNumbers = [&]( const auto& pmts ) {
        for ( const auto& pmt : pmts ) {
          const auto id     = getSmartID( pmt );
          const auto modNum = id.pdMod();
          const auto pdNum  = id.pdNumInMod();
          if ( modNum > maxlocalModNum ) { maxlocalModNum = modNum; }
          if ( modNum < m_modNumOffset ) { m_modNumOffset = modNum; }
          if ( pdNum > maxPDNumInMod ) { maxPDNumInMod = pdNum; }
        }
      };

      // create owned PMTs
      auto createPDs = [&]( const auto& pmts ) {
        for ( const auto& pmt : pmts ) {
          const auto id          = getSmartID( pmt );
          const auto localModNum = panelLocalModNum( id );
          assert( localModNum < m_PDs.size() );
          auto& mod = m_PDs[localModNum];
          assert( id.pdNumInMod() < mod.size() );
          auto& pd = mod[id.pdNumInMod()];
          assert( nullptr == pd.get() ); // should not be set yet
          pd = std::make_unique<PD>( *this, id, pmt );
        }
      };

      // Load DD4HEP PMTs
      const auto r_pmts = panel.allPMTsR();
      const auto h_pmts = panel.allPMTsH();

      // run pre-check on ranges
      checkNumbers( r_pmts );
      checkNumbers( h_pmts );
      assert( ( maxlocalModNum - m_modNumOffset ) < m_PDs.size() );
      assert( maxPDNumInMod < m_PDs[0].size() );

      // create the owned PD instances
      createPDs( r_pmts );
      createPDs( h_pmts );

      // Initialise the PD finder
      m_pdFinder = std::make_unique<PDFinder<double>>( *this );
    }
#else
    /// DetDesc Constructor
    PDPanel( const DetElem& panel )
        : m_rich( panel.rich() )
        , m_side( panel.side() )
        , m_panelID( panel.rich(), panel.side(), LHCb::RichSmartID::MaPMTID )
        , m_panel( &panel )
        , m_pdPanelToGloM( panel.PDPanelToGlobalMatrix() )
        , m_gloToPDPanelM( panel.PDPanelToGlobalMatrix().Inverse() )
        , m_gloToLocM( panel.geometryPlus()->toLocalMatrix() )
        , m_locToGloM( panel.geometryPlus()->toLocalMatrix().Inverse() )
        , m_gloToPDPanelMSIMD( panel.globalToPDPanelMatrixSIMD() )
        , m_detectionPlaneSIMD( panel.detectionPlaneSIMD() )
        , m_detectionPlaneNormalSIMD( panel.detectionPlaneNormalSIMD() ) {

      // Panel shift in z
      m_zShift = panel.deRich()->param<double>( Rich::Rich1 == m_rich //
                                                    ? "Rich1PmtDetPlaneZInPmtPanel"
                                                    : "Rich2PmtDetPlaneZInPmtPanel" );

      // create SIMD transforms
      toSIMDTrans( m_gloToLocM, m_gloToLocMSIMD );

      // Panel half x/y sizes
      const auto panelSize = panel.deRich()->param<std::vector<double>>( Rich::Rich1 == m_rich //
                                                                             ? "Rich1PMTModulePlaneHalfSize"
                                                                             : "Rich2MixedPMTModulePlaneHalfSize" );
      m_xyHalfSizeSIMD[0]  = fabs( panelSize[0] );
      m_xyHalfSizeSIMD[1]  = fabs( panelSize[1] );

      // pre-loop over IDs to determine min/max module numbers for this panel
      // note, doing this here rather than relying on RichSmartID::panelLocalModuleNum()
      // as that does not work correctly for 'classic' PMTs. Once support for this
      // older scheme is no londer needed we remove the local offsets here.
      using UINT = decltype( m_modNumOffset );
      UINT maxlocalModNum{0}, maxPDNumInMod{0};
      m_modNumOffset = std::numeric_limits<UINT>::max();
      for ( const auto dePD : panel.dePDs() ) {
        const auto id     = dePD->pdSmartID();
        const auto modNum = id.pdMod();
        const auto pdNum  = id.pdNumInMod();
        if ( modNum > maxlocalModNum ) { maxlocalModNum = modNum; }
        if ( modNum < m_modNumOffset ) { m_modNumOffset = modNum; }
        if ( pdNum > maxPDNumInMod ) { maxPDNumInMod = pdNum; }
      }

      // sanity checks
      assert( ( maxlocalModNum - m_modNumOffset ) < m_PDs.size() );
      assert( maxPDNumInMod < m_PDs[0].size() );

      // access the PDs and make owned wrapped objects
      for ( const auto dePD : panel.dePDs() ) {
        const auto id          = dePD->pdSmartID();
        const auto localModNum = panelLocalModNum( id );
        assert( localModNum < m_PDs.size() );
        auto& mod = m_PDs[localModNum];
        assert( id.pdNumInMod() < mod.size() );
        auto& pd = mod[id.pdNumInMod()];
        assert( nullptr == pd.get() ); // should not be set yet
        pd = std::make_unique<PD>( *this, *dePD );
      }

      // Initialise the PD finder (if not classic PMTs)
      if ( !panel.isClassic() ) { m_pdFinder = std::make_unique<PDFinder<double>>( *this ); }
    }
#endif

  public:
    // NEW ray tracing - implemented locally

    // ray trace to PD detectors (SIMD)
    RayTStructSIMD detIntersectSIMD( const SIMDPoint&          pGlobal, //
                                     const SIMDVector&         vGlobal, //
                                     const LHCb::RichTraceMode mode ) const;

    // ray trace to PD detectors (scalar)
    RayTStruct detIntersect( const Gaudi::XYZPoint&    pGlobal, //
                             const Gaudi::XYZVector&   vGlobal, //
                             const LHCb::RichTraceMode mode ) const;

  private:
    /// formatted printout
    template <typename STREAM>
    STREAM& fillStream( STREAM& s ) const {
      auto mend = []() {
        if constexpr ( std::is_same_v<MsgStream, STREAM> ) {
          return endmsg;
        } else {
          return "\n";
        }
      };
      s << "[ " << rich() << " " << Rich::text( rich(), side() )                  //
        << " NumPDModules=" << pdModules().size()                                 //
        << " ModNumOffset=" << m_modNumOffset                                     //
        << " ZShift=" << zShift()                                                 //
        << " CentrePtnInGlo=" << ( pdPanelToGlobal() * Gaudi::XYZPoint{0, 0, 0} ) //
        << " (x,y)HalfSizes=+-(" << m_xyHalfSizeSIMD[0][0] << "," << m_xyHalfSizeSIMD[1][0] << ")";
      const auto& p = m_detectionPlaneSIMD;
      FP          a{p.A()[0]}, b{p.B()[0]}, c{p.C()[0]}, d{p.D()[0]};
      if ( fabs( a ) < 1e-12 ) { a = 0.0; } // Prevent machine precision diffs in output ...
      s << "  DetectionPlane=[ " << a << " " << b << " " << c << " " << d << " ]" << mend();
      s << "  PDPanelToGlo=" << mend();
      printTransform( s, pdPanelToGlobal() );
      s << mend() << "  GloToPDPanel=" << mend();
      printTransform( s, globalToPDPanel() );
      s << mend() << "  GloToLoc=" << mend();
      printTransform( s, globalToLocal() );
      s << mend() << "  LocToGlo=" << mend();
      printTransform( s, localToGlobal() );
      return s;
    }

  public:
    /// Overload MsgStream operator
    friend inline auto& operator<<( MsgStream& s, const PDPanel& p ) { return p.fillStream( s ); }
    /// Overload ostream operator
    friend inline auto& operator<<( std::ostream& s, const PDPanel& p ) { return p.fillStream( s ); }

  private:
    /// PD Finder
    template <typename FPTYPE>
    class PDFinder final : public LHCb::SIMD::AlignedBase<LHCb::SIMD::VectorAlignment> {

    private:
      /// Type for PD Number
      using PDNum = std::uint16_t;
      /// Type for Scalar index
      using ScalarIndex = std::uint32_t;
      /// Type for SIMD array of indices
      using SIMDIndices = LHCb::SIMD::INT<ScalarIndex>;
      /// SIMD Type for array of PDs
      template <typename TYPE>
      using SIMDPDs = SIMD::STDArray<const PD*, TYPE>;
      /// Type for lookup storage
      using PDArray = Vc::vector<const PD*>;

    private:
      /// The lookup table
      class LookupTable final : public LHCb::SIMD::AlignedBase<LHCb::SIMD::VectorAlignment>, private PDArray {

      private:
        /// Number of X bins
        PDNum m_nXbins{0};
        /// Number of y bins
        PDNum m_nYbins{0};

      public:
        /// Number of X bins
        inline auto nXBins() const noexcept { return m_nXbins; }
        /// Number of Y bins
        inline auto nYBins() const noexcept { return m_nYbins; }
        /// Number of X bins
        inline auto nXBinsSIMD() const noexcept { return SIMDIndices::IndexType( nXBins() ); }
        /// Number of Y bins
        inline auto nYBinsSIMD() const noexcept { return SIMDIndices::IndexType( nYBins() ); }

      public:
        /// Constructor
        LookupTable( const PDNum nX, const PDNum nY ) : m_nXbins( nX ), m_nYbins( nY ) { clear(); }
        /// Default constructor
        LookupTable() = delete;

      public:
        /// Combine two (scalar) x,y indices in a single one
        [[nodiscard]] ScalarIndex xyIndex( const ScalarIndex ix, //
                                           const ScalarIndex iy ) const noexcept {
          return ( nYBins() * ix ) + iy;
        }
        /// Combine two (SIMD) x,y indices in a single one
        [[nodiscard]] typename SIMDIndices::IndexType       //
        xyIndex( const typename SIMDIndices::IndexType& ix, //
                 const typename SIMDIndices::IndexType& iy ) const noexcept {
          return ( nYBinsSIMD() * ix ) + iy;
        }
        /// Access the PD for a given combined xy index (Scalar)
        [[nodiscard]] const PD* get( const ScalarIndex ixy ) const noexcept { return ( *this )[ixy]; }
        /// Access the PD for a given set of (x,y) indices (Scalar)
        [[nodiscard]] const PD* get( const ScalarIndex ix, //
                                     const ScalarIndex iy ) const noexcept {
          return get( xyIndex( ix, iy ) );
        }

      public:
        /// Set the PD for a given bin
        void set( const ScalarIndex ix, //
                  const ScalarIndex iy, //
                  const PD*         pd ) noexcept {
          ( *this )[xyIndex( ix, iy )] = pd;
        }
        /// Clear the table
        void clear() noexcept {
          PDArray::clear();
          resize( nXBins() * nYBins(), 0 );
        }
      };

    private:
      // SIMD cached values

      ///< Minimum X (SIMD)
      alignas( LHCb::SIMD::VectorAlignment ) SIMDFP m_minXSIMD = SIMDFP::Zero();
      ///< Maximum X (SIMD)
      alignas( LHCb::SIMD::VectorAlignment ) SIMDFP m_maxXSIMD = SIMDFP::Zero();
      ///< Minimum Y (SIMD)
      alignas( LHCb::SIMD::VectorAlignment ) SIMDFP m_minYSIMD = SIMDFP::Zero();
      ///< Maximum Y (SIMD)
      alignas( LHCb::SIMD::VectorAlignment ) SIMDFP m_maxYSIMD = SIMDFP::Zero();
      /// 1 / X increment
      alignas( LHCb::SIMD::VectorAlignment ) SIMDFP m_incXSIMD = SIMDFP::Zero();
      /// 1 / Y increment
      alignas( LHCb::SIMD::VectorAlignment ) SIMDFP m_incYSIMD = SIMDFP::Zero();

    private:
      FPTYPE                       m_minX{9e9};   ///< Minimum X
      FPTYPE                       m_maxX{-9e9};  ///< Maximum X
      FPTYPE                       m_minY{9e9};   ///< Minimum Y
      FPTYPE                       m_maxY{-9e9};  ///< Maximum Y
      FPTYPE                       m_incX{0};     ///< 1 / Increment in X
      FPTYPE                       m_incY{0};     ///< 1 / Increment in Y
      std::unique_ptr<LookupTable> m_lookupTable; ///< The lookup table

    public:
      /// Default constructor
      PDFinder() = default;

      /// Constructor from a panel
      PDFinder( const PDPanel& panel ) { initialise( panel ); }

    private:
      /// get min X
      inline auto minX() const noexcept { return m_minX; }
      /// get max X
      inline auto maxX() const noexcept { return m_maxX; }
      /// get min X
      inline auto minY() const noexcept { return m_minY; }
      /// get max X
      inline auto maxY() const noexcept { return m_maxY; }
      /// # X bins
      inline auto nXBins() const noexcept { return m_lookupTable->nXBins(); }
      /// # X bins
      inline auto nYBins() const noexcept { return m_lookupTable->nYBins(); }
      /// Get x for a given index value
      [[nodiscard]] FPTYPE binX( const ScalarIndex i ) const noexcept {
        return m_minX + ( ( (FPTYPE)i + FPTYPE( 0.5f ) ) / m_incX );
      }
      /// Get y for a given index value
      [[nodiscard]] FPTYPE binY( const ScalarIndex i ) const noexcept {
        return m_minY + ( ( (FPTYPE)i + FPTYPE( 0.5f ) ) / m_incY );
      }
      /// Get the x index
      template <typename TYPE>
      inline auto xIndex( const TYPE& x ) const noexcept {
        if constexpr ( std::is_arithmetic<TYPE>::value ) {
          // scalar
          return ( ScalarIndex )( x < minX() ? 0 : x > maxX() ? nXBins() - 1 : ( ( x - minX() ) * m_incX ) );
        } else {
          // SIMD
          // mask for x < min value
          const auto mask = x < m_minXSIMD;
          // form indices
          auto xtmp = ( x - m_minXSIMD ) * m_incXSIMD;
          // Underflow protection
          xtmp.setZero( mask );
          auto xi = LHCb::SIMD::simd_cast<SIMDIndices::IndexType>( xtmp );
          // Overflow protection
          xi( xi >= m_lookupTable->nXBinsSIMD() ) = SIMDIndices::IndexType( nXBins() - 1 );
          // return
          return xi;
        }
      }
      /// Get the y index
      template <typename TYPE>
      inline auto yIndex( const TYPE& y ) const noexcept {
        if constexpr ( std::is_arithmetic<TYPE>::value ) {
          // scalar
          return ( ScalarIndex )( y < minY() ? 0 : y > maxY() ? nYBins() - 1 : ( ( y - minY() ) * m_incY ) );
        } else {
          // SIMD
          // mask for y < min value
          const auto mask = y < m_minYSIMD;
          // form indices
          auto ytmp = ( y - m_minYSIMD ) * m_incYSIMD;
          // Underflow protection
          ytmp.setZero( mask );
          auto yi = LHCb::SIMD::simd_cast<SIMDIndices::IndexType>( ytmp );
          // Overflow protection
          yi( yi >= m_lookupTable->nYBinsSIMD() ) = SIMDIndices::IndexType( nYBins() - 1 );
          // return
          return yi;
        }
      }
      /// Get the position to use for a PD in the lookup table
      inline auto getPosition( const PDPanel&, const PD& pd ) const noexcept {
        const auto& ptn = pd.centrePointPanel();
        // eventually might want to transform this to be a point in the same
        // plane used by the panel intersection in the global frame
        return ptn;
      }
      /// Returns the 2D (x,y) distance^2 between a given point and PD centre
      inline auto distance2( const FPTYPE   x,     //
                             const FPTYPE   y,     //
                             const PDPanel& panel, //
                             const PD&      pd ) const noexcept {
        const auto ptn = getPosition( panel, pd );
        return ( std::pow( x - ptn.X(), 2 ) + //
                 std::pow( y - ptn.Y(), 2 ) );
      }
      /// Get the PD closest to a given (x,y) point
      [[nodiscard]] auto closestXY( const PDPanel& panel, //
                                    const FPTYPE x, const FPTYPE y ) const noexcept {
        const PD* minPD = nullptr;
        // Loop over all the PDs to find the closest
        FPTYPE      minDist2( 9e30 );
        const auto& mods = panel.pdModules();
        for ( const auto& mod : mods ) {
          for ( const auto& pd : mod ) {
            if ( !pd.get() ) { continue; }
            const auto dist2 = distance2( x, y, panel, *pd );
            if ( dist2 < minDist2 ) {
              minPD    = pd.get();
              minDist2 = dist2;
            }
          }
        }
        // can only use this assert once we sort out how to deal with known
        // missing PDs that then correctly generate a larger shift in the lookup table
        // as the bins for those PDs get assigned a neighbouring one.
        // assert( std::sqrt( minDist2 ) < ( minPD->isHType() ? 20 : 0.15 ) );
        return minPD;
      }

    public:
      /// Initialise this object from a list of PD objects
      void initialise( const PDPanel& panel ) {

        // Minimum diff in X/Y between PDs, same module
        FPTYPE minXdiff_N{9e20}, minYdiff_N{9e20};
        FPTYPE minXdiff_S{9e20}, minYdiff_S{9e20};

        // loop over all PDs and deduce boundary constants etc.
        const auto& mods = panel.pdModules();
        for ( const auto& mod : mods ) {
          for ( const auto& pdA : mod ) {
            if ( !pdA.get() ) { continue; }

            // Get the PD position
            const auto ptnA = getPosition( panel, *pdA );

            // Get the min/max X/Y centre of PDs in the panel
            if ( ptnA.X() < m_minX ) { m_minX = ptnA.X(); }
            if ( ptnA.X() > m_maxX ) { m_maxX = ptnA.X(); }
            if ( ptnA.Y() < m_minY ) { m_minY = ptnA.Y(); }
            if ( ptnA.Y() > m_maxY ) { m_maxY = ptnA.Y(); }

            // Brute force second loop over PDs to deduce the separations between them
            for ( const auto& mod : mods ) {
              for ( const auto& pdB : mod ) {
                if ( !pdB.get() ) { continue; }

                // Skip comparison for same PD, and also perform it for R types only
                // as we need the bins to reflect the smaller PDs. The H type PDs
                // will then just naturally map to a 2x2 array of bins.
                if ( pdA.get() != pdB.get() && !pdA->isHType() && !pdB->isHType() ) {

                  // PD position
                  const auto ptnB = getPosition( panel, *pdB );

                  const FPTYPE xDiff = fabs( ptnB.X() - ptnA.X() );
                  const FPTYPE yDiff = fabs( ptnB.Y() - ptnA.Y() );

                  // Column numbers
                  const int colA = pdA->pdSmartID().panelLocalModuleColumn();
                  const int colB = pdB->pdSmartID().panelLocalModuleColumn();

                  // Diffs for PDs in neighbouring columns
                  if ( std::abs( colB - colA ) == 1 ) {
                    if ( xDiff > 1 && xDiff < minXdiff_N ) { minXdiff_N = xDiff; }
                    if ( yDiff > 1 && yDiff < minYdiff_N ) { minYdiff_N = yDiff; }
                  }
                  // Diffs for PDs in same column
                  if ( colB == colA ) {
                    if ( xDiff > 1 && xDiff < minXdiff_S ) { minXdiff_S = xDiff; }
                    if ( yDiff > 1 && yDiff < minYdiff_S ) { minYdiff_S = yDiff; }
                  }
                }
              }
            }
          }
        }

        // Add half neighbouring size diff to min max values
        m_minX -= 0.5 * minXdiff_N;
        m_maxX += 0.5 * minXdiff_N;
        m_minY -= 0.5 * minYdiff_N;
        m_maxY += 0.5 * minYdiff_N;
        if ( Rich::Rich2 == panel.rich() ) {
          // Do not fully understand this but RICH2 requires a 1/2 shift in Y..
          m_minY -= 0.5 * minYdiff_N;
          m_maxY -= 0.5 * minYdiff_N;
        }

        // bin size is average of N and S diffs
        const auto minXdiff = 0.5 * ( minXdiff_N + minXdiff_S );
        const auto minYdiff = 0.5 * ( minYdiff_N + minYdiff_S );

        // Compute the number of bins in the lookup table
        const auto        nXbins_f = ( m_maxX - m_minX ) / minXdiff;
        const auto        nYbins_f = ( m_maxY - m_minY ) / minYdiff;
        const ScalarIndex nXbins   = std::round( nXbins_f );
        const ScalarIndex nYbins   = std::round( nYbins_f );
        // If calculation has gone correctly, # bins should be integer values
        assert( fabs( (FPTYPE)nXbins - nXbins_f ) < 1e-5 );
        assert( fabs( (FPTYPE)nYbins - nYbins_f ) < 1e-5 );

        // 1 / X and Y increments ( inverse, for speed in lookup )
        m_incX = FPTYPE( 1.0 ) / minXdiff;
        m_incY = FPTYPE( 1.0 ) / minYdiff;

        // std::cout << "X range     " << m_minX << " " << m_maxX << std::endl;
        // std::cout << "Y range     " << m_minY << " " << m_maxY << std::endl;
        // std::cout << "Diffs X     " << minXdiff_N << " " << minXdiff_S << std::endl;
        // std::cout << "Diffs Y     " << minYdiff_N << " " << minYdiff_S << std::endl;
        // std::cout << "XY Bins (f) " << nXbins_f << " " << nYbins_f << std::endl;
        // std::cout << "XY Bins (i) " << nXbins << " " << nYbins << std::endl;

        // cache SIMD values
        m_minXSIMD = SIMDFP( m_minX );
        m_maxXSIMD = SIMDFP( m_maxX );
        m_minYSIMD = SIMDFP( m_minY );
        m_maxYSIMD = SIMDFP( m_maxY );
        m_incXSIMD = SIMDFP( m_incX );
        m_incYSIMD = SIMDFP( m_incY );

        // initialise the look up table
        m_lookupTable = std::make_unique<LookupTable>( nXbins, nYbins );
        for ( auto iX = 0u; iX < nXbins; ++iX ) {
          for ( auto iY = 0u; iY < nYbins; ++iY ) {
            // get (x,y) coords
            const auto x( binX( iX ) ), y( binY( iY ) );
            // find the PD for this bin
            const auto pd = closestXY( panel, x, y );
            // save in the lookup table
            m_lookupTable->set( iX, iY, pd );
            // sanity checks
            assert( xIndex( x ) == iX );
            assert( yIndex( y ) == iY );
            assert( find( x, y ) == pd );
            assert( find( SIMDFP( x ), SIMDFP( y ) )[0] == pd );
            assert( find( SIMDFP( x ), SIMDFP( y ) )[LHCb::SIMD::FPF::Size - 1] == pd );
          }
        }
      }

    public:
      /// Find the PD for a given position (x,y)
      template <typename TYPE>
      inline auto find( const TYPE x, const TYPE y ) const noexcept {
        assert( m_lookupTable.get() );
        if constexpr ( std::is_arithmetic<TYPE>::value ) {
          // Scalar
          return m_lookupTable->get( xIndex( x ), yIndex( y ) );
        } else {
          // SIMD
          // Get the PD indices
          const auto xyi = m_lookupTable->xyIndex( xIndex( x ), yIndex( y ) );
          // PDs to return
          SIMDPDs<TYPE> pds;
          // Fill the PDs. Can this be vectorised ??
          GAUDI_LOOP_UNROLL( LHCb::SIMD::FPF::Size )
          for ( std::size_t i = 0; i < TYPE::Size; ++i ) { pds[i] = m_lookupTable->get( (ScalarIndex)xyi[i] ); }
          // return the filled PDs
          return pds;
        }
      }
      /// Find the PD for a given position point
      template <typename POINT>
      inline auto find( const POINT& p ) const noexcept {
        return find( p.x(), p.y() );
      }
    };

    /// Photon detector finder
    alignas( LHCb::SIMD::VectorAlignment ) std::unique_ptr<PDFinder<double>> m_pdFinder;

  public:
    /// Find the PDs for a given (x,y) point
    template <typename POINT>
    inline auto getPDs( const POINT& point ) const noexcept {
      assert( m_pdFinder.get() );
      return m_pdFinder.get()->find( point );
    }

  private:
    // data

    /// The RICH detector type
    Rich::DetectorType m_rich = Rich::InvalidDetector;

    /// The RICH PD panel (up, down, left or right)
    Rich::Side m_side = Rich::InvalidSide;

    /// Panel ID
    LHCb::RichSmartID m_panelID;

    /// Owned PD objects
    alignas( LHCb::SIMD::VectorAlignment ) ModuleArray m_PDs;

    /// DetDesc panel object
    const DetElem* m_panel{nullptr};

    /// Shift of the panel plane position in z
    double m_zShift{0};

    /// Panel local module number offset
    LHCb::RichSmartID::DataType m_modNumOffset{0};

    /// Panel local to global frame transformation (includes additional translation)
    ROOT::Math::Transform3D m_pdPanelToGloM;

    /// Global to panel local frame transformation (includes additional translation)
    ROOT::Math::Transform3D m_gloToPDPanelM;

    /// Global to panel local frame transformation
    ROOT::Math::Transform3D m_gloToLocM;

    /// Panel local to global frame transformation
    ROOT::Math::Transform3D m_locToGloM;

    /// SIMD Global to panel local frame transformation (includes additional translation)
    alignas( LHCb::SIMD::VectorAlignment ) Rich::SIMD::Transform3D<Rich::SIMD::DefaultScalarFP> m_gloToPDPanelMSIMD;

    /// SIMD Global to panel local frame transformation
    alignas( LHCb::SIMD::VectorAlignment ) Rich::SIMD::Transform3D<Rich::SIMD::DefaultScalarFP> m_gloToLocMSIMD;

    /// SIMD detection plane (global coords)
    alignas( LHCb::SIMD::VectorAlignment ) Rich::SIMD::Plane<Rich::SIMD::DefaultScalarFP> m_detectionPlaneSIMD;

    /// SIMD detection plane normal (global coords)
    alignas( LHCb::SIMD::VectorAlignment ) Rich::SIMD::Vector<Rich::SIMD::DefaultScalarFP> m_detectionPlaneNormalSIMD;

    /// (X,Y) panel half sizes for this panel
    alignas( LHCb::SIMD::VectorAlignment ) XYArraySIMD m_xyHalfSizeSIMD = {{}};
  };

} // namespace Rich::Detector
