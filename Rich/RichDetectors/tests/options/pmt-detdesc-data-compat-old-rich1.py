###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################

from Gaudi.Configuration import *
from Configurables import LHCbApp, DDDBConf
from Configurables import LHCb__UnpackRawEvent as UnpackRawEvent
from GaudiConf import IOHelper

LHCbApp().Simulation = True
LHCbApp().DataType = "Upgrade"
LHCbApp().DDDBtag = "upgrade/dddb-20220111"

from DDDB.CheckDD4Hep import UseDD4Hep
if UseDD4Hep:
    # Use a geometry from before changes to RICH1
    # https://gitlab.cern.ch/lhcb/Detector/-/merge_requests/205
    DDDBConf().GeometryVersion = 'run3/before-rich1-geom-update-26052022'
    # https://gitlab.cern.ch/lhcb-conddb/lhcb-conditions-database/-/merge_requests/26
    LHCbApp().CondDBtag = "jonrob/all-pmts-active"
else:
    from Configurables import CondDB
    CondDB().setProp("Upgrade", True)
    LHCbApp().CondDBtag = "master"

data = [
    # Older sample created using 2019 DDDB tag
    # "PFN:root://eoslhcb.cern.ch//eos/lhcb/user/j/jonrob/data/MC/Upgrade/NewPMTsSE/13104011/XDST/Brunel-Std-Upgrade-PmtArrayUpdate-SmtID-Pythia8-lumi20-Aug2019-0-000-100.xdst"
    # Private sample created by Bartosz using DDDB tag "upgrade/dddb-20220111"
    "PFN:root://eoslhcb.cern.ch//eos/lhcb/user/j/jonrob/data/MC/Run3/BM-20220111-DDDB/30000000/XDST/Brunel_100.xdst"
]

UnpackRawEvent('UnpackODIN').RawEventLocation = 'Trigger/RawEvent'
UnpackRawEvent('UnpackRich').RawEventLocation = 'Rich/RawEvent'

IOHelper('ROOT').inputFiles(data, clear=True)
FileCatalog().Catalogs = ['xmlcatalog_file:out.xml']
