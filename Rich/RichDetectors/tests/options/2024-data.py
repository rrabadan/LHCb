from __future__ import print_function
###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################

from Gaudi.Configuration import *
from GaudiConf import IOHelper
from Configurables import LHCbApp, DDDBConf
from DDDB.CheckDD4Hep import UseDD4Hep

data = [
    "mdf:root://eoslhcb.cern.ch//eos/lhcb/user/j/jonrob/data/data/RunIII/Hlt2/LHCb/RefIndexCalib/2024/data-0002.mdf"
]
IOHelper('MDF').inputFiles(data, clear=True)

LHCbApp().Simulation = True
LHCbApp().DataType = "Upgrade"
if not UseDD4Hep:
    from Configurables import CondDB
    CondDB().setProp("Upgrade", True)
    LHCbApp().DDDBtag = "upgrade/master"
    LHCbApp().CondDBtag = "upgrade/master"
