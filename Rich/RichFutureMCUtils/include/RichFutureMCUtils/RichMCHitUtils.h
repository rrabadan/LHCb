/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#pragma once

// Kernel
#include "Kernel/RichSmartID.h"

// Event model
#include "Event/MCRichHit.h"

// Rich Utils
#include "RichUtils/RichMap.h"
#include "RichUtils/RichPixelCluster.h"

namespace Rich::Future::MC::Relations {

  /// Helper class for RichMCHits
  class MCHitUtils {

  public:
    /// Constructor from MC Hits
    MCHitUtils( const LHCb::MCRichHits& mchits );

  public:
    // access methods

    /// Get the MCRichHits associated to a given RichSmartID
    decltype( auto ) mcRichHits( const LHCb::RichSmartID smartID ) const {
      const auto i = m_smartIDsToHits.find( smartID );
      return ( i != m_smartIDsToHits.end() ? i->second : m_emptyHitContainer );
    }

    /// Get the MCRichHits associated to a given pixel clust
    decltype( auto ) mcRichHits( const Rich::PDPixelCluster& cluster ) const {
      // primary ID
      auto hits = mcRichHits( cluster.primaryID() );
      // add secondary IDs
      for ( const auto& S : cluster.secondaryIDs() ) {
        const auto chits = mcRichHits( S );
        hits.insert( hits.end(), chits.begin(), chits.end() );
      }
      // return
      return hits;
    }

    // Get the MCRichHits for a given MCParticle
    decltype( auto ) mcRichHits( const LHCb::MCParticle* mcp ) const {
      const auto i = m_MCPsToHits.find( mcp );
      return ( i != m_MCPsToHits.end() ? i->second : m_emptyHitContainer );
    }

  public:
    // types

    /// Type for container of MCHits
    using MCHits = LHCb::MCRichHit::ConstVector;

  private:
    // types

    /// Type for mapping between RichSmartIDs and MCRichHits
    using SmartIDToMCRichHits = Rich::Map<const LHCb::RichSmartID, MCHits>;

    /// Type for mapping from MCParticle to MCRichHits
    using MCPartToMCRichHits = Rich::Map<const LHCb::MCParticle*, MCHits>;

  private:
    // cached data

    /// Mapping from IDs to hits
    SmartIDToMCRichHits m_smartIDsToHits;

    /// Mapping from MCPs to hits
    MCPartToMCRichHits m_MCPsToHits;

    /// Empty container for missing links
    const MCHits m_emptyHitContainer;
  };

} // namespace Rich::Future::MC::Relations
