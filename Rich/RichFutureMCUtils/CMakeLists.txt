###############################################################################
# (c) Copyright 2000-2021 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
#[=======================================================================[.rst:
Rich/RichFutureMCUtils
----------------------
#]=======================================================================]

gaudi_add_library(RichFutureMCUtilsLib
    SOURCES
        src/lib/RichMCHitUtils.cpp
        src/lib/RichMCOpticalPhotonUtils.cpp
        src/lib/RichRecMCHelper.cpp
        src/lib/RichSmartIDMCUtils.cpp
        src/lib/TrackToMCParticle.cpp
    LINK
        PUBLIC
            LHCb::LHCbKernel
            LHCb::MCEvent
            LHCb::RelationsLib
            LHCb::RichUtils
            LHCb::TrackEvent
)

gaudi_add_module(RichFutureMCUtils
    SOURCES
        src/component/TrackToMCParticleRelations.cpp
        src/component/DecodedDataFromMCRichHits.cpp
        src/component/DecodedDataAddMCInfo.cpp
    LINK
        LHCb::LHCbAlgsLib
        LHCb::LinkerEvent
        LHCb::MCEvent
        LHCb::RichFutureKernel
        LHCb::RichFutureMCUtilsLib
        LHCb::TrackEvent
)
