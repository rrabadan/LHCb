/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#include "RichFutureMCUtils/RichMCOpticalPhotonUtils.h"

using namespace Rich::Future::MC::Relations;

MCOpticalPhotonUtils::MCOpticalPhotonUtils( const LHCb::MCRichOpticalPhotons& mcphotons ) {
  for ( const auto mcphot : mcphotons ) {
    if ( mcphot ) {
      const auto mcH = mcphot->mcRichHit();
      if ( mcH ) {
        m_hitToPhot[mcH] = mcphot;
        const auto mcP   = mcH->mcParticle();
        if ( mcP ) {
          auto& phots = m_mcpToPhots[mcP];
          if ( phots.empty() ) { phots.reserve( 40 ); }
          phots.push_back( mcphot );
        }
      }
    }
  }
}
