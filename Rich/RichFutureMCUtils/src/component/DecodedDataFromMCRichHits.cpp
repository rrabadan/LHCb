/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

// Gaudi (must be first)
#include "GaudiKernel/ParsersFactory.h"
#include "GaudiKernel/RndmGenerators.h"
#include "GaudiKernel/StdArrayAsProperty.h"

// Rich (Future) Kernel
#include "RichFutureKernel/RichAlgBase.h"

// Gaudi Functional
#include "LHCbAlgs/Transformer.h"

// Rich Utils
#include "RichFutureUtils/RichDecodedData.h"
#include "RichUtils/RichSmartIDSorter.h"

// Event Model
#include "Event/MCRichDigitSummary.h"
#include "Event/MCRichHit.h"

#include <algorithm>
#include <cassert>
#include <memory>
#include <tuple>
#include <unordered_map>

namespace Rich::Future::MC {

  namespace {
    /// Output data
    using OutData = std::tuple<Rich::Future::DAQ::DecodedData, LHCb::MCRichDigitSummarys>;
  } // namespace

  using namespace Rich::Future::DAQ;

  /** @class DecodedDataFromMCRichHits
   *
   *  Emulates the decoded data structures from MCRichHits
   *
   *  @author Chris Jones
   *  @date   2016-12-07
   */
  class DecodedDataFromMCRichHits final
      : public LHCb::Algorithm::MultiTransformer<OutData( LHCb::MCRichHits const& ),
                                                 Gaudi::Functional::Traits::BaseClass_t<AlgBase<>>> {

  public:
    /// Standard constructor
    DecodedDataFromMCRichHits( const std::string& name, ISvcLocator* pSvcLocator )
        : MultiTransformer( name, pSvcLocator,
                            // inputs
                            {KeyValue{"MCRichHitsLocation", LHCb::MCRichHitLocation::Default}},
                            // output
                            {KeyValue{"DecodedDataLocation", DecodedDataLocation::Default},
                             KeyValue{"DigitSummaryLocation", LHCb::MCRichDigitSummaryLocation::Default}} ) {
      // setProperty( "OutputLevel", MSG::VERBOSE ).ignore();
    }

  public:
    /// Initialize
    StatusCode initialize() override {
      return MultiTransformer::initialize().andThen(
          [&] { m_randZeroOne = Rndm::Numbers( randSvc(), Rndm::Flat( 0.0, 1.0 ) ); } );
    }

    /// Algorithm execution via transform
    OutData operator()( LHCb::MCRichHits const& main_mchits ) const override {

      OutData out_data;
      auto&   decoded_data = std::get<Rich::Future::DAQ::DecodedData>( out_data );
      auto&   summaries    = std::get<LHCb::MCRichDigitSummarys>( out_data );

      // map from Smart ID to associated MC hits
      using HitData = std::pair<const LHCb::MCRichHit*, double>;
      std::unordered_map<LHCb::RichSmartID, std::vector<HitData>> hitsPerPix;

      // Get the location of the container of an MC hit in the TES
      auto hitLocation = [&]( const auto* obj ) { return ( obj ? objectLocation( obj->parent() ) : "Not Contained" ); };

      // Is hit in the main event ?
      auto inMainEvent = [&]( const auto* mchit ) {
        return ( hitLocation( mchit ) == ( "/Event/" + LHCb::MCRichHitLocation::Default ) );
      };

      // Get hit time from MC hit, including offsets
      auto getTime = [&]( const auto* mchit ) {
        if ( !mchit ) { return 0.0; }
        // Deduce if this is a spillover event and if it is apply offset
        const auto   loc         = hitLocation( mchit );
        const double spillOffset =                                             //
            ( ( loc.find( "PrevPrev" ) != std::string::npos ) ? -50.0 :        //
                  ( loc.find( "Prev" ) != std::string::npos ) ? -25.0 :        //
                      ( loc.find( "NextNext" ) != std::string::npos ) ? 50.0 : //
                          ( loc.find( "Next" ) != std::string::npos ) ? 25.0 : //
                              0.0 );
        const auto hitT = spillOffset + mchit->timeOfFlight();
        _ri_verbo << "TES Location '" << loc << "' SpillOffset=" << spillOffset << " HitTime=" << hitT << endmsg;
        return hitT;
      };

      // process a container of MC hits
      auto addMCHits = [&]( auto const& mchits ) {
        for ( const auto mchit : mchits ) {
          const auto id = mchit->sensDetID();
          if ( id.isValid() ) {
            _ri_verbo << id << " " << mchit->mcRichDigitHistoryCode() << endmsg;
            // Apply all sorts MC based filtering here, e.g. signal only, timing windows etc.
            // To be extended as needed
            if ( m_rejectAllBackgrounds && mchit->isBackground() ) {
              _ri_verbo << " -> Rejected as background" << endmsg;
              continue;
            }
            if ( m_rejectScintillation && mchit->radScintillation() ) {
              _ri_verbo << " -> Rejected as scintillation" << endmsg;
              continue;
            }
            // Are we inside the 0-25 ns window ?
            const auto hitT        = getTime( mchit );
            const auto hitTShifted = hitT - m_timeShift[id.rich()];
            _ri_verbo << "Shifted time = " << hitTShifted << endmsg;
            if ( hitTShifted >= 0.0 && hitTShifted <= 25.0 ) {
              _ri_verbo << " -> In Time" << endmsg;
              // finally save this id -> hit mapping
              hitsPerPix[id].emplace_back( mchit, hitT );
            }
          } // hit is valid
        }   // mc hit loop
      };

      // add the main event hits
      addMCHits( main_mchits );

      // ... and if requested spillover hits
      if ( m_enableSpillover ) {
        auto addSpilloverHits = [&]( auto const& mchits ) {
          if ( mchits.exist() ) { addMCHits( *mchits.get() ); }
        };
        addSpilloverHits( m_prevHits );
        addSpilloverHits( m_prevPrevHits );
        addSpilloverHits( m_nextHits );
        addSpilloverHits( m_nextNextHits );
        addSpilloverHits( m_lhcBkgHits );
      }

      // disabled hits
      DetectorArray<std::size_t> disHits{0, 0};

      // Form list of unique IDs
      LHCb::RichSmartID::Vector ids;
      ids.reserve( hitsPerPix.size() );
      summaries.reserve( hitsPerPix.size() );
      for ( const auto& [id, hits] : hitsPerPix ) {

        // Should never fail but just in case...
        if ( hits.empty() ) { continue; }

        // temporary copy of SmartID
        auto newID = id;

        // which rich
        const auto rich = id.rich();

#ifdef USE_DD4HEP
        if ( m_detdescMCinput ) {
          // If built for DD4HEP apply correction to PMT module numbers to account
          // for different numbering scheme between DD4HEP and DetDesc.
          // Option needs to be explicitly activated only when input is known to
          // be DetDesc based MC.
          // ***** To eventually be removed when DetDesc finally dies completely *****
          const Rich::DetectorArray<Rich::PanelArray<LHCb::RichSmartID::DataType>> mod_corr{{{0, 0}, {6, 18}}};
          const auto pdMod      = id.pdMod() + mod_corr[rich][id.panel()];
          const auto pdNumInMod = id.pdNumInMod();
          newID.setPD( pdMod, pdNumInMod );
        }
#endif

        // Are we allowing more than one decoded hit per channel ?
        if ( !m_allowMultiHits ) {
          // 'Normal' mode. Only allow a single decoded hit per channel

          // Apply random inefficiency for each RICH
          if ( m_randZeroOne.shoot() > m_detEff[rich] ) {
            disHits[rich] += hits.size();
            continue;
          }

          // Add best time for this ID
          if ( m_includeTimeInfo ) {
            // Find the one with the eariest time.
            // As hits cannot be empty here, no need to handle this case.
            const auto min_it = std::min_element( hits.begin(), hits.end(),
                                                  []( const auto& a, const auto& b ) { return a.second < b.second; } );
            assert( min_it != hits.end() );
            newID.setTime( min_it->second );
          }

          // Add (dd4hep corrected) ID to decoded list
          ids.emplace_back( newID );

          // Only made 1 digit here so label rest as disabled
          disHits[rich] += ( hits.size() - 1 );

        } else {
          // Allow more than one hit per channel, one per MC hit
          // Useful for granularity studies.

          // loop over all MC hits for this channel ID and collect IDs with time
          LHCb::RichSmartID::Vector time_ids;
          time_ids.reserve( hits.size() );
          for ( const auto& hit : hits ) {
            // Apply random inefficiency for each RICH
            if ( m_randZeroOne.shoot() > m_detEff[rich] ) { continue; }
            // Set time
            newID.setTime( hit.second );
            // Add to IDs with time
            time_ids.emplace_back( newID );
          }

          // sort IDs by time
          std::sort( time_ids.begin(), time_ids.end(),
                     []( const auto& a, const auto& b ) { return a.time() < b.time(); } );

          // fill into decoded IDs
          std::size_t nHitsMade = 0;
          for ( const auto id : time_ids ) {
            ids.emplace_back( id );
            // Limit # hits made
            if ( ++nHitsMade >= m_maxHitsPerChannel[rich] ) { break; }
          }

          // counter for disabled hits
          disHits[rich] += ( hits.size() - nHitsMade );
        }

        // Form MC summaries for each MCHit
        for ( const auto& hit : hits ) {
          auto summary = std::make_unique<LHCb::MCRichDigitSummary>();
          // do not want dd4hep corrected ID here as utility does its own correction.
          summary->setRichSmartID( id );
          summary->setMCParticle( hit.first->mcParticle() );
          auto history = hit.first->mcRichDigitHistoryCode();
          history.setSignalEvent( inMainEvent( hit.first ) );
          summary->setHistory( history );
          summaries.add( summary.release() );
        }

      } // loop over hits

      // Count disabled hits per event
      for ( const auto rich : {Rich::Rich1, Rich::Rich2} ) { m_disabledHits[rich] += disHits[rich]; }

      // Sort
      SmartIDSorter::sortByRegion( ids );

      if ( msgLevel( MSG::DEBUG ) ) {
        _ri_debug << "Created " << ids.size() << " RichSmartIDs" << endmsg;
        for ( const auto id : ids ) { _ri_verbo << id << endmsg; }
      }

      // fill decoded data structure
      decoded_data.addSmartIDs( ids );

      // return
      return out_data;
    }

  private:
    // data

    /// random number between 0 and 1
    Rndm::Numbers m_randZeroOne{};

  private:
    // counters

    /// # Disabled Hits
    mutable DetectorArray<Gaudi::Accumulators::StatCounter<>> m_disabledHits{
        {{this, "# Rich1 Disabled Hits"}, {this, "# Rich2 Disabled Hits"}}};

  private:
    // Handles to access MCRichHits other than Signal, as they are not necessary there
    DataObjectReadHandle<LHCb::MCRichHits> m_prevHits{this, "PrevLocation", "Prev/" + LHCb::MCRichHitLocation::Default};
    DataObjectReadHandle<LHCb::MCRichHits> m_prevPrevHits{this, "PrevPrevLocation",
                                                          "PrevPrev/" + LHCb::MCRichHitLocation::Default};
    DataObjectReadHandle<LHCb::MCRichHits> m_nextHits{this, "NextLocation", "Next/" + LHCb::MCRichHitLocation::Default};
    DataObjectReadHandle<LHCb::MCRichHits> m_nextNextHits{this, "NextNextLocation",
                                                          "NextNext/" + LHCb::MCRichHitLocation::Default};
    DataObjectReadHandle<LHCb::MCRichHits> m_lhcBkgHits{this, "LHCBackgroundLocation",
                                                        "LHCBackground/" + LHCb::MCRichHitLocation::Default};

  private:
    // properties

    /** Temporary workaround for processing DetDesc MC as input.
     *  When active, applies corrections to the data to make compatible
     *  with the dd4hep builds. */
    Gaudi::Property<bool> m_detdescMCinput{this, "IsDetDescMC", true};

    /// Include time information in the decoded data
    Gaudi::Property<bool> m_includeTimeInfo{this, "IncludeTimeInfo", true};

    /// Reject all background hits
    Gaudi::Property<bool> m_rejectAllBackgrounds{this, "RejectAllBackgrounds", false};

    /// Reject scintillation hits
    Gaudi::Property<bool> m_rejectScintillation{this, "RejectScintillation", false};

    /// Include spillover events
    Gaudi::Property<bool> m_enableSpillover{this, "EnableSpillover", true};

    /// Allow multiple hits per channel
    Gaudi::Property<bool> m_allowMultiHits{this, "AllowMultipleHits", false};

    /// When allowing multiple hits per channel, the max number to add per channel
    Gaudi::Property<DetectorArray<std::size_t>> m_maxHitsPerChannel{this, "MaxHitsPerChannel", {99999u, 99999u}};

    /// Time window shift for each RICH
    Gaudi::Property<DetectorArray<double>> m_timeShift{
        this, "TimeCalib", {0.0, 40.0}, "Global time shift for each RICH, to get both to same calibrated point"};

    /// RICH Detector efficiency factor
    Gaudi::Property<DetectorArray<double>> m_detEff{
        this, "ReadoutEfficiency", {0.95, 0.95}, "Readout efficiency to emulate for each RICH"};
  };

  // Declaration of the Algorithm Factory
  DECLARE_COMPONENT( DecodedDataFromMCRichHits )

} // namespace Rich::Future::MC
