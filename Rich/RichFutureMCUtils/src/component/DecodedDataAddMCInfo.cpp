/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

// Rich (Future) Kernel
#include "RichFutureKernel/RichAlgBase.h"

// Gaudi Functional
#include "LHCbAlgs/Transformer.h"

// Rich Utils
#include "RichFutureUtils/RichDecodedData.h"
#include "RichUtils/RichSmartIDSorter.h"

// Event Model
#include "Event/MCRichHit.h"

#include <algorithm>
#include <memory>
#include <tuple>
#include <unordered_map>

namespace Rich::Future::MC {

  using namespace Rich::Future::DAQ;

  /** @class DecodedDataFromMCRichHits
   *
   *  Update decoded data with MC information (e.g. time)
   *
   *  @author Chris Jones
   *  @date   2016-12-07
   */
  class DecodedDataAddMCInfo final
      : public LHCb::Algorithm::Transformer<DecodedData( DecodedData const&, LHCb::MCRichHits const& ),
                                            Gaudi::Functional::Traits::BaseClass_t<AlgBase<>>> {

  public:
    /// Standard constructor
    DecodedDataAddMCInfo( const std::string& name, ISvcLocator* pSvcLocator )
        : Transformer( name, pSvcLocator,
                       // inputs
                       {KeyValue{"InDecodedDataLocation", DecodedDataLocation::Default},
                        KeyValue{"MCRichHitsLocation", LHCb::MCRichHitLocation::Default}},
                       // output
                       {KeyValue{"OutDecodedDataLocation", DecodedDataLocation::Default}} ) {
      // setProperty( "OutputLevel", MSG::VERBOSE ).ignore();
    }

  public:
    /// Algorithm execution via transform
    DecodedData operator()( DecodedData const& data, LHCb::MCRichHits const& main_mchits ) const override {

      // map from Smart ID to associated MC hits
      std::unordered_map<LHCb::RichSmartID, std::vector<LHCb::MCRichHit*>> hitsPerPix;

      // Get the location of the container of an MC hit in the TES
      auto hitLocation = [&]( const auto* obj ) { return ( obj ? objectLocation( obj->parent() ) : "Not Contained" ); };

      // Get hit time from MC hit, including offsets
      auto getTime = [&]( const auto* mchit ) {
        if ( !mchit ) { return 0.0; }
        // Deduce if this is a spillover event and if it is apply offset
        const auto loc         = hitLocation( mchit );
        double     spillOffset = 0.0;
        if ( loc.find( "PrevPrev" ) != std::string::npos ) {
          spillOffset = -50.0;
        } else if ( loc.find( "Prev" ) != std::string::npos ) {
          spillOffset = -25.0;
        } else if ( loc.find( "NextNext" ) != std::string::npos ) {
          spillOffset = 50.0;
        } else if ( loc.find( "Next" ) != std::string::npos ) {
          spillOffset = 25.0;
        }
        _ri_verbo << "Loc " << loc << " " << spillOffset << endmsg;
        // return offset + TOF
        return spillOffset + mchit->timeOfFlight();
      };

      // process a container of MC hits
      auto addMCHits = [&]( auto const& mchits ) {
        _ri_debug << "Found " << mchits.size() << " MCRichHits at " << objectLocation( mchits ) << endmsg;
        for ( const auto mchit : mchits ) {
          auto id = mchit->sensDetID().channelDataOnly();
#ifdef USE_DD4HEP
          if ( m_detdescMCinput ) {
            // If built for DD4HEP apply correction to PMT module numbers to account
            // for different numbering scheme between DD4HEP and DetDesc.
            // Option needs to be explicitly activated only when input is known to
            // be DetDesc based MC.
            // ***** To eventually be removed when DetDesc finally dies completely *****
            const Rich::DetectorArray<Rich::PanelArray<LHCb::RichSmartID::DataType>> mod_corr{{{0, 0}, {6, 18}}};
            const auto pdMod      = id.pdMod() + mod_corr[id.rich()][id.panel()];
            const auto pdNumInMod = id.pdNumInMod();
            id.setPD( pdMod, pdNumInMod );
          }
#endif
          if ( id.isValid() ) { hitsPerPix[id].push_back( mchit ); }
        }
      };

      // add the main hits
      addMCHits( main_mchits );

      // .. and spillover if required
      if ( m_enableSpillover ) {
        auto addSpilloverHits = [&]( auto const& mchits ) {
          if ( mchits.exist() ) { addMCHits( *mchits.get() ); }
        };
        addSpilloverHits( m_prevHits );
        addSpilloverHits( m_prevPrevHits );
        addSpilloverHits( m_nextHits );
        addSpilloverHits( m_nextNextHits );
        addSpilloverHits( m_lhcBkgHits );
      }

      // Clone decoded data
      DecodedData out_data = data;

      // Loop over data and add MC time info
      // First, RICH detectors
      for ( auto& rD : out_data ) {
        // sides per RICH
        for ( auto& pD : rD ) {
          // PD modules per side
          for ( auto& mD : pD ) {
            // PDs per module
            for ( auto& pd : mD ) {
              // IDs per PD
              for ( auto& id : pd.smartIDs() ) {

                // Find MC hit, if available, for this ID
                const LHCb::MCRichHit* mch       = nullptr;
                const auto             id_in_map = hitsPerPix.find( id );
                if ( id_in_map != hitsPerPix.end() ) {
                  const auto& pix_mchits = id_in_map->second;
                  if ( !pix_mchits.empty() ) {
                    // Find best MCHit to use.
                    // Either first labelled as signal, otherwise just the first in the container.
                    mch = pix_mchits.front();
                    for ( const auto* h : pix_mchits ) {
                      if ( h->isSignal() ) {
                        mch = h;
                        break;
                      }
                    }
                  }
                }

                // Set time in ID
                // If no MC hit found, set 0 (so will be out of time)
                id.setTime( getTime( mch ) );
                _ri_verbo << id << endmsg;
              }
            }
          }
        }
      }

      // return
      return out_data;
    }

  private:
    // Handles to access MCRichHits other than Signal, as they are not necessary there
    DataObjectReadHandle<LHCb::MCRichHits> m_prevHits{this, "PrevLocation", "Prev/" + LHCb::MCRichHitLocation::Default};
    DataObjectReadHandle<LHCb::MCRichHits> m_prevPrevHits{this, "PrevPrevLocation",
                                                          "PrevPrev/" + LHCb::MCRichHitLocation::Default};
    DataObjectReadHandle<LHCb::MCRichHits> m_nextHits{this, "NextLocation", "Next/" + LHCb::MCRichHitLocation::Default};
    DataObjectReadHandle<LHCb::MCRichHits> m_nextNextHits{this, "NextNextLocation",
                                                          "NextNext/" + LHCb::MCRichHitLocation::Default};
    DataObjectReadHandle<LHCb::MCRichHits> m_lhcBkgHits{this, "LHCBackgroundLocation",
                                                        "LHCBackground/" + LHCb::MCRichHitLocation::Default};

  private:
    // properties

    /** Temporary workaround for processing DetDesc MC as input.
     *  When active, applies corrections to the data to make compatible
     *  with the dd4hep builds. */
    Gaudi::Property<bool> m_detdescMCinput{this, "IsDetDescMC", true};

    /// Include spillover events
    Gaudi::Property<bool> m_enableSpillover{this, "EnableSpillover", true};
  };

  // Declaration of the Algorithm Factory
  DECLARE_COMPONENT( DecodedDataAddMCInfo )

} // namespace Rich::Future::MC
