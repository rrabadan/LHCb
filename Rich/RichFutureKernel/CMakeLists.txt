###############################################################################
# (c) Copyright 2000-2021 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
#[=======================================================================[.rst:
Rich/RichFutureKernel
---------------------
#]=======================================================================]

gaudi_add_library(RichFutureKernel
    SOURCES
        src/RichAlgBase.cpp
        src/RichHistoAlgBase.cpp
        src/RichHistoToolBase.cpp
        src/RichToolBase.cpp
        src/RichTupleAlgBase.cpp
        src/RichTupleToolBase.cpp
    LINK
        PUBLIC
            AIDA::aida
            Boost::headers
            Gaudi::GaudiAlgLib
            Gaudi::GaudiKernel
            LHCb::RichFutureUtils
            LHCb::RichUtils
        PRIVATE
            Gaudi::GaudiUtilsLib
)
