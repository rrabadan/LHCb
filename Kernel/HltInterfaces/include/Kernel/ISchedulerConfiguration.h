/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once
#include "GaudiKernel/INamedInterface.h"
#include "Kernel/STLExtensions.h"
#include <any>
#include <cassert>
#include <map>
#include <string>

/** @class ISchedulerConfiguration ISchedulerConfiguration.h
 *
 *  @author Gerhard Raven
 *  @date   2021-07-02
 */

namespace LHCb::Interfaces {
  struct ISchedulerConfiguration : extend_interfaces<INamedInterface> {
    /// Return the interface ID
    DeclareInterfaceID( ISchedulerConfiguration, 1, 0 );

    class State {

    public:
      struct NodeState {
        uint16_t executionCtr;
        bool     passed;
      };

      friend std::ostream& operator<<( std::ostream& os, NodeState const& state ) {
        return os << state.executionCtr << "|" << state.passed;
      }

      struct AlgState {
        bool isExecuted;
        bool filterPassed;
      };

      LHCb::span<const NodeState> nodes() const {
        assert( m_data.has_value() );
        return m_nodes;
      }
      LHCb::span<NodeState> nodes() {
        assert( m_data.has_value() );
        return m_nodes;
      }
      const NodeState& node( int i ) const {
        assert( m_data.has_value() );
        return m_nodes[i];
      }
      LHCb::span<const AlgState> algorithms() const {
        assert( m_data.has_value() );
        return m_algs;
      }
      LHCb::span<AlgState> algorithms() {
        assert( m_data.has_value() );
        return m_algs;
      }
      const AlgState& algorithm( int i ) const {
        assert( m_data.has_value() );
        return m_algs[i];
      }

      template <typename Alloc1, typename Alloc2>
      State( std::vector<NodeState, Alloc1> nodeStates, std::vector<AlgState, Alloc2> algorithmStates )
          : m_nodes{nodeStates}
          , m_algs{algorithmStates}
          , // slighly naughty, but we know that moving a vector doesn't invalidate a span over its data...
          m_data{std::pair{std::move( nodeStates ), std::move( algorithmStates )}}
          , m_copy{[]( std::any const& a, State& that ) -> State& {
            using Pair   = std::pair<std::vector<NodeState, Alloc1>, std::vector<AlgState, Alloc2>>;
            auto& out    = that.m_data.emplace<Pair>( std::any_cast<Pair const&>( a ) );
            that.m_nodes = out.first;
            that.m_algs  = out.second;
            return that;
          }} {}

      State( const State& s ) { ( *m_copy )( s.m_data, *this ); }
      State& operator=( const State& s ) { return ( *m_copy )( s.m_data, *this ); }

      // Moving a vector (even if inside an `any`) doesn't invalidate a span over it,
      // so the `span`s can just be copied (or moved) blindly...
      State( State&& ) = default;
      State& operator=( State&& ) = default;

    private:
      LHCb::span<NodeState> m_nodes;
      LHCb::span<AlgState>  m_algs;
      std::any              m_data;

      State& ( *m_copy )( std::any const&, State& );
    };

    /// introspection
    virtual std::map<std::string, int> getNodeNamesWithIndices() const = 0;

    // printout
    virtual std::stringstream buildPrintableStateTree( const State& ) const = 0;
    virtual std::stringstream buildAlgsWithStates( const State& ) const     = 0;
  };
} // namespace LHCb::Interfaces
