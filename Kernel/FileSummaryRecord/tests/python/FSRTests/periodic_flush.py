###############################################################################
# (c) Copyright 2022-2023 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
import os
import json
from traceback import format_exc
from unittest import TestCase
from pprint import pformat
from multiprocessing import Process

FILENAMEFSR = f"{__name__}.fsr.json"
FILENAME = f"{__name__}.root"
FILENAMEJSON = f"{__name__}.json"


def checkDiff(a, b):
    try:
        TestCase().assertEqual(a, b)
    except AssertionError as err:
        return str(err)


def keep_intermediate_files(filename):
    print("===> starting thread to copy FSR dumps", flush=True)
    from time import sleep, time
    wait_time = 0.01
    max_time = time() + 300
    counter = -1
    # when to stop looking for new FSR dumps after event 5
    # or if we have been running for too long
    while counter < 5 and time() < max_time:
        try:
            data = open(filename).read()
            parsed_data = json.loads(data)  # check that the file can be parser
            # we make a copy of the FSR file for each event, only if the output file guid is filled
            if not parsed_data.get("guid"):
                continue
            counter = parsed_data.get("EvtCounter.count", {}).get(
                "nEntries", -1)
            if counter >= 0 and not os.path.exists(f"{filename}.~{counter}~"):
                with open(f"{filename}.~{counter}~", "w") as f:
                    f.write(data)
                    print("=== FSR begin ===")
                    print(data.rstrip())
                    print("=== FSR  end  ===")
                    print(f"---> wrote {filename}.~{counter}~", flush=True)
        except FileNotFoundError:
            pass
        except json.JSONDecodeError:
            continue  # the file is corrupted (it might be being written)

        sleep(wait_time)

    print("===> stopping thread to copy FSR dumps", flush=True)


def config():
    from PyConf.application import configure, configure_input, ApplicationOptions, root_writer
    from PyConf.control_flow import CompositeNode
    from PyConf.components import setup_component
    from PyConf.Algorithms import EventCountAlg, Gaudi__Examples__IntDataProducer, GaudiTesting__SleepyAlg
    from Configurables import ApplicationMgr

    options = ApplicationOptions(_enabled=False)
    # No data from the input is used, but something should be there for the configuration
    options.input_files = ["dummy_input_file_name.dst"]
    options.input_type = 'ROOT'
    options.output_file = FILENAME
    options.output_type = 'ROOT'
    options.data_type = 'Upgrade'
    options.dddb_tag = 'upgrade/dddb-20220705'
    options.conddb_tag = 'upgrade/sim-20220705-vc-mu100'
    options.geometry_version = 'run3/trunk'
    options.conditions_version = 'master'
    options.simulation = True
    options.evt_max = 5
    options.monitoring_file = FILENAMEJSON
    # options.output_level = 2

    config = configure_input(options)

    app = ApplicationMgr()
    app.EvtSel = "NONE"  # ignore input configuration
    app.ExtSvc.append(
        config.add(
            setup_component(
                "LHCb__FSR__Sink",
                instance_name="FileSummaryRecord",
                AcceptRegex=r"^EvtCounter\.count$",
                OutputFile=FILENAMEFSR,
                AutoFlushPeriod=0.3,  # seconds
            )))

    producer = Gaudi__Examples__IntDataProducer()

    # the ExtraOutputs options for SleepyAlg are not used, but there to please PyConf
    # (it does not allow two algorithm instances that differ only by the name)
    cf = CompositeNode("test", [
        GaudiTesting__SleepyAlg(
            name="SleepBefore", SleepTime=1, ExtraOutputs=["Stamp1"]),
        EventCountAlg(name="EvtCounter"),
        GaudiTesting__SleepyAlg(
            name="SleepAfter", SleepTime=1, ExtraOutputs=["Stamp2"]),
        producer,
        root_writer(options.output_file, [producer.OutputLocation]),
    ])
    app.OutStream.clear()
    config.update(configure(options, cf))

    # make sure the histogram file is not already there
    for name in ([FILENAME, FILENAMEFSR, FILENAMEJSON] +
                 [f"{FILENAMEFSR}.~{i}~" for i in range(10)]):
        if os.path.exists(name):
            os.remove(name)

    # start a subprocess that tracks changes to FILENAMEFSR and keeps
    # back up versions of it
    Process(target=keep_intermediate_files, args=(FILENAMEFSR, )).start()


def check(causes, result):
    result["root_output_file"] = FILENAME

    # we expect 6 copies of the FSR JSON dump, with increasing counts
    missing_files = [
        name for name in ([f"{FILENAMEFSR}.~{i}~" for i in range(6)])
        if not os.path.exists(name)
    ]
    if missing_files:
        causes.append("missing output file(s)")
        result["missing_files"] = ", ".join(missing_files)
        return False

    data = "FSR not found"
    try:
        for i in range(6):
            data = json.load(open(f"{FILENAMEFSR}.~{i}~"))
            expected = i
            found = data.get("EvtCounter.count", {}).get("nEntries")
            if found != expected:
                causes.append("invalid partial count")
                if "count_mismatch" in result.annotations:
                    base = result["count_mismatch"] + "\n"
                else:
                    base = ""
                result[
                    "count_mismatch"] = f"{base}FSR snapshot {i} expected to contain {expected} but we found {found}"

    except Exception as err:
        causes.append("failure in JSON dumps")
        result["python_exception"] = result.Quote(format_exc())
        return False

    result["FSR"] = result.Quote(pformat(data))

    return True
