/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#include "Event/EventCountFSR.h"

#include "Gaudi/Accumulators.h"
#include "Gaudi/Algorithm.h"
#include "GaudiAlg/FixTESPath.h"
#include "GaudiKernel/IIncidentListener.h"
#include "GaudiKernel/IIncidentSvc.h"
#include "GaudiKernel/IOpaqueAddress.h"
#include "GaudiKernel/IRegistry.h"

/**
 *  @author Rob Lambert
 *  @date   2009-11-11
 *
 *  EventAccounting fills an EventCountFSR.
 *         This alg should be run by default in Brunel and DaVinci
 *         The EventCountFSR holds information on how many events should be in the file
 *         and how many were read from the input files used.
 *         A flag is also set in case the number stored is known to be wrong.
 *   -<c>"OverrideStatus"</c>: Override the status by the DefaultStatus in the FSR at the end of the job
 *   -<c>"DefaultStatus"</c>:  The status to start with if nothing else is known
 *
 */
class EventAccounting : public extends<FixTESPath<Gaudi::Algorithm>, IIncidentListener> {
public:
  using extends::extends;

  StatusCode initialize() override;
  StatusCode execute( const EventContext& ) const override;
  StatusCode finalize() override;

  // IIncindentListener interface
  void handle( const Incident& ) override;

protected:
  LHCb::EventCountFSR* m_eventFSR = nullptr; // FSR for current file

  std::map<std::string, unsigned int> m_count_files; // a map of string to int for filenames

  mutable Gaudi::Accumulators::Counter<> m_count_events{this, "Number of events seen"};
  mutable Gaudi::Accumulators::Counter<> m_count_output{this, "Number of incidents seen"};

  Gaudi::Property<bool> m_overrideStatus{this, "OverrideStatus", false, "override status at end of job with default"};
  Gaudi::Property<std::string> m_defaultStatusStr = {this, "DefaultStatus", "UNCHECKED",
                                                     "status to start with if nothing else is known"};
  Gaudi::Property<std::string> m_FSRName{this, "OutputDataContainer", LHCb::EventCountFSRLocation::Default,
                                         "output location of summary data in FSR"};

  LHCb::EventCountFSR::StatusFlag m_defaultStatus = {
      LHCb::EventCountFSR::StatusFlag::UNCHECKED}; /// status to start with if nothing else is known, cast from
                                                   /// DefaultStatus

  ServiceHandle<IDataProviderSvc> m_fileRecordSvc{this, "FileRecordDataSvc", "FileRecordDataSvc",
                                                  "Reference to run records data service"};

private:
  ServiceHandle<IIncidentSvc> m_incSvc{this, "IncidentSvc", "IncidentSvc", "the incident service"};
};

// Declaration of the Algorithm Factory
DECLARE_COMPONENT( EventAccounting )

StatusCode EventAccounting::initialize() {
  return extends::initialize().andThen( [&]() {
    ;
    m_defaultStatus = LHCb::EventCountFSR::StatusFlagToType( m_defaultStatusStr.value() );
    if ( msgLevel( MSG::DEBUG ) ) debug() << "==> Default status: " << m_defaultStatus << endmsg;

    // prepare TDS for FSR
    m_eventFSR = new LHCb::EventCountFSR();
    m_eventFSR->setStatusFlag( m_defaultStatus );
    m_fileRecordSvc->registerObject( fullTESLocation( m_FSRName.value(), true ), m_eventFSR ).ignore();

    // check extended file incidents are defined
#ifdef GAUDI_FILE_INCIDENTS
    m_incSvc->addListener( this, IncidentType::WroteToOutputFile );
    if ( msgLevel( MSG::DEBUG ) ) debug() << "registered with incSvc" << endmsg;
      // if not then the counting is not reliable
#else
    m_eventFSR->setStatusFlag( LHCb::EventCountFSR::UNRELIABLE );
    warn() << "cannot register with incSvc" << endmsg;
#endif // GAUDI_FILE_INCIDENTS
    return StatusCode::SUCCESS;
  } );
}

StatusCode EventAccounting::execute( const EventContext& ) const {
  // maintain a simple event count
  ++m_count_events;
  m_eventFSR->setInput( m_count_events.nEntries() );

  return StatusCode::SUCCESS;
}

StatusCode EventAccounting::finalize() {
  // if more than one file is written, the count is unreliable
  if ( m_count_files.size() != 1 ) m_eventFSR->setStatusFlag( LHCb::EventCountFSR::StatusFlag::UNRELIABLE );

  // this can be overwritten with options for production
  if ( m_overrideStatus.value() ) {
    info() << "Overriding status: " << m_eventFSR->statusFlag() << " with: " << m_defaultStatus << endmsg;
    m_eventFSR->setStatusFlag( m_defaultStatus );
  }

  // some printout of FSRs
  if ( msgLevel( MSG::DEBUG ) ) {
    debug() << "number of output files seen: " << m_count_files.size() << endmsg;
    // FSR - use the class method which prints it
    debug() << "FSR: " << *m_eventFSR << endmsg;
    // check if the FSRs can be retrieved from the TS
    DataObject*          obj = nullptr;
    LHCb::EventCountFSR* readFSR =
        m_fileRecordSvc->retrieveObject( fullTESLocation( m_FSRName.value(), true ), obj ).isSuccess()
            ? dynamic_cast<LHCb::EventCountFSR*>( obj )
            : nullptr;
    if ( readFSR ) {
      debug() << "READ FSR: " << *readFSR << endmsg;
    } else {
      error() << "get():: No valid data at '" << m_FSRName.value() << "'" << endmsg;
    }
  }
  return extends::finalize(); // must be called after all other actions
}

/// IIncindentListener interface
void EventAccounting::handle( const Incident& incident ) {
  // check extended file incidents are defined
#ifdef GAUDI_FILE_INCIDENTS
  if ( incident.type() == IncidentType::WroteToOutputFile ) {
    // maintain a per-file counter for now... could be used later to write many FSRs
    m_count_files[incident.source()] += 1;
    // this number will be incorrect if there is more than one file produced
    ++m_count_output;
    // set every time, and worry about it only in finalize
    m_eventFSR->setOutput( m_count_output.nEntries() );
  }
#endif
}
