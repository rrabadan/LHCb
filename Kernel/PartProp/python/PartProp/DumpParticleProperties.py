#!/usr/bin/env python
###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################

#  @author Vanya BELYAEV Ivan.Belyaev@nikhef.nl
#  @date 2010-10-22
"""Trivial script to dump the table of Particle Properties"""

import sys


def dump(args=[]):

    from argparse import ArgumentParser
    parser = ArgumentParser(
        prog='dump_particle_properties',
        description="""
        Dump the table of particle properties
        Optionally specify the DDDB and CONDDB ags, datatype and Simulation flags
        """,
    )
    parser.add_argument(
        "-d",
        "--DDDB",
        type=str,
        dest='DDDBtag',
        help="DDDB tag to be used [default: %(default)s]",
        default='')
    parser.add_argument(
        "-c",
        "--CONDDB",
        type=str,
        dest='CONDDBtag',
        help="CONDDB tag to be used [default: %(default)s]",
        default='')
    parser.add_argument(
        "-t",
        "--DATATYPE",
        type=str,
        dest='DataType',
        help="DataType to be used [default: %(default)s]",
        default='')
    parser.add_argument(
        "-s",
        "--Simulation",
        action="store_true",
        dest='Simulation',
        help="Simulation ? [default: %(default)s]",
        default=False)

    ## eliminate artifacts and parse command-line arguments
    if not args: args = [a for a in sys.argv[1:] if '--' != a]
    arguments = parser.parse_args(args)

    from Configurables import ApplicationMgr
    ApplicationMgr(OutputLevel=6)

    import PartProp.PartPropAlg
    import PartProp.Service

    ## configure PyConf
    from PyConf.application import ApplicationOptions, configure_input
    options = ApplicationOptions(_enabled=False)
    options.input_type = 'None'
    options.dddb_tag = arguments.DDDBtag if arguments.DDDBtag else 'master'
    options.conddb_tag = arguments.CONDDBtag if arguments.CONDDBtag else 'master'
    options.data_type = arguments.DataType if arguments.DataType else '2024'
    options.simulation = arguments.Simulation
    config = configure_input(options)

    from GaudiPython.Bindings import AppMgr
    gaudi = AppMgr()
    gaudi.initialize()
    pps = gaudi.ppSvc()
    print(pps.all())


if '__main__' == __name__:
    dump([a for a in sys.argv[1:] if '--' != a])
