#!/usr/bin/env python
###############################################################################
# (c) Copyright 2023 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################

import pytest
from PartProp.ParticleTable import ParticleTable

from GaudiKernel.SystemOfUnits import MeV, ps


@pytest.fixture()
def particle_table():
    return ParticleTable()


def test_mass(particle_table):
    assert round(particle_table.mass("B0"), 2) == round(5279.64 * MeV, 2)
    assert round(particle_table.mass("B+"), 2) == round(5279.33 * MeV, 2)
    assert round(particle_table.mass("B_s0"), 2) == round(5366.88 * MeV, 2)
    assert round(particle_table.mass("K+"), 2) == round(493.677 * MeV, 2)
    assert round(particle_table.mass("KS0"), 2) == round(497.611 * MeV, 2)
    assert round(particle_table.mass("pi+"), 2) == round(139.57061 * MeV, 2)
    assert round(particle_table.mass("pi0"), 2) == round(134.97700 * MeV, 2)


def test_cc(particle_table):
    assert particle_table.cc("B0") == "B~0"
    assert particle_table.cc("B+") == "B-"
    assert particle_table.cc("B_s0") == "B_s~0"
    assert particle_table.cc("K+") == "K-"
    assert particle_table.cc("KS0") == "KS0"
    assert particle_table.cc("pi+") == "pi-"
    assert particle_table.cc("pi0") == "pi0"


def test_charge(particle_table):
    assert particle_table.charge("B0") == 0.
    assert particle_table.charge("B+") == 1.
    assert particle_table.charge("B_s0") == 0.
    assert particle_table.charge("K-") == -1.
    assert particle_table.charge("KS0") == 0.
    assert particle_table.charge("pi-") == -1.
    assert particle_table.charge("pi0") == 0.


def test_pid(particle_table):
    assert particle_table.pid("B0") == 511
    assert particle_table.pid("B+") == 521
    assert particle_table.pid("B_s0") == 531
    assert particle_table.pid("K-") == -321
    assert particle_table.pid("KS0") == 310
    assert particle_table.pid("pi-") == -211
    assert particle_table.pid("pi0") == 111

    assert particle_table.abs_pid("B0") == 511
    assert particle_table.abs_pid("B+") == 521
    assert particle_table.abs_pid("B_s0") == 531
    assert particle_table.abs_pid("K-") == 321
    assert particle_table.abs_pid("KS0") == 310
    assert particle_table.abs_pid("pi-") == 211
    assert particle_table.abs_pid("pi0") == 111


def test_lifetime(particle_table):
    assert round(particle_table.lifetime("B0"), 2) == round(1.520120 * ps, 2)
    assert round(particle_table.lifetime("B+"), 2) == round(1.638158 * ps, 2)
    assert round(particle_table.lifetime("B_s0"), 2) == round(1.512000 * ps, 2)
