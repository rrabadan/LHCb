/*****************************************************************************\
* (c) Copyright 2000-2024 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once

#include "RootIOAlgBase.h"
#include "RootIOHandler.h"

namespace LHCb::IO {

  /**
   * Algorithms able to mix ROOT files, selecting which input to use for every iteration
   *
   * See documentation of RootIOAlgBase for details on how this algorithm works,
   * in particular why it's not as nicely functional as it seems and how to
   * configure it.
   *
   * Note that the Property NSkip, if used, is skipping that amount of event from
   * each input file
   */
  template <bool ReadAllBranches = false>
  class RootIOMixerBase : public RootIOAlgBase<ReadAllBranches> {
  public:
    using RootIOAlgBase<ReadAllBranches>::RootIOAlgBase;

    StatusCode initialize() override;
    StatusCode finalize() override {
      for ( auto& ioHandler : m_ioHandlers ) { ioHandler->finalize(); }
      return RootIOAlgBase<ReadAllBranches>::finalize();
    };

    typename RootIOHandler<ReadAllBranches>::EventType nextEvent( EventContext const& evtCtx ) const override;

    /**
     * Should return a number between 0 (included) and given number (excluded) specifying which
     * input file should be used for next event.
     * Any result greater or equal to given number may yield to undefined behavior
     */
    virtual unsigned int chooseInput( unsigned int ) const = 0;

  private:
    Gaudi::Property<std::vector<std::vector<std::string>>> m_input{this, "Input", {}, "List of input files"};
    // set of ioHandlers to use, one per input file
    // Note that IOHandler::next is thread safe, so the usage of mutable is safe
    mutable std::vector<std::unique_ptr<RootIOHandler<ReadAllBranches>>> m_ioHandlers;
  };

  using RootIOMixer    = LHCb::IO::RootIOMixerBase<false>;
  using RootIOMixerExt = LHCb::IO::RootIOMixerBase<true>;

} // namespace LHCb::IO

template <bool ReadAllBranches>
StatusCode LHCb::IO::RootIOMixerBase<ReadAllBranches>::initialize() {
  return RootIOAlgBase<ReadAllBranches>::initialize().andThen( [&]() {
    // create one IOHandler per file
    m_ioHandlers.reserve( m_input.size() );
    for ( auto& input : m_input ) {
      auto& ioHandler = m_ioHandlers.emplace_back( std::make_unique<RootIOHandler<ReadAllBranches>>( *this, input ) );
      ioHandler->initialize( *this, &*this->m_incidentSvc, this->m_bufferNbEvents.value(),
                             this->m_nbSkippedEvents.value(), this->m_eventTreeName.value(),
                             this->m_eventBranches.value(), this->m_allowMissingInput.value(),
                             this->m_storeOldFSRs.value(), this->m_evtPrintFrequency.value() );
    }
  } );
}

template <bool ReadAllBranches>
typename LHCb::IO::RootIOHandler<ReadAllBranches>::EventType
LHCb::IO::RootIOMixerBase<ReadAllBranches>::nextEvent( EventContext const& evtCtx ) const {
  return m_ioHandlers[chooseInput( m_ioHandlers.size() ) % m_ioHandlers.size()]->next( *this, evtCtx );
}
