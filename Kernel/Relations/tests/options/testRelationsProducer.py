###############################################################################
# (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################

from Gaudi.Configuration import *

from Configurables import LHCb__Relations__Test__SquaresProducer as SquaresProducer
from Configurables import LHCb__Relations__Test__SquaresConsumer as SquaresConsumer

app = ApplicationMgr()
app.TopAlg = [SquaresProducer(), SquaresConsumer()]

app.EvtMax = 2
app.EvtSel = "NONE"
app.HistogramPersistency = "NONE"
