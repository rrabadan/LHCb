/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once

#include "GaudiKernel/IInterface.h"
#include "Relations/Bases.h"
#include "Relations/IRelation.h"
#include "Relations/RelationBase.h"
#include "Relations/RelationUtils.h"
#include <algorithm>
#include <utility>

namespace Relations {

  /** @class Relation Relation.h Relations/Relation.h
   *
   *  @brief Implementation of ordinary unidirectional relations
   *
   *  Implementation of ordinary unidirectional relations
   *  from objects of type "FROM" to objects of type "TO".
   *
   *  Data types for "FROM" and "TO":
   *
   *   - or have the templated specializations through
   *     ObjectTypeTraits structure with defined streamer operator
   *     to/from StreamBuffer class
   *
   *  @see ObjectTypeTraits
   *  @see StreamBuffer
   *  @see IRelations
   *  @warning for the current implementation the actual type of
   *           FROM should differ from the actual type of TO
   *
   *  @author Vanya Belyaev Ivan.Belyaev@itep.ru
   *  @date   25/01/2002
   */
  template <class FROM, class TO>
  class Relation : public BaseTable, public virtual IRelation<FROM, TO> {
  public:
    /// short cut for own     type
    using OwnType = Relation<FROM, TO>;
    /// short cut for inverse type
    using InvType = Relation<TO, FROM>;
    /// short cut for interface
    using IBase = IRelation<FROM, TO>;
    /// short cut for the actual implementation type
    using Base = typename Relations::RelationBase<FROM, TO>;
    /// shortcut for inverse base
    using InvBase = typename Relations::RelationBase<TO, FROM>;
    /// shortcut for direct base type
    using Direct = Base;
    /// shortcut for inverse base type
    using Inverse = InvBase;
    /// shortcut for direct  interface
    using IDirect = IBase;
    /// shortcut for inverse base type
    using IInverse = typename IDirect::InverseType;
    /// basic types from Interface
    using Range = typename IBase::Range;
    /// basic types from Interface
    using From_ = typename IBase::From_;
    using From  = typename IBase::From;
    /// basic types from Interface
    using To_ = typename IBase::To_;
    using To  = typename IBase::To;
    /// the actual type of the entry
    using Entry = typename IBase::Entry;

    /// Default constructor
    Relation() = default;
    /// constructor with reserve size
    Relation( const size_t reserve ) : m_direct( reserve ) {}
    /** constructor from arbitrary "direct" interface
     *  @param copy object to be copied
     */
    Relation( const IDirect& copy ) : m_direct( copy ) {}
    /** constructor from "inverse interface"
     *  It is an efficient way to "invert" relation
     *  @param inv interface to be inverted
     *  @param tag artificial argument, to distinguish from copy constructor
     */

    struct inverse_tag {};
    Relation( const IInverse& inv, inverse_tag ) : m_direct( inv, typename Direct::inverse_tag{} ) {}
    /** copy constructor is publc,
     *  but it is not recommended for direct usage
     *  @param copy object to be copied
     */
    Relation( const OwnType& c ) : IRelation<FROM, TO>( c ), m_direct( c.m_direct ) {}

    /// move constructor
    Relation( OwnType&& m ) : IRelation<FROM, TO>( std::move( m ) ), m_direct( std::move( m.m_direct ) ) {}

    /// assignement operator is deleted
    Relation& operator=( const OwnType& ) = delete;
    Relation& operator=( OwnType&& ) = delete;

    /// destructor (virtual)
    virtual ~Relation() = default;

    // major functional methods (fast, 100% inline)

    /// retrive all relations from the object (fast,100% inline)
    Range i_relations( From_ object ) const {
      auto ip = m_direct.i_relations( object );
      return Range( ip.first, ip.second );
    }
    /// retrive all relations from ALL objects (fast,100% inline)
    Range i_relations() const {
      auto ip = m_direct.i_relations();
      return Range( ip.first, ip.second );
    }
    /// make the relation between 2 objects (fast,100% inline method)
    StatusCode i_relate( From_ object1, To_ object2 ) { return i_add( Entry{object1, object2} ); }
    /// add the entry
    StatusCode i_add( const Entry& entry ) {
      StatusCode sc = m_direct.i_add( entry );
      return m_inverse_aux ? sc.andThen( [&] { return m_inverse_aux->i_add( {entry.to(), entry.from()} ); } ) : sc;
    }
    /// remove the concrete relation between objects (fast,100% inline method)
    StatusCode i_remove( From_ object1, To_ object2 ) {
      StatusCode sc = m_direct.i_remove( object1, object2 );
      return m_inverse_aux ? sc.andThen( [&] { return m_inverse_aux->i_remove( object2, object1 ); } ) : sc;
    }
    /// remove all relations FROM the defined object (fast,100% inline method)
    StatusCode i_removeFrom( From_ object ) {
      StatusCode sc = m_direct.i_removeFrom( object );
      return m_inverse_aux ? sc.andThen( [&] { return m_inverse_aux->i_removeTo( object ); } ) : sc;
    }
    /// remove all relations TO the defined object (fast,100% inline method)
    StatusCode i_removeTo( To_ object ) {
      StatusCode sc = m_direct.i_removeTo( object );
      return m_inverse_aux ? sc.andThen( [&] { return m_inverse_aux->i_removeFrom( object ); } ) : sc;
    }
    /// remove ALL relations form ALL  object to ALL objects (fast,100% inline)
    StatusCode i_clear() {
      StatusCode sc = m_direct.i_clear();
      return m_inverse_aux ? sc.andThen( [&] { return m_inverse_aux->i_clear(); } ) : sc;
    }
    /// rebuild ALL relations form ALL  object to ALL objects (fast,100% inline)
    StatusCode i_rebuild() {
      // 1) get all relations
      Range r = i_relations();
      // 2) copy them into temporary storage
      std::vector<typename IBase::Entry> _e( r.begin(), r.end() );
      // 3) clear all existing relations
      return i_clear()
          .andThen( [&] {
            // 4) reserve space for new relations
            return reserve( _e.size() );
          } )
          .andThen( [&] {
            // 5) build new relations
            for ( const auto& entry : _e ) i_push( entry.from(), entry.to() );
            // (re)sort
            i_sort();
          } );
    }
    /** make the relation between 2 objects (fast,100% inline method)
     *  - Call for i_sort() is mandatory!
     */
    void i_push( From_ object1, To_ object2 ) {
      m_direct.i_push( object1, object2 );
      if ( m_inverse_aux ) { m_inverse_aux->i_push( object2, object1 ); }
    }
    /** (re)sort of the table
     *   mandatory to use after i_push
     */
    void i_sort() {
      m_direct.i_sort();
      if ( m_inverse_aux ) { m_inverse_aux->i_sort(); }
    }

    // merge

    /** merge with the sorted range of relations
     *  @attention the raneg is assumed to be sorted!
     *  @param range the range to be added
     *  @return self-reference
     */
    Relation& merge( const Range& range ) {
      m_direct.merge( range );
      if ( m_inverse_aux ) { m_inverse_aux->imerge( range ); }
      return *this;
    }
    /** merge with the sorted range of relations
     *  @attention the raneg is assumed to be sorted!
     *  @param range the range to be added
     *  @return self-reference
     */
    Relation& imerge( const typename IInverse::Range& range ) {
      m_direct.imerge( range );
      if ( m_inverse_aux ) { m_inverse_aux->merge( range ); }
      return *this;
    }
    /** merge with the sorted range of relations
     *  @attention the raneg is assumed to be sorted!
     *  @param range the range to be added
     *  @return self-reference
     */
    Relation& operator+=( const Range& range ) { return merge( range ); }

    // abstract methods from interface

    /// retrive all relations from the object
    Range relations( From_ object ) const override { return i_relations( object ); }
    /// retrive all relations from ALL objects
    Range relations() const override { return i_relations(); }
    /// make the relation between 2 objects
    StatusCode relate( From_ object1, To_ object2 ) override { return i_relate( object1, object2 ); }
    /// add teh relation
    StatusCode add( const Entry& entry ) override { return i_add( entry ); }
    /// remove the concrete relation between objects
    StatusCode remove( From_ object1, To_ object2 ) override { return i_remove( object1, object2 ); }
    /// remove all relations FROM the defined object
    StatusCode removeFrom( From_ object ) override { return i_removeFrom( object ); }
    /// remove all relations TO the defined object
    StatusCode removeTo( To_ object ) override { return i_removeTo( object ); }
    /// remove ALL relations form ALL  object to ALL objects
    StatusCode clear() override { return i_clear(); }
    /// rebuild ALL relations form ALL  object to ALL objects
    StatusCode rebuild() override { return i_rebuild(); }
    /// update the object after POOL/ROOT reading
    virtual StatusCode update() { return i_rebuild(); }

    /// get the pointer to direct table
    Direct* directBase() { return &m_direct; }
    /// get the reference to direct table
    const Direct& _direct() const { return m_direct; }
    /** set new inverse table
     *  @attention the method is not for public usage !!!
     */
    void setInverseBase( Inverse* inverse ) { m_inverse_aux = inverse; }
    /// reserve the relations (for efficiency reasons)
    StatusCode reserve( const size_t num ) {
      if ( m_inverse_aux ) { m_inverse_aux->i_reserve( num ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ ); }
      return m_direct.i_reserve( num );
    }

  public:
    /// Access the size of the relations
    [[nodiscard]] std::size_t size() const { return m_direct.size(); }

  private:
    /// the holder of all direct relations
    Direct m_direct;
    ///  the pointer to inverse table
    Inverse* m_inverse_aux{nullptr};
  };

} // end of namespace Relations
