/*****************************************************************************\
* (c) Copyright 2020 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include <Kernel/AllocationTracker.h>

using namespace AllocationTracker;

namespace {
  // TODO maybe add some mutexing
  callback_function_ptr s_callback{nullptr};
} // namespace

void AllocationTracker::setCallback( callback_function_ptr callback ) { s_callback = callback; }

callback_function_ptr AllocationTracker::getCallback() { return s_callback; }