/*****************************************************************************\
* (c) Copyright 2020 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once
#include <cstddef>
#include <cstdint>
#include <type_traits>

/** @namespace AllocationTracker
 *
 *  The AllocationTracker namespace contains a collection of tools for
 *  intercepting and monitoring calls to system memory allocation functions.
 *
 *  Calls to memory allocation functions are intercepted by adding a library
 *  to LD_PRELOAD, which can be achieved by passing the --preload argument to
 *  gaudirun.py, for example:
 *    gaudirun.py --preload libAllocationTrackerPreload.so myoptions.py
 *
 *  A second library, libAllocationTracker.so, is linked into both the preload
 *  library and the application. This provides the method
 *    AllocationTracker::setCallback( callback_function_ptr ),
 *  which can be used by the application to register a pointer to a free
 *  function that will be called upon every memory allocation intercepted by
 *  the preloaded library. If libAllocationTrackerPreload.so is not added to
 *  LD_PRELOAD then the registered function will (clearly) never be called.
 *
 *  All calls to the system memory allocation functions are forwarded to their
 *  original implementations, so application behaviour should not be changed by
 *  loading the preload library.
 *
 *  The signature for a callback function is:
 *     void function_name( AllocationTracker::Function system_call,
 *                         std::size_t num_bytes, uint64_t rdtsc,
 *                         malloc_t malloc_ptr, free_t free_t )
 *  Where:
 *   - `AllocationTracker::Function system_call` is an enum type indicating
 *     which system call was intercepted (malloc, calloc, ...)
 *   - `std::size_t num_bytes` gives the number of bytes that were allocated
 *   - `uint64_t rdtsc` gives the number of ticks of the rdtsc counter that
 *     were spent in the [forwarded] call to the system allocation function.
 *   - `malloc_t malloc_ptr` is a pointer to the system malloc()
 *   - `free_t free_t` is a pointer to the system free()
 *
 *  Pointers to the system malloc() and free() are provided because the
 *  callback may need to dynamically allocate (for example, if it wants to
 *  record non-trivial statistics that require dynamic data structures), but
 *  it cannot easily use `new`, `delete`, `std::malloc` and so on, as those
 *  would lead to the callback being called recursively. Rather than requiring
 *  all callbacks to implement logic to cope with this, the provided pointers
 *  can be used instead. For STL containers, the provided allocator
 *    AllocationTracker::allocator_with_ptrs<T>
 *  can be used.
 *
 *  If the tools are used in a multithreaded application then the provided
 *  callback must be thread-safe.
 *
 *  An example of registering a callback can be seen in HLTControlFlowMgr,
 *  which uses the IAllocationTracker interface to abstract away the choice
 *  of callback function between TimingAllocationTracker and
 *  StacktraceAllocationTracker.
 *
 *  @author Olli Lupton
 *  @date   06/04/2020
 */
namespace AllocationTracker {
  /** Signal which allocation function was used
   */
  enum Function {
    MALLOC = 0,
    CALLOC,
    REALLOC,
    POSIX_MEMALIGN,
    MEMALIGN,
    ALIGNED_ALLOC,
    VALLOC,
    PVALLOC,
    NUM_ALLOCATION_FUNCS,
    FREE /* implementation detail */
  };

  /** Pointer to a function with the signature of malloc()
   */
  using malloc_t = void* (*)( std::size_t );

  /** Pointer to a function with the signature of free()
   */
  using free_t = void ( * )( void* );

  /** Pointer to a function that is called for each dynamic allocation.
   *  The last two arguments give malloc() and free() functions that will *not* trigger recursive calls to the callback.
   */
  using callback_function_ptr = void ( * )( Function, std::size_t, uint64_t, malloc_t, free_t );

  /** Update the function pointer that will be called when an allocation is intercepted.
   */
  void setCallback( callback_function_ptr );

  /** Get the function pointer that should be called when an allocation is intercepted
   */
  callback_function_ptr getCallback();

  /** Custom allocator that holds pointers to implementations of malloc() and free().
   *  This is useful when overriding malloc() and free(), as it can be used with pointers to the system-provided (i.e.
   *  original) malloc() and free() inside the overriden versions to avoid infinite recursion...
   */
  template <typename T = void>
  struct allocator_with_ptrs {
    using value_type = T;
    using malloc_t   = void* (*)( std::size_t );
    using free_t     = void ( * )( void* );
    allocator_with_ptrs( malloc_t malloc_ptr, free_t free_ptr ) noexcept : m_malloc{malloc_ptr}, m_free{free_ptr} {}

    template <typename U>
    allocator_with_ptrs( allocator_with_ptrs<U> const& other ) noexcept
        : m_malloc{other.m_malloc}, m_free{other.m_free} {}

    value_type* allocate( std::size_t n ) { return static_cast<value_type*>( m_malloc( n * sizeof( value_type ) ) ); }

    void deallocate( value_type* p, std::size_t ) noexcept { m_free( p ); }

    using propagate_on_container_copy_assignment = std::true_type;
    using propagate_on_container_move_assignment = std::true_type;
    using propagate_on_container_swap            = std::true_type;

    template <typename U>
    friend bool operator==( allocator_with_ptrs const& lhs, allocator_with_ptrs<U> const& rhs ) noexcept {
      return ( lhs.m_malloc == rhs.m_malloc ) && ( lhs.m_free == rhs.m_free );
    }

  private:
    template <typename>
    friend struct allocator_with_ptrs;

    malloc_t m_malloc{nullptr};
    free_t   m_free{nullptr};
  };

  template <typename T, typename U>
  bool operator!=( allocator_with_ptrs<T> const& lhs, allocator_with_ptrs<U> const& rhs ) noexcept {
    return !( lhs == rhs );
  }
} // namespace AllocationTracker