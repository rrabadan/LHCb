###############################################################################
# (c) Copyright 2000-2021 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
#[=======================================================================[.rst:
Kernel/XMLSummaryKernel
-----------------------
#]=======================================================================]

gaudi_add_module(XMLSummaryKernel
    SOURCES
        src/XMLSummarySvc.cpp
    LINK
        Python::Python
        Gaudi::GaudiKernel
        Gaudi::GaudiUtilsLib
)

gaudi_add_module(XMLSummaryKernelTests
    SOURCES
        tests/src/ThrowingCounter.cpp
        tests/src/ExitingCounter.cpp
    LINK
        LHCb::LHCbAlgsLib
)

gaudi_install(PYTHON)
gaudi_generate_confuserdb()

gaudi_add_tests(QMTest)
set_property(
    TEST
        XMLSummaryKernel.exception
        XMLSummaryKernel.exit
        XMLSummaryKernel.readerror
        XMLSummaryKernel.writedst
        XMLSummaryKernel.writeerror
    APPEND PROPERTY 
        ENVIRONMENT
            "XMLSUMMARYKERNELROOT=${CMAKE_CURRENT_SOURCE_DIR}"
)
