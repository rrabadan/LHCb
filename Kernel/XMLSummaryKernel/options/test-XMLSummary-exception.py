###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
# Test, will read and write a Dst file
# Will fill some counters, and output the summary.xml
# This should throw an exception after 3 events
# These options are mostly copied from $GAUDIEXAMPLESROOT/python/CounterEx.py
from Gaudi.Configuration import *
from XMLSummaryKernel.Configuration import XMLSummary
from Configurables import ThrowingCounter

importOptions("$XMLSUMMARYKERNELROOT/options/test-XMLSummary.py")
XMLSummary().XMLSummary = "summary-exception.xml"
ApplicationMgr().TopAlg += [ThrowingCounter(name="Counter")]
