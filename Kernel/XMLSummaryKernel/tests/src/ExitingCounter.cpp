/*****************************************************************************\
* (c) Copyright 2023 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include <LHCbAlgs/Consumer.h>

struct ExitingCounter : LHCb::Algorithm::Consumer<void(), Gaudi::Functional::Traits::useLegacyGaudiAlgorithm> {
  using Consumer::Consumer;
  void operator()() const override {
    auto& executedCounter = counter( "executed" );
    ++executedCounter;
    if ( executedCounter.nEntries() > 3 ) { exit( 0 ); }
  }
};

DECLARE_COMPONENT( ExitingCounter )
