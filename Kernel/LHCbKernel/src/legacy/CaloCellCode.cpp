/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#include "Detector/Calo/CaloCellCode.h"
#include "Detector/Calo/CaloCellID.h"

#include <limits>
#include <type_traits>

namespace {

  /** @var AreaName
   *  The array of calorimeter area names
   *  @author Vanya BELYAEV Ivan.Belyaev@nikhef.nl
   *  @date 2009-09-28
   */
  static const std::array<std::string, LHCb::Detector::Calo::CellCode::CaloAreaNums> s_AreaName = {
      {"Outer", "Middle", "Inner", "PinArea"}};

  /// get the area index from name
  inline int _area( std::string_view area ) {
    auto it = std::find( s_AreaName.begin(), s_AreaName.end(), area );
    return it == s_AreaName.end() ? -1 : std::distance( s_AreaName.begin(), it );
  }

} // namespace

namespace {
  using _Type1 = LHCb::Detector::Calo::CellCode::ContentType;
  using _Type2 = unsigned int;
} // namespace

static_assert( std::numeric_limits<_Type1>::is_specialized && std::is_integral<_Type1>::value &&
                   std::numeric_limits<_Type1>::is_integer && std::numeric_limits<_Type1>::digits == 32u &&
                   !std::is_signed<_Type1>::value,
               "_Type1 not valid" );

static_assert( std::numeric_limits<_Type2>::is_specialized && std::is_integral<_Type2>::value &&
                   std::numeric_limits<_Type1>::is_integer && std::numeric_limits<_Type2>::digits == 32u &&
                   !std::is_signed<_Type1>::value,
               "_Type2 not valid" );

static_assert( LHCb::Detector::Calo::CellCode::BitsTotal == 32, "32 bits" );

static_assert( LHCb::Detector::Calo::CellCode::SpdCalo < LHCb::Detector::Calo::CellCode::CaloNums &&
                   LHCb::Detector::Calo::CellCode::PrsCalo < LHCb::Detector::Calo::CellCode::CaloNums &&
                   LHCb::Detector::Calo::CellCode::EcalCalo < LHCb::Detector::Calo::CellCode::CaloNums &&
                   LHCb::Detector::Calo::CellCode::HcalCalo < LHCb::Detector::Calo::CellCode::CaloNums,
               "Det < CaloNums" );

static_assert( LHCb::Detector::Calo::CellCode::Outer < LHCb::Detector::Calo::CellCode::CaloAreaNums &&
                   LHCb::Detector::Calo::CellCode::Middle < LHCb::Detector::Calo::CellCode::CaloAreaNums &&
                   LHCb::Detector::Calo::CellCode::Inner < LHCb::Detector::Calo::CellCode::CaloAreaNums,
               "Region < AreaNums" );

static_assert( (unsigned int)LHCb::Detector::Calo::CellCode::PinArea <
                   (unsigned int)LHCb::Detector::Calo::CellCode::CaloAreaNums,
               "PinArea < CaloAreaNums" );

namespace LHCb::Detector::Calo::CellCode {

  /* get the area name from calorimeter index and number
   * @attention function make heavy use of hadcoded structure of Calorimeter!
   * @warning   different convention for Hcal
   * @param  calo (INPUT) calorimeter index
   * @param  area (INPUT) area index
   * @return name for the area
   * @author Vanya BELYAEV Ivan.Belyaev@nikhef.nl
   * @date 2009-09-28
   */
  const std::string& caloArea( const int calo, const int area ) {
    switch ( calo ) {
    case SpdCalo:
    case PrsCalo:
      switch ( area ) {
      case 0:
        return s_AreaName[Outer]; // Outer
      case 1:
        return s_AreaName[Middle]; // Middle
      case 2:
        return s_AreaName[Inner]; // Inner
      default:
        return s_BadName;
      }
    case EcalCalo:
      switch ( area ) {
      case 0:
        return s_AreaName[Outer]; // Outer
      case 1:
        return s_AreaName[Middle]; // Middle
      case 2:
        return s_AreaName[Inner]; // Inner
      case 3:
        return s_AreaName[PinArea]; // Pin-Area
      default:
        return s_BadName;
      }
    case HcalCalo:
      switch ( area ) {
      case 0:
        return s_AreaName[Outer]; // Outer
      case 1:
        return s_AreaName[Inner]; // Middle, return as Inner ! ATTENTION!
      case 3:
        return s_AreaName[PinArea]; // Pin-Area
      default:
        return s_BadName;
      }
    default:
      return s_BadName;
    }
  }

  /*  get the area index from calorimeter index and name
   *  @attention function make heavy use of hadcoded structure of Calorimeter!
   *  @warning   the different convention for Hcal
   *  @param  calo (INPUT) calorimeter index
   *  @param  area (INPUT) area name
   *  @return indx for the area
   *  @author Vanya BELYAEV Ivan.Belyaev@nikhef.nl
   *  @date 2009-09-28
   */
  int caloArea( const int calo, std::string_view area ) {
    //
    switch ( calo ) {
    case HcalCalo:
      switch ( _area( area ) ) {
      case 0:
        return Outer; // 0
      case 1:
        return -1; // ATTENTION!!!
      case 2:
        return Middle; // 1        // ATTENTION!!!
      case 3:
        return PinArea; // 3
      default:
        return -1;
      }
    case EcalCalo:
      return _area( area );
    case SpdCalo:
    case PrsCalo:
      switch ( _area( area ) ) {
      case 0:
        return Outer; // 0
      case 1:
        return Middle; // 1
      case 2:
        return Inner; // 2
      default:
        return -1;
      }
    default:
      return -1;
    }
  }

  /*  Is the given area is Pin-diod area ?
   *  @attention It must be coherent with caloArea
   *  @see caloArea
   *  @param calo (INPUT) calorimeetr index
   *  @param area (INPUT) calorimeter index
   *  @reutrn true if the area is Pin-diod area
   *  @author Vanya BELYAEV Ivan.Belyaev@nikhef.nl
   *  @date 2009-09-28
   */
  bool isPinArea( Index calo, const int area ) {
    return (int)PinArea == area && ( Index::EcalCalo == calo || Index::HcalCalo == calo );
  }
  std::string toString( const Index& i ) { return caloName( i ); }
  StatusCode  parse( Index& i, const std::string& s ) {
    i = caloNum( s );
    return StatusCode{i != Index::Undefined};
  }

} // namespace LHCb::Detector::Calo::CellCode
