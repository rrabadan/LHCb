/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "Kernel/HitPattern.h"
#include "Detector/FT/FTChannelID.h"

namespace {

  template <size_t N>
  int numholes( const LHCb::HitPattern::pattern_type<N>& pattern ) {
    bool   active                = false;
    size_t nummissingsincelastup = 0;
    size_t rc{0};
    for ( size_t n = 0; n < N; ++n ) {
      if ( pattern[n] ) {
        rc += nummissingsincelastup;
        nummissingsincelastup = 0;
        active                = true;
      } else if ( active )
        nummissingsincelastup += 1;
    }
    return rc;
  }
} // namespace

namespace LHCb {

  HitPattern::HitPattern( span<const LHCbID> ids ) {
    for ( const auto id : ids ) add( id );
  }

  template <size_t N>
  std::ostream& operator<<( std::ostream& os, const LHCb::HitPattern::pattern_type<N>& pattern ) {
    for ( size_t n = 0; n < N; ++n ) os << int( pattern[n] ); // works only up to 9 but that should be plenty
    return os;
  }

  std::ostream& HitPattern::fillStream( std::ostream& s ) const {
    s << "veloA:    " << m_veloA << std::endl
      << "veloC:    " << m_veloC << std::endl
      << "UT:       " << m_ut << std::endl
      << "FT:       " << m_ft << std::endl
      << "Muon:     " << m_muon << std::endl;
    return s;
  }

  namespace {
    /// Count the total number of missing bits in between the first and last active bits

  } // namespace

  size_t HitPattern::numVeloHoles() const { return numholes( veloA() ) + numholes( veloC() ); }

  size_t HitPattern::numFTHoles() const { return numholes( ft() ); }

  HitPattern::VPPattern HitPattern::velo() const {
    VPPattern rc = m_veloA;
    for ( size_t i = 0; i < m_veloA.size(); ++i ) rc[i] += m_veloC[i];
    return rc;
  }

} // namespace LHCb
