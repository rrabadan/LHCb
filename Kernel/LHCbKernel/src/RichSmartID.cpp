/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

//-----------------------------------------------------------------------------
/** @file RichSmartID.cpp
 *
 *  Implementation file for RICH Channel ID class : RichSmartID
 *
 *  @author  Chris Jones  Christopher.Rob.Jones@cern.ch
 *  @date    2005-01-06
 */
//-----------------------------------------------------------------------------

// STL
#include <iostream>
#include <sstream>

// local
#include "Kernel/RichSmartID.h"

// Gaudi
#include "GaudiKernel/GaudiException.h"

std::ostream& LHCb::RichSmartID::dumpBits( std::ostream& s ) const {
  for ( auto iCol = 0u; iCol < NBits; ++iCol ) { s << isBitOn( iCol ); }
  return s;
}

std::ostream& LHCb::RichSmartID::fillStream( std::ostream& s ) const {

  s << "{";

  // Dump the bits if requested
  if ( getenv( "RICHSMARTID_DUMP_BITS" ) ) {
    s << " ";
    dumpBits( s );
  }

  // PD type
  const auto PMT = isPMT();
  const auto HPD = isHPD();

  // Type
  s << ( HPD ? " HPD" : PMT ? " PMT" : "UndefinedPD" );

  // if PMT add size (large or small)
  if ( PMT && pdIsSet() ) { s << ( isLargePMT() ? ":h" : ":r" ); }

  // Is this smart ID valid
  if ( isValid() ) {

    // RICH detector
    if ( richIsSet() ) { s << " " << Rich::text( rich() ); }

    // Panel
    if ( panelIsSet() ) {
      s << ( rich() == Rich::Rich1 ? ( panel() == Rich::top ? " Top" : " Bot" )
                                   : ( panel() == Rich::left ? " L-A" : " R-C" ) );
    }

    // PD
    if ( pdIsSet() ) {
      s << ( PMT ? " PD[Mod,NInMod]:" : " PD[Col,NInCol]:" ) //
        << std::setfill( '0' ) << std::setw( PMT ? 3 : 2 ) << pdMod();
      s << "," << std::setfill( '0' ) << std::setw( 2 ) << pdNumInMod();
      // Include PMT derived info
      if ( PMT ) {
        s << " Mod[Col,NInCol]:" << std::setfill( '0' ) << std::setw( 2 ) << panelLocalModuleColumn();
        s << "," << std::setfill( '0' ) << std::setw( 2 ) << columnLocalModuleNum();
        s << " PD[EC,NInEC]:" << format( "%1i", elementaryCell() );
        s << "," << format( "%1i", pdNumInEC() );
      }
    }

    // Pixel info
    const auto pixColSet = pixelColIsSet();
    const auto pixRowSet = pixelRowIsSet();
    if ( pixColSet || pixRowSet ) {
      if ( pixColSet && pixRowSet ) {
        s << " Pix[Col,Row]:" << std::setfill( '0' ) << std::setw( PMT ? 1 : 2 ) << pixelCol() << "," << pixelRow();
      } else {
        const int fSPix = ( PMT ? 2 : 3 );
        if ( pixColSet ) { s << std::setfill( '0' ) << std::setw( fSPix ) << " pixCol" << pixelCol(); }
        if ( pixRowSet ) { s << std::setfill( '0' ) << std::setw( fSPix ) << " pixRow" << pixelRow(); }
      }
      // Include PMT derived info
      if ( PMT && pixColSet && pixRowSet ) { s << " Anode:" << std::setfill( '0' ) << std::setw( 2 ) << anodeIndex(); }
    }

    // Subpixel
    if ( pixelSubRowIsSet() ) { s << " pixSubRow:" << format( "%1i", pixelSubRow() ); }

    // ADC time
    if ( adcTimeIsSet() ) { s << " ADCTime:" << std::setfill( '0' ) << std::setw( 5 ) << adcTime(); }

  } else {
    // This SmartID has no valid bits set. This is bad ...
    s << " WARNING Invalid RichSmartID";
  }

  // end
  s << " }";

  return s;
}

void LHCb::RichSmartID::rangeError( const DataType   value,    //
                                    const DataType   maxValue, //
                                    std::string_view message ) const {
  std::ostringstream mess;
  mess << message << " value " << value << " exceeds field maximum " << maxValue;
  throw GaudiException( mess.str(), "*RichSmartID*", StatusCode::FAILURE );
}

std::string LHCb::RichSmartID::toString() const {
  std::ostringstream text;
  text << *this;
  return text.str();
}
