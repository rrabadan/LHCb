/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "Kernel/UTNames.h"
#include "Detector/UT/ChannelID.h"
#include <charconv>
#include <iostream>
#include <sstream>
#include <string>

std::string LHCb::UTNames::UniqueSectorToString( const LHCb::Detector::UT::ChannelID& chan ) {
  return UniqueModuleToString( chan ) + SectorToString( chan );
}

std::string LHCb::UTNames::SectorToString( const LHCb::Detector::UT::ChannelID& chan ) {
  return "Sector" + std::to_string( chan.sector() );
}

std::string LHCb::UTNames::UniqueModuleToString( const LHCb::Detector::UT::ChannelID& chan ) {
  return UniqueFaceToString( chan ) + ModuleToString( chan );
}

std::string LHCb::UTNames::ModuleToString( const LHCb::Detector::UT::ChannelID& chan ) {
  return "Module" + std::to_string( chan.module() );
}

std::vector<std::string> LHCb::UTNames::sides() {
  std::vector<std::string> sides;
  sides.reserve( s_SideTypMap().size() );
  for ( const auto& i : s_SideTypMap() ) { sides.emplace_back( i.first ); }
  return sides;
}

std::vector<std::string> LHCb::UTNames::faces() {
  std::vector<std::string> faces;
  faces.reserve( s_FaceTypMap().size() );
  for ( const auto& i : s_FaceTypMap() ) {
    if ( i.first != "UnknownRegion" ) faces.emplace_back( i.first );
  }
  return faces;
}

const std::vector<std::string>& LHCb::UTNames::layers() {
  // messy
  static const std::vector<std::string> halflayers = {"aX", "aU", "bV", "bX"};
  return halflayers;
}

std::vector<std::string> LHCb::UTNames::allSides() { return sides(); }

std::vector<std::string> LHCb::UTNames::allLayers() { return layers(); }

std::string LHCb::UTNames::UniqueLayerToString( unsigned int layer ) {
  std::ostringstream oss;
  oss << LHCb::UTNames::Layer( layer );
  return oss.str();
}

std::string LHCb::UTNames::UniqueLayerToString( unsigned int layer, unsigned int side ) {
  std::ostringstream oss;
  oss << LHCb::UTNames::Layer( layer );
  return ToString( static_cast<Side>( side ) ) + oss.str();
}

std::string LHCb::UTNames::UniqueStaveToString( const LHCb::Detector::UT::ChannelID& chan ) {
  return UniqueLayerToString( chan ) + "Stave" + std::to_string( chan.stave() );
}

std::string LHCb::UTNames::UniqueFaceToString( const LHCb::Detector::UT::ChannelID& chan ) {
  return UniqueStaveToString( chan ) + ToString( static_cast<Face>( chan.face() ) );
}

std::string LHCb::UTNames::channelToString( const LHCb::Detector::UT::ChannelID& chan ) {
  return UniqueSectorToString( chan ) + "Strip" + std::to_string( chan.strip() );
}

LHCb::Detector::UT::ChannelID LHCb::UTNames::stringToChannel( std::string_view name ) {

  // convert string to channel

  // get the station, layer and box
  const std::vector<std::string> thesides = sides();
  const unsigned int             side     = findSideType( name, thesides );
  const std::vector<std::string> thefaces = faces();
  const unsigned int             face     = findFaceType( name, thefaces );

  unsigned int layer = 0;
  if ( name.find( "aX" ) != std::string_view::npos ) {
    layer = 0;
  } else if ( name.find( "aU" ) != std::string_view::npos ) {
    layer = 1;
  } else if ( name.find( "bV" ) != std::string_view::npos ) {
    layer = 2;
  } else if ( name.find( "bX" ) != std::string_view::npos ) {
    layer = 3;
  } else {
    throw std::runtime_error{"Wrong LayerString in LHCb::UTNames::stringToChannel"};
  }

  auto startStave  = name.find( "Stave" );
  auto startModule = name.find( "Module" );
  auto startSector = name.find( "Sector" );
  auto startStrip  = name.find( "Strip" );

  // name could finish with Sector, not strip
  if ( ( startStave == std::string_view::npos ) || ( startModule == std::string_view::npos ) ||
       startSector == std::string_view::npos )
    throw std::runtime_error{"Wrong Stave/Module/Sector in LHCb::UTNames::stringToChannel"};

  unsigned int strip, sector, module, stave;
  stave  = toInt( name.substr( startStave + 5, startStave + 6 ) );
  module = toInt( name.substr( startModule + 6, startModule + 7 ) );
  if ( startStrip == std::string_view::npos ) { // no strip in name
    sector = toInt( name.substr( startSector + 6 ) );
    strip  = 0;
  } else {
    sector = toInt( name.substr( startSector + 6, startSector + 7 ) );
    strip  = toInt( name.substr( startStrip + 5 ) );
  }

  return Detector::UT::ChannelID( LHCb::Detector::UT::ChannelID::detType::typeUT, side, layer, stave, face, module,
                                  sector, strip );
}

unsigned int LHCb::UTNames::findSideType( std::string_view testname, const std::vector<std::string>& names ) {
  auto n = std::find_if( std::begin( names ), std::end( names ),
                         [&]( std::string_view s ) { return testname.find( s ) != std::string_view::npos; } );
  if ( n != std::end( names ) ) {
    return static_cast<unsigned int>( SideToType( *n ) );
  } else {
    throw std::runtime_error{"Wrong name for UT Side (Should be UTA/C"};
  }
}

unsigned int LHCb::UTNames::findFaceType( std::string_view testname, const std::vector<std::string>& names ) {
  auto n = std::find_if( std::begin( names ), std::end( names ),
                         [&]( std::string_view s ) { return testname.find( s ) != std::string_view::npos; } );
  if ( n != std::end( names ) ) {
    return static_cast<unsigned int>( FaceToType( *n ) );
  } else {
    throw std::runtime_error{"Wrong name for UT Face (Should be Front/Back"};
  }
}

unsigned int LHCb::UTNames::toInt( std::string_view str ) {
  unsigned int outValue = 0;
  auto [_, ec]          = std::from_chars( str.begin(), str.end(), outValue );
  if ( ec != std::errc{} ) {
    std::cerr << "ERROR " << make_error_code( ec ).message() << "** " << str << " **" << std::endl;
  }
  return outValue;
}

LHCb::UTNames::Side LHCb::UTNames::SideToType( std::string_view aName ) {
  auto iter = s_SideTypMap().find( aName );
  if ( iter == s_SideTypMap().end() )
    throw std::runtime_error{"Wrong name for UT Side (Should be UTA/C"};
  else
    return iter->second;
}

const std::string& LHCb::UTNames::ToString( Station aEnum ) {
  static const std::string s_UnknownStation = "UnknownStation";
  auto                     iter             = std::find_if( s_StationTypMap().begin(), s_StationTypMap().end(),
                            [&]( const auto& i ) { return i.second == aEnum; } );
  return iter != s_StationTypMap().end() ? iter->first : s_UnknownStation;
}

LHCb::UTNames::detRegion LHCb::UTNames::detRegionToType( std::string_view aName ) {
  auto iter = s_detRegionTypMap().find( aName );
  return iter != s_detRegionTypMap().end() ? iter->second : LHCb::UTNames::detRegion::UnknownRegion;
}

const std::string& LHCb::UTNames::ToString( detRegion aEnum ) {
  static const std::string s_UnknownRegion = "UnknownRegion";
  auto                     iter            = std::find_if( s_detRegionTypMap().begin(), s_detRegionTypMap().end(),
                            [&]( const std::pair<const std::string, detRegion>& i ) { return i.second == aEnum; } );
  return iter != s_detRegionTypMap().end() ? iter->first : s_UnknownRegion;
}

const std::string& LHCb::UTNames::ToString( Side aEnum ) {
  static const std::string s_UnknownSide = "UnknownSide";
  auto                     iter =
      std::find_if( s_SideTypMap().begin(), s_SideTypMap().end(), [&]( const auto& i ) { return i.second == aEnum; } );
  return iter != s_SideTypMap().end() ? iter->first : s_UnknownSide;
}

inline const std::string& LHCb::UTNames::ToString( Layer aEnum ) {
  static const std::string s_UnknownRegion = "UnknownLayer";
  auto                     iter            = std::find_if( s_LayerTypMap().begin(), s_LayerTypMap().end(),
                            [&]( const std::pair<const std::string, Layer>& i ) { return i.second == aEnum; } );
  return iter != s_LayerTypMap().end() ? iter->first : s_UnknownRegion;
}

inline LHCb::UTNames::Face LHCb::UTNames::FaceToType( std::string_view aName ) {
  auto iter = s_FaceTypMap().find( aName );
  if ( iter == s_FaceTypMap().end() ) throw std::runtime_error( "Wrong name of UTFace in UTNames" );
  return iter->second;
}

inline const std::string& LHCb::UTNames::ToString( Face aEnum ) {
  static const std::string s_UnknownRegion = "UnknownFace";
  auto                     iter            = std::find_if( s_FaceTypMap().begin(), s_FaceTypMap().end(),
                            [&]( const std::pair<const std::string, Face>& i ) { return i.second == aEnum; } );
  return iter != s_FaceTypMap().end() ? iter->first : s_UnknownRegion;
}

inline LHCb::UTNames::Layer LHCb::UTNames::LayerToType( std::string_view aName ) {
  auto iter = s_LayerTypMap().find( aName );
  if ( iter == s_LayerTypMap().end() ) throw std::runtime_error( "Wrong name of UTLayer in UTNames" );
  return iter->second;
}
