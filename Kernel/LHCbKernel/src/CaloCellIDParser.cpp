/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#include "Kernel/CaloCellIDParser.h"

#include "GaudiKernel/ParsersFactory.h"
#include "GaudiKernel/ToStream.h"

#include <iomanip>
#include <sstream>

/** Implementation of streaming&parsing function for class LHCb::Detecto::Calo::CellID
 *  @author Vanya BELYAEV Ivan.Belyaev@nikhef.nl
 *  @date 2009-09-29
 */
namespace Gaudi::Parsers {

  /**
   *  @author Vanya BELYAEV Ivan.Belyaev@nikhef.nl
   *  @author alexander.mazurov@gmail.com
   *  @date 2011-08-29
   */
  template <typename Iterator, typename Skipper>
  class CCIDGrammar : public qi::grammar<Iterator, LHCb::Detector::Calo::CellID(), Skipper> {
  public:
    /// the actual type of parsed result
    using ResultT  = LHCb::Detector::Calo::CellID; // the actual type of parsed result
    using ContentT = unsigned int;

    struct tag_calo {};
    struct tag_area {};
    struct tag_row {};
    struct tag_col {};
    struct Operations {
      void operator()( LHCb::Detector::Calo::CellID& val, const ContentT v ) const { val.setAll( v ); }
      void match( LHCb::Detector::Calo::CellID& val, const unsigned short Value, const unsigned int Shift,
                  const unsigned int Mask ) const {
        ContentT tmp = val.all();
                 operator()( val, LHCb::Detector::Calo::CellCode::setField( tmp, Value, Shift, Mask ) );
      }

      void operator()( LHCb::Detector::Calo::CellID& val, unsigned short v, tag_calo ) const {
        val.setCalo( static_cast<LHCb::Detector::Calo::CellCode::Index>( v ) );
      }
      void operator()( LHCb::Detector::Calo::CellID& val, const std::string& v, tag_calo ) const {
        val.setCalo( LHCb::Detector::Calo::CellCode::caloNum( v ) );
      }
      void operator()( LHCb::Detector::Calo::CellID& val, unsigned short v, tag_area ) const {
        match( val, v, LHCb::Detector::Calo::CellCode::ShiftArea, LHCb::Detector::Calo::CellCode::MaskArea );
      }
      void operator()( LHCb::Detector::Calo::CellID& val, const std::string& v, tag_area ) const {
        operator()( val, LHCb::Detector::Calo::CellCode::caloArea( val.calo(), v ), tag_area() );
      }

      void operator()( LHCb::Detector::Calo::CellID& val, unsigned short v, tag_row ) const {
        match( val, v, LHCb::Detector::Calo::CellCode::ShiftRow, LHCb::Detector::Calo::CellCode::MaskRow );
      }

      void operator()( LHCb::Detector::Calo::CellID& val, unsigned short v, tag_col ) const {
        match( val, v, LHCb::Detector::Calo::CellCode::ShiftCol, LHCb::Detector::Calo::CellCode::MaskCol );
      }
    };

    CCIDGrammar() : CCIDGrammar::base_type( result ) {
      max_limit %= qi::int_[qi::_a = qi::_1] >> qi::eps( qi::_a <= qi::_r1 && qi::_a >= 0 );
      result =
          ( -( qi::lit( "LHCb.CaloCellID" ) | qi::lit( "CaloCellID" ) ) >> qi::lit( '(' ) >>
            ( max_limit( 4 )[op( qi::_val, qi::_1, tag_calo() )] | calo[op( qi::_val, qi::_1, tag_calo() )] ) >> ',' >>
            ( max_limit( 4 )[op( qi::_val, qi::_1, tag_area() )] | area[op( qi::_val, qi::_1, tag_area() )] ) >> ',' >>
            max_limit( 64 )[op( qi::_val, qi::_1, tag_row() )] >> ',' >>
            max_limit( 64 )[op( qi::_val, qi::_1, tag_col() )] >> qi::lit( ')' ) ) |
          ( qi::lit( '(' ) >> qi::int_[op( qi::_val, qi::_1 )] >> qi::lit( ')' ) ) | qi::int_[op( qi::_val, qi::_1 )];
    }
    StringGrammar<Iterator, Skipper>                            area, calo;
    qi::rule<Iterator, LHCb::Detector::Calo::CellID(), Skipper> result;
    qi::rule<Iterator, int( int ), qi::locals<int>, Skipper>    max_limit;
    ph::function<Operations>                                    op;
  };
  REGISTER_GRAMMAR( LHCb::Detector::Calo::CellID, CCIDGrammar );

  /*  parse cellID from the string
   *  @param result (OUPUT) the parsed cellID
   *  @param input  (INPUT) the input string
   *  @return status code
   *  @author Vanya BELYAEV Ivan.Belyaev@nikhef.nl
   *  @date 2009-09-29
   */
  StatusCode parse( LHCb::Detector::Calo::CellID& result, const std::string& input ) { return parse_( result, input ); }

} // namespace Gaudi::Parsers
