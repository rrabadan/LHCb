/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "Kernel/LHCbID.h"

//-----------------------------------------------------------------------------
// Implementation file for class : LHCbID
//
// 2007-07-11 : Chris Jones
//-----------------------------------------------------------------------------

std::ostream& LHCb::LHCbID::fillStream( std::ostream& s ) const {
  s << "{ LHCbID : ";
  if ( !isDefined() ) {
    s << "UNDEFINED " << lhcbID();
  } else if ( isVP() ) {
    s << vpID();
  } else if ( isUT() ) {
    s << utID();
  } else if ( isRich() ) {
    s << richID();
  } else if ( isCalo() ) {
    s << caloID();
  } else if ( isMuon() ) {
    s << muonID();
  } else if ( isFT() ) {
    s << ftID();
  }
  return s << " }";
}
