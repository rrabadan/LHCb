/*****************************************************************************\
* (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once
#include "Detector/UT/ChannelID.h"
#include "boost/container/flat_map.hpp"
#include <algorithm>
#include <ostream>
#include <vector>

namespace LHCb {

  // Forward declarations

  /** @class UTNames UTNames.h
   * @author Jianchun Wang
   *
   */

  class UTNames final {
  public:
    /// Station names
    enum struct Station { UnknownStation = 0, UTa = 1, UTb = 2 };
    /// Region names
    enum struct detRegion { UnknownRegion = 0, RegionC = 1, RegionB = 2, RegionA = 3 };
    /// Side names
    enum struct Side { UTC = 0, UTA = 1 };
    /// Layer namesi (including C/A sides)
    enum struct Layer { aX = 0, aU = 1, bV = 2, bX = 3 };
    /// face names
    enum struct Face { Back = 0, Front = 1 };

    /// FIXME: No constructor needed, as all methods are static -- so change this t0 =delete
    UTNames() = default;

    /// conversion to string for enum type Station
    static const std::string& ToString( Station aEnum );

    /// conversion to string for enum type Side
    static const std::string& ToString( Side aEnum );

    /// conversion to string for enum type detRegion
    static const std::string& ToString( detRegion aEnum );

    /// conversion to string for enum type HalfLayer
    static const std::string& ToString( Layer aEnum );

    /// conversion to string for enum type Face
    static const std::string& ToString( Face aEnum );

    /// conversion of string to enum for type Side
    static LHCb::UTNames::Side SideToType( std::string_view aName );

    /// conversion of string to enum for type detRegion
    static LHCb::UTNames::detRegion detRegionToType( std::string_view aName );

    /// conversion of string to enum for type HalfLayer
    static LHCb::UTNames::Layer LayerToType( std::string_view aName );

    /// conversion of string to enum for type Face
    static LHCb::UTNames::Face FaceToType( std::string_view aName );

    /// detector name
    static std::string detector() { return "UT"; }

    /// station string from id
    static std::string StationToString( const LHCb::Detector::UT::ChannelID& chan ) {
      return ToString( static_cast<Station>( ( chan.layer() / 2 + 1 ) ) );
    }

    /// side string from id
    static std::string SideToString( const LHCb::Detector::UT::ChannelID& chan ) {
      return ToString( static_cast<Side>( chan.side() ) );
    }

    /// channel to string from id
    static std::string channelToString( const LHCb::Detector::UT::ChannelID& chan );

    /// unique box string from id
    static std::string UniqueRegionToString( LHCb::Detector::UT::ChannelID chan ) {
      return UniqueLayerToString( chan ) + ToString( static_cast<detRegion>( chan.detRegion() ) );
    }

    /// unique layer string from layer
    static std::string UniqueLayerToString( unsigned int layer );

    /// unique layer string from layer and side
    static std::string UniqueLayerToString( unsigned int layer, unsigned int side );

    /// unique layer string from id
    static std::string UniqueLayerToString( const LHCb::Detector::UT::ChannelID& chan ) {
      return UniqueLayerToString( chan.layer(), chan.side() );
    }

    /// unique sector string from id
    static std::string UniqueSectorToString( const LHCb::Detector::UT::ChannelID& chan );

    /// sector string from id
    static std::string SectorToString( const LHCb::Detector::UT::ChannelID& chan );

    /// unique module string from id
    static std::string UniqueModuleToString( const LHCb::Detector::UT::ChannelID& chan );

    /// module string from id
    static std::string ModuleToString( const LHCb::Detector::UT::ChannelID& chan );

    /// stave string from id
    static std::string UniqueStaveToString( const LHCb::Detector::UT::ChannelID& chan );

    /// face string from id
    static std::string UniqueFaceToString( const LHCb::Detector::UT::ChannelID& chan );

    /// vector of string names for all stations
    static std::vector<std::string> allSides();

    /// vector of string names for all layers
    static std::vector<std::string> allLayers();

    /// vector of string names for all regions
    static std::vector<std::string> allDetRegions();

    /// vector of string names for sides
    static std::vector<std::string> sides();

    /// vector of string names for faces
    static std::vector<std::string> faces();

    /// vector of string names for layers
    static const std::vector<std::string>& layers();

    /// convert string to channel
    static Detector::UT::ChannelID stringToChannel( std::string_view name );

  private:
    /// find type in vector
    static unsigned int findSideType( std::string_view testName, const std::vector<std::string>& names );

    /// find type in vector
    static unsigned int findFaceType( std::string_view testName, const std::vector<std::string>& names );

    /// convert string to int
    static unsigned int toInt( std::string_view str );

    static const auto& s_StationTypMap() {
      static const boost::container::flat_map<std::string, Station, std::less<>> m = {
          {"UnknownStation", Station::UnknownStation}, {"UTa", Station::UTa}, {"UTb", Station::UTb}};
      return m;
    }
    static const auto& s_detRegionTypMap() {
      static const boost::container::flat_map<std::string, detRegion, std::less<>> m = {
          {"UnknownRegion", detRegion::UnknownRegion},
          {"RegionC", detRegion::RegionC},
          {"RegionB", detRegion::RegionB},
          {"RegionA", detRegion::RegionA}};
      return m;
    }
    static const auto& s_SideTypMap() {
      static const boost::container::flat_map<std::string, Side, std::less<>> m = {{"UTC", Side::UTC},
                                                                                   {"UTA", Side::UTA}};
      return m;
    }
    static const auto& s_LayerTypMap() {
      static const boost::container::flat_map<std::string, Layer, std::less<>> m = {
          {"aX", Layer::aX}, {"aU", Layer::aU}, {"bV", Layer::bV}, {"bX", Layer::bX}};
      return m;
    }
    static const auto& s_FaceTypMap() {
      static const boost::container::flat_map<std::string, Face, std::less<>> m = {{"Back", Face::Back},
                                                                                   {"Front", Face::Front}};
      return m;
    }

  }; // class UTNames

  inline std::ostream& operator<<( std::ostream& s, LHCb::UTNames::Station e ) {
    switch ( e ) {
    case LHCb::UTNames::Station::UnknownStation:
      return s << "UnknownStation";
    case LHCb::UTNames::Station::UTa:
      return s << "UTa";
    case LHCb::UTNames::Station::UTb:
      return s << "UTb";
    default:
      return s << "ERROR wrong value " << int( e ) << " for enum LHCb::UTNames::Station";
    }
  }

  inline std::ostream& operator<<( std::ostream& s, LHCb::UTNames::detRegion e ) {
    switch ( e ) {
    case LHCb::UTNames::detRegion::UnknownRegion:
      return s << "UnknownRegion";
    case LHCb::UTNames::detRegion::RegionC:
      return s << "RegionC";
    case LHCb::UTNames::detRegion::RegionB:
      return s << "RegionB";
    case LHCb::UTNames::detRegion::RegionA:
      return s << "RegionA";
    default:
      return s << "ERROR wrong value " << int( e ) << " for enum LHCb::UTNames::detRegion";
    }
  }

  inline std::ostream& operator<<( std::ostream& s, LHCb::UTNames::Side e ) {
    switch ( e ) {
    case LHCb::UTNames::Side::UTC:
      return s << "UTC";
    case LHCb::UTNames::Side::UTA:
      return s << "UTA";
    default:
      return s << "ERROR wrong value " << int( e ) << " for enum LHCb::UTNames::Side";
    }
  }

  inline std::ostream& operator<<( std::ostream& s, LHCb::UTNames::Layer e ) {
    switch ( e ) {
    case LHCb::UTNames::Layer::aX:
      return s << "UTaX";
    case LHCb::UTNames::Layer::aU:
      return s << "UTaU";
    case LHCb::UTNames::Layer::bV:
      return s << "UTbV";
    case LHCb::UTNames::Layer::bX:
      return s << "UTbX";
    default:
      return s << "ERROR wrong value " << int( e ) << " for enum LHCb::UTNames::HalfLayer";
    }
  }

  inline std::ostream& operator<<( std::ostream& s, LHCb::UTNames::Face e ) {
    switch ( e ) {
    case LHCb::UTNames::Face::Back:
      return s << "Back";
    case LHCb::UTNames::Face::Front:
      return s << "Front";
    default:
      return s << "ERROR wrong value " << int( e ) << " for enum LHCb::UTNames::Face";
    }
  }

} // namespace LHCb

// -----------------------------------------------------------------------------
// end of class
// -----------------------------------------------------------------------------
