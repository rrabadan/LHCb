/*****************************************************************************\
* (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

// Author: G. Raven, November 2022

#pragma once
#include <functional>
#include <type_traits>

namespace LHCb {

  template <typename Range, typename Transform, typename UnaryPredicate>
  class TransformedRange {
    Range          range;
    Transform      f;
    UnaryPredicate pred;

    struct Sentinel {};

    struct Iterator {
      using iterator_type = decltype( begin( range ) );
      iterator_type           i;
      TransformedRange const* parent;
      Iterator( const iterator_type& _i, TransformedRange const* _parent ) : i{_i}, parent{_parent} {
        if ( isInvalid() ) ++( *this );
      }
      decltype( auto ) operator*() const { return std::invoke( parent->f, *i ); }
      bool             operator!=( Sentinel ) const {
        using std::end;
        return i != end( parent->range );
      }
      Iterator& operator++() {
        do { ++i; } while ( isInvalid() );
        return *this;
      }
      auto isInvalid() const {
        using std::end;
        return i != end( parent->range ) && !std::invoke( parent->pred, *i );
      }
    };

  public:
    template <typename R, typename F, typename P,
              typename = std::enable_if_t<std::is_invocable_v<F, decltype( *begin( std::declval<R>() ) )>>>
    TransformedRange( R&& r, F&& f, P&& pred )
        : range{std::forward<R>( r )}, f{std::forward<F>( f )}, pred{std::forward<P>( pred )} {}
    auto begin() const {
      using std::begin;
      return Iterator{begin( range ), this};
    }
    auto end() const { return Sentinel{}; }
  };

  template <typename Range, typename Transform, typename UnaryPredicate>
  TransformedRange( Range&&, Transform&&, UnaryPredicate && )->TransformedRange<Range, Transform, UnaryPredicate>;

} // namespace LHCb
