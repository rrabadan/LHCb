/*****************************************************************************\
* (c) Copyright 2000-2022 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#pragma once
#include "GaudiKernel/Kernel.h"
#include "Kernel/RichDetectorType.h"
#include "Kernel/RichSide.h"
#include "LHCbMath/bit_cast.h"
#include <algorithm>
#include <cassert>
#include <cstdint>
#include <ostream>
#include <vector>

class DeRichPMTPanel;
class DeRichPMTPanelClassic;
namespace Rich::Future {
  class RawBankDecoder;
}

namespace LHCb {

  /** @class RichSmartID32 RichSmartID32.h
   *
   *  Identifier for RICH detector objects (RICH Detector, PD panel, PD or PD pixel)
   *
   *  @author Chris Jones   Christopher.Rob.Jones@cern.ch
   *  @date  24/02/2011
   */
  class RichSmartID32 final {

    // Allow some specific classes friendship
    friend DeRichPMTPanel;
    friend DeRichPMTPanelClassic;
    friend Rich::Future::RawBankDecoder;

  public:
    // definitions

    /// Type for internal key
    using KeyType = std::uint32_t;

    /// Vector of RichSmartID32s
    using Vector = std::vector<LHCb::RichSmartID32>;

    /// Numerical type for bit packing
    using BitPackType = std::uint32_t;

    /// Type for values in data fields
    using DataType = std::uint32_t;

    /// Number of bits
    static constexpr const BitPackType NBits = 32;

  private:
    // data

    /// Get the initialisation value from a value, shift and mask
    static constexpr KeyType initData( const BitPackType value, //
                                       const BitPackType shift, //
                                       const BitPackType mask ) noexcept {
      return ( value << shift ) & mask;
    }

    /** The bit-packed internal data word.
     *  Default initialisation is as an HPD ID */
    KeyType m_key{initData( HPDID, ShiftIDType, MaskIDType )};

  public:
    // data access

    /// Retrieve the bit-packed internal data word
    [[nodiscard]] constexpr KeyType key() const noexcept { return m_key; }

    /// implicit conversion to unsigned int
    constexpr operator uint32_t() const noexcept { return key(); }

    /// implicit conversion to unsigned long
    constexpr operator uint64_t() const noexcept { return static_cast<uint64_t>( key() ); }

    /// implicit conversion to signed int
    constexpr operator int32_t() const noexcept { return as_int(); }

    /// implicit conversion to signed long
    constexpr operator int64_t() const noexcept { return static_cast<int64_t>( as_int() ); }

    /// Set the bit-packed internal data word
    constexpr RichSmartID32& setKey( const LHCb::RichSmartID32::KeyType value ) noexcept {
      m_key = value;
      return *this;
    }

  private:
    // internal bit packing

    // Setup up the type bit field

    /// Number of bits to use for the PD type
    static constexpr const BitPackType BitsIDType = 1;
    /// Use the last bit of the word
    static constexpr const BitPackType ShiftIDType = ( BitPackType )( NBits - BitsIDType );
    /// Mask for the PD type
    static constexpr const BitPackType MaskIDType = ( BitPackType )( ( 1 << BitsIDType ) - 1 ) << ShiftIDType;
    /// Max possible value that can be stored in the PD type field
    static constexpr const BitPackType MaxIDType = ( BitPackType )( 1 << BitsIDType ) - 1;

  public:
    /** @enum IDType
     *
     *  The type of photon detector this RichSmartID32 represents
     *
     *  @author Chris Jones  Christopher.Rob.Jones@cern.ch
     *  @date   25/02/2011
     */
    enum IDType : int8_t {
      Undefined = -1, ///< Undefined
      MaPMTID   = 0,  ///< Represents an MaPMT channel
      HPDID     = 1   ///< Represents an HPD channel
    };

  public:
    /// Access the ID type
    [[nodiscard]] constexpr RichSmartID32::IDType idType() const noexcept {
      return ( RichSmartID32::IDType )( ( key() & MaskIDType ) >> ShiftIDType );
    }

    /// Shortcut to check if this is a MaPMT identifier
    [[nodiscard]] constexpr bool isPMT() const noexcept { return idType() == MaPMTID; }

    /// Shortcut to check if this is a HPD identifier
    [[nodiscard]] constexpr bool isHPD() const noexcept { return idType() == HPDID; }

  public:
    /// Set the ID type
    constexpr void setIDType( const LHCb::RichSmartID32::IDType type )
#ifdef NDEBUG
        noexcept
#endif
    {
#ifndef NDEBUG
      checkRange( type, MaxIDType, "IDType" );
#endif
      setData( type, ShiftIDType, MaskIDType );
    }

  public:
    /** @class HPD RichSmartID32.h
     *
     *  Implementation details for HPDs
     *
     *  @author Chris Jones   Christopher.Rob.Jones@cern.ch
     *  @date  24/02/2011
     */
    class HPD {

    public:
      /// Number of HPD pixels per row
      static constexpr const DataType PixelsPerRow = 32;
      /// Number of HPD pixels per column
      static constexpr const DataType PixelsPerCol = 32;
      /// Total number of HPD pixels
      static constexpr const DataType TotalPixels = PixelsPerRow * PixelsPerCol;

      // Number of bits for each data field in the word
      static constexpr const BitPackType BitsPixelSubRow      = 3; ///< Number of bits for HPD sub pixel field
      static constexpr const BitPackType BitsPixelCol         = 5; ///< Number of bits for HPD pixel column
      static constexpr const BitPackType BitsPixelRow         = 5; ///< Number of bits for HPD pixel row
      static constexpr const BitPackType BitsPDNumInCol       = 5; ///< Number of bits for HPD 'number in column'
      static constexpr const BitPackType BitsPDCol            = 5; ///< Number of bits for HPD column
      static constexpr const BitPackType BitsPanel            = 1; ///< Number of bits for HPD panel
      static constexpr const BitPackType BitsRich             = 1; ///< Number of bits for RICH detector
      static constexpr const BitPackType BitsPixelSubRowIsSet = 1;
      static constexpr const BitPackType BitsPixelColIsSet    = 1;
      static constexpr const BitPackType BitsPixelRowIsSet    = 1;
      static constexpr const BitPackType BitsPDIsSet          = 1;
      static constexpr const BitPackType BitsPanelIsSet       = 1;
      static constexpr const BitPackType BitsRichIsSet        = 1;

      // The shifts
      static constexpr const BitPackType ShiftPixelSubRow      = 0;
      static constexpr const BitPackType ShiftPixelCol         = ShiftPixelSubRow + BitsPixelSubRow;
      static constexpr const BitPackType ShiftPixelRow         = ShiftPixelCol + BitsPixelCol;
      static constexpr const BitPackType ShiftPDNumInCol       = ShiftPixelRow + BitsPixelRow;
      static constexpr const BitPackType ShiftPDCol            = ShiftPDNumInCol + BitsPDNumInCol;
      static constexpr const BitPackType ShiftPanel            = ShiftPDCol + BitsPDCol;
      static constexpr const BitPackType ShiftRich             = ShiftPanel + BitsPanel;
      static constexpr const BitPackType ShiftPixelSubRowIsSet = ShiftRich + BitsRich;
      static constexpr const BitPackType ShiftPixelColIsSet    = ShiftPixelSubRowIsSet + BitsPixelSubRowIsSet;
      static constexpr const BitPackType ShiftPixelRowIsSet    = ShiftPixelColIsSet + BitsPixelColIsSet;
      static constexpr const BitPackType ShiftPDIsSet          = ShiftPixelRowIsSet + BitsPixelRowIsSet;
      static constexpr const BitPackType ShiftPanelIsSet       = ShiftPDIsSet + BitsPDIsSet;
      static constexpr const BitPackType ShiftRichIsSet        = ShiftPanelIsSet + BitsPanelIsSet;

      // The masks
      static constexpr const BitPackType MaskPixelSubRow = ( BitPackType )( ( 1 << BitsPixelSubRow ) - 1 )
                                                           << ShiftPixelSubRow;
      static constexpr const BitPackType MaskPixelCol   = ( BitPackType )( ( 1 << BitsPixelCol ) - 1 ) << ShiftPixelCol;
      static constexpr const BitPackType MaskPixelRow   = ( BitPackType )( ( 1 << BitsPixelRow ) - 1 ) << ShiftPixelRow;
      static constexpr const BitPackType MaskPDNumInCol = ( BitPackType )( ( 1 << BitsPDNumInCol ) - 1 )
                                                          << ShiftPDNumInCol;
      static constexpr const BitPackType MaskPDCol            = ( BitPackType )( ( 1 << BitsPDCol ) - 1 ) << ShiftPDCol;
      static constexpr const BitPackType MaskPanel            = ( BitPackType )( ( 1 << BitsPanel ) - 1 ) << ShiftPanel;
      static constexpr const BitPackType MaskRich             = ( BitPackType )( ( 1 << BitsRich ) - 1 ) << ShiftRich;
      static constexpr const BitPackType MaskPixelSubRowIsSet = ( BitPackType )( ( 1 << BitsPixelSubRowIsSet ) - 1 )
                                                                << ShiftPixelSubRowIsSet;
      static constexpr const BitPackType MaskPixelColIsSet = ( BitPackType )( ( 1 << BitsPixelColIsSet ) - 1 )
                                                             << ShiftPixelColIsSet;
      static constexpr const BitPackType MaskPixelRowIsSet = ( BitPackType )( ( 1 << BitsPixelRowIsSet ) - 1 )
                                                             << ShiftPixelRowIsSet;
      static constexpr const BitPackType MaskPDIsSet    = ( BitPackType )( ( 1 << BitsPDIsSet ) - 1 ) << ShiftPDIsSet;
      static constexpr const BitPackType MaskPanelIsSet = ( BitPackType )( ( 1 << BitsPanelIsSet ) - 1 )
                                                          << ShiftPanelIsSet;
      static constexpr const BitPackType MaskRichIsSet = ( BitPackType )( ( 1 << BitsRichIsSet ) - 1 )
                                                         << ShiftRichIsSet;

      // Max Values
      static constexpr const DataType MaxPixelSubRow = ( DataType )( 1 << BitsPixelSubRow ) - 1;
      static constexpr const DataType MaxPixelCol    = ( DataType )( 1 << BitsPixelCol ) - 1;
      static constexpr const DataType MaxPixelRow    = ( DataType )( 1 << BitsPixelRow ) - 1;
      static constexpr const DataType MaxPDNumInCol  = ( DataType )( 1 << BitsPDNumInCol ) - 1;
      static constexpr const DataType MaxPDCol       = ( DataType )( 1 << BitsPDCol ) - 1;
      static constexpr const DataType MaxPanel       = ( DataType )( 1 << BitsPanel ) - 1;
      static constexpr const DataType MaxRich        = ( DataType )( 1 << BitsRich ) - 1;
    };

  public:
    /** @class MaPMT RichSmartID32.h
     *
     *  Implementation details for MaPMTs
     *
     *  @author Chris Jones   Christopher.Rob.Jones@cern.ch
     *  @date  24/02/2011
     */
    class MaPMT {

    public:
      /// Number of PMT pixels per row
      static constexpr const DataType PixelsPerRow = 8;
      /// Number of PMT pixels per column
      static constexpr const DataType PixelsPerCol = 8;
      /// Total number of PMT pixels
      static constexpr const DataType TotalPixels = PixelsPerRow * PixelsPerCol;
      /// Number PMTs per EC for R type PMTs
      static constexpr const DataType RTypePMTsPerEC = 4;
      /// Number PMTs per EC for H type PMTs
      static constexpr const DataType HTypePMTsPerEC = 1;
      /// Number of ECs per module
      static constexpr const DataType ECsPerModule = 4;
      /// Number of modules per column
      static constexpr const DataType ModulesPerColumn = 6;
      /// Number of module columns per panel, in each RICH
      static constexpr const Rich::DetectorArray<DataType> ModuleColumnsPerPanel = {
#ifdef USE_DD4HEP
          {11, 14} // With dd4hep we have an extra column reserved at the end of each RICH2 panel
#else
          {11, 12}
#endif
      };
      /// Maximum number of module columns in any panel, RICH1 or RICH2
      static constexpr const DataType MaxModuleColumnsAnyPanel =
          std::max( ModuleColumnsPerPanel[Rich::Rich1], ModuleColumnsPerPanel[Rich::Rich2] );
      /// Number of modules per panel, in each RICH
      static constexpr const Rich::DetectorArray<DataType> ModulesPerPanel = {
          {( ModulesPerColumn * ModuleColumnsPerPanel[Rich::Rich1] ),
           ( ModulesPerColumn * ModuleColumnsPerPanel[Rich::Rich2] )}};
      /** Maximum number of modules in any panel
       *  @todo Should be largest number above, but to retain support for 'classic' PMTs
       *  need to set to 92 to cover fact the old numbering scheme had more modules.
       *  FIXME Change back once support for the 'classic' PMTs is no longer required. */
      static constexpr const DataType MaxModulesPerPanel =
          92; // std::max(ModulesPerPanel[Rich::Rich1],ModulesPerPanel[Rich::Rich2]);
      /// Module 'global' number offsets for each RICH and Panel
      static constexpr const Rich::DetectorArray<Rich::PanelArray<DataType>> PanelModuleOffsets = {
          {{0, ModulesPerPanel[Rich::Rich1]},
           {2 * ModulesPerPanel[Rich::Rich1], ( 2 * ModulesPerPanel[Rich::Rich1] ) + ModulesPerPanel[Rich::Rich2]}}};
      /// Number of modules in RICH1
      static constexpr const DataType RICH1Modules = ( Rich::NPDPanelsPerRICH * ModulesPerPanel[Rich::Rich1] );
      /// Number of modules in RICH2
      static constexpr const DataType RICH2Modules = ( Rich::NPDPanelsPerRICH * ModulesPerPanel[Rich::Rich2] );
      /// Maximum modules per RICH
      static constexpr const Rich::DetectorArray<DataType> MaxModulesPerRICH = {{RICH1Modules, RICH2Modules}};
      /// Total number of modules
      static constexpr const DataType TotalModules = RICH1Modules + RICH2Modules;
      /// Maximum number of PDs per module
      static constexpr const DataType MaxPDsPerModule = 16;

      // parameters for global pixel view
      /// PMT Shift parameter in global view
      static constexpr const DataType PDGlobalViewShift = 2 + ( 2 * PixelsPerRow );
      /// Max x range for global view
      static constexpr const Rich::DetectorArray<DataType> PDGlobalViewRangeX = {
          {( PDGlobalViewShift * ModuleColumnsPerPanel[0] ), ( PDGlobalViewShift * ModuleColumnsPerPanel[1] )}};
      /// Max y range for global view
      static constexpr const Rich::DetectorArray<DataType> PDGlobalViewRangeY = {
          {( ModulesPerColumn * PDGlobalViewShift * ECsPerModule / 2 ),
           ( ModulesPerColumn * PDGlobalViewShift * ECsPerModule / 2 )}};

      /// Compute the global module number from the panel local module number
      static auto panelLocalToGlobalModN( const Rich::DetectorType rich, //
                                          const Rich::Side         side, //
                                          const DataType           locModN ) noexcept {
        return locModN + PanelModuleOffsets[rich][side];
      }

      /// Compute the panel local module number from the global module number
      static auto globalToPanelLocalModN( const Rich::DetectorType rich, //
                                          const Rich::Side         side, //
                                          const DataType           gloModN ) noexcept {
        return gloModN - PanelModuleOffsets[rich][side];
      }

      // Number of bits for each data field in the word
      static constexpr const BitPackType BitsPixelCol         = 3; ///< Number of bits for MaPMT pixel column
      static constexpr const BitPackType BitsPixelRow         = 3; ///< Number of bits for MaPMT pixel row
      static constexpr const BitPackType BitsPDNumInMod       = 4; ///< Number of bits for MaPMT 'number in module'
      static constexpr const BitPackType BitsPDMod            = 9; ///< Number of bits for MaPMT module
      static constexpr const BitPackType BitsPanel            = 1; ///< Number of bits for MaPMT panel
      static constexpr const BitPackType BitsRich             = 1; ///< Number of bits for RICH detector
      static constexpr const BitPackType BitsPixelSubRowIsSet = 1;
      static constexpr const BitPackType BitsPixelColIsSet    = 1;
      static constexpr const BitPackType BitsPixelRowIsSet    = 1;
      static constexpr const BitPackType BitsPDIsSet          = 1;
      static constexpr const BitPackType BitsPanelIsSet       = 1;
      static constexpr const BitPackType BitsRichIsSet        = 1;
      static constexpr const BitPackType BitsLargePixel       = 1;

      // The shifts
      static constexpr const BitPackType ShiftPixelCol         = 0;
      static constexpr const BitPackType ShiftPixelRow         = ShiftPixelCol + BitsPixelCol;
      static constexpr const BitPackType ShiftPDNumInMod       = ShiftPixelRow + BitsPixelRow;
      static constexpr const BitPackType ShiftPDMod            = ShiftPDNumInMod + BitsPDNumInMod;
      static constexpr const BitPackType ShiftPanel            = ShiftPDMod + BitsPDMod;
      static constexpr const BitPackType ShiftRich             = ShiftPanel + BitsPanel;
      static constexpr const BitPackType ShiftPixelSubRowIsSet = ShiftRich + BitsRich;
      static constexpr const BitPackType ShiftPixelColIsSet    = ShiftPixelSubRowIsSet + BitsPixelSubRowIsSet;
      static constexpr const BitPackType ShiftPixelRowIsSet    = ShiftPixelColIsSet + BitsPixelColIsSet;
      static constexpr const BitPackType ShiftPDIsSet          = ShiftPixelRowIsSet + BitsPixelRowIsSet;
      static constexpr const BitPackType ShiftPanelIsSet       = ShiftPDIsSet + BitsPDIsSet;
      static constexpr const BitPackType ShiftRichIsSet        = ShiftPanelIsSet + BitsPanelIsSet;
      static constexpr const BitPackType ShiftLargePixel       = ShiftRichIsSet + BitsRichIsSet;

      // The masks
      static constexpr const BitPackType MaskPixelCol   = ( BitPackType )( ( 1 << BitsPixelCol ) - 1 ) << ShiftPixelCol;
      static constexpr const BitPackType MaskPixelRow   = ( BitPackType )( ( 1 << BitsPixelRow ) - 1 ) << ShiftPixelRow;
      static constexpr const BitPackType MaskPDNumInMod = ( BitPackType )( ( 1 << BitsPDNumInMod ) - 1 )
                                                          << ShiftPDNumInMod;
      static constexpr const BitPackType MaskPDMod            = ( BitPackType )( ( 1 << BitsPDMod ) - 1 ) << ShiftPDMod;
      static constexpr const BitPackType MaskPanel            = ( BitPackType )( ( 1 << BitsPanel ) - 1 ) << ShiftPanel;
      static constexpr const BitPackType MaskRich             = ( BitPackType )( ( 1 << BitsRich ) - 1 ) << ShiftRich;
      static constexpr const BitPackType MaskPixelSubRowIsSet = ( BitPackType )( ( 1 << BitsPixelSubRowIsSet ) - 1 )
                                                                << ShiftPixelSubRowIsSet;
      static constexpr const BitPackType MaskPixelColIsSet = ( BitPackType )( ( 1 << BitsPixelColIsSet ) - 1 )
                                                             << ShiftPixelColIsSet;
      static constexpr const BitPackType MaskPixelRowIsSet = ( BitPackType )( ( 1 << BitsPixelRowIsSet ) - 1 )
                                                             << ShiftPixelRowIsSet;
      static constexpr const BitPackType MaskPDIsSet    = ( BitPackType )( ( 1 << BitsPDIsSet ) - 1 ) << ShiftPDIsSet;
      static constexpr const BitPackType MaskPanelIsSet = ( BitPackType )( ( 1 << BitsPanelIsSet ) - 1 )
                                                          << ShiftPanelIsSet;
      static constexpr const BitPackType MaskRichIsSet = ( BitPackType )( ( 1 << BitsRichIsSet ) - 1 )
                                                         << ShiftRichIsSet;
      static constexpr const BitPackType MaskLargePixel = ( BitPackType )( ( 1 << BitsLargePixel ) - 1 )
                                                          << ShiftLargePixel;

      // Max Values
      static constexpr const DataType MaxPixelCol   = ( DataType )( 1 << BitsPixelCol ) - 1;
      static constexpr const DataType MaxPixelRow   = ( DataType )( 1 << BitsPixelRow ) - 1;
      static constexpr const DataType MaxPDNumInMod = ( DataType )( 1 << BitsPDNumInMod ) - 1;
      static constexpr const DataType MaxPDMod      = ( DataType )( 1 << BitsPDMod ) - 1;
      static constexpr const DataType MaxPanel      = ( DataType )( 1 << BitsPanel ) - 1;
      static constexpr const DataType MaxRich       = ( DataType )( 1 << BitsRich ) - 1;
    };

  private:
    /// Reinterpret the internal unsigned representation as a signed 32 bit int
    [[nodiscard]] constexpr int32_t as_int() const noexcept { return bit_cast<int32_t>( m_key ); }

    /// Set the given data into the given field, without validity bit
    constexpr void setData( const DataType    value, //
                            const BitPackType shift, //
                            const BitPackType mask ) noexcept {
      setKey( ( ( value << shift ) & mask ) | ( m_key & ~mask ) );
    }

    /// Set the given data into the given field, with validity bit
    constexpr void setData( const DataType    value, //
                            const BitPackType shift, //
                            const BitPackType mask,  //
                            const BitPackType okMask ) noexcept {
      setKey( ( ( value << shift ) & mask ) | ( m_key & ~mask ) | okMask );
    }

    /// Checks if a data value is within range for a given field
    constexpr void checkRange( const DataType   value,    //
                               const DataType   maxValue, //
                               std::string_view message ) const {
      if ( value > maxValue ) { rangeError( value, maxValue, message ); }
    }

    /// Issue an exception in the case of a range error
    void rangeError( const DataType   value,    //
                     const DataType   maxValue, //
                     std::string_view message ) const;

  public:
    // constructors

    /// Default Constructor
    constexpr RichSmartID32() = default;

    /// Constructor from internal type (unsigned int)
    explicit constexpr RichSmartID32( const LHCb::RichSmartID32::KeyType key ) noexcept : m_key( key ) {}

    /// Constructor from unsigned 64 bit int
    explicit constexpr RichSmartID32( const uint64_t key ) noexcept
        : m_key( static_cast<LHCb::RichSmartID32::KeyType>( key & 0x00000000FFFFFFFF ) ) {}

    /// Constructor from signed 32 bit int type
    explicit constexpr RichSmartID32( const int32_t key ) noexcept
        : m_key( bit_cast<LHCb::RichSmartID32::KeyType>( key ) ) {}

    /// Constructor from signed 64 bit int
    explicit constexpr RichSmartID32( const int64_t key ) noexcept
        : m_key( static_cast<LHCb::RichSmartID32::KeyType>( key & 0x00000000FFFFFFFF ) ) {}

    /// Pixel level constructor including sub-pixel information
    constexpr RichSmartID32( const Rich::DetectorType rich,        //
                             const Rich::Side         panel,       //
                             const DataType           pdNumInCol,  //
                             const DataType           pdCol,       //
                             const DataType           pixelRow,    //
                             const DataType           pixelCol,    //
                             const DataType           pixelSubRow, //
                             const IDType             type = HPDID )
#ifdef NDEBUG
        noexcept
#endif
    {
      assert( HPDID == type );
      setIDType( type );
      if ( HPDID == type ) {
        setRich_HPD( rich );
        setPanel_HPD( panel );
        setPD_HPD( pdCol, pdNumInCol );
        setPixelRow_HPD( pixelRow );
        setPixelCol_HPD( pixelCol );
        setPixelSubRow( pixelSubRow );
      }
    }

    /// Pixel level constructor
    constexpr RichSmartID32( const Rich::DetectorType rich,            //
                             const Rich::Side         panel,           //
                             const DataType           pdNumInMod,      //
                             const DataType           pdMod,           //
                             const DataType           pixelRow,        //
                             const DataType           pixelCol,        //
                             const IDType             type    = HPDID, //
                             const bool               isHType = false )
#ifdef NDEBUG
        noexcept
#endif
    {
      setIDType( type );
      if ( MaPMTID == type ) {
        setRich_PMT( rich );
        setPanel_PMT( panel );
        setPD_PMT( pdMod, pdNumInMod );
        setPixelRow_PMT( pixelRow );
        setPixelCol_PMT( pixelCol );
        setLargePMT( isHType );
      } else {
        setRich_HPD( rich );
        setPanel_HPD( panel );
        setPD_HPD( pdMod, pdNumInMod );
        setPixelRow_HPD( pixelRow );
        setPixelCol_HPD( pixelCol );
        assert( !isHType );
      }
    }

    /// PD level constructor
    constexpr RichSmartID32( const Rich::DetectorType rich,            //
                             const Rich::Side         panel,           //
                             const DataType           pdNumInMod,      //
                             const DataType           pdMod,           //
                             const IDType             type    = HPDID, //
                             const bool               isHType = false )
#ifdef NDEBUG
        noexcept
#endif
    {
      setIDType( type );
      if ( MaPMTID == type ) {
        setRich_PMT( rich );
        setPanel_PMT( panel );
        setPD_PMT( pdMod, pdNumInMod );
        setLargePMT( isHType );
      } else {
        setRich_HPD( rich );
        setPanel_HPD( panel );
        setPD_HPD( pdMod, pdNumInMod );
        assert( !isHType );
      }
    }

    /// PD panel level constructor
    constexpr RichSmartID32( const Rich::DetectorType rich,  //
                             const Rich::Side         panel, //
                             const IDType             type = HPDID )
#ifdef NDEBUG
        noexcept
#endif
    {
      setIDType( type );
      if ( MaPMTID == type ) {
        setRich_PMT( rich );
        setPanel_PMT( panel );
      } else {
        setRich_HPD( rich );
        setPanel_HPD( panel );
      }
    }

    /// RICH level constructor
    constexpr explicit RichSmartID32( const Rich::DetectorType rich, //
                                      const IDType             type = HPDID )
#ifdef NDEBUG
        noexcept
#endif
    {
      setIDType( type );
      if ( MaPMTID == type ) {
        setRich_PMT( rich );
      } else {
        setRich_HPD( rich );
      }
    }

  public:
    // comparison operators

    /// < operator
    constexpr friend bool operator<( const LHCb::RichSmartID32& lhs, const LHCb::RichSmartID32& rhs ) noexcept {
      return lhs.key() < rhs.key();
    }
    /// Equality operator
    constexpr friend bool operator==( const LHCb::RichSmartID32& lhs, const LHCb::RichSmartID32& rhs ) noexcept {
      return lhs.key() == rhs.key();
    }
    /// > operator
    constexpr friend bool operator>( const LHCb::RichSmartID32& lhs, const LHCb::RichSmartID32& rhs ) noexcept {
      return lhs.key() > rhs.key();
    }

    /// <= operator
    constexpr friend bool operator<=( const LHCb::RichSmartID32& lhs, const LHCb::RichSmartID32& rhs ) noexcept {
      return !( lhs > rhs );
    }
    /// Inequality operator
    constexpr friend bool operator!=( const LHCb::RichSmartID32& lhs, const LHCb::RichSmartID32& rhs ) noexcept {
      return !( lhs == rhs );
    }
    /// >= operator
    constexpr friend bool operator>=( const LHCb::RichSmartID32& lhs, const LHCb::RichSmartID32& rhs ) noexcept {
      return !( lhs < rhs );
    }

  private:
    // HPD specific setters

    /// Set the RICH detector identifier for HPDs
    constexpr void setRich_HPD( const Rich::DetectorType rich )
#ifdef NDEBUG
        noexcept
#endif
    {
#ifndef NDEBUG
      assert( isHPD() );
      checkRange( rich, HPD::MaxRich, "RICH" );
#endif
      setData( rich, HPD::ShiftRich, HPD::MaskRich, HPD::MaskRichIsSet );
    }

    /// Set the RICH PD panel identifier for HPDs
    constexpr void setPanel_HPD( const Rich::Side panel )
#ifdef NDEBUG
        noexcept
#endif
    {
#ifndef NDEBUG
      assert( isHPD() );
      checkRange( panel, HPD::MaxPanel, "Panel" );
#endif
      setData( panel, HPD::ShiftPanel, HPD::MaskPanel, HPD::MaskPanelIsSet );
    }

    /// Set the RICH PD column and number in column identifier for HPDs
    constexpr void setPD_HPD( const DataType col, const DataType nInCol )
#ifdef NDEBUG
        noexcept
#endif
    {
#ifndef NDEBUG
      assert( isHPD() );
      checkRange( col, HPD::MaxPDCol, "PDColumn" );
      checkRange( nInCol, HPD::MaxPDNumInCol, "PDNumInCol" );
#endif
      setData( col, HPD::ShiftPDCol, HPD::MaskPDCol, HPD::MaskPDIsSet );
      setData( nInCol, HPD::ShiftPDNumInCol, HPD::MaskPDNumInCol );
    }

    /// Set the RICH PD pixel row identifier for HPDs
    constexpr void setPixelRow_HPD( const DataType row )
#ifdef NDEBUG
        noexcept
#endif
    {
#ifndef NDEBUG
      assert( isHPD() );
      checkRange( row, HPD::MaxPixelRow, "PixelRow" );
#endif
      setData( row, HPD::ShiftPixelRow, HPD::MaskPixelRow, HPD::MaskPixelRowIsSet );
    }

    /// Set the RICH PD pixel column identifier for HPDs
    constexpr void setPixelCol_HPD( const DataType col )
#ifdef NDEBUG
        noexcept
#endif
    {
#ifndef NDEBUG
      assert( isHPD() );
      checkRange( col, HPD::MaxPixelCol, "PixelColumn" );
#endif
      setData( col, HPD::ShiftPixelCol, HPD::MaskPixelCol, HPD::MaskPixelColIsSet );
    }

  private:
    // PMT specific setters

    /// Set the RICH detector identifier for PMTs
    constexpr void setRich_PMT( const Rich::DetectorType rich )
#ifdef NDEBUG
        noexcept
#endif
    {
#ifndef NDEBUG
      assert( isPMT() );
      checkRange( rich, MaPMT::MaxRich, "RICH" );
#endif
      setData( rich, MaPMT::ShiftRich, MaPMT::MaskRich, MaPMT::MaskRichIsSet );
    }

    /// Set the RICH PD panel identifier for PMTs
    constexpr void setPanel_PMT( const Rich::Side panel )
#ifdef NDEBUG
        noexcept
#endif
    {
#ifndef NDEBUG
      assert( isPMT() );
      checkRange( panel, MaPMT::MaxPanel, "Panel" );
#endif
      setData( panel, MaPMT::ShiftPanel, MaPMT::MaskPanel, MaPMT::MaskPanelIsSet );
    }

    /// Set the RICH PD column and number in column identifier for PMTs
    constexpr void setPD_PMT( const DataType mod, const DataType nInMod )
#ifdef NDEBUG
        noexcept
#endif
    {
#ifndef NDEBUG
      assert( isPMT() );
      checkRange( mod, MaPMT::MaxPDMod, "PDModule" );
      checkRange( nInMod, MaPMT::MaxPDNumInMod, "PDNumInMod" );
#endif
      setData( mod, MaPMT::ShiftPDMod, MaPMT::MaskPDMod, MaPMT::MaskPDIsSet );
      setData( nInMod, MaPMT::ShiftPDNumInMod, MaPMT::MaskPDNumInMod );
    }

    /// Set the RICH PD pixel row identifier for PMTs
    constexpr void setPixelRow_PMT( const DataType row )
#ifdef NDEBUG
        noexcept
#endif
    {
#ifndef NDEBUG
      assert( isPMT() );
      checkRange( row, MaPMT::MaxPixelRow, "PixelRow" );
#endif
      setData( row, MaPMT::ShiftPixelRow, MaPMT::MaskPixelRow, MaPMT::MaskPixelRowIsSet );
    }

    /// Set the RICH PD pixel column identifier for PMTs
    constexpr void setPixelCol_PMT( const DataType col )
#ifdef NDEBUG
        noexcept
#endif
    {
#ifndef NDEBUG
      assert( isPMT() );
      checkRange( col, MaPMT::MaxPixelCol, "PixelColumn" );
#endif
      setData( col, MaPMT::ShiftPixelCol, MaPMT::MaskPixelCol, MaPMT::MaskPixelColIsSet );
    }

    /// Set the RICH PMT pixel row and column via an anode number
    constexpr void setAnode_PMT( const DataType anode )
#ifdef NDEBUG
        noexcept
#endif
    {
      setPixelRow_PMT( anode / MaPMT::PixelsPerCol );
      setPixelCol_PMT( MaPMT::PixelsPerRow - 1 - ( anode % MaPMT::PixelsPerCol ) );
    }

    /// Set the RICH PD information via module number and EC data
    constexpr void setPD_EC_PMT( const DataType mod, const DataType ec, const DataType nInEC )
#ifdef NDEBUG
        noexcept
#endif
    {
      // compute number in module from EC data
      const auto nInMod = ( ec * numPMTsPerEC() ) + nInEC;
      // set the PD
      setPD_PMT( mod, nInMod );
    }

  public:
    // setters

    /// Set the RICH detector identifier
    constexpr void setRich( const Rich::DetectorType rich )
#ifdef NDEBUG
        noexcept
#endif
    {
      if ( isPMT() ) {
        setRich_PMT( rich );
      } else {
        setRich_HPD( rich );
      }
    }

    /// Set the RICH PD panel identifier
    constexpr void setPanel( const Rich::Side panel )
#ifdef NDEBUG
        noexcept
#endif
    {
      if ( isPMT() ) {
        setPanel_PMT( panel );
      } else {
        setPanel_HPD( panel );
      }
    }

    /// Set the RICH PD module/column and number in module/column identifier
    constexpr void setPD( const DataType mod, const DataType nInMod )
#ifdef NDEBUG
        noexcept
#endif
    {
      if ( isPMT() ) {
        setPD_PMT( mod, nInMod );
      } else {
        setPD_HPD( mod, nInMod );
      }
    }

    /// Set the RICH PMT pixel row and column via an anode number
    constexpr void setAnode( const DataType anode )
#ifdef NDEBUG
        noexcept
#endif
    {
      assert( isPMT() );
      setAnode_PMT( anode );
    }

    /// Set the RICH PD information via module number and EC data
    constexpr void setPD_EC( const DataType mod, const DataType ec, const DataType nInEC )
#ifdef NDEBUG
        noexcept
#endif
    {
      assert( isPMT() );
      setPD_EC_PMT( mod, ec, nInEC );
    }

    /// Set the RICH PD pixel row identifier
    constexpr void setPixelRow( const DataType row )
#ifdef NDEBUG
        noexcept
#endif
    {
      if ( isPMT() ) {
        setPixelRow_PMT( row );
      } else {
        setPixelRow_HPD( row );
      }
    }

    /// Set the RICH PD pixel column identifier
    constexpr void setPixelCol( const DataType col )
#ifdef NDEBUG
        noexcept
#endif
    {
      if ( isPMT() ) {
        setPixelCol_PMT( col );
      } else {
        setPixelCol_HPD( col );
      }
    }

    /// Set the RICH photon detector pixel sub-row identifier (Alice mode only)
    constexpr void setPixelSubRow( const DataType pixelSubRow ) {
      if ( HPDID == idType() ) {
#ifndef NDEBUG
        checkRange( pixelSubRow, HPD::MaxPixelSubRow, "PixelSubRow" );
#endif
        setData( pixelSubRow, HPD::ShiftPixelSubRow, HPD::MaskPixelSubRow, HPD::MaskPixelSubRowIsSet );
      } else {
        // MaPMTs do not have sub-pixel field...
        throw std::runtime_error{"MaPMTs cannot have their sub-pixel field set"};
      }
    }

  public:
    // getters for IDs

    /// Decoding function to strip the sub-pixel information and return a pixel RichSmartID32
    [[nodiscard]] constexpr LHCb::RichSmartID32 pixelID() const noexcept {
      return RichSmartID32( isPMT() //
                                ? key()
                                : key() & ~( HPD::MaskPixelSubRow + HPD::MaskPixelSubRowIsSet ) );
    }

    /// Decoding function to return an identifier for a single PD, stripping all pixel level
    /// information
    [[nodiscard]] constexpr LHCb::RichSmartID32 pdID() const noexcept {
      return RichSmartID32(
          key() & ( isPMT() ? ( MaPMT::MaskRich + MaPMT::MaskPanel + MaPMT::MaskPDNumInMod + MaPMT::MaskPDMod +
                                MaPMT::MaskLargePixel + MaPMT::MaskRichIsSet + MaPMT::MaskPanelIsSet +
                                MaPMT::MaskPDIsSet + MaskIDType )
                            : ( HPD::MaskRich + HPD::MaskPanel + HPD::MaskPDNumInCol + HPD::MaskPDCol +
                                HPD::MaskRichIsSet + HPD::MaskPanelIsSet + HPD::MaskPDIsSet + MaskIDType ) ) );
    }

    /// Decoding function to strip the photon-detector information and return a PD panel RichSmartID32
    [[nodiscard]] constexpr LHCb::RichSmartID32 panelID() const noexcept {
      return RichSmartID32(
          key() &
          ( isPMT() ? ( MaPMT::MaskRich + MaPMT::MaskPanel + MaPMT::MaskRichIsSet + MaPMT::MaskPanelIsSet + MaskIDType )
                    : ( HPD::MaskRich + HPD::MaskPanel + HPD::MaskRichIsSet + HPD::MaskPanelIsSet + MaskIDType ) ) );
    }

    /// Decoding function to strip all but the RICH information and return a RICH RichSmartID32
    [[nodiscard]] constexpr LHCb::RichSmartID32 richID() const noexcept {
      return RichSmartID32( key() & ( isPMT() ? ( MaPMT::MaskRich + MaPMT::MaskRichIsSet + MaskIDType )
                                              : ( HPD::MaskRich + HPD::MaskRichIsSet + MaskIDType ) ) );
    }

    /// Returns only the data fields, with the validity bits stripped
    [[nodiscard]] constexpr LHCb::RichSmartID32 dataBitsOnly() const noexcept {
      return RichSmartID32( key() &
                            ( isPMT() ? ( MaPMT::MaskRich + MaPMT::MaskPanel + MaPMT::MaskPDNumInMod +
                                          MaPMT::MaskPDMod + MaPMT::MaskPixelRow + MaPMT::MaskPixelCol )
                                      : ( HPD::MaskRich + HPD::MaskPanel + HPD::MaskPDNumInCol + HPD::MaskPDCol +
                                          HPD::MaskPixelRow + HPD::MaskPixelCol + HPD::MaskPixelSubRow ) ) );
    }

  public:
    // data accessors

    /// Retrieve The pixel sub-row (Alice mode) number
    [[nodiscard]] constexpr DataType pixelSubRow() const noexcept {
      // Note MaPMTs have no sub-pixel ...
      return ( DataType )( isHPD() ? ( ( key() & HPD::MaskPixelSubRow ) >> HPD::ShiftPixelSubRow ) : 0 );
    }

    /// Retrieve The pixel column number
    [[nodiscard]] constexpr DataType pixelCol() const noexcept {
      return ( DataType )( isPMT() ? ( ( key() & MaPMT::MaskPixelCol ) >> MaPMT::ShiftPixelCol )
                                   : ( ( key() & HPD::MaskPixelCol ) >> HPD::ShiftPixelCol ) );
    }

    /// Retrieve The pixel row number
    [[nodiscard]] constexpr DataType pixelRow() const noexcept {
      return ( DataType )( isPMT() ? ( ( key() & MaPMT::MaskPixelRow ) >> MaPMT::ShiftPixelRow )
                                   : ( ( key() & HPD::MaskPixelRow ) >> HPD::ShiftPixelRow ) );
    }

    /// Retrieve The PD number in module
    [[nodiscard]] constexpr DataType pdNumInMod() const noexcept {
      return ( DataType )( isPMT() ? ( ( key() & MaPMT::MaskPDNumInMod ) >> MaPMT::ShiftPDNumInMod )
                                   : ( ( key() & HPD::MaskPDNumInCol ) >> HPD::ShiftPDNumInCol ) );
    }
    /// Alias method for number in column (for HPD use case)
    [[nodiscard]] constexpr DataType pdNumInCol() const noexcept { return pdNumInMod(); }

    /// Retrieve The PD module number
    [[nodiscard]] constexpr DataType pdMod() const noexcept {
      return ( DataType )( isPMT() ? ( ( key() & MaPMT::MaskPDMod ) >> MaPMT::ShiftPDMod )
                                   : ( ( key() & HPD::MaskPDCol ) >> HPD::ShiftPDCol ) );
    }
    /// Alias method for PD column number (for HPD use case)
    [[nodiscard]] constexpr DataType pdCol() const noexcept { return pdMod(); }

    /// Retrieve The RICH panel
    [[nodiscard]] constexpr Rich::Side panel() const noexcept {
      return ( Rich::Side )( isPMT() ? ( ( key() & MaPMT::MaskPanel ) >> MaPMT::ShiftPanel )
                                     : ( ( key() & HPD::MaskPanel ) >> HPD::ShiftPanel ) );
    }

    /// Retrieve The RICH Detector
    [[nodiscard]] constexpr Rich::DetectorType rich() const noexcept {
      return ( Rich::DetectorType )( isPMT() ? ( ( key() & MaPMT::MaskRich ) >> MaPMT::ShiftRich )
                                             : ( ( key() & HPD::MaskRich ) >> HPD::ShiftRich ) );
    }

  public:
    /// Retrieve Pixel sub-row field is set
    [[nodiscard]] constexpr bool pixelSubRowIsSet() const noexcept {
      // Note MaPMTs have no sub-pixel ...
      return ( isHPD() ? 0 != ( ( key() & HPD::MaskPixelSubRowIsSet ) >> HPD::ShiftPixelSubRowIsSet ) : false );
    }

    /// Retrieve Pixel column field is set
    [[nodiscard]] constexpr bool pixelColIsSet() const noexcept {
      return ( isPMT() ? 0 != ( ( key() & MaPMT::MaskPixelColIsSet ) >> MaPMT::ShiftPixelColIsSet )
                       : 0 != ( ( key() & HPD::MaskPixelColIsSet ) >> HPD::ShiftPixelColIsSet ) );
    }

    /// Retrieve Pixel row field is set
    [[nodiscard]] bool pixelRowIsSet() const noexcept {
      return ( isPMT() ? 0 != ( ( key() & MaPMT::MaskPixelRowIsSet ) >> MaPMT::ShiftPixelRowIsSet )
                       : 0 != ( ( key() & HPD::MaskPixelRowIsSet ) >> HPD::ShiftPixelRowIsSet ) );
    }

    /// Retrieve PD column field is set
    [[nodiscard]] constexpr bool pdIsSet() const noexcept {
      return ( isPMT() ? 0 != ( ( key() & MaPMT::MaskPDIsSet ) >> MaPMT::ShiftPDIsSet )
                       : 0 != ( ( key() & HPD::MaskPDIsSet ) >> HPD::ShiftPDIsSet ) );
    }

    /// Retrieve RICH panel field is set
    [[nodiscard]] constexpr bool panelIsSet() const noexcept {
      return ( isPMT() ? 0 != ( ( key() & MaPMT::MaskPanelIsSet ) >> MaPMT::ShiftPanelIsSet )
                       : 0 != ( ( key() & HPD::MaskPanelIsSet ) >> HPD::ShiftPanelIsSet ) );
    }

    /// Retrieve RICH detector field is set
    [[nodiscard]] constexpr bool richIsSet() const noexcept {
      return ( isPMT() ? 0 != ( ( key() & MaPMT::MaskRichIsSet ) >> MaPMT::ShiftRichIsSet )
                       : 0 != ( ( key() & HPD::MaskRichIsSet ) >> HPD::ShiftRichIsSet ) );
    }

  public:
    /// Returns true if the RichSmartID32 contains valid RICH detector data
    [[nodiscard]] constexpr bool richDataAreValid() const noexcept { return richIsSet(); }

    /// Returns true if the RichSmartID32 contains valid PD data
    [[nodiscard]] constexpr bool pdDataAreValid() const noexcept {
      return ( pdIsSet() && panelIsSet() && richIsSet() );
    }

    /// Returns true if the RichSmartID32 contains valid pixel data
    [[nodiscard]] constexpr bool pixelDataAreValid() const noexcept {
      return ( pixelColIsSet() && pixelRowIsSet() && pdDataAreValid() );
    }

    /// Returns true if the RichSmartID32 contains valid pixel sub-row (Alice mode) data
    [[nodiscard]] constexpr bool pixelSubRowDataIsValid() const noexcept {
      return ( pixelSubRowIsSet() && pixelDataAreValid() );
    }

    /// Returns true if at least one data field has been set
    [[nodiscard]] constexpr bool isValid() const noexcept {
      return ( richIsSet() || panelIsSet() || pdIsSet() || pixelRowIsSet() || pixelColIsSet() || pixelSubRowIsSet() );
    }

  public:
    // PMT specific data

    /** Returns true if the SmartID is for a 'large' (H-type) PMT.
     *  @attention Will always return false for HPDs... */
    [[nodiscard]] constexpr bool isLargePMT() const noexcept {
      return ( isHPD() ? false : 0 != ( ( key() & MaPMT::MaskLargePixel ) >> MaPMT::ShiftLargePixel ) );
    }

    /// Alias method to explicitly ask if H-Type
    [[nodiscard]] constexpr bool isHTypePMT() const noexcept { return isLargePMT(); }

    /** Set the large PMT flag.
     *  @attention Does nothing for HPDs */
    constexpr void setLargePMT( const bool flag ) noexcept {
      assert( isPMT() );
      if ( isPMT() ) { setData( flag, MaPMT::ShiftLargePixel, MaPMT::MaskLargePixel ); }
    }

  public:
    // PMT specific derived information

    /// Number of PMTs per EC for this PMT type
    [[nodiscard]] constexpr DataType numPMTsPerEC() const
#ifdef NDEBUG
        noexcept
#endif
    {
      assert( isPMT() );
      return ( isPMT() ? ( isLargePMT() ? MaPMT::HTypePMTsPerEC : MaPMT::RTypePMTsPerEC )
                       // HPD fallback
                       : 1 );
    }

    /// Derive PMT elementry cell number from PMT number in module
    [[nodiscard]] constexpr DataType elementaryCell() const
#ifdef NDEBUG
        noexcept
#endif
    {
      assert( isPMT() );
      return ( isPMT() ? pdNumInMod() / numPMTsPerEC()
                       // HPD fallback
                       : 0 );
    }

    /// Derive PMT number within its elementry cell number from PMT number in module
    [[nodiscard]] constexpr DataType pdNumInEC() const
#ifdef NDEBUG
        noexcept
#endif
    {
      assert( isPMT() );
      return ( isPMT() ? pdNumInMod() % numPMTsPerEC()
                       // HPD fallback
                       : 0 );
    }

    /// Derive PMT anode index (0-63) from pixel column and row numbers
    [[nodiscard]] constexpr DataType anodeIndex() const
#ifdef NDEBUG
        noexcept
#endif
    {
      assert( isPMT() );
      return ( isPMT() ? ( pixelRow() * MaPMT::PixelsPerCol ) + ( MaPMT::PixelsPerRow - 1 - pixelCol() )
                       // HPD fallback
                       : 0 );
    }

    /// Returns the 'local' module number in each panel (i.e. starts at 0 in each panel)
    [[nodiscard]] constexpr DataType panelLocalModuleNum() const
#ifdef NDEBUG
        noexcept
#endif
    {
      assert( isPMT() );
      // For PMTs, module number minus panel offset
      return ( isPMT() ? pdMod() - MaPMT::PanelModuleOffsets[rich()][panel()]
                       // HPD fall back
                       : 0 );
    }

    /// Access the 'local' module column number (i.e. starts at 0 in each panel)
    [[nodiscard]] constexpr DataType panelLocalModuleColumn() const
#ifdef NDEBUG
        noexcept
#endif
    {
      assert( isPMT() );
      return ( isPMT() ? ( panelLocalModuleNum() / MaPMT::ModulesPerColumn )
                       // HPD fallback
                       : 0 );
    }

    /// Returns the 'local' module number in each column (i.e. starts at 0 in each column)
    [[nodiscard]] constexpr DataType columnLocalModuleNum() const
#ifdef NDEBUG
        noexcept
#endif
    {
      assert( isPMT() );
      return ( isPMT() ? ( panelLocalModuleNum() % MaPMT::ModulesPerColumn )
                       // HPD fall back
                       : 0 );
    }

    /// Returns a 'local' EC pixel x coordinate for each PMT corrected for the PMT rotations in the EC
    [[nodiscard]] constexpr DataType ecLocalPMTFrameX() const
#ifdef NDEBUG
        noexcept
#endif
    {
      assert( isPMT() );
      if ( isPMT() ) {
        if ( isHTypePMT() ) {
          return ( MaPMT::PixelsPerCol - 1 - pixelCol() );
        } else {
          const auto iPMT = pdNumInEC();
          assert( iPMT < numPMTsPerEC() );
          return ( 0 == iPMT ? pixelCol() :                                       //
                       1 == iPMT ? pixelRow() :                                   //
                           2 == iPMT ? ( MaPMT::PixelsPerRow - 1 - pixelRow() ) : //
                               ( MaPMT::PixelsPerCol - 1 - pixelCol() ) );
        }
      } else {
        // HPD fallback
        return 0u;
      }
    }

    /// Returns a 'local' EC pixel column number for each PMT corrected for the PMT rotations in the EC
    [[nodiscard]] constexpr DataType ecLocalPMTFrameY() const
#ifdef NDEBUG
        noexcept
#endif
    {
      assert( isPMT() );
      if ( isPMT() ) {
        if ( isHTypePMT() ) {
          return ( MaPMT::PixelsPerRow - 1 - pixelRow() );
        } else {
          const auto iPMT = pdNumInEC();
          assert( iPMT < numPMTsPerEC() );
          return ( 0 == iPMT ? pixelRow() :                                   //
                       1 == iPMT ? ( MaPMT::PixelsPerCol - 1 - pixelCol() ) : //
                           2 == iPMT ? pixelCol() :                           //
                               ( MaPMT::PixelsPerRow - 1 - pixelRow() ) );
        }
      } else {
        // HPD fallback
        return 0u;
      }
    }

    /// Returns a 'global' EC pixel row number for each PMT corrected for the PMT rotations in the EC
    [[nodiscard]] constexpr std::int32_t ecGlobalPMTFrameX() const
#ifdef NDEBUG
        noexcept
#endif
    {
      assert( isPMT() );
      static_assert( MaPMT::PDGlobalViewShift % 2 == 0 );
      if ( isPMT() ) {
        const auto iCol    = panelLocalModuleColumn();
        const auto iLocalX = ecLocalPMTFrameX();
        return ( isHTypePMT() ? ( iCol * MaPMT::PDGlobalViewShift ) + ( 2 * iLocalX ) + 1
                              : ( iCol * MaPMT::PDGlobalViewShift ) +
                                    ( ( pdNumInEC() % 2 ) * ( MaPMT::PDGlobalViewShift / 2 ) ) + iLocalX );
      } else {
        // HPD fallback
        return 0;
      }
    }

    /// Returns a 'global' EC pixel column number for each PMT corrected for the PMT rotations in the EC
    [[nodiscard]] constexpr std::int32_t ecGlobalPMTFrameY() const
#ifdef NDEBUG
        noexcept
#endif
    {
      assert( isPMT() );
      static_assert( MaPMT::PDGlobalViewShift % 2 == 0 );
      if ( isPMT() ) {
        const auto         iPDM    = columnLocalModuleNum();
        const auto         iEC     = elementaryCell();
        const auto         iLocalY = ecLocalPMTFrameY();
        const std::int32_t shift =
            ( ( MaPMT::ModulesPerColumn - 1 - iPDM ) * MaPMT::PDGlobalViewShift * MaPMT::ECsPerModule ) +
            ( ( MaPMT::ECsPerModule - 1 - iEC ) * MaPMT::PDGlobalViewShift ) - MaPMT::PDGlobalViewRangeY[rich()];
        return ( isHTypePMT() ? shift + ( ( 2 * iLocalY ) + 1 )
                              : shift + ( ( 1 - ( pdNumInEC() / 2 ) ) * ( MaPMT::PDGlobalViewShift / 2 ) ) + iLocalY );
      } else {
        // HPD fallback
        return 0;
      }
    }

    /// Returns a 'global' EC pixel row number for each PMT corrected for the PMT rotations in the EC
    [[nodiscard]] constexpr std::int32_t ecGlobalPMTFrameX2() const
#ifdef NDEBUG
        noexcept
#endif
    {
      assert( isPMT() );
      static_assert( MaPMT::PDGlobalViewShift % 2 == 0 );
      if ( isPMT() ) {
        const auto iCol    = panelLocalModuleColumn();
        const auto iLocalX = -ecLocalPMTFrameX();
        return ( isHTypePMT() ? ( iCol * MaPMT::PDGlobalViewShift ) + ( 2 * iLocalX ) + 1
                              : ( iCol * MaPMT::PDGlobalViewShift ) -
                                    ( ( pdNumInEC() % 2 ) * ( MaPMT::PDGlobalViewShift / 2 ) ) + iLocalX );
      } else {
        // HPD fallback
        return 0;
      }
    }

  public:
    // messaging

    /// ostream operator
    friend std::ostream& operator<<( std::ostream& str, const RichSmartID32& id ) { return id.fillStream( str ); }

    /// Print this RichSmartID32 in a human readable way
    std::ostream& fillStream( std::ostream& s ) const;

    /** Return the output of the ostream printing of this object as a string.
     *  Mainly for use in GaudiPython. */
    [[nodiscard]] std::string toString() const;

  private:
    // Utilities

    /// Test if a given bit in the ID is on
    [[nodiscard]] constexpr bool isBitOn( const int32_t pos ) const noexcept {
      return ( 0 != ( key() & ( KeyType{1} << pos ) ) );
    }

    /// Print the ID as a series of bits (0/1)
    std::ostream& dumpBits( std::ostream& s ) const;
  };

} // namespace LHCb
