/*****************************************************************************\
* (c) Copyright 2023 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once

#include <cstddef>
#include <functional>
#include <utility>

namespace LHCb {

  namespace details {
    struct AlwaysTrue {
      template <typename T>
      constexpr bool operator()( const T& ) const noexcept {
        return true;
      }
    };
  } // namespace details

  /**
   * @brief OutputIterator struct that increments a count when using the assignment operator and Predicate is true.
   *
   * @tparam Predicate
   *
   * @details Fulfills https://en.cppreference.com/w/cpp/named_req/OutputIterator
   * and can be used e.g. in a std::set_intersection counting only elements fulfilling the predicate.
   */
  template <typename Predicate>
  class CountIfIterator {
    std::size_t m_count{0};
    Predicate   m_predicate;

  public:
    explicit CountIfIterator( Predicate pred = {} ) : m_predicate{std::move( pred )} {}
    CountIfIterator& operator++() { return *this; }
    CountIfIterator& operator*() { return *this; }
    template <typename T>
    CountIfIterator& operator=( const T& data ) {
      if ( std::invoke( m_predicate, data ) ) ++m_count;
      return *this;
    }

    auto count() { return m_count; }
  };

  /**
   * @brief OutputIterator struct that increments a count when using the assignment operator.
   * @details Fulfills https://en.cppreference.com/w/cpp/named_req/OutputIterator
   * and can be used e.g. in a std::set_intersection.
   */
  using CountIterator = CountIfIterator<details::AlwaysTrue>;

} // namespace LHCb
