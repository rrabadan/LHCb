/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once
#include "EventLocalAllocator.h"
#include <algorithm>
#include <array>
#include <cassert>
#include <numeric>
#include <tuple>
#include <vector>
#if __cplusplus >= 202002
#  include <ranges>
namespace LHCb::Container::detail {
  namespace ranges = std::ranges;
}
#else
#  include <range/v3/view/subrange.hpp>
#  ifndef NDEBUG
#    include <range/v3/algorithm.hpp>
#    include <range/v3/version.hpp>
#    include <range/v3/view.hpp>
#  endif
namespace LHCb::Container::detail {
  namespace ranges = ::ranges;
}
#endif

namespace LHCb::Container {

  /**
   * Desirably efficient container of hits, in effect a
   * smarter wrapper around a 1D vector and an array of offsets.
   *
   * In effect, the collection of Hits* for the
   * n-dimensional detector geometry* elements is mapped to a
   * 1-dimensional vector of Hits*, together with an array
   * of offsets. Sorting by a general comparison function is
   * supported.
   *
   * Everything highlighted with the asterisk is a template argument
   * for the MultiIndexedContainer.
   *
   * Example use-cases are the PrFTHitHandler and the PrUTHitHandler.
   * In general, you want Hit Handlers to be the front-end to this
   * container.
   *
   * Numbering always starts at 0.
   *
   * TODO:
   *  - Write a story about why there is some special behaviour for the
   * single-hit insertion, such that an explicit sort *must* be called
   * in that situation.
   *
   *  - Main concerns: Laurent thinks it's probably beneficial to add a trivial
   * implementation of the getUniqueDetectorId for the 1D-case, which just
   * is the identity function. But be wary, as a 1D-call to the
   * getHits for a nested subdetector gives you all hits for the children
   * subdetectors, while the speed gain is really only in the 1D-Call/1D detector
   * case.
   *
   *  - Right now there is an assert for the number of arguments and the nesting of
   * subdetectors. However, there is no assert whether the sub-detector ID might
   * exceed the
   *
   *  - Support addHit([subdetectorId], hit) instead of just addHit(hit, [subdetectorId])
   *
   * @author  Roel Aaij <roel.aaij@cern.ch>,
   * 			Laurent Dufour <laurent.dufour@cern.ch>,
   * 			Renato Quagliani <Renato.Quagliani@cern.ch>,
   * 			Gerhard Raven <gerhard.raven@nikhef.nl>
   */

  namespace detail {
    template <size_t... J, size_t... I>
    constexpr auto prod( std::index_sequence<I...> ) {
      return ( ... * std::get<I>( std::array{J...} ) );
    }

    template <size_t N, size_t... I>
    constexpr auto pivot( std::index_sequence<I...> ) {
      return std::index_sequence<N - I...>{};
    }

    template <size_t... Extent>
    struct Extents {
      template <size_t Dim>
      static constexpr int size() {
        return std::get<Dim>( std::array{Extent...} );
      }

      template <int M>
      static constexpr int stride() {
        constexpr auto N = sizeof...( Extent );
        static_assert( M < N );
        if constexpr ( M == N - 1 ) {
          return 1;
        } else {
          return prod<Extent...>( pivot<N - 1>( std::make_index_sequence<( N - 1 ) - M>() ) );
        }
      }

      template <typename T, size_t N, size_t... Is>
      static constexpr int index_offset( std::array<T, N> i, std::index_sequence<Is...> ) {
        return ( ... + ( std::get<Is>( i ) * stride<Is>() ) );
      }

      template <typename... Is>
      static constexpr int offset( Is... is ) {
        return index_offset( std::array{is...}, std::index_sequence_for<Is...>{} );
      }
    };
  } // namespace detail

  template <class Hit, size_t... sizes>
  class MultiIndexedContainer {
  public:
    using offset_t        = size_t;
    using const_reference = const Hit&;
    using value_type      = Hit;
    using allocator_type  = LHCb::Allocators::EventLocal<value_type>;

    using Hits                   = typename std::vector<value_type, allocator_type>;
    using iterator               = typename Hits::iterator;
    using const_iterator         = typename Hits::const_iterator;
    using const_reverse_iterator = typename Hits::const_reverse_iterator;

    /**
     * For the offsets we reserve one more, to make the calculation of the
     * number of hits quicker (can always do n(uniqueSubDetId).second - n(uniqueSubDetId).first )
     */
    using Offsets = std::array<std::pair<offset_t, offset_t>, ( ... * sizes )>;
    using Ids     = std::array<size_t, ( ... * sizes )>;

    using HitRange = detail::ranges::subrange<const_iterator>;

    /**
     * Constructor for a relatively empty hit container.
     */
    MultiIndexedContainer( std::size_t reserve = 0, allocator_type alloc = {} ) : m_hits{alloc} {
      if ( reserve != 0 ) m_hits.reserve( reserve );
      clearOffsets();
      std::fill( begin( m_nIds ), end( m_nIds ), 0u );
    }

    MultiIndexedContainer( Hits&& hits, Offsets&& offsets )
        : m_hits( std::move( hits ) ), m_offsets( std::move( offsets ) ) {}

    [[nodiscard]] allocator_type get_allocator() const { return m_hits.get_allocator(); }
    void                         reserve( size_t reserve ) { m_hits.reserve( reserve ); }

    template <typename... Args>
    std::pair<iterator, iterator> range_( Args&&... args ) {
      auto rng = getOffset( std::forward<Args>( args )... );
      return {std::next( begin( m_hits ), rng.first ), std::next( begin( m_hits ), rng.second )};
    }

    template <typename... Args>
    HitRange range( Args&&... args ) const {
      auto rng = getOffset( std::forward<Args>( args )... );
      return {std::next( begin( m_hits ), rng.first ), std::next( begin( m_hits ), rng.second )};
    }

    /**
     * Returns a range of all the hits in this container.
     *
     * The sorting of this range depends on the possibly
     * used sort() method, but is always segmented
     * in unique detector ids.
     */
    HitRange range() const { return {std::begin( m_hits ), std::end( m_hits )}; }

    Hit& hit( size_t index ) { return m_hits[index]; }

    const Hit& hit( size_t index ) const { return m_hits[index]; }

    /**
     * Returns the total number of hits in this container.
     */
    [[nodiscard]] size_t size() const { return m_hits.size(); }

    /**
     * Returns the number of hits in this container for a given
     * sub-detector.
     */
    template <typename... Args>
    constexpr size_t size( Args&&... args ) const {
      auto [obegin, oend] = getOffset( std::forward( args )... );
      assert( oend >= obegin && "ill-formed offsets" );
      return oend - obegin;
    }

    template <typename... Args>
    constexpr bool empty( Args&&... args ) const {
      auto [obegin, oend] = getOffset( std::forward( args )... );
      assert( oend >= obegin && "ill-formed offsets" );
      return oend == obegin;
    }

    static constexpr size_t nSubDetectors() { return ( ... * sizes ); }

    template <size_t N>
    static constexpr size_t subSize() {
      return std::get<N>( std::array{sizes...} );
    }

    /**
     * Function to insert a range of hits in (detectorElementId).
     *
     * The argument are two r-valued references, but in practice this will
     * take its form as two iterators over the hits to be inserted.
     *
     * The responsibility of passing these iterators and generating the
     * hits to be inserted is found in the HitManagers
     * (e.g. {@see TStationHitManager::i_prepareHits}).
     *
     * Current practice: give a range instead of the raw begin() and end().
     *
     */
    template <typename I, typename... Args>
    void insert( I&& b, I&& e, Args&&... args ) {
      auto n = std::distance( b, e );
      m_hits.reserve( m_hits.size() + n );

      const auto subDetectorId = getUniqueDetectorElementId( std::forward<Args>( args )... );

      m_hits.insert( std::next( begin( m_hits ), m_offsets[subDetectorId].second ), std::forward<I>( b ),
                     std::forward<I>( e ) );

      // Add number of entries to the end offset,
      // and add the offsets to the next uniqueDetectorIds
      m_offsets[subDetectorId].second += n;
      m_nIds[subDetectorId] += n;

      for ( size_t i = subDetectorId + 1; i < nSubDetectors(); ++i ) {
        m_offsets[i] = {m_offsets[i].first + n, m_offsets[i].second + n};
      }
    }

    /**
     * Function to create a single hit in (detectorElementId).
     * A reference to the hit created is returned.
     *
     * The user of this function is responsible for giving the right
     * constructor arguments. Unexpected errors can occur when this does not
     * match the constructor of Hit.
     *
     * Note that due to the implementation of the addHit algorithm,
     * one should explicitly call setOffsets before further using the
     * hit manager.
     */
    template <typename Tuple, typename... LocArgs>
    Hit& addHit( Tuple&& cargs, LocArgs&&... subDetectorElement ) {
      static_assert( ( sizeof...( subDetectorElement ) <= sizeof...( sizes ) ),
                     "The number of indices provided is strictly higher than the nesting for this subdetector." );

      auto id = getUniqueDetectorElementId( std::forward<LocArgs>( subDetectorElement )... );
#ifndef NDEBUG
      m_ids.emplace_back( id );
#endif
      ++m_nIds[id];

      return std::apply(
          [this]( auto&&... args ) -> decltype( auto ) {
            return m_hits.emplace_back( std::forward<decltype( args )>( args )... );
          },
          std::forward<Tuple>( cargs ) );
    }

    void setOffsets() {
      // Set the offsets
      constexpr auto nSub = nSubDetectors();
      m_offsets[0]        = {0, m_nIds[0]};
      for ( size_t i = 1; i < nSub; ++i ) {
        m_offsets[i] = {m_offsets[i - 1].second, m_offsets[i - 1].second + m_nIds[i]};
      }
    }

    /**
     * Explicitly sorts the *entire* container based on the
     * ordering function defined in the Hit class.
     *
     * This ordering must include the index parameter used to
     * construct this container by.
     *
     * The offsets are updated afterwards to reflect the new
     * ordering.
     */
    void forceSort() {
      typename Hit::Order my_ordering{};
      std::sort( m_hits.begin(), m_hits.end(), my_ordering );

      setOffsets();
    }

    /**
     * This method, which *only works well on a sorted hit container*,
     * checks whether two consecutive hits are identical, where identical
     * is defined by the provided function.
     *
     * The implementation assumes that more than one hit typically has to
     * be removed, which makes using a separate vector probably cheaper
     * than using erase / remove from the internal hits vector.
     *
     * Note:
     * The trivial function which calls the == operator of the Hits
     * is obvious, but not always the desired choice: in 2024 data
     * taking, double hits which came from the same channelID, but
     * with different detailed properties (clusterCharge etc) were
     * observed for the UT.
     * However, it *is* assumed that the things that are considered
     * unique by the provided function, are consecutively placed
     * in the container by the defined Sorting of the hits!
     **/
    template <typename Functor>
    void forceUnique( Functor are_equal ) {
      if ( m_hits.empty() ) return;

      Ids  new_ids  = m_nIds;
      Hits new_hits = {};

      new_hits.reserve( m_hits.size() );
      Hit previousHit = m_hits[0];

      for ( unsigned int uniqueDetectorId = 0; uniqueDetectorId < nSubDetectors(); ++uniqueDetectorId ) {
        for ( const auto& hit : range( uniqueDetectorId ) ) {
          if ( are_equal( hit, previousHit ) && !new_hits.empty() ) {
            --new_ids[uniqueDetectorId];
            continue;
          } else {
            previousHit = hit;
            new_hits.emplace_back( hit );
          }
        }
      }

      m_hits = new_hits;
      m_nIds = new_ids;

      setOffsets();
    }

    template <typename...>
    struct always_false : std::bool_constant<false> {};

    /**
     * Function used only for debug ordering check
     */
#ifndef NDEBUG
    template <typename COMPARE>
    bool is_sorted( const COMPARE& compare ) const {
      //
      // Zip the ids and hits together
#  if __cplusplus == 202002
      // workaround for https://github.com/root-project/root/issues/13001 until fixed or C++23
      const std::size_t count = std::min( m_ids.size(), m_hits.size() );

      std::vector<std::tuple<unsigned, const_reference>> zipped;
      zipped.reserve( count );
      for ( std::size_t i = 0; i < count; ++i ) { zipped.emplace_back( m_ids[i], m_hits[i] ); }
#  else
      auto zipped = detail::ranges::views::zip( m_ids, m_hits );
#  endif
      // Sorting predicate that defers hit base sorting to the compare
      // function passed in
      auto pred = [&compare]( const std::tuple<unsigned, Hit>& t1, const std::tuple<unsigned, Hit>& t2 ) {
        if ( std::get<0>( t1 ) < std::get<0>( t2 ) ) {
          return true;
        } else if ( std::get<0>( t2 ) < std::get<0>( t1 ) ) {
          return false;
        } else {
          return compare( std::get<1>( t1 ), std::get<1>( t2 ) );
        }
      };

      return std::is_sorted( zipped.begin(), zipped.end(), pred );
    }
#endif

    auto offsets() const { return m_offsets; }

    void clear() {
      m_hits.clear();
      clearOffsets();
      std::fill( std::begin( m_nIds ), std::end( m_nIds ), 0 );
    }

    template <typename... I>
    static constexpr int getUniqueDetectorElementId( I... i ) {
      return detail::Extents<sizes...>::offset( i... );
    }

  private:
    Hits m_hits;

    Offsets m_offsets;

    /**
     * These two arrays are here to enable faster functionality
     * for adding hits one-by-one.
     */
#ifndef NDEBUG
    std::vector<unsigned> m_ids; // used only for debug
#endif
    Ids m_nIds;

    template <typename... Args>
    constexpr typename Offsets::value_type getOffset( Args&&... args ) const {
      auto uniqueDetectorElementId = getUniqueDetectorElementId( std::forward<Args>( args )... );
      return m_offsets[uniqueDetectorElementId];
    }

    void clearOffsets() {
      constexpr auto zero = typename Offsets::value_type{0, 0};
      std::fill( std::begin( m_offsets ), std::end( m_offsets ), zero );
    }
  };

} // namespace LHCb::Container
