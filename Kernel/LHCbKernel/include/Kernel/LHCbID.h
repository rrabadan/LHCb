/*****************************************************************************\
* (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#pragma once

#include "Detector/Calo/CaloCellID.h"
#include "Detector/FT/FTChannelID.h"
#include "Detector/Muon/TileID.h"
#include "Detector/UT/ChannelID.h"
#include "Detector/VP/VPChannelID.h"
#include "Kernel/RichSmartID.h"
#include <cstdint>
#include <ostream>

namespace LHCb {
  inline namespace v2 {

    /** @class LHCbID LHCbID.h
     *
     * LHCb wide channel identifier
     *
     * @author Marco Cattaneo
     *
     */
    class LHCbID final {
    public:
      /// types of sub-detector channel ID
      enum class channelIDtype {
        UNDEFINED                    = 0,
        Plume                        = 1,
        VP                           = 2,
        UT                           = 3,
        reserved_magnet_sidestations = 4,
        FT                           = 5,
        reserved_mighty_tracker      = 6,
        reserved_torch               = 7,
        Rich                         = 8,
        Calo                         = 9,
        Muon                         = 10
      };

      /// Default Constructor
      constexpr LHCbID() = default;

      /// Constructor from unsigned int
      explicit constexpr LHCbID( const unsigned int theID ) : m_lhcbID( theID ) {}

      /// Constructor from type and unsigned int id
      constexpr LHCbID( const channelIDtype t, const unsigned int id )
          : m_lhcbID{( ( static_cast<unsigned int>( t ) << detectorTypeBits ) & detectorTypeMask ) | ( id & IDMask )} {
        assert( ( id & IDMask ) == id );
      }

      /// allow SIMD implementations to work with the bit representation....
      template <typename int_v>
      static constexpr auto make( const channelIDtype t, const int_v v ) {
        return ( ( static_cast<unsigned int>( t ) << detectorTypeBits ) & detectorTypeMask ) + v;
      }

      /// Constructor from VPChannelID, VP corresponding to pixel solution for upgrade
      constexpr LHCbID( const Detector::VPChannelID chanID ) : LHCbID{channelIDtype::VP, chanID} {}

      /// Constructor from UTChannelID
      constexpr LHCbID( const Detector::UT::ChannelID chanID ) : LHCbID{channelIDtype::UT, chanID} {}

      /// Constructor from RichSmartID
      constexpr LHCbID( const RichSmartID& chanID )
          : LHCbID{channelIDtype::Rich, (std::uint32_t)chanID.dataBitsOnly()} {
        // Save the MaPMT/HPD flag in bit 27
        setBit( 27, chanID.idType() );
        // Set the validity bits
        setBit( 26, chanID.pixelDataAreValid() );
      }

      /// Constructor from Calo::CellID
      constexpr LHCbID( const Detector::Calo::CellID chanID ) : LHCbID{channelIDtype::Calo, chanID.all()} {}

      /// Constructor from MuonTileID
      constexpr LHCbID( const Detector::Muon::TileID& chanID ) {
        // store lower part as it is // 18 bits
        const auto lhcbid_muon = chanID.key();
        const auto compactLay =
            ( ( chanID & Detector::Muon::Base::MaskLayoutX ) >> Detector::Muon::Base::ShiftLayoutX ) +
            ( ( chanID & Detector::Muon::Base::MaskLayoutY ) >> Detector::Muon::Base::ShiftLayoutY ) *
                Detector::Muon::Base::max_compacted_xGrid;
        m_lhcbID = ( ( static_cast<unsigned int>( channelIDtype::Muon ) << detectorTypeBits ) & detectorTypeMask ) |
                   ( ( compactLay << Detector::Muon::Base::ShiftCompactedLayout ) &
                     Detector::Muon::Base::MaskCompactedLayout ) |
                   ( lhcbid_muon & Detector::Muon::Base::MaskKey ); // 18 bits
      }

      /// Constructor from FTChannelID
      constexpr LHCbID( const Detector::FTChannelID chanID ) : LHCbID{channelIDtype::FT, chanID} {}

    public:
      /// Retrieve const  the internal representation
      constexpr auto lhcbID() const noexcept { return m_lhcbID; }

      /// Update the ID bits (to recreate the channelID)
      constexpr LHCbID& setID( const unsigned int value ) noexcept {
        m_lhcbID &= ~IDMask;
        m_lhcbID |= value & IDMask;
        return *this;
      }

      /// Retrieve the LHCb detector type bits
      constexpr auto detectorType() const noexcept {
        return channelIDtype( ( lhcbID() & detectorTypeMask ) >> detectorTypeBits );
      }

      /// Update the LHCb detector type bits
      constexpr LHCbID& setDetectorType( const channelIDtype value ) noexcept {
        const auto val = static_cast<unsigned int>( value );
        m_lhcbID &= ~detectorTypeMask;
        m_lhcbID |= ( val << detectorTypeBits ) & detectorTypeMask;
        return *this;
      }

    public:
      /// Does this LHCbID have a defined type
      constexpr bool isDefined() const noexcept { return channelIDtype::UNDEFINED != detectorType(); }

      /// return true if this is a VP identifier
      constexpr bool isVP() const noexcept { return channelIDtype::VP == detectorType(); }

      /// return the VPChannelID
      constexpr auto vpID() const noexcept { return Detector::VPChannelID( isVP() ? lhcbID() & IDMask : 0xF0000000 ); }

      /// return true if this is a UT Silicon Tracker identifier
      constexpr bool isUT() const noexcept { return channelIDtype::UT == detectorType(); }

      /// return the ChannelID
      constexpr auto utID() const noexcept {
        return ( isUT() ? Detector::UT::ChannelID( lhcbID() & IDMask ) : Detector::UT::ChannelID( 0xF0000000 ) );
      }

      /// return true if this is a Rich identifier
      constexpr bool isRich() const noexcept { return channelIDtype::Rich == detectorType(); }

      /// return the richSmartID
      constexpr auto richID() const {
        // Create the RichSMartID data bits
        const RichSmartID::KeyType data( isRich() ? ( lhcbID() & IDMask ) : 0 );
        // Create a temporary RichSmartID
        RichSmartID tmpid( data );
        // Retrieve the MaPMT/HPD flag
        if ( isRich() ) { tmpid.setIDType( static_cast<RichSmartID::IDType>( testBit( 27 ) ) ); }
        // Object to return, with RICH and panel fields set
        RichSmartID id( tmpid.rich(), tmpid.panel(), tmpid.pdNumInCol(), tmpid.pdCol(), tmpid.idType() );
        // Set pixels fields
        if ( testBit( 26 ) ) {
          id.setPixelRow( tmpid.pixelRow() );
          id.setPixelCol( tmpid.pixelCol() );
          if ( tmpid.idType() == RichSmartID::HPDID ) { id.setPixelSubRow( tmpid.pixelSubRow() ); }
        }
        // return
        return id;
      }

      /// return true if this is a Calo identifier
      constexpr bool isCalo() const noexcept { return channelIDtype::Calo == detectorType(); }

      /// return the CaloCellID
      constexpr auto caloID() const noexcept {
        return isCalo() ? Detector::Calo::CellID( lhcbID() & IDMask ) : Detector::Calo::CellID( 0xF0000000 );
      }

      /// return true if this is a Muon identifier
      constexpr bool isMuon() const noexcept { return channelIDtype::Muon == detectorType(); }

      /// return the TileID
      constexpr auto muonID() const noexcept {
        Detector::Muon::TileID id( 0xF0000000 );
        if ( isMuon() ) {
          const Detector::Muon::TileID tmpid( lhcbID() & 0x3FFFF );
          const auto                   compacted_layout =
              ( lhcbID() & Detector::Muon::Base::MaskCompactedLayout ) >> Detector::Muon::Base::ShiftCompactedLayout;
          id = Detector::Muon::TileID(
              tmpid.key(), Detector::Muon::Layout( compacted_layout % Detector::Muon::Base::max_compacted_xGrid,
                                                   compacted_layout / Detector::Muon::Base::max_compacted_xGrid ) );
        }
        return Detector::Muon::TileID( id );
      }

      /// return true if this is a Fibre Tracker identifier
      constexpr bool isFT() const noexcept { return channelIDtype::FT == detectorType(); }

      /// return the FTChannelID
      constexpr auto ftID() const noexcept {
        return isFT() ? Detector::FTChannelID( lhcbID() & IDMask ) : Detector::FTChannelID( 0xF0000000 );
      }

      /// Check the LHCbID sub-detector channel ID type identifier
      constexpr bool checkDetectorType( const LHCbID::channelIDtype type ) const noexcept {
        return type == detectorType();
      }

      /// General ID: returns detector ID = internal unsigned int
      constexpr unsigned int channelID() const {
        switch ( detectorType() ) {
        case channelIDtype::VP:
          return vpID().channelID();
        case channelIDtype::UT:
          return utID().channelID();
        case channelIDtype::Rich:
          return richID().key();
        case channelIDtype::Calo: // C++17 [[ fall-through ]]
        case channelIDtype::Muon:
          return lhcbID() & IDMask;
        case channelIDtype::FT:
          return ftID().channelID();
        default:
          return 0;
        }
      }

    public:
      /// comparison equality
      constexpr friend bool operator==( const LHCbID lhs, const LHCbID rhs ) { return lhs.lhcbID() == rhs.lhcbID(); }

      /// comparison ordering
      constexpr friend bool operator<( const LHCbID lhs, const LHCbID rhs ) { return lhs.lhcbID() < rhs.lhcbID(); }

    public:
      /// Print this LHCbID in a human readable way
      std::ostream& fillStream( std::ostream& s ) const;

      friend std::ostream& operator<<( std::ostream& str, const LHCbID& obj ) { return obj.fillStream( str ); }

    private:
      /// Offsets of bitfield lhcbID
      static constexpr auto detectorTypeBits = 28;

      /// Bitmasks for bitfield lhcbID
      enum lhcbIDMasks { IDMask = 0xfffffffL, detectorTypeMask = 0xf0000000L };

    private:
      /// Checks if a given bit is set
      constexpr bool testBit( const unsigned int pos ) const noexcept { return ( 0 != ( lhcbID() & ( 1 << pos ) ) ); }

      /// Sets the given bit to the given value
      constexpr LHCbID& setBit( const unsigned int pos, const unsigned int value ) noexcept {
        m_lhcbID |= value << pos;
        return *this;
      }

    private:
      /// the internal representation
      unsigned int m_lhcbID{0};

    }; // class LHCbID

    inline std::ostream& operator<<( std::ostream& s, const LHCbID::channelIDtype e ) {
      switch ( e ) {
      case LHCbID::channelIDtype::VP:
        return s << "VP";
      case LHCbID::channelIDtype::FT:
        return s << "FT";
      case LHCbID::channelIDtype::UT:
        return s << "UT";
      case LHCbID::channelIDtype::Rich:
        return s << "Rich";
      case LHCbID::channelIDtype::Calo:
        return s << "Calo";
      case LHCbID::channelIDtype::Muon:
        return s << "Muon";
      case LHCbID::channelIDtype::Plume:
        return s << "Plume";
      default:
        return s << "ERROR wrong value " << int( e ) << " for enum LHCbID::channelIDtype";
      }
    }

  } // namespace v2
} // namespace LHCb
