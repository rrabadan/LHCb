###############################################################################
# (c) Copyright 2023 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
import torch
import numpy as np
from torch import nn
from enum import Enum
import re
from cppyy import gbl
import warnings

LayerBase = gbl.LHCb.VectorizedML.Layers.LayerBase
LayerType = gbl.LHCb.VectorizedML.Layers.Type

__supported_preprocessing_layers__ = [LayerType.StandardScaler]
__supported_postprocessing_layers__ = [LayerType.PiecewiseLinearTransform]
__supported_transformation_layers__ = list(
    set(__supported_preprocessing_layers__ +
        __supported_postprocessing_layers__))


def to_numpy_array(x):
    if torch.is_tensor(x):
        return x.detach().cpu().numpy()
    else:
        return x


class TransformationLayer:
    def __init__(self, cpp_layer: LayerBase):
        # Get basic info
        self.m_layer_name = cpp_layer.name()
        self.m_layer_type = cpp_layer.type()
        self.m_num_input = cpp_layer.nInputs()
        self.m_num_output = cpp_layer.nOutputs()
        self.m_num_bins = cpp_layer.nBins()
        self.m_layer_index = cpp_layer.index()

    def layer_name(self):
        return self.m_layer_name

    def layer_type(self):
        return self.m_layer_type

    def num_input(self):
        return self.m_num_input

    def num_output(self):
        return self.m_num_output

    def num_bins(self):
        return self.m_num_bins

    def m_layer_index(self):
        return self.m_layer_index

    def write_to_json(self, data):
        raise NotImplementedError()

    def read_from_json(self, data):
        raise NotImplementedError()

    def forward(self, input):
        raise NotImplementedError()


class PiecewiseLinearTransform(TransformationLayer):
    def __init__(self, cpp_layer: LayerBase):
        super().__init__(cpp_layer)
        self.m_quantiles = np.array([])

    def write_to_json(self, data):
        data[f'{self.m_layer_name}.quantiles'] = self.m_quantiles

    def read_from_json(self, data):
        self.m_quantiles = to_numpy_array(
            data.pop(f'{self.m_layer_name}.quantiles'))

    def forward(self, x):
        def get_index(val, array):
            if val < array[0]: return 0
            for i in range(len(array) - 1):
                if (val >= array[i]) and (val < array[i + 1]):
                    return i
            return len(array) - 2

        def get_yval(val, idx):
            xlow = self.m_quantiles[idx]
            xup = self.m_quantiles[idx + 1]
            dy = (self.m_quantiles[-1] - self.m_quantiles[0]) / self.num_bins()
            ylow = idx * dy + self.m_quantiles[0]
            yup = ylow + dy
            scale = dy / (xup - xlow)
            offset = ylow - scale * xlow
            return scale * val + offset

        return np.array(
            [get_yval(val, get_index(val, self.m_quantiles)) for val in x])

    def add_flatten_layer(self, data, bounds, smoothen=False):
        from scipy import stats
        assert (len(bounds) == 2)
        qbins = np.array(
            [i / (self.num_bins()) for i in range(1, self.num_bins())])
        self.m_quantiles = np.array([bounds[0]] + list(
            stats.mstats.mquantiles(data, qbins)) + [bounds[1]])
        if smoothen:
            # option to smoothen the quantiles to reduce stat. fluctuations in behaviour
            from scipy.interpolate import UnivariateSpline
            diffs = [
                self.m_quantiles[i + 1] - self.m_quantiles[i]
                for i in range(len(self.m_quantiles) - 1)
            ]
            xvals = [i / len(diffs) for i in range(len(diffs))]
            spline = UnivariateSpline(xvals, diffs)
            new_diffs = spline(xvals)
            # ensure bounds by normalization
            new_diffs = new_diffs / sum(new_diffs)
            new_edges = [0]
            for diff in new_diffs:
                new_edges.append(diff + new_edges[-1])
            self.m_quantiles = np.array([bounds[0]] + new_edges[1:-1] +
                                        [bounds[1]])


class StandardScaler(TransformationLayer):
    def __init__(self, cpp_layer: LayerBase):
        super().__init__(cpp_layer)
        self.m_mean = np.array([])
        self.m_standard_deviation = np.array([])

    def write_to_json(self, data):
        data[f'{self.m_layer_name}.mean'] = self.m_mean
        data[
            f'{self.m_layer_name}.standard_deviation'] = self.m_standard_deviation

    def read_from_json(self, data):
        self.m_mean = to_numpy_array(data.pop(f'{self.m_layer_name}.mean'))
        self.m_standard_deviation = to_numpy_array(
            data.pop(f'{self.m_layer_name}.standard_deviation'))

    def forward(self, x):
        return (x - self.m_mean) / self.m_standard_deviation


def fetch_transformation_layer(cpp_layer: LayerBase):
    layer_type = cpp_layer.type()
    if layer_type == LayerType.PiecewiseLinearTransform:
        return PiecewiseLinearTransform(cpp_layer)
    elif layer_type == LayerType.PiecewiseLinearTransform:
        return StandardScaler(cpp_layer)
    else:
        return None


def fetch_torch_layer(cpp_layer: LayerBase):
    layer_type = cpp_layer.type()
    if layer_type == LayerType.Linear:
        return nn.Linear(cpp_layer.nInputs(), cpp_layer.nOutputs())
    elif layer_type == LayerType.Sigmoid:
        return nn.Sigmoid()
    elif layer_type == LayerType.ReLU:
        return nn.ReLU()
    else:
        return None


class Sequence(nn.Module):
    def __init__(self, model):
        super().__init__()
        self.m_preprocessing_layer = None
        self.m_postprocessing_layer = None

        mod = model.model()
        nLayers = mod.nLayers()

        # build layers
        from collections import OrderedDict
        pytorch_stack = OrderedDict()

        cpp_layers = [mod.get_layer(i) for i in range(nLayers)]
        for layer_idx, cpp_layer in enumerate(cpp_layers):
            layer_name = cpp_layer.name()
            layer_type = cpp_layer.type()
            torch_layer = fetch_torch_layer(cpp_layer)
            if torch_layer:
                assert (layer_name not in pytorch_stack)
                pytorch_stack[layer_name] = torch_layer
                continue

            transformation_layer = fetch_transformation_layer(cpp_layer)
            if transformation_layer:
                if (layer_idx == 0
                        and layer_type in __supported_preprocessing_layers__):
                    self.m_preprocessing_layer = transformation_layer
                elif (layer_idx == (nLayers - 1)
                      and layer_type in __supported_postprocessing_layers__):
                    self.m_postprocessing_layer = transformation_layer
                else:
                    raise RuntimeError(
                        f"The layer {layer_name} with type {layer_type} is identified as a transformation layer, but it's invalid."
                    )
                continue

            # If layer is not valid pytorch layer or valid transformation layer, we raise error here
            raise RuntimeError(
                f"The layer {layer_name} with type {layer_type} is not a valid layer."
            )

        self._layer_names = [layer.name() for layer in cpp_layers]
        self._stack = nn.Sequential(pytorch_stack)

        feats = model.features()
        self._features = [feats.name(i) for i in range(feats.size())]

    def forward(self, x):
        return self._stack(x)

    def features(self):
        return self._features

    def write_to_json(self, filename):
        # save weights dictionary to json
        import json
        from json import JSONEncoder
        from torch.utils.data import Dataset

        class EncodeTensor(JSONEncoder, Dataset):
            def default(self, obj):
                if isinstance(obj, torch.Tensor):
                    return obj.cpu().detach().numpy().tolist()
                if isinstance(obj, np.ndarray):
                    return obj.tolist()
                return super(EncodeTensor, self).default(obj)

        with open(filename, 'w') as json_file:
            # allow versioning of files for unforeseen changes
            # and feature names for checking proper model loading
            data = {'metadata': {'version': 1, 'features': self._features}}
            # save torch parameters
            data.update(self.state_dict())
            # save non-torch layer info if needed
            if self.m_preprocessing_layer:
                self.m_preprocessing_layer.write_to_json(data)
            if self.m_postprocessing_layer:
                self.m_postprocessing_layer.write_to_json(data)
            json.dump(data, json_file, cls=EncodeTensor)

    def read_from_json(self, filename):
        import json

        class DecodeListToTensor(json.JSONDecoder):
            def decode(self, s):
                obj = super().decode(s)
                if not isinstance(obj, dict): return obj

                for key, value in obj.items():
                    if not isinstance(value, list): continue
                    obj[key] = torch.Tensor(np.array(value))
                return obj

        with open(filename, 'r') as json_file:
            data = json.load(json_file, cls=DecodeListToTensor)
            # first extract all non-torch info
            metadata = data['metadata']
            assert (self._features == data['metadata']['features'])
            del data['metadata']
            # transformation layers
            if self.m_preprocessing_layer:
                self.m_preprocessing_layer.read_from_json(data)
            if self.m_postprocessing_layer:
                self.m_postprocessing_layer.read_from_json(data)
            # now load (remaining) torch info
            self.load_state_dict(data)

    def add_flatten_layer(self,
                          data,
                          bounds,
                          smoothen=False,
                          number_of_bins=None):
        if not self.m_postprocessing_layer:
            raise RuntimeError(
                f"You have to define postprocessing layer in c++ first.")

        if self.m_postprocessing_layer.layer_type(
        ) != LayerType.PiecewiseLinearTransform:
            raise RuntimeError(
                f"Exist a post-processing layer with type {self.m_postprocessing_layer.layer_type()}, please check."
            )

        self.m_postprocessing_layer.add_flatten_layer(data, bounds, smoothen)

    def model_evaluation(self, x, preprocessing=True, postprocessing=True):
        if preprocessing and self.m_preprocessing_layer:
            # If input is tensor, transform it into numpy first
            if torch.is_tensor(x):
                preprocessing_input = x.detach().cpu().numpy()
            else:
                preprocessing_input = x
            # Compute proprocessing result
            preprocessing_output = self.m_preprocessing_layer.forward(
                preprocessing_input)
            # Move output to tensor in order to be processed by pytorch
            preprocessing_output = torch.from_numpy(preprocessing_output)
            x = preprocessing_output.to(x.device)

        # Make sure input is always tensors
        if not torch.is_tensor(x):
            x = torch.from_numpy(x)

        # Evaluate pytorch model
        with torch.no_grad():
            x = self.forward(x).detach().cpu().numpy()

        # Evaluate postprocessing if exist
        if postprocessing and self.m_postprocessing_layer:
            x = self.m_postprocessing_layer.forward(x)

        return x

    def forward_flattened(self, x):
        warnings.warn(
            "The method forward_flattened is depreacted, this method doesn't consider the preprocessing transformation, please use model_evaluation instead.",
            DeprecationWarning)

        if (not self.m_postprocessing_layer) or (
                self.m_postprocessing_layer.layer_type() !=
                LayerType.PiecewiseLinearTransform):
            raise RuntimeError(
                f"You have to define PiecewiseLinearTransform layer in c++ first."
            )

        x = (self.forward(x)).detach().cpu().numpy()

        return self.m_postprocessing_layer.forward(x)
