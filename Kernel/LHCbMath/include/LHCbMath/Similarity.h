/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once

#include "GaudiKernel/GenericMatrixTypes.h"
#include "GaudiKernel/GenericVectorTypes.h"
#include "GaudiKernel/SymmetricMatrixTypes.h"

#include "Kernel/STLExtensions.h"

namespace LHCb::Math {

  namespace detail {

    inline auto to_span( double& target ) { return span<double, 1>{&target, 1}; }
    inline auto to_span( const double& target ) { return span<const double, 1>{&target, 1}; }

    template <typename T, unsigned int N>
    auto to_span( const ROOT::Math::SVector<T, N>& x ) {
      return span<const T, N>{x.Array(), N};
    }
    template <typename T, unsigned int N>
    auto to_span( ROOT::Math::SVector<T, N>& x ) {
      return span<T, N>{x.Array(), N};
    }

    template <typename T, unsigned int N>
    auto to_span( ROOT::Math::SMatrix<T, N, N, ROOT::Math::MatRepSym<T, N>>& x ) {
      return span<double, N*( N + 1 ) / 2>{x.Array(), N * ( N + 1 ) / 2};
    }
    template <typename T, unsigned int N>
    auto to_span( const ROOT::Math::SMatrix<T, N, N, ROOT::Math::MatRepSym<T, N>>& x ) {
      return span<const double, N*( N + 1 ) / 2>{x.Array(), N * ( N + 1 ) / 2};
    }

    template <typename T, unsigned int N, unsigned int M>
    auto to_span( ROOT::Math::SMatrix<T, N, M, ROOT::Math::MatRepStd<T, N, M>>& x ) {
      return span<double, N * M>{x.Array(), N * M};
    }
    template <typename T, unsigned int N, unsigned int M>
    auto to_span( const ROOT::Math::SMatrix<T, N, M, ROOT::Math::MatRepStd<T, N, M>>& x ) {
      return span<const double, N * M>{x.Array(), N * M};
    }

    void similarity_5_1( span<const double, 15> Ci, span<const double, 5> Fi, span<double, 1> Ti );
    void similarity_5_2( span<const double, 15> Ci, span<const double, 10> Fi, span<double, 4> Ti );
    void similarity_5_5( span<const double, 15> Ci, span<const double, 25> Fi, span<double, 15> Ti );
    void similarity_5_5_transport_matrix( span<const double, 15> Ci, span<const double, 25> Fi, span<double, 15> Ti );
    void similarity_5_7( span<const double, 15> Ci, span<const double, 35> Fi, span<double, 28> Ti );
    void similarity_2_5( span<const double, 3> Ci, span<const double, 10> Fi, span<double, 15> Ti );

    bool average( span<const double, 5> X1, span<const double, 15> C1, span<const double, 5> X2,
                  span<const double, 15> C2, span<double, 5> X, span<double, 15> C );

    double filter( span<double, 5> X, span<double, 15> C, span<const double, 5> Xref, span<const double, 5 * 2> H,
                   span<const double, 2> refResidual, span<const double, 2> errorMeas2 );
    double filter( span<double, 5> X, span<double, 15> C, span<const double, 5> Xref, span<const double, 5 * 1> H,
                   span<const double, 1> refResidual, span<const double, 1> errorMeas2 );

    // backwards 1D compatible
    inline double filter( span<double, 5> X, span<double, 15> C, span<const double, 5> Xref, span<const double, 5> H,
                          const double refResidual, const double errorMeas2 ) {
      return filter( X, C, Xref, H, to_span( refResidual ), to_span( errorMeas2 ) );
    }

    // Specific implementation of filter for VP2D
    struct H_VP2D {};
    double filter( span<double, 5> X, span<double, 15> C, span<const double, 5> Xref, H_VP2D,
                   span<const double, 2> refResidual, span<const double, 2> errorMeas2 );

  } // namespace detail

  // perform similarity transform --
  inline double Similarity( const Gaudi::Vector5& F, const Gaudi::SymMatrix5x5& origin ) {
    using detail::to_span;
    double target;
    detail::similarity_5_1( to_span( origin ), to_span( F ), to_span( target ) );
    return target;
  }
  inline ROOT::Math::SMatrix<double, 1, 1> Similarity( const Gaudi::Matrix1x5& F, const Gaudi::SymMatrix5x5& origin ) {
    using detail::to_span;
    ROOT::Math::SMatrix<double, 1, 1> target;
    detail::similarity_5_1( to_span( origin ), to_span( F ), to_span( target ) );
    return target;
  }
  inline ROOT::Math::SMatrix<double, 2, 2> Similarity( const ROOT::Math::SMatrix<double, 2, 5>& F,
                                                       const Gaudi::SymMatrix5x5&               origin ) {
    using detail::to_span;
    ROOT::Math::SMatrix<double, 2, 2> target;
    detail::similarity_5_2( to_span( origin ), to_span( F ), to_span( target ) );
    return target;
  }

  // 'target' and 'origin' are NOT allowed to be the same object.
  // (actually, in the SSE3 and AVX versions it is possible, but not in
  // the generic version -- hence this could be a very confusing bug)
  inline void Similarity( const Gaudi::Matrix5x5&    F,      //
                          const Gaudi::SymMatrix5x5& origin, //
                          Gaudi::SymMatrix5x5&       target ) {
    using detail::to_span;
    detail::similarity_5_5( to_span( origin ), to_span( F ), to_span( target ) );
  }

  // 'target' and 'origin' are NOT allowed to be the same object.
  // specialized version of standard 5x5 similarity transform
  // for track transport matrices. (i.e. for transporting covariances)
  inline void Similarity_transport_matrix( const Gaudi::Matrix5x5& F, const Gaudi::SymMatrix5x5& origin,
                                           Gaudi::SymMatrix5x5& target ) {
    using detail::to_span;
    detail::similarity_5_5_transport_matrix( to_span( origin ), to_span( F ), to_span( target ) );
  }

  inline Gaudi::SymMatrix5x5 Similarity( const Gaudi::Matrix5x5&    F, //
                                         const Gaudi::SymMatrix5x5& origin ) {
    Gaudi::SymMatrix5x5 target;
    Similarity( F, origin, target );
    return target; // rely on RVO to make this efficient...
  }

  // 'target' and 'origin' are NOT allowed to be the same object.
  // (actually, in the SSE3 and AVX versions it is possible, but not in
  // the generic version -- hence this could be a very confusing bug)
  inline void Similarity( const ROOT::Math::SMatrix<double, 7, 5>& F,      //
                          const Gaudi::SymMatrix5x5&               origin, //
                          Gaudi::SymMatrix7x7&                     target ) {
    using detail::to_span;
    detail::similarity_5_7( to_span( origin ), to_span( F ), to_span( target ) );
  }

  inline Gaudi::SymMatrix7x7 Similarity( const ROOT::Math::SMatrix<double, 7, 5>& F,
                                         const Gaudi::SymMatrix5x5&               origin ) {
    Gaudi::SymMatrix7x7 target;
    Similarity( F, origin, target );
    return target; // rely on RVO to make this efficient...
  }

  // 'target' and 'origin' are NOT allowed to be the same object.
  inline void Similarity( const ROOT::Math::SMatrix<double, 5, 2>& F,      //
                          const Gaudi::SymMatrix2x2&               origin, //
                          Gaudi::SymMatrix5x5&                     target ) {
    detail::similarity_2_5( detail::to_span( origin ), detail::to_span( F ), detail::to_span( target ) );
  }

  inline Gaudi::SymMatrix5x5 Similarity( const ROOT::Math::SMatrix<double, 5, 2>& F,
                                         const Gaudi::SymMatrix2x2&               origin ) {
    Gaudi::SymMatrix5x5 target;
    Similarity( F, origin, target );
    return target; // rely on RVO to make this efficient...
  }

  inline bool Average( const Gaudi::Vector5&      X1, //
                       const Gaudi::SymMatrix5x5& C1, //
                       const Gaudi::Vector5&      X2, //
                       const Gaudi::SymMatrix5x5& C2, //
                       Gaudi::Vector5&            X,  //
                       Gaudi::SymMatrix5x5&       C ) {
    using detail::to_span;
    return detail::average( to_span( X1 ), to_span( C1 ), to_span( X2 ), to_span( C2 ), to_span( X ), to_span( C ) );
  }

  // compute a filter step in a Kalman filter
  // updates X and C in situ, and returns the chisquared
  // Warning: template implemented for 1D and 2D, and only tested for VP2D scenario.
  template <auto N>
  double Filter( Gaudi::Vector5&           X,           //
                 Gaudi::SymMatrix5x5&      C,           //
                 const Gaudi::Vector5&     Xref,        //
                 span<const double, 5 * N> H,           //
                 span<const double, N>     refResidual, //
                 span<const double, N>     errorMeas2 ) {
    static_assert( N == 1 || N == 2 );
    using detail::to_span;
    return detail::filter( to_span( X ), to_span( C ), to_span( Xref ), H, refResidual, errorMeas2 );
  }

  // VP2D-specific
  inline double Filter( Gaudi::Vector5&       X,           //
                        Gaudi::SymMatrix5x5&  C,           //
                        const Gaudi::Vector5& Xref,        //
                        detail::H_VP2D        H,           //
                        span<const double, 2> refResidual, //
                        span<const double, 2> errorMeas2 ) {
    using detail::to_span;
    return detail::filter( to_span( X ), to_span( C ), to_span( Xref ), H, refResidual, errorMeas2 );
  }

  // backwards 1D compatible interface
  inline double Filter( Gaudi::Vector5&         X,           //
                        Gaudi::SymMatrix5x5&    C,           //
                        const Gaudi::Vector5&   Xref,        //
                        const Gaudi::Matrix1x5& H,           //
                        const double            refResidual, //
                        const double            errorMeas2 ) {
    using detail::to_span;
    return detail::filter( to_span( X ), to_span( C ), to_span( Xref ), to_span( H ), to_span( refResidual ),
                           to_span( errorMeas2 ) );
  }

} // namespace LHCb::Math
