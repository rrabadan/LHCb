/*****************************************************************************\
* (c) Copyright 2023-2024 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once

#include <GaudiKernel/StatusCode.h>
#include <LHCbMath/FastMaths.h>
#include <LHCbMath/MatVec.h>
#include <LHCbMath/SIMDWrapper.h>
#include <optional>
#include <string>

namespace LHCb::VectorizedML {
  using namespace LHCb::LinAlg;
  using simd = SIMDWrapper::best::types;

  template <int nInput, int nOutput, typename FloatType = simd::float_v>
  class InferenceBase {
  public:
    using FType     = FloatType;
    using InputVec  = Vec<FType, nInput>;
    using OutputVec = Vec<FType, nOutput>;

    virtual ~InferenceBase(){};

    // evaluate of actual inference, specific to each type
    virtual OutputVec evaluate( InputVec const& input ) const = 0;

    // to load properties of inference
    virtual StatusCode load( std::optional<std::string> const& ) = 0;
  };

} // namespace LHCb::VectorizedML
