/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#pragma once

// STL
#include <cstdint>
#include <type_traits>

namespace LHCb {

  namespace BitCast {

    namespace details {

      // type to convert float to appropriate unsigned int type for bit casting
      template <typename FP>
      struct FloatToUInt;

      // type to convert float to appropriate signed int type for bit casting
      template <typename FP>
      struct FloatToInt;

      // specialise FloatToUInt for floats
      template <>
      struct FloatToUInt<float> {
        using type = std::uint32_t;
      };

      // specialise FloatToInt for floats
      template <>
      struct FloatToInt<float> {
        using type = std::int32_t;
      };

    } // namespace details

    /// unsigned int type to use in bit cast operations
    template <typename FP>
    using UInt32 = typename details::FloatToUInt<FP>::type;

    /// signed int type to use in bit cast operations
    template <typename FP>
    using Int32 = typename details::FloatToInt<FP>::type;

  } // namespace BitCast

} // namespace LHCb
