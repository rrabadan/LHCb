/*****************************************************************************\
* (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once

#include "STLExtensions.h"

#include <algorithm>
#include <array>
#include <iostream>
#include <numeric>
#include <stdexcept>
#include <string>

//
//  utilities to (re)implement MVA generated neural nets
//
//  please note that this version requires the number of observables
//  to be known at compile time i.e. the operator() of `Transformer`,
//  `Layers`, 'MVA' only accept spans with a compile time known extent.
//
namespace TMV::Utils {

  template <typename CName, typename... Names>
  class Validator {
    CName                fClassName;
    std::tuple<Names...> fNames;

  public:
    constexpr Validator( CName className, std::tuple<Names...> names )
        : fClassName{std::move( className )}, fNames{std::move( names )} {}

    void operator()( LHCb::span<const std::string_view> in ) const {
      auto cmp = [&]( auto lhs, auto rhs ) {
        bool r = ( lhs == rhs );
        if ( !r )
          std::cout << "Problem in class \"" << fClassName << "\": mismatch in input variable names\n"
                    << lhs << " != " << rhs << '\n';
        return r;
      };

      bool ok = ( in.size() == sizeof...( Names ) );
      if ( !ok ) {
        std::cout << "Problem in class \"" << fClassName << "\": expected " << sizeof...( Names )
                  << " variables, but got " << in.size() << std::endl;
      };
      ok = ok && std::apply( [&, i = begin( in )]( auto... n ) mutable { return ( cmp( *i++, n ) && ... ); }, fNames );
      if ( !ok ) throw std::runtime_error( std::string{"bad configuration for TMV "}.append( fClassName ) );
    }
  };

  template <auto N, auto M, typename Float>
  class Transformer {
    struct Transform {
      std::array<Float, M> scale;
      std::array<Float, M> offset;
    };
    std::array<Transform, N> transform{};

  public:
    constexpr Transformer( std::array<std::array<Float, M>, N> const& min,
                           std::array<std::array<Float, M>, N> const& max ) {
      for ( unsigned i = 0; i < N; ++i )
        for ( unsigned j = 0; j < M; ++j ) {
          Float s                = Float{2} / ( max[i][j] - min[i][j] );
          transform[i].scale[j]  = s;
          transform[i].offset[j] = min[i][j] * s + Float{1};
        }
    }
    constexpr Transformer( std::array<Float, M> const& min,
                           std::array<Float, M> const& max ) /* C++20: requires( N==1 ) */
        : Transformer{std::array<std::array<Float, M>, N>{min}, std::array<std::array<Float, M>, N>{max}} {
      static_assert( N == 1 );
    }

    constexpr static auto extent() { return M; }

    constexpr std::array<Float, M> operator()( unsigned i, LHCb::span<const Float, M> r ) const {
      assert( i < N );
      std::array<Float, M> w;
      for ( size_t j{0}; j < M; ++j ) { w[j] = transform[i].scale[j] * r[j] - transform[i].offset[j]; }
      return w;
    }
  };
  template <auto M, typename Float>
  Transformer( std::array<Float, M> const&, std::array<Float, M> const& ) -> Transformer<1, M, Float>;

  template <auto N, auto M, typename F, typename Float>
  class Layer {
    std::array<std::array<Float, M>, N> fMatrix;
    F                                   f;

  public:
    constexpr Layer( std::array<std::array<Float, M>, N> m, F f ) : fMatrix{std::move( m )}, f{std::move( f )} {}
    constexpr Layer( std::array<Float, M> m, F f ) /* C++20: requires( N==1 ) */
        : fMatrix{{std::move( m )}}, f{std::move( f )} {
      static_assert( N == 1 );
    }

    template <typename Range>
    constexpr auto operator()( Range&& r ) const {
      assert( size( r ) == M - 1 );
      if constexpr ( N == 1 ) {
        return f( std::inner_product( r.begin(), r.end(), fMatrix[0].begin(), fMatrix[0].back() ) );
      } else {
        std::array<Float, N> out;
        std::transform( fMatrix.begin(), fMatrix.end(), out.begin(), [&]( const auto& w ) {
          return std::inner_product( r.begin(), r.end(), w.begin(), w.back() );
        } );
        // moving f out of std::transform allows the compiler to unroll the transform loop
        // additonally, the loop below is auto-vectorised if the activation function f is simple enough
        // (e.g. ReLU)
        for ( auto& o : out ) { o = f( o ); }
        return out;
      }
    }

    template <typename R>
    friend constexpr auto operator|( R&& r, const Layer& l ) {
      return l( std::forward<R>( r ) );
    }
  };

  template <auto M, typename F, typename Float>
  Layer( std::array<Float, M>, F ) -> Layer<1, M, F, Float>;

  template <typename Validator, typename Transformation, typename... Layers>
  class MVA {
    Validator             validator;
    Transformation        transformer;
    std::tuple<Layers...> layers;
    size_t                sig;
    static constexpr auto N = Transformation::extent();

  public:
    constexpr MVA( Validator validator, Transformation transformer, size_t sig, Layers... layers )
        : validator{std::move( validator )}
        , transformer{std::move( transformer )}
        , layers{std::move( layers )...}
        , sig{sig} {}

    void validate( LHCb::span<const std::string_view> in ) const { validator( in ); }

    template <typename Float>
    constexpr auto operator()( LHCb::span<const Float, N> input ) const {
      return std::apply( [&]( auto&&... l ) { return ( transformer( sig, input ) | ... | l ); }, layers );
    }
  };

  // TODO: deprecated! kept only for compatibility with GlobalPID https://gitlab.cern.ch/lhcb/Rec/-/issues/246
  inline bool validateInput( LHCb::span<const std::string> in, LHCb::span<const std::string_view> expected,
                             std::string_view fClassName ) {
    // sanity checks
    if ( in.size() != expected.size() ) {
      std::cout << "Problem in class \"" << fClassName << "\": mismatch in number of input values: " << in.size()
                << " != " << expected.size() << std::endl;
      return false;
    }

    // validate input variables
    bool fStatusIsClean = true;
    for ( size_t ivar = 0; ivar < in.size(); ivar++ ) {
      if ( in[ivar] != expected[ivar] ) {
        std::cout << "Problem in class \"" << fClassName << "\": mismatch in input variable names" << std::endl
                  << " for variable [" << ivar << "]: " << in[ivar] << " != " << expected[ivar] << std::endl;
        fStatusIsClean = false;
      }
    }
    return fStatusIsClean;
  }

} // namespace TMV::Utils
