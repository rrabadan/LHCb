/*****************************************************************************\
* (c) Copyright 2020-2023 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#define BOOST_TEST_DYN_LINK
#define BOOST_TEST_MODULE MatVecTest
#include "LHCbMath/MatVec.h"
#include "LHCbMath/SIMDWrapper.h"
#include <boost/test/unit_test.hpp>
#include <limits>

using namespace LHCb::LinAlg;
using simd = SIMDWrapper::best::types;

BOOST_AUTO_TEST_CASE( test_Vec ) {
  constexpr auto epsilon = std::numeric_limits<float>::epsilon();

  Vec unit_z{0, 0, 1}, unit_x{1, 0, 0}, unit_y{0, 1, 0};

  auto sum = unit_x + unit_y + unit_z; // operator+
  BOOST_CHECK( abs( sum( 0 ) - 1.f ) < epsilon );
  BOOST_CHECK( abs( sum( 1 ) - 1.f ) < epsilon );
  BOOST_CHECK( abs( sum( 2 ) - 1.f ) < epsilon );

  auto x_minus_y = unit_x - unit_y; // operator-
  BOOST_CHECK( abs( x_minus_y( 0 ) - 1.f ) < epsilon );
  BOOST_CHECK( abs( x_minus_y( 1 ) - -1.f ) < epsilon );
  BOOST_CHECK( abs( x_minus_y( 2 ) - 0.f ) < epsilon );

  BOOST_CHECK( abs( unit_x.dot( unit_y ) ) < epsilon ); // dot

  auto times2 = unit_x * 2 + unit_y * 2 + unit_z * 2; // operator* with number
  BOOST_CHECK( abs( times2( 0 ) - 2.f ) < epsilon );
  BOOST_CHECK( abs( times2( 1 ) - 2.f ) < epsilon );
  BOOST_CHECK( abs( times2( 2 ) - 2.f ) < epsilon );

  auto zero = unit_y * unit_z; // operator* elementwise
  BOOST_CHECK( abs( zero( 0 ) - 0.f ) < epsilon );
  BOOST_CHECK( abs( zero( 1 ) - 0.f ) < epsilon );
  BOOST_CHECK( abs( zero( 2 ) - 0.f ) < epsilon );

  BOOST_CHECK( abs( Vec{1}.cast_to_value() - 1.f ) < epsilon ); // cast_to_value (only works when 1 dim

  // sub gets you a subvector of the specified type
  Vec<int, 2> should_be_unit_x_2d = unit_y.sub<Vec<int, 2>, 1>();
  BOOST_CHECK( abs( should_be_unit_x_2d( 0 ) - 1.f ) < epsilon );
  BOOST_CHECK( abs( should_be_unit_x_2d( 1 ) - 0.f ) < epsilon );

  Vec<float, 3> some_vec{1.f, 1.f, 1.f};
  Vec<float, 3> some_vec_divided_by_2 = some_vec / 2; // operator/ elementwise
  BOOST_CHECK( abs( some_vec_divided_by_2( 0 ) - 0.5f ) < epsilon );
  BOOST_CHECK( abs( some_vec_divided_by_2( 1 ) - 0.5f ) < epsilon );
  BOOST_CHECK( abs( some_vec_divided_by_2( 2 ) - 0.5f ) < epsilon );
}

BOOST_AUTO_TEST_CASE( test_Mat ) {
  constexpr auto epsilon = std::numeric_limits<float>::epsilon();
  Mat<float, 2>  mat{};
  mat( 0, 0 ) = 4;
  mat( 0, 1 ) = 3;
  mat( 1, 0 ) = 2;
  mat( 1, 1 ) = 1;

  Mat<float, 2> mat_times_2 = mat * 2; // operator*
  BOOST_CHECK( abs( mat_times_2( 0, 0 ) - 8.f ) < epsilon );
  BOOST_CHECK( abs( mat_times_2( 0, 1 ) - 6.f ) < epsilon );
  BOOST_CHECK( abs( mat_times_2( 1, 0 ) - 4.f ) < epsilon );
  BOOST_CHECK( abs( mat_times_2( 1, 1 ) - 2.f ) < epsilon );

  Mat<float, 2> mat_divided_by_2 = mat / 2; // operator/ elementwise
  BOOST_CHECK( abs( mat_divided_by_2( 0, 0 ) - 2.f ) < epsilon );
  BOOST_CHECK( abs( mat_divided_by_2( 0, 1 ) - 1.5f ) < epsilon );
  BOOST_CHECK( abs( mat_divided_by_2( 1, 0 ) - 1.f ) < epsilon );
  BOOST_CHECK( abs( mat_divided_by_2( 1, 1 ) - 0.5f ) < epsilon );

  Mat<float, 2> mat_plus_mat_plus_mat_minus_mat = mat + mat + mat - mat; // operator+, operator-
  BOOST_CHECK( abs( mat_plus_mat_plus_mat_minus_mat( 0, 0 ) - 8.f ) < epsilon );
  BOOST_CHECK( abs( mat_plus_mat_plus_mat_minus_mat( 0, 1 ) - 6.f ) < epsilon );
  BOOST_CHECK( abs( mat_plus_mat_plus_mat_minus_mat( 1, 0 ) - 4.f ) < epsilon );
  BOOST_CHECK( abs( mat_plus_mat_plus_mat_minus_mat( 1, 1 ) - 2.f ) < epsilon );

  Mat<float, 2, 3> other{0, 1, 2, /*<-first_row   second_row->*/ 3, 4, 5};
  Mat<float, 2, 3> mat_times_other = mat * other;
  BOOST_CHECK( abs( mat_times_other( 0, 0 ) - 9.f ) < epsilon );
  BOOST_CHECK( abs( mat_times_other( 0, 1 ) - 16.f ) < epsilon );
  BOOST_CHECK( abs( mat_times_other( 0, 2 ) - 23.f ) < epsilon );
  BOOST_CHECK( abs( mat_times_other( 1, 0 ) - 3.f ) < epsilon );
  BOOST_CHECK( abs( mat_times_other( 1, 1 ) - 6.f ) < epsilon );
  BOOST_CHECK( abs( mat_times_other( 1, 2 ) - 9.f ) < epsilon );

  Mat<float, 3, 2> other_T = other.transpose();
  BOOST_CHECK( abs( other( 0, 0 ) - other_T( 0, 0 ) ) < epsilon );
  BOOST_CHECK( abs( other( 0, 1 ) - other_T( 1, 0 ) ) < epsilon );
  BOOST_CHECK( abs( other( 0, 2 ) - other_T( 2, 0 ) ) < epsilon );
  BOOST_CHECK( abs( other( 1, 0 ) - other_T( 0, 1 ) ) < epsilon );
  BOOST_CHECK( abs( other( 1, 1 ) - other_T( 1, 1 ) ) < epsilon );
  BOOST_CHECK( abs( other( 1, 2 ) - other_T( 2, 1 ) ) < epsilon );

  auto sub = other.sub<Mat<float, 2, 2>, 0, 0>();
  BOOST_CHECK( abs( sub( 0, 0 ) - other( 0, 0 ) ) < epsilon );
  BOOST_CHECK( abs( sub( 0, 1 ) - other( 0, 1 ) ) < epsilon );
  BOOST_CHECK( abs( sub( 1, 0 ) - other( 1, 0 ) ) < epsilon );
  BOOST_CHECK( abs( sub( 1, 1 ) - other( 1, 1 ) ) < epsilon );

  Vec<float, 2> unit_y{0.f, 1.f};
  Vec<float, 2> sub_times_unit_y = sub * unit_y;
  BOOST_CHECK( abs( sub_times_unit_y( 0 ) - 1.f ) < epsilon );
  BOOST_CHECK( abs( sub_times_unit_y( 1 ) - 4.f ) < epsilon );

  auto y = initialize_with_zeros<Mat<float, 4>>();

  Mat<float, 4> y_with_mat{0, 0, 0, 0, 0, 4, 3, 0, 0, 2, 1, 0, 0, 0, 0, 0};

  y = y.place_at<1, 1>( mat );
  for ( int i = 0; i < 4; i++ ) {
    for ( int j = 0; j < 4; j++ ) { BOOST_CHECK( abs( y( i, j ) - y_with_mat( i, j ) ) < epsilon ); }
  }

  Vec<float, 3> vec_sim{1.f, 1.f, 1.f};
  Mat<float, 3> mat_sim{0, 1, 2, 3, 4, 5, 6, 7, 8};
  float         float_sim = 36.f;
  BOOST_CHECK( abs( similarity( vec_sim, mat_sim ) - float_sim ) < epsilon );
}

BOOST_AUTO_TEST_CASE( test_MatSym ) {
  constexpr auto epsilon = std::numeric_limits<float>::epsilon();

  Mat<float, 2> _sym1_2d{};
  _sym1_2d( 0, 0 ) = 1;
  _sym1_2d( 0, 1 ) = 2;
  _sym1_2d( 1, 0 ) = 2;
  _sym1_2d( 1, 1 ) = 3;

  auto sym1_2d = _sym1_2d.cast_to_sym();

  MatSym<float, 2> also_sym1_2d{1, 2, 3};

  BOOST_CHECK( abs( sym1_2d( 0, 0 ) - also_sym1_2d( 0, 0 ) ) < epsilon );
  BOOST_CHECK( abs( sym1_2d( 1, 0 ) - also_sym1_2d( 1, 0 ) ) < epsilon );
  BOOST_CHECK( abs( sym1_2d( 0, 1 ) - also_sym1_2d( 0, 1 ) ) < epsilon );
  BOOST_CHECK( abs( sym1_2d( 1, 1 ) - also_sym1_2d( 1, 1 ) ) < epsilon );

  auto sym1_square = ( sym1_2d * sym1_2d + sym1_2d - sym1_2d ).cast_to_sym(); // operator*, operator+, operator-
  BOOST_CHECK( abs( sym1_square( 0, 0 ) - 5.f ) < epsilon );
  BOOST_CHECK( abs( sym1_square( 1, 0 ) - 8.f ) < epsilon );
  BOOST_CHECK( abs( sym1_square( 0, 1 ) - 8.f ) < epsilon );
  BOOST_CHECK( abs( sym1_square( 1, 1 ) - 13.f ) < epsilon );

  Vec<float, 2> unit_y{0.f, 1.f};
  auto          sym_times_unit_y = sym1_2d * unit_y; // operator*
  BOOST_CHECK( abs( sym_times_unit_y( 0 ) - 2.f ) < epsilon );
  BOOST_CHECK( abs( sym_times_unit_y( 1 ) - 3.f ) < epsilon );

  //clang-format off
  MatSym<float, 3> someMatrix{2, -1, 2, 0, -1, 2};
  MatSym<float, 3> someMatrixInverse{0.75f, 0.5f, 1.f, 0.25f, 0.5f, 0.75f};
  //clang-format on
  bool isEqual                   = true;
  auto shouldBeSomeMatrixInverse = someMatrix.invChol().second;
  for ( auto i = 0u; i < someMatrixInverse.m.size(); ++i ) {
    if ( fabs( someMatrixInverse.m[i] - shouldBeSomeMatrixInverse.m[i] ) > std::numeric_limits<float>::epsilon() ) {
      isEqual = false;
      break;
    }
  }
  BOOST_CHECK( isEqual );
}

BOOST_AUTO_TEST_CASE( test_MatSym_vectorized ) {
  using simd = SIMDWrapper::best::types;
  MatSym<simd::float_v, 3> someMatrix{2, -1, 2, 0, -1, 2};
  MatSym<simd::float_v, 3> someMatrixInverse{0.75f, 0.5f, 1.f, 0.25f, 0.5f, 0.75f};
  auto                     shouldBeSomeMatrixInverse = someMatrix.invChol().second;
  for ( auto i = 0u; i < someMatrixInverse.n_rows; ++i ) {
    for ( auto j = 0u; j < someMatrixInverse.n_cols; ++j ) {
      BOOST_CHECK( !any( abs( someMatrixInverse( i, j ) - shouldBeSomeMatrixInverse( i, j ) ) >
                         simd::float_v{std::numeric_limits<float>::epsilon()} ) );
    }
  }
}
