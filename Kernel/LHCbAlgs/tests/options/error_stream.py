###############################################################################
# (c) Copyright 2022-2024 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from PyConf.Algorithms import (
    MessageSvcEventFilter,
    ConfigurableDummy,
)
from PyConf.application import ApplicationOptions, configure, configure_input
from PyConf.control_flow import CompositeNode, NodeLogic

message_producer = ConfigurableDummy(
    CFD=3, DecisionWarning=True)  # 33% prescale

algs_node = CompositeNode(
    "algs", [message_producer],
    combine_logic=NodeLogic.LAZY_AND,
    force_order=True)

event_filter = MessageSvcEventFilter()

writer_node = CompositeNode(
    "writer", [event_filter],
    combine_logic=NodeLogic.LAZY_AND,
    force_order=True)

top = CompositeNode(
    "top", [algs_node, writer_node],
    combine_logic=NodeLogic.NONLAZY_AND,
    force_order=True)

# Define the application environment and run it
options = ApplicationOptions(_enabled=False)
options.n_threads = 4
options.n_event_slots = 4
options.evt_max = 50
options.input_type = "NONE"
options.geometry_backend = "NONE"
options.simulation = False

config2 = configure_input(options)
config = configure(options, top)
