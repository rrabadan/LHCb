###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from PyConf.application import ApplicationOptions, configure, configure_input
from PyConf.control_flow import CompositeNode
from PyConf.application import make_odin
from PyConf.Algorithms import PrintHeader

# Define the application environment and run it
options = ApplicationOptions(_enabled=False)
options.n_threads = 1
options.n_event_slots = 1
options.evt_max = 50
options.input_files = [
    '/data/bfys/lhcb/data/2014/RAW/FULL/LHCb1/TEST/142671/142671_0000000001.raw'
]
options.input_type = "MDF"
options.input_raw_format = 4.3
options.evt_max = 100
options.simulation = True
options.data_type = 'Upgrade'
options.dddb_tag = 'dddb-20171126'
options.conddb_tag = 'sim-20171127-vc-md100'

config = configure_input(options)
node = CompositeNode(
    "printer", children=[PrintHeader(ODINLocation=make_odin())])

config.update(configure(options, node))
