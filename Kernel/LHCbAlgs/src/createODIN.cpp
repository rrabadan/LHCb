/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "Event/RawBank.h"
#include "LHCbAlgs/Transformer.h"
#include "ODINCodec.h"

namespace LHCb {

  /** Trivial algorithm to create DAQ/ODIN object from ODIN RawEvent bank
   *
   *  @author Marco Cattaneo
   *  @date   2008-01-15
   *  @author Marco Clemencic
   *  @date   2016-09-19
   */
  struct createODIN final : Algorithm::Transformer<ODIN( const RawBank::View& )> {
    /// Standard constructor
    createODIN( const std::string& name, ISvcLocator* pSvcLocator )
        : Transformer( name, pSvcLocator, KeyValue( "RawBanks", "DAQ/RawBanks/ODIN" ),
                       KeyValue( "ODIN", ODINLocation::Default ) ) {}

    ODIN operator()( const RawBank::View& banks ) const override {
      if ( banks.empty() ) throw GaudiException( "createODIN", "no ODIN bank in raw event", StatusCode::FAILURE );
      return ODINCodec::decode( *banks.front() );
    }
  };

  // Declaration of the Algorithm Factory
  DECLARE_COMPONENT_WITH_ID( createODIN, "createODIN" )

} // namespace LHCb
