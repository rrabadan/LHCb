/*****************************************************************************\
* (c) Copyright 2024 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "Event/CaloAdc.h"
#include "Event/CaloCluster.h"
#include "Event/FlavourTag.h"
#include "Event/MCParticle.h"
#include "Event/Particle.h"
#include "Event/RecSummary.h"
#include "Event/RelatedInfoMap.h"
#include "Event/TwoProngVertex.h"
#include "Event/WeightsVector.h"
#include "LHCbAlgs/Transformer.h"
#include "Relations/Relation1D.h"
#include "Relations/RelationWeighted1D.h"

/** @brief Shallow copy of objects to be persisted to their final TES locations.
 */

template <typename T>
class CopyToSelection final : public LHCb::Algorithm::Transformer<typename T::Selection( const typename T::Range& )> {

public:
  CopyToSelection( const std::string& name, ISvcLocator* pSvcLocator )
      : LHCb::Algorithm::Transformer<typename T::Selection( const typename T::Range& )>(
            name, pSvcLocator, {"Input", ""}, {"Output", ""} ) {}

  typename T::Selection operator()( const typename T::Range& input ) const override {
    return typename T::Selection{input.begin(), input.end()};
  }
};

DECLARE_COMPONENT_WITH_ID( CopyToSelection<LHCb::Particle>, "CopyParticles" )
DECLARE_COMPONENT_WITH_ID( CopyToSelection<LHCb::FlavourTag>, "CopyFlavourTags" )
DECLARE_COMPONENT_WITH_ID( CopyToSelection<LHCb::RecVertex>, "CopyRecVertices" )
DECLARE_COMPONENT_WITH_ID( CopyToSelection<LHCb::Vertex>, "CopyVertices" )
DECLARE_COMPONENT_WITH_ID( CopyToSelection<LHCb::TwoProngVertex>, "CopyTwoProngVertices" )
DECLARE_COMPONENT_WITH_ID( CopyToSelection<LHCb::RichPID>, "CopyRichPIDs" )
DECLARE_COMPONENT_WITH_ID( CopyToSelection<LHCb::MuonPID>, "CopyMuonPIDs" )
DECLARE_COMPONENT_WITH_ID( CopyToSelection<LHCb::NeutralPID>, "CopyNeutralPIDs" )
DECLARE_COMPONENT_WITH_ID( CopyToSelection<LHCb::GlobalChargedPID>, "CopyGlobalChargedPIDs" )
DECLARE_COMPONENT_WITH_ID( CopyToSelection<LHCb::Event::Calo::v1::CaloChargedPID>, "CopyCaloChargedPIDs" )
DECLARE_COMPONENT_WITH_ID( CopyToSelection<LHCb::Event::Calo::v1::BremInfo>, "CopyBremInfos" )
DECLARE_COMPONENT_WITH_ID( CopyToSelection<LHCb::Track>, "CopyTracks" )
DECLARE_COMPONENT_WITH_ID( CopyToSelection<LHCb::CaloHypo>, "CopyCaloHypos" )
DECLARE_COMPONENT_WITH_ID( CopyToSelection<LHCb::CaloCluster>, "CopyCaloClusters" )
DECLARE_COMPONENT_WITH_ID( CopyToSelection<LHCb::ProtoParticle>, "CopyProtoParticles" )

// These don't have Selection type container so can only be persisted implicitely at the moment.
// DECLARE_COMPONENT_WITH_ID( CopyToSelection<LHCb::CaloDigit>, "CopyCaloDigit" )
// DECLARE_COMPONENT_WITH_ID( CopyToSelection<LHCb::CaloAdc>, "CopyCaloAdc" )
// DECLARE_COMPONENT_WITH_ID( CopyToSelection<LHCb::WeightsVector>, "CopyWeightsVector" )

/** @brief Copy of relations to be persisted to their final TES locations. No cloning of the from or to.
 */
template <typename T>
class CopyRelations final : public LHCb::Algorithm::Transformer<T( const T& )> {

public:
  CopyRelations( const std::string& name, ISvcLocator* pSvcLocator )
      : LHCb::Algorithm::Transformer<T( const T& )>( name, pSvcLocator, {"Input", ""}, {"Output", ""} ) {}

  T operator()( const T& input ) const override { return T{input}; }
};

// Relation copiers
namespace { // cannot have types with a comma in a macro, so define some aliases for them
  using P2V   = LHCb::Relation1D<LHCb::Particle, LHCb::VertexBase>;
  using P2MC  = LHCb::Relation1D<LHCb::Particle, LHCb::MCParticle>;
  using P2i   = LHCb::Relation1D<LHCb::Particle, int>;
  using P2RIM = LHCb::Relation1D<LHCb::Particle, LHCb::RelatedInfoMap>;
  using PP2MC = LHCb::RelationWeighted1D<LHCb::ProtoParticle, LHCb::MCParticle, double>;
} // namespace
DECLARE_COMPONENT_WITH_ID( CopyRelations<P2V>, "CopyP2VRelations" )
DECLARE_COMPONENT_WITH_ID( CopyRelations<P2MC>, "CopyP2MCPRelations" )
DECLARE_COMPONENT_WITH_ID( CopyRelations<P2i>, "CopyP2IntRelations" )
DECLARE_COMPONENT_WITH_ID( CopyRelations<P2RIM>, "CopyP2InfoRelations" )
DECLARE_COMPONENT_WITH_ID( CopyRelations<PP2MC>, "CopyPP2MCPRelations" )