/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#include "Gaudi/Algorithm.h"
#include "GaudiKernel/DataObjectHandle.h"

//-----------------------------------------------------------------------------
// Implementation file for class : DataObjectVersionFilter
//
// 2009-11-06 : Chris Jones
//-----------------------------------------------------------------------------

/** @class DataObjectVersionFilter DataObjectVersionFilter.h
 *
 *  Checks the version of a given DataObject in the TES, and sets FilterPassed according
 *  to a min and max version number. Useful to allow reprocessing of some data depending
 *  on the version number
 *
 *  @author Chris Jones
 *  @date   2009-11-06
 */
class DataObjectVersionFilter final : public Gaudi::Algorithm {
public:
  using Algorithm::Algorithm;
  StatusCode execute( const EventContext& ) const override;

private:
  Gaudi::Property<unsigned int>    m_minV{this, "MinVersion", 0, "Min version number"};
  Gaudi::Property<unsigned int>    m_maxV{this, "MaxVersion", 9999999, "Max version number"};
  DataObjectReadHandle<DataObject> m_loc = {this, "DataObjectLocation", "", "DataObject location in TES"};
};

// Declaration of the Algorithm Factory
DECLARE_COMPONENT( DataObjectVersionFilter )

StatusCode DataObjectVersionFilter::execute( const EventContext& evtCtx ) const {
  bool OK = true;

  try {
    const DataObject* data = m_loc.getIfExists();
    if ( data ) {
      const auto ver = (unsigned int)data->version();
      if ( msgLevel( MSG::DEBUG ) ) debug() << "version = " << ver << endmsg;
      OK = ( ver <= m_maxV.value() && ver >= m_minV.value() );
    } else {
      OK = false;
    }
  } catch ( const GaudiException& ) { OK = false; }

  execState( evtCtx ).setFilterPassed( OK );

  return StatusCode::SUCCESS;
}
