/*****************************************************************************\
* (c) Copyright 2000-2021 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// Include files
#include "Event/ODIN.h"
#include "Event/RawBank.h"
#include "Event/RawEvent.h"
#include "Gaudi/Accumulators.h"
#include "GaudiAlg/GaudiTool.h"
#include "GaudiAlg/IGenericTool.h"
#include "GaudiKernel/DataObjectHandle.h"
#include "GaudiKernel/GaudiException.h"
#include "GaudiKernel/SerializeSTL.h"
#include "ODINCodec.h"

#include <sstream>

/** @class ODINDecodeTool ODINDecodeTool.h
 *
 *  Tool to decode the ODIN raw bank, fill a ODIN object with its data and
 *  register it to the transient store.
 *
 *  @see ODINCodecBaseTool for the properties.
 *
 *  @author Marco Clemencic
 *  @date   2009-02-02
 */
class ODINDecodeTool final : public extends<GaudiTool, IGenericTool> {

public:
  /// Standard constructor
  using extends::extends;

  /// Initialize the tool
  StatusCode initialize() override;

  /// Do the conversion
  void execute() override;

private:
  /// Decode the ODIN RawBank and fill the ODIN object
  /// @param bank ODIN raw bank to decode
  /// @param odin optional pointer to an ODIN object to fill, if null a new object is created (ownership on the caller)
  /// @return pointer to a new ODIN object or the value of the parameter odin
  LHCb::ODIN* i_decode( const LHCb::RawBank& bank, LHCb::ODIN* odin = nullptr ) const;
  // get for one location
  LHCb::RawEvent* tryEventAt( const std::string& ) const;
  /** @brief Returns a pointer to the first RawEvent in the search path
   */
  LHCb::RawEvent* findFirstRawEvent() const;
  /// Where to look first, saves the last place I was able to retrieve successfully
  mutable std::string m_tryFirstRawEventLocation = "";

  /// whether to use the RootOnTes next time
  mutable bool m_tryRootOnTes = true;
  /// Location in the transient store of the ODIN object.
  /// FIXME: this is not stricly true, as we also read from this location...
  DataObjectWriteHandle<LHCb::ODIN>         m_odinLocation{this, "ODINLocation", LHCb::ODINLocation::Default,
                                                   "Location of the ODIN object in the transient store"};
  Gaudi::Property<std::vector<std::string>> m_rawEventLocations{
      this,
      "RawEventLocations",
      {LHCb::RawEventLocation::Trigger, LHCb::RawEventLocation::Default},
      "Search path for the raw event"};
  /// If set to true, override the destination object.
  Gaudi::Property<bool> m_force{this, "Force", false, "If already present, override the destination object."};
  /// Flag to indicate if unknown version numbers have to be ignored.
  Gaudi::Property<bool>                                 m_ignoreBankVersion{this, "IgnoreUnknownBankVersion", false,
                                            "Do not stop in case of unknown bank version number, assuming"
                                            " it is binary compatible with the latest known version."};
  mutable Gaudi::Accumulators::MsgCounter<MSG::WARNING> m_noODIN{this, "Cannot find ODIN bank in RawEvent"};
};

// Declaration of the Tool Factory
DECLARE_COMPONENT( ODINDecodeTool )

//=============================================================================
// Initialize
//=============================================================================
StatusCode ODINDecodeTool::initialize() {
  StatusCode sc = extends::initialize(); // always first
  if ( sc.isFailure() ) return sc;       // error message already printed

  if ( m_odinLocation.objKey() != LHCb::ODINLocation::Default ) {
    info() << "Using '" << m_odinLocation.objKey() << "' as location of the ODIN object" << endmsg;
  }

  if ( !m_rawEventLocations.empty() ) { m_tryFirstRawEventLocation = m_rawEventLocations[0]; }
  m_tryRootOnTes = !rootInTES().empty();

  if ( m_rawEventLocations.empty() || ( m_rawEventLocations[0] != LHCb::RawEventLocation::Default &&
                                        m_rawEventLocations[0] != LHCb::RawEventLocation::Trigger ) ) {
    info() << "Using '" << m_rawEventLocations << "' as search path for the RawEvent object" << endmsg;
  }

  return sc;
}
//=============================================================================
// Main function
//=============================================================================
void ODINDecodeTool::execute() {
  // load the odin
  LHCb::ODIN* odin = m_odinLocation.getIfExists();

  // Check if there is already an ODIN object
  if ( odin ) {
    if ( !m_force ) {
      // ODIN already there and we are not supposed to touch it
      if ( msgLevel( MSG::DEBUG ) ) debug() << "Keep existing ODIN object" << endmsg;
      return;
    }
    if ( m_force && msgLevel( MSG::DEBUG ) ) {
      // Modify/overwrite the already registered object
      debug() << "Modify existing ODIN object" << endmsg;
    }
  }

  if ( msgLevel( MSG::DEBUG ) ) debug() << "Getting RawEvent" << endmsg;
  LHCb::RawEvent* rawEvent = findFirstRawEvent();
  if ( !rawEvent ) {
    using namespace GaudiUtils;
    // Throw a meaningful exception it the bank is not found;
    std::ostringstream out;
    out << "Cannot find RawEvent in " << m_rawEventLocations;
    throw GaudiException( out.str(), name(), StatusCode::FAILURE );
  }

  // Check if have an ODIN bank...
  const auto& odinBanks = rawEvent->banks( LHCb::RawBank::ODIN );
  if ( !odinBanks.empty() && odinBanks.front() ) { // ... good, we can decode it
    odin = i_decode( *odinBanks.front(), odin );
    // synchronize DataObject::version with ODIN internal version
    odin->setVersion( LHCb::ODIN::BANK_VERSION );
    if ( odin && ( !odin->registry() ) ) { // register ODIN object if valid and not yet registered
      m_odinLocation.put( std::unique_ptr<LHCb::ODIN>( odin ) );
    }
  } else {
    ++m_noODIN;
  }
}

LHCb::RawEvent* ODINDecodeTool::findFirstRawEvent() const {
  // try once with cached location
  LHCb::RawEvent* raw = tryEventAt( m_tryFirstRawEventLocation );
  if ( raw ) return raw;
  for ( const auto& loc : m_rawEventLocations ) {
    raw = tryEventAt( loc );
    if ( raw ) {
      m_tryFirstRawEventLocation = loc;
      return raw;
    }
  }
  return nullptr;
}
//=============================================================================
// Search methods
//=============================================================================
LHCb::RawEvent* ODINDecodeTool::tryEventAt( const std::string& loc ) const {
  if ( loc.empty() ) return nullptr;
  // I could also initialize this and store in the class, but I think this is fast enough to do now,
  // at least must be much faster than looking in the rooOnTes a second time
  const bool rootOnTesSensitive = !rootInTES().empty();
  // try once with rootInTes, if there is a rootIn or rootOn tes defined! Otherwise it is a waste
  LHCb::RawEvent* raw = getIfExists<LHCb::RawEvent>( loc, rootOnTesSensitive && m_tryRootOnTes );
  if ( raw ) return raw;
  // if there is no root on tes, then I don't need to look there, ever!
  if ( !rootOnTesSensitive ) return nullptr;
  // try once with opposite, if I'm sensitive to the rootOnTes
  raw = getIfExists<LHCb::RawEvent>( loc, !m_tryRootOnTes );
  if ( raw ) m_tryRootOnTes = !m_tryRootOnTes;
  return raw;
}

//=============================================================================
// Decode
//=============================================================================
LHCb::ODIN* ODINDecodeTool::i_decode( const LHCb::RawBank& bank, LHCb::ODIN* odin ) const {
  if ( !odin ) {
    return new LHCb::ODIN{LHCb::ODINCodec::decode( bank, m_ignoreBankVersion )};
  } else {
    *odin = LHCb::ODINCodec::decode( bank, m_ignoreBankVersion );
    return odin;
  }
}

//=============================================================================
