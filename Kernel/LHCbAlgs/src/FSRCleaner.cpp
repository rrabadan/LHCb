/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#include "EmptyEventNodeCleaner.h"

#include "GaudiKernel/SmartDataPtr.h"

#include <string>

//-----------------------------------------------------------------------------
// Implementation file for class : FSRCleaner
//
// 2012-08-01 : Rob Lambert
//-----------------------------------------------------------------------------

/**
 *  Searches for and removes empty data nodes in the FSR tree
 *
 *  @author Rob Lambert
 *  @date   2012-08-01
 */
class FSRCleaner final : public EmptyEventNodeCleaner {

public:
  /// Standard constructor
  FSRCleaner( const std::string& name, ISvcLocator* pSvcLocator );

  StatusCode execute( const EventContext& ) const override { return StatusCode::SUCCESS; } /// do nothing
  StatusCode finalize() override; /// Real algorithm execution in finalize
};

FSRCleaner::FSRCleaner( const std::string& name, ISvcLocator* pSvcLocator )
    : EmptyEventNodeCleaner( name, pSvcLocator ) {
  // Set sensible FSR defaults
  setProperty( "InputStream", "/FileRecords" ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
  setProperty( "DataService", "FileRecordDataSvc" ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
}

StatusCode FSRCleaner::finalize() {
  SmartIF<IDataManagerSvc> mgr( &*m_dataSvc );
  // cleanup InputStream
  std::string rootNodeName;
  mgr.as<IProperty>()->getProperty( "RootName", rootNodeName ).ignore();
  if ( rootNodeName == m_inputStream ) {
    // in case we used the Root, we need to call clearStore
    // clearSubTree would fail as it tries to call remove on the
    // parent of the given node, and there is none for Root
    mgr->clearStore().ignore();
  } else {
    // Try and load the root DataObject for the configured stream
    SmartDataPtr<DataObject> root( m_dataSvc.get(), m_inputStream );
    // if found, recursively clean
    if ( root ) {
      auto sc = mgr->clearSubTree( root );
      if ( !sc.isSuccess() ) return sc;
    }
  }
  return EmptyEventNodeCleaner::finalize();
}

// Declaration of the Algorithm Factory
DECLARE_COMPONENT( FSRCleaner )
