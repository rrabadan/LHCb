/*****************************************************************************\
* (c) Copyright 2000-2021 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once

#include <Event/ODIN.h>
#include <Event/RawBank.h>
#include <Kernel/STLExtensions.h>
#include <cstdint>

namespace LHCb {
  namespace ODINCodec {
    LHCb::ODIN decode( const LHCb::RawBank& bank, const bool ignoreBankVersion = false );
    void       encode( const LHCb::ODIN& odin, const int bank_version, LHCb::span<std::uint32_t> output_buffer );
  } // namespace ODINCodec
} // namespace LHCb
