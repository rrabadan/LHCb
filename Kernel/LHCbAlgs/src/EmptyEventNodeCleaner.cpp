/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#include "EmptyEventNodeCleaner.h"

#include "GaudiKernel/IRegistry.h"
#include "GaudiKernel/SmartDataPtr.h"
#include "GaudiKernel/SmartIF.h"

//-----------------------------------------------------------------------------
// Implementation file for class : EmptyEventNodeCleaner
//
// 2012-01-31 : Chris Jones
//-----------------------------------------------------------------------------

StatusCode EmptyEventNodeCleaner::execute( const EventContext& ) const {
  SmartIF<IDataManagerSvc> mgr( &*m_dataSvc );
  // Try and load the root DataObject for the configured stream
  SmartDataPtr<DataObject> root( m_dataSvc.get(), m_inputStream );
  // if found, recursively clean
  if ( root ) {
    auto sc = mgr->clearSubTree( root );
    if ( !sc.isSuccess() ) return sc;
  }
  return StatusCode::SUCCESS;
}

// Declaration of the Algorithm Factory
DECLARE_COMPONENT( EmptyEventNodeCleaner )
