/*****************************************************************************\
* (c) Copyright 2022 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "Event/RawEvent.h"
#include "LHCbAlgs/Consumer.h"

#include <csignal>
#include <fcntl.h>
#include <fmt/format.h>
#include <stdio.h>

/** @class SignalMDFWriter
 *
 * PrepareMDFSignalHandler
 *
 *  Register signal handler that writes the raw event.
 *
 * The behavior is undefined if any signal handler performs any of the following:
 *   ...
 *   - access to an object with thread storage duration
 *
 * https://maskray.me/blog/2021-02-14-all-about-thread-local-storage
 *
 */

namespace {
  using DataHandle = DataObjectReadHandle<LHCb::RawEvent>;

  constexpr size_t OUTPUT_PATH_SIZE               = 1024;
  DataHandle*      s_rawEventHandle               = nullptr;
  char             s_outputPath[OUTPUT_PATH_SIZE] = "";

  void signal_handler( int ) {
    auto f = ::open( s_outputPath, O_WRONLY | O_CREAT | O_TRUNC, S_IWUSR | S_IRUSR | S_IRGRP | S_IWGRP | S_IROTH );
    if ( f == -1 ) { std::_Exit( EXIT_FAILURE ); }

    const auto* rawEvent = s_rawEventHandle->getIfExists();
    if ( rawEvent ) {
      // MDF event header
      LHCb::RawBank const* const daqBank = rawEvent->banks( LHCb::RawBank::DAQ )[0];
      ::write( f, daqBank->data(), daqBank->totalSize() - daqBank->hdrSize() );
      // MDF raw banks
      constexpr auto types = LHCb::RawBank::types();
      for ( const auto type : types ) {
        if ( type != LHCb::RawBank::DAQ ) {
          for ( LHCb::RawBank const* const bank : rawEvent->banks( type ) ) { ::write( f, bank, bank->totalSize() ); }
        }
      }
    }

    ::close( f );
    std::_Exit( EXIT_FAILURE );
  }

} // namespace

class SignalMDFWriter : public LHCb::Algorithm::Consumer<void()> {
public:
  SignalMDFWriter( const std::string& name, ISvcLocator* pSvcLocator ) : Consumer( name, pSvcLocator ) {}

  StatusCode start() override;
  StatusCode stop() override;
  void       operator()() const override;

private:
  Gaudi::Property<std::string>      m_outputPrefix{this, "OutputPrefix", "signal"};
  Gaudi::Property<std::vector<int>> m_signals{this, "Signals", {SIGILL, SIGABRT, SIGBUS, SIGSEGV}};
  DataHandle                        m_rawEvent{this, "RawEvent", ""};
};

DECLARE_COMPONENT( SignalMDFWriter )

StatusCode SignalMDFWriter::start() {
  return Consumer::start().andThen( [&] {
    s_rawEventHandle = &m_rawEvent;
    for ( auto signal : m_signals ) std::signal( signal, signal_handler );
  } );
}

StatusCode SignalMDFWriter::stop() { return Consumer::stop(); }

void SignalMDFWriter::operator()() const {
  // 1. Make sure the transient MapView is allocated and initialized.
  // 2. Obtain the ODIN raw data to construct the name of the output.
  auto const* rawEvent = m_rawEvent.get();
  auto const  banks    = rawEvent->banks( LHCb::RawBank::ODIN );
  if ( banks.size() != 1 ) {
    throw GaudiException( "Unexpected number of ODIN banks: " + std::to_string( banks.size() ), __PRETTY_FUNCTION__,
                          StatusCode::FAILURE );
  }
  // Assume ODIN v7
  auto s = fmt::format( "{}_{:010}_{:020}.mdf", m_outputPrefix.value(), banks[0]->data()[0],
                        static_cast<uint64_t>( banks[0]->data()[8] ) +
                            ( static_cast<uint64_t>( banks[0]->data()[9] ) << 32 ) );
  if ( s.size() + 1 > OUTPUT_PATH_SIZE ) {
    throw GaudiException( "OutputPrefix property is too long", __PRETTY_FUNCTION__, StatusCode::FAILURE );
  }
  std::strcpy( s_outputPath, s.c_str() );
}
