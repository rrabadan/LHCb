/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#include "Gaudi/Algorithm.h"

// ----------------------------------------------------------------------------
// Implementation file for class: ServiceStarter
//
// 09/01/2012: Marco Clemencic
// ----------------------------------------------------------------------------

namespace LHCbAlgsTest {

  /**
   * Simple test algorithm to trigger the instantiation of a service at execute phase
   *
   * @author Marco Clemencic
   * @date 09/01/2012
   */
  struct ServiceStarter final : Gaudi::Algorithm {

    using Algorithm::Algorithm;
    StatusCode execute( const EventContext& ) const override;

    StatusCode              i_retrieveService( const std::string& currentPhase );
    ServiceHandle<IService> m_service{this, "Service", "", "Service to retrieve."};
  };

  DECLARE_COMPONENT( ServiceStarter )

  StatusCode ServiceStarter::execute( const EventContext& ) const {
    info() << "Retrieving " << m_service.name() << endmsg;
    if ( m_service.retrieve() != StatusCode::SUCCESS ) {
      error() << "Could not get service";
      return StatusCode::FAILURE;
    }
    return StatusCode::SUCCESS;
  }

} // namespace LHCbAlgsTest
