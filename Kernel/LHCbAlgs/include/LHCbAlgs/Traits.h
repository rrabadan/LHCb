/*****************************************************************************\
* (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once
#include "GaudiAlg/FunctionalDetails.h"
#include "GaudiAlg/FunctionalUtilities.h"
// FIXME: remove one level of indirection and decide 'here' between DD4HEP and DetDesc...
#include "DetDesc/GenericConditionAccessorHolder.h"
#include <GAUDI_VERSION.h>

namespace LHCb::Algorithm::Traits {

  using namespace Gaudi::Functional::Traits;

  // At this stage, the only customization is the default base class "FixTESPath<Gaudi::Algorithm>"
  using Default = BaseClass_t<FixTESPath<Gaudi::Algorithm>>;

  namespace details {
    // add a baseclass in case it isn't defined yet...
    template <typename Tr, typename Base = FixTESPath<Gaudi::Algorithm>>
    using add_base_t =
        std::conditional_t<Gaudi::cpp17::is_detected_v<Gaudi::Functional::details::detail2::BaseClass_t, Tr>, Tr,
                           use_<Tr, BaseClass_t<Base>>>;
  } // namespace details

  using LHCb::DetDesc::usesBaseAndConditions;
  using LHCb::DetDesc::usesConditions;

  namespace details {
    struct OpaqueView {
      OpaqueView() = default;
      template <typename T>
      OpaqueView( const T& ) {}
    };
  } // namespace details

  template <typename T>
  using writeOnly = writeViewFor<T, details::OpaqueView>;

} // namespace LHCb::Algorithm::Traits

// Abusing the fact that this file is included in all LHCb Algorithm files to
// tackle the backward compatibility with Gaudi versions < 39 by defining an
// empty Gaudi::FSMCallbackHolder class
#if GAUDI_MAJOR_VERSION < 39
namespace Gaudi {
  template <class BASE>
  class FSMCallbackHolder : public BASE {
    using BASE::BASE;
  };
} // namespace Gaudi
#else
#  include <Gaudi/FSMCallbackHolder.h>
#endif
