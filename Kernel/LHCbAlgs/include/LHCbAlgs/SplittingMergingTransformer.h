/*****************************************************************************\
* (c) Copyright 2000-2021 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once
#include "GaudiAlg/SplittingMergingTransformer.h"
#include "LHCbAlgs/Traits.h"

namespace LHCb::Algorithm {

  using Gaudi::Functional::vector_of_const_;
  /**
   * Redefinition of Gaudi::Functional::MergingTransformer with LHCb customization
   *
   * See Traits.h for the LHCb defaults
   */
  template <typename Signature, typename Traits_ = Traits::Default>
  struct SplittingMergingTransformer
      : Gaudi::FSMCallbackHolder<
            Gaudi::Functional::SplittingMergingTransformer<Signature, Traits::details::add_base_t<Traits_>>> {
    using Gaudi::FSMCallbackHolder<Gaudi::Functional::SplittingMergingTransformer<
        Signature, Traits::details::add_base_t<Traits_>>>::FSMCallbackHolder;
  };

} // namespace LHCb::Algorithm
