/*****************************************************************************\
* (c) Copyright 2000-2024 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "GaudiAlg/GaudiAlgorithm.h"

namespace LHCb::Algorithm {
  template <typename T>
  class CopyAnyData final : public GaudiAlgorithm {
  public:
    using GaudiAlgorithm::GaudiAlgorithm;
    StatusCode execute() override {
      auto* object = getIfExists<AnyDataWrapper<T>>( m_input );
      if ( object ) {
        T    copy      = object->getData();
        auto newObject = std::make_unique<AnyDataWrapper<T>>( std::move( copy ) );
        put( newObject.release(), m_output );
      }
      return StatusCode::SUCCESS;
    }

  private:
    Gaudi::Property<std::string> m_input{this, "Input", ""};
    Gaudi::Property<std::string> m_output{this, "Output", ""};
  };
} // namespace LHCb::Algorithm
