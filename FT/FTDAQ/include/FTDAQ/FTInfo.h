/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once

#include "Detector/FT/FTConstants.h"
#include <array>
#include <map>
#include <string>

/** Constant information of the detector
 *  @author Sebastien Ponce
 *  @date   2016-09-30
 */

namespace LHCb::Pr::FT {
  namespace Info {
    const inline std::string SciFiHitsLocation = "FT/SciFiHits";
    const inline std::string FTCondLocation    = "Conditions/FT";
    const inline std::string FTZonesLocation   = "Conditions/FT/FTZones";
#ifdef USE_DD4HEP
    const auto cond_path = std::string{"/world/AfterMagnetRegion/T/FT:ReadoutMap"};
    // dd4hep will always have the tag, so skip the test
#else
    const auto cond_path = std::string{"/dd/Conditions/ReadoutConf/FT/ReadoutMap"};
#endif

  } // namespace Info
} // namespace LHCb::Pr::FT

namespace PrFTInfo = LHCb::Pr::FT::Info;
