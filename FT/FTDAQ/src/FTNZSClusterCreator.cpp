/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

// from boost
#include "boost/container/static_vector.hpp"

// from Gaudi
#include "LHCbAlgs/Transformer.h"

// LHCbKernel
#include "Detector/FT/FTChannelID.h"

// from FTEvent
#include "Event/FTCluster.h"
#include "Event/FTDigit.h"
#include "Event/FTLiteCluster.h"

namespace LHCb::FT {

  /**
   *  @author Lex Greeven, Sevda Esen
   *  @date   2020-03-09
   */
  class NZSClusterCreator : public Algorithm::Transformer<FTLiteCluster::FTLiteClusters( const FTDigit::FTDigits& )> {
  public:
    NZSClusterCreator( const std::string& name, ISvcLocator* pSvcLocator );

    FTLiteCluster::FTLiteClusters operator()( const FTDigit::FTDigits& digits ) const override;

  private:
    // Job options
    // cluster size options
    Gaudi::Property<unsigned int> m_clusterMaxWidth{this, "ClusterMaxWidth", 4, "Maximal cluster width"};
    Gaudi::Property<float>        m_lowestFraction{this, "LowestFraction", -0.250,
                                            "The fraction is defined in the range (-0.250,0.750)"};
    // Threshold settings.
    Gaudi::Property<bool> m_usePEnotADC{
        this, "UsePENotADC", false, "Flag to use (float)PE instead of (int)ADC. Thresholds should be set accordingly"};

    Gaudi::Property<float> m_adcThreshold1{this, "ADCThreshold1", 1, "add-to-cluster threshold"};
    Gaudi::Property<float> m_adcThreshold2{this, "ADCThreshold2", 2, "seed threshold"};
    Gaudi::Property<float> m_adcThreshold3{this, "ADCThreshold3", 3, "single-channel threshold"};
    Gaudi::Property<float> m_adcThreshold1Weight{this, "ADCThreshold1Weight", 1., "add-to-cluster weight"};
    Gaudi::Property<float> m_adcThreshold2Weight{this, "ADCThreshold2Weight", 2., "seed weight"};
    Gaudi::Property<float> m_adcThreshold3Weight{this, "ADCThreshold3Weight", 6., "single-channel weight"};

    // type to have pair of digits and the flag for cluster candidates
    enum clusterFlag {
      small,          // 0
      largeEdgeFirst, // 1
      largeMiddle,    // 2
      largeEdgeSecond // 3
    };

    typedef std::pair<std::vector<const FTDigit*>, clusterFlag> clusterCandidate;
    typedef std::vector<clusterCandidate>                       clusterCandidates;

    // loop over digits to find cluster candidates, flagged them for the size
    // large clusters are cutted at max size but not fragmented
    void findClusterCandidates( const FTDigit::FTDigits& digits, clusterCandidates& candidates ) const;

    // loop over cluster candidates, check cluster thresholds
    FTLiteCluster::FTLiteClusters makeClusters( clusterCandidates&             candidates,
                                                FTLiteCluster::FTLiteClusters& clusterCont ) const;

    float charge( const FTDigit& digit ) const { return digit.adcCount(); };
  };

  // Declaration of the Algorithm Factory
  DECLARE_COMPONENT_WITH_ID( NZSClusterCreator, "FTNZSClusterCreator" )

} // namespace LHCb::FT

LHCb::FT::NZSClusterCreator::NZSClusterCreator( const std::string& name, ISvcLocator* pSvcLocator )
    : Transformer( name, pSvcLocator, KeyValue{"InputLocation", LHCb::FTDigitLocation::Default},
                   KeyValue{"OutputLocation", LHCb::FTLiteClusterLocation::Default + "fromNZSdigits"} ) {}

//=============================================================================
// Main execution
//=============================================================================
LHCb::FTLiteCluster::FTLiteClusters LHCb::FT::NZSClusterCreator::operator()( const FTDigit::FTDigits& digits ) const {

  FTLiteCluster::FTLiteClusters clusterCont{};
  clusterCont.reserve( digits.size() );

  clusterCandidates candidates{};
  candidates.reserve( 15e3 );

  findClusterCandidates( digits, candidates );
  debug() << "found " << candidates.size() << " candidates " << endmsg;

  clusterCont = makeClusters( candidates, clusterCont );

  debug() << "found " << clusterCont.size() << " clusters " << endmsg;

  return clusterCont;
}

//----------------------------------------------
// Main clustering, check for neighbouring digits over threshold1
// Sum of total change should be over either treshold 2 or 3 for single clusters
// No check for fragmented clusters as total charge doesn't represent real total charge
//----------------------------------------------
void LHCb::FT::NZSClusterCreator::findClusterCandidates(
    const FTDigit::FTDigits& digits, LHCb::FT::NZSClusterCreator::clusterCandidates& candidates ) const {

  // simple lambda: check if two digits are next to each other in same SiPM
  // n=1 next, n=-1 previous
  auto compare = []( auto a, auto b, int n ) {
    return ( a.channelID().globalSiPMID() == b.channelID().globalSiPMID() &&
             a.channelID().channel() == b.channelID().channel() + n );
  };

  // FTDigit startClusIter; // begin channel of cluster
  // FTDigit stopClusIter; // end channel of cluster
  // bool startClustering = false; // bool to check if clustering started already

  // Digit Container is sorted wrt channelID
  auto digitRange = digits.range();
  auto digitIter  = digitRange.end();

  std::vector<const LHCb::FTDigit*> digitVec;
  digitVec.reserve( m_clusterMaxWidth );

  // loop over digits
  while ( digitIter != digitRange.begin() ) {
    digitVec.clear();

    if ( charge( ( *digitIter ) ) >= m_adcThreshold1 ) {

      // ADC above seed : start clustering
      if ( msgLevel( MSG::VERBOSE ) )
        verbose() << " ---> START NEW CLUSTER WITH SEED @ " << ( *digitIter ).channelID() << endmsg;

      digitVec.push_back( &( *digitIter ) );
      auto startClusIter = digitIter; // begin channel of cluster
      auto stopClusIter  = digitIter; // end channel of cluster

      // loop till below threshold1 or end, optionally exit when cluster bigger than a limit
      while ( ( --digitIter != digitRange.begin() ) && ( charge( *digitIter ) >= m_adcThreshold1 ) &&
              ( ( startClusIter - stopClusIter + 1 ) < m_clusterMaxWidth ) ) {
        // current digit in the same SiPM, and neighbour channel to last channel
        if ( compare( *digitIter, *stopClusIter, +1 ) ) {
          stopClusIter = digitIter;
          digitVec.push_back( &( *digitIter ) );
        } else
          break; // end
      }

      bool isInFragBefore( false ), isInFragAfter( false );

      // check if this is a fragment of an already divided cluster
      if ( startClusIter > digitRange.begin() ) {
        isInFragBefore = ( charge( *( startClusIter + 1 ) ) >= m_adcThreshold1 &&
                           compare( *( startClusIter + 1 ), *startClusIter, -1 ) );
      }
      // check if there are any further fragments
      if ( stopClusIter < digitRange.end() - 1 ) {
        isInFragAfter = ( charge( *( stopClusIter - 1 ) ) >= m_adcThreshold1 &&
                          compare( *( stopClusIter - 1 ), *stopClusIter, +1 ) );
      }

      if ( msgLevel( MSG::VERBOSE ) )
        verbose() << " ---> Done with cluster finding, now calculating charge / frac.pos" << endmsg;

      // manage the 3 categories of clusters according to the config flags
      // (single (0), edge(1), middle(2)
      bool isLarge = ( isInFragBefore || isInFragAfter );
      bool isEdge  = ( isInFragBefore != isInFragAfter );

      // unsigned int flag= (!isLarge          ? 0:
      // isLarge && isEdge ? 1: 2);

      clusterFlag flag = ( !isLarge ? clusterFlag::small
                                    : isLarge && isEdge && isInFragAfter
                                          ? clusterFlag::largeEdgeFirst
                                          : isLarge && isEdge && isInFragBefore ? clusterFlag::largeEdgeSecond
                                                                                : clusterFlag::largeMiddle );

      candidates.emplace_back( digitVec, flag );
    } // first digit over threshold1
    else
      digitIter--;
  } // end loop over digits
}

//-----------------------------------------------
// Make raw clusters, this will be passed on to the encoder
//-----------------------------------------------
LHCb::FTLiteCluster::FTLiteClusters
LHCb::FT::NZSClusterCreator::makeClusters( LHCb::FT::NZSClusterCreator::clusterCandidates& candidates,
                                           LHCb::FTLiteCluster::FTLiteClusters&            clusterCont ) const {

  if ( msgLevel( MSG::VERBOSE ) ) verbose() << " Start making clusters " << endmsg;
  std::vector<LHCb::Detector::FTChannelID> ids;
  ids.reserve( 4 );

  for ( const auto& cand : candidates ) {
    ids.clear();
    float totalCharge  = 0.0;
    float wsumPosition = 0.0;

    // Loop over digits in the cluster
    unsigned int widthClus  = cand.first.size();
    auto         startDigit = cand.first[0];

    unsigned int i = 0;
    for ( auto clusDigit : cand.first ) {

      ids.push_back( ( clusDigit )->channelID() );
      float channelWeight = charge( *clusDigit );
      if ( !m_usePEnotADC )
        channelWeight = ( channelWeight >= m_adcThreshold3
                              ? m_adcThreshold3Weight.value()
                              : channelWeight >= m_adcThreshold2
                                    ? m_adcThreshold2Weight.value()
                                    : channelWeight >= m_adcThreshold1 ? m_adcThreshold1Weight.value() : 0. );

      totalCharge += channelWeight;

      // mean position will be [ (rel. pos. from left) * charge ] / totalCharge
      //(see below for fragmented clusters)
      wsumPosition += i * channelWeight;
      i++;
    } // end of loop over digits in 'cluster'

    // single/small clusters should pass one of the two thresholds
    // no check is done for fragmented clusters or large clusters
    if ( ( cand.second > 0 ) || ( widthClus == 1 && totalCharge > m_adcThreshold3Weight.value() - 0.5 ) ||
         ( widthClus > 1 && totalCharge > m_adcThreshold1Weight.value() + m_adcThreshold2Weight.value() - 0.5 ) ) {
      // compute position: the middle of the cluster only (no weighting), otherwise use weights
      // also add channelID (uint) offset
      float clusPosition = ( cand.second == 0 ? ( startDigit )->channelID() + wsumPosition / totalCharge
                                              : ( startDigit )->channelID() + float( widthClus - 1 ) / 2 );

      // The fractional position is defined in (-0.250, 0.750)
      unsigned int clusChanPosition     = std::floor( clusPosition - m_lowestFraction );
      float        fractionChanPosition = ( clusPosition - clusChanPosition );
      int          frac                 = int( 2 * ( fractionChanPosition - m_lowestFraction ) );

      unsigned int flag = cand.second;
      // second Edge is treated differently
      // large flag is 1 and fraction is always 0 to distinguish it from first edge
      // instead of center channel, we send last channel number
      if ( cand.second == clusterFlag::largeEdgeSecond ) {
        flag                 = 1;
        frac                 = 0;
        clusChanPosition     = ( startDigit )->channelID() + widthClus - 1;
        fractionChanPosition = 0;
      }

      // save single clusters and edge clusters, or all if keepBigCluster is on
      auto channelID = LHCb::Detector::FTChannelID{clusChanPosition};
      auto quarter   = channelID.globalQuarterIdx();
      clusterCont.addHit( std::forward_as_tuple( channelID, frac, flag ), quarter );

    } // total charge check
  }   // end candidate loop

  return clusterCont;
}
//-------------------------------------------------------------------
