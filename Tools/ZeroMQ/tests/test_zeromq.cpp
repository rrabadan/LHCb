/*****************************************************************************\
* (c) Copyright 2020 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include <iostream>
#include <string>
#include <thread>
#include <vector>

#include <ZeroMQ/IZeroMQSvc.h>
#include <ZeroMQ/ZeroMQPoller.h>
#include <ZeroMQ/functions.h>

#include <catch2/catch.hpp>

TEST_CASE( "ZeroMQPoller" ) {

  bool                     error = false;
  IZeroMQSvc               zmqSvc{};
  std::vector<std::string> messages;
  messages.reserve( 2 );

  auto control = zmqSvc.socket( zmq::PAIR );
  control.bind( "inproc://control" );

  std::thread poll_thread{[&zmqSvc, &messages]() {
    auto s1 = zmqSvc.socket( zmq::PAIR );
    s1.bind( "inproc://test1" );
    auto s2 = zmqSvc.socket( zmq::PAIR );
    s2.bind( "inproc://test2" );

    auto control = zmqSvc.socket( zmq::PAIR );
    control.connect( "inproc://control" );
    zmqSvc.send( control, "READY" );

    auto poller = ZeroMQPoller{};
    auto id_s1  = poller.register_socket( s1, zmq::POLLIN );
    auto id_s2  = poller.register_socket( s2, zmq::POLLIN );

    std::vector<std::pair<size_t, int>> ids;
    int                                 tries = 4;

    while ( messages.size() < 2 && --tries > 0 ) {
      auto ids = poller.poll( 1000 );
      for ( const auto& entry : ids ) {
        if ( entry.first == id_s1 && entry.second == zmq::POLLIN ) {
          messages.emplace_back( zmqSvc.receive<std::string>( s1 ) );
        }
        if ( entry.first == id_s2 && entry.second == zmq::POLLIN ) {
          messages.emplace_back( zmqSvc.receive<std::string>( s2 ) );
        }
      }
    }
  }};

  auto s1 = zmqSvc.socket( zmq::PAIR );
  zmq::setsockopt( s1, zmq::SNDTIMEO, 1000 );
  s1.connect( "inproc://test1" );
  auto s2 = zmqSvc.socket( zmq::PAIR );
  zmq::setsockopt( s2, zmq::SNDTIMEO, 1000 );
  s2.connect( "inproc://test2" );

  auto msg = zmqSvc.receive<std::string>( control );
  CHECK( msg == "READY" );

  try {
    zmqSvc.send( s1, std::string{"test1"} );
    zmqSvc.send( s2, std::string{"test2"} );
  } catch ( ... ) { error = true; }

  poll_thread.join();

  CHECK( error == false );
  CHECK( messages == std::vector<std::string>{{{"test1"}, {"test2"}}} );
}

#include <ZeroMQ/SerializeSize.h>

TEST_CASE( "Serialize" ) {

  auto t = std::make_tuple( 1, 1, 5, "test" );

  SECTION( "tuple" ) { CHECK( Serialize::serialize_size( t ) == 20 ); }
  SECTION( "vector of tuple" ) {
    auto v = std::vector<decltype( t )>{1, t};
    CHECK( Serialize::serialize_size( v ) == 20 );
  }
}
