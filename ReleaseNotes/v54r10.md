2023-06-21 LHCb v54r10
===

This version uses Detector [v1r14](../../../../Detector/-/tags/v1r14), Gaudi [v36r14](../../../../Gaudi/-/tags/v36r14) and LCG [103](http://lcginfo.cern.ch/release/103/) with ROOT 6.28.00.

This version is released on the `master` branch.
Built relative to LHCb [v54r9](/../../tags/v54r9), with the following changes:

### New features ~"new feature"



### Fixes ~"bug fix" ~workaround

- ~Persistency | Fix HltDecReportWriter when TaskID > 0, !4162 (@graven)


### Enhancements ~enhancement

- ~Core | Fix detection and propagation of FSRs from input files, !4138 (@clemenci)


### Code cleanups and changes to tests ~modernisation ~cleanup ~testing



### Documentation ~Documentation


### Other

- Merging for sprucing output, !4165 (@nskidmor)
