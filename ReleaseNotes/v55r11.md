2024-07-08 LHCb v55r11
===

This version uses
Detector [v1r34](../../../../Detector/-/tags/v1r34),
Gaudi [v38r1](../../../../Gaudi/-/tags/v38r1) and
LCG [105a](http://lcginfo.cern.ch/release/105a/) with ROOT 6.30.04.

This version is released on the `2024-patches` branch.
Built relative to LHCb [v55r10p1](/../../tags/v55r10p1), with the following changes:

### New features ~"new feature"



### Fixes ~"bug fix" ~workaround

- ~Persistency | PackedDataChecksum: Avoid padding bits in checksums for aggregates, !4624 (@jonrob) [Moore#793]


### Enhancements ~enhancement

- Add counters to packing of MCParticle and MCVertex, !4627 (@graven)
- Support enum values in TupleObj::column, !4588 (@graven)


### Code cleanups and changes to tests ~modernisation ~cleanup ~testing

- ~Persistency | Minor follow-up cleanup of PackedDataChecksum.h, !4628 (@jonrob)


### Documentation ~Documentation


### Other

- ~Tracking ~VP | VPMicroCluster: Define setters for interstrip fraction from uint8_t instead of float, !4620 (@jonrob) [Moore#793]
- ~Tracking ~VP ~UT ~FT | Add forced sorting to indexed VP, FT and UT hit cluster containers, !4595 (@ldufour)
- ~UT | Add functionality to force MultiIndexedContainer to contain unique entries, !4625 (@ldufour)
- Add lines_maker option to LbExec options to be compatible with Moore options, !4631 (@sesen)
- Add UT hit error scaling factor as a condition, !4596 (@hawu)
