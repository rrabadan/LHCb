2023-01-27 LHCb v54r4
===

This version uses
Gaudi [v36r9](../../../../Gaudi/-/tags/v36r9),
Detector [v1r8](../../../../Detector/-/tags/v1r8) and
LCG [101a](http://lcginfo.cern.ch/release/101a_LHCB_7/) with ROOT 6.24.08.

This version is released on the `master` branch.
Built relative to LHCb [v54r3](/../../tags/v54r3), with the following changes:

### Fixes ~"bug fix" ~workaround

- Fixed an indexing error in transfer of covariance matrixes in SOATrackConversion.h, !3937 (@spradlin) [#285]


### Code cleanups and changes to tests ~modernisation ~cleanup ~testing

- ~Configuration | Speed up python configuration and improve profiling, !3829 (@rmatev) [#261,Moore#25]
- ~RICH | Add allocate/deallocate mismatch checking code to RICH derived conditions, !3928 (@jonrob) [#284]


### Other

- ~Calo ~"Event model" | Add track-cluster match info to neutral pp, !3913 (@mveghel)
- ~RICH | RichHistoBase: Making histogram booking const and thread safe, !3927 (@jonrob)
- ~Functors | Add a new configurable function for getting Selection Report location, !3845 (@amathad)
- ~Persistency | Needed for Moore!2006, !3929 (@sesen)
- ~Persistency | Selective packing for persistency, !3721 (@msaur)
- ~Core | Add missing invocation of DetectorDataService::finalize, !3932 (@clemenci)
- ~Core | ParamFileSvc - Improve error reporting when given URL fails to be opened, !3920 (@jonrob)
- Vocabulary event model functions: treat a SmartRef as-if it is a pointer, !3941 (@graven)
- Remove code obsoleted by selective packing, !3936 (@graven)
- Fix warning suppression for C++20 extension, !3931 (@clemenci)
- Fix name collisions in XMLSummaryKernel tests, !3930 (@clemenci)
- Force test validator to take into account stderr exclusions, !3925 (@clemenci)
- Fixes to be able to build public headers in isolation, !3857 (@clemenci)
- Add stderr exclusion for switch to Geant4 units, !3924 (@clemenci)
- Cleanup LinksByKey interface, !3923 (@graven)
- Unify various Linker wrapper classes, !3917 (@graven)
- Add helper functions for UT hits and seed tracks, !3915 (@decianm)
- Small fixes to compile on aarch64, !3910 (@clemenci)
- Add a few new platforms to PlatformInfo, !3908 (@clemenci)
- Fix warnings seen in a super project build, !3914 (@rmatev)
- More const-correct Linked{From,To}, !3912 (@graven)
