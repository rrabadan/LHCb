2024-08-22 LHCb v55r12
===

This version uses
Detector [v1r35](../../../../Detector/-/tags/v1r35),
Gaudi [v38r1](../../../../Gaudi/-/tags/v38r1) and
LCG [105a](http://lcginfo.cern.ch/release/105a/) with ROOT 6.30.04.

This version is released on the `2024-patches` branch.
Built relative to LHCb [v55r11](/../../tags/v55r11), with the following changes:

### New features ~"new feature"

- Extend HltDecReportsFilter to select with regex, !4593 (@sesen) [Moore#778]
- A shallow copy algorithm for all persistable keyed container types, !4541 (@sesen)

### Fixes ~"bug fix" ~workaround

- ~Decoding | Return empty SelReport when decoding fails integrity checks, !4646 (@thboettc)
- ~Persistency | Set IODataManager AgeLimit = 0, !4641 (@cburr)
- Explicitly enable TGeoManager multi-threading for DD4hepSvc to stop seg-faults, !4652 (@isanders)

### Enhancements ~enhancement

- Suppress TTree summary printout, !4632 (@roneil)
- Add JacobdP4dMom version considering charge, !4512 (@hjage)
- Expose UT sector dxDy for Allen., !4640 (@dtou)
- Port of default ParticleTable from DDDB upgrade/master, !4579 (@hjage)

### Code cleanups and changes to tests ~modernisation ~cleanup ~testing

- Change input MDF for RICH 2022 decode test, !4642 (@jonrob)
- Refactor Rec's loop_mask to LHCbKernel's traits, !4637 (@ldufour)
