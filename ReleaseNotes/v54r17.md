2023-09-11 LHCb v54r17
===

This version uses
Gaudi [v36r16](../../../../Gaudi/-/tags/v36r16),
Detector [v1r19](../../../../Detector/-/tags/v1r19) and
LCG [103](http://lcginfo.cern.ch/release/103/) with ROOT 6.28.00.

This version is released on the `` branch.
Built relative to LHCb [v54r16](/../../tags/v54r16), with the following changes:

### New features ~"new feature"

- ~Decoding ~FT ~Monitoring | Basic error monitoring for SciFi, !4156 (@bleverin)
- ~Luminosity | Resolve "Follow-up from "Service to write luminosity information to TTree when reading from FSRs"", !4270 (@efranzos) [#327]

### Fixes ~"bug fix" ~workaround



### Enhancements ~enhancement



### Code cleanups and changes to tests ~modernisation ~cleanup ~testing



### Documentation ~Documentation


### Other

- ~Decoding ~FT | Add 273505 as the first run where FT banks are properly labelled, !4273 (@lohenry) [#319]
- ~RICH ~"MC checking" | Strip RichSmartID time info when used for MC associations, !4281 (@jonrob)

