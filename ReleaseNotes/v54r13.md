2023-07-31 LHCb v54r13
===

This version uses
Detector [v1r17](../../../../Detector/-/tags/v1r17),
Gaudi [v36r16](../../../../Gaudi/-/tags/v36r16) and
LCG [103](http://lcginfo.cern.ch/release/103/) with ROOT 6.28.00.

This version is released on the `master` branch.
Built relative to LHCb [v54r12](/../../tags/v54r12), with the following changes:

### New features ~"new feature"

- ~Decoding ~RICH | Upgrade RichSmartID channel identifier to use 64bits., !4166 (@jonrob)


### Fixes ~"bug fix" ~workaround



### Enhancements ~enhancement



### Code cleanups and changes to tests ~modernisation ~cleanup ~testing

- ~Build | Add x86_64_v3-el9-gcc12+cuda12_1-opt+g to known platform list, !4202 (@jonrob)
- Dropped unused (I)RateFromTCK, !4213 (@sponce)


### Documentation ~Documentation


### Other

- ~Decoding ~FT | Add workaround in FT decoding for RawBanks incorrectly labeled as v7 when infact they are v8, !4210 (@lohenry)
- ~RICH ~"MC checking" | RICH Support for 'Reco From MC Information' for upgrade studies, !4181 (@jonrob)
- ~"Event model" ~Persistency | Split track containers and persistency (versioning), !4127 (@mveghel)
- ~Build | Suppress warning with gcc 12.1, !4203 (@jonrob)
- Fill EventStats for 2022 reprocessing, !4224 (@mstahl)
- Dropped unused EventCountHisto algorithm, !4197 (@sponce)
- MDF copy writer for 2022 reprocessing, !4176 (@mstahl)
