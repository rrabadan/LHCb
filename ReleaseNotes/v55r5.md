2024-04-03 LHCb v55r5
===

This version uses
Detector [v1r30](../../../../Detector/-/tags/v1r30),
Gaudi [v38r1](../../../../Gaudi/-/tags/v38r1) and
LCG [105a](http://lcginfo.cern.ch/release/105a/) with ROOT 6.30.04.

This version is released on the `master` branch.
Built relative to LHCb [v55r4](/../../tags/v55r4), with the following changes:

### New features ~"new feature"



### Fixes ~"bug fix" ~workaround



### Enhancements ~enhancement



### Code cleanups and changes to tests ~modernisation ~cleanup ~testing

- PackedCaloHypo simple warning message modification, !4470 (@msaur)


### Documentation ~Documentation


### Other

