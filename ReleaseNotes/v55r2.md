2024-03-05 LHCb v55r2
===

This version uses
Gaudi [v38r0](../../../../Gaudi/-/tags/v38r0),
Detector [v1r27](../../../../Detector/-/tags/v1r27) and
LCG [105a](http://lcginfo.cern.ch/release/105a/) .

This version is released on the `master` branch.
Built relative to LHCb [v55r1](/../../tags/v55r1), with the following changes:

### New features ~"new feature"



### Fixes ~"bug fix" ~workaround

- Fix truth matching persistency bug, !4453 (@jzhuo)


### Enhancements ~enhancement

- ~"Event model" | Add ODIN EventType bit accessor and clean up TriggerTypes, !4192 (@rmatev)


### Code cleanups and changes to tests ~modernisation ~cleanup ~testing

- Cleanup and improve ZeroMQ tests, !4448 (@raaij) [#343]


### Documentation ~Documentation


### Other

- ~Decoding ~Monitoring | Updated PLUME decoding in HLT2, !4435 (@fferrari)
- ProtoParticle: avoid comparing SmartRef to 0, use nullptr instead, !4452 (@graven)
- Tiny ref fix fir ex2, !4451 (@sponce)
- Introduce number of track parameters in TrackParameters, !4450 (@ausachov)
- Fixed Muon decoding test, !4449 (@sponce)
- Allow unknown locations when unpacking for tistos, !4445 (@sesen)
- Fix Allen UT geometry, !4395 (@jzhuo)
- UThit position methods, !4269 (@hawu)
- Added VELO Error Bank to RawEvent.h, !4409 (@jabrown)
