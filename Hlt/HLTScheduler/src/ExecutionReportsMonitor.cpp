/*****************************************************************************\
* (c) Copyright 2019-2022 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "Gaudi/Accumulators/Histogram.h"
#include "GaudiKernel/EventContext.h"
#include "GaudiKernel/StatusCode.h"
#include "Kernel/EventContextExt.h"
#include "Kernel/ISchedulerConfiguration.h"
#include "LHCbAlgs/Consumer.h"
#include <map>
#include <string>
#include <vector>

class ExecutionReportsMonitor final : public LHCb::Algorithm::Consumer<void( EventContext const& )> {
public:
  ExecutionReportsMonitor( const std::string& name, ISvcLocator* pSvcLocator ) : Consumer{name, pSvcLocator} {}
  StatusCode start() override;
  void       operator()( EventContext const& evtCtx ) const override;

private:
  ServiceHandle<LHCb::Interfaces::ISchedulerConfiguration> m_scheduler{this, "Scheduler", "HLTControlFlowMgr"};

  mutable std::optional<Gaudi::Accumulators::Histogram<1, Gaudi::Accumulators::atomicity::full, unsigned int>>
      m_execCount;
  mutable std::optional<Gaudi::Accumulators::Histogram<1, Gaudi::Accumulators::atomicity::full, unsigned int>>
      m_passCount;
  mutable std::optional<Gaudi::Accumulators::ProfileHistogram<1, Gaudi::Accumulators::atomicity::full, unsigned int>>
      m_passFraction;

  std::map<std::string, int> m_name_indices;
};

DECLARE_COMPONENT( ExecutionReportsMonitor )

StatusCode ExecutionReportsMonitor::start() {
  return Consumer::start().andThen( [&]() {
    m_name_indices = m_scheduler->getNodeNamesWithIndices();

    std::vector<std::string> labels;
    labels.reserve( m_name_indices.size() );
    for ( const auto& [decision_name, index] : m_name_indices ) { labels.push_back( decision_name ); }
    auto size = static_cast<unsigned int>( labels.size() );
    Gaudi::Accumulators::Axis<decltype( m_execCount )::value_type::AxisArithmeticType> axis{size, 0, size, "", labels};

    m_execCount.emplace( this, "exec_count", "Number of times a node was executed", axis );
    m_passCount.emplace( this, "pass_count", "Number of times a node passed", axis );
    m_passFraction.emplace( this, "pass_frac", "Fraction of times a node passed", axis );

    return StatusCode::SUCCESS;
  } );
}

void ExecutionReportsMonitor::operator()( EventContext const& evtCtx ) const {
  auto const& lhcbExt = evtCtx.getExtension<LHCb::EventContextExtension>();
  auto const& state   = lhcbExt.getSchedulerExtension<LHCb::Interfaces::ISchedulerConfiguration::State>();

  for ( const auto& [i, item] : LHCb::range::enumerate( m_name_indices ) ) {
    const auto& [name, index] = item;
    const auto& node          = state.node( index );
    bool        executed      = node.executionCtr == 0;
    bool        passed = executed && node.passed; // Attention! node.passed is true when the node hasn't been executed
    m_execCount.value()[i] += executed;
    if ( executed ) {
      m_passCount.value()[i] += passed;
      m_passFraction.value()[i] += passed;
    }
  }
}
