###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from Configurables import (
    HLTControlFlowMgr,
    HiveWhiteBoard,
    HiveDataBrokerSvc,
    ConfigurableDummy,
)
from Gaudi.Configuration import ApplicationMgr

isolated_fails = ([True] * 9 + [False] * 1) * 10
algs = [
    ConfigurableDummy(
        name='IsolatedFails',
        CFDs=isolated_fails,
        DecisionException=True,
    ),
]

whiteboard = HiveWhiteBoard("EventDataSvc", EventSlots=1)

HLTControlFlowMgr().CompositeCFNodes = [
    ('top', 'NONLAZY_OR', [a.name() for a in algs], True),
]

HLTControlFlowMgr().StopAfterNFailures = 2
HLTControlFlowMgr().ThreadPoolSize = 1

app = ApplicationMgr(
    EvtMax=100,
    EvtSel='NONE',
    ExtSvc=[whiteboard, 'Gaudi::Monitoring::MessageSvcSink'],
    EventLoop=HLTControlFlowMgr(StopAfterNFailures=3),
    TopAlg=algs)

HiveDataBrokerSvc().DataProducers = app.TopAlg
