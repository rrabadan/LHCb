###############################################################################
# (c) Copyright 2019-2022 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""Write a DST file for consumption in subsequent tests."""
from Configurables import (
    WriteHandleAlg,
    OutputStream,
    HistoAlgorithm,
    ApplicationMgr,
    HLTControlFlowMgr,
    HiveDataBrokerSvc,
    HiveWhiteBoard,
    # EvtStoreSvc,
    Gaudi__RootCnvSvc as RootCnvSvc,
    Gaudi__Monitoring__MessageSvcSink as MessageSvcSink,
    EventPersistencySvc,
    IODataManager,
    HistogramPersistencySvc,
)
from Gaudi.Configuration import FileCatalog
from Gaudi.Configuration import WARNING

n_slots = 1
n_threads = 1

producer = WriteHandleAlg(
    "Producer",
    UseHandle=True,
    Output="/Event/MyCollision",
    OutputLevel=WARNING,
)
writer = OutputStream(
    OutputLevel=WARNING,
    ItemList=[str(producer.Output) + "#1"],
    Output="DATAFILE='PFN:data.dst'  SVC='Gaudi::RootCnvSvc' OPT='RECREATE'")
old_histos_mon = HistoAlgorithm()
all_algs = [producer, writer, old_histos_mon]
scheduler = HLTControlFlowMgr(
    "HLTControlFlowMgr",
    CompositeCFNodes=[('top', 'LAZY_AND', [a.getFullName() for a in all_algs],
                       True)],
    ThreadPoolSize=n_threads,
)
HiveDataBrokerSvc(DataProducers=all_algs)
event_store = HiveWhiteBoard(
    # event_store = EvtStoreSvc(  # TODO with this output is not written at all
    "EventDataSvc",
    DataLoader=EventPersistencySvc(
        CnvServices=[RootCnvSvc()], OutputLevel=WARNING),
    EventSlots=n_slots,
    ForceLeaves=True,
    RootCLID=1,
    EnableFaultHandler=True,
)

ApplicationMgr(
    EvtMax=100,
    EvtSel="NONE",
    HistogramPersistency="ROOT",  # enable legacy histogram saving
    ExtSvc=[
        event_store,
        FileCatalog(Catalogs=["xmlcatalog_file:scheduler.xml"]),
        IODataManager(),
        MessageSvcSink(),
        HistogramPersistencySvc(OutputFile="write.legacy_histos.root"),
    ],
    EventLoop=scheduler,
)

# Print something about the output so it can be compared to the reference
import atexit, os


def print_output():
    os.system("root -b -l -q data.dst -e 'Event->Print()'")
    os.system("root -b -l -q write.legacy_histos.root -e '.ls'")


atexit.register(print_output)
