/*****************************************************************************\
* (c) Copyright 2024 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "Event/HltDecReports.h"
#include "LHCbAlgs/FilterPredicate.h"

#include "Gaudi/Accumulators.h"
#include "GaudiKernel/GaudiException.h"

#include <algorithm>
#include <string>
#include <vector>

#include <numeric>
/** @class HltDecReportsFilter
 *
 *  Checks content of DecReports for list of requested lines
 *  or list of regex expressions
 *  Filter fails if none of the lines have positive decision
 */
class HltDecReportsFilter final : public LHCb::Algorithm::FilterPredicate<bool( const LHCb::HltDecReports& )> {

  Gaudi::Property<std::vector<std::string>> m_lines{this, "Lines", {}};

  std::regex                   m_regex;
  Gaudi::Property<std::string> m_str{this, "Regex", "", [this]( const auto& ) { this->m_regex = this->m_str.value(); },
                                     "Line name regex"};

public:
  /// Standard constructor
  HltDecReportsFilter( const std::string& name, ISvcLocator* pSvcLocator )
      : FilterPredicate{name, pSvcLocator, KeyValue{"DecReports", ""}} {}

  StatusCode initialize() override {
    return FilterPredicate::initialize().andThen( [&] {
      if ( !m_str.empty() and !m_lines.empty() )
        throw GaudiException( "This filter should be used either with line list or a regex. Check the configuration. ",
                              __PRETTY_FUNCTION__, StatusCode::FAILURE );
    } );
  }

  bool operator()( const LHCb::HltDecReports& decReports ) const override {

    auto any_fired = [&]( std::vector<std::string> const& lines ) {
      return std::any_of( lines.begin(), lines.end(), [&]( const auto& line ) -> bool {
        if ( auto i = decReports.find( line ); i != decReports.end() ) { return i->second.decision(); }
        throw GaudiException( "requested line not present", __PRETTY_FUNCTION__, StatusCode::FAILURE );
      } );
    };

    auto any_matched = [&]( auto const& regex ) {
      return std::any_of( decReports.begin(), decReports.end(), [&]( const auto& item ) {
        return item.second.decision() && std::regex_match( item.first, regex );
      } );
    };

    bool b = m_str.empty() ? any_fired( m_lines ) : any_matched( m_regex );
    m_cutEff += b;
    return b;
  }

  // Counter for recording cut retention statistics
  mutable Gaudi::Accumulators::BinomialCounter<> m_cutEff{this, "Cut selection efficiency"};
};

//=============================================================================
// Declaration of the Algorithm Factory
DECLARE_COMPONENT( HltDecReportsFilter )
//=============================================================================
