/*****************************************************************************\
* (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "Event/HltLumiSummary.h"
#include "Event/ODIN.h"
#include "Gaudi/Accumulators/Histogram.h"
#include "GaudiKernel/EventContext.h"
#include "GaudiKernel/StatusCode.h"
#include "Kernel/EventContextExt.h"
#include "Kernel/IANNSvc.h"
#include "Kernel/IIndexedLumiSchemaSvc.h"
#include "LHCbAlgs/Consumer.h"
#include <string>

using BXTypes = LHCb::ODIN::BXTypes;

namespace {
  template <typename T, typename K, typename OWNER>
  void map_emplace( T& t, K&& key, OWNER* owner, std::string const& name, std::string const& title,
                    Gaudi::Accumulators::Axis<typename T::mapped_type::AxisArithmeticType> axis1 ) {
    auto const [it, inserted] = t.emplace( std::piecewise_construct, std::forward_as_tuple( key ),
                                           std::forward_as_tuple( owner, name, title, axis1 ) );
    if ( !inserted ) {
      // TODO check that we're consistent if we're inserting the same histogram twice.
      // it->second;
    }
  }

  template <typename T, typename K, typename OWNER>
  void map_emplace( T& t, K&& key, OWNER* owner, std::string const& name, std::string const& title,
                    Gaudi::Accumulators::Axis<typename T::mapped_type::AxisArithmeticType> axis1,
                    Gaudi::Accumulators::Axis<typename T::mapped_type::AxisArithmeticType> axis2 ) {
    auto const [it, inserted] = t.emplace( std::piecewise_construct, std::forward_as_tuple( key ),
                                           std::forward_as_tuple( owner, name, title, axis1, axis2 ) );
    if ( !inserted ) {
      // TODO check that we're consistent if we're inserting the same histogram twice.
      // it->second;
    }
  }

  template <typename T>
  std::string to_string( const T& streamable ) {
    std::ostringstream oss;
    oss << streamable;
    return oss.str();
  }

  const auto AxisBCID = Gaudi::Accumulators::Axis<double>( 3565, -0.5, 3564.5 );
  const auto AxisTime = Gaudi::Accumulators::Axis<double>( 3600, 0, 3600 );
} // namespace

class HltLumiSummaryMonitor final
    : public LHCb::Algorithm::Consumer<void( const LHCb::HltLumiSummary&, const LHCb::ODIN& )> {
public:
  HltLumiSummaryMonitor( const std::string& name, ISvcLocator* pSvcLocator )
      : Consumer{name, pSvcLocator, {KeyValue{"Input", ""}, KeyValue{"ODIN", ""}}} {}
  StatusCode initialize() override;
  void       operator()( const LHCb::HltLumiSummary&, const LHCb::ODIN& ) const override;

private:
  void add( unsigned encodingKey ) const;
  int  threshold( std::string const& counter ) const {
    auto threshold = m_emptyThresholds.find( counter );
    return threshold != m_emptyThresholds.end() ? threshold->second : 0;
  }

  ServiceHandle<IIndexedLumiSchemaSvc> m_annSvc{this, "DecoderMapping", "HltANNSvc"};

  Gaudi::Property<std::vector<unsigned>> m_encodingKeys{
      this, "PreloadEncodingKeys", {}, "HltLumiSummary encoding keys to use for pre-initializing histograms"};

  Gaudi::Property<std::vector<std::string>> m_bcidCounters{
      this,
      "BCIDCounters",
      {"VeloTracks", "VeloVertices", "ECalEtot", "ECalET", "SciFiT3M45"},
      "Names of counters for which to make per BCID plots"};

  // counters name changed, so at the moment not applied
  Gaudi::Property<std::map<std::string, int>> m_emptyThresholds{this, "EmptyThresholds", {}};

  mutable std::map<std::pair<BXTypes, std::string>, Gaudi::Accumulators::Histogram<1>>        m_value;
  mutable std::map<std::pair<BXTypes, std::string>, Gaudi::Accumulators::ProfileHistogram<1>> m_time_mean;
  mutable std::map<std::pair<BXTypes, std::string>, Gaudi::Accumulators::ProfileHistogram<1>> m_time_pnz;

  mutable std::map<std::string, Gaudi::Accumulators::Histogram<2>>        m_bcid_value;
  mutable std::map<std::string, Gaudi::Accumulators::ProfileHistogram<1>> m_bcid_mean;

  mutable std::mutex         m_lock;
  mutable std::set<unsigned> m_knownKeys;
};

DECLARE_COMPONENT( HltLumiSummaryMonitor )

void HltLumiSummaryMonitor::add( unsigned encodingKey ) const {
  auto lock = std::scoped_lock{m_lock};

  if ( m_knownKeys.count( encodingKey ) ) return;
  info() << "Adding lumi schema encoding key " << encodingKey << endmsg;
  m_knownKeys.emplace( encodingKey );

  auto schema = m_annSvc->lumiCounters( encodingKey, 0 );

  for ( const auto& counterDef : schema.counters ) {
    const auto&        counter          = counterDef.name;
    const unsigned int maxValue         = 1 << counterDef.size;
    const unsigned int nBins            = std::min( maxValue, 2048u );
    const double       minValueScaled   = -counterDef.shift / counterDef.scale;
    const double       rangeValueScaled = maxValue / counterDef.scale;
    const double       maxValueScaled   = minValueScaled + rangeValueScaled;
    const double       binSize          = rangeValueScaled / nBins;

    for ( auto bx : {BXTypes::NoBeam, BXTypes::Beam1, BXTypes::Beam2, BXTypes::BeamCrossing} ) {
      map_emplace(
          m_value, std::make_pair( bx, counter ), this, "value/" + to_string( bx ) + "/" + counter,
          "Counter value for " + counter + "/" + to_string( bx ),
          Gaudi::Accumulators::Axis<double>( nBins, minValueScaled - binSize / 2., maxValueScaled - binSize / 2. ) );
      map_emplace( m_time_mean, std::make_pair( bx, counter ), this, "mean/" + to_string( bx ) + "/" + counter,
                   "Mean value per second for " + counter + "/" + to_string( bx ), AxisTime );
      map_emplace( m_time_pnz, std::make_pair( bx, counter ), this, "pnz/" + to_string( bx ) + "/" + counter,
                   "P(X > " + std::to_string( threshold( counter ) ) + ") per second for " + counter + "/" +
                       to_string( bx ),
                   AxisTime );
    }

    if ( std::find( m_bcidCounters.begin(), m_bcidCounters.end(), counter ) != m_bcidCounters.end() ) {
      map_emplace(
          m_bcid_value, counter, this, "value/bcid/" + counter, "Counter value per BCID for " + counter, AxisBCID,
          Gaudi::Accumulators::Axis<double>( nBins, minValueScaled - binSize / 2., maxValueScaled - binSize / 2. ) );
      map_emplace( m_bcid_mean, counter, this, "mean/bcid/" + counter, "Mean value per BCID for " + counter, AxisBCID );
    }
  }
}

StatusCode HltLumiSummaryMonitor::initialize() {
  for ( auto& key : m_encodingKeys ) { add( key ); }
  return Consumer::initialize();
}

void HltLumiSummaryMonitor::operator()( const LHCb::HltLumiSummary& summary, const LHCb::ODIN& odin ) const {
  if ( msgLevel( MSG::VERBOSE ) ) {
    auto default_precision = verbose().precision();
    verbose() << "Counters: {" << std::setprecision( 19 );
    for ( const auto& [counter, value] : summary.extraInfo() ) { verbose() << counter << ':' << value << ", "; }
    verbose() << '}' << std::setprecision( default_precision ) << endmsg;
  }

  unsigned encodingKey = summary.info( "encodingKey", 0 );
  if ( encodingKey == 0 ) return;
  add( encodingKey );

  const auto time = ( odin.gpsTime() / 1000000 ) % 3600;
  for ( const auto& [counter, value] : summary.extraInfo() ) {
    const auto key  = std::make_pair( odin.bunchCrossingType(), counter );
    const auto bcid = odin.bunchId();
    if ( m_value.find( key ) == m_value.end() ) { error() << "No histogram defined with key " << key << endmsg; }
    ++m_value.at( key )[value];
    m_time_mean.at( key )[time] += value;
    m_time_pnz.at( key )[time] += ( value > threshold( counter ) );
    if ( auto const it = m_bcid_value.find( counter ); it != m_bcid_value.end() ) {
      ++( it->second )[{bcid, value}];
      m_bcid_mean.at( counter )[bcid] += value;
    }
  }
}
