###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from PyConf.Algorithms import RawEventDump
from PyConf.application import ApplicationOptions, configure, configure_input
from PyConf.control_flow import CompositeNode

options = ApplicationOptions(_enabled=False)
options.evt_max = 10
options.set_input_and_conds_from_testfiledb("2012_raw_default")
options.input_files = ["rewritten.mdf"]

dump = RawEventDump(RawBanks=["HltSelReports", "HltDecReports"], DumpData=True)

node = CompositeNode("Seq", children=[dump])
config.update(configure(options, node))
