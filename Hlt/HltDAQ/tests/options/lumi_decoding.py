###############################################################################
# (c) Copyright 2022 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from Gaudi.Configuration import VERBOSE
from PRConfig.TestFileDB import test_file_db

from PyConf.application import (
    default_raw_banks,
    configure_input,
    configure,
    make_odin,
    CompositeNode,
)
from PyConf.Algorithms import (
    HltRoutingBitsFilter,
    HltLumiSummaryDecoder,
    HltLumiSummaryMonitor,
    PrintHeader,
)

options = test_file_db["2023_raw_hlt1_269939"].make_lbexec_options(
    evt_max=2000,
    histo_file='lumi_decoding_histo.root',
    mdf_ioalg_name='IOAlgFileRead')

configure_input(options)
odin = make_odin()

rb_filter = HltRoutingBitsFilter(
    name="RequireLumiRoutingBit",
    RequireMask=(1 << 1, 0, 0),
    RawBanks=default_raw_banks('HltRoutingBits'),
    PassOnError=False)
summary = HltLumiSummaryDecoder(
    name="HltLumiSummaryDecoder",
    RawBanks=default_raw_banks("HltLumiSummary")).OutputContainerName
monitor = HltLumiSummaryMonitor(
    name="HltLumiSummaryMonitor",
    Input=summary,
    ODIN=odin,
    OutputLevel=VERBOSE)
print_event = PrintHeader(name="PrintHeader", ODINLocation=odin)

top_node = CompositeNode("Top", [rb_filter, print_event, monitor])
configure(options, top_node)

from Configurables import LHCb__DetDesc__ReserveDetDescForEvent as reserveIOV
reserveIOV("reserveIOV").PreloadGeometry = False
