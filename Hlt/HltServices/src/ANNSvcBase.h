/*****************************************************************************\
* (c) Copyright 2000-2024 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once
#include "GaudiKernel/Service.h"
#include "Kernel/IIndexedANNSvc.h"
#include "Kernel/IIndexedLumiSchemaSvc.h"
#include "Kernel/SynchronizedValue.h"
#include <Gaudi/MonitoringHub.h>
#include <map>
#include <nlohmann/json.hpp>
#include <string>

namespace ANNSvcBase_details {
  inline const std::array<std::string, 5> s_majors{"Hlt1SelectionID", "Hlt2SelectionID", "SpruceSelectionID", "InfoID",
                                                   "PackedObjectLocations"};

}

class ANNSvcBase : public extends<Service, IIndexedANNSvc, IIndexedLumiSchemaSvc> {

public:
  using extends::extends;
  ~ANNSvcBase() override;

  StatusCode initialize() override;

  map_t const&         i2s( unsigned int key, const Gaudi::StringKey& major ) const override final;
  inv_map_t const&     s2i( unsigned int key, const Gaudi::StringKey& major ) const override final;
  lumi_schema_t const& lumiCounters( unsigned key, unsigned version ) const override final;

  // implement methods to support use as Entity
  friend void to_json( nlohmann::json& json, ANNSvcBase const& that );

protected:
  virtual nlohmann::json generate_json( unsigned int key ) const                        = 0;
  virtual map_t          fetch( unsigned int key, const Gaudi::StringKey& major ) const = 0;
  virtual lumi_schema_t  fetch_lumi( unsigned int key, unsigned int version ) const     = 0;

private:
  void                    updateRegistration();
  Gaudi::Monitoring::Hub* m_monitoringHub = nullptr;

  Gaudi::Property<bool>                   m_allow_zero_as_key{this, "AllowZeroAsKey", false};
  Gaudi::Property<bool>                   m_register_with_monitoring_hub{this, "RegisterWithMonitoringHub", false,
                                                       [this]( const auto& ) { updateRegistration(); }};
  Gaudi::Property<std::set<unsigned int>> m_default_publish{this, "KeysToAlwaysPublish", {}};

  mutable std::array<LHCb::cxx::SynchronizedValue<std::map<unsigned int, map_t>>, ANNSvcBase_details::s_majors.size()>
      m_maps;
  mutable std::array<LHCb::cxx::SynchronizedValue<std::map<unsigned int, inv_map_t>>,
                     ANNSvcBase_details::s_majors.size()>
      m_inv_maps;

  mutable LHCb::cxx::SynchronizedValue<std::map<std::pair<unsigned, unsigned>, lumi_schema_t>> m_lumi;
};
