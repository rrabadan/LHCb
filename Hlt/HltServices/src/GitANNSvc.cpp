/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "ANNSvcBase.h"
#include "Gaudi/Accumulators.h"
#include <Gaudi/Parsers/Factory.h>
#include <GaudiKernel/IIncidentListener.h>
#include <GaudiKernel/IIncidentSvc.h>
#include <LHCb/FileSummaryRecordIncident.h>
#include <charconv>
#include <fmt/format.h>
#include <git2.h>
#include <nlohmann/json.hpp>

namespace {

  class ListenerRegistration {
    IIncidentSvc*      svc    = nullptr;
    IIncidentListener* parent = nullptr;

  public:
    template <typename Parent>
    ListenerRegistration( Parent* parent, const char* type ) : parent{parent} {
      svc = parent->template service<IIncidentSvc>( "IncidentSvc" );
      if ( !svc ) throw GaudiException{"Unable to register with IncidentSvc", __PRETTY_FUNCTION__, StatusCode::FAILURE};
      svc->addListener( parent, type );
    }
    ~ListenerRegistration() { svc->removeListener( parent ); }
  };

  unsigned int to_unsigned( std::string_view k ) {
    unsigned int i = 0;
    if ( auto [_, ec] = std::from_chars( k.begin(), k.end(), i ); ec != std::errc{} ) {
      throw GaudiException( fmt::format( "GitANNSvc: {} ** {} **", std::make_error_code( ec ).message(), k ),
                            __PRETTY_FUNCTION__, StatusCode::FAILURE );
    }
    return i;
  }

  std::string to_string( const git_blob* blob ) {
    assert( blob != nullptr );
    return std::string{reinterpret_cast<const char*>( git_blob_rawcontent( blob ) ), git_blob_rawsize( blob )};
  }

  // TODO: this is a copy from ParamFileSvc -- find a way to avoid the code duplication...
  class git {
    git_repository* m_repo = nullptr;

    struct object {
      git_object*     obj = nullptr;
                      operator bool() const { return obj != nullptr; }
                      operator git_object**() { return &obj; }
      const git_blob* as_blob() const {
        return git_object_type( obj ) == GIT_OBJECT_BLOB ? reinterpret_cast<const git_blob*>( obj ) : nullptr;
      }
      std::optional<std::string> as_string() const {
        auto* blob = as_blob();
        if ( !blob ) return std::nullopt; // can we set git_error_last to something usefull??
        return to_string( blob );
      }
      ~object() { git_object_free( obj ); }
    };

  public:
    git( std::string const& path ) {
      git_libgit2_init();
      if ( git_repository_open_bare( &m_repo, path.c_str() ) < 0 ) { m_repo = nullptr; };
    }

    operator bool() const { return m_repo != nullptr; }

    std::optional<std::string> read( std::string const& oidish ) const {
      assert( m_repo != nullptr );
      object obj;
      if ( git_revparse_single( obj, m_repo, oidish.c_str() ) < 0 ) return std::nullopt;
      return obj.as_string();
    }

    std::optional<std::string> read( std::string_view rev, std::string_view path ) const {
      assert( m_repo != nullptr );
      return read( fmt::format( "{}:{}", rev, path ) );
    }

    ~git() {
      git_repository_free( m_repo );
      git_libgit2_shutdown();
    }
  };

} // namespace

namespace Gaudi::Parsers {
  StatusCode parse( std::vector<std::pair<std::string, std::string>>& result, const std::string& input ) {
    return parse_( result, input );
  }
} // namespace Gaudi::Parsers

class GitANNSvc : public extends<ANNSvcBase, IIncidentListener> {

  Gaudi::Property<std::vector<std::pair<std::string, std::string>>> m_repo{
      this,
      "Repositories",
      {std::pair{"/cvmfs/lhcb-condb.cern.ch/git-conddb/file-content-metadata.git", "master"}},
      "list of (repository,tag) pairs"};
  std::optional<ListenerRegistration>                  m_listenerRegistration;
  Gaudi::Property<bool>                                m_readFSR{this, "CheckFSRForDecodingKeys", true};
  Gaudi::Property<std::map<unsigned int, std::string>> m_key2commit{this, "KeyMapping", {}};
  Gaudi::Property<std::map<unsigned int, std::string>> m_key2JSON{this, "Overrule", {}};
  Gaudi::Property<std::string> m_fmt{this, "RefFormatter", "{0}:ann/json/{1:.2}/{1}.json"}; // 0=tag, 1=key, 2=label
  Gaudi::Property<std::string> m_altfmt{this, "AltRefFormatter", "key-{1}:ann/json/{1:.2}/{1}.json"};

  Gaudi::Property<std::map<unsigned int, std::string>> m_lumi_key2JSON{this, "LumiOverrule", {}};
  Gaudi::Property<std::string>                         m_lumi_fmt{this, "LumiRefFormatter",
                                          "{0}:luminosity_counters/json/{1:.2}/{1}.json"}; // 0=tag, 1=key, 2=version
  Gaudi::Property<std::string>                         m_lumi_altfmt{this, "LumiAltRefFormatter",
                                             "key-{1}:luminosity_counters/json/{1:.2}/{1}.json"};

  std::map<unsigned int, nlohmann::json> m_fromFSR;

  std::optional<std::string> fetch_from_repo( std::string const& repo, std::string const& name ) const {
    if ( repo.empty() ) error() << "no repo specified..." << endmsg;
    auto g = git{repo};
    if ( !g ) throw GaudiException( "unable to open git repo: " + repo, __PRETTY_FUNCTION__, StatusCode::FAILURE );
    if ( msgLevel( MSG::DEBUG ) ) {
      debug() << "fetch_from_repo: trying to get " << name << " from " << repo << endmsg;
    }
    return g.read( name );
  }

  template <typename F>
  std::optional<std::string> fetch_from_repos( F&& fmt ) const {
    for ( const auto& [repo, tag] : m_repo.value() ) {
      if ( auto s = fetch_from_repo( repo, fmt( tag ) ); s ) return s;
    }
    return std::nullopt;
  }

  std::optional<nlohmann::json> fetch_ann_json( unsigned int key, const Gaudi::StringKey& major ) const {
    if ( auto io = m_key2JSON.find( key ); io != m_key2JSON.end() ) {
      warning() << "key " << fmt::format( "0x{:08x}", key ) << " has an explicitly configured overrule -- using that..."
                << endmsg;
      auto json = nlohmann::json::parse( io->second, nullptr, false );
      if ( json.is_discarded() ) return std::nullopt;
      return json;
    }

    if ( auto io = m_fromFSR.find( key ); io != m_fromFSR.end() ) {
      warning() << "key " << fmt::format( "0x{:08x}", key ) << " has been found in FSR -- using that..." << endmsg;
      return std::optional{io->second};
    }

    auto i  = m_key2commit.find( key );
    auto id = ( i != m_key2commit.end() ? i->second : fmt::format( "{:08x}", key ) );

    if ( msgLevel( MSG::DEBUG ) )
      debug() << "fetch_ann_json( " << key << " -> " << id << " ) from repo " << m_repo.value() << endmsg;

    auto s = fetch_from_repos(
        [&]( auto const& tag ) { return fmt::format( fmt::runtime( m_fmt.value() ), tag, id, major.str() ); } );
    if ( !s )
      s = fetch_from_repos(
          [&]( auto const& tag ) { return fmt::format( fmt::runtime( m_altfmt.value() ), tag, id, major.str() ); } );
    if ( !s ) return std::nullopt;
    auto json = nlohmann::json::parse( *s, nullptr, false );
    if ( json.is_discarded() ) return std::nullopt;
    return json;
  }

  template <typename Inserter>
  void fetch_( unsigned int key, const Gaudi::StringKey& major, Inserter inserter ) const {
    auto json = fetch_ann_json( key, major );
    if ( !json )
      throw GaudiException( fmt::format( "unable to obtain json for key {:08x} from repositories {}", key,
                                         Gaudi::Utils::toString( m_repo.value() ) ),
                            "GitANNSvc::fetch", StatusCode::FAILURE );
    auto j = json->find( major.str() );
    if ( j == json->end() ) return;
    // JSON dicts have string keys...
    for ( const auto& [k, v] : j->template get<std::map<std::string, std::string>>() ) inserter( k, v );
  }

  nlohmann::json generate_json( unsigned int key ) const override {
    auto js = fetch_ann_json( key, "" );
    if ( !js ) fatal() << " failure from fetch_ann_json(" << key << ", \"\") " << endmsg;
    return js.value();
  }

  map_t fetch( unsigned int key, const Gaudi::StringKey& major ) const override {
    if ( msgLevel( MSG::DEBUG ) ) debug() << "fetch(" << key << "," << major << ")" << endmsg;
    map_t m;
    fetch_( key, major, [&m]( std::string_view k, std::string v ) { m.insert( {to_unsigned( k ), std::move( v )} ); } );
    return m;
  }

  std::optional<nlohmann::json> fetch_lumi_json( unsigned int key ) const {
    if ( auto io = m_lumi_key2JSON.find( key ); io != m_lumi_key2JSON.end() ) {
      warning() << "lumi key " << fmt::format( "0x{:08x}", key )
                << " has an explicitly configured overrule -- using that..." << endmsg;
      auto json = nlohmann::json::parse( io->second, nullptr, false );
      if ( json.is_discarded() ) return std::nullopt;
      return json;
    }

    auto id = fmt::format( "{:08x}", key );

    if ( msgLevel( MSG::DEBUG ) )
      debug() << "fetch_lumi_json( " << key << " -> " << id << " ) from repo " << m_repo.value() << endmsg;

    auto s = fetch_from_repos(
        [&]( auto const& tag ) { return fmt::format( fmt::runtime( m_lumi_fmt.value() ), tag, id ); } );
    if ( !s )
      s = fetch_from_repos(
          [&]( auto const& tag ) { return fmt::format( fmt::runtime( m_lumi_altfmt.value() ), tag, id ); } );
    if ( !s ) return std::nullopt;
    auto json = nlohmann::json::parse( *s, nullptr, false );
    if ( json.is_discarded() ) return std::nullopt;
    return json;
  }

  lumi_schema_t fetch_lumi( unsigned int key, unsigned int version ) const override {
    if ( msgLevel( MSG::DEBUG ) ) debug() << "fetch lumi(" << key << "," << version << ")" << endmsg;
    auto json = fetch_lumi_json( key );
    if ( !json )
      throw GaudiException( fmt::format( "unable to obtain json for key {:08x} from repositories {}", key,
                                         Gaudi::Utils::toString( m_repo.value() ) ),
                            "GitANNSvc::fetch_lumi", StatusCode::FAILURE );
    auto vs = json->find( "version" );
    if ( vs == json->end() || vs->template get<unsigned>() != version ) {
      // must have correct version!!!)
      error() << "wrong version" << endmsg;
      return {};
    }
    auto sz = json->find( "size" );
    if ( sz == json->end() ) {
      error() << "no size" << endmsg;
      return {};
    }
    auto j = json->find( "counters" );
    if ( j == json->end() ) {
      error() << "no counters" << endmsg;
      return {};
    }
    lumi_schema_t m;
    m.size = sz->template get<unsigned>();
    std::transform( j->begin(), j->end(), std::back_inserter( m.counters ),
                    []( const nlohmann::json& k ) { return IIndexedLumiSchemaSvc::from_json( k ); } );
    return m;
  }

public:
  using extends::extends;

  StatusCode initialize() override {
    return extends::initialize().andThen( [&] {
      if ( m_readFSR ) m_listenerRegistration.emplace( this, LHCb::FileSummaryRecordIncident::Type );
    } );
  }

  StatusCode finalize() override {
    m_listenerRegistration.reset();
    return extends::finalize();
  }

  void handle( const Incident& inc ) override {
    // NOTE: This relies on FSRSink being present -- that is what generates a FileSummaryRecordIncident
    //       when it receives a CONNECTED_INPUT incident, and the connected input is a TFile which contains
    //       an FSR (i.e. object with the right magic name, which can be parsed as JSON)
    if ( inc.type() != LHCb::FileSummaryRecordIncident::Type ) return;

    if ( msgLevel( MSG::DEBUG ) ) debug() << "handling FileSummaryRecordIncident" << endmsg;
    const auto& fsr = dynamic_cast<const LHCb::FileSummaryRecordIncident&>( inc ).data;
    if ( msgLevel( MSG::DEBUG ) ) debug() << "got FSR: " << fsr << endmsg;
    if ( !fsr.contains( "ann" ) ) {
      if ( msgLevel( MSG::DEBUG ) ) debug() << "FSR does not have an 'ann' entry" << endmsg;
      return;
    }
    debug() << "got an FSR with decoding keys!" << endmsg;
    nlohmann::json ann = fsr["ann"];
    for ( const auto& [k, v] : ann.items() ) {
      debug() << "got " << k << " -> " << v << endmsg;
      auto key = to_unsigned( k );
      // check that 'k' is not explicitly overruled:
      if ( m_key2JSON.find( key ) != m_key2JSON.end() ) {
        warning() << "ignoring key " << k
                  << " from FSR because an explicit Overrule has been configured -- are you sure that Overrule is "
                     "really needed?"
                  << endmsg;
        continue;
      }
      // store decoding tables from the FSR so that `fetch_ann_json` will be able to use them when needed
      m_fromFSR.emplace( key, v );
    }
  }
};

DECLARE_COMPONENT( GitANNSvc )
