/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include <fmt/format.h>
#include <git2.h>
#include <iostream>
#include <nlohmann/json.hpp>
#include <regex>
#include <sstream>

#include <GaudiKernel/finally.h>
#include <HltServices/TCKUtils.h>

void LHCb::TCK::Git::check( int error ) {
  if ( error >= 0 ) return;
  const git_error* e = git_error_last();
  throw std::runtime_error{fmt::format( "Git error {}{}: {}", error, e->klass, e->message )};
}

void LHCb::TCK::Git::check_user( const std::string& repo_dir, git_repository* repo ) {
  git_config* config    = nullptr;
  const char* user_name = nullptr;
  check( git_config_open_default( &config ) );
  int found = git_config_get_string( &user_name, config, "user.name" );
  if ( found != 0 ) {
    std::string config_file = repo_dir + "/" + "config";
    check( git_config_add_file_ondisk( config, config_file.c_str(), GIT_CONFIG_LEVEL_LOCAL, repo, 0 ) );
    check( git_config_set_string( config, "user.name", "Hlt Oper" ) );
    check( git_config_set_string( config, "user.email", "hlt_oper@cern.ch" ) );
  }
  git_config_free( config );
};

std::tuple<git_repository*, git_signature*> LHCb::TCK::Git::create_git_repository( const std::string& repo_name ) {
  git_repository* repo = nullptr;
  git_signature*  sig  = nullptr;

  git_repository_init_options opts;
  git_repository_init_options_init( &opts, GIT_REPOSITORY_INIT_OPTIONS_VERSION );
  opts.flags = GIT_REPOSITORY_INIT_MKPATH;
  opts.flags |= GIT_REPOSITORY_INIT_BARE;
  opts.initial_head = "main";
  check( git_repository_init_ext( &repo, repo_name.c_str(), &opts ) ); // create bare repo

  check_user( repo_name, repo );

  // create initial commit
  check( git_signature_default( &sig, repo ) );

  git_oid tree_id{};
  {
    git_index* index{nullptr};
    check( git_repository_index( &index, repo ) );
    check( git_index_write_tree( &tree_id, index ) );
    git_index_free( index );
  }

  git_oid commit_id{};
  {
    git_tree* tree{nullptr};
    git_tree_lookup( &tree, repo, &tree_id );
    git_commit_create_v( &commit_id, repo, "HEAD", sig, sig, nullptr, "Initial commit", tree, 0 );
    git_tree_free( tree );
  }
  git_commit* commit = nullptr;
  git_commit_lookup( &commit, repo, &commit_id );

  return {repo, sig};
}

void LHCb::TCK::Git::add_json_manifest_entry( nlohmann::json& manifest, LHCb::TCK::Info const& info ) {
  manifest[info.digest] = nlohmann::json{{"TCK", info.tck},
                                         {"Release2Type", {{info.release, info.type}}},
                                         {"label", info.label},
                                         {"metadata", nlohmann::json::parse( info.metadata )}};
}

LHCb::TCK::Info LHCb::TCK::Git::tck_info( git_repository* repo, std::string tck ) {
  LHCb::TCK::Info info{};

  // Find the tag (=TCK) and get the label from the tag message
  git_oid tag_oid{};
  check( git_reference_name_to_id( &tag_oid, repo, ( "refs/tags/" + tck ).c_str() ) );

  // Find the target of the tag, which should be commit. From the
  // commit, get the commit message, which contains the release, hlt
  // type and md5sum
  git_object*           target  = nullptr;
  [[maybe_unused]] auto _target = finally( [&target]() { git_object_free( target ); } );
  check( git_object_lookup( &target, repo, &tag_oid, GIT_OBJECT_COMMIT ) );

  if ( git_object_type( target ) == GIT_OBJECT_COMMIT ) {
    auto*                 target_oid = git_object_id( target );
    git_commit*           commit     = nullptr;
    [[maybe_unused]] auto _commit    = finally( [&commit]() { git_commit_free( commit ); } );
    check( git_commit_lookup( &commit, repo, target_oid ) );

    // The TCK metadata is stored in the file "metadata" in JSON
    // format. We need to read that blob to get it.
    git_tree*             tree  = nullptr;
    [[maybe_unused]] auto _tree = finally( [&tree]() { git_tree_free( tree ); } );
    check( git_commit_tree( &tree, commit ) );
    git_tree_entry const* metadata_entry =
        git_tree_entry_byname( tree, "metadata.json" ); // we can "never" change this name!
    if ( metadata_entry == nullptr ) { throw std::runtime_error{"Failed to find metadata.json file"}; }
    auto [_, metadata]   = blob_to_json( repo, metadata_entry );
    int metadata_version = metadata.at( "version" );
    if ( metadata_version > 1 ) {
      throw std::runtime_error{fmt::format( "Metadata version {} not supported", metadata_version )};
    }

    auto release = metadata.at( "stack" ).at( "name" );

    info = {metadata.at( "digest" ).get<std::string>(),
            tck,
            release,
            metadata.at( "type" ),
            metadata.at( "label" ),
            metadata.dump()};
  }
  return info;
}

std::tuple<std::string, nlohmann::json> LHCb::TCK::Git::blob_to_json( git_repository* repo, git_tree_entry const* te ) {
  git_blob* blob = nullptr;
  check( git_blob_lookup( &blob, repo, git_tree_entry_id( te ) ) );
  std::istringstream in( std::string{(char const*)git_blob_rawcontent( blob ), git_blob_rawsize( blob )} );
  auto               blob_json = nlohmann::json::parse( in.str() );
  std::string        name      = git_tree_entry_name( te );
  git_blob_free( blob );
  return {name, blob_json};
}

nlohmann::json LHCb::TCK::Git::tree_to_json( git_repository* repo, git_tree const* tree ) {
  struct Payload {
    git_repository* repo = nullptr;
    nlohmann::json  json = {};
    int             operator()( char const* root, git_tree_entry const* te ) {
      if ( GIT_OBJ_BLOB == git_tree_entry_type( te ) ) {
        auto [name, blob_json] = blob_to_json( repo, te );
        std::string filename   = std::string{root} + name;
        json[filename]         = std::move( blob_json );
      }
      return 0;
    }
  };
  auto cb = Payload{repo};
  check( walk_tree( tree, &cb ) );
  return std::move( cb.json );
}

std::string LHCb::TCK::Git::extract_json( git_repository* repo, std::string const& tck ) {
  if ( tck.empty() ) { throw std::runtime_error{"Must specify a TCK when converting to JSON"}; }
  git_object* obj = nullptr;
  check( git_revparse_single( &obj, repo, ( tck + "^{tree}" ).c_str() ) );
  git_tree* tree = nullptr;
  check( git_tree_lookup( &tree, repo, git_object_id( obj ) ) );
  git_object_free( obj );
  auto sequence_json = tree_to_json( repo, tree );
  git_tree_free( tree );

  nlohmann::json manifest;
  auto           info = tck_info( repo, tck );
  add_json_manifest_entry( manifest, info );

  // Use assignment here to avoid known issue with brace initialization
  nlohmann::json tck_db = {{"manifest", std::move( manifest )}, {info.digest, sequence_json}};
  return tck_db.dump();
}

std::ostream& LHCb::TCK::Git::operator<<( std::ostream& s, git_oid const& oid ) {
  char buffer[GIT_OID_HEXSZ] = {0};
  git_oid_fmt( buffer, &oid );
  return s << "oid: " << std::string_view{buffer, sizeof( buffer )};
}
