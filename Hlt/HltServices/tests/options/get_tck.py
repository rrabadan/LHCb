from __future__ import print_function
###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################

from PyConf.Algorithms import HltRoutingBitsFilter
from PyConf.control_flow import CompositeNode
from PyConf.application import ApplicationOptions, configure_input, configure, make_odin, default_raw_banks, default_raw_event
import GaudiPython as GP

options = ApplicationOptions(_enabled=False)
options.set_input_and_conds_from_testfiledb("HltServices-close_cdb_file")
options.gaudipython_mode = True
config = configure_input(options)

from Configurables import CondDB
CondDB().LatestGlobalTagByDataTypes = ["2015"]

rawevent = default_raw_event("HltRoutingBits")
routingBits = default_raw_banks("HltRoutingBits")
odin = default_raw_banks("ODIN")
physFilter = HltRoutingBitsFilter(
    name="PhysFilter", RawBanks=routingBits, RequireMask=(0x0, 0x4, 0x0))

cf_node = CompositeNode(
    "Sequence",
    children=[rawevent, odin,
              make_odin(), routingBits, physFilter])
config.update(configure(options, cf_node))

appMgr = GP.AppMgr()
TES = appMgr.evtSvc()
tcks = set()

while True:
    appMgr.run(1)
    odin = TES['/Event/Decode_ODIN/ODIN']
    if not odin:
        break
    tcks.add(odin.triggerConfigurationKey())

# make sure TCKs are printed on the last line of output
appMgr.stop()
appMgr.finalize()
appMgr.terminate()

print(tcks)
