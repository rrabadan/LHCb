/*****************************************************************************\
* (c) Copyright 2000-2023 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

/** @class UTRawBankToUTDigitsAlg
 *
 *  Algorithm to decode UTRawBank to UTDigits (CPU decoder)
 *  Implementation file for class : UTRawBankToUTDigitsAlg
 *
 *  - \b RawEventLocation: Location of UTRawBank
 *  - \b OutputDigitData:  Location of output UTDigit
 *
 *  @author Wojciech Krupa (based on code by Xuhao Yuan, wokrupa@cern.ch)
 *  @author Xuhao Yuan (based on code by A Beiter and M Needham)
 *  @date   2021-04-19  / 2023-05-25 / 2024-04-17
 */

#include "Event/RawBank.h"
#include "Event/RawEvent.h"
#include "Event/UTDigit.h"
#include "Kernel/IUTReadoutTool.h"
#include "Kernel/UTDAQBoard.h"
#include "Kernel/UTDAQDefinitions.h"
#include "Kernel/UTDAQID.h"
#include "Kernel/UTDecoder.h"
#include "LHCbAlgs/Transformer.h"
#include "UTDAQ/UTADCWord.h"
#include "UTDAQ/UTDAQHelper.h"
#include "UTDAQ/UTHeaderWord.h"
#include <algorithm>
#include <bitset>
#include <iomanip>
#include <string>
#include <vector>

class UTRawBankToUTDigitsAlg : public LHCb::Algorithm::Transformer<LHCb::UTDigits( const LHCb::RawBank::View& )> {
public:
  /// Standard constructor
  UTRawBankToUTDigitsAlg( const std::string& name, ISvcLocator* pSvcLocator )
      : Transformer{name, pSvcLocator, {{"UTBank", {}}}, {"OutputDigitData", LHCb::UTDigitLocation::UTDigits}} {}

  using PositionMethod = UTDecoder<::UTDAQ::version::v5>::PositionMethod;
  LHCb::UTDigits operator()( const LHCb::RawBank::View& ) const override; ///< Algorithm execution

  // Counters for decoded banks
  mutable Gaudi::Accumulators::Counter<> m_validBanks{this, "# Valid UT banks"};
  mutable Gaudi::Accumulators::Counter<> m_validDigits{this, "# Valid UT digits"};
  mutable Gaudi::Accumulators::Counter<> m_invalidBanks{this, "# Invalid banks"};
  mutable Gaudi::Accumulators::Counter<> m_invalidDigits{this, "# Invalid digits - duplicate "};
  mutable Gaudi::Accumulators::Counter<> m_errors_channel{this, "# Invalid UTChannelIDs"};

private:
  // The readout tool for handling DAQID and ChannelID
  ToolHandle<IUTReadoutTool> readoutTool{this, "ReadoutTool", "UTReadoutTool"};
};

// Declaration of the Algorithm Factory
DECLARE_COMPONENT( UTRawBankToUTDigitsAlg )

using namespace LHCb;

//=============================================================================
// Main execution
//=============================================================================
LHCb::UTDigits UTRawBankToUTDigitsAlg::operator()( const LHCb::RawBank::View& rawevt ) const {
  // Retrieve  the decoded Digits
  LHCb::UTDigits digits;
  for ( const LHCb::RawBank* bank : rawevt ) {

    if ( bank->size() == 0 ) continue;

    if ( msgLevel( MSG::DEBUG ) ) {
      debug() << "------------------------- ------- UT RAW BANK  ------------------------------- -----------" << endmsg;
      debug() << "Bank: 0x" << std::uppercase << std::hex << bank->sourceID() << endmsg;
      debug() << "   Size: " << std::dec << bank->size() << "B" << endmsg;
      debug() << "   Type: " << bank->type() << endmsg;
      debug() << "   Version: " << bank->version() << endmsg;
      debug() << "|     |      HEADER                 | LANE5        | LANE4        | LANE3        | LANE2        "
                 "| LANE1        | LANE0       |"
              << endmsg;
      u_int8_t     iw2     = 0;
      unsigned int counter = 0;
      for ( const u_int8_t* w = bank->begin<u_int8_t>(); w != bank->end<u_int8_t>(); ++w ) {
        ++iw2;
        if ( ( iw2 - 1 ) % 32 == 0 ) {
          counter++;
          if ( counter % 34 == 0 ) {
            debug() << endmsg;
            debug() << "---------------------------------------------------------------------------------------------"
                       "-------------------------------"
                    << endmsg;
          }
          debug() << endmsg;
          debug() << "0x" << std::setfill( '0' ) << std::setw( 2 ) << std::hex << unsigned( iw2 ) << " ";
        }
        if ( ( iw2 - 1 ) % 4 == 0 ) debug() << " | ";
        debug() << std::uppercase << std::hex << std::setw( 2 ) << unsigned( *w ) << " ";
      }
      debug() << endmsg;
    }

    // Run decoder over UT Bank
    auto decode = [&]( auto decoder ) {
      if ( msgLevel( MSG::DEBUG ) ) {
        std::string bxid = std::bitset<12>( decoder.getBXID() ).to_string();
        std::string flag = std::bitset<4>( decoder.getflags() ).to_string();
        debug() << "Bxid: " << bxid << " " << std::dec << decoder.getBXID() << " Flag bits: " << flag << endmsg;
        debug() << "Digits in lanes from 0 to 5: ";
        for ( unsigned int lane = 0; lane < 6; lane++ ) { debug() << decoder.nClusters( lane ) << " "; }
        debug() << endmsg;
      }
      for ( const auto& aWord : decoder.posRange() ) {
        // Catch problems during construction of UTChannelID which may be caused by the timing violation
        try {
          UTDAQID                 daqid( ( UTDAQID::BoardID )( LHCb::UTDAQ::boardIDfromSourceID( bank->sourceID() ) ),
                         ( UTDAQID::LaneID )( aWord.channelID() / 512 ),
                         ( UTDAQID::ChannelID )( aWord.channelID() & 0x1ff ) );
          Detector::UT::ChannelID channelID = readoutTool->daqIDToChannelID( daqid );
          if ( msgLevel( MSG::DEBUG ) ) {
            debug() << "----------- DECODED DIGIT --------------------" << endmsg;
            std::string binary = std::bitset<16>( aWord.value() ).to_string();
            debug() << "Binary: " << binary << " Hex: " << std::hex << std::setfill( '0' ) << std::uppercase
                    << std::setw( 4 ) << aWord.value() << endmsg;
            debug() << "Channel from data bank: " << std::dec << aWord.channelID()
                    << " ADC from data bank: " << aWord.adc() << endmsg;
            debug() << daqid << endmsg;
            debug() << channelID << endmsg;
          }
          ++m_validDigits;
          // Catch problem during inserting to keyed container (also may be caused by the timing violation).
          try {
            digits.insert( new UTDigit( channelID, aWord.adc(), daqid.id() ), channelID );
          } catch ( const std::exception& ex ) {
            if ( msgLevel( MSG::WARNING ) ) {
              if ( msgLevel( MSG::WARNING ) ) warning() << ex.what() << endmsg;
            }
            ++m_invalidDigits;
          }
        } catch ( ... ) {
          if ( msgLevel( MSG::WARNING ) ) {
            warning() << "Warning!: Problem with channel ID: " << aWord.channelID() << endmsg;
          }
          ++m_errors_channel;
          continue;
        }
      }
    };
    if ( ::UTDAQ::version{bank->version()} == ::UTDAQ::version::v2 ) {
      decode( UTDecoder<::UTDAQ::version::v2>{*bank} );
      ++m_validBanks;
    } else if ( ::UTDAQ::version{bank->version()} == ::UTDAQ::version::v5 ) {
      decode( UTDecoder<::UTDAQ::version::v5>{*bank} );
      ++m_validBanks;
    } else {
      error() << "Wrong version of the RawBank" << endmsg;
      ++m_invalidBanks;
    }
  }
  return digits;
}

//=============================================================================
