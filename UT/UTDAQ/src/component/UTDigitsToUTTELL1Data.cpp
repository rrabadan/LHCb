/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "Event/RawBank.h"
#include "Event/UTDigit.h"
#include "Event/UTTELL1Data.h"
#include "Kernel/IUTReadoutTool.h"
#include "Kernel/LHCbConstants.h"
#include "Kernel/UTDAQDefinitions.h"
#include "Kernel/UTTell1Board.h"
#include "LHCbAlgs/Transformer.h"
#include "UTDet/DeUTDetector.h"
#include <algorithm>
#include <string>
#include <vector>

/** @class RawBankToUTProcFull RawBankToUTProcFull.h
 *
 *  Algorithm to create UTTELL1Data (type ProcFull) from RawEvent object
 *
 *  @author A. Beiter (based on code by M. Needham)
 *  @date   2018-09-04
 */

class UTDigitsToUTTELL1Data
    : public LHCb::Algorithm::Transformer<LHCb::UTTELL1Datas( const LHCb::UTDigits&, const DeUTDetector& ),
                                          LHCb::Algorithm::Traits::usesConditions<DeUTDetector>> {
public:
  UTDigitsToUTTELL1Data( const std::string& name, ISvcLocator* pSvcLocator );
  LHCb::UTTELL1Datas operator()( const LHCb::UTDigits&, const DeUTDetector& ) const override; ///< Algorithm execution
private:
  PublicToolHandle<IUTReadoutTool> m_readoutTool{this, "ReadoutTool", "UTReadoutTool"};
};

using namespace LHCb;

//-----------------------------------------------------------------------------
// Implementation file for class : RawBufferToUTClusterAlg
//
// 2004-01-07 : Matthew Needham
//-----------------------------------------------------------------------------

DECLARE_COMPONENT( UTDigitsToUTTELL1Data )

UTDigitsToUTTELL1Data::UTDigitsToUTTELL1Data( const std::string& name, ISvcLocator* pSvcLocator )
    : Transformer{name,
                  pSvcLocator,
                  {KeyValue{"inputLocation", UTDigitLocation::UTDigits},
                   KeyValue{"detectorLocation", DeUTDetLocation::location()}},
                  {"outputLocation", UTTELL1DataLocation::UTSubPeds}} {}

LHCb::UTTELL1Datas UTDigitsToUTTELL1Data::operator()( const LHCb::UTDigits& digits,
                                                      const DeUTDetector&   tracker ) const {

  if ( digits.size() != tracker.nStrip() ) {
    throw GaudiException{"Digit cont size does not equal number of detector strips", "UTDigitsToUTTELL1Data",
                         StatusCode::SUCCESS};
  }

  // make a new digits container
  UTTELL1Datas outCont;
  // make correct number of output containers
  for ( unsigned int i = 0; i < m_readoutTool->nBoard(); ++i ) {
    const UTTell1Board* board = m_readoutTool->findByOrder( i );
    UTTELL1Data::Data   dataVec;
    dataVec.resize( UTDAQ::noptlinks );
    for ( auto& dv : dataVec ) dv.resize( LHCbConstants::nStripsInBeetle );
    UTTELL1Data* tell1Data = new UTTELL1Data( dataVec );
    int          key       = (int)board->boardID().id();
    outCont.insert( tell1Data, key );
  } // nBoard

  // then its just one big loop
  for ( const auto& digit : digits ) {
    UTDAQ::chanPair    aPair   = m_readoutTool->offlineChanToDAQ( digit->channelID(), 0.0 );
    UTTELL1Data*       adcBank = outCont.object( aPair.first.id() );
    UTTELL1Data::Data& dataVec = adcBank->data();
    const unsigned int beetle  = aPair.second / LHCbConstants::nStripsInBeetle;
    const unsigned int strip   = aPair.second % LHCbConstants::nStripsInBeetle;
    dataVec[beetle][strip]     = int( digit->depositedCharge() );
  }
  return outCont;
}
