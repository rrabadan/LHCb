/*****************************************************************************\
* (c) Copyright 2000-2022 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once

#include "Event/RawBank.h"
#include "Kernel/STLExtensions.h"
#include "LHCbMath/SIMDWrapper.h"
#include "UTDet/DeUTDetector.h"
#include "UTDet/DeUTSector.h"
#include "UTInfo.h"

#include "Detector/UT/ChannelID.h"

#include <array>
#include <boost/container/small_vector.hpp>
#include <optional>

namespace LHCb::UTDAQ {

  constexpr const unsigned int nSides      = static_cast<unsigned int>( UTInfo::DetectorNumbers::Sides );
  constexpr const unsigned int nHalfLayers = static_cast<unsigned int>( UTInfo::DetectorNumbers::HalfLayers );
  constexpr const unsigned int nStaves     = static_cast<unsigned int>( UTInfo::DetectorNumbers::Staves );
  constexpr const unsigned int nFaces      = static_cast<unsigned int>( UTInfo::DetectorNumbers::Faces );
  constexpr const unsigned int nModules    = static_cast<unsigned int>( UTInfo::DetectorNumbers::Modules );
  constexpr const unsigned int nSubSectors = static_cast<unsigned int>( UTInfo::DetectorNumbers::SubSectors );

  constexpr const unsigned int nDoubleColumnsUTa = 32;
  constexpr const unsigned int nDoubleColumnsUTb = 36;

  using simd = SIMDWrapper::best::types;

  /**
   * counts number of UT clusters in the given raw banks
   * if count exceeds max, it gives up and returns no value
   */
  std::optional<unsigned int> nbUTClusters( LHCb::RawBank::View banks, unsigned int maxNbClusters );
  unsigned int                boardIDfromSourceID( unsigned int ut_sourceID );
  unsigned int                sourceIDfromBoardID( unsigned int ut_boardID );

  using LayerInfo = DeUTDetector::LayerGeom;

  constexpr const unsigned int max_sectors1 = static_cast<unsigned int>( UTInfo::SectorNumbers::NumSectorsUTa );
  constexpr const unsigned int max_sectors2 = static_cast<unsigned int>( UTInfo::SectorNumbers::NumSectorsUTb );

  struct LUT final {
    std::array<int, max_sectors1> Station1;
    std::array<int, max_sectors2> Station2;
  };

  struct GeomCache final {
    std::array<LayerInfo, static_cast<int>( UTInfo::DetectorNumbers::TotalLayers )> layers;
    LUT                                                                             sectorLUT;
    GeomCache( const DeUTDetector& );
    GeomCache() = default;

    /**
     * fills container of sector fullID with all sectors concerned by
     * a hit at given layer and coordinates and with given x tolerance
     */
    void findSectorsFullID( unsigned int layer, float x, float y, float xTol, float yTol,
                            boost::container::small_vector_base<int>& sectors ) const;

    /**
     * This addresses a bug in old implementations, where some sector numbers were swapped
     */
    static constexpr std::array<int, 12> m_sectorsToSwapStation1 = {402, 403, 404, 407, 408, 409,
                                                                    458, 459, 460, 463, 464, 465};
    static constexpr std::array<int, 12> m_sectorsToSwapStation2 = {458, 459, 460, 463, 464, 465,
                                                                    514, 515, 516, 519, 520, 521};
  };

  constexpr static unsigned int sectorFullID( unsigned int side, unsigned int halflayer, unsigned int stave,
                                              unsigned int face, unsigned int module, unsigned int subsector ) {
    assert( side < nSides );
    assert( halflayer < nHalfLayers );
    assert( stave < nStaves );
    assert( face < nFaces );
    assert( module < nModules );
    assert( subsector < nSubSectors );
    return ( ( ( ( ( ( side * nHalfLayers + halflayer ) * nStaves ) + stave ) * nFaces + face ) * nModules ) +
             module ) *
               nSubSectors +
           subsector;
  }

  constexpr static unsigned int sectorFullID( const LHCb::Detector::UT::ChannelID& channelID ) {
    return sectorFullID( channelID.side(), channelID.layer(), channelID.stave(), channelID.face(), channelID.module(),
                         channelID.sector() );
  }

  [[maybe_unused]] static simd::int_v sectorFullID( unsigned int halflayer, typename simd::int_v realSC,
                                                    typename simd::int_v realSR ) {
    [[maybe_unused]] const int nSidesI      = static_cast<int>( nSides );
    [[maybe_unused]] const int nHalfLayersI = static_cast<int>( nHalfLayers );
    [[maybe_unused]] const int nStavesI     = static_cast<int>( nStaves ) - 1 + static_cast<int>( halflayer / 2 );
    [[maybe_unused]] const int nFacesI      = static_cast<int>( nFaces );
    [[maybe_unused]] const int nModulesI    = static_cast<int>( nModules );
    [[maybe_unused]] const int nSubSectorsI = static_cast<int>( nSubSectors );

    assert( halflayer < nHalfLayers && "layer out of bound" );

    const simd::mask_v upper_t  = ( realSR > UTInfo::upper_row );
    const simd::mask_v side_t   = ( realSC > ( 2 * nStavesI - 1 ) );
    const simd::int_v  stave_t  = select( side_t, ( realSC - 2 * nStavesI ) >> 1, ( 2 * nStavesI - realSC - 1 ) >> 1 );
    const simd::int_v  module_t = upper_t + ( realSR >> 2 );
    unsigned int       oddmask  = 0x1L;
    const simd::int_v  face_t   = ( select( ( stave_t == UTInfo::Atypestave_id ) &&
                                             ( module_t == UTInfo::module_flipA || module_t == UTInfo::module_flipB ),
                                         realSR, realSR >> 1 ) +
                                 1 ) &
                               oddmask;
    const simd::int_v subsector_t =
        select( ( stave_t < UTInfo::Ctypestave_startid ) && realSR > UTInfo::nonAsensor_startid &&
                    realSR < UTInfo::nonAsensor_endid,
                ( realSC + upper_t + face_t + side_t ) & oddmask, 0 );
    return ( ( ( ( ( ( ( simd::int_v )(side_t)*nHalfLayersI + halflayer ) * nStaves ) + stave_t ) * nFacesI + face_t ) *
               nModulesI ) +
             module_t ) *
               nSubSectorsI +
           subsector_t;
  }

  constexpr static unsigned int sectorFullID( unsigned int halflayer, unsigned int realSC, unsigned int realSR ) {

    [[maybe_unused]] const unsigned int staves = nStaves - 1 + static_cast<unsigned int>( halflayer / 2 );

    assert( halflayer < nHalfLayers && "layer out of bound" );
    const bool         upper_t  = ( realSR > UTInfo::upper_row );
    const bool         side_t   = ( realSC > ( 2 * staves - 1 ) );
    const unsigned int stave_t  = ( side_t ) ? ( realSC - 2 * staves ) / 2 : ( 2 * staves - realSC - 1 ) / 2;
    const unsigned int module_t = upper_t + realSR / 4;
    const unsigned int face_t   = ( ( ( stave_t == UTInfo::Atypestave_id ) &&
                                            ( module_t == UTInfo::module_flipA || module_t == UTInfo::module_flipB )
                                        ? realSR
                                        : realSR / 2 ) +
                                  1 ) %
                                2;
    const unsigned int subsector_t = ( ( stave_t < UTInfo::Ctypestave_startid ) &&
                                       realSR > UTInfo::nonAsensor_startid && realSR < UTInfo::nonAsensor_endid )
                                         ? ( realSC + upper_t + face_t + side_t ) % 2
                                         : 0;

    return ( ( ( ( ( (unsigned int)(side_t)*nHalfLayers + halflayer ) * nStaves + stave_t ) * nFaces + face_t ) *
               nModules ) +
             module_t ) *
               nSubSectors +
           subsector_t;
  }

  //=============================================================================
  // -- Create lookup tables from sector positions (at compile time)
  //=============================================================================
  template <std::size_t nCols,
            std::size_t nRows = static_cast<unsigned int>( UTInfo::SectorNumbers::EffectiveSectorsPerColumn )>
  constexpr static std::array<int, nCols * nRows> populateSectorLUT( unsigned int layer ) {

    std::array<int, nCols * nRows> lut{};
    for ( std::size_t iCol = 0; iCol < nCols; iCol++ ) {
      for ( std::size_t iRow = 0; iRow < nRows; iRow++ ) {
        lut[iRow + nRows * iCol] = sectorFullID( layer, iCol, iRow );
      }
    }
    return lut;
  }

  //=============================================================================
  // -- find the sectors - SIMD version
  //=============================================================================
  template <typename F, typename I, typename = std::enable_if_t<std::is_same_v<I, typename LHCb::type_map<F>::int_t>>>
  inline __attribute__( ( always_inline ) ) auto findSectors( unsigned int layerIndex, F x, F y, F xTol, F yTol,
                                                              const LayerInfo& info, I& subcolmin2, I& subcolmax2,
                                                              I& subrowmin2, I& subrowmax2 ) {

    static_assert( std::is_same_v<F, typename LHCb::type_map<I>::float_t> );
    static_assert( std::is_same_v<I, typename LHCb::type_map<F>::int_t> );
    using M = typename LHCb::type_map<F>::mask_t;

    F localX = x - info.dxDy * y;
    // deal with sector overlaps and geometry imprecision
    xTol        = xTol + F{1.0f}; // mm
    F localXmin = localX - xTol;
    F localXmax = localX + xTol;

    I subcolmin{( localXmin * info.invHalfSectorXSize - 0.5f ) + 2.0f * ( info.nColsPerSide ) + 0.5f}; // faking
                                                                                                       // rounding
    I subcolmax{( localXmax * info.invHalfSectorXSize - 0.5f ) + 2.0f * ( info.nColsPerSide ) + 0.5f}; // faking
                                                                                                       // rounding

    M outOfAcceptanceCol = ( subcolmax < 0 ) || ( subcolmin > I{4 * info.nColsPerSide - 1} );
    // if( all(outOfAcceptanceCol) ) return;

    // deal with sector shifts in tilted layers and overlaps in regular ones
    yTol        = yTol + ( ( layerIndex == 1 || layerIndex == 2 ) ? F{8.0f} : F{1.0f} ); //  mm
    F localYmin = y - yTol;
    F localYmax = y + yTol;
    I subrowmin{( localYmin * info.invHalfSectorYSize - 0.5f ) + 2.0f * ( info.nRowsPerSide ) + 0.5f}; // faking
                                                                                                       // rounding
    I subrowmax{( localYmax * info.invHalfSectorYSize - 0.5f ) + 2.0f * ( info.nRowsPerSide ) + 0.5f}; // faking
                                                                                                       // rounding

    M outOfAcceptanceRow = ( subrowmax < 0 ) || ( subrowmin > I{4 * info.nRowsPerSide - 1} );
    // if( all(outOfAcceptanceRow) ) return;

    // on the acceptance limit
    subcolmin2 = max( subcolmin, I{0} );
    subcolmax2 = min( subcolmax, I{4 * info.nColsPerSide - 1} );

    subrowmin2 = max( subrowmin, I{0} );
    subrowmax2 = min( subrowmax, I{( 4 * info.nRowsPerSide ) - 1} );

    return !outOfAcceptanceCol && !outOfAcceptanceRow;
  }

} // namespace LHCb::UTDAQ
