/*****************************************************************************\
* (c) Copyright 2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once
#include <cassert>
#include <cstdint>
#include <iostream>

/** @class UTDAQID UTDAQID.h "UTDAQ/UTDAQID.h"
 *
 *  Class to describe a UT DAQ component with an ID
 *
 *  Combines Board ID (which half of which TELL40), LaneID (which lane inside raw data block), and channel number (ASIC
 * + strip number ) to describe a UT DAQ channel that can have hits
 *
 *  @author M. Rudolph
 *  @date   2020-02-13
 */

class UTDAQID final {
  enum class masks {
    channel = 0x000001ff,
    lane    = 0x00000e00,
    board   = 0x000ff000
  }; /// Enumeration for use in  bit packing

  template <masks m>
  [[nodiscard]] static constexpr unsigned int extract( unsigned int i ) {
    constexpr auto b =
        __builtin_ctz( static_cast<unsigned int>( m ) ); // FIXME: C++20 replace __builtin_ctz with std::countr_zero
    return ( i & static_cast<unsigned int>( m ) ) >> b;
  }

  template <masks m>
  [[nodiscard]] static constexpr unsigned int shift( unsigned int i ) {
    constexpr auto b =
        __builtin_ctz( static_cast<unsigned int>( m ) ); // FIXME: C++20 replace __builtin_ctz with std::countr_zero
    auto v = ( i << static_cast<unsigned int>( b ) );
    assert( extract<m>( v ) == i );
    return v;
  }

public:
  enum BoardID : uint32_t;
  enum LaneID : uint32_t;
  enum ChannelID : uint16_t;

  constexpr explicit UTDAQID( unsigned int id ) : m_id( id ) {}

  // full constructor with channel number in the lane
  constexpr UTDAQID( BoardID boardID, LaneID laneIdx, ChannelID channelNum )
      : m_id{shift<masks::board>( boardID ) | shift<masks::lane>( laneIdx ) | shift<masks::channel>( channelNum )} {}

  // constructor for a board ID, and a specific lane in that board's output
  constexpr UTDAQID( BoardID boardID, LaneID laneIdx ) : UTDAQID{boardID, laneIdx, ChannelID( 0 )} {}

  // constructor for a board ID, and a specific lane in that board's output
  constexpr UTDAQID( BoardID boardID ) : UTDAQID{boardID, LaneID( 0 ), ChannelID( 0 )} {}

  /// Default Constructor
  constexpr UTDAQID() = default;

  /// board
  [[nodiscard]] constexpr BoardID board() const { return BoardID( extract<masks::board>( m_id ) ); }

  /// lane
  [[nodiscard]] constexpr LaneID lane() const { return LaneID( extract<masks::lane>( m_id ) ); }

  /// channel
  [[nodiscard]] constexpr ChannelID channel() const { return ChannelID( extract<masks::channel>( m_id ) ); }

  /// comparison equality
  [[nodiscard]] constexpr friend bool operator==( const UTDAQID& lhs, const UTDAQID& rhs ) {
    return lhs.id() == rhs.id();
  }

  /// comparison <
  [[nodiscard]] constexpr friend bool operator<( const UTDAQID& lhs, const UTDAQID& rhs ) {
    return lhs.id() < rhs.id();
  }

  /// Retrieve UT DAQ ID as plain unsigned int
  [[nodiscard]] constexpr unsigned int id() const { return m_id; }

  /// Operator overloading for stringoutput
  friend std::ostream& operator<<( std::ostream& s, const UTDAQID& obj ) { return obj.fillStream( s ); }

  // Fill the ASCII output stream
  std::ostream& fillStream( std::ostream& s ) const;

  /** print method for python Not needed in C++ */
  [[nodiscard]] std::string toString() const;

  enum General { nullBoard = 0x000fffff };

private:
  unsigned int m_id = 0; ///< UTDAQID
};

#include <sstream>
#include <string>

inline std::string UTDAQID::toString() const {
  std::ostringstream o;
  fillStream( o );
  return o.str();
}

inline std::ostream& UTDAQID::fillStream( std::ostream& s ) const {
  return s << "{ UTDAQID: " << id() << " board: " << uint16_t( board() ) << " lane: " << uint16_t( lane() )
           << " channel : " << uint16_t( channel() ) << " }";
}
