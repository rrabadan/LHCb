/*****************************************************************************\
* (c) Copyright 2022 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once

#include <algorithm>
#include <map>
#include <optional>
#include <type_traits>
#include <vector>

namespace UT::Utils {
  class SourceId {
  public:
    SourceId() = default;
    SourceId( unsigned int v ) : value{v} {}
    operator unsigned int() const { return value; }

  private:
    unsigned int value = 0;
  };

  class BoardNum {
  public:
    BoardNum() = default;
    BoardNum( unsigned int v ) : value{v} {}
    operator unsigned int() const { return value; }

  private:
    unsigned int value = 0;
  };

  namespace details {
    template <typename K>
    struct traits {};
    template <>
    struct traits<SourceId> {
      using this_t  = SourceId;
      using other_t = BoardNum;
    };
    template <>
    struct traits<BoardNum> {
      using this_t  = BoardNum;
      using other_t = SourceId;
    };
  } // namespace details

  /// Helper class to map UT source id to board number and vice versa.
  class BoardMapping {
    std::map<SourceId, BoardNum> m_s2b;
    std::map<BoardNum, SourceId> m_b2s;

    template <typename K>
    auto& data() {
      if constexpr ( std::is_same_v<K, SourceId> ) {
        return m_s2b;
      } else {
        return m_b2s;
      }
    }

    template <typename K>
    const auto& data() const {
      if constexpr ( std::is_same_v<K, SourceId> ) {
        return m_s2b;
      } else {
        return m_b2s;
      }
    }

  public:
    BoardMapping()                      = default;
    BoardMapping( const BoardMapping& ) = default;
    BoardMapping( BoardMapping&& )      = default;
    BoardMapping& operator=( const BoardMapping& ) = default;
    BoardMapping& operator=( BoardMapping&& ) = default;

    template <typename K>
    std::optional<typename details::traits<K>::other_t> operator[]( K k ) const {
      if ( auto it = data<K>().find( k ); it != data<K>().end() ) { return it->second; }
      return {};
    }

    template <typename K>
    void erase( K k ) {
      if ( auto it = data<K>().find( k ); it != data<K>().end() ) {
        using V = typename details::traits<K>::other_t;
        data<V>().erase( it->second );
        data<K>().erase( it );
      }
    }

    template <typename K>
    void add( K k, typename details::traits<K>::other_t v ) {
      erase( k );
      data<K>()[k]                                    = v;
      data<typename details::traits<K>::other_t>()[v] = k;
    }

    void clear() {
      m_s2b.clear();
      m_b2s.clear();
    }

    template <typename K>
    std::vector<K> getAll() const {
      std::vector<K> ks;
      ks.reserve( data<K>().size() );
      std::transform( begin( data<K>() ), end( data<K>() ), std::back_inserter( ks ),
                      []( auto p ) { return p.first; } );
      return ks;
    }
  };
} // namespace UT::Utils
