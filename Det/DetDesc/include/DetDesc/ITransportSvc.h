/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once

#include "DetDesc/IGeometryInfo.h"
#include "DetDesc/Material.h"

#include "GaudiKernel/IService.h"
#include "GaudiKernel/Point3DTypes.h"
#include "GaudiKernel/Vector3DTypes.h"

#include <any>

/**
 *  Definition of abstract interface for Transport Service
 *
 *  @author Vanya Belyaev Ivan.Belyaev@itep.ru
 */
struct ITransportSvc : extend_interfaces<IService> {

  /**
   * Declaration of the unique interface identifier
   * ( interface id, major version, minor version )
   */
  DeclareInterfaceID( ITransportSvc, 5, 0 );

  /// Create an instance of the accelerator cache
  virtual std::any createCache() const = 0;

  /** Estimate the distance between 2 points in units
   *  of radiation length units
   *  This method is re-entrant and thus thread safe.
   *  @param point1 first  point
   *  @param point2 second point
   *  @param threshold threshold value
   *  @param geometry the geometry to be used
   *  @param geometryGuess a guess for navigation
   */
  virtual double distanceInRadUnits( const Gaudi::XYZPoint& point1, const Gaudi::XYZPoint& point2, std::any& accelCache,
                                     IGeometryInfo const& geometry, double threshold = 0,
                                     IGeometryInfo* geometryGuess = nullptr ) const = 0;

  /** general method ( returns the "full history" of the volume
   *  boundary intersections
   *  with different material properties between 2 points )
   *  This method is re-entrant and thus thread safe.
   *  @param point   initial point on the line
   *  @param vector  direction vector of the line
   *  @param tickMin minimal value of line paramater
   *  @param tickMax maximal value of line parameter
   *  @param accelCache Accelerator cache
   *  @param threshold threshold value
   *  @param geometry the geometry to be used
   *  @param geometryGuess a guess for navigation
   *  @returns container of intersections
   */
  virtual Intersections intersections( const Gaudi::XYZPoint& point, const Gaudi::XYZVector& vector,
                                       const double tickMin, const double tickMax, std::any& accelCache,
                                       IGeometryInfo const& geometry, double threshold = 0,
                                       IGeometryInfo* geometryGuess = nullptr ) const = 0;
};
