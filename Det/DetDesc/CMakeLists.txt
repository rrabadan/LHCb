###############################################################################
# (c) Copyright 2000-2022 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
#[=======================================================================[.rst:
Det/DetDesc
-----------
#]=======================================================================]

if(USE_DD4HEP)
  set(BUILD_TESTING false) # prevent headers build test
  gaudi_add_header_only_library(DetDescLib
        LINK
            LHCb::LbDD4hepLib)
else()
  gaudi_add_library(DetDescLib
    SOURCES
        src/Lib/3DTransformationFunctions.cpp
        src/Lib/AlignmentCondition.cpp
        src/Lib/CLID_HistoCondition.cpp
        src/Lib/Condition.cpp
        src/Lib/ConditionDerivation.cpp
        src/Lib/ConditionInfo.cpp
        src/Lib/DataStoreLoadAgent.cpp
        src/Lib/DetDesc.cpp
        src/Lib/DetectorElement.cpp
        src/Lib/DetectorElementException.cpp
        src/Lib/Element.cpp
        src/Lib/FastControlInfo.cpp
        src/Lib/GeoInfo.cpp
        src/Lib/GeometryInfoException.cpp
        src/Lib/GeometryInfoPlus.cpp
        src/Lib/GlobalToLocalDelta.cpp
        src/Lib/HistoCondition.cpp
        src/Lib/HistoParam.cpp
        src/Lib/ICondIOVResource.cpp
        src/Lib/IGeometryErrorSvc.cpp
        src/Lib/IntersectionErrors.cpp
        src/Lib/Isotope.cpp
        src/Lib/LAssembly.cpp
        src/Lib/LVolume.cpp
        src/Lib/LogVolBase.cpp
        src/Lib/LogVolumeException.cpp
        src/Lib/Material.cpp
        src/Lib/MaterialException.cpp
        src/Lib/Mixture.cpp
        src/Lib/PVolume.cpp
        src/Lib/PVolumeException.cpp
        src/Lib/Param.cpp
        src/Lib/ParamAsHisto.cpp
        src/Lib/ParamException.cpp
        src/Lib/ParamList.cpp
        src/Lib/ParamValidDataObject.cpp
        src/Lib/ReadOutInfo.cpp
        src/Lib/RunChangeIncident.cpp
        src/Lib/Services.cpp
        src/Lib/SimpleValidity.cpp
        src/Lib/SlowControlInfo.cpp
        src/Lib/SolidBase.cpp
        src/Lib/SolidBoolean.cpp
        src/Lib/SolidBox.cpp
        src/Lib/SolidChild.cpp
        src/Lib/SolidCons.cpp
        src/Lib/SolidException.cpp
        src/Lib/SolidIntersection.cpp
        src/Lib/SolidPolyHedronHelper.cpp
        src/Lib/SolidPolycone.cpp
        src/Lib/SolidSphere.cpp
        src/Lib/SolidSubtraction.cpp
        src/Lib/SolidTrap.cpp
        src/Lib/SolidTrd.cpp
        src/Lib/SolidTubs.cpp
        src/Lib/SolidUnion.cpp
        src/Lib/Surface.cpp
        src/Lib/TabulatedProperty.cpp
        src/Lib/ValidDataObject.cpp
    LINK
        PUBLIC
            Gaudi::GaudiKernel
            Gaudi::GaudiAlgLib
            Gaudi::GaudiUtilsLib
            LHCb::LHCbMathLib
            Rangev3::rangev3
            ROOT::GenVector
            ROOT::MathCore
            VDT::vdt
            yaml-cpp
            nlohmann_json::nlohmann_json
            Detector::DetectorLib
)
endif()
