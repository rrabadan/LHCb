/*****************************************************************************\
* (c) Copyright 2000-2022 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once

#include <Core/DeIOV.h>
#include <Core/Utils.h>
#include <DD4hep/AlignmentData.h>
#include <DD4hep/ConditionDerived.h>
#include <DD4hep/DetElement.h>
#include <DD4hep/Detector.h>
#include <DD4hep/GrammarUnparsed.h>
#include <DD4hep/detail/ConditionsInterna.h>
#include <DDCond/ConditionsSlice.h>
#include <GaudiKernel/IService.h>
#include <Kernel/STLExtensions.h>
#include <algorithm>
#include <any>
#include <boost/callable_traits.hpp>
#include <cstdint>
#include <fmt/core.h>
#include <memory>
#include <stdexcept>
#include <type_traits>

namespace LHCb::Det::LbDD4hep {

  namespace detail {

    /// find out whether a type is actually a dd4hep handle
    template <class T>
    static auto testHandle( int )
        -> std::integral_constant<bool,
                                  std::is_base_of_v<dd4hep::Handle<typename std::decay_t<T>::Object>, std::decay_t<T>>>;
    template <class>
    static auto testHandle( long ) -> std::false_type;
    template <class T>
    struct IsHandle : decltype( testHandle<T>( 0 ) ) {};
    template <class T>
    inline constexpr bool IsHandle_v = IsHandle<T>::value;

    template <typename Callable>
    inline constexpr auto arity_v = std::tuple_size_v<boost::callable_traits::args_t<Callable>>;

    template <typename Type>
    using ReturnType = std::conditional_t<IsHandle_v<Type> || std::is_same_v<std::decay_t<Type>, YAML::Node>,
                                          std::decay_t<Type>, Type&>;

    struct MissingConditionError : std::runtime_error {
      MissingConditionError() = delete;
      MissingConditionError( const std::string& message, std::size_t index, dd4hep::Condition::key_type key )
          : std::runtime_error( fmt::format( "{} (input {})", message, index ) )
          , m_original_message( message )
          , m_index( index )
          , m_key( key ) {}
      std::string                 m_original_message{};
      std::size_t                 m_index{};
      dd4hep::Condition::key_type m_key{};
    };

    inline void throwTypeMismatch( std::type_info const& FOUND, //
                                   std::type_info const& EXPECTED ) {
      const std::string fS = ( typeid( nullptr ) == FOUND ? "UNKNOWN" : System::typeinfoName( FOUND ) );
      const std::string eS = ( typeid( nullptr ) == EXPECTED ? "UNKNOWN" : System::typeinfoName( EXPECTED ) );
      throw std::runtime_error{"Type mis-match, Found='" + fS + "' Expected='" + eS + "'"};
    }

    // wrapper around a non copyable class adding copy constructor and
    // operator=, both only throwing an exception
    // This allows to then store the type into an std::any while still
    // failing in case the object is copied, although only at run time
    template <typename Content>
    class CopyWrapper {
    private:
      Content m_;

    public:
      CopyWrapper( Content&& c ) : m_( std::move( c ) ) {}
      // allow automatic, implicit conversion to const reference to 'Content', i.e. all that `get` needs
      operator const Content&() const { return m_; }
      operator const Content &&() = delete; // make sure one cannot accidentally get a dangling reference...

      CopyWrapper( CopyWrapper&& ) = default;
      CopyWrapper& operator=( CopyWrapper&& ) = default;

      CopyWrapper( const CopyWrapper& c ) : m_{std::move( const_cast<Content&>( c.m_ ) )} {
        throw GaudiException( "CopyWrapper", "Copy constructor of CopyWrapper called, that should never happen",
                              StatusCode::FAILURE );
      }
      CopyWrapper& operator=( const CopyWrapper& ) {
        throw GaudiException( "CopyWrapper", "operator= of CopyWrapper called, that should never happen",
                              StatusCode::FAILURE );
      }
    };

    /// Type trait evaluating to true/false depending on if the type is has 'over alignment'
    /// constraints (i.e. requires alignment > 16, e.g. for AVX2, AVX512... build)
    template <typename T>
    using IsOverAligned =
        std::conditional_t<( std::alignment_of<T>::value > 16 ), std::bool_constant<true>, std::bool_constant<false>>;

    template <typename T>
    // Always use std::any when passing drived conditions to dd4hep
    // using PassAsAny = std::bool_constant<true>;
    // Never use std::any when passing drived conditions to dd4hep
    // using PassAsAny = std::bool_constant<false>;
    // Only use std::any with types with no default constructor (required to directly pass type to dd4hep storage)
    // or memory over-alignment requirements (which dd4hep has a bug against as it does not respect this requirement).
    using PassAsAny = std::disjunction<IsOverAligned<T>, std::negation<std::is_default_constructible<T>>>;

    template <typename T>
    using IsBoxed = std::conjunction<std::negation<std::is_copy_constructible<T>>, std::negation<std::is_abstract<T>>>;

    template <typename T>
    using BoxedType = std::conditional_t<IsBoxed<T>::value, CopyWrapper<T>, T>;

    template <typename Input>
    ReturnType<Input> fetch_1( dd4hep::cond::ConditionUpdateContext const& context, std::size_t n ) {
      try {
        auto cond = context.condition( context.key( n ), true );
        if constexpr ( IsHandle_v<Input> ) {
          return Input( cond );
        } else {
          using DT = std::decay_t<Input>;
          if constexpr ( std::is_same_v<DT, YAML::Node> ) {
            return LHCb::Detector::utils::json2yaml( cond.get<nlohmann::json>() );
          } else if constexpr ( !PassAsAny<DT>::value ) {
            return cond.get<DT>();
          } else {
            const DT* p = nullptr;
            // First see if type is std::any. If it is we can then apply our own type checks
            // as we cannot rely on those in dd4hep itself giving anything like good diagnostics.
            // Unfortunately due to how dd4hep handles this no other way than just trying a
            // cast to std::any and catching the bad_cast exception if that fails.
            try {
              if constexpr ( IsBoxed<DT>::value ) {
                const auto* cp = std::any_cast<CopyWrapper<DT>>( &cond.get<std::any>() );
                if ( cp ) { p = &static_cast<const DT&>( *cp ); }
              } else {
                p = std::any_cast<DT>( &cond.get<std::any>() );
              }
              if ( !p ) { throwTypeMismatch( cond.get<std::any>().type(), typeid( DT ) ); }
            } catch ( const std::bad_cast& ) {
              // dd4hep 'cast' to std::any failed. Fallback to directly accessing type
              // i.e. we just assume that is what dd4hep has stored.
              // If not, and we get another bad_cast exception, throw as meaningful exception as we can.
              try {
                p = &cond.get<DT>();
              } catch ( const std::bad_cast& ) { throwTypeMismatch( typeid( nullptr ), typeid( DT ) ); }
            }
            assert( p );
            return *p;
          }
        }
      } catch ( std::runtime_error const& e ) { throw MissingConditionError( e.what(), n, context.key( n ) ); }
    }

    template <typename TypeList, std::size_t... I>
    using ReturnTypes = std::tuple<ReturnType<std::tuple_element_t<I, TypeList>>...>;

    template <typename TypeList, std::size_t... I>
    auto fetch_n( dd4hep::cond::ConditionUpdateContext const& ctx, std::index_sequence<I...> ) {
      return ReturnTypes<TypeList, I...>{fetch_1<std::tuple_element_t<I, TypeList>>( ctx, I )...};
    }

    template <typename Transform, std::ptrdiff_t N = detail::arity_v<Transform>>
    auto fetch_inputs_for( dd4hep::cond::ConditionUpdateContext const& ctx ) {
      using InputTypes = boost::callable_traits::args_t<Transform>;
      if ( ctx.dependency->dependencies.size() != N )
        throw GaudiException( "Mismatch between the number of inputs and the number of arguments of the callback",
                              "fetch_inputs_for", StatusCode::FAILURE );
      return fetch_n<InputTypes>( ctx, std::make_index_sequence<N>{} );
    }
  } // namespace detail

  /**
   * small functor wrapping a lambda and suitable for creating
   * a DD4hep derived condition
   */
  class GenericConditionUpdateCall : public dd4hep::cond::ConditionUpdateCall {
  public:
    /// Type for a user provided callback function.
    /// The first argument is the ConditionKey of the target and is used to be
    /// able to reuse a transformation function that behaves differently depending
    /// on the requested output, The ConditionUpdateContext will be filled with the
    /// input conditions, and the last argument is the Condition instance to update.
    using ConditionCallbackFunction =
        std::function<void( dd4hep::cond::ConditionUpdateContext const&, dd4hep::Condition& )>;
    // constructor, taking a transform and creating the 2 callables used for operator() and resolve
    // the constructor is templated and the callables' types are not, allowing a generic interface
    template <typename Transform>
    GenericConditionUpdateCall( Transform f, LHCb::cxx::source_location src_loc ) : m_src_loc( std::move( src_loc ) ) {
      m_fillCondition = [=, f = std::move( f )]( dd4hep::detail::ConditionObject*            condition,
                                                 dd4hep::cond::ConditionUpdateContext const& ctx ) {
        auto ins            = std::apply( f, detail::fetch_inputs_for<Transform>( ctx ) );
        using ConditionType = decltype( ins );
        if constexpr ( detail::PassAsAny<ConditionType>::value ) {
          // Pass as std::any to dd4hep, to allow for better type checking on our side when reading back.
          // Requires type to to copy constructible hence the boxed type.
          if constexpr ( detail::IsBoxed<ConditionType>::value ) {
            condition->data.bind<std::any>() = detail::BoxedType<ConditionType>( std::move( ins ) );
          } else {
            condition->data.bind<std::any>() = std::move( ins );
          }
        } else {
          condition->data.bind<ConditionType>() = std::move( ins );
        }
      };
    }
    dd4hep::Condition operator()( const dd4hep::ConditionKey&           key,
                                  dd4hep::cond::ConditionUpdateContext& context ) override {
      auto cond  = std::make_unique<dd4hep::detail::ConditionObject>();
      cond->hash = key;
      try {
        m_fillCondition( cond.get(), context );
      } catch ( detail::MissingConditionError& e ) {
        throw detail::MissingConditionError(
            fmt::format( "{} @{}:{}", e.m_original_message, m_src_loc.file_name(), m_src_loc.line() ), e.m_index,
            e.m_key );
      }
      return cond.release();
    }
    void resolve( dd4hep::Condition, dd4hep::cond::ConditionUpdateContext& context ) override {
      // This is needed to ensure that the IOV of the derived condition is computed correctly
      auto& deps = context.dependency->dependencies;
      std::for_each( begin( deps ), end( deps ), [&context]( auto key ) { context.condition( key ); } );
    }

  private:
    std::function<void( dd4hep::detail::ConditionObject*, dd4hep::cond::ConditionUpdateContext& )> m_fillCondition;
    LHCb::cxx::source_location                                                                     m_src_loc;
  };

  /**
   * small functor wrapping a function pointer and suitable for creating
   * a DD4hep derived condition.
   * Used for shared conditions as the id() function can be used to check
   * whether 2 FPointerConditionUpdateCall actually invoke the same function.
   */
  class FPointerConditionUpdateCall : public dd4hep::cond::ConditionUpdateCall {
  public:
    /// Type for a user provided callback as function pointer.

    template <typename Output, typename... Input>
    FPointerConditionUpdateCall( Output ( *f )( Input... ), LHCb::cxx::source_location src_loc )
        : m_f{reinterpret_cast<std::uintptr_t>( f )}, m_src_loc( std::move( src_loc ) ) {

      m_fillCondition = [f]( dd4hep::detail::ConditionObject*            condition,
                             dd4hep::cond::ConditionUpdateContext const& ctx ) {
        if constexpr ( detail::PassAsAny<Output>::value ) {
          // Pass as std::any to dd4hep, to allow for better type checking on our side when reading back.
          // Requires type to to copy constructible hence the boxed type.
          condition->data.bind<std::any>() = detail::BoxedType<Output>(
              std::move( std::apply( f, detail::fetch_inputs_for<Output ( * )( Input... )>( ctx ) ) ) );
        } else {
          condition->data.bind<Output>() = std::apply( f, detail::fetch_inputs_for<Output ( * )( Input... )>( ctx ) );
        }
      };
    }
    dd4hep::Condition operator()( const dd4hep::ConditionKey&           key,
                                  dd4hep::cond::ConditionUpdateContext& context ) override {

      auto cond  = std::make_unique<dd4hep::detail::ConditionObject>();
      cond->hash = key;
      try {
        m_fillCondition( cond.get(), context );
      } catch ( detail::MissingConditionError& e ) {
        throw detail::MissingConditionError(
            fmt::format( "{} @{}:{}", e.m_original_message, m_src_loc.file_name(), m_src_loc.line() ), e.m_index,
            e.m_key );
      }
      return cond.release();
    }

    void resolve( dd4hep::Condition, dd4hep::cond::ConditionUpdateContext& context ) override {
      // This is needed to ensure that the IOV of the derived condition is computed correctly
      auto& deps = context.dependency->dependencies;
      std::for_each( begin( deps ), end( deps ), [&context]( auto key ) { context.condition( key ); } );
    }

    /* return the value of the function pointer, as int.
    used to check whether the ConditiuonCall use the same function pointer */
    std::uintptr_t id() const { return m_f; }

  private:
    std::function<void( dd4hep::detail::ConditionObject*, dd4hep::cond::ConditionUpdateContext& )> m_fillCondition;
    std::uintptr_t                                                                                 m_f;
    LHCb::cxx::source_location                                                                     m_src_loc;
  };

  /** @class IDD4hepSvc IDD4hepSvc.h DetDesc/IDD4hepSvc.h
   *
   *  Definition of abstract interface for the service providing the DD4hep Geometry
   *
   */
  struct IDD4hepSvc : extend_interfaces<IService> {

    using DD4HepSlicePtr                       = std::shared_ptr<dd4hep::cond::ConditionsSlice>;
    using DD4HepDerivationFunc                 = std::shared_ptr<dd4hep::cond::ConditionUpdateCall>;
    static constexpr auto DefaultSliceLocation = "/Event/IOVLock";

    /** Declaration of the unique interface identifier
     *  ( interface id, major version, minor version)
     */
    DeclareInterfaceID( IDD4hepSvc, 1, 1 );

    /**
     * get the Condition slice from the cache
     */
    virtual DD4HepSlicePtr get_slice( size_t iov ) = 0;

    /**
     * drop a Condition slice from the cache
     */
    virtual void drop_slice( size_t iov ) = 0;

    /**
     * drop all condition slices from the cache
     */
    virtual void clear_slice_cache() = 0;

    /**
     * Add a derived condition to DD4hep
     * @arg inputs the keys for the conditions the derived one depends on.
     *      These conditions will be given as input to the func functor
     * @arg output the key for the new derived condition
     * @arg func a functor able to create/update the derived condition
     *      out of the inputs
     */
    virtual bool add( LHCb::span<const std::string> inputs, const std::string& output, DD4HepDerivationFunc& func ) = 0;

    /**
     * Add a shared derived condition to DD4hep
     * Unlike "add", shared derivations can be defined my multiple algorithms provided all
     * parameters are identical.
     * @arg inputs the keys for the conditions the derived one depends on.
     *      These conditions will be given as input to the func functor
     * @arg output the key for the new derived condition
     * @arg func a functor able to create/update the derived condition
     *      out of the inputs
     */
    virtual bool addShared( LHCb::span<const std::string> inputs, const std::string& output,
                            DD4HepDerivationFunc& func ) = 0;

    /**
     * Helper to update in the conditions overlay the alignment condition of a given DetElement (in LHCb units).
     */
    virtual void update_alignment( const dd4hep::DetElement& de, const dd4hep::Delta& delta ) = 0;

    /**
     * Helper to update in the conditions overlay the alignment condition of a given DetIOV (in LHCb units).
     */
    void update_alignment( const LHCb::Detector::DeIOV& de, const dd4hep::Delta& delta ) {
      update_alignment( de.detector(), delta );
    }

    /**
     * Write alignment conditions from the overlay to files in the directory specified as `path`.
     */
    virtual void write_alignments( std::string_view path ) const = 0;

    /**
     * Load alignment conditions into the overlay from files in the directory specified as `path`.
     */
    virtual void load_alignments( std::string_view path ) = 0;
  };

} // namespace LHCb::Det::LbDD4hep
