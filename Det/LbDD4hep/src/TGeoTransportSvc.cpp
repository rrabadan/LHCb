/*****************************************************************************\
* (c) Copyright 2019-2022 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#include "TGeoTransportSvc.h"
#include <Core/Units.h>
#include <DD4hep/Detector.h>
#include <DetDesc/IDetectorElement.h>
#include <DetDesc/IGeometryInfo.h>
#include <DetDesc/Services.h>
#include <GaudiKernel/IMessageSvc.h>
#include <GaudiKernel/SmartDataPtr.h>
#include <TVector3.h>
#include <boost/format.hpp>

StatusCode TGeoTransportSvc::initialize() {
  /// initialise the base class
  StatusCode statusCode = Service::initialize();
  if ( statusCode.isFailure() ) return statusCode; // Propagate the error
  return StatusCode::SUCCESS;
}

StatusCode TGeoTransportSvc::finalize() { return StatusCode::SUCCESS; }

double TGeoTransportSvc::distanceInRadUnits( const Gaudi::XYZPoint& point1, const Gaudi::XYZPoint& point2,
                                             std::any& accelCache, IGeometryInfo const& geometry, double threshold,
                                             IGeometryInfo* geometryGuess ) const {
  // check for the  distance
  if ( point1 == point2 ) { return 0; }

  // retrieve the history
  const Gaudi::XYZVector Vector( point2 - point1 );

  Intersections local_intersects =
      intersections( point1, Vector, 0.0, 1.0, accelCache, geometry, threshold, geometryGuess );

  struct AccumulateIntersections final {
    inline double operator()( double length, const Intersection& intersection ) const {
      const LHCb::Detector::Material mat = intersection.second;
      const Interval&                Int = intersection.first;
      return length + ( ( !mat ) ? 0 : ( Int.second - Int.first ) / mat.radLength() );
    }
  };

  //  radiation length in tick units
  const auto RadLength =
      std::accumulate( local_intersects.begin(), local_intersects.end(), 0.0, AccumulateIntersections() );

  // scale
  const auto TickLength = std::sqrt( Vector.mag2() );

  return RadLength * TickLength;
}

Intersections TGeoTransportSvc::intersections( const Gaudi::XYZPoint& point, const Gaudi::XYZVector& vect,
                                               const double tickMin, const double tickMax, std::any& /* accelCache */,
                                               IGeometryInfo const& /* geometry */, double /* threshold */,
                                               IGeometryInfo* /* guessGeometry */ ) const {
  /// check the input parameters of the line
  if ( tickMin >= tickMax && vect.mag2() <= 0 ) { return {}; }

  Gaudi::XYZPoint start = point + vect * tickMin;
  Gaudi::XYZPoint end   = point + vect * tickMax;

  // Getting the intersections using the ROOT Geometry
  // CAREFUL: The unit is the cm
  std::vector<MatIntervalCmp> tgeo_Intercepts;
  getTGeoIntersections( LHCb::Detector::detail::toDD4hepUnits( start ), LHCb::Detector::detail::toDD4hepUnits( end ),
                        tgeo_Intercepts, gGeoManager );

  Intersections intercepts;
  for ( auto mi : tgeo_Intercepts ) {
    Intersection tmp;
    tmp.first.first                    = mi.start;
    tmp.first.second                   = mi.end;
    const LHCb::Detector::Material mat = findMaterial( mi.materialName );
    tmp.second                         = mat;
    intercepts.push_back( tmp );
  }

  return intercepts;
}

/**
 * We only have as name the last part of the path so we search through the
 * existing materials
 */
const LHCb::Detector::Material TGeoTransportSvc::findMaterial( std::string shortname ) const {
  auto& detector = dd4hep::Detector::getInstance();
  return detector.material( shortname );
}

void TGeoTransportSvc::getTGeoIntersections( Gaudi::XYZPoint start, Gaudi::XYZPoint end,
                                             std::vector<MatIntervalCmp>& Intercepts, TGeoManager* m ) const {
  TVector3 v( end.X() - start.X(), end.Y() - start.Y(), end.Z() - start.Z() );

  Double_t xp = v.X() / v.Mag();
  Double_t yp = v.Y() / v.Mag();
  Double_t zp = v.Z() / v.Mag();

  Double_t    snext;
  TString     path;
  Double_t    pt[3];
  Double_t    loc[3];
  Double_t    epsil    = 1.E-2;
  Int_t       ismall   = 0;
  Int_t       nbound   = 0;
  Double_t    length   = 0.;
  TGeoMedium* med      = nullptr;
  TGeoShape*  shape    = nullptr;
  TGeoNode*   lastnode = nullptr;

  m->InitTrack( start.X(), start.Y(), start.Z(), xp, yp, zp );
  if ( this->msgLevel( MSG::DEBUG ) ) {
    debug() << "Track: " << start.X() << ", " << start.Y() << ", " << start.Z() << ", " << xp << ", " << yp << ", "
            << zp << endmsg;
  }
  TGeoNode* nextnode = m->GetCurrentNode();
  while ( nextnode ) {
    med = 0;
    if ( nextnode )
      med = nextnode->GetVolume()->GetMedium();
    else
      return;
    shape    = nextnode->GetVolume()->GetShape();
    lastnode = nextnode;
    nextnode = m->FindNextBoundaryAndStep();
    snext    = m->GetStep();
    if ( snext < 1.e-8 ) {
      ismall++;
      if ( ( ismall < 3 ) && ( lastnode != nextnode ) ) {
        // First try to cross a very thin layer
        length += snext;
        nextnode = m->FindNextBoundaryAndStep();
        snext    = m->GetStep();
        if ( snext < 1.E-8 ) continue;
        // We managed to cross the layer
        ismall = 0;
      } else {
        // Relocate point
        if ( ismall > 3 ) {
          error() << "ERROR: Small steps in: " << m->GetPath() << " shape=" << shape->ClassName() << endmsg;
          return;
        }
        memcpy( pt, m->GetCurrentPoint(), 3 * sizeof( Double_t ) );
        const Double_t* dir = m->GetCurrentDirection();
        for ( Int_t i = 0; i < 3; i++ ) pt[i] += epsil * dir[i];
        snext = epsil;
        length += snext;
        m->CdTop();
        nextnode = m->FindNode( pt[0], pt[1], pt[2] );
        if ( m->IsOutside() ) return;
        TGeoMatrix* mat = m->GetCurrentMatrix();
        mat->MasterToLocal( pt, loc );
        if ( !m->GetCurrentVolume()->Contains( loc ) ) {
          m->CdUp();
          nextnode = m->GetCurrentNode();
        }
        continue;
      }
    } else {
      ismall = 0;
    }
    nbound++;
    length += snext;
    if ( med ) {
      if ( this->msgLevel( MSG::DEBUG ) ) {
        debug() << "STEP #" << nbound << " " << path.Data() << endmsg;
        debug() << " step=" << snext << " length=" << length
                << " rad=" << med->GetMaterial()->GetDensity() * snext / med->GetMaterial()->GetRadLen()
                << med->GetName() << endmsg;
      }
      // Now filling up the data
      MatIntervalCmp cur;
      cur.start        = ( length - snext ) / v.Mag();
      cur.end          = ( length ) / v.Mag();
      cur.radlength    = med->GetMaterial()->GetDensity() * snext / med->GetMaterial()->GetRadLen();
      cur.material     = med->GetMaterial();
      cur.materialName = med->GetName();
      cur.locations.push_back( m->GetPath() );

      // Checking whether we need to agregate the entries in the vector
      if ( Intercepts.size() > 0 ) {
        MatIntervalCmp& prev = Intercepts.back();
        if ( prev.materialName == cur.materialName ) {
          prev.end = cur.end;
          prev.radlength += cur.radlength;
          prev.locations.push_back( cur.locations.front() );
        } else {
          // Different material, no need to agregate
          Intercepts.push_back( cur );
        }
      } else {
        // first entry in vector anyway
        Intercepts.push_back( cur );
      }
    }
  }
}

DECLARE_COMPONENT( TGeoTransportSvc )
