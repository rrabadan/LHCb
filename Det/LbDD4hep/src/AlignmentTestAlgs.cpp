/*****************************************************************************\
* (c) Copyright 2022 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include <Detector/VP/DeVP.h>
#include <GaudiKernel/SystemOfUnits.h>
#include <LHCbAlgs/Consumer.h>
#include <LbDD4hep/IDD4hepSvc.h>
#include <string>
#include <type_traits>

namespace LHCb::Det::LbDD4hep::Tests::Alignment {
  using LHCb::Algorithm::Consumer;
  struct UpdateTest : Consumer<void( const LHCb::Detector::DeVP& ), usesConditions<LHCb::Detector::DeVP>> {
    UpdateTest( const std::string& name, ISvcLocator* svcLoc )
        : Consumer( name, svcLoc, {KeyValue( "VP", "/world/BeforeMagnetRegion/VP:DetElement-Info-IOV" )} ) {}

    StatusCode initialize() override {
      return Consumer::initialize().andThen( [&]() {
        m_dd4hep = service( "LHCb::Det::LbDD4hep::DD4hepSvc" );
        return m_dd4hep ? StatusCode::SUCCESS : StatusCode::FAILURE;
      } );
    }

    void operator()( const LHCb::Detector::DeVP& det ) const override {
      auto align = det.delta(); // get the alignment data in LHCb units
      info() << "current alignment -> " << align << endmsg;
      // change the alignment
      align.translation.SetX( -2 * align.translation.X() + 0.3 * Gaudi::Units::cm );
      align.translation.SetY( align.translation.Y() + 1 * Gaudi::Units::mm );
      // update the overlay
      m_dd4hep->update_alignment( det.detector(), align );
      // write the overlay to disc
      m_dd4hep->write_alignments( m_overlayRoot );
      {
        // mess with the overlay to confirm that the following refresh actually works
        m_dd4hep->update_alignment( det.detector(), dd4hep::Delta{} );
      }
      // refresh the overlay from files
      m_dd4hep->load_alignments( m_overlayRoot );
      // force a reload of the conditions at the next possible occasion
      m_dd4hep->clear_slice_cache();
    }

    SmartIF<IDD4hepSvc> m_dd4hep;

    Gaudi::Property<std::string> m_overlayRoot{this, "OverlayRoot", "./OverlayRoot",
                                               "directory where to write the overlay content "};
  };
  DECLARE_COMPONENT( UpdateTest )
} // namespace LHCb::Det::LbDD4hep::Tests::Alignment
