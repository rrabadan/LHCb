/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#include "Kernel/ICondDBInfo.h"

#include "GaudiKernel/Algorithm.h"
#include "GaudiKernel/DataStoreItem.h"
#include "GaudiKernel/GaudiException.h"
#include "GaudiKernel/IUpdateManagerSvc.h"
#include "GaudiKernel/MsgStream.h"

#include <string>
#include <vector>

/**
 *  Load entries in the detector transient store using IDataSvc::preLoad().
 *  The node to be loaded is set with the option LoadDDDB.Node.
 *
 *  @author Marco Clemencic
 *  @date   2005-10-14
 */
class LoadDDDB : public Gaudi::Algorithm {
public:
  using Algorithm::Algorithm;

  StatusCode initialize() override;
  StatusCode execute( const EventContext& ) const override;

private:
  ServiceHandle<ICondDBInfo> m_xmlParserSvc{this, "XmlParserSvc", "XmlParserSvc"};
  // trigger the initialization of the Condition Update sub-system
  ServiceHandle<IUpdateManagerSvc> m_updMgrSvc{this, "UpdateManagerSvc", "UpdateManagerSvc"};
  Gaudi::Property<std::string>     m_treeToLoad{this, "Node", "/dd*"};
};

// Declaration of the Algorithm Factory
DECLARE_COMPONENT( LoadDDDB )

//=============================================================================
// Initialization
//=============================================================================
StatusCode LoadDDDB::initialize() {
  return Algorithm::initialize().andThen( [&]() {
    std::vector<LHCb::CondDBNameTagPair> tmp;
    m_xmlParserSvc->defaultTags( tmp );
    for ( auto& item : tmp ) { info() << "Database " << item.first << " tag " << item.second << endmsg; }
    return StatusCode::SUCCESS;
  } );
}

//=============================================================================
// Main execution
//=============================================================================
StatusCode LoadDDDB::execute( const EventContext& ) const {
  info() << "Loading the DDDB" << endmsg;
  try {
    detSvc()->addPreLoadItem( m_treeToLoad ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
    detSvc()->preLoad().ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
  } catch ( GaudiException& x ) {
    fatal() << "Caught GaudiException" << endmsg;
    int i = 0;
    for ( GaudiException* ex = &x; 0 != ex; ex = ex->previous() ) {
      fatal() << std::string( i++, ' ' ) << " ==> " << ex->what() << endmsg;
    }
    return StatusCode::FAILURE;
  } catch ( std::exception& x ) {
    fatal() << "Caught exception '" << System::typeinfoName( typeid( x ) ) << "'" << endmsg;
    fatal() << " ==> " << x.what() << endmsg;
    return StatusCode::FAILURE;
  } catch ( ... ) {
    fatal() << "Caught unknown exception!!" << endmsg;
    return StatusCode::FAILURE;
  }
  info() << "done." << endmsg;
  return StatusCode::SUCCESS;
}
