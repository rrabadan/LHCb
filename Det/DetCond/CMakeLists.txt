###############################################################################
# (c) Copyright 2000-2022 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
#[=======================================================================[.rst:
Det/DetCond
-----------
#]=======================================================================]

gaudi_add_module(DetCond
    SOURCES
        src/LoadDDDB.cpp
        src/RunStampCheck.cpp
        src/TestConditionAlg.cpp
        examples/FunctionalConditionAccessor.cpp
    LINK
        Gaudi::GaudiKernel
        LHCb::LHCbAlgsLib
        LHCb::DetDescLib
        LHCb::LHCbKernel
        LHCb::LHCbMathLib
        yaml-cpp
)
if(USE_DD4HEP)
    target_link_libraries(DetCond PRIVATE DD4hep::DDCore)
endif()

gaudi_install(PYTHON)
gaudi_generate_confuserdb()
lhcb_add_confuser_dependencies(
    GaudiCoreSvc:GaudiCoreSvc
)

# Prepare test Git CondDB overlay
foreach(_db IN ITEMS TESTCOND DD4TESTCOND)
    file(REMOVE_RECURSE ${CMAKE_CURRENT_BINARY_DIR}/test/${_db})
    file(COPY tests/data/${_db}/ DESTINATION test/${_db}/)
    execute_process(COMMAND git init -q ${CMAKE_CURRENT_BINARY_DIR}/test/${_db})
endforeach()

if(USE_DD4HEP)
    # DD4Hep plugin used for derived condition testing
    add_library(FakePlugins MODULE tests/src/fake_cond.cpp)
    target_link_libraries(FakePlugins Detector::DetectorLib LHCb::DetDescLib)
    install(TARGETS FakePlugins DESTINATION lib OPTIONAL)
    add_custom_command(TARGET FakePlugins POST_BUILD
        COMMAND run $<TARGET_FILE:DD4hep::listcomponents> -o ${CMAKE_CURRENT_BINARY_DIR}/FakePlugins.components $<TARGET_FILE_NAME:FakePlugins>
    )
    # This is implied by the gaudi_add_module above
    # set_property(TARGET target_runtime_paths APPEND
    #     PROPERTY runtime_ld_library_path $<SHELL_PATH:$<TARGET_FILE_DIR:FakePlugins>>)
    install(FILES ${CMAKE_CURRENT_BINARY_DIR}/FakePlugins.components DESTINATION lib ${_gaudi_install_optional})
endif()

if(NOT BUILD_TESTING)
    return()
endif()

gaudi_add_tests(QMTest)

set_property(
    TEST
        DetCond.condition_accessor.begin_event_and_reserve
        DetCond.condition_accessor.begin_event_no_reserve
        DetCond.condition_accessor.begin_event_no_reserve_sharedcond
        DetCond.condition_accessor.dd4hep_check_db_access
        DetCond.condition_accessor.dd4hep_check_db_access_bare
        DetCond.condition_accessor.dd4hep_cond_as_json
        DetCond.condition_accessor.dd4hep_cond_as_yaml
        DetCond.condition_accessor.dd4hep_cond_no_prop
        DetCond.condition_accessor.dd4hep_conditions
        DetCond.condition_accessor.dd4hep_conditions_error
        DetCond.condition_accessor.dd4hep_override_conditions
        DetCond.condition_accessor.dd4hep_sharedconditions
        DetCond.condition_accessor.detdesc_cond_as_json
        DetCond.condition_accessor.detdesc_cond_as_yaml
        DetCond.condition_accessor.detdesc_cond_no_prop
        DetCond.condition_accessor.no_begin_event
    APPEND PROPERTY
        ENVIRONMENT TEST_DBS_ROOT=${CMAKE_CURRENT_BINARY_DIR}/test
)

set_property(
    TEST
        DetCond.condition_accessor.dd4hep_check_db_access
        DetCond.condition_accessor.dd4hep_check_db_access_bare
        DetCond.condition_accessor.prepare
    APPEND PROPERTY
        ENVIRONMENT GIT_TEST_REPOSITORY=${CMAKE_CURRENT_BINARY_DIR}/test_repo
)

if(USE_DD4HEP)
    # disable DetDesc specific tests
    set_property(
        TEST
            DetCond.condition_accessor.begin_event_and_reserve
            DetCond.condition_accessor.begin_event_no_reserve
            DetCond.condition_accessor.begin_event_no_reserve_sharedcond
            DetCond.condition_accessor.detdesc_cond_as_json
            DetCond.condition_accessor.detdesc_cond_as_yaml
            DetCond.condition_accessor.detdesc_cond_no_prop
            DetCond.condition_accessor.no_begin_event
        PROPERTY
            DISABLED TRUE
    )
else()
    # disable DD4HEP specific tests
    set_property(
        TEST
            DetCond.condition_accessor.dd4hep_check_db_access
            DetCond.condition_accessor.dd4hep_check_db_access_bare
            DetCond.condition_accessor.dd4hep_cond_as_json
            DetCond.condition_accessor.dd4hep_cond_as_yaml
            DetCond.condition_accessor.dd4hep_cond_no_prop
            DetCond.condition_accessor.dd4hep_conditions
            DetCond.condition_accessor.dd4hep_conditions_error
            DetCond.condition_accessor.dd4hep_override_conditions
            DetCond.condition_accessor.dd4hep_sharedconditions
        PROPERTY
            DISABLED TRUE
    )
endif()
