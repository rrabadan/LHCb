/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "UTDet/DeUTSector.h"
#include "DetDesc/Condition.h"
#include "DetDesc/IGeometryInfo.h"
#include "DetDesc/SolidBox.h"
#include "GaudiAlg/GaudiTool.h"
#include "GaudiKernel/GaudiException.h"
#include "GaudiKernel/SystemOfUnits.h"
#include "Kernel/LHCbConstants.h"
#include "Kernel/UTNames.h"
#include "UTDet/DeUTSensor.h"
#include "UTDet/DeUTStave.h"
#include "UTDet/StatusMap.h"
#include <algorithm>
#include <numeric>

using namespace LHCb;

/** @file DeUTSector.cpp
 *
 *  Implementation of class :  DeUTSector
 *
 *  @author Xuhao Yuan (based on code by Andy Beiter, Jianchun Wang, Matt Needham)
 *  @date   2021-03-29
 *
 */
namespace {
  struct SortByY {
    template <typename Element>
    bool operator()( Element* obj1, Element* obj2 ) const {
      return ( !obj1 ) ? true : ( !obj2 ) ? false : obj1->globalCentre().y() < obj2->globalCentre().y();
    }
  };
  std::map<unsigned int, DeUTSector::Status> toEnumMap( const std::map<int, int>& input ) {
    std::map<unsigned int, DeUTSector::Status> output;
    std::transform( input.begin(), input.end(), std::inserter( output, output.end() ), []( auto in ) {
      return std::pair{in.first, DeUTSector::Status( in.second )};
    } );
    return output;
  }

  static const std::map<DeUTSector::Status, std::string> s_map = {{DeUTSector::OK, "OK"},
                                                                  {DeUTSector::Open, "Open"},
                                                                  {DeUTSector::Short, "Short"},
                                                                  {DeUTSector::Pinhole, "Pinhole"},
                                                                  {DeUTSector::NotBonded, "NotBonded"},
                                                                  {DeUTSector::LowGain, "LowGain"},
                                                                  {DeUTSector::Noisy, "Noisy"},
                                                                  {DeUTSector::ReadoutProblems, "ReadoutProblems"},
                                                                  {DeUTSector::OtherFault, "OtherFault"},
                                                                  {DeUTSector::Dead, "Dead"},
                                                                  {DeUTSector::UnknownStatus, "Unknown"}};
} // namespace

DeUTSector::Status toStatus( std::string_view str ) {
  auto i = std::find_if( s_map.begin(), s_map.end(), [&]( const auto& i ) { return i.second == str; } );
  return i == s_map.end() ? DeUTSector::UnknownStatus : i->first;
}

std::string toString( DeUTSector::Status status ) {
  auto i = s_map.find( status );
  return i == s_map.end() ? "UnknownStatus" : i->second;
}

StatusCode parse( DeUTSector::Status& s, std::string const& str ) {
  std::string value; // get rid of any enclosing ' and " by explicitly parsing str into an std::string
  using Gaudi::Parsers::parse;
  return parse( value, str ).andThen( [&]() -> StatusCode {
    auto i = std::find_if( s_map.begin(), s_map.end(), [&value]( const auto& i ) { return i.second == value; } );
    if ( i == s_map.end() ) { return StatusCode::FAILURE; }
    s = i->first;
    return StatusCode::SUCCESS;
  } );
}

std::ostream& DeUTSector::printOut( std::ostream& os ) const {

  // stream to cout
  return os << " Sector :  " << name() << '\n'
            << " Nickname: " << m_nickname << "\n ID " << id() << "\n type  " << type() << "\n pitch " << m_pitch
            << "\n strip " << m_nStrip << "\n capacitance " << m_capacitance / Gaudi::Units::picofarad
            << "\n dead width " << m_deadWidth << "\n center " << globalCentre() << "\n Sector status "
            << sectorStatus() << "\n fraction active " << fractionActive() << "\n version " << m_versionString
            << std::endl;
}

MsgStream& DeUTSector::printOut( MsgStream& os ) const {

  // stream to Msg service
  return os << " Sector : \n " << name() << '\n'
            << " Nickname: " << m_nickname << "\n ID " << id() << "type \n " << type() << " pitch \n " << m_pitch
            << "n strip \n " << m_nStrip << " capacitance \n " << m_capacitance / Gaudi::Units::picofarad
            << "dead width \n " << m_deadWidth << "\n center " << globalCentre() << "\n fraction active "
            << fractionActive() << "\n version " << m_versionString << std::endl;
}

StatusCode DeUTSector::initialize() {
  // initialize method
  MsgStream msg( msgSvc(), name() );

  StatusCode sc = DeUTBaseElement::initialize();
  if ( sc.isFailure() ) {
    msg << MSG::ERROR << "Failed to initialize detector element" << endmsg;
    return sc;
  }
  m_pitch       = param<double>( "pitch" );
  m_nStrip      = param<int>( "numStrips" );
  m_capacitance = param<double>( "capacitance" );
  m_type        = param<std::string>( "type" );

  // version
  if ( exists( "version" ) ) m_versionString = param<std::string>( "version" );

  // guard ring
  m_deadWidth = param<double>( "verticalGuardRing" );
  m_noiseValues.assign( nBeetle(), 0 );
  m_electronsPerADC = 1.0;

  assert( m_versionString != "DC06" );
  sc = registerCondition( this, m_statusString, &DeUTSector::updateStatusCondition, true );
  if ( sc.isFailure() ) {
    msg << MSG::ERROR << "Failed to register status conditions" << endmsg;
    return sc;
  }

  // Try to add the noise from the DB as well..
  // Can't test the version string, it's unfortunalety not trustable
  // it exists a DC07 version (why?) that doesn't contain Noise
  assert( m_versionString != "DC07" );
  sc = registerCondition( this, m_noiseString, &DeUTSector::updateNoiseCondition, true );
  if ( sc.isFailure() ) {
    msg << MSG::ERROR << "Failed to register noise conditions" << endmsg;
    return sc;
  }

  // NOTE: Try loading hit error factor conditions.
  // Okay if they do not exist, so that set as DEBUG message.
  // They are meant for data-taking,
  // which does not rely on DetDesc DetElement
  sc = updateHitErrorFactorCondition();
  if ( sc.isFailure() ) { msg << MSG::DEBUG << "Failed to register hit error factor conditions" << endmsg; }

  const unsigned subID = param<int>( "subID" );

  auto initializefunc = [&]( auto m_parent, auto subid ) {
    unsigned int tSize = m_parent->numSectorsExpected();

    // sector number needs info from mother
    if ( m_parent->staveRotZ() == "No" ) {
      setID( m_parent->firstSector() + subid );
    } else {
      setID( m_parent->firstSector() + tSize - subid + 1 );
    }

    // row..
    m_row = id() - m_parent->firstSector() + 1;

    m_prodID = m_parent->prodID();
  };

  if ( dynamic_cast<DeUTModule*>( this->parentIDetectorElementPlus() ) ) {
    --m_firstStrip;
    // get the parent
    m_module                               = getParent<DeUTModule>();
    m_column                               = m_module->column();
    m_staveID                              = m_module->stave();
    const Detector::UT::ChannelID parentID = m_module->elementID();
    // see if stereo
    m_isStereo = ( parentID.layer() % 4 == 1 ) || ( parentID.layer() % 4 == 2 );

    initializefunc( m_module, subID );
    m_row = param<int>( "secID" );

    setElementID( Detector::UT::ChannelID( Detector::UT::ChannelID::detType::typeUT, parentID.side(), parentID.layer(),
                                           parentID.stave(), parentID.face(), parentID.module(), id(), 0 ) );
  } else {
    m_version = GeoVersion::v0;
    // get the parent
    m_stave                                = getParent<DeUTStave>();
    m_column                               = m_stave->column();
    m_staveID                              = m_stave->stave();
    const Detector::UT::ChannelID parentID = m_stave->elementID();
    // see if stereo
    m_isStereo = ( parentID.layer() % 4 == 1 ) || ( parentID.layer() % 4 == 2 );

    initializefunc( m_stave, subID - 1 );

    unsigned int t_sector = id();
    unsigned int t_region = m_stave->detRegion();
    unsigned int t_face   = ( t_region != 2 ) ? t_sector % 2 : face_map( t_sector );
    int          t_module;
    if ( t_region != 2 ) {
      t_module = ( ( ( t_sector - 1 ) % 14 ) / 7 ) * 4 +
                 ( ( ( t_sector - 1 ) % 14 < 8 ) ? ( ( t_sector - 1 ) % 7 ) / 2 : ( ( t_sector - 1 ) % 7 + 1 ) / 2 );
    } else {
      t_module = module_map( t_sector );
    }
    int t_subsector = ( t_region != 2 ) ? 0 : sector_map( t_sector );

    setElementID( Detector::UT::ChannelID( Detector::UT::ChannelID::detType::typeUT, parentID.side(), parentID.layer(),
                                           parentID.stave(), t_face, t_module, t_subsector, 0 ) );
  }

  // get the nickname
  m_nickname = UTNames().UniqueSectorToString( this->elementID() );
  // get the attached sensors
  m_sensors = getChildren<DeUTSensor>();
  std::sort( m_sensors.begin(), m_sensors.end(), SortByY() );
  m_thickness = m_sensors.front()->thickness();

  m_hybridType =
      ( DeUTSector::type() == "Dual" ? HybridType::D : DeUTSector::type() == "Quad" ? HybridType::Q : HybridType::N );

  std::string region = std::to_string( ( this->elementID() ).detRegion() );
  std::string col    = staveNumber( ( this->elementID() ).detRegion(), ( this->elementID() ).station() );
  std::string sector = std::to_string( subID );

  m_conditionPathName =
      UTNames().UniqueLayerToString( ( this->elementID() ) ) + "LayerR" + region + "Stave" + col + "Sector" + sector;

  sc = registerConditionsCallbacks();
  if ( sc.isFailure() ) {
    msg << MSG::ERROR << "Failed to registerConditions call backs" << endmsg;
    return sc;
  }
  sc = cacheInfo();
  if ( sc.isFailure() ) {
    msg << MSG::ERROR << "Failed to cache geometry" << endmsg;
    return sc;
  }
  // registerCondition( m_conditionLocation, &DeUTSector::createTell1Map );

  //   std::cout << " Sector :  "  << name()
  //             << "\n Chan " << elementID()
  //             << "\n Nickname: " << nickname()
  //             << "\n ID " << id()
  //             << "\n type  " << type()
  //             << "\n pitch " << pitch()
  //             << "\n strip " << nStrip()
  //             << "\n capacitance " << capacitance()/Gaudi::Units::picofarad
  //             << "\n thickness " << thickness()
  //             << "\n measEff " << measEff()
  //             << "\n dead width " << deadWidth()
  //             << "\n center " << globalCentre()
  //             << "\n Sector status " << sectorStatus()
  //             << "\n fraction active " << fractionActive()
  //             << "\n column " << column()
  //             << "\n row " << row()
  //             << "\n prodID " << prodID()
  //             << "\n conditionsPath " << conditionsPathName()
  //             << "\n staveType " << staveType()
  //             << std::endl;

  return StatusCode::SUCCESS;
}

float DeUTSector::noise( LHCb::Detector::UT::ChannelID aChannel ) const {
  // check strip is valid
  if ( !isStrip( aChannel.strip() + m_firstStrip ) ) return 999;

  const Status theStatus = stripStatus( aChannel );

  // take ok strips
  if ( theStatus == DeUTSector::OK ) return m_noiseValues[aChannel.asic()];

  // and pinholes...
  if ( theStatus == DeUTSector::Pinhole ) return m_noiseValues[aChannel.asic()];

  return 999;
}

float DeUTSector::rawNoise( LHCb::Detector::UT::ChannelID aChannel ) const {
  return std::sqrt( noise( aChannel ) * noise( aChannel ) + cmNoise( aChannel ) * cmNoise( aChannel ) );
}

float DeUTSector::rawSectorNoise() const {
  return std::sqrt( sectorNoise() * sectorNoise() + cmSectorNoise() * cmSectorNoise() );
}

float DeUTSector::rawBeetleNoise( unsigned int beetle ) const {
  return std::sqrt( beetleNoise( beetle ) * beetleNoise( beetle ) + cmBeetleNoise( beetle ) * cmBeetleNoise( beetle ) );
}

float DeUTSector::rawPortNoise( unsigned int beetle, unsigned int port ) const {
  return std::sqrt( portNoise( beetle, port ) * portNoise( beetle, port ) +
                    cmPortNoise( beetle, port ) * cmPortNoise( beetle, port ) );
}

float DeUTSector::sectorNoise() const {
  const std::vector<DeUTSector::Status> statusVector = beetleStatus();

  float sum( 0 ), number( 0 );

  for ( unsigned int asic( 0 ); asic < nBeetle(); ++asic ) {
    if ( statusVector[asic] == DeUTSector::OK || statusVector[asic] == DeUTSector::Pinhole ) {
      sum += m_noiseValues[asic];
      number += 1;
    }
  }

  if ( number < 1 ) return 999.99f;
  MsgStream msg( msgSvc(), name() );
  if ( msg.level() <= MSG::DEBUG )
    msg << MSG::DEBUG << number << " asics out of " << nBeetle() << " are not taken into account" << endmsg;
  return sum / number;
}

float DeUTSector::beetleNoise( unsigned int beetle ) const {
  if ( beetle == 0 || beetle > nBeetle() ) {
    throw std::out_of_range( "DeUTSector::beetleNoise: beetle out of range" );
  }
  auto number = m_noiseValues[beetle];
  return number >= 1 ? number : 999.99f;
}

float DeUTSector::portNoise( unsigned int beetle, unsigned int port ) const {
  if ( beetle == 0 || beetle > nBeetle() ) { throw std::out_of_range( "DeUTSector::portNoise: beetle out of range" ); }

  if ( port == 0 || port > 4 ) { throw std::out_of_range( "DeUTSector::portNoise: port out of range" ); }

  const std::vector<DeUTSector::Status> statusVector = stripStatus();

  float sum( 0. ), number( 0. );

  for ( unsigned int chan( ( beetle - 1 ) * LHCbConstants::nStripsInBeetle +
                           ( port - 1 ) * LHCbConstants::nStripsInPort );
        chan < ( beetle - 1 ) * LHCbConstants::nStripsInBeetle + port * LHCbConstants::nStripsInPort; chan++ ) {
    if ( statusVector[chan] == DeUTSector::OK || statusVector[chan] == DeUTSector::Pinhole ) {
      sum += m_noiseValues[chan];
      number += 1;
    }
  }

  if ( number < 1 ) return 999.99f;
  MsgStream msg( msgSvc(), name() );
  if ( msg.level() <= MSG::DEBUG )
    msg << MSG::DEBUG << number << " strips out of " << LHCbConstants::nStripsInPort << " are not taken into account"
        << endmsg;
  return sum / number;
}

void DeUTSector::setNoise( unsigned int beetle, double value ) {
  Condition* aCon = condition( m_noiseString );
  if ( !aCon ) {
    MsgStream msg( msgSvc(), name() );
    msg << MSG::ERROR << "Failed to find status condition" << endmsg;
  } else {
    auto& reference                       = aCon->param<std::vector<double>>( "SectorNoise" );
    reference[beetle - m_firstBeetle]     = value;
    m_noiseValues[beetle - m_firstBeetle] = value;
  }
}

void DeUTSector::setNoise( std::vector<double> values ) {
  Condition* aCon( condition( m_noiseString ) );
  if ( !aCon ) {
    MsgStream msg( msgSvc(), name() );
    msg << MSG::ERROR << "Failed to find status condition" << endmsg;
  } else {
    auto& reference = aCon->param<std::vector<double>>( "SectorNoise" );
    reference       = values;
    m_noiseValues   = std::move( values );
  }
}

void DeUTSector::setCMNoise( unsigned int beetle, double value ) {
  Condition* aCon = condition( m_noiseString );
  if ( !aCon ) {
    MsgStream msg( msgSvc(), name() );
    msg << MSG::ERROR << "Failed to find status condition" << endmsg;
  } else {
    auto& reference                        = aCon->param<std::vector<double>>( "cmNoise" );
    reference[beetle - m_firstBeetle]      = value;
    m_cmModeValues[beetle - m_firstBeetle] = value;
  }
}

void DeUTSector::setCMNoise( std::vector<double> values ) {
  Condition* aCon( condition( m_noiseString ) );
  if ( !aCon ) {
    MsgStream msg( msgSvc(), name() );
    msg << MSG::ERROR << "Failed to find status condition" << endmsg;
  } else {
    auto& reference = aCon->param<std::vector<double>>( "cmNoise" );
    reference       = values;
    m_cmModeValues  = std::move( values );
  }
}

void DeUTSector::setADCConversion( double value ) {
  Condition* aCon( condition( m_noiseString ) );
  if ( !aCon ) {
    MsgStream msg( msgSvc(), name() );
    msg << MSG::ERROR << "Failed to find status condition" << endmsg;
  } else {
    auto& reference   = aCon->param<double>( "electronsPerADC" );
    reference         = value;
    m_electronsPerADC = std::move( value );
  }
}

float DeUTSector::cmNoise( LHCb::Detector::UT::ChannelID aChannel ) const {
  // check strip is valid
  if ( !isStrip( aChannel.strip() ) ) return 999;

  const Status theStatus = stripStatus( aChannel );

  auto strip = aChannel.strip();

  // take ok strips
  if ( theStatus == DeUTSector::OK )
    return m_cmModeValues[static_cast<unsigned int>( strip / LHCbConstants::nStripsInBeetle )];

  // and pinholes...
  if ( theStatus == DeUTSector::Pinhole )
    return m_cmModeValues[static_cast<unsigned int>( strip / LHCbConstants::nStripsInBeetle )];

  return 999;
}

float DeUTSector::cmSectorNoise() const {
  const std::vector<DeUTSector::Status> statusVector = beetleStatus();

  float sum( 0 ), number( 0 );

  for ( unsigned int asic( 0 ); asic < nBeetle(); ++asic ) {
    if ( statusVector[asic] == DeUTSector::OK || statusVector[asic] == DeUTSector::Pinhole ) {
      sum += m_cmModeValues[asic];
      number += 1;
    }
  }

  if ( number < 1 ) {
    return 999.99f;
  } else {
    MsgStream msg( msgSvc(), name() );
    if ( msg.level() <= MSG::DEBUG )
      msg << MSG::DEBUG << number << " beetles out of " << nBeetle() << " are not taken into account" << endmsg;
    return sum / number;
  }
}

float DeUTSector::cmBeetleNoise( unsigned int beetle ) const {
  if ( beetle == 0 || beetle > nBeetle() ) {
    throw std::out_of_range( "DeUTSector::cmBeetleNoise: beetle out of range" );
  }
  auto number = m_cmModeValues[beetle];
  return number >= 1 ? number : 999.99f;
}

float DeUTSector::cmPortNoise( unsigned int beetle, unsigned int port ) const {
  if ( beetle == 0 || beetle > nBeetle() ) {
    throw std::out_of_range( "DeUTSector::cmPortNoise: beetle out of range" );
  }
  if ( port == 0 || port > 4 ) { throw std::out_of_range( "DeUTSector::cmPortNoise: port out of range" ); }

  const std::vector<DeUTSector::Status> statusVector = stripStatus();

  float sum( 0 ), number( 0 );

  for ( unsigned int chan( ( beetle - 1 ) * LHCbConstants::nStripsInBeetle +
                           ( port - 1 ) * LHCbConstants::nStripsInPort );
        chan < ( beetle - 1 ) * LHCbConstants::nStripsInBeetle + port * LHCbConstants::nStripsInPort; chan++ ) {
    if ( statusVector[chan] == DeUTSector::OK || statusVector[chan] == DeUTSector::Pinhole ) {
      sum += m_cmModeValues[chan];
      number += 1;
    }
  }

  if ( number < 1 )
    return 999.99f;
  else {
    MsgStream msg( msgSvc(), name() );
    if ( msg.level() <= MSG::DEBUG )
      msg << MSG::DEBUG << number << " strips out of " << LHCbConstants::nStripsInPort << " are not taken into account"
          << endmsg;
    return sum / number;
  }
}

double DeUTSector::toADC( double e ) const { return e / m_electronsPerADC; }

double DeUTSector::toElectron( double val ) const { return val * m_electronsPerADC; }

LHCb::LineTraj<double> DeUTSector::trajectory( Detector::UT::ChannelID aChan, double offset ) const {

  if ( !contains( aChan ) ) {
    MsgStream msg( msgSvc(), name() );
    msg << MSG::ERROR << "Failed to link to sector " << nickname() << " test strip  number " << aChan.strip()
        << " strip " << endmsg;
    throw GaudiException( "Failed to make trajectory", "DeUTSector.cpp", StatusCode::FAILURE );
  }

  return createTraj( aChan.strip(), offset );
}

LHCb::LineTraj<double> DeUTSector::createTraj( unsigned int strip, double offset ) const {
  const Sensors& theSensors = m_sensors;
  if ( theSensors.size() != 1 ) {
    MsgStream msg( msgSvc(), name() );
    msg << MSG::ERROR << "Unexpected number of UT sensors in DeUTSector::createTraj" << endmsg;
    throw GaudiException( "unexpected number of UT sensors in DeUTSector::createTraj", "DeUTSector.cpp",
                          StatusCode::FAILURE );
  }
  return theSensors.front()->trajectory( strip, offset );
}

StatusCode DeUTSector::cacheInfo() {
  auto firstTraj = createTraj( m_firstStrip, 0 );
  // if ( m_stripflip && xInverted() ) firstTraj = createTraj( m_nStrip, 0 );
  if ( m_stripflip && xInverted() ) firstTraj = createTraj( m_nStrip + m_firstStrip, 0 );

  // get the start and end point. for piecewise trajectories, we
  // effectively make an approximation by a straight line.
  const Gaudi::XYZPoint g1 = firstTraj.beginPoint();
  const Gaudi::XYZPoint g2 = firstTraj.endPoint();

  const double activeWidth = m_sensors.front()->activeWidth();

  // direction
  Gaudi::XYZVector direction = g2 - g1;
  m_stripLength              = std::sqrt( direction.Mag2() );
  direction                  = direction.Unit();

  // cross with normal along z
  Gaudi::XYZVector zVec( 0, 0, 1 );
  Gaudi::XYZVector norm = direction.Cross( zVec );

  // trajectory of middle
  const Gaudi::XYZPoint g3 = g1 + 0.5 * ( g2 - g1 );
  const Gaudi::XYZPoint g4 = g3 + activeWidth * norm;

  // creating the 'fast' trajectories
  const Gaudi::XYZVector vectorlayer = ( g4 - g3 ).unit() * m_pitch;
  const Gaudi::XYZPoint  p0          = g3 - 0.5 * m_stripLength * direction;
  m_dxdy                             = direction.x() / direction.y();
  m_dzdy                             = direction.z() / direction.y();
  m_dy                               = m_stripLength * direction.y();
  m_dp0di.SetX( vectorlayer.x() - vectorlayer.y() * m_dxdy );
  m_dp0di.SetY( vectorlayer.y() );
  m_dp0di.SetZ( vectorlayer.z() - vectorlayer.y() * m_dzdy );
  m_p0.SetX( p0.x() - p0.y() * m_dxdy );
  m_p0.SetY( p0.y() );
  m_p0.SetZ( p0.z() - p0.y() * m_dzdy );

  // Update the stereo angle. We correct by 'pi' if necessary.
  Gaudi::XYZVector dir = direction;
  if ( dir.y() < 0 ) dir *= -1;
  m_angle    = atan2( -dir.x(), dir.y() );
  m_cosAngle = cos( m_angle );
  m_sinAngle = sin( m_angle );

  return StatusCode::SUCCESS;
}

Detector::UT::ChannelID DeUTSector::nextLeft( const Detector::UT::ChannelID testChan ) const {
  if ( ( contains( testChan ) ) && ( testChan.strip() > 0 ) && ( isStrip( testChan.strip() + m_firstStrip - 1u ) ) ) {
    return Detector::UT::ChannelID( testChan.channelID() - 1u );
  } else {
    return LHCb::Detector::UT::ChannelID( 0u );
  }
}

Detector::UT::ChannelID DeUTSector::nextRight( const LHCb::Detector::UT::ChannelID testChan ) const {
  if ( ( contains( testChan ) ) && ( isStrip( testChan.strip() + m_firstStrip + 1u ) ) ) {
    return Detector::UT::ChannelID( testChan.channelID() + 1u );
  } else {
    return LHCb::Detector::UT::ChannelID( 0u );
  }
}

StatusCode DeUTSector::registerConditionsCallbacks() {

  // cache trajectories
  // initialize method
  MsgStream msg( msgSvc(), name() );

  if ( m_sensors.empty() ) {
    msg << MSG::ERROR << "Sterile detector element ! No conditions registered" << endmsg;
    return StatusCode::FAILURE;
  }

  // FIXME: see gaudi/Gaudi!1162, lhcb/LHCb!2875
  StatusCode sc = registerCondition( this, const_cast<DeUTSensor*>( m_sensors.front() ), &DeUTSector::cacheInfo, true );
  if ( sc.isFailure() ) {
    msg << MSG::ERROR << "Failed to register geometry condition for first child" << endmsg;
    return StatusCode::FAILURE;
  }

  // FIXME: see gaudi/Gaudi!1162, lhcb/LHCb!2875
  sc = registerCondition( this, const_cast<DeUTSensor*>( m_sensors.back() ), &DeUTSector::cacheInfo, true );
  if ( sc.isFailure() ) {
    msg << MSG::ERROR << "Failed to register geometry condition for last child" << endmsg;
    return StatusCode::FAILURE;
  }

  return sc;
}

StatusCode DeUTSector::updateStatusCondition() {

  auto* aCon = const_cast<Condition*>( statusCondition() );
  if ( aCon == nullptr ) {
    MsgStream msg( msgSvc(), name() );
    msg << "failed to find status condition" << endmsg;
    return StatusCode::FAILURE;
  }

  const int tStatus = aCon->param<int>( "SectorStatus" );
  m_status          = Status( tStatus );

  m_beetleStatus = toEnumMap( aCon->param<std::map<int, int>>( "BeetleStatus" ) );

  m_stripStatus = toEnumMap( aCon->param<std::map<int, int>>( "StripStatus" ) );

  if ( aCon->exists( "measuredEff" ) ) {
    m_measEff = aCon->param<double>( "measuredEff" );
  } else {
    m_measEff = 1.0;
    aCon->addParam( "measuredEff", m_measEff, "Measured sector Eff" );
  }

  createCondition( m_readoutString, m_readoutpathString );
  Condition* bCon = condition( m_readoutString );
  if ( bCon == nullptr ) {
    MsgStream msg( msgSvc(), name() );
    msg << MSG::ERROR << "failed to find Readout condition" << endmsg;
    return StatusCode::FAILURE;
  }
  m_stripflip = bCon->exists( "nTell40InUT" );

  return StatusCode::SUCCESS;
}

StatusCode DeUTSector::updateHitErrorFactorCondition() {
  createCondition( m_hitErrorFactorsString, m_hitErrorFactorsPathString );
  Condition* bCon = condition( m_hitErrorFactorsString );
  if ( bCon == nullptr ) { return StatusCode::FAILURE; }
  m_hitErrorFactors = bCon->param<std::array<double, 4>>( "HitErrorFactors" );
  return StatusCode::SUCCESS;
}

StatusCode DeUTSector::updateNoiseCondition() {
  Condition* aCon = condition( m_noiseString );
  if ( aCon == nullptr ) {
    MsgStream msg( msgSvc(), name() );
    msg << MSG::ERROR << "failed to find noise condition" << endmsg;
    return StatusCode::FAILURE;
  }

  const auto& tmpNoise = aCon->param<std::vector<double>>( "SectorNoise" );

  if ( tmpNoise.size() == nBeetle() ) {
    m_noiseValues = tmpNoise;
  } else if ( tmpNoise.size() == nStrip() ) {
    MsgStream msg( msgSvc(), name() );
    msg << MSG::DEBUG << "Detect per strip (old version) SectorNoise. Will calculate average for each ASIC." << endmsg;
    std::vector<double> avgNoise;
    avgNoise.reserve( 4 );
    for ( unsigned int i = 0; i < tmpNoise.size(); i += LHCbConstants::nStripsInBeetle ) {
      double sum = std::accumulate( tmpNoise.begin() + i, tmpNoise.begin() + i + LHCbConstants::nStripsInBeetle, 0.0 );
      double average = sum / LHCbConstants::nStripsInBeetle;
      avgNoise.push_back( average );
    }
    m_noiseValues = avgNoise;
  } else {
    MsgStream msg( msgSvc(), name() );
    msg << MSG::ERROR << "Size mismatch for SectorNoise: new value " << tmpNoise.size() << " vs. ref value "
        << nBeetle() << endmsg;
    return StatusCode::FAILURE;
  }

  // Backward compatibility for different type of electronsPerADC condition
  try {
    const auto& tmpElectrons = aCon->param<double>( "electronsPerADC" );
    m_electronsPerADC        = tmpElectrons;
  } catch ( ... ) {
    const auto& tmpElectrons = aCon->param<std::vector<double>>( "electronsPerADC" );
    double      sum          = std::accumulate( tmpElectrons.begin(), tmpElectrons.end(), 0.0 );
    double      average      = sum / tmpElectrons.size();
    m_electronsPerADC        = average;
  }

  if ( aCon->exists( "cmNoise" ) ) {
    const auto& tmpCM = aCon->param<std::vector<double>>( "cmNoise" );
    if ( tmpCM.size() == nBeetle() )
      m_cmModeValues = tmpCM;
    else if ( tmpCM.size() == nStrip() ) {
      MsgStream msg( msgSvc(), name() );
      msg << MSG::DEBUG << "Detect per strip (old version) cmNoise. Will calculate average for each ASIC." << endmsg;
      std::vector<double> avgCM;
      avgCM.reserve( 4 );
      for ( unsigned int i = 0; i < tmpCM.size(); i += LHCbConstants::nStripsInBeetle ) {
        double sum     = std::accumulate( tmpCM.begin() + i, tmpCM.begin() + i + LHCbConstants::nStripsInBeetle, 0.0 );
        double average = sum / LHCbConstants::nStripsInBeetle;
        avgCM.push_back( average );
      }
      m_cmModeValues = avgCM;
    } else {
      MsgStream msg( msgSvc(), name() );
      msg << MSG::ERROR << "Size mismatch for cmNoise: new value " << tmpCM.size() << " vs. ref value " << nBeetle()
          << endmsg;
      return StatusCode::FAILURE;
    }
  } else {
    // doesn't exists (MC early databases...)
    m_cmModeValues.assign( nBeetle(), 0. );
    aCon->addParam( "cmNoise", m_cmModeValues, "Common mode per sector" );
  }

  return StatusCode::SUCCESS;
}

const DeUTSensor* DeUTSector::findSensor( const Gaudi::XYZPoint& point ) const {
  // return pointer to the layer from point
  auto iter = std::find_if( m_sensors.begin(), m_sensors.end(),
                            [&point]( const DeUTSensor* s ) { return s->isInside( point ); } );
  return iter != m_sensors.end() ? *iter : nullptr;
}

bool DeUTSector::globalInActive( const Gaudi::XYZPoint& point, Gaudi::XYZPoint tol ) const {
  return std::any_of( m_sensors.begin(), m_sensors.end(),
                      [&]( const DeUTSensor* s ) { return s->globalInActive( point, tol ); } );
}

bool DeUTSector::globalInBondGap( const Gaudi::XYZPoint& point, double tol ) const {
  const DeUTSensor* aSensor = findSensor( point );
  return aSensor ? aSensor->globalInBondGap( point, tol ) : false;
}

double DeUTSector::fractionActive() const {

  // fraction of the sector that works
  const auto& statusVector = stripStatus();
  return std::count( statusVector.begin(), statusVector.end(), DeUTSector::OK ) / double( nStrip() );
}

void DeUTSector::setMeasEff( const double value ) {
  m_measEff       = value;
  Condition* aCon = condition( m_statusString );
  if ( aCon == nullptr ) {
    MsgStream msg( msgSvc(), name() );
    msg << MSG::ERROR << "Failed to find status condition" << endmsg;
  } else {
    auto& tvalue = aCon->param<double>( "measuredEff" );
    tvalue       = double( value );
  }
}

void DeUTSector::setSectorStatus( const DeUTSector::Status& newStatus ) {
  m_status = newStatus;

  // Set the condition
  Condition* aCon = condition( m_statusString );
  if ( aCon == nullptr ) {
    MsgStream msg( msgSvc(), name() );
    msg << MSG::ERROR << "Failed to find status condition" << endmsg;
  } else {
    int& value = aCon->param<int>( "SectorStatus" );
    value      = int( newStatus );
  }
}

void DeUTSector::setBeetleStatus( unsigned int beetle, const DeUTSector::Status& newStatus ) {

  // update the beetle status properly...
  MsgStream msg( msgSvc(), name() );

  if ( sectorStatus() != DeUTSector::OK ) {
    // if the sector is not ok nothing to be done
    if ( msg.level() <= MSG::DEBUG ) msg << MSG::DEBUG << "Sector is off anyway: set request ignored " << endmsg;
  } else {
    if ( newStatus == DeUTSector::OK ) {
      // Lazarus walks...if we have an entry in the map delete it
      m_beetleStatus.erase( beetle );
      setStatusCondition( "BeetleStatus", beetle, newStatus );
    } else {
      // death comes to this beetle, update the map
      if ( std::find( ::Status::validBeetleStates().begin(), ::Status::validBeetleStates().end(), newStatus ) !=
           ::Status::validBeetleStates().end() ) {
        m_beetleStatus[beetle] = newStatus;
        setStatusCondition( "BeetleStatus", beetle, newStatus );
      } // check is valid state
      else {
        msg << "Not a valid Beetle state: set request ignored " << endmsg;
      }
    }
  }
}

void DeUTSector::setStripStatus( unsigned int strip, const DeUTSector::Status& newStatus ) {
  // update the strip status properly...
  MsgStream msg( msgSvc(), name() );

  if ( sectorStatus() != DeUTSector::OK || beetleStatus( strip ) != DeUTSector::OK ) {
    // if the sector is not ok nothing to be done
    if ( msg.level() <= MSG::DEBUG ) msg << MSG::DEBUG << "Sector/Beetle is off anyway: set request ignored " << endmsg;
  } else {
    if ( newStatus == DeUTSector::OK ) {
      // Lazarus walks...if we have an entry in the map delete it
      m_stripStatus.erase( strip );
      setStatusCondition( "StripStatus", strip, newStatus );
    } else {
      // death comes to this strip, update the map
      Status oldStatus = m_stripStatus.find( strip )->second;
      if ( std::find( ::Status::protectedStates().begin(), ::Status::protectedStates().end(), oldStatus ) ==
           ::Status::protectedStates().end() ) {
        m_stripStatus[strip] = newStatus;
        setStatusCondition( "StripStatus", strip, newStatus );
      } else {
        msg << "Strip in protected state: set request ignored " << endmsg;
      }
    }
  }
}

void DeUTSector::setStatusCondition( const std::string& type, unsigned int entry,
                                     const DeUTSector::Status& newStatus ) {
  // Set the condition
  Condition* aCon = condition( m_statusString );
  if ( !aCon ) {
    MsgStream msg( msgSvc(), name() );
    msg << MSG::ERROR << "Failed to find status condition" << endmsg;
  } else {
    auto& condMap  = aCon->param<std::map<int, int>>( type );
    condMap[entry] = int( newStatus );
  }
}

std::string DeUTSector::conditionsPathName() const { return m_conditionPathName; }

std::pair<double, double> DeUTSector::halfLengths() const {
  auto        solid = geometryPlus()->lvolume()->solid();
  const auto& box   = dynamic_cast<const SolidBox&>( *solid );
  return {box.xHalfLength(), box.yHalfLength()};
}

std::string DeUTSector::staveNumber( unsigned int reg, unsigned int station ) const {

  int           col                      = 0;
  constexpr int num_stave_in_UTaReg1and3 = 6;
  constexpr int num_stave_in_UTaReg2     = 4; // number of staves in Region 1,2,3 of UTa
  constexpr int num_stave_in_UTbReg1and3 = 7;
  constexpr int num_stave_in_UTbReg2     = 4; // number of staves in Region 1,2,3 of UTb

  // UTaX or UTaU
  if ( station == 1 ) {
    switch ( reg ) {
    case 1:
      col = column();
      break;
    case 2:
      col = column() - num_stave_in_UTaReg1and3;
      break;
    case 3:
      col = column() - num_stave_in_UTaReg1and3 - num_stave_in_UTaReg2;
      break;
    }
  } else if ( station == 2 ) { // UTbV or UTbX
    switch ( reg ) {
    case 1:
      col = column();
      break;
    case 2:
      col = column() - num_stave_in_UTbReg1and3;
      break;
    case 3:
      col = column() - num_stave_in_UTbReg1and3 - num_stave_in_UTbReg2;
      break;
    }
  }

  return std::to_string( col );
}

/** stream operator for status */
std::ostream& operator<<( std::ostream& s, DeUTSector::Status e ) {
  using namespace std::string_view_literals;
  auto i = s_map.find( e );
  return s << ( i == s_map.end() ? "Unknown"sv : std::string_view{i->second} );
}
