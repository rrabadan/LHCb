/*****************************************************************************\
* (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "DetDesc/Condition.h"
#include "LHCbAlgs/Consumer.h"
#include "UTDet/DeUTDetector.h"
#include <DetDesc/GenericConditionAccessorHolder.h>

struct DeUTTester
    : LHCb::Algorithm::Consumer<void( const DeUTDetector& ), LHCb::DetDesc::usesConditions<DeUTDetector>> {

  DeUTTester( const std::string& name, ISvcLocator* loc )
      : Consumer{name, loc, {KeyValue{"DeUT", DeUTDetLocation::location()}}} {}

  void operator()( const DeUTDetector& ut ) const override { info() << "DeUT volume: " << ut.lVolumeName() << endmsg; }
};

DECLARE_COMPONENT( DeUTTester )
