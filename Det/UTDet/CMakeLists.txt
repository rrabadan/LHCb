###############################################################################
# (c) Copyright 2000-2021 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
#[=======================================================================[.rst:
Det/UTDet
---------
#]=======================================================================]
if (USE_DD4HEP)
  set(BUILD_TESTING false) # prevent headers build test

  gaudi_add_header_only_library(UTDetLib
    LINK Detector::DetectorLib
  )

else()

    gaudi_add_library(UTDetLib
        SOURCES
            src/Lib/DeUTBaseElement.cpp
            src/Lib/DeUTDetector.cpp
            src/Lib/DeUTLayer.cpp
            src/Lib/DeUTModule.cpp
            src/Lib/DeUTSector.cpp
            src/Lib/DeUTSensor.cpp
            src/Lib/DeUTStation.cpp
		src/Lib/DeUTSide.cpp
		src/Lib/DeUTFace.cpp
            src/Lib/DeUTStave.cpp
            src/Lib/StatusMap.cpp
        LINK
            PUBLIC
                Gaudi::GaudiKernel
                LHCb::DetDescCnvLib
                LHCb::DetDescLib
                LHCb::LHCbKernel
    )

    gaudi_add_module(UTDet
        SOURCES
            src/Component/XmlUTDetectorCnv.cpp
            src/Component/XmlUTLayerCnv.cpp
            src/Component/XmlUTModuleCnv.cpp
            src/Component/XmlUTSectorCnv.cpp
            src/Component/XmlUTSensorCnv.cpp
            src/Component/XmlUTStationCnv.cpp
		src/Component/XmlUTSideCnv.cpp
		src/Component/XmlUTFaceCnv.cpp
            src/Component/XmlUTStaveCnv.cpp
        LINK
            LHCb::UTDetLib
    )

    gaudi_add_dictionary(UTDetDict
        HEADERFILES dict/UTDetDict.h
        SELECTION dict/UTDetDict.xml
        LINK
            LHCb::UTDetLib
        OPTIONS ${LHCB_DICT_GEN_DEFAULT_OPTS}
    )

endif()


# Link with Detector only in DD4hep case, otherwise the DetDesc version
# is in here
if(USE_DD4HEP)
set(TestDEUT_DDD4hepTarget
        Detector::DetectorLib
        )
endif()

gaudi_add_module(TestDeUT
    SOURCES
        tests/src/component/TestDEUT.cpp
    LINK
        Gaudi::GaudiKernel
        LHCb::LHCbAlgsLib
        LHCb::DetDescLib
        LHCb::LHCbKernel
        LHCb::UTDetLib
        ${TestDEUT_DDD4hepTarget}
)

gaudi_add_tests(QMTest)
