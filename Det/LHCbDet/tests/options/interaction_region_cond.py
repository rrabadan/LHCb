###############################################################################
# (c) Copyright 2023 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from PyConf.application import ApplicationOptions
from DDDB.CheckDD4Hep import UseDD4Hep

options = ApplicationOptions(_enabled=False)
if UseDD4Hep:
    options.geometry_version = "run3/trunk"
    options.conditions_version = "master"

    # In the tag the condition is valid for run numbers 200-400
    from Configurables import LHCb__Tests__FakeRunNumberProducer as DummyRunNumber
    DummyRunNumber("DummyRunNumber").Start = 200
else:
    options.dddb_tag = "upgrade/master"
    options.conddb_tag = "upgrade/interaction_region"
