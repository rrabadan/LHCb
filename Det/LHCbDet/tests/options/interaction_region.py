###############################################################################
# (c) Copyright 2023 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from PyConf.application import (configure, setup_component, ComponentConfig,
                                ApplicationOptions)
from PyConf.control_flow import CompositeNode, NodeLogic
from PyConf.Algorithms import InteractionRegionExample
from GaudiKernel.Constants import DEBUG
from Configurables import ApplicationMgr
from Configurables import LHCb__DetDesc__ReserveDetDescForEvent as reserveIOV
from Configurables import DDDBConf
from DDDB.CheckDD4Hep import UseDD4Hep

options = ApplicationOptions(_enabled=False)
options.simulation = True
options.data_type = 'Upgrade'
options.input_type = 'NONE'
options.evt_max = 1

mgr = ApplicationMgr()
mgr.EvtSel = "NONE"
mgr.EvtMax = 1

reserveIOV("reserveIOV").PreloadGeometry = False

config = ComponentConfig()

if UseDD4Hep:
    from Configurables import LHCb__Tests__FakeRunNumberProducer as DummyRunNumber
    from Configurables import LHCbApp, ApplicationMgr, LHCb__Det__LbDD4hep__DD4hepSvc as DD4hepSvc

    config.add(
        DDDBConf(
            Simulation=options.simulation,
            GeometryVersion=(options.geometry_version or options.dddb_tag),
            ConditionsVersion=(options.conditions_version
                               or options.conddb_tag),
            DataType=options.data_type))

    DD4hepSvc().DetectorList = ["/world", "VP"]

    def make_fake_run_number():
        odin_loc = '/Event/DAQ/DummyODIN'
        return setup_component(
            DummyRunNumber, "DummyRunNumber", Step=0, ODIN=odin_loc)

    make_fake_odin = make_fake_run_number
else:
    from PyConf.application import make_fake_event_time as make_fake_odin
    from Configurables import CondDB
    config.add(
        DDDBConf(Simulation=options.simulation, DataType=options.data_type))
    config.add(
        CondDB(
            Upgrade=True,
            Tags={
                'DDDB': options.dddb_tag,
                'SIMCOND': options.conddb_tag,
            }))

node = CompositeNode(
    'test_node',
    combine_logic=NodeLogic.NONLAZY_AND,
    children=[
        InteractionRegionExample(name="InteractionRegion", OutputLevel=DEBUG)
    ])

config.update(configure(options, node, make_fake_odin=make_fake_odin))
