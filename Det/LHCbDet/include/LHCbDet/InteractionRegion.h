/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once

#if defined( USE_DD4HEP )
#  include <Detector/LHCb/DeLHCb.h>
#else
#  include <DetDesc/DetectorElement.h>
#endif
#include <DetDesc/IDetectorElement.h>
#include <VPDet/DeVP.h>

#include <GaudiAlg/GetData.h>
#include <GaudiKernel/Algorithm.h>
#include <Math/Point3D.h>
#include <Math/SMatrix.h>
#include <Math/SMatrixDfwd.h>

namespace LHCb::Conditions {

  /// Helper class to obtain the InteractionRegion data in a
  /// backwards-compatible way. If the dedicated condition is
  /// available it will be used, otherwise DeVP::beamSpot() will be
  /// used.
  ///
  /// For DD4HEP: DeLHCb has a method to obtain the interaction
  /// region. DeVP should be used if the optional return by DeLHCb has
  /// not been initialised.
  ///
  /// For DetDesc: DeLHCb doesn't have a method (because it's not
  /// implemented as a separate class), so check if the condition is
  /// available. If it is, use it directly and otherwise use DeVP.
  class InteractionRegion final {

  public:
    inline static const std::string ConditionPath = "/dd/Conditions/Online/LHCb/InteractionRegion";

#if !defined( USE_DD4HEP )
    /// Constructor from the VP detector element in case the
    /// conditions is not available in LHCb; for use with DetDesc.
    InteractionRegion( const DeVP& vp, const Gaudi::Algorithm* parent = nullptr ) : m_parent{parent} {
      // Set the interaction region from the position of the VeLo
      // resolvers
      if ( m_parent && m_parent->msgLevel( MSG::DEBUG ) ) {
        m_parent->debug() << "DetDesc: Using beamSpot from DeVP to set position of InteractionRegion." << endmsg;
      }
      avgPosition = vp.beamSpot();
      spread      = ROOT::Math::SMatrixSym3D{};
    }

    /// Constructor from a YAML::Node if the condition is available;
    /// for use with DetDesc.
    InteractionRegion( const YAML::Node& region, const Gaudi::Algorithm* parent = nullptr ) : m_parent{parent} {
      //
      if ( m_parent && m_parent->msgLevel( MSG::DEBUG ) ) {
        m_parent->debug() << "DetDesc: Using InteractionRegion condition." << endmsg;
      }

      auto pos = region["position"].as<std::vector<double>>();
      assert( pos.size() == 3u );
      avgPosition.SetCoordinates( pos.begin(), pos.end() );

      auto sprd = region["spread"].as<std::vector<double>>();
      assert( sprd.size() == 6u );
      spread.SetElements( sprd.begin(), sprd.end(), true );
    }

#else

    /// Constructor from LHCb and VP detector elements to get the
    /// "real" condition from DeLHCb if it's available and otherwise
    /// from DeVP; for use with DD4hep
    InteractionRegion( const LHCb::Detector::DeLHCb& lhcb, //
                       const DeVP& vp, const Gaudi::Algorithm* parent = nullptr )
        : m_parent{parent} {
      //
      auto ir = lhcb.interactionRegion();
      if ( ir ) {
        if ( m_parent && m_parent->msgLevel( MSG::DEBUG ) ) {
          m_parent->debug() << "DD4HEP: Using InteractionRegion from DeLHCb." << endmsg;
        }
        avgPosition = ir->avgPosition;
        spread      = ir->spread;
      } else {
        if ( m_parent && m_parent->msgLevel( MSG::DEBUG ) ) {
          m_parent->debug() << "DD4HEP: Using beamSpot from DeVP to set position of InteractionRegion." << endmsg;
        }
        avgPosition = vp.beamSpot();
        spread      = ROOT::Math::SMatrixSym3D{};
      }
    }
#endif

    /// Creates a condition derivation for the given key
    template <typename PARENT>
    static auto addConditionDerivation( PARENT* parent, LHCb::DetDesc::ConditionKey key ) {
      if ( parent->msgLevel( MSG::DEBUG ) ) {
        parent->debug() << "InteractionRegion::addConditionDerivation : Key=" << key << endmsg;
      }
#if defined( USE_DD4HEP )
      return parent->addConditionDerivation( std::array{LHCb::standard_geometry_top, LHCb::Det::VP::det_path},
                                             std::move( key ),
                                             [p = parent]( LHCb::Detector::DeLHCb const& lhcb, DeVP const& vp ) {
                                               return InteractionRegion{lhcb, vp, p};
                                             } );
#else
      // NOTE: CheckData test only needed here to deal with fact
      // not all DB tags currently in use have the required mapping conditions.
      // We detect this here and just return a default uninitialised object.
      // downstream users always check if the object is initialised before using
      // the object, which is only done when the DB tags require it.
      // Once support for the old DB tags is no longer required the test can be removed.
      if ( Gaudi::Utils::CheckData<Condition>()( parent->detSvc(), ConditionPath ) ) {
        return parent->addConditionDerivation( std::array{ConditionPath}, std::move( key ),
                                               [p = parent]( const YAML::Node& region ) {
                                                 return InteractionRegion{region, p};
                                               } );
      } else {
        return parent->addConditionDerivation( {LHCb::Det::VP::det_path}, std::move( key ),
                                               [p = parent]( const DeVP& vp ) {
                                                 return InteractionRegion{vp, p};
                                               } );
      }
#endif
    }

    double tX() const { return ( spread( 2, 2 ) != 0.0 ) ? spread( 0, 2 ) / spread( 2, 2 ) : 0.0; }

    double tY() const { return ( spread( 2, 2 ) != 0.0 ) ? spread( 1, 2 ) / spread( 2, 2 ) : 0.0; }

    bool validSpread() const { return spread( 0, 0 ) != 0.; }

    ROOT::Math::XYZPoint     avgPosition;
    ROOT::Math::SMatrixSym3D spread;

  private:
    /// Pointer back to parent algorithm (for messaging)
    const Gaudi::Algorithm* m_parent{nullptr};
  };
} // namespace LHCb::Conditions
