/*****************************************************************************\
* (c) Copyright 2019-2022 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include <string>
#include <vector>

#include <boost/algorithm/string.hpp>

#include <DetDesc/Condition.h>
#include <LHCbDet/InteractionRegion.h>

#include <LHCbAlgs/Consumer.h>

#ifdef USE_DD4HEP
#  include <DD4hep/Grammar.h>
#endif

namespace LHCb::DetDesc::Examples {

  // Example of algorithm accessing conditions
  struct InteractionRegionExample : Algorithm::Consumer<void( const LHCb::Conditions::InteractionRegion& ),
                                                        DetDesc::usesConditions<LHCb::Conditions::InteractionRegion>> {
    // constructor
    InteractionRegionExample( const std::string& name, ISvcLocator* loc )
        : Consumer{name, loc, {KeyValue{"IRPath", "IR"}}} {}

    StatusCode initialize() override {
      return Consumer::initialize().andThen( [&]() {
        LHCb::Conditions::InteractionRegion::addConditionDerivation(
            this, inputLocation<LHCb::Conditions::InteractionRegion>() );
      } );
    }

    void operator()( const LHCb::Conditions::InteractionRegion& ir ) const override {
      std::stringstream msg;
      msg << ir.spread;
      std::vector<std::string> rows;
      boost::split( rows, msg.str(), boost::is_any_of( "\n" ) );
      info() << "interaction region: " << ir.avgPosition << endmsg;
      for ( auto r : rows ) { info() << r << endmsg; }
    }
  };

} // namespace LHCb::DetDesc::Examples

DECLARE_COMPONENT_WITH_ID( LHCb::DetDesc::Examples::InteractionRegionExample, "InteractionRegionExample" )
