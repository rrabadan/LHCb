/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "FTDet/DeFTQuarter.h"
#include "DetDesc/SolidBox.h"
/** @file DeFTQuarter.cpp
 *
 *  Implementation of class : DeFTQuarter
 *
 *  @author Jeroen van Tilburg
 *  @date   2016-07-18
 */

//=============================================================================
// classID function
//=============================================================================
const CLID& DeFTQuarter::clID() const { return DeFTQuarter::classID(); }

//=============================================================================
// Initialization
//=============================================================================
StatusCode DeFTQuarter::initialize() {
  /// Loop over modules
  m_modules.resize( this->childIDetectorElements().size(), nullptr );
  for ( auto* iM : childIDetectorElements() ) {
    DeFTModule* module = dynamic_cast<DeFTModule*>( iM );
    if ( module ) {
      unsigned int moduleID = to_unsigned( module->moduleID() );
      if ( moduleID < m_modules.size() ) { m_modules[moduleID] = module; }
    }
  } /// iM

  m_quarterID = static_cast<LHCb::Detector::FTChannelID::QuarterID>( param<int>( "quarterID" ) );
  for ( const auto* module : m_modules ) {
    const auto m1     = module->geometryPlus()->toGlobal( ROOT::Math::XYZPoint{0., 0., 0.} );
    const auto m2     = module->geometryPlus()->toGlobal( ROOT::Math::XYZPoint{0., 1., 0.} );
    const auto deltaY = m1 - m2;
    m_meanModuleDxdy += deltaY.x() / deltaY.y();
    m_meanModuleDzdy += deltaY.z() / deltaY.y();
    // get the module shape as a box
    const auto* containing_box =
        dynamic_cast<const SolidBox*>( module->geometryPlus()->lvolume()->solid()->coverTop() );
    const auto sizeY = containing_box->ysize();
    // the central point is in the centre of the module, so go down half the size in y
    // to get the z position at the mirror
    m_meanModuleZ += module->geometryPlus()->toGlobal( ROOT::Math::XYZPoint( 0, -0.5f * sizeY, 0. ) ).z();
  }
  m_meanModuleDxdy /= this->m_modules.size();
  m_meanModuleDzdy /= this->m_modules.size();
  m_meanModuleZ /= this->m_modules.size();

  return StatusCode::SUCCESS;
}

/// Find the module for a given XYZ point
const DeFTModule* DeFTQuarter::findModule( const Gaudi::XYZPoint& aPoint ) const {
  auto iM = std::find_if( m_modules.begin(), m_modules.end(),
                          [&aPoint]( const DeFTModule* m ) { return m && m->isInside( aPoint ); } );
  return iM != m_modules.end() ? *iM : nullptr;
}
