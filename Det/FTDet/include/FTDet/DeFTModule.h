/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once

#ifdef USE_DD4HEP
#  include "Detector/FT/DeFTModule.h"
using DeFTModule = LHCb::Detector::DeFTModule;
#else

#  include "DetDesc/DetectorElement.h"
#  include "Detector/FT/FTChannelID.h"
#  include "FTDet/DeFTMat.h"
#  include "GaudiKernel/Plane3DTypes.h"

/** @class DeFTModule DeFTModule.h "FTDet/DeFTModule.h"
 *
 *  This is the detector element class of the Fibre Tracker (FT) half module.
 *
 *  @author Jeroen van Tilburg
 *  @date   2016-07-18
 */

static const CLID CLID_DeFTModule = 8605;

class DeFTModule final : public DetDesc::DetectorElementPlus {

public:
  /// Standard constructor
  using DetectorElementPlus::DetectorElementPlus;

  /** Initialization method */
  StatusCode initialize() override;

  /** Retrieves reference to class identifier
   *  @return The class identifier for this class
   */
  const CLID& clID() const override;

  /** Another reference to class identifier
   *  @return The class identifier for this class
   */
  static const CLID& classID() { return CLID_DeFTModule; }

  /** @return moduleID */
  [[nodiscard]] LHCb::Detector::FTChannelID::ModuleID moduleID() const { return m_elementID.module(); }

  /** @return quarterID */
  [[nodiscard]] LHCb::Detector::FTChannelID::QuarterID quarterID() const { return m_elementID.quarter(); }

  /** @return  layerID */
  [[nodiscard]] LHCb::Detector::FTChannelID::LayerID layerID() const { return m_elementID.layer(); }

  /** @return stationID */
  [[nodiscard]] LHCb::Detector::FTChannelID::StationID stationID() const { return m_elementID.station(); }

  /** Element id */
  [[nodiscard]] LHCb::Detector::FTChannelID elementID() const { return m_elementID; }

  /** Set element id */
  DeFTModule& setElementID( const LHCb::Detector::FTChannelID& chanID ) {
    m_elementID = chanID;
    return *this;
  }

  /** @return flag true if this quarter is bottom half */
  [[nodiscard]] bool isBottom() const { return m_elementID.isBottom(); }

  /** @return flag true if this quarter is top half */
  [[nodiscard]] bool isTop() const { return m_elementID.isTop(); }

  /** @return Vector of pointers to the FT Mats */
  [[nodiscard]] const auto& mats() const { return m_mats; }

  /** Find the FT Mat where a global point is
   *  @return Pointer to the relevant Mat
   */
  [[nodiscard]] const DeFTMat* findMat( const Gaudi::XYZPoint& point ) const;

  /** Const method to return the mat for a given channel id
   * @param  aChannel  an FT channel id
   * @return pointer to detector element
   */
  [[nodiscard]] const DeFTMat* findMat( const LHCb::Detector::FTChannelID& aChannel ) const {
    return m_mats[to_unsigned( aChannel.mat() )];
  }

  /// Find a mat from global X and Y coordinates
  const DeFTMat* findMatWithXY( const ROOT::Math::XYZPoint& globalPoint ) const {
    auto iter = std::find_if( m_mats.begin(), m_mats.end(), [&globalPoint]( const DeFTMat* m ) {
      auto localPoint = m->toLocal( globalPoint );
      return m->isInside( m->toGlobal( ROOT::Math::XYZPoint( localPoint.x(), localPoint.y(), 0. ) ) );
    } );
    return iter != m_mats.end() ? *iter : nullptr;
  }

  /** Returns the xy-plane at z-middle the layer */
  [[nodiscard]] const Gaudi::Plane3D& plane() const { return m_plane; }

  /** Get the pseudoChannel for a given FTChannelID
   *  The pseudoChannel is defined for a full quarter and runs
   *  in increasing global |x|.
   *  This function is useful for occupancy plots.
   *  @param the FTChannelID
   *  @return the pseudoChannel, between 0 and 6*16*128
   */
  [[nodiscard]] int pseudoChannel( LHCb::Detector::FTChannelID channelID ) const;

  /** Get the FTChannelID corresponding to a given pseudoChannel
   *  The pseudoChannel is defined for a full quarter and runs
   *  in increasing global |x|.
   *  This function is useful to generate noise form afterpulses.
   *  @param pseudoChannel, between 0 and 16*128
   *  @return the corresponding FTChannelID
   */
  [[nodiscard]] LHCb::Detector::FTChannelID channelFromPseudo( const int pseudoChannel ) const;

  Gaudi::XYZPoint  toLocal( const Gaudi::XYZPoint& p ) const { return this->geometryPlus()->toLocal( p ); }
  Gaudi::XYZPoint  toGlobal( const Gaudi::XYZPoint& p ) const { return this->geometryPlus()->toGlobal( p ); }
  Gaudi::XYZVector toLocal( const Gaudi::XYZVector& v ) const { return this->geometryPlus()->toLocal( v ); }
  Gaudi::XYZVector toGlobal( const Gaudi::XYZVector& v ) const { return this->geometryPlus()->toGlobal( v ); }

  /// Visitor functions
  void applyToAllChildren( const std::function<void( const DeFTMat& )>& func ) const {
    for ( const auto* mat : m_mats ) { func( *mat ); };
  };

private:
  LHCb::Detector::FTChannelID m_elementID; ///< element ID

  /// vector of pointers to mats
  std::array<const DeFTMat*, 4> m_mats{{nullptr, nullptr, nullptr, nullptr}};

  int            m_nChannelsInModule; ///< number of channels per module
  Gaudi::Plane3D m_plane;             ///< xy-plane in the z-middle of the module
  bool           m_reversed;          ///< Flag set when the pseudochannel-ordering is reversed

}; // end of class

/// Find mat method
#endif
