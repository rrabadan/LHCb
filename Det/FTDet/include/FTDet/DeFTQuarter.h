/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once

#ifdef USE_DD4HEP
#  include "Detector/FT/DeFTQuarter.h"
using DeFTQuarter = LHCb::Detector::DeFTQuarter;
#else
#  include "DetDesc/DetectorElement.h"
#  include "Detector/FT/FTChannelID.h"
#  include "FTDet/DeFTModule.h"

/** @class DeFTQuarter DeFTQuarter.h "FTDet/DeFTQuarter.h"
 *
 *  This is the detector element class for a Fibre Tracker Quarter.
 *
 *  @author Jeroen van Tilburg
 *  @date   2016-07-18
 *
 */

static const CLID CLID_DeFTQuarter = 8603;

class DeFTQuarter final : public DetDesc::DetectorElementPlus {

public:
  using ID = LHCb::Detector::FTChannelID::QuarterID;
  /// Standard constructor
  using DetectorElementPlus::DetectorElementPlus;

  /** Retrieves reference to class identifier
   * @return the class identifier for this class
   */
  const CLID& clID() const override;

  /** Another reference to class identifier
   * @return the class identifier for this class
   */
  static const CLID& classID() { return CLID_DeFTQuarter; };

  /** Initialization method */
  StatusCode initialize() override;

  /** Const method to return the module for a given channel id
   * @param  aChannel  an FT channel id
   * @return pointer to detector element
   */
  [[nodiscard]] const DeFTModule* findModule( const LHCb::Detector::FTChannelID aChannel ) const {
    auto m = to_unsigned( aChannel.module() );
    return m < m_modules.size() ? m_modules[m] : nullptr;
  }

  /** Const method to return the module for a given XYZ point
   * @param  aPoint the given point
   * @return const pointer to detector element
   */
  [[nodiscard]] const DeFTModule* findModule( const Gaudi::XYZPoint& aPoint ) const;

  /// Find a module from global X and Y coordinates
  const DeFTModule* findModuleWithXY( const ROOT::Math::XYZPoint& globalPoint ) const {
    auto iter = std::find_if( m_modules.begin(), m_modules.end(), [&globalPoint]( const DeFTModule* m ) {
      auto localPoint = m->toLocal( globalPoint );
      return m->isInside( m->toGlobal( ROOT::Math::XYZPoint( localPoint.x(), localPoint.y(), 0. ) ) );
    } );
    return iter != m_modules.end() ? *iter : nullptr;
  }

  /** Flat vector of all FT modules
   * @return vector of modules
   */
  [[nodiscard]] const auto& modules() const { return m_modules; }

  /** @return quarterID */
  [[nodiscard]] LHCb::Detector::FTChannelID::QuarterID quarterID() const { return m_quarterID; }

  /** @return flag true if this quarter is bottom half */
  [[nodiscard]] bool isBottom() const { return is_bottom( m_quarterID ); }

  /** @return flag true if this quarter is top half */
  [[nodiscard]] bool isTop() const { return is_top( m_quarterID ); }

  [[nodiscard]] float meanModuleZ() const { return m_meanModuleZ; }
  [[nodiscard]] float meanModuleDxdy() const { return m_meanModuleDxdy; }
  [[nodiscard]] float meanModuleDzdy() const { return m_meanModuleDzdy; }

private:
  LHCb::Detector::FTChannelID::QuarterID                m_quarterID{}; ///< quarter ID number
  boost::container::static_vector<const DeFTModule*, 6> m_modules{};   ///< vector of modules
  float                                                 m_meanModuleZ{};
  float                                                 m_meanModuleDzdy{};
  float                                                 m_meanModuleDxdy{};
}; // end of class
#endif
