/*****************************************************************************\
* (c) Copyright 2000-2022 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once

#include <Core/MagneticFieldGrid.h>
#include <GaudiKernel/IAlgTool.h>
#include <GaudiKernel/Point3DTypes.h>
#include <GaudiKernel/Vector3DTypes.h>

/** @class IBIntegrator IBIntegrator.h Kernel/IBIntegrator.h
 *  Interface class for field integral tool.
 *
 *  @author Rutger Hierck
 *  @date   2002-05-28
 */
struct IBIntegrator : extend_interfaces<IAlgTool> {

  // Return the interface ID
  DeclareInterfaceID( IBIntegrator, 4, 0 );

  /// Get the z of center and the total Bdl
  virtual void calculateBdlAndCenter( const LHCb::Magnet::MagneticFieldGrid* field, const Gaudi::XYZPoint& beginPoint,
                                      const Gaudi::XYZPoint& endPoint, const double tX, const double tY,
                                      double& zCenter, Gaudi::XYZVector& Bdl ) const = 0;
};
