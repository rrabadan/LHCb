/*****************************************************************************\
* (c) Copyright 2021-2022 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once

#include <DetDesc/ConditionKey.h>

#ifdef USE_DD4HEP
#  include <Detector/Magnet/DeMagnet.h>
using DeMagnet = LHCb::Detector::DeMagnet;
namespace LHCb::Det::Magnet {
  inline const LHCb::DetDesc::ConditionKey det_path = "/world/MagnetRegion/Magnet:DetElement-Info-IOV";
} // namespace LHCb::Det::Magnet

#else

#  include <Kernel/ILHCbMagnetSvc.h>

namespace LHCb::Det::Magnet {
  inline const LHCb::DetDesc::ConditionKey det_path = "DeMagnet";
}

/*
 * DeMagnet derived condition that wraps the MagneticFieldSvc
 */
class DeMagnet {

public:
  DeMagnet( ILHCbMagnetSvc* svc ) : m_magFieldSvc{svc} {}

  Gaudi::XYZVector fieldVector( const Gaudi::XYZPoint& pos ) const { return m_magFieldSvc->fieldVector( pos ); }

  const LHCb::Magnet::MagneticFieldGrid* fieldGrid() const { return m_magFieldSvc->fieldGrid(); }

  bool useRealMap() const { return m_magFieldSvc->useRealMap(); }

  bool isDown() const { return m_magFieldSvc->isDown(); }

  double signedRelativeCurrent() const { return m_magFieldSvc->signedRelativeCurrent(); }

private:
  /// The actual service to which we forward the calls
  ILHCbMagnetSvc* m_magFieldSvc = nullptr;
};

#endif
