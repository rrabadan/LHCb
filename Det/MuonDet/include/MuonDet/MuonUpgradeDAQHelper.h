/*****************************************************************************\
* (c) Copyright 2000-2021 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once

#include "Detector/Muon/TileID.h"
#include "MuonDet/MuonDAQConstants.h"
#include "MuonDet/MuonNODEBoard.h"
#include "MuonDet/MuonTell40Board.h"
#include "MuonDet/MuonTell40PCI.h"
#include "MuonDet/MuonUpgradeStationCabling.h"
#include "MuonDet/MuonUpgradeTSMap.h"
#include <memory>
#include <string>
#include <vector>

class IDataProviderSvc;

/** @class MuonUpgradeDAQHelper MuonUpgradeDAQHelper.h MuonDet/MuonUpgradeDAQHelper.h
 *
 *
 *  @author Alessia Satta
 *  @date   2021-12-07
 */

class MuonUpgradeDAQHelper final {
public:
  void                initSvc( IDataProviderSvc* detSvc );
  void                resetAll();
  inline unsigned int getTSLayoutX( unsigned int s, unsigned int r ) const { return m_TSlayoutX[s][r]; };
  inline unsigned int getTSLayoutY( unsigned int s, unsigned int r ) const { return m_TSlayoutY[s][r]; };

  LHCb::Detector::Muon::TileID findTS( const LHCb::Detector::Muon::TileID digit ) const;
  std::string                  findODEPath( const LHCb::Detector::Muon::TileID TS ) const;
  int                          findODENumber( const LHCb::Detector::Muon::TileID TS ) const;
  std::string                  findTSPath( std::string ODEPath, long TSPosition, int station ) const;
  void       DAQaddressInODE( LHCb::Detector::Muon::TileID digitTile, unsigned int& ODENumber, unsigned int& frame,
                              unsigned int& channelInFrame ) const;
  StatusCode DAQaddressInTELL40( LHCb::Detector::Muon::TileID digitTile, long& Tell40Number,
                                 unsigned int& Tell40PCINumber, unsigned int& Tell40PCILinkNumber ) const;
  LHCb::Detector::Muon::TileID        TileInTell40Frame( long Tell40Number, unsigned int Tell40PCINumber,
                                                         unsigned int Tell40PCILinkNumber, unsigned int chInFrame ) const;
  inline LHCb::Detector::Muon::TileID TileInTell40FrameNoHole( long Tell40Number, unsigned int Tell40PCINumber,
                                                               unsigned int Tell40PCILinkNumber,
                                                               unsigned int chInFrame ) const {
    return m_mapTileInTell40[Tell40Number - 1][Tell40PCINumber]
                            [Tell40PCILinkNumber * MuonUpgradeDAQHelper_ODEFrameSize + chInFrame];
  };

  inline LHCb::Detector::Muon::TileID TileInTell40FrameNoHoleTest( long Tell40Number, unsigned int Tell40PCINumber,
                                                                   unsigned int Tell40PCILinkNumber,
                                                                   unsigned int chInFrame ) const {
    return *( &m_mapTileInTell40[0][0][0] +
              ( Tell40Number - 1 ) * MuonUpgradeDAQHelper_maxTell40PCINumber_linkNumber_ODEFrameSize +
              Tell40PCINumber * MuonUpgradeDAQHelper_linkNumber_ODEFrameSize +
              Tell40PCILinkNumber * MuonUpgradeDAQHelper_ODEFrameSize + chInFrame );
  };
  inline LHCb::Detector::Muon::TileID TileInTell40FrameNoHoleFast( unsigned int offset ) const {
    return *( &m_mapTileInTell40[0][0][0] + offset );
  };
  inline unsigned int stationOfLink( long Tell40Number, unsigned int Tell40PCINumber,
                                     unsigned int Tell40PCILinkNumber ) const {
    return m_mapStationOfLink[Tell40Number - 1][Tell40PCINumber][Tell40PCILinkNumber];
  };
  inline unsigned int regionOfLink( long Tell40Number, unsigned int Tell40PCINumber,
                                    unsigned int Tell40PCILinkNumber ) const {
    return m_mapRegionOfLink[Tell40Number - 1][Tell40PCINumber][Tell40PCILinkNumber];
  };
  inline unsigned int quarterOfLink( long Tell40Number, unsigned int Tell40PCINumber,
                                     unsigned int Tell40PCILinkNumber ) const {
    return m_mapQuarterOfLink[Tell40Number - 1][Tell40PCINumber][Tell40PCILinkNumber];
  };

  inline unsigned int whichstationTell40( long Tell40Number ) const { return m_which_stationIsTell40[Tell40Number]; };
  void getSynchConnection( unsigned int NODE, unsigned int frame, long& Tell40Number, unsigned int& Tell40PCINumber,
                           unsigned int& Tell40PCILinkNumber ) const;
  std::string  getBasePath( std::string statname ) const;
  std::string  getStationName( int station ) const;
  unsigned int getNumberOfActiveLinkInTell40PCI( unsigned int Tell40Number, unsigned int Tell40PCINumber ) const {
    return m_Tell40PCINumberOfActiveLink[Tell40Number - 1][Tell40PCINumber];
  }
  std::string getODENameInECS( unsigned int ODENumber ) const { return m_ODENameInECS[ODENumber - 1]; }
  void        getODENumberAndSynch( long Tell40Number, unsigned int Tell40PCINumber, unsigned int Tell40PCILinkNumber,
                                    int& ODENumber, int& SynchNumber ) const;

  int getSourceID( long Tell40Number, unsigned int Tell40PCINumber ) const {
    return m_sourceID[Tell40Number - 1][Tell40PCINumber];
  }

  LHCb::Detector::Muon::TileID getTileIDInNODE( unsigned int ODENumber, unsigned int Tell40PCILinkNumber ) const {
    return m_mapTileInNODE[ODENumber][Tell40PCILinkNumber];
  }

  unsigned int getODENumberNoHole( long Tell40Number, unsigned int Tell40PCINumber,
                                   unsigned int Tell40PCILinkNumber ) const {
    return m_ODENumberNoHole[Tell40Number][Tell40PCINumber][Tell40PCILinkNumber];
  }

  unsigned int getODEFrameNumberNoHole( long Tell40Number, unsigned int Tell40PCINumber,
                                        unsigned int Tell40PCILinkNumber ) const {
    return m_ODEFrameNumberNoHole[Tell40Number][Tell40PCINumber][Tell40PCILinkNumber];
  }

  unsigned int getNumberOfTell40Boards( unsigned int station ) const { return m_NumberOfTell40Boards[station]; }
  const std::vector<MuonTell40PCI>& getTell40s() const { return v_pci; };
  unsigned int                      getDeclaredLinks( long Tell40Number, unsigned int Tell40PCINumber ) const {
    return m_declaredLinks[Tell40Number - 1][Tell40PCINumber];
  };

  const std::vector<MuonTell40PCI> getTell40sFromDB() const;

private:
  IDataProviderSvc* m_detSvc = nullptr;

  StatusCode test();
  void       initTELL40();
  /// NODE sanity checks
  void initNODE();
  /// steering initialization
  void initDAQMaps();
  /// TELL1 sanity checks
  void         initMaps();
  void         initSourceID();
  unsigned int findTSPosition( std::string ODEPath, LHCb::Detector::Muon::TileID TSTile ) const;
  unsigned int findDigitInTS( std::string TSPath, LHCb::Detector::Muon::TileID TSTile,
                              LHCb::Detector::Muon::TileID digit, bool hole ) const;
  void         resetLUT();

  static const int                         m_nStations{4};
  std::map<std::string, std::string> const basePath{{"M2", "/dd/Conditions/ReadoutConf/Muon/Cabling/M2Upgrade/"},
                                                    {"M3", "/dd/Conditions/ReadoutConf/Muon/Cabling/M3Upgrade/"},
                                                    {"M4", "/dd/Conditions/ReadoutConf/Muon/Cabling/M4Upgrade/"},
                                                    {"M5", "/dd/Conditions/ReadoutConf/Muon/Cabling/M5Upgrade/"}};
  std::array<std::string, 4> const         stationName{"M2", "M3", "M4", "M5"};

  std::array<std::array<int, 16>, 2> const m_layoutX{{{
                                                          48, 48, 48, 48, // M2 vstrips
                                                          48, 48, 48, 48, // M3 vstrips
                                                          12, 12, 12, 12, // M4 vstrips
                                                          12, 12, 12, 12, // M5 vstrips
                                                      },
                                                      {
                                                          8, 4, 0, 0, // M2 vstrips
                                                          8, 4, 2, 2, // M3 vstrips
                                                          0, 4, 2, 2, // M4 vstrips
                                                          0, 4, 2, 0, // M5 vstrips
                                                      }}};
  std::array<std::array<int, 16>, 2> const m_layoutY{{{
                                                          1, 2, 8, 8, // M2 vstrips
                                                          1, 2, 2, 2, // M3 vstrips
                                                          8, 2, 2, 2, // M4 vstrips
                                                          8, 2, 2, 8, // M5 vstrips
                                                      },
                                                      {
                                                          8, 16, 0, 0, // M2 vstrips
                                                          8, 8, 8, 8,  // M3 vstrips
                                                          0, 8, 8, 8,  // M4 vstrips
                                                          0, 8, 8, 0,  // M5 vstrips
                                                      }}};
  unsigned int                             m_TSlayoutX[5][4];
  unsigned int                             m_TSlayoutY[5][4];
  std::string                              m_fullNODEPath[MuonUpgradeDAQHelper_maxNODENumber];

  int              layout[20];
  std::string      m_ODENameInECS[MuonUpgradeDAQHelper_maxNODENumber];
  unsigned int     m_TSLength[5][4];
  int              m_TUSize[5][4];
  std::vector<int> m_tellPerStation[5];
  // for UPGRADE
  unsigned int m_startNODEInStation[5];
  unsigned int m_stopNODEInStation[5];
  unsigned int m_ODENameSRQStart[5][4][4];
  unsigned int m_ODENameSRQStop[5][4][4];

  // not used
  unsigned int m_NODENumberInTell40[MuonUpgradeDAQHelper_maxTell40Number];
  // not used
  std::vector<unsigned int> m_NODEInTell40[MuonUpgradeDAQHelper_maxTell40Number];

  std::array<unsigned int, m_nStations> m_NumberOfTell40Boards;

  // Tiles in each ODE  ODENumber as index starting from zero (NODESerialNumber-1) returned vector is 192
  std::vector<LHCb::Detector::Muon::TileID> m_mapTileInNODE[MuonUpgradeDAQHelper_maxNODENumber];

  // map to know ODE/synch frame ->Tell40  Tell40input connection
  // to which Tell40 a specif nsync is connected Tell40 is numbered from 1
  unsigned int m_Tell40Number[MuonUpgradeDAQHelper_maxNODENumber][MuonUpgradeDAQHelper_frameNumber] = {};

  // map to know ODE/synch frame ->Tell40 PCI Tell40input connection
  // to which Tell40 a specif nsync is connected Tell40 PCI is numbered from 0
  unsigned int m_Tell40PCINumber[MuonUpgradeDAQHelper_maxNODENumber][MuonUpgradeDAQHelper_frameNumber] = {};

  // map to know ODE/synch frame ->Tell40 PCI link Tell40input connection
  // to which Tell40 a specif nsync is connected Tell40 PCI link is numbered from 0 (link counts also the holes)
  unsigned int m_Tell40PCILinkNumber[MuonUpgradeDAQHelper_maxNODENumber][MuonUpgradeDAQHelper_frameNumber] = {};

  // map Tell40 PCI Tell40input connection to  ODE/synch frame

  // map which ODE is connected to Tell40 (numbered from 0) PCINumber (numbered from 0) link (hole are not counted)
  unsigned int m_ODENumberNoHole[MuonUpgradeDAQHelper_maxTell40Number][MuonUpgradeDAQHelper_maxTell40PCINumber]
                                [MuonUpgradeDAQHelper_linkNumber] = {};

  // map which ODE frame is connected to Tell40 (numbered from 0) PCINumber (numbered from 0) link (hole are not
  // counted)
  unsigned int m_ODEFrameNumberNoHole[MuonUpgradeDAQHelper_maxTell40Number][MuonUpgradeDAQHelper_maxTell40PCINumber]
                                     [MuonUpgradeDAQHelper_linkNumber] = {};

  // map which ODE is connected to Tell40 (numbered from 0) PCINumber (numbered from 0) link (hole are counted)
  unsigned int m_ODENumberWithHole[MuonUpgradeDAQHelper_maxTell40Number][MuonUpgradeDAQHelper_maxTell40PCINumber]
                                  [MuonUpgradeDAQHelper_linkNumber] = {};

  // map which ODE frame is connected to Tell40 (numbered from 0) PCINumber (numbered from 0) link (hole are  counted)
  unsigned int m_ODEFrameNumberWithHole[MuonUpgradeDAQHelper_maxTell40Number][MuonUpgradeDAQHelper_maxTell40PCINumber]
                                       [MuonUpgradeDAQHelper_linkNumber] = {};

  //
  unsigned int m_Tell40LinkToActiveLink[MuonUpgradeDAQHelper_maxTell40Number][MuonUpgradeDAQHelper_maxTell40PCINumber]
                                       [MuonUpgradeDAQHelper_linkNumber] = {};

  // map Tell40frame ->Detector::Muon::TileID
  LHCb::Detector::Muon::TileID m_mapTileInTell40[MuonUpgradeDAQHelper_maxTell40Number]
                                                [MuonUpgradeDAQHelper_maxTell40PCINumber]
                                                [MuonUpgradeDAQHelper_linkNumber * MuonUpgradeDAQHelper_ODEFrameSize];
  // map which station is connected to a particular Tell40
  unsigned int m_which_stationIsTell40[MuonUpgradeDAQHelper_maxTell40Number] = {};

  unsigned int m_Tell40PCINumberOfActiveLink[MuonUpgradeDAQHelper_maxTell40Number]
                                            [MuonUpgradeDAQHelper_maxTell40PCINumber] = {};
  unsigned int m_mapStationOfLink[MuonUpgradeDAQHelper_maxTell40Number][MuonUpgradeDAQHelper_maxTell40PCINumber]
                                 [MuonUpgradeDAQHelper_linkNumber] = {};
  unsigned int m_mapRegionOfLink[MuonUpgradeDAQHelper_maxTell40Number][MuonUpgradeDAQHelper_maxTell40PCINumber]
                                [MuonUpgradeDAQHelper_linkNumber] = {};
  unsigned int m_mapQuarterOfLink[MuonUpgradeDAQHelper_maxTell40Number][MuonUpgradeDAQHelper_maxTell40PCINumber]
                                 [MuonUpgradeDAQHelper_linkNumber] = {};
  std::array<std::array<int, MuonUpgradeDAQHelper_maxTell40PCINumber>, MuonUpgradeDAQHelper_maxTell40Number>
                             m_sourceID = {{}};
  std::vector<MuonTell40PCI> v_pci;
  unsigned int m_declaredLinks[MuonUpgradeDAQHelper_maxTell40Number][MuonUpgradeDAQHelper_maxTell40PCINumber] = {};
};
