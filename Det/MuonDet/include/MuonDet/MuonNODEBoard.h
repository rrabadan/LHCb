/*****************************************************************************\
* (c) Copyright 2000-2021 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once

#include "DetDesc/Condition.h"
#include "Detector/Muon/TileID.h"
#include "MuonDet/CLID_MuonNODEBoard.h"

#include "GaudiKernel/DataObject.h"

/**
 *  @author Alessia Satta
 *  @date   2021-07-12
 */
class MuonNODEBoard : public Condition {
public:
  MuonNODEBoard();

  inline static const CLID& classID() { return CLID_MuonNODEBoard; }
  StatusCode                initialize() override;

  using Condition::update;
  StatusCode update( long tslayx, long tslayy, long tsnumb, std::vector<long> gridx, std::vector<long> gridy,
                     std::vector<long> quadrant );

  StatusCode         addTSName( std::string name );
  inline void        setNODESerialNumber( long num ) { m_NODENumber = num; }
  unsigned int       getNODESerialNumber() { return m_NODENumber; }
  inline void        setTSLayoutX( long num ) { m_TSLayoutX = num; }
  inline void        setTSLayoutY( long num ) { m_TSLayoutY = num; }
  inline long        getTSLayoutX() { return m_TSLayoutX; }
  inline long        getTSLayoutY() { return m_TSLayoutY; }
  inline long        getTSNumber() { return m_TSNumber; }
  inline void        setTSNumber( long num ) { m_TSNumber = num; }
  inline long        getTSGridX( int i ) { return m_TSGridX[i]; }
  inline long        getTSGridY( int i ) { return m_TSGridY[i]; }
  inline long        getTSQuadrant( int i ) { return m_TSQuadrant[i]; }
  inline std::string getTSName( int i ) { return m_TSName[i]; }
  inline void        setRegion( long i ) { m_region = i; }
  inline long        region() { return m_region; }
  inline void        setQuadrant( long i ) { m_unique_quadrant = i; }
  inline long        quadrant() { return m_unique_quadrant; }
  void               setQuadrants();
  bool               isQuadrantContained( long quadrant );
  bool               isTSContained( LHCb::Detector::Muon::TileID TSTile );
  void               setECSName( std::string name ) { m_ECSName = name; }

  inline std::string ECSName() { return m_ECSName; }

protected:
private:
  long                     m_region;
  long                     m_NODENumber;
  long                     m_TSLayoutX;
  long                     m_TSLayoutY;
  long                     m_TSNumber;
  std::vector<long>        m_TSGridX;
  std::vector<long>        m_TSGridY;
  std::vector<long>        m_TSQuadrant;
  std::vector<std::string> m_TSName;
  std::string              m_ECSName;
  long                     m_unique_quadrant;
  long                     m_quadrant[4];
};
