/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once

#ifdef USE_DD4HEP

#  include "Detector/Muon/ReadoutCond.h"

using MuonReadoutCond = LHCb::Detector::Muon::ReadoutCond;

#else

// Include files
#  include "DetDesc/Condition.h"
#  include "GaudiKernel/DataObject.h"
#  include <vector>
//#include "Kernel/FPEGuard.h"
#  include <cmath>

#  include "MuonDet/CLID_MuonReadoutCond.h"

/** @class MuonReadoutCond MuonReadoutCond.h MuonDet/MuonReadoutCond.h
 *
 *  The muon readout parameters implemented as a Condition
 *
 *  Stores the numbers related to the electronics and chamber effects
 *  required to convert GEANT entry/exit points into electronics channels
 *  fired for a given chamber readout
 *
 *  @author David Hutchcroft
 *  @date   21/01/2002
 */

class MuonReadoutCond : public Condition {
public:
  /// Default Constructors
  MuonReadoutCond();

  /// Copy constructor
  MuonReadoutCond( MuonReadoutCond& obj );

  using Condition::update;
  /// Update using another MuonReadoutCond: deep copy all contents,
  /// except for the properties of a generic DataObject
  virtual void update( MuonReadoutCond& obj );

  /// Destructor
  ~MuonReadoutCond();

public:
  /// Sets the cumlative cross talk arrays
  StatusCode initialize() override;

  // Re-implemented from DataObject

  /// Class ID of this instance
  inline const CLID& clID() const override { return classID(); }

  /// Class ID of this class
  inline static const CLID& classID() { return CLID_MuonReadoutCond; }

public:
  // local setters and getters

  /// get number of readouts associated to this chamber
  int number() const { return m_RList.size(); }
  /// get number of readouts associated to this chamber
  std::size_t size() const { return m_RList.size(); }

  /// adds another readout of either "Anode" or "Cathode" as a std::string,
  /// returns the index of the readout created in index.
  StatusCode addReadout( const std::string& rType, int& index );

  /// get readoutType (Anode=0, Cathode=1)
  inline int readoutType( int i ) const { return m_RList[i].ReadoutType; }

  /// get Efficiency as a fraction (mean value)
  inline double efficiency( int i ) const { return m_RList[i].Efficiency; }
  /// set efficiency
  void setEfficiency( double eff, const int& i ) { m_RList[i].Efficiency = eff; }

  /// get (maximum) synchronization imprecision (ns)
  inline double syncDrift( int i ) const { return m_RList[i].SyncDrift; }
  /// set (maximum) synchronization imprecision (ns)
  void setSyncDrift( double sync, int i ) { m_RList[i].SyncDrift = sync; }

  /// get Chamber Noise rate (counts/sec/cm2)
  inline double chamberNoise( int i ) const { return m_RList[i].ChamberNoise; }
  /// set Chamber Noise rate (counts/sec/cm2)
  void setChamberNoise( double chamNoise, int i ) { m_RList[i].ChamberNoise = chamNoise; }

  /// get Electronics noise rates (counts/sec/channel)
  inline double electronicsNoise( int i ) const { return m_RList[i].ElectronicsNoise; }
  /// set Electronics noise rates (counts/sec/channel)
  void setElectronicsNoise( double elecNoise, int i ) { m_RList[i].ElectronicsNoise = elecNoise; }

  /// get average dead time of a channel (ns)
  inline double meanDeadTime( int i ) const { return m_RList[i].MeanDeadTime; }
  /// set average dead time of a channel (ns)
  void setMeanDeadTime( double mDead, int i ) { m_RList[i].MeanDeadTime = mDead; }

  /// get RMS of the dead time (ns)
  inline double rmsDeadTime( int i ) const { return m_RList[i].RMSDeadTime; }
  /// set RMS of the dead time (ns)
  void setRMSDeadTime( double rmsDead, int i ) { m_RList[i].RMSDeadTime = rmsDead; }

  /// get time gate start relative to T0 (ns)
  inline double timeGateStart( const int i ) const { return m_RList[i].TimeGateStart; }
  /// set time gate start relative to T0 (ns)
  void setTimeGateStart( double tGate, int i ) { m_RList[i].TimeGateStart = tGate; }

  /// get size of double hit rate at pad edge in X
  void setPadEdgeSizeX( double pedge, int i ) { m_RList[i].PadEdgeSizeX = pedge; }
  /// get width (sigma) of pad edge effect in X (cm)
  void setPadEdgeSigmaX( double psigma, int i ) { m_RList[i].PadEdgeSigmaX = psigma; }
  /// get size of double hit rate at pad edge in Y
  void setPadEdgeSizeY( double pedge, int i ) { m_RList[i].PadEdgeSizeY = pedge; }
  /// get width (sigma) of pad edge effect in Y (cm)
  void setPadEdgeSigmaY( const double psigma, int i ) { m_RList[i].PadEdgeSigmaY = psigma; }

  /// function to add a cluster size with a corresponding probablilty in X
  void addClusterX( int size, double prob, int i );
  /// function to add a cluster size with a corresponding probablilty in Y
  void addClusterY( int size, double prob, int i );

  int singleGapClusterX( double randomNumber, double xDistPadEdge, int i ) const;

  int singleGapClusterY( double randomNumber, double xDistPadEdge, int i ) const;

  /// Function to set the time jitter PDF, minimum and increment
  void setTimeJitter( const std::vector<double>& jitterVec, double min, double max, int i );

  /// get a time jitter distribution in ns
  std::vector<double> timeJitter( double& min, double& max, const int& i ) const {
    min = m_RList[i].JitterMin;
    max = m_RList[i].JitterMax;
    return m_RList[i].JitterVector;
  }

private:
  // some typdefs for the structures to store the information in

  // used to keep the cluster size parameters before turning them into
  // cumlative probabilites
  typedef struct {
    int    clSize;
    double clProb;
  } _clus;

  // this holds the details of a readout (Anode or Cathode)
  typedef struct {
    int                 ReadoutType;
    double              Efficiency;
    double              SyncDrift;
    double              ChamberNoise;
    double              ElectronicsNoise;
    double              MeanDeadTime;
    double              RMSDeadTime;
    double              TimeGateStart;
    double              PadEdgeSizeX;
    double              PadEdgeSigmaX;
    double              PadEdgeSizeY;
    double              PadEdgeSigmaY;
    std::vector<_clus>  ClusterX;
    std::vector<_clus>  ClusterY;
    std::vector<double> CumProbX;
    std::vector<double> CumProbY;
    std::vector<double> JitterVector;
    double              JitterMin;
    double              JitterMax;
  } _readoutParameter;

private:
  /// used by the copy and update methods
  std::vector<_readoutParameter> getRList() const { return m_RList; }

  /// returns a single gap cluster size in x (0) or y (1)
  /// should be given a random number between 0 and 1 and distance from the
  /// pad edge (cm)
  int singleGapCluster( int xy, double randomNumber, double xpos, int i ) const;

  /// The list of the readouts
  std::vector<_readoutParameter> m_RList;
};

#endif
