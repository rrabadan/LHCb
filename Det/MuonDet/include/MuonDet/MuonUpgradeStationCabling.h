/*****************************************************************************\
* (c) Copyright 2000-2021 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once

#include "DetDesc/Condition.h"
#include "MuonDet/CLID_MuonUpgradeStationCabling.h"

#include "GaudiKernel/DataObject.h"

#include <vector>

/**
 *  @author Alessia Satta
 *  @date   2004-01-07
 */
class MuonUpgradeStationCabling : public Condition {
public:
  StatusCode initialize() override;

  using Condition::update;
  virtual void update( Condition& obj );
  void         update( ValidDataObject& obj ) override;
  StatusCode   update( long l1numb );

  /// Class ID of this class
  inline static const CLID& classID() { return CLID_MuonUpgradeStationCabling; }

  long                            getNumberOfTell40Board() const { return m_numberOfTell40Board; }
  const std::string&              getTell40Name( unsigned int i ) const { return m_listOfTell40[i]; }
  const std::vector<std::string>& getAllTell40Names() const { return m_listOfTell40; }
  StatusCode                      addTell40Name( std::string name );

private:
  long                     m_numberOfTell40Board = 0;
  std::vector<std::string> m_listOfTell40;
};
