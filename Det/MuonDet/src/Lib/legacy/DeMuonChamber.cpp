/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "MuonDet/DeMuonChamber.h"

#include "Core/FloatComparison.h"
#include "DetDesc/Condition.h"
#include "DetDesc/IGeometryInfo.h"
#include "DetDesc/IPVolume.h"
#include "DetDesc/SolidBox.h"

#include "GaudiKernel/Plane3DTypes.h"

#include "boost/algorithm/string/predicate.hpp"

/** @file DeMuonChamber.cpp
 *
 * Implementation of class : DeMuonChamber
 *
 * @author David Hutchcroft, David.Hutchcroft@cern.ch
 *
 */

/// Constructor setting pad sizes and number of gas gaps and chamber number
DeMuonChamber::DeMuonChamber( int nStation, int nRegion, int nChamber )
    : m_StationNumber( nStation ), m_RegionNumber( nRegion ), m_ChamberNumber( nChamber ) {}

StatusCode DeMuonChamber::initialize() {
  return DetectorElementPlus::initialize().andThen( [&] {
    int                sta( 0 ), reg( 0 ), chm( 0 );
    const std::string& name  = this->name();
    int                len   = name.size();
    int                start = DeMuonLocation::Default.size();
    std::string        substring;
    substring.assign( name, start, len );
    char patt[400];
    sprintf( patt, "%s", substring.c_str() );
    std::string stanum;
    stanum.assign( name, start, 3 );
    sscanf( stanum.c_str(), "/M%d", &sta );
    std::string regnum;
    regnum.assign( name, start + 11, 3 );
    sscanf( regnum.c_str(), "/R%d", &reg );
    std::string chnum;
    chnum.assign( name, start + 19, 10 );
    sscanf( chnum.c_str(), "/Cham%d", &chm );
    //  setStationNumber(sta-1); // error !!!!!! when M1 is not present station number =1 for M2 !!!
    setStationNumber( sta - 1 );
    setRegionNumber( reg - 1 );
    setChamberNumber( chm - 1 );

    // get resolution parameters
    m_chmbGrid = param<std::string>( "Grid" );

    // for now with MWPCs and RPCs this is a good formula
    setGridName( m_chmbGrid );
    return StatusCode::SUCCESS;
  } );
}

bool DeMuonChamber::checkHitAndGapInChamber( float xpos, float ypos ) const {
  IPVolume* firstGap      = getFirstGasGap();
  IPVolume* firstGapLayer = getGasGapLayer( 0 );

  Gaudi::XYZPoint pInMother  = firstGap->toMother( Gaudi::XYZPoint( 0, 0, 0 ) );
  Gaudi::XYZPoint pInChamber = firstGapLayer->toMother( pInMother );
  Gaudi::XYZPoint pInGlobal  = geometryPlus()->toGlobal( pInChamber );
  Gaudi::XYZPoint point( xpos, ypos, 0 );
  point.SetZ( pInGlobal.z() );
  Gaudi::XYZPoint pointInChamber = geometryPlus()->toLocal( point );
  Gaudi::XYZPoint pointInLayer   = firstGapLayer->toLocal( pointInChamber );

  return firstGap->isInside( pointInLayer );
}

bool DeMuonChamber::calculateHitPosInGap( int gapNumber, float xpos, float ypos, float xSlope, float ySlope,
                                          float zavegaps, Gaudi::XYZPoint& entryGlobal,
                                          Gaudi::XYZPoint& exitGlobal ) const {
  IPVolume* p_Gap      = getGasGap( gapNumber );
  IPVolume* p_GapLayer = getGasGapLayer( gapNumber );

  if ( p_Gap != NULL ) {
    const ISolid*   gapSolid = ( p_Gap )->lvolume()->solid();
    const SolidBox* gapBox   = dynamic_cast<const SolidBox*>( gapSolid );
    const double    zhalfgap = gapBox->zHalfLength();
    const double    xhalfgap = gapBox->xHalfLength();
    const double    yhalfgap = gapBox->yHalfLength();

    Gaudi::XYZPoint loch       = geometryPlus()->toLocal( Gaudi::XYZPoint( xpos, ypos, zavegaps ) );
    Gaudi::XYZPoint logaslayer = p_GapLayer->toLocal( loch );
    Gaudi::XYZPoint poslocal   = p_Gap->toLocal( logaslayer );

    const double    zcenter    = poslocal.z();
    Gaudi::XYZPoint slopelocal = Gaudi::XYZPoint( xSlope, ySlope, 1.0F );
    const double    zinf       = -zhalfgap;
    const double    zsup       = +zhalfgap;
    double          xentry     = poslocal.x() + ( zinf - zcenter ) * ( slopelocal.x() / slopelocal.z() );
    double          xexit      = poslocal.x() + ( zsup - zcenter ) * ( slopelocal.x() / slopelocal.z() );
    double          yentry     = poslocal.y() + ( zinf - zcenter ) * ( slopelocal.y() / slopelocal.z() );
    double          yexit      = poslocal.y() + ( zsup - zcenter ) * ( slopelocal.y() / slopelocal.z() );

    // check that gap boundaries have not been crossed
    const double xmin    = -xhalfgap;
    const double ymin    = -yhalfgap;
    const double xmax    = xhalfgap;
    const double ymax    = yhalfgap;
    double       zmin    = -zhalfgap;
    double       zmax    = zhalfgap;
    bool         correct = true;
    if ( xentry < xmin || xentry > xmax ) correct = false;
    if ( yentry < ymin || yentry > ymax ) correct = false;
    if ( xexit < xmin || xexit > xmax ) correct = false;
    if ( yexit < ymin || yexit > ymax ) correct = false;
    if ( correct ) {
      // then x
      if ( LHCb::essentiallyZero( slopelocal.x() ) ) return false;
      double z1  = ( xmin - poslocal.x() ) / ( slopelocal.x() / slopelocal.z() );
      double z2  = ( xmax - poslocal.x() ) / ( slopelocal.x() / slopelocal.z() );
      double zz1 = std::min( z1, z2 );
      double zz2 = std::max( z1, z2 );
      zmin       = std::max( zmin, zz1 );
      zmax       = std::min( zmax, zz2 );
      // then y
      if ( LHCb::essentiallyZero( slopelocal.y() ) ) return false;
      z1   = ( xmin - poslocal.y() ) / ( slopelocal.y() / slopelocal.z() );
      z2   = ( xmax - poslocal.y() ) / ( slopelocal.y() / slopelocal.z() );
      zz1  = std::min( z1, z2 );
      zz2  = std::max( z1, z2 );
      zmin = std::max( zmin, zz1 );
      zmax = std::min( zmax, zz2 );
      if ( zmin > zhalfgap || zmax < -zhalfgap ) return false;
      xentry = poslocal.x() + ( slopelocal.x() / slopelocal.z() ) * ( zmin - zcenter );
      xexit  = poslocal.x() + ( slopelocal.x() / slopelocal.z() ) * ( zmax - zcenter );
      yentry = poslocal.y() + ( slopelocal.y() / slopelocal.z() ) * ( zmin - zcenter );
      yexit  = poslocal.y() + ( slopelocal.y() / slopelocal.z() ) * ( zmax - zcenter );
      // back to the global frame
      Gaudi::XYZPoint entryInLayer = p_Gap->toMother( Gaudi::XYZPoint( xentry, yentry, zmin ) );
      Gaudi::XYZPoint entryInCh    = p_GapLayer->toMother( entryInLayer );

      entryGlobal                 = ( geometryPlus() )->toGlobal( entryInCh );
      Gaudi::XYZPoint exitInLayer = p_Gap->toMother( ( Gaudi::XYZPoint( xexit, yexit, zmax ) ) );
      Gaudi::XYZPoint exitInCh    = p_GapLayer->toMother( exitInLayer );

      exitGlobal = ( geometryPlus() )->toGlobal( exitInCh );
      return true;
    }
  }
  return false;
}

float DeMuonChamber::calculateAverageGap( int gapNumberStart, int gapNumberStop, float xpos, float ypos ) const {
  float zaverage = 0;
  for ( int i = gapNumberStart; i <= gapNumberStop; i++ ) {
    IPVolume* pGapLayer = getGasGapLayer( i );
    IPVolume* pGap      = getGasGap( i );
    // get 3 points to build a plane for the gap center
    Gaudi::XYZPoint point1 =
        geometryPlus()->toGlobal( pGapLayer->toMother( pGap->toMother( Gaudi::XYZPoint( 0., 0., 0. ) ) ) );
    Gaudi::XYZPoint point2 =
        geometryPlus()->toGlobal( pGapLayer->toMother( pGap->toMother( Gaudi::XYZPoint( 1., 0., 0. ) ) ) );
    Gaudi::XYZPoint point3 =
        geometryPlus()->toGlobal( pGapLayer->toMother( pGap->toMother( Gaudi::XYZPoint( 0., 1., 0. ) ) ) );
    Gaudi::Plane3D   plane( point1, point2, point3 );
    double           zInterceptThisPlane = 0.;
    Gaudi::XYZVector para                = plane.Normal();
    if ( !LHCb::essentiallyZero( para.Z() ) ) {
      double a            = para.X();
      double b            = para.Y();
      double d            = plane.HesseDistance();
      zInterceptThisPlane = -( a * xpos + b * ypos + d ) / ( para.Z() );
    } else
      zInterceptThisPlane = point1.z();
    zaverage = zaverage + (float)zInterceptThisPlane;
  }
  return zaverage / ( gapNumberStop - gapNumberStart + 1 );
}

std::tuple<ROOT::Math::XYZPoint, ROOT::Math::XYZPoint> DeMuonChamber::getGapPoints( int gap, double x,
                                                                                    double y ) const {
  IPVolume*    gasGap      = getGasGap( gap );
  IPVolume*    gasGapLayer = getGasGapLayer( gap );
  const auto*  gapBox      = dynamic_cast<const SolidBox*>( gasGap->lvolume()->solid() );
  const double zhalfgap    = gapBox->zHalfLength();
  // traslate xyz e local to global..
  const Gaudi::XYZPoint pointInLayer1 = gasGap->toMother( Gaudi::XYZPoint( x, y, -1 * zhalfgap + 0.1 ) );
  const Gaudi::XYZPoint pointInLayer2 = gasGap->toMother( Gaudi::XYZPoint( x, y, zhalfgap - 0.1 ) );
  const Gaudi::XYZPoint pointInCh1    = gasGapLayer->toMother( pointInLayer1 );
  const Gaudi::XYZPoint pointInCh2    = gasGapLayer->toMother( pointInLayer2 );
  const Gaudi::XYZPoint point1        = geometryPlus()->toGlobal( pointInCh1 );
  const Gaudi::XYZPoint point2        = geometryPlus()->toGlobal( pointInCh2 );
  return {point1, point2};
}

IPVolume* DeMuonChamber::getFirstGasGap() const {

  for ( auto pvIterator = geometryPlus()->lvolume()->pvBegin(); pvIterator != geometryPlus()->lvolume()->pvEnd();
        ++pvIterator ) {

    const ILVolume* geoCh = ( *pvIterator )->lvolume();

    if ( boost::ends_with( geoCh->name(), "/lvGasGap" ) ) {
      // loop un variys gap layer to get the real gas gap volume
      auto i = std::find_if( geoCh->pvBegin(), geoCh->pvEnd(),
                             []( const auto* l ) { return !l->lvolume()->sdName().empty(); } );
      if ( i != geoCh->pvEnd() ) return *i;
    }
  }
  return nullptr;
}

IPVolume* DeMuonChamber::getGasGap( int number ) const {
  int loopnumber = 0;
  for ( auto pvIterator = geometryPlus()->lvolume()->pvBegin(); pvIterator != geometryPlus()->lvolume()->pvEnd();
        ++pvIterator ) {

    const ILVolume* geoCh = ( *pvIterator )->lvolume();

    if ( boost::ends_with( geoCh->name(), "/lvGasGap" ) ) {
      // loop un variys gap layer to get the real gas gap volume
      for ( auto pvGapIterator = geoCh->pvBegin(); pvGapIterator != geoCh->pvEnd(); pvGapIterator++ ) {
        if ( !( *pvGapIterator )->lvolume()->sdName().empty() ) {
          if ( loopnumber == number ) return *pvGapIterator;
          loopnumber++;
        }
      }
    }
  }
  return nullptr;
}

IPVolume* DeMuonChamber::getGasGapLayer( int number ) const {
  int loopnumber = 0;
  for ( auto pvIterator = geometryPlus()->lvolume()->pvBegin(); pvIterator != geometryPlus()->lvolume()->pvEnd();
        pvIterator++ ) {

    const ILVolume* geoCh = ( *pvIterator )->lvolume();

    if ( boost::ends_with( geoCh->name(), "/lvGasGap" ) ) {
      if ( loopnumber == number ) return ( *pvIterator );
      loopnumber++;
    }
  }
  return nullptr;
}

std::optional<DeMuonChamber::PointInGasGap> DeMuonChamber::isPointInGasGap( Gaudi::XYZPoint pointInChamber ) const {
  int loopnumber = 0;
  for ( auto pvIterator = geometryPlus()->lvolume()->pvBegin(); pvIterator != geometryPlus()->lvolume()->pvEnd();
        pvIterator++ ) {

    const ILVolume* geoCh = ( *pvIterator )->lvolume();

    if ( boost::ends_with( geoCh->name(), "/lvGasGap" ) ) {
      // loop un variys gap layer to get the real gas gap volume

      bool isIn = ( *pvIterator )->isInside( pointInChamber );

      if ( isIn ) {
        Gaudi::XYZPoint myPointInGasFrame = ( *pvIterator )->toLocal( pointInChamber );
        for ( auto pvGapIterator = ( geoCh->pvBegin() ); pvGapIterator != ( geoCh->pvEnd() ); pvGapIterator++ ) {
          if ( !( ( *pvGapIterator )->lvolume()->sdName().empty() ) ) {
            bool isInGap = ( *pvGapIterator )->isInside( myPointInGasFrame );
            if ( isInGap ) {
              return DeMuonChamber::PointInGasGap{( *pvGapIterator )->toLocal( myPointInGasFrame ), loopnumber,
                                                  *pvGapIterator};
            } else {
              return {};
            }
          }
        }
      }
      loopnumber++;
    }
  }
  return {};
}

int DeMuonChamber::getGasGapNumber() const {
  int loopnumber = 0;
  for ( auto pvIterator = geometryPlus()->lvolume()->pvBegin(); pvIterator != geometryPlus()->lvolume()->pvEnd();
        pvIterator++ ) {

    const ILVolume* geoCh = ( *pvIterator )->lvolume();
    if ( boost::ends_with( geoCh->name(), "/lvGasGap" ) ) {
      // loop un variys gap layer to get the real gas gap volume
      for ( auto pvGapIterator = geoCh->pvBegin(); pvGapIterator != geoCh->pvEnd(); pvGapIterator++ ) {
        if ( !( *pvGapIterator )->lvolume()->sdName().empty() ) { loopnumber++; }
      }
    }
  }
  return loopnumber;
}
