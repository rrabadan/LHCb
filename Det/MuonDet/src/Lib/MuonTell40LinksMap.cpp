/*****************************************************************************\
* (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#include "MuonDet/MuonTell40LinksMap.h"

#include "GaudiKernel/GaudiException.h"
#include "GaudiKernel/StatusCode.h"

namespace LHCb::MuonUpgrade {

  Tell40LinksMap::Tell40LinksMap( const DeMuonDetector& det, const LHCb::Detector::DeLHCb&
#ifdef USE_DD4HEP
                                                                 lhcbdet
#endif
  ) {

    auto in_cabling_Tell40s = det.getUpgradeDAQInfo()->getTell40s();
    // loop on existing Tell40

#ifdef USE_DD4HEP
    for ( const auto& pci : in_cabling_Tell40s ) {
      unsigned int  countLinks             = 0;
      unsigned int  originalPositionNoHole = 0;
      std::uint16_t s                      = pci.getSourceID();

      unsigned int TandPCINumber =
          ( pci.Tell40Number() - 1 ) * MuonUpgradeDAQHelper_maxTell40PCINumber + pci.Tell40PCINumber();

      const auto linksActiveCond = lhcbdet.tell40links( s );

      if ( !linksActiveCond.has_value() ) {
        // Only warn once per SourceID
        fillDefaultMaps( pci );
        m_initializedCondition[TandPCINumber] = InitStatus::FAKE;
      } else {
        unsigned int enabled                  = 0;
        m_initializedCondition[TandPCINumber] = InitStatus::OK;
        // LHCb general condition has this source ID so use its value instead.
        // maybe faster nee dto check auto linksInTell = pci.activeLinkNumber();
        for ( unsigned int i = 0; i < MuonUpgradeDAQHelper_linkNumber; i++ ) {
          if ( pci.getODENumber( i ) > 0 ) {
            // RX in input to Tell40 is used
            if ( linksActiveCond.value().isEnabled( i ) ) {
              // is it enabled in online?
              enabled |= ( 0x1 << i );
              m_internalMapNoHole[TandPCINumber][countLinks].connected     = true;
              m_internalMapNoHole[TandPCINumber][countLinks].corresponding = originalPositionNoHole;
              countLinks++;
            }
            originalPositionNoHole++;
          }
        }
        m_mapEnabledWithHole[TandPCINumber] = enabled;
      }
    }
#else
    // lhcbdet condition does not exist, so initialize as fake
    for ( const auto& pci : in_cabling_Tell40s ) {
      // unsigned int  countLinks             = 0;
      // unsigned int  originalPositionNoHole = 0;
      // maybe use it ???std::uint16_t s                      = pci.getSourceID();

      unsigned int TandPCINumber =
          ( pci.Tell40Number() - 1 ) * MuonUpgradeDAQHelper_maxTell40PCINumber + pci.Tell40PCINumber();

      fillDefaultMaps( pci );
      m_initializedCondition[TandPCINumber] = InitStatus::FAKE;
    }

#endif
  }

  std::optional<unsigned int> Tell40LinksMap::linkNumberWithNoHole( unsigned int TNumber, unsigned int PCINumber,
                                                                    unsigned int link ) const {
    if ( TNumber == 0 ) return {};
    if ( TNumber < MuonUpgradeDAQHelper_maxTell40Number + 1 ) {
      if ( PCINumber < MuonUpgradeDAQHelper_maxTell40PCINumber ) {
        if ( link < MuonUpgradeDAQHelper_linkNumber ) {
          unsigned int TandPCINumber = ( TNumber - 1 ) * MuonUpgradeDAQHelper_maxTell40PCINumber + PCINumber;
          if ( m_internalMapNoHole[TandPCINumber][link].connected ) {
            return m_internalMapNoHole[TandPCINumber][link].corresponding;
          }
        }
      }
    }
    return {};
  }

  connectionMap Tell40LinksMap::getAllConnection( unsigned int TNumber, unsigned int PCINumber ) const {
    std::array<unsigned int, MuonUpgradeDAQHelper_linkNumber> map = {0};
    unsigned int TandPCINumber = ( TNumber - 1 ) * MuonUpgradeDAQHelper_maxTell40PCINumber + PCINumber;
    unsigned int clink         = 0;
    for ( unsigned int il = 0; il < MuonUpgradeDAQHelper_linkNumber; il++ ) {
      if ( m_internalMapNoHole[TandPCINumber][il].connected ) {
        map[il] = m_internalMapNoHole[TandPCINumber][il].corresponding;
        clink++;
      }
    }
    return connectionMap( clink, map );
  }

  unsigned int Tell40LinksMap::getEnabled( unsigned int TNumber, unsigned int PCINumber ) const {
    unsigned int TandPCINumber = ( TNumber - 1 ) * MuonUpgradeDAQHelper_maxTell40PCINumber + PCINumber;
    if ( TandPCINumber < m_mapEnabledWithHole.size() ) return m_mapEnabledWithHole[TandPCINumber];
    return 0;
  };

  void Tell40LinksMap::fillDefaultMaps( const MuonTell40PCIBoard& pci ) {

    unsigned int countLinks = 0;

    // maybe store also for sourceID ??std::uint16_t s = pci.getSourceID();

    unsigned int TandPCINumber =
        ( pci.Tell40Number() - 1 ) * MuonUpgradeDAQHelper_maxTell40PCINumber + pci.Tell40PCINumber();
    m_mapEnabledWithHole[TandPCINumber] = 0;

    for ( unsigned int i = 0; i < MuonUpgradeDAQHelper_linkNumber; i++ ) {
      if ( pci.getODENumber( i ) > 0 ) {
        m_internalMapNoHole[TandPCINumber][countLinks].connected     = true;
        m_internalMapNoHole[TandPCINumber][countLinks].corresponding = countLinks;
        countLinks++;
      }
    }
    for ( unsigned int i = countLinks; i < MuonUpgradeDAQHelper_linkNumber; i++ ) {
      m_internalMapNoHole[TandPCINumber][i].connected     = false;
      m_internalMapNoHole[TandPCINumber][i].corresponding = 0;
    }
  }
} // namespace LHCb::MuonUpgrade
