/*****************************************************************************\
* (c) Copyright 2000-2021 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once

#include <Kernel/IOHandler.h>
#include <MDF/Buffer.h>
#include <MDF/MDFHeader.h>
#include <MDF/RawDataConnection.h>

#include <Event/RawEvent.h>

#include <GaudiKernel/IService.h>
#include <GaudiKernel/SmartIF.h>
#include <GaudiUtils/IIODataManager.h>

#include <fmt/format.h>

#include <assert.h>
#include <mutex>

namespace LHCb::MDF {

  struct FileReadInputHandler;
  using IOHandlerFileRead = LHCb::IO::IOHandler<ByteBuffer, FileReadInputHandler>;

  /**
   * helper class dealing with a set of input files and able to read/seek from them
   */
  struct FileReadInputHandler {

    /// constructor
    template <typename Owner>
    FileReadInputHandler( Owner& owner, std::vector<std::string> const& input ) : m_input( input ) {
      // Retrieve conversion service handling event iteration
      SmartIF<IService> service = owner.service( "Gaudi::IODataManager/IODataManager" );
      m_ioMgr                   = service.as<Gaudi::IIODataManager>();
      if ( !m_ioMgr ) {
        throw GaudiException{"IOHandlerFileRead",
                             "Unable to localize interface IID_IIODataManager from service IODataManager",
                             StatusCode::FAILURE};
      }
    }

    /// initialize. Connects to first input already and thus may throw EndOfInput
    template <typename Owner>
    void initialize( Owner& owner, IIncidentSvc* incidentSvc ) {
      m_incidentSvc = incidentSvc;
      connectToCurrentInput( owner );
    }

    /**
     * rewinds to the begining of input
     */
    void rewind() { throw GaudiException{"IOHandlerFileRead", "Rewind not implemented", StatusCode::FAILURE}; }

    void finalize() {}

    /// destructor
    ~FileReadInputHandler() { m_ioMgr.reset(); }

    /**
     * skips next n events without reading them (only headers will be actually read)
     * @throws IO::EndOfInput
     */
    template <typename Owner>
    void skip( Owner& owner, unsigned int );
    /**
     * wrapper around seek method of IOMgr, handling errors
     * @param offset how much to seek from current place
     * @throws IO::EndOfInput
     */
    void seek( int offset ) {
      if ( m_ioMgr->seek( &m_curConnection, offset, SEEK_CUR ) == -1 ) { throw IO::EndOfInput(); }
    }
    /**
     * wrapper around read method of IOMgr, handling errors
     * @param buffer the buffer where to put the data
     * @param size number of bytes to read
     * @throws IO::EndOfInput
     */
    /// wrapper around read method of IOMgr, handling errors and throwing EndOfInput
    void read( LHCb::span<std::byte> buffer ) {
      auto sc = m_ioMgr->read( &m_curConnection, buffer.data(), buffer.size() );
      if ( sc.isFailure() ) { throw IO::EndOfInput(); }
    }
    /**
     * reads the next Event header into given buffer
     * buffer must be large enough or the behavior is undefined
     * @throws IO::EndOfInput
     */
    template <typename Owner>
    void readNextEventHeader( Owner& owner, LHCb::MDFHeader& );

    /**
     * prefetches a number of events from input, allocating a Buffer object to store them and returning it
     * It will try to fill the buffer unless input is empty
     * @param bufferSize the size of the buffer to allocate
     */
    template <typename Owner>
    std::shared_ptr<ByteBuffer> prefetchEvents( Owner&, unsigned int nbEvents );

    /**
     * connects to current input
     * @throws IO::EndOfInput
     */
    template <typename Owner>
    void connectToCurrentInput( Owner& owner );

    /// Incident service. FIXME : drop whenever FSRSink is not depending on this anymore
    IIncidentSvc* m_incidentSvc;
    /// vector of input files
    std::vector<std::string> const m_input;
    /// currently used input as an index in m_input
    unsigned int m_curInput{0};
    /// connection to current input, only valid if connected
    LHCb::RawDataConnection m_curConnection{0, ""};
    /// Reference to file manager service
    SmartIF<Gaudi::IIODataManager> m_ioMgr;

    /**
     * This member allow to deal with the case where an MDFHEader
     * is read from a file but there is not enough space in the current buffer
     * to read the associated event. In such case, the data is stored temporarily
     * in curHeader to be reused when next buffer is built
     * Used only in prefetchEvents
     */
    std::optional<MDFHeader> m_curHeader{};
  };

} // namespace LHCb::MDF

template <typename Owner>
std::shared_ptr<LHCb::MDF::ByteBuffer> LHCb::MDF::FileReadInputHandler::prefetchEvents( Owner&       owner,
                                                                                        unsigned int nbEvents ) {
  // we may be called for 0 events under particular circumstances
  if ( 0 == nbEvents ) {
    auto                             buffer = unique_ptr_free<std::byte>{};
    std::vector<LHCb::MDF::MDFEvent> events;
    return std::make_shared<ByteBuffer>( std::move( buffer ), std::move( events ) );
  }
  // allocate a new buffer according to requested size, taking 50K per event
  // 50K is low, this is to ensure that in most cases we will not reallocate the event vector
  // We also set a minimum of 2M so that most big events can be handled anyway, even if people
  // ask for very few events
  unsigned int bufferSize = std::max( nbEvents * 50000, 2000000u );
  auto         buffer     = unique_ptr_free<std::byte>{reinterpret_cast<std::byte*>( std::malloc( bufferSize ) )};
  if ( nullptr == buffer ) { std::runtime_error{"Unable to allocate memory for new buffer"}; }
  // create associated events vector and reserve space
  std::vector<LHCb::MDF::MDFEvent> events;
  events.reserve( nbEvents );
  // Fill buffer with banks while there is enough space and create associated events
  auto curBufPtr  = buffer.get();
  auto headerSize = sizeof( LHCb::MDFHeader );
  try {
    while ( curBufPtr + headerSize <= buffer.get() + bufferSize ) {
      LHCb::MDFHeader* header = reinterpret_cast<LHCb::MDFHeader*>( curBufPtr );
      // in case we had read an MDFHeader and not its event, use it now, else read from file
      if ( m_curHeader ) {
        memcpy( (std::byte*)header, (std::byte*)&m_curHeader.value(), headerSize );
        m_curHeader.reset();
      } else {
        readNextEventHeader( owner, *header );
      }
      if ( curBufPtr + header->recordSize() >= buffer.get() + bufferSize ) {
        // buffer is full, let's stop here and store the header we've read in our static storage
        m_curHeader = *header;
        // check that the buffer size is not too small for a single event !
        if ( header->recordSize() > bufferSize ) {
          throw std::runtime_error{fmt::format( "Found too big event ({} bytes), not fitting into input buffers. "
                                                "Please consider changing ioalg_buffer_nb_events to at least {}",
                                                header->recordSize(), ( ( header->recordSize() + 49999 ) / 50000 ) )};
        }
        break;
      }
      // enough space for rawbanks of the next event in buffer. Let's first copy the banks to the buffer
      assert( header->recordSize() >= headerSize );
      read( {curBufPtr + headerSize,
             static_cast<LHCb::span<std::byte>::size_type>( header->recordSize() - headerSize )} );
      // now let's build the event and copy the raw banks
      events.emplace_back( header, LHCb::span<std::byte>{(std::byte*)header->data(), header->size()},
                           header->compression() & 0xF );
      curBufPtr += header->recordSize();
    }
  } catch ( IO::EndOfInput& e ) {
    // we've reached the end of the input
    // if we have data, just break, else rethrow
    if ( curBufPtr == buffer.get() && nbEvents != 0 ) throw e;
  }
  return std::make_shared<ByteBuffer>( std::move( buffer ), std::move( events ) );
}

template <typename Owner>
void LHCb::MDF::FileReadInputHandler::connectToCurrentInput( Owner& owner ) {
  while ( m_curInput < m_input.size() ) {
    m_curConnection = LHCb::RawDataConnection( 0, m_input[m_curInput] );
    auto sc         = m_ioMgr->connectRead( false, &m_curConnection );
    if ( sc.isSuccess() ) {
      // Needed for OpenFilesTracker. FIXME : to be dropped when OpenFilesTracker does not need it anymore
      m_incidentSvc->fireIncident( Incident( m_input[m_curInput], "CONNECTED_INPUT" ) );
      return;
    }
    owner.error() << "could not connect to input " << m_input[m_curInput] << endmsg;
    m_curInput++;
  }
  throw IO::EndOfInput();
}

template <typename Owner>
void LHCb::MDF::FileReadInputHandler::readNextEventHeader( Owner& owner, LHCb::MDFHeader& header ) {
  StatusCode sc = m_ioMgr->read( &m_curConnection, (std::byte*)&header, sizeof( LHCb::MDFHeader ) );
  while ( !sc.isSuccess() ) {
    if ( m_curInput >= m_input.size() ) {
      // we got over all the input files, we got called by extra threads not yet aware
      throw IO::EndOfInput();
    }
    // No space left ? Or input error ? Anyway close current input
    owner.info() << fmt::format( "Over with input from {}, processed {}/{} files : ", m_input[m_curInput],
                                 m_curInput + 1, m_input.size() )
                 << endmsg;

    m_ioMgr->disconnect( &m_curConnection ).ignore();
    // and connect to next one
    m_curInput++;
    connectToCurrentInput( owner );
    if ( m_curInput >= m_input.size() ) { throw IO::EndOfInput(); }
    // try again to read header
    sc = m_ioMgr->read( &m_curConnection, (std::byte*)&header, sizeof( LHCb::MDFHeader ) );
  }
}

template <typename Owner>
void LHCb::MDF::FileReadInputHandler::skip( Owner& owner, unsigned int numEvt ) {
  LHCb::MDFHeader header;
  for ( unsigned int i = 0; i < numEvt; ++i ) {
    readNextEventHeader( owner, header );
    seek( header.recordSize() - sizeof( LHCb::MDFHeader ) );
  }
}
