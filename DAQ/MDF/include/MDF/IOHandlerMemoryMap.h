/***************************************************************************** \
* (c) Copyright 2000-2021 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once

#include <Event/RawEvent.h>
#include <Kernel/IOHandler.h>
#include <MDF/Buffer.h>
#include <MDF/MDFHeader.h>
#include <MDF/MMappedFile.h>
#include <MDF/RawDataConnection.h>

#include <GaudiKernel/GaudiException.h>
#include <GaudiKernel/MsgStream.h>

#include <mutex>
#include <sys/mman.h>

namespace LHCb::MDF {

  using SharedMMappedFile = std::shared_ptr<LHCb::MDF::MMappedFile>;
  using MMapBuffer        = LHCb::MDF::OwningBuffer<SharedMMappedFile>;
  struct MMapInputHandler;
  using IOHandlerMemoryMap = LHCb::IO::IOHandler<MMapBuffer, MMapInputHandler>;

  /**
   * helper class dealing with a set of input files and able to read/seek from them
   */
  struct MMapInputHandler {

    struct EndOfFile {};

    /// constructor
    template <typename Owner>
    MMapInputHandler( Owner&, std::vector<std::string> const& input ) : m_input( input ) {}

    /// initialize. Connects to first input already and thus may throw EndOfInput
    template <typename Owner>
    void initialize( Owner& owner, IIncidentSvc* incidentSvc ) {
      m_incidentSvc = incidentSvc;
      try {
        m_curMappedFile = connectToCurrentInput( owner );
        m_curPtr        = m_curMappedFile->data().data();
      } catch ( LHCb::IO::EndOfInput& e ) {
        throw GaudiException( "Empty input, can not load any event", "IOHandlerMemoryMap", StatusCode::FAILURE );
      }
    }

    void finalize() {}

    /**
     * rewinds to the begining of input
     */
    void rewind() { throw GaudiException{"IOHandlerMemoryMap", "Rewind not implemented", StatusCode::FAILURE}; }
    /**
     * skips next n events without reading them (only headers will be actually read)
     * @throws LHCb::IO::EndOfInput
     */
    template <typename Owner>
    void skip( Owner&, unsigned int );
    /**
     * skips current event, according to its size provided via header
     * @throws LHCb::IO::EndOfInput
     */
    void skip( LHCb::MDFHeader& header ) {
      m_curPtr += header.recordSize();
      if ( m_curPtr > m_curMappedFile->end() ) throw LHCb::IO::EndOfInput();
    }
    /**
     * returns pointer to next Event header
     * @throws LHCb::IO::EndOfInput, EndOfFile
     */
    template <typename Owner>
    LHCb::MDFHeader* readNextEventHeader( Owner& );
    /// accessor to currently mapped file
    SharedMMappedFile curMappedFile() { return m_curMappedFile; }

    /**
     * prefetches a number of events from input, allocating a Buffer object to store them and returning it
     * It will try to fill the buffer unless input is empty
     * @param bufferSize the size of the buffer to allocate
     */
    template <typename Owner>
    std::shared_ptr<MMapBuffer> prefetchEvents( Owner&, unsigned int nbEvents );

    /**
     * connects to current input and returns the mmapped buffer containing the data
     * @throws LHCb::IO::EndOfInput
     */
    template <typename Owner>
    SharedMMappedFile connectToCurrentInput( Owner& );

    /// Incident service. FIXME : drop whenever FSRSink is not depending on this anymore
    IIncidentSvc* m_incidentSvc;
    /// vector of input files
    std::vector<std::string> const m_input;
    /// currently used input as an index in m_input
    unsigned int m_curInput{0};
    /// current pointer in the mapped file
    std::byte* m_curPtr{nullptr};
    /// currently mapped file
    SharedMMappedFile m_curMappedFile;
  };

} // namespace LHCb::MDF

/**
 * prefetches events from current mapped file, up to the given number
 * in case the current mapped files does not contain enough events, only prefetches from that file anyway
 * @param bufferSize the size of the buffer to allocate
 */
template <typename Owner>
std::shared_ptr<LHCb::MDF::MMapBuffer> LHCb::MDF::MMapInputHandler::prefetchEvents( Owner&       owner,
                                                                                    unsigned int nbEvents ) {
  // prepare a vector to host events and reserve space
  std::vector<LHCb::MDF::MDFEvent> events;
  events.reserve( nbEvents );
  // get hold of current mapped files
  auto mmapBuffer = curMappedFile();
  // Fill buffer with banks while there is enough space and create associated events
  try {
    for ( unsigned int i = 0; i < nbEvents; i++ ) {
      auto* header = readNextEventHeader( owner );
      // skip event, may raise an EndOfInput
      skip( *header );
      // now let's build the event and copy the raw banks
      events.emplace_back( header, LHCb::span<std::byte>{(std::byte*)header->data(), header->size()},
                           header->compression() & 0xF );
    }
  } catch ( MMapInputHandler::EndOfFile& e ) {
    // we've reached the end of the current mapped file, stop here
  } catch ( LHCb::IO::EndOfInput& e ) {
    // we've reached the end of the input
    // if we have no data rethrow
    if ( events.size() == 0 ) throw e;
  }
  return std::make_shared<MMapBuffer>( std::move( mmapBuffer ), std::move( events ) );
}

template <typename Owner>
LHCb::MDF::SharedMMappedFile LHCb::MDF::MMapInputHandler::connectToCurrentInput( Owner& owner ) {
  while ( m_curInput < m_input.size() ) {
    try {
      return std::make_shared<LHCb::MDF::MMappedFile>( m_input[m_curInput], *m_incidentSvc );
    } catch ( GaudiException& error ) {
      owner.error() << error.tag() << " : " << error.message() << endmsg;
      m_curInput++;
    }
  }
  throw LHCb::IO::EndOfInput();
}

template <typename Owner>
LHCb::MDFHeader* LHCb::MDF::MMapInputHandler::readNextEventHeader( Owner& owner ) {
  // Check for end of file
  if ( m_curMappedFile->end() <= m_curPtr + sizeof( LHCb::MDFHeader ) ) {
    // End of the file reached
    if ( m_curInput < m_input.size() ) { owner.info() << "Over with input from " << m_input[m_curInput] << endmsg; }
    // Prepare next file for next call
    ++m_curInput;
    m_curMappedFile = connectToCurrentInput( owner );
    m_curPtr        = m_curMappedFile->data().data();
    // throw End of file exception
    throw EndOfFile();
  }
  // standard case, just return current position in file
  return reinterpret_cast<LHCb::MDFHeader*>( m_curPtr );
}

template <typename Owner>
void LHCb::MDF::MMapInputHandler::skip( Owner& owner, unsigned int numEvt ) {
  for ( unsigned int i = 0; i < numEvt; ++i ) { skip( *readNextEventHeader( owner ) ); }
}
