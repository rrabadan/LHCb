###############################################################################
# (c) Copyright 2000-2021 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
#[=======================================================================[.rst:
DAQ/MDF
-------
#]=======================================================================]

gaudi_add_library(MDFLib
    SOURCES
        src/StreamDescriptor.cpp
        src/MMappedFile.cpp
        src/Buffer.cpp
        src/RawDataConnection.cpp
        src/RawEventPrintout.cpp
    LINK
        PUBLIC
            Gaudi::GaudiKernel
            Gaudi::GaudiUtilsLib
            LHCb::DAQEventLib
            LHCb::LHCbAlgsLib
            LHCb::LHCbKernel
            LHCb::LHCbMathLib
)

gaudi_add_module(MDF
    SOURCES
        src/MDFIO.cpp
        src/MDFTest.cpp
        src/MDFWriter.cpp
        src/RawDataCnvSvc.cpp
        src/RawDataSelector.cpp
        src/StreamDescriptor.cpp
        components/IOAlgFileRead.cpp
        components/IOAlgMemoryMap.cpp
        components/Components.cpp
        components/MDFSelector.cpp
        components/RawEventTestDump.cpp
    LINK
        PUBLIC
        LHCb::DAQEventLib
        LHCb::LHCbAlgsLib
        Gaudi::GaudiKernel
        Gaudi::GaudiUtilsLib
        LHCb::LHCbKernel
        LHCb::LHCbMathLib
        LHCb::MDFLib
        PRIVATE
            ROOT::Core
            PkgConfig::zstd
)

gaudi_add_tests(QMTest)
