###############################################################################
# (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""Read a DST (ROOT file) which was produced from an MDF."""
import os
from PyConf.control_flow import CompositeNode
from PyConf.application import ApplicationOptions, configure_input, configure, default_raw_event
import GaudiPython as GP

options = ApplicationOptions(_enabled=False)
# Read the output of the dst_from_mdf test
options.input_files = [
    os.path.join(os.getenv("PREREQUISITE_0", ""), "output.dst")
]
options.set_conds_from_testfiledb("MiniBrunel_2018_MinBias_FTv4_MDF")
options.evt_max = 1000
options.input_type = 'ROOT'
options.gaudipython_mode = True

config = configure_input(options)

readEvent = default_raw_event("ALL")
cf_node = CompositeNode('Seq', children=[readEvent])
config.update(configure(options, cf_node))

appMgr = GP.AppMgr()
TES = appMgr.evtsvc()

for i in range(1000):
    appMgr.run(1)
    print(i)
    # Query the LHCb::RawEvent, which is likely to fail if it was corrupt in
    # the read/write cycle
    TES["/Event/DAQ/RawEvent"].banks(GP.gbl.LHCb.RawBank.DstData)
