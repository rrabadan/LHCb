/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "Event/RawEvent.h"
#include "LHCbAlgs/Consumer.h"
#include "MDF/RawEventPrintout.h"

#include <atomic>

namespace LHCb {

  /**
   *  Creates and fills dummy RawEvent
   *
   *  @author Markus Frank
   *  @date   2005-10-13
   */
  class RawEventTestDump : public Algorithm::Consumer<void( RawEvent const& )> {

    Gaudi::Property<bool> m_full{this, "FullDump", false, "If true, full bank contents are dumped"};
    Gaudi::Property<bool> m_dump{this, "DumpData", false, "If true, full bank contents are dumped"};
    Gaudi::Property<bool> m_check{this, "CheckData", false, "If true, full bank contents are checked"};
    Gaudi::Property<int>  m_debug{this, "Debug", 0, "Number of events where all dump flags should be considered true"};

    // event counter
    mutable std::atomic<int> m_numEvent{0};

  public:
    RawEventTestDump( const std::string& name, ISvcLocator* pSvcLocator )
        : Consumer{name, pSvcLocator, {{"RawLocation", RawEventLocation::Default}}} {}

    void operator()( RawEvent const& raw ) const override {
      MsgStream  info( msgSvc(), name() );
      bool       dmp = m_numEvent < m_debug || m_dump.value();
      bool       chk = m_numEvent < m_debug || m_check.value();
      bool       ful = m_numEvent < m_debug || m_full.value();
      static int evt = 0;
      ++evt;
      info << MSG::INFO;
      for ( RawBank::BankType i : RawBank::types() ) {
        const auto& b = raw.banks( i );
        int         cnt, inc = ( i == RawBank::Rich ) ? 64 : 32;
        const int*  p;
        if ( b.size() > 0 ) {
          if ( dmp ) {
            info << "Evt No:" << std::left << std::setw( 6 ) << evt << " has " << b.size() << " bank(s) of type " << i
                 << " (" << RawEventPrintout::bankType( i ) << ") " << endmsg;
          }
          int k = 0;
          for ( auto itB = b.begin(); itB != b.end(); itB++, k++ ) {
            const RawBank* r = *itB;
            if ( dmp ) { info << "Bank:  [" << RawEventPrintout::bankHeader( r ) << "] " << endmsg; }
            if ( ful ) {
              cnt = 0;
              std::stringstream s;
              for ( p = r->begin<int>(); p != r->end<int>(); ++p ) {
                s << std::hex << std::setw( 8 ) << std::hex << *p << " ";
                if ( ++cnt == 10 ) {
                  info << "  Data:" << s.str() << endmsg;
                  s.str( "" );
                  cnt = 0;
                }
              }
              if ( cnt > 0 ) info << "  Data:" << s.str() << endmsg;
            }
            if ( chk ) { // Check the patterns put in by RawEventCreator
              int kc = k;
              int ks = k + 1;
              if ( r->type() != RawBank::DAQ ) {
                if ( r->size() != inc * ks ) {
                  info << "Bad bank size:" << r->size() << " expected:" << ks * inc << endmsg;
                }
                if ( r->sourceID() != kc ) {
                  info << "Bad source ID:" << r->sourceID() << " expected:" << kc << endmsg;
                }
                for ( p = r->begin<int>(), cnt = 0; p != r->end<int>(); ++p, ++cnt ) {
                  if ( *p != cnt ) { info << "Bad BANK DATA:" << *p << endmsg; }
                }
                if ( cnt != ( inc * ks ) / int( sizeof( int ) ) ) {
                  info << "Bad amount of data in bank:" << cnt << " word" << endmsg;
                }
              }
            }
          }
        }
      }
      ++m_numEvent;
    }
  };
} // namespace LHCb

DECLARE_COMPONENT( LHCb::RawEventTestDump )
