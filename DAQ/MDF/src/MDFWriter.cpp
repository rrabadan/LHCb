/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#include "MDF/MDFHeader.h"
#include "MDF/MDFIO.h"
#include "MDF/RawDataConnection.h"
#include "TMD5.h"

#include "Event/RawEvent.h"

#include "GaudiKernel/Algorithm.h"
#include "GaudiKernel/IIncidentSvc.h"
#include "GaudiKernel/MsgStream.h"
#include "GaudiKernel/SmartDataPtr.h"
#include "GaudiKernel/StreamBuffer.h"
#include "GaudiUtils/IIODataManager.h"

#include <iomanip>
#include <memory>
#include <mutex>

namespace LHCb {

  /**
   * @author:  M.Frank
   * @version: 1.0
   */
  class MDFWriter : public Algorithm, protected MDFIO {
  public:
    typedef Gaudi::IDataConnection Connection;

  protected:
    ServiceHandle<Gaudi::IIODataManager> m_ioMgr{this, "DataManager", "Gaudi::IODataManager/IODataManager"};
    /// Stream descriptor (Initializes networking)
    std::unique_ptr<Connection>  m_connection{};
    Gaudi::Property<std::string> m_connectParams{this, "Connection", "", "Input parameters for connection parameters"};
    Gaudi::Property<int>         m_compress{this, "Compress", 2, "Compression algorithm identifier"};
    Gaudi::Property<std::string> m_compressAlg{this, "CompressAlg", "ZLIB", "File compression algorithm"};
    Gaudi::Property<int>         m_genChecksum{this, "ChecksumType", 1, "Flag to create checksum"};
    Gaudi::Property<bool>        m_genMD5{this, "GenerateMD5", true, "Flag to create MD5 checksum"};
    /// Streambuffer to hold uncompressed data
    StreamBuffer m_data;
    /// checksum object
    TMD5                         m_md5{};
    Gaudi::Property<std::string> m_bankLocation{this, "BankLocation", RawEventLocation::Default,
                                                "Location of the raw banks in the TES"};
    long long int                m_bytesWritten{0}; /// Bytes write count
    Gaudi::Property<int>         m_inputType{this, "InputDataType", MDFIO::MDF_NONE,
                                     "Input data type (like MDFIO::m_dataType)"};
    Gaudi::Property<int> m_addSpace{this, "AddSpace", 1, "additional dataspace to be used to add data [KBYTES]"};

    /** Allocate space for IO buffer
     * @param[in] ioDesc Output IO descriptor
     * @param[in] len    Total length of the data buffer
     *
     * @return  Pointer to allocated memory space
     */
    std::pair<char*, int> getDataSpace( void* const /* ioDesc */, size_t len ) override;

    /** Write byte buffer to output stream
     * @param[in] ioDesc Output IO descriptor
     * @param[in] data   Data buffer to be streamed
     * @param[in] len    Length of the data buffer
     *
     * @return  Status code indicating success or failure.
     */
    StatusCode writeBuffer( void* const ioDesc, const void* data, size_t len ) override;

    /// Additional dataspace in buffer [BYTES]
    int additionalSpace() const { return m_addSpace * 1024; }

  private:
    /// To enable multi-threading
    mutable std::mutex m_mutex;

  public:
    MDFWriter( const std::string& name, ISvcLocator* pSvcLocator );

    StatusCode initialize() override;
    StatusCode finalize() override;
    StatusCode execute() override;
  };
} // End namespace LHCb

DECLARE_COMPONENT( LHCb::MDFWriter )

LHCb::MDFWriter::MDFWriter( const std::string& nam, ISvcLocator* pSvc )
    : Algorithm( nam, pSvc ), MDFIO( MDFIO::MDF_RECORDS, nam ) {
  m_data.reserve( 1024 * 64 );
  declareProperty( "DataType", m_dataType ); // Output data type
  declareProperty( "ForceTAE", m_forceTAE = false );
  declareProperty( "IgnoreTAE", m_ignoreTAE = false );
}

/// Initialize
StatusCode LHCb::MDFWriter::initialize() {
  return Algorithm::initialize().andThen( [&] {
    MsgStream log( msgSvc(), name() );
    log << MSG::INFO << "Initialize MDFWriter" << endmsg;

    std::string con = getConnection( m_connectParams );
    m_connection    = std::make_unique<RawDataConnection>( this, con );
    m_ioMgr->connectWrite( m_connection.get(), Gaudi::IDataConnection::RECREATE, "MDF" ).ignore();
    if ( m_connection->isConnected() ) {
      log << MSG::INFO << "Received event request connection." << endmsg;
      return StatusCode::SUCCESS;
    } else {
      log << MSG::ERROR << "FAILED receiving event request connection." << endmsg;
      return StatusCode::FAILURE;
    }
  } );
}

/// Finalize
StatusCode LHCb::MDFWriter::finalize() {
  if ( m_genMD5.value() ) {
    MsgStream log( msgSvc(), name() );
    m_md5.Final();
    log << MSG::INFO << "Size:" << std::setw( 8 ) << m_bytesWritten << " Output:" << m_connectParams
        << " MD5 sum:" << m_md5.AsString() << endmsg;
  }
  serviceLocator()
      ->service( "IncidentSvc" )
      .as<IIncidentSvc>()
      ->fireIncident( Incident( m_connectParams, IncidentType::EndOutputFile ) );
  m_ioMgr->disconnect( m_connection.get() ).ignore();
  return Algorithm::finalize();
}

/// Allocate space for IO buffer
LHCb::MDFIO::MDFDescriptor LHCb::MDFWriter::getDataSpace( void* const /* ioDesc */, size_t len ) {
  m_data.reserve( len + additionalSpace() );
  return MDFDescriptor( m_data.data(), m_data.size() );
}

/// Execute procedure
StatusCode LHCb::MDFWriter::execute() {
  std::scoped_lock            lock( m_mutex );
  StatusCode                  sc = StatusCode::FAILURE;
  std::pair<const char*, int> data;
  setupMDFIO( msgSvc(), eventSvc() );

  switch ( m_inputType.value() ) {
  case MDFIO::MDF_NONE:
    sc = commitRawBanks( m_compressAlg, m_compress, m_genChecksum, m_connection.get(), m_bankLocation );
    break;
  case MDFIO::MDF_BANKS:
    data = getDataFromAddress();
    if ( data.first ) {
      RawBank* b = (RawBank*)data.first;
      switch ( m_dataType ) {
      case MDFIO::MDF_RECORDS:
        sc = writeBuffer( m_connection.get(), b->data(), data.second - b->hdrSize() );
        sc.isSuccess() ? ++m_writeActions : ++m_writeErrors;
        break;
      case MDFIO::MDF_BANKS:
        sc = writeBuffer( m_connection.get(), data.first, data.second );
        sc.isSuccess() ? ++m_writeActions : ++m_writeErrors;
        break;
      default:
        break;
      }
    }
    break;
  case MDFIO::MDF_RECORDS:
    data = getDataFromAddress();
    if ( data.first ) {
      switch ( m_dataType ) {
      case MDFIO::MDF_RECORDS:
        sc = writeBuffer( m_connection.get(), data.first, data.second );
        sc.isSuccess() ? ++m_writeActions : ++m_writeErrors;
        break;
      case MDFIO::MDF_BANKS: {
        MDFHeader* h = (MDFHeader*)data.first;
        m_data.reserve( data.second + 100 * sizeof( int ) );
        RawBank* b   = (RawBank*)m_data.data();
        size_t   len = sizeof( MDFHeader ) + h->subheaderLength();
        b->setMagic();
        b->setType( RawBank::DAQ );
        b->setSize( len );
        b->setVersion( DAQ_STATUS_BANK );
        b->setSourceID( 0 );
        ::memcpy( b->data(), data.first, data.second );
        sc = writeBuffer( m_connection.get(), m_data.data(), data.second + b->hdrSize() );
        sc.isSuccess() ? ++m_writeActions : ++m_writeErrors;
        break;
      }
      default:
        break;
      }
    }
    break;
  default:
    break;
  }
  if ( sc.isSuccess() )
    serviceLocator()
        ->service( "IncidentSvc" )
        .as<IIncidentSvc>()
        ->fireIncident( Incident( m_connectParams, IncidentType::WroteToOutputFile ) );
  return sc;
}

/// Write byte buffer to output stream
StatusCode LHCb::MDFWriter::writeBuffer( void* const ioDesc, const void* data, size_t len ) {
  Connection* c  = (Connection*)ioDesc;
  StatusCode  sc = m_ioMgr->write( c, data, len );
  if ( sc.isSuccess() ) {
    m_bytesWritten += len;
    if ( m_genMD5.value() ) { m_md5.Update( (const unsigned char*)data, len ); }
  }
  return sc;
}
