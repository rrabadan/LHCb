/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "MDF/RawDataConnection.h"
#include "MDF/StreamDescriptor.h"
#include <cstring>
#include <iostream>
#include <zstd.h>
using namespace LHCb;
using namespace Gaudi;

/// Standard constructor
RawDataConnection::RawDataConnection( const IInterface* owner, const std::string& fname )
    : IDataConnection( owner, fname ) { //               01234567890123456789012345678901234567890
  // Check if FID: A82A3BD8-7ECB-DC11-8DC0-000423D950B0
  if ( fname.length() == 36 && fname[8] == '-' && fname[13] == '-' && fname[18] == '-' && fname[23] == '-' ) {
    m_name = "FID:" + fname;
  }
  m_age         = 0;
  m_bind.ioDesc = m_access.ioDesc = 0;
}

/// Standard destructor
RawDataConnection::~RawDataConnection() {}

StatusCode RawDataConnection::connectRead() {
  m_state = DataConnectionState::UNKNOWN;
  m_bind  = StreamDescriptor::bind( m_pfn );
  if ( m_bind.ioDesc > 0 ) {
    m_access = StreamDescriptor::accept( m_bind );
    if ( m_access.ioDesc != 0 ) {
      resetAge();
      return StatusCode::SUCCESS;
    }
    StreamDescriptor::close( m_bind );
  }
  return StatusCode::FAILURE;
}

StatusCode RawDataConnection::connectWrite( IoType typ ) {
  // Only support writing uncompressed data
  m_state = DataConnectionState::UNCOMPRESSED;
  switch ( typ ) {
  case CREATE:
  case RECREATE:
    resetAge();
    m_access = StreamDescriptor::connect( m_pfn );
    return m_access.ioDesc == 0 ? StatusCode::FAILURE : StatusCode::SUCCESS;
  default:
    break;
  }
  return StatusCode::FAILURE;
}

StatusCode RawDataConnection::read( void* const data, size_t len ) {
  resetAge();

  int    sc;
  size_t offset = 0;
  if ( m_state == DataConnectionState::UNKNOWN ) {

    offset = sizeof( uint32_t );
    if ( len < offset ) {
      std::cerr << "Cannot read fewer than the first 4 bytes of an MDF file as auto detection will fail" << std::endl;
      // log << MSG::FATAL << "Cannot read fewer than the first 4 bytes of an MDF file as auto detection will fail" <<
      // endmsg;
      return StatusCode::FAILURE;
    }

    // Read the first 4 bytes to check if the file is compressed with ZSTD
    uint32_t header;
    sc = StreamDescriptor::read( m_access, &header, offset );
    if ( sc != 1 ) return StatusCode( sc );

    // Copy the header into the data buffer if available
    if ( data ) { memcpy( data, &header, sizeof( header ) ); }

    if ( header == ZSTD_MAGICNUMBER ) {
      m_state = DataConnectionState::COMPRESSED_ZSTD;
      offset  = 0;
      m_buffIn.resize( ZSTD_DStreamInSize() );
      m_buffOut.resize( ZSTD_DStreamOutSize() );

      m_dstream = std::unique_ptr<ZSTD_DStream, decltype( &ZSTD_freeDCtx )>( ZSTD_createDCtx(), ZSTD_freeDCtx );
      if ( !m_dstream ) {
        std::cerr << "Failed to create ZSTD context" << std::endl;
        return StatusCode::FAILURE;
      }
      m_output_pos = 0;
      m_output     = {m_buffOut.data(), m_buffOut.size(), 0};

      memcpy( m_buffIn.data(), &header, sizeof( uint32_t ) );
      // check current position
      auto previousPos = StreamDescriptor::seek( m_access, 0, SEEK_CUR );
      sc               = StreamDescriptor::read( m_access, m_buffIn.data() + sizeof( uint32_t ),
                                   m_buffIn.size() - sizeof( uint32_t ) );
      if ( sc != 1 ) return StatusCode( sc );
      // Find number of bytes read (including 4 magic bytes)
      size_t read = StreamDescriptor::seek( m_access, 0, SEEK_CUR ) - previousPos + sizeof( uint32_t );
      m_input     = {m_buffIn.data(), read, 0};

    } else {
      m_state = DataConnectionState::UNCOMPRESSED;
    }
  }

  switch ( m_state ) {
  case DataConnectionState::COMPRESSED_ZSTD:
    while ( true ) {
      // we should enter in 3 cases :
      //  - more input is available
      //  - more data is available in the output buffer, even if input has been fully consummed
      //  - more data might be be available in the internal buffer of the zstd stream. ie last decompression of
      //    last bit of input did not fit completely in the output buffer
      while ( m_input.pos < m_input.size || m_output.pos > m_output_pos || m_output.pos == m_output.size ) {
        // Do we need to decompress more ? Yes if output buffer is fully consumed and
        //   - either last decompression did not fit into it at the end of the input
        //   - or more input is available
        if ( m_output_pos == m_output.pos && ( m_output.pos == m_output.size || m_input.pos < m_input.size ) ) {
          m_output         = {m_buffOut.data(), m_buffOut.size(), 0};
          m_output_pos     = 0;
          size_t const err = ZSTD_decompressStream( m_dstream.get(), &m_output, &m_input );
          if ( ZSTD_isError( err ) ) {
            std::cerr << "zstd decompression error: " << ZSTD_getErrorName( err ) << std::endl;
            return StatusCode::FAILURE;
          }
        }
        size_t available_bytes = m_output.pos - m_output_pos;
        size_t requested_bytes = len - offset;
        size_t to_copy         = requested_bytes > available_bytes ? available_bytes : requested_bytes;
        if ( data ) { memcpy( static_cast<char*>( data ) + offset, m_buffOut.data() + m_output_pos, to_copy ); }
        m_output_pos += to_copy;
        offset += to_copy;
        if ( offset == len ) { return StatusCode::SUCCESS; }
      }
      // check current position
      auto previousPos = StreamDescriptor::seek( m_access, 0, SEEK_CUR );
      sc               = StreamDescriptor::read( m_access, m_buffIn.data(), m_buffIn.size() );
      // Find number of bytes read
      size_t read = StreamDescriptor::seek( m_access, 0, SEEK_CUR ) - previousPos;
      // Read was successful if either we get 1 or 0 and something was read (case of end of file)
      if ( !( sc == 1 || ( sc == 0 && read > 0 ) ) ) return StatusCode( sc );
      m_input = {m_buffIn.data(), read, 0};
    }

  case DataConnectionState::UNCOMPRESSED:
    if ( data ) {
      sc = StreamDescriptor::read( m_access, static_cast<char*>( data ) + offset, len - offset );
      return sc == 1 ? StatusCode::SUCCESS : StatusCode( sc );
    } else {
      // no output buffer, prefer a seek
      auto seekResult = StreamDescriptor::seek( m_access, len - offset, SEEK_CUR );
      return seekResult >= 0 ? StatusCode::SUCCESS : StatusCode::FAILURE;
    }

  default:
    std::cerr << "Unknown state in RawDataConnection::read" << std::endl;
    return StatusCode::FAILURE;
  }
}

/// Write raw byte buffer to output stream
StatusCode RawDataConnection::write( const void* data, int len ) {
  resetAge();
  return StreamDescriptor::write( m_access, data, len ) ? StatusCode::SUCCESS : StatusCode::FAILURE;
}

/// Seek on the file described by ioDesc. Arguments as in ::seek()
long long int RawDataConnection::seek( long long int where, int origin ) {
  resetAge();
  if ( m_state == DataConnectionState::UNKNOWN && where != 0 ) {
    if ( origin != SEEK_CUR || where < static_cast<long long>( sizeof( uint32_t ) ) )
      throw std::runtime_error( "Cannot seek less than 4 bytes within unknown files" );
    // Read the first 4 bytes to check if the file is compressed and set m_state
    uint32_t   header;
    StatusCode sc = read( &header, sizeof( uint32_t ) );
    if ( !sc.isSuccess() ) return -1;
    where -= sizeof( uint32_t );
  }
  if ( m_state == DataConnectionState::COMPRESSED_ZSTD && ( where != 0 || origin != SEEK_CUR ) ) {
    if ( origin == SEEK_CUR ) {
      // use read with nullptr for the output buffer
      StatusCode sc = read( nullptr, where );
      if ( !sc.isSuccess() ) return -1;
      where = 0;
    } else {
      throw std::runtime_error( "Seeking only supports SEEK_CUR for compressed files" );
    }
  }
  return StreamDescriptor::seek( m_access, where, origin );
}

StatusCode RawDataConnection::disconnect() {
  m_state = DataConnectionState::UNKNOWN;
  if ( m_access.ioDesc != 0 ) StreamDescriptor::close( m_access );
  if ( m_bind.ioDesc != 0 ) StreamDescriptor::close( m_bind );
  m_bind.ioDesc = m_access.ioDesc = 0;
  return StatusCode::SUCCESS;
}
