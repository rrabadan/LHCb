###############################################################################
# (c) Copyright 2000-2024 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from PyConf.control_flow import CompositeNode
from PyConf.application import ApplicationOptions, configure_input, configure, input_from_root_file
from PyConf.Algorithms import RawEventDump

options = ApplicationOptions(_enabled=False)
options.set_input_and_conds_from_testfiledb("S20r0p2_stripped_test")
options.evt_max = 1

config = configure_input(options)

eventDump = RawEventDump(
    name="RawEventDump",
    DumpData=True,
    RawEventLocations=[
        input_from_root_file("DAQ/RawEvent", forced_type='LHCb::RawEvent'),
        input_from_root_file("Trigger/RawEvent", forced_type='LHCb::RawEvent'),
        input_from_root_file("Rich/RawEvent", forced_type='LHCb::RawEvent'),
        input_from_root_file("Calo/RawEvent", forced_type='LHCb::RawEvent'),
        input_from_root_file("Muon/RawEvent", forced_type='LHCb::RawEvent'),
        input_from_root_file("Other/RawEvent", forced_type='LHCb::RawEvent'),
        input_from_root_file("pRec/RawEvent", forced_type='LHCb::RawEvent'),
        input_from_root_file("Emu/RawEvent", forced_type='LHCb::RawEvent')
    ])

cf_node = CompositeNode('Seq', children=[eventDump])
config.update(configure(options, cf_node))
