/*****************************************************************************\
* (c) Copyright 2000-2022 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "Event/RawBank.h"
#include "Event/RawEvent.h"
#include "GaudiKernel/ParsersFactory.h"
#include "LHCbAlgs/SplittingMergingTransformer.h"
#include <algorithm>
#include <vector>

namespace {
  template <typename K, typename V>
  bool are_keys_unique( const std::vector<std::pair<K, V>>& kv ) {
    std::vector<K> keys;
    keys.reserve( kv.size() );
    std::transform( kv.begin(), kv.end(), std::back_inserter( keys ), []( const auto& i ) { return i.first; } );
    std::sort( keys.begin(), keys.end() );
    return std::adjacent_find( keys.begin(), keys.end() ) == keys.end();
  }
} // namespace

namespace LHCb {

  using RawBankVector = std::vector<RawBank const*>;

  StatusCode parse( std::vector<std::pair<RawBank::BankType, int>>& result, const std::string& input ) {
    using Gaudi::Parsers::parse;
    std::vector<std::string> vs;
    return parse( vs, input ).andThen( [&] {
      std::vector<std::pair<RawBank::BankType, int>> vbti;
      for ( auto const& s : vs ) {
        std::pair<std::string, int> psi;
        auto                        sc = parse( psi, s ).andThen( [&] {
          RawBank::BankType bt;
          return parse( bt, psi.first ).andThen( [&] { vbti.emplace_back( bt, psi.second ); } );
        } );
        if ( sc.isFailure() ) return sc;
      }
      result = vbti;
      return StatusCode{StatusCode::SUCCESS};
    } );
  }

  class UnpackRawEvents : public LHCb::Algorithm::SplittingMergingTransformer<
                              std::vector<RawBankVector>( const LHCb::Algorithm::vector_of_const_<RawEvent>& ),
                              LHCb::Algorithm::Traits::writeViewFor<RawBankVector, RawBank::View>> {
    Gaudi::Property<std::vector<std::pair<RawBank::BankType, int>>> m_types{
        this, "BankTypeIndices", {}, [this]( auto const& ) {
          // make sure that the given types are unique
          if ( !are_keys_unique( m_types.value() ) ) {
            throw GaudiException{"UnpackRawEvents::BankTypeIndices", "Specified types must be unique",
                                 StatusCode::FAILURE};
          }
        }};

  public:
    UnpackRawEvents( const std::string& name, ISvcLocator* locator )
        : SplittingMergingTransformer{name, locator, {"RawEventLocations", {}}, {"RawBankLocations", {}}} {}

    std::vector<RawBankVector> operator()( const LHCb::Algorithm::vector_of_const_<RawEvent>& evts ) const override {
      std::vector<RawBankVector> banks;
      assert( m_types.size() == outputLocationSize() ); // make sure we were configured correctly
      banks.reserve( m_types.size() );
      std::transform( m_types.begin(), m_types.end(), std::back_inserter( banks ),
                      [&evts]( std::pair<RawBank::BankType, int> const& map ) {
                        const auto& bnks = evts.at( map.second ).banks( map.first );
                        return RawBankVector{bnks.begin(), bnks.end()};
                      } );
      if ( msgLevel( MSG::DEBUG ) ) {
        for ( auto b : banks ) debug() << "created views for " << b.size() << " bank types " << endmsg;
      }
      return banks;
    }
  };

  DECLARE_COMPONENT( UnpackRawEvents )
} // namespace LHCb
