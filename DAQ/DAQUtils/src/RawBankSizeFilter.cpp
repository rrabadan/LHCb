/*****************************************************************************\
* (c) Copyright 2023 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "Event/RawBank.h"
#include "LHCbAlgs/FilterPredicate.h"
#include <numeric>
/** @class RawBankSizeFilter
 *
 *  Checks content of a raw bank
 *  Filter fails if the bank is empty
 */
class RawBankSizeFilter final : public LHCb::Algorithm::FilterPredicate<bool( const LHCb::RawBank::View& )> {

public:
  /// Standard constructor
  RawBankSizeFilter( const std::string& name, ISvcLocator* pSvcLocator )
      : FilterPredicate{name, pSvcLocator, KeyValue{"RawBankLocation", ""}} {}

  bool operator()( const LHCb::RawBank::View& view ) const override {

    if ( view.empty() ) {
      if ( msgLevel( MSG::DEBUG ) ) {
        debug() << "input location " << inputLocation() << " has no DstData banks " << endmsg;
      }
      ++m_emptyBanks;
      return false;
    }

    std::size_t total_size =
        std::accumulate( view.begin(), view.end(), std::size_t( 0 ), []( std::size_t sz, const LHCb::RawBank* b ) {
          if ( b ) sz += b->size();
          return sz;
        } );

    if ( msgLevel( MSG::DEBUG ) ) {
      const auto* rawBank0 = view.front();
      if ( rawBank0 )
        debug() << " bank type " << rawBank0->type() << " in " << inputLocation() << " has total size of " << total_size
                << endmsg;
    }

    bool acc = ( total_size > m_minsize );
    m_acc += acc;
    return acc;
  }

private:
  Gaudi::Property<std::size_t>                              m_minsize{this, "MinRawBankSize", 0};
  mutable Gaudi::Accumulators::SummingCounter<unsigned int> m_emptyBanks{this, "Number of empty raw banks"};
  mutable Gaudi::Accumulators::BinomialCounter<>            m_acc{this, "Number of too small raw banks size"};
};

//=============================================================================
// Declaration of the Algorithm Factory
DECLARE_COMPONENT( RawBankSizeFilter )
//=============================================================================
