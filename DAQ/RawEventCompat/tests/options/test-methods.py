from __future__ import print_function
###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
import RawEventCompat
import RawEventCompat.Configuration as conf

test_locations = {
    0.0: {
        "Bank_A": "/Foo",
        "Bank_B": "/Bar",
        "Bank_C": "FooBar/#TCK#/"
    },
    99.0: {
        "Bank_A": "FooBar",
        "Bank_B": ["FooBar", "Spam"],
        "Bank_C": "FooBar"
    },
    7.0: {
        "Bank_A": "FooBar",
        "Bank_B": ["FooBar", "Spam"],
        "Bank_D": "FooBar",
        "Bank_E": "FooBar"
    }
}

test_versions = {"Spam": 0.0, "Eggs": 99.0}

conf.RawEventFormatConf().Locations = test_locations

conf.RawEventFormatConf().RecoDict = test_versions

loc, reco = conf._getDict()

if loc != test_locations or reco != test_versions:
    raise ValueError("Couldn't set up custom dictionaries")

for version in test_versions:
    conf._checkv(version)

for version in test_locations:
    conf._checkv(version)

for version in test_versions:
    revdict = conf.ReverseDict(version)
    for bank in test_locations[test_versions[version]]:
        locs = test_locations[test_versions[version]][bank]
        if type(locs) is not list:
            locs = [locs]
        for aloc in locs:
            if aloc not in revdict:
                print(revdict)
                raise KeyError("ReverseDict doesn't work as expected")

print("Pass")
