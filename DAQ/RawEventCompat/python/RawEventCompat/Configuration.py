###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""Python module for manipulating the raw event splitting and combining

This module defines both functions and configurables, such that the user can decide which is best.

Uses dictionaries from RawEventFormat, which is in DBASE

"""
from __future__ import print_function
from Gaudi.Configuration import *


####################################################
# Helpers to avoid code duplications
####################################################
def _checkv(version, locations=None, recodict=None):
    "Check the version exists, return the numeric version"
    locations, recodict = _getDict(locations, recodict)

    if type(version) is str:
        if version not in recodict:
            raise KeyError("the chosen reconstruction pass is not known " +
                           version)
        else:
            version = recodict[version]

    if version not in locations:
        raise KeyError("the chosen version is not known " + str(version))

    return version


def _getDict(locations=None, recodict=None):
    "Find the latest dictionaries, using the configurable"
    #find the latest dictionaries
    if locations is None:
        if not RawEventFormatConf().isPropertySet("Locations"):
            raise ValueError(
                "RawEventFormatConf Locations not yet specified.To force the loading from a dictionary use RawEventFormatConf().forceLoad()"
            )
        locations = RawEventFormatConf().getProp("Locations")
    if recodict is None:
        if not RawEventFormatConf().isPropertySet("RecoDict"):
            raise ValueError(
                "RawEventFormatConf RecoDict not yet specified.To force the loading from a dictionary use RawEventFormatConf().forceLoad()"
            )
        recodict = RawEventFormatConf().getProp("RecoDict")
    return locations, recodict


####################################################
# Simple python functions
####################################################


def ReverseDict(version, locations=None, recodict=None):
    """
    A simple method to return a reversed look up of the location dictionary
    version : which version to look up
    locations: the locations dictionary passed
    """
    #find dictionaries
    locations, recodict = _getDict(locations, recodict)
    #check the options
    version = _checkv(version, locations, recodict)

    reversed = {}
    for key in locations[version]:
        #if it's a list, treat as a search path, add both
        loc = locations[version][key]
        if type(loc) is not list:
            loc = [loc]
        for aloc in loc:
            if aloc in reversed:
                reversed[aloc].append(key)
            else:
                reversed[aloc] = [key]

    return reversed


####################################################
# Lowest level configurable, interrogates the DB
####################################################
class RawEventFormatConf(ConfigurableUser):
    "A simple configurable to hold the dictionaries of which event locations are stored where. A configurable is one way to do this, allowing users to modify things, or maybe some python shelved dictionary with a method to get at it"
    __slots__ = {
        "Locations": None  #which locations have been where
        ,
        "RecoDict": None  #which reco version goes to which RawEvent version
        ,
        "TCK": None,
        "TCKReplacePattern": "#TCK#",
        "GenericReplacePatterns": {}
    }
    _propertyDocDct = {
        "Locations":
        "which banks have been where, loaded from a databse in RawEventFormat in DBASE.",
        "RecoDict":
        "which reco version goes to which RawEvent version, loaded from a database in RawEventFormat in DBASE.",
        "TCK":
        "A TCK with which to replace any TCK strings",
        "TCKReplacePattern":
        "#TCK#",
        "GenericReplacePatterns":
        "A dictionary to use to replace other strings{}"
    }

    def forceLoad(self):
        """
        Force to get the dictionary from the database when called
        """
        try:
            import RawEventFormat
        except ImportError:
            raise ImportError(
                "No RawEventFormat module found, needed for RawEventFormatConf, unless you specify the raw event location dictionaries explicitly"
            )
        self.setProp("Locations", RawEventFormat.Raw_location_db)
        self.setProp("RecoDict", RawEventFormat.Reco_dict_db)

    def loadIfRequired(self):
        """
        Only take the dictionary from the DB if it hasn't been explicitly set
        """
        if self.isPropertySet("Locations") and self.isPropertySet("RecoDict"):
            return
        try:
            import RawEventFormat
        except ImportError:
            raise ImportError(
                "No RawEventFormat module found, needed for RawEventFormatConf, unless you specify the raw event location dictionaries explicitly"
            )
        if not self.isPropertySet("Locations"):
            self.setProp("Locations", RawEventFormat.Raw_location_db)
        if not self.isPropertySet("RecoDict"):
            self.setProp("RecoDict", RawEventFormat.Reco_dict_db)

    def __apply_configuration__(self):
        #don't do anything if explicitly configured
        self.loadIfRequired()
