###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from PyConf.application import (configure_input, configure, ApplicationOptions,
                                default_raw_banks)
from PyConf.control_flow import CompositeNode, NodeLogic
from PyConf.Algorithms import VPRetinaFullClusterDecoder

options = ApplicationOptions(_enabled=False)
config = configure_input(options)

decoder = VPRetinaFullClusterDecoder(
    RawBanks=default_raw_banks("VPRetinaCluster"))

cf_node = CompositeNode(
    "retina_decoding", [decoder],
    combine_logic=NodeLogic.LAZY_AND,
    force_order=True)

config.update(configure(options, cf_node))
