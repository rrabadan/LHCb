/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "VPRetinaTopologyID.h"
#include <algorithm>
#include <array>
#include <cstdint>
#include <iostream>
#include <tuple>
#include <vector>

// constructor
VPRetinaTopologyID::VPRetinaTopologyID() {}

// one-line helper to check if a bit is active
namespace {
  template <unsigned bit>
  constexpr bool test_bit( unsigned int i ) {
    return i & ( 0x1u << bit );
  }
} // namespace

// one-line helpers to check cluster position
namespace {
  template <unsigned... bits>
  constexpr bool require_unset_bits( unsigned i ) {
    return ( ( !test_bit<bits>( i ) ) && ... );
  }

  constexpr bool bottom_right_isolated( unsigned i ) { return require_unset_bits<3, 4, 7>( i ); }
  constexpr bool central_column_all_zero( unsigned i ) { return require_unset_bits<3, 4, 5>( i ); }
  constexpr bool central_row_all_zero( unsigned i ) { return require_unset_bits<1, 4, 7>( i ); }
  constexpr bool top_right_isolated( unsigned i ) { return require_unset_bits<4, 5, 7>( i ); }
  constexpr bool top_left_isolated( unsigned i ) { return require_unset_bits<1, 4, 5>( i ); }
  constexpr bool bottom_left_isolated( unsigned i ) { return require_unset_bits<1, 3, 4>( i ); }
} // namespace

// function that returns true if 3x3 straight topology corresponds to an actual cluster
bool VPRetinaTopologyID::is3x3cluster( unsigned int topo3x3 ) {
  return test_bit<0>( topo3x3 ) || ( test_bit<1>( topo3x3 ) && test_bit<3>( topo3x3 ) );
}

// function that returns true if 3x3 reversed topology corresponds to an actual cluster
bool VPRetinaTopologyID::is3x3revcluster( unsigned int topo3x3 ) {
  return test_bit<6>( topo3x3 ) || ( test_bit<3>( topo3x3 ) && test_bit<7>( topo3x3 ) );
}

// function that returns true if 2x4 topology corresponds to an actual cluster
bool VPRetinaTopologyID::is2x4cluster( unsigned int topo2x4 ) {
  if ( topo2x4 != 0 ) {
    return true;
  } else {
    return false;
  }
}

// compute the fractional coordinates from a 3x3 cluster topology
std::tuple<int, int> VPRetinaTopologyID::frac3x3cluster( unsigned int topo3x3 ) {
  uint32_t shift_col = 0;
  uint32_t shift_row = 0;
  uint32_t n         = 0;

  for ( unsigned int iiX = 0; iiX < 3; ++iiX ) {
    for ( unsigned int iiY = 0; iiY < 3; ++iiY ) {
      if ( topo3x3 & ( 1 << ( iiX * 3 + iiY ) ) ) {
        shift_col += iiX;
        shift_row += iiY;
        n++;
      }
    }
  }
  return std::make_tuple( ( uint64_t )( ( ( shift_row << 2 ) + n / 2 ) / n ),
                          ( uint64_t )( ( ( shift_col << 2 ) + n / 2 ) / n ) );
}

// compute the fractional coordinates from a 2x4 cluster topology
std::tuple<int, int> VPRetinaTopologyID::frac2x4cluster( unsigned int topo2x4 ) {
  uint32_t shift_col = 0;
  uint32_t shift_row = 0;
  uint32_t n         = 0;

  for ( unsigned int iiX = 0; iiX < 2; ++iiX ) {
    for ( unsigned int iiY = 0; iiY < 4; ++iiY ) {
      if ( topo2x4 & ( 1 << ( iiX * 4 + iiY ) ) ) {
        shift_col += iiX;
        shift_row += iiY;
        n++;
      }
    }
  }
  return std::make_tuple( ( uint64_t )( ( ( shift_row << 2 ) + n / 2 ) / n ),
                          ( uint64_t )( ( ( shift_col << 2 ) + n / 2 ) / n ) );
}

// compute the equivalent 3x3 straight cluster topology
uint32_t VPRetinaTopologyID::equivalent_topo3x3( unsigned int topo3x3 ) {
  if ( bottom_left_isolated( topo3x3 ) ) {
    return topo3x3 & 1;
  } else if ( central_column_all_zero( topo3x3 ) ) {
    return topo3x3 & ( 1 + 2 + 4 );
  } else if ( central_row_all_zero( topo3x3 ) ) {
    return topo3x3 & ( 1 + 8 + 64 );
  } else if ( top_right_isolated( topo3x3 ) ) {
    return topo3x3 & ( 1 + 2 + 4 + 8 + 64 );
  } else if ( top_left_isolated( topo3x3 ) ) {
    return topo3x3 & ( 1 + 8 + 64 + 128 + 256 );
  } else if ( bottom_right_isolated( topo3x3 ) ) {
    return topo3x3 & ( 1 + 2 + 4 + 32 + 256 );
  } else {
    return topo3x3;
  }
}

// compute the equivalent 3x3 reversed cluster topology
uint32_t VPRetinaTopologyID::equivalent_topo3x3_rev( unsigned int topo3x3 ) {
  if ( bottom_right_isolated( topo3x3 ) ) {
    return topo3x3 & 0x40;
  } else if ( central_column_all_zero( topo3x3 ) ) {
    return topo3x3 & ( 0x40 + 0x80 + 0x100 );
  } else if ( central_row_all_zero( topo3x3 ) ) {
    return topo3x3 & ( 0x1 + 0x8 + 0x40 );
  } else if ( top_right_isolated( topo3x3 ) ) {
    return topo3x3 & ( 0x1 + 0x2 + 0x4 + 0x8 + 0x40 );
  } else if ( top_left_isolated( topo3x3 ) ) {
    return topo3x3 & ( 0x1 + 0x8 + 0x40 + 0x80 + 0x100 );
  } else if ( bottom_left_isolated( topo3x3 ) ) {
    return topo3x3 & ( 0x4 + 0x20 + 0x40 + 0x80 + 0x100 );
  } else {
    return topo3x3;
  }
}

// break down an isolated SP into its two components: if the SP contains a single cluster, the SP itself and a null SP
// are returned. If the SP contains two clusters, two SPs are returned containg one cluster each.
std::tuple<uint32_t, uint32_t> VPRetinaTopologyID::breakdown_topo2x4( unsigned int topo2x4 ) {
  uint32_t SP0;
  uint32_t SP1;
  if ( ( ( topo2x4 & 2 ) == 0 ) && ( ( topo2x4 & 32 ) == 0 ) ) {
    SP0 = topo2x4 & ( 1 + 16 );
    SP1 = topo2x4 & ( 4 + 8 + 64 + 128 );
  } else if ( ( ( topo2x4 & 4 ) == 0 ) && ( ( topo2x4 & 64 ) == 0 ) ) {
    SP0 = topo2x4 & ( 1 + 2 + 16 + 32 );
    SP1 = topo2x4 & ( 8 + 128 );
  } else {
    SP0 = topo2x4;
    SP1 = 0;
  }

  if ( SP0 != 0 ) {
    return std::make_tuple( SP0, SP1 );
  } else {
    return std::make_tuple( SP1, 0 );
  }
}

//============================================================================================================================
// Function to compute the cluster topology ID and the coordinate fractional parts from the full 3x3 straight cluster
// topology
//============================================================================================================================

std::array<uint32_t, 512> VPRetinaTopologyID::topo3x3_to_topoIDfrac() {
  std::array<uint32_t, 512> vec_topo3x3{};
  std::array<uint32_t, 512> vec_topo3x3eqv{};
  std::array<uint32_t, 512> fracrow_clu3x3{};
  std::array<uint32_t, 512> fraccol_clu3x3{};
  std::array<uint32_t, 512> vec_topoID3x3{};
  std::array<uint32_t, 512> vec_topoIDfrac3x3{};
  std::array<uint32_t, 16>  cnt3x3{};

  for ( unsigned int topo3x3 = 0; topo3x3 < 512; ++topo3x3 ) {
    vec_topo3x3[topo3x3] = topo3x3;
    if ( is3x3cluster( topo3x3 ) ) {
      vec_topo3x3eqv[topo3x3] = equivalent_topo3x3( topo3x3 );
      fracrow_clu3x3[topo3x3] = ( std::get<0>( frac3x3cluster( equivalent_topo3x3( topo3x3 ) ) ) ) % 4;
      fraccol_clu3x3[topo3x3] = ( std::get<1>( frac3x3cluster( equivalent_topo3x3( topo3x3 ) ) ) ) % 4;
      if ( topo3x3 == vec_topo3x3eqv[topo3x3] ) {
        vec_topoID3x3[topo3x3] = cnt3x3[( fracrow_clu3x3[topo3x3] % 4 ) * 4 + ( fraccol_clu3x3[topo3x3] % 4 )];
        cnt3x3[( fracrow_clu3x3[topo3x3] % 4 ) * 4 + ( fraccol_clu3x3[topo3x3] % 4 )] += 1;
      } else {
        vec_topoID3x3[topo3x3] = vec_topoID3x3[vec_topo3x3eqv[topo3x3]];
      }
    } else {
      vec_topo3x3eqv[topo3x3] = 0;
      fracrow_clu3x3[topo3x3] = 0;
      fraccol_clu3x3[topo3x3] = 0;
      vec_topoID3x3[topo3x3]  = 31;
    }
  }

  for ( unsigned int topo3x3 = 0; topo3x3 < 512; ++topo3x3 ) {
    vec_topoIDfrac3x3[topo3x3] =
        ( vec_topoID3x3[topo3x3] << 4 ) | ( fraccol_clu3x3[topo3x3] << 2 ) | ( fracrow_clu3x3[topo3x3] );
  }

  return vec_topoIDfrac3x3;
}

//============================================================================================================================
// Function to compute the cluster topology ID and the coordinate fractional parts from the full 3x3 reversed cluster
// topology
//============================================================================================================================

std::array<uint32_t, 512> VPRetinaTopologyID::topo3x3_rev_to_topoIDfrac() {
  std::array<uint32_t, 512> vec_topo3x3{};
  std::array<uint32_t, 512> vec_topo3x3eqv{};
  std::array<uint32_t, 512> fracrow_clu3x3{};
  std::array<uint32_t, 512> fraccol_clu3x3{};
  std::array<uint32_t, 512> vec_topoID3x3{};
  std::array<uint32_t, 512> vec_topoIDfrac3x3{};
  std::array<uint32_t, 16>  cnt3x3{};

  for ( unsigned int topo3x3 = 0; topo3x3 < 512; ++topo3x3 ) {
    vec_topo3x3[topo3x3] = topo3x3;
    if ( is3x3revcluster( topo3x3 ) ) {
      vec_topo3x3eqv[topo3x3] = equivalent_topo3x3_rev( topo3x3 );
      fracrow_clu3x3[topo3x3] = ( std::get<0>( frac3x3cluster( equivalent_topo3x3_rev( topo3x3 ) ) ) ) % 4;
      fraccol_clu3x3[topo3x3] = ( std::get<1>( frac3x3cluster( equivalent_topo3x3_rev( topo3x3 ) ) ) ) % 4;
      if ( topo3x3 == vec_topo3x3eqv[topo3x3] ) {
        vec_topoID3x3[topo3x3] = cnt3x3[( fracrow_clu3x3[topo3x3] % 4 ) * 4 + ( fraccol_clu3x3[topo3x3] % 4 )];
        cnt3x3[( fracrow_clu3x3[topo3x3] % 4 ) * 4 + ( fraccol_clu3x3[topo3x3] % 4 )] += 1;
      } else {
        vec_topoID3x3[topo3x3] = vec_topoID3x3[vec_topo3x3eqv[topo3x3]];
      }
    } else {
      vec_topo3x3eqv[topo3x3] = 0;
      fracrow_clu3x3[topo3x3] = 0;
      fraccol_clu3x3[topo3x3] = 0;
      vec_topoID3x3[topo3x3]  = 31;
    }
  }

  for ( unsigned int topo3x3 = 0; topo3x3 < 512; ++topo3x3 ) {
    vec_topoIDfrac3x3[topo3x3] =
        ( vec_topoID3x3[topo3x3] << 4 ) | ( fraccol_clu3x3[topo3x3] << 2 ) | ( fracrow_clu3x3[topo3x3] );
  }

  return vec_topoIDfrac3x3;
}

//====================================================================================================================
// Function to compute the cluster topology ID and the coordinate fractional parts  from the full 2x4 cluster topology
//====================================================================================================================

std::array<uint32_t, 256> VPRetinaTopologyID::topo2x4_to_topoIDfrac() {
  std::array<uint32_t, 256> vec_topo2x4{};
  std::array<uint32_t, 256> vec_topo2x4_bd0{};
  std::array<uint32_t, 256> vec_topo2x4_bd1{};
  std::array<uint32_t, 256> fracrow_clu2x4_0{};
  std::array<uint32_t, 256> fracrow_clu2x4_1{};
  std::array<uint32_t, 256> fraccol_clu2x4_0{};
  std::array<uint32_t, 256> fraccol_clu2x4_1{};
  std::array<uint32_t, 256> vec_topoID2x4{};
  std::array<uint32_t, 256> vec_topoIDfrac2x4{};
  std::array<uint32_t, 16>  cnt2x4{};

  for ( unsigned int topo2x4 = 0; topo2x4 < 256; ++topo2x4 ) {
    vec_topo2x4[topo2x4] = topo2x4;
    if ( is2x4cluster( topo2x4 ) ) {
      uint32_t SP0             = std::get<0>( breakdown_topo2x4( topo2x4 ) );
      vec_topo2x4_bd0[topo2x4] = SP0;
      uint32_t ID0;
      uint32_t SP1             = std::get<1>( breakdown_topo2x4( topo2x4 ) );
      vec_topo2x4_bd1[topo2x4] = SP1;
      uint32_t ID1;
      if ( SP0 == 0 ) {
        ID0 = 63;
      } else {
        fracrow_clu2x4_0[topo2x4] = ( std::get<0>( frac2x4cluster( SP0 ) ) ) % 4;
        fraccol_clu2x4_0[topo2x4] = ( std::get<1>( frac2x4cluster( SP0 ) ) ) % 4;
        if ( SP1 == 0 ) {
          ID0 = cnt2x4[( fracrow_clu2x4_0[topo2x4] % 4 ) * 4 + ( fraccol_clu2x4_0[topo2x4] % 4 )];
          cnt2x4[( fracrow_clu2x4_0[topo2x4] % 4 ) * 4 + ( fraccol_clu2x4_0[topo2x4] % 4 )] += 1;
        } else {
          ID0 = vec_topoID2x4[SP0] & 63;
        }
      }
      if ( SP1 == 0 ) {
        ID1 = 63;
      } else {
        fracrow_clu2x4_1[topo2x4] = ( std::get<0>( frac2x4cluster( SP1 ) ) ) % 4;
        fraccol_clu2x4_1[topo2x4] = ( std::get<1>( frac2x4cluster( SP1 ) ) ) % 4;
        if ( SP0 == 0 ) {
          ID1 = cnt2x4[( fracrow_clu2x4_1[topo2x4] % 4 ) * 4 + ( fraccol_clu2x4_1[topo2x4] % 4 )];
          cnt2x4[( fracrow_clu2x4_1[topo2x4] % 4 ) * 4 + ( fraccol_clu2x4_1[topo2x4] % 4 )] += 1;
        } else {
          ID1 = vec_topoID2x4[SP1] & 63;
        }
      }

      vec_topoID2x4[topo2x4] = ( ID1 << 6 ) + ( ID0 );

    } else {

      fracrow_clu2x4_0[topo2x4] = 0;
      fraccol_clu2x4_0[topo2x4] = 0;
      fracrow_clu2x4_1[topo2x4] = 0;
      fraccol_clu2x4_1[topo2x4] = 0;
      vec_topoID2x4[topo2x4]    = ( 63 << 6 ) + 63;
    }
  }

  for ( unsigned int topo2x4 = 0; topo2x4 < 256; ++topo2x4 ) {
    vec_topoIDfrac2x4[topo2x4] = ( ( ( vec_topoID2x4[topo2x4] >> 6 ) & 63 ) << 14 ) |
                                 ( fraccol_clu2x4_1[topo2x4] << 12 ) | ( fracrow_clu2x4_1[topo2x4] << 10 ) |
                                 ( ( vec_topoID2x4[topo2x4] & 63 ) << 4 ) | ( fraccol_clu2x4_0[topo2x4] << 2 ) |
                                 ( fracrow_clu2x4_0[topo2x4] );
  }

  return vec_topoIDfrac2x4;
}

//=====================================================================================================================
// Function to retrieve the full 3x3 straight cluster topology from the topology ID and the coordinate fractional parts
//=====================================================================================================================

uint32_t VPRetinaTopologyID::topoIDfrac_to_topo3x3( uint32_t topoID, uint32_t fx, uint32_t fy ) {
  const auto& vec_topoIDfrac3x3 = topo3x3_to_topoIDfrac();
  auto        i = std::find( vec_topoIDfrac3x3.begin(), vec_topoIDfrac3x3.end(), ( topoID << 4 ) | ( fx << 2 ) | fy );
  // since  the first entry in vec_topoIDfrac3x3 corresponds to a non-valid cluster (no active pixels), it can be used
  // as default value in case the topology is not found
  return ( i != vec_topoIDfrac3x3.end() ) ? i - vec_topoIDfrac3x3.begin() : 0;
}

//=====================================================================================================================
// Function to retrieve the full 3x3 reversed cluster topology from the topology ID and the coordinate fractional parts
//=====================================================================================================================

uint32_t VPRetinaTopologyID::topoIDfrac_to_topo3x3_rev( uint32_t topoID, uint32_t fx, uint32_t fy ) {
  const auto& vec_topoIDfrac3x3 = topo3x3_rev_to_topoIDfrac();
  auto        i = std::find( vec_topoIDfrac3x3.begin(), vec_topoIDfrac3x3.end(), ( topoID << 4 ) | ( fx << 2 ) | fy );
  // since  the first entry in vec_topoIDfrac3x3 corresponds to a non-valid cluster (no active pixels), it can be used
  // as default value in case the topology is not found
  return ( i != vec_topoIDfrac3x3.end() ) ? i - vec_topoIDfrac3x3.begin() : 0;
}

//============================================================================================================
// Function to retrieve the full 2x4 cluster topology from the topology ID and the coordinate fractional parts
//============================================================================================================

uint32_t VPRetinaTopologyID::topoIDfrac_to_topo2x4( uint32_t topoID, uint32_t fx, uint32_t fy ) {
  const auto& vec_topoIDfrac2x4 = topo2x4_to_topoIDfrac();
  auto        i                 = std::find_if( vec_topoIDfrac2x4.begin(), vec_topoIDfrac2x4.end(),
                         [v = ( topoID << 4 ) | ( fx << 2 ) | fy]( uint32_t id ) { return ( id & 1023 ) == v; } );
  return i - vec_topoIDfrac2x4.begin(); // if not found, this returns vec_topoIDfrac2x4.size()
}
