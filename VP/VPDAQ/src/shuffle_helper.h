/*****************************************************************************\
* (c) Copyright 2023 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once

#include <GaudiKernel/IRndmGen.h>
#include <GaudiKernel/RndmGenerators.h>
#include <GaudiKernel/SmartIF.h>
#include <GaudiKernel/StatusCode.h>
#include <cstdint>
#include <limits>

namespace Utils {
  /// Small adapter for Rndm::Numbers
  class UniformRandomBitGenerator {
  public:
    UniformRandomBitGenerator() = default;
    StatusCode initialize( const SmartIF<IRndmGenSvc>& svc ) {
      if ( svc && !m_generator ) m_generator = svc->generator( Rndm::Flat( 0., 1. ) );
      return m_generator ? StatusCode::SUCCESS : StatusCode::FAILURE;
    }
    using result_type = std::uint32_t;
    static constexpr result_type min() { return 0; }
    static constexpr result_type max() { return std::numeric_limits<result_type>::max(); }
    result_type operator()() { return m_generator ? static_cast<result_type>( m_generator->shoot() * max() ) : max(); }

  private:
    SmartIF<IRndmGen> m_generator;
  };
} // namespace Utils
