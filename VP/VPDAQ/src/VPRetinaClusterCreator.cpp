/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "Detector/VP/VPChannelID.h"
#include "Event/RawBank.h"
#include "Event/RawEvent.h"
#include "Event/VPLightCluster.h"
#include "GaudiKernel/RndmGenerators.h"
#include "GaudiKernel/Transform3DTypes.h"
#include "Kernel/VPConstants.h"
#include "LHCbAlgs/Transformer.h"
#include "VPDAQ/VPRetinaClusterConstants.h"
#include "VPDet/DeVP.h"
#include "VPKernel/PixelUtils.h"
#include "VPKernel/VeloPixelInfo.h"
#include "VPRetinaMatrix.h"
#include "VPRetinaTopologyID.h"
#include "shuffle_helper.h"
#include <algorithm> // std::shuffle
#include <array>
#include <cstdint>
#include <iomanip>
#include <iterator>
#include <tuple>
#include <vector>

/** @class VPRetinaClusterCreator VPRetinaClusterCreator.h
 * Algorithm to create RetinaCluster Raw Bank from SuperPixel.
 *
 * There is one raw bank per sensor, that is the sensor number (0-207)
 * is the source ID of the bank. Note that this means there is no
 * need to encode the sensor in the RetinaCluster addresses.
 *
 * Each bank has a four byte word header, followed by a four byte
 * RetinaCluster word for each RetinaCluster on the sensor.
 *
 * The header word is currently simply the number of RetinaCluster
 * on the sensor.
 *
 * The RetinaCluster word encoding is the following:
 *
 * bit 0-1    RetinaCluster Fraction Row (0-1 by step of 0.25)
 * bit 2-9    RetinaCluster Row (0-255)
 * bit 10-11  RetinaCluster Fraction Column (0-1 by step of 0.25)
 * bit 12-21  RetinaCluster Column (0-767)
 * bit 22     SensorID in sensor pair
 * bit 23-29  TopologyID and flags

 * bit 30     Isolation bit
 * bit 31     UNUSED
 *
 * The bank SourceID is
 *
 * bits 15..11 - TFC partition: VELOA = 00010, VELOC = 00011
 * bits 10..9  - Reserved, currently 00
 * bit   8     - DataFlow ID (see below)
 * bits  7..0  - VELO module number (0-51)
 *
 * The DataFlow ID * 2 + sensorID bit = sensor position in the module
 * Note the sensorID is in the SuperPixel word and mixed between the
 * two sensors in the bank.
 *
 * The full sensor index is then
 * bits 9..2 - Module ID (from SourceID bits 7..0)
 * bit  1    - DataFlow ID (from SourceID bit 8)
 * bit  0    - SensorID bit (from Cluster word bit 22)
 *
 *
 * @author Federico Lazzari
 * @date   2018-06-20
 */

class VPRetinaClusterCreator
    : public LHCb::Algorithm::MultiTransformer<std::tuple<LHCb::RawEvent, LHCb::RawBank::View>(
          const LHCb::RawBank::View& )
                                               /* , LHCb::Algorithm::Traits::writeOnly<LHCb::RawEvent> */> {

public:
  /// Standard constructor
  VPRetinaClusterCreator( const std::string& name, ISvcLocator* pSvcLocator );

  StatusCode initialize() override; ///< Algorithm initialization

  /// Algorithm execution
  std::tuple<LHCb::RawEvent, LHCb::RawBank::View> operator()( const LHCb::RawBank::View& ) const override;

private:
  /// make RetinaClusters from bank
  std::vector<uint32_t> makeRetinaClusters( LHCb::span<const unsigned int>, LHCb::Detector::VPChannelID::SensorID,
                                            LHCb::Detector::VPChannelID::SensorID ) const;
  Gaudi::Property<unsigned int> m_chain_length{
      this, "chain_length", 20,
      "Number of RetinaMatrix in a chain, a lower number increase clone, a bigger number require more space in FPGA"};

  Gaudi::Property<unsigned int> m_icf_cut{
      this, "icf_cut", 144,
      "Maximum number of SPs per sensor pair and per event that the Isolation Cluster Flagging (ICF) can take as "
      "input. If the number of SPs is bigger than this value, SPs are not flagged."};

  // random number generator for mixing the cluster
  mutable Utils::UniformRandomBitGenerator m_rndm;
};

using namespace LHCb;
using namespace VPRetinaCluster;

DECLARE_COMPONENT( VPRetinaClusterCreator )

namespace {
  struct SPCache {
    std::array<float, 4> fxy;
    std::uint8_t         pattern;
    std::uint8_t         nx1;
    std::uint8_t         nx2;
    std::uint8_t         ny1;
    std::uint8_t         ny2;
  };
  //=========================================================================
  // Cache Super Pixel cluster patterns.
  //=========================================================================
  auto create_SPPatterns() {
    std::array<SPCache, 256> SPCaches;
    // create a cache for all super pixel cluster patterns.
    // this is an unoptimized 8-way flood fill on the 8 pixels
    // in the super pixel.
    // no point in optimizing as this is called once in
    // initialize() and only takes about 20 us.

    // define deltas to 8-connectivity neighbours
    const int dx[] = {-1, 0, 1, -1, 0, 1, -1, 1};
    const int dy[] = {-1, -1, -1, 1, 1, 1, 0, 0};

    // clustering buffer for isolated superpixels.
    std::uint8_t sp_buffer[8];

    // SP index buffer and its size for single SP clustering
    std::uint8_t sp_idx[8];
    std::uint8_t sp_idx_size = 0;

    // stack and stack pointer for single SP clustering
    std::uint8_t sp_stack[8];
    std::uint8_t sp_stack_ptr = 0;

    // loop over all possible SP patterns
    for ( unsigned int sp = 0; sp < 256; ++sp ) {
      sp_idx_size = 0;
      for ( unsigned int shift = 0; shift < 8; ++shift ) {
        const std::uint8_t p = sp & ( 1 << shift );
        sp_buffer[shift]     = p;
        if ( p ) { sp_idx[sp_idx_size++] = shift; }
      }

      // loop over pixels in this SP and use them as
      // cluster seeds.
      // note that there are at most two clusters
      // in a single super pixel!
      std::uint8_t clu_idx = 0;
      for ( unsigned int ip = 0; ip < sp_idx_size; ++ip ) {
        std::uint8_t idx = sp_idx[ip];

        if ( 0 == sp_buffer[idx] ) { continue; } // pixel is used

        sp_stack_ptr             = 0;
        sp_stack[sp_stack_ptr++] = idx;
        sp_buffer[idx]           = 0;
        std::uint8_t x           = 0;
        std::uint8_t y           = 0;
        std::uint8_t n           = 0;

        while ( sp_stack_ptr ) {
          idx                    = sp_stack[--sp_stack_ptr];
          const std::uint8_t row = idx % 4;
          const std::uint8_t col = idx / 4;
          x += col;
          y += row;
          ++n;

          for ( unsigned int ni = 0; ni < 8; ++ni ) {
            const std::int8_t ncol = col + dx[ni];
            if ( ncol < 0 || ncol > 1 ) continue;
            const std::int8_t nrow = row + dy[ni];
            if ( nrow < 0 || nrow > 3 ) continue;
            const std::uint8_t nidx = ( ncol << 2 ) | nrow;
            if ( 0 == sp_buffer[nidx] ) continue;
            sp_stack[sp_stack_ptr++] = nidx;
            sp_buffer[nidx]          = 0;
          }
        }

        const uint32_t cx = x / n;
        const uint32_t cy = y / n;
        const float    fx = x / static_cast<float>( n ) - cx;
        const float    fy = y / static_cast<float>( n ) - cy;

        // store the centroid pixel
        SPCaches[sp].pattern |= ( ( cx << 2 ) | cy ) << 4 * clu_idx;

        // set the two cluster flag if this is the second cluster
        SPCaches[sp].pattern |= clu_idx << 3;

        // set the pixel fractions
        SPCaches[sp].fxy[2 * clu_idx]     = fx;
        SPCaches[sp].fxy[2 * clu_idx + 1] = fy;

        // increment cluster count. note that this can only become 0 or 1!
        ++clu_idx;
      }
    }
    return SPCaches;
  }
  // SP pattern buffers for clustering, cached once.
  // There are 256 patterns and there can be at most two
  // distinct clusters in an SP.
  static const std::array<SPCache, 256> s_SPCaches = create_SPPatterns();
} // namespace

//=============================================================================
// Standard constructor, initializes variables
//=============================================================================
VPRetinaClusterCreator::VPRetinaClusterCreator( const std::string& name, ISvcLocator* pSvcLocator )
    : MultiTransformer{name,
                       pSvcLocator,
                       {{"RawBanks", {}}},
                       {{"RetinaClusterLocation", LHCb::RawEventLocation::VeloCluster}, {"RetinaRawBanks", {}}}} {}

StatusCode VPRetinaClusterCreator::initialize() {
  return MultiTransformer::initialize().andThen( [&] { return m_rndm.initialize( randSvc() ); } );
}
//=============================================================================
// Main execution
//=============================================================================
std::tuple<LHCb::RawEvent, LHCb::RawBank::View> VPRetinaClusterCreator::
                                                operator()( const LHCb::RawBank::View& tBanks ) const {

  if ( tBanks.empty() ) return {};
  if ( tBanks[0]->type() != LHCb::RawBank::VP )
    throw GaudiException( "Wrong RawBank::Type", __PRETTY_FUNCTION__, StatusCode::FAILURE );

  const unsigned int version = tBanks[0]->version();
  if ( version != 2 && version != VPRetinaCluster::c_SPBankVersion ) {
    warning() << "Unsupported raw bank version (" << version << ", needs " << VPRetinaCluster::c_SPBankVersion << ")"
              << endmsg;
    return {};
  }

  if ( msgLevel( MSG::DEBUG ) ) debug() << "Read " << tBanks.size() << " raw banks from TES" << endmsg;

  unsigned int nBanks = 0;

  RawEvent rawEvent{};
  // Loop over VP RawBanks
  for ( auto iterBank : tBanks ) {

    const auto sourceID = iterBank->sourceID();

    unsigned int              sensor0;
    unsigned int              sensor1;
    std::vector<unsigned int> data;
    data.reserve( ( iterBank->range<uint32_t>() ).size() );

    if ( version == 2 ) {
      sensor0 = sourceID;
      sensor1 = sensor0 + 1;
      for ( auto spw : ( iterBank->range<uint32_t>() ).subspan( 1 ) ) { data.push_back( spw ); }
    } else {
      // The sensor index is
      // bits 9..2 - Module ID (from SourceID bits 8..1)
      // bit  1    - DataFlow ID (from SourceID bit 0)
      // bit  0    - SensorID bit 0 for sensor 0 (of pair), 1 for sensor 1
      // TFC parition is in mask 0x1800 and is 0x1800 for C and 0x1000 for A sides
      assert( ( ( sourceID & 0x1800 ) == 0x1800 ) || ( ( sourceID & 0x1800 ) == 0x1000 ) );
      sensor0 = ( sourceID & 0x1FFU ) << 1;
      sensor1 = sensor0 + 1;

      if ( iterBank->range<uint32_t>().size() > m_icf_cut ) { // if there are too many SPs ICF is not run
        for ( auto spw : iterBank->range<uint32_t>() ) { data.push_back( spw ); }
      } else {                                                    // otherwise set 'no neighbour' hint flags
        std::array<bool, VP::NPixelsPerSensor> buffer0 = {false}; // buffer for checking super pixel neighbours - sensor
                                                                  // 0
        std::array<bool, VP::NPixelsPerSensor> buffer1 = {false}; // buffer for checking super pixel neighbours - sensor
                                                                  // 1
        for ( auto spw : iterBank->range<uint32_t>() ) {
          assert( ( spw & 0xFFu ) != 0 );
          unsigned int idx = ( spw >> 8 ) & 0x7FFF;
          if ( spw & 0x800000U ) { // sensor 1
            buffer1[idx] = true;
          } else { // sensor 0
            buffer0[idx] = true;
          }
        }

        constexpr auto dx = std::array{-1, 0, 1, -1, 0, 1, -1, 1};
        constexpr auto dy = std::array{-1, -1, -1, 1, 1, 1, 0, 0};

        for ( auto spw : iterBank->range<uint32_t>() ) {
          const unsigned int idx          = ( spw >> 8 );
          const unsigned int row          = idx & 0x3Fu;
          const unsigned int col          = ( idx >> 6 ) & 0x1FFu;
          unsigned int       no_neighbour = 1;
          for ( unsigned int ni = 0; ni < 8; ++ni ) {
            const int nrow = row + dy[ni];
            if ( nrow < 0 || nrow > 63 ) continue;
            const int ncol = col + dx[ni];
            if ( ncol < 0 || ncol > 383 ) continue;
            const unsigned int nidx = ( ncol << 6 ) | nrow;
            if ( spw & 0x800000U ) { // sensor 1
              if ( buffer1[nidx] ) {
                no_neighbour = 0;
                break;
              }
            } else {
              if ( buffer0[nidx] ) {
                no_neighbour = 0;
                break;
              }
            }
          }
          spw |= ( no_neighbour << 31 );
          data.push_back( spw );
        }
      }
    }

    if ( version == 2 ) {
      auto sensorCluster = makeRetinaClusters( data, LHCb::Detector::VPChannelID::SensorID( sensor0 ),
                                               LHCb::Detector::VPChannelID::SensorID( sensor1 ) );

      std::vector<uint32_t> notSortedClusters;
      notSortedClusters.reserve( sensorCluster.size() + 1 );
      notSortedClusters.push_back( sensorCluster.size() );
      notSortedClusters.insert( end( notSortedClusters ), begin( sensorCluster ), end( sensorCluster ) );

      rawEvent.addBank( sensor0, LHCb::RawBank::VPRetinaCluster, 3, notSortedClusters );

    } else {
      auto sensorPairCluster = makeRetinaClusters( data, LHCb::Detector::VPChannelID::SensorID( sensor0 ),
                                                   LHCb::Detector::VPChannelID::SensorID( sensor1 ) );

      std::vector<uint32_t> notSortedClusters;
      notSortedClusters.reserve( sensorPairCluster.size() );
      notSortedClusters.insert( end( notSortedClusters ), begin( sensorPairCluster ), end( sensorPairCluster ) );

      // randomize the cluster order as the TELL40 will not make any specific sorting
      std::shuffle( notSortedClusters.begin(), notSortedClusters.end(), m_rndm );

      rawEvent.addBank( sourceID, LHCb::RawBank::VPRetinaCluster, VPRetinaCluster::c_bankVersion, notSortedClusters );
    }

    ++nBanks;

  } // loop over all banks

  if ( msgLevel( MSG::DEBUG ) ) debug() << "Added " << nBanks << " raw banks of retina clusters to TES" << endmsg;

  auto view = rawEvent.banks( LHCb::RawBank::VPRetinaCluster );
  return {std::move( rawEvent ), std::move( view )};
}

//=============================================================================
// make RetinaClusters from bank
//=============================================================================
std::vector<uint32_t>
VPRetinaClusterCreator::makeRetinaClusters( LHCb::span<const unsigned int>        bank,
                                            LHCb::Detector::VPChannelID::SensorID sensor0,
                                            LHCb::Detector::VPChannelID::SensorID sensor1 ) const {

  std::vector<VPRetinaMatrix> RetinaMatrixVector0;
  std::vector<VPRetinaMatrix> RetinaMatrixVector1;
  RetinaMatrixVector0.reserve( m_chain_length );
  RetinaMatrixVector1.reserve( m_chain_length );
  std::vector<uint32_t>     retinaCluster;
  VPRetinaTopologyID        topo_obj;
  std::array<uint32_t, 256> vec_topoID2x4 = topo_obj.topo2x4_to_topoIDfrac();

  if ( msgLevel( MSG::VERBOSE ) ) {
    verbose() << "Clustering " << bank.size() << " SPs from sensors " << to_unsigned( sensor0 ) << " and "
              << to_unsigned( sensor1 ) << endmsg;
  }

  // Read super pixel
  for ( const uint32_t sp_word : bank ) {

    uint8_t sp = sp_word & 0xFFU;

    if ( 0 == sp ) continue; // protect against zero super pixels.

    const uint32_t sp_addr          = ( sp_word & 0x007FFF00U ) >> 8;
    const uint32_t sp_row           = sp_addr & 0x3FU;
    const uint32_t sp_col           = ( sp_addr >> 6 );
    const uint32_t no_sp_neighbours = sp_word & 0x80000000U;
    const uint32_t no_sp_neigh_flag = no_sp_neighbours >> 31;
    // running two sensors in parallel due to mixed SP clusters in the bank
    // check bit 23 of SP for which sensor in pair, set bit 22 of RetinaCluster word
    const uint32_t sensorBit = ( sp_word & 0x800000U ) ? ( 0x1U << sensorID_shift ) : 0x0U;
    // if sensorBit != 0x0 then sensor1 of pair
    auto& sensor             = sensorBit ? sensor1 : sensor0;
    auto& RetinaMatrixVector = sensorBit ? RetinaMatrixVector1 : RetinaMatrixVector0;

    // if a super pixel is not isolated
    // we use Retina Clustering Algorithm
    if ( !no_sp_neighbours ) {
      // we look for already created Retina
      auto iterRetina = std::find_if( RetinaMatrixVector.begin(), RetinaMatrixVector.end(),
                                      [&]( const VPRetinaMatrix& m ) { return m.IsInRetina( sp_row, sp_col ); } );
      if ( iterRetina != RetinaMatrixVector.end() ) {
        iterRetina->AddSP( sp_row, sp_col, sp );
        continue;
      } else {
        // if we have not reached maximum chain length
        // we create a new retina
        if ( RetinaMatrixVector.size() < m_chain_length ) {
          RetinaMatrixVector.emplace_back( sp_row, sp_col, sp, sensor );
          continue;
        }
      }
    }
    // if a super pixel is isolated or the RetinaMatrix chain is full
    // the clustering boils down to a simple pattern look up.

    // there is always at least one cluster in the super
    // pixel. look up the pattern and add it.

    // remove after caches rewrite
    const auto&    spcache = s_SPCaches[sp];
    const uint32_t idx     = spcache.pattern;

    const uint32_t row = idx & 0x03U;
    const uint32_t col = ( idx >> 2 ) & 1;

    uint64_t cX;
    uint64_t cY;
    // if the two clusters in the SP are
    // top-left for the first cluster
    // and bottom-right for the second cluster
    // aka if two clusters are present in the isolated SP
    // and the column of the two clusters is different
    // and the row of the first is bigger that the row of the second
    // then the coordinates of the second cluster are computed
    if ( ( idx & 8 ) && ( ( ( idx >> 2 ) & 1 ) != ( ( idx >> 6 ) & 1 ) ) &&
         ( ( idx & 0x03U ) > ( ( idx >> 4 ) & 3 ) ) ) {
      cX = ( ( sp_col * 2 + ( ( idx >> 6 ) & 3 ) ) << fracCol_nbit ) + ( uint64_t )( spcache.fxy[2] * 4 + 0.5 );
      cY = ( ( sp_row * 4 + ( ( idx >> 4 ) & 1 ) ) << fracRow_nbit ) + ( uint64_t )( spcache.fxy[3] * 4 + 0.5 );
      // if a single cluster is present in the isolated SP or
      // they are not top-left and bottom-right
      // compute the coordinates of the first cluster
    } else {
      cX = ( ( sp_col * 2 + col ) << fracCol_nbit ) + ( uint64_t )( spcache.fxy[0] * 4 + 0.5 );
      cY = ( ( sp_row * 4 + row ) << fracRow_nbit ) + ( uint64_t )( spcache.fxy[1] * 4 + 0.5 );
    }

    uint32_t topoID2x4 = ( vec_topoID2x4[sp] >> ( fracRow_nbit + fracCol_nbit ) );

    retinaCluster.push_back( 1 << isoBit_shift | no_sp_neigh_flag << isoFlag_shift |
                             ( topoID2x4 & topo2x4_mask ) << topo2x4_shift | cX << Row_nbit | cY | sensorBit );

    // if two clusters are present in the isolated SP
    if ( idx & 8 ) {
      const uint32_t row = ( idx >> 4 ) & 3;
      const uint32_t col = ( idx >> 6 ) & 1;
      uint64_t       cX;
      uint64_t       cY;
      // if the two clusters in the SP are
      // top-left for the first cluster
      // and bottom-right for the second cluster
      // aka if two clusters are present in the isolated SP
      // and the column of the two clusters is different
      // and the row of the first is bigger that the row of the second
      // then the coordinates of the second cluster are computed
      if ( ( idx & 8 ) && ( ( ( idx >> 2 ) & 1 ) != ( ( idx >> 6 ) & 1 ) ) &&
           ( ( idx & 0x03U ) > ( ( idx >> 4 ) & 3 ) ) ) {
        cX = ( ( sp_col * 2 + ( ( idx >> 2 ) & 1 ) ) << fracCol_nbit ) + ( uint64_t )( spcache.fxy[0] * 4 + 0.5 );
        cY = ( ( sp_row * 4 + ( idx & 0x03U ) ) << fracRow_nbit ) + ( uint64_t )( spcache.fxy[1] * 4 + 0.5 );
        // otherwise compute the coordinates of the second cluster
      } else {
        cX = ( ( sp_col * 2 + col ) << fracCol_nbit ) + ( uint64_t )( spcache.fxy[2] * 4 + 0.5 );
        cY = ( ( sp_row * 4 + row ) << fracRow_nbit ) + ( uint64_t )( spcache.fxy[3] * 4 + 0.5 );
      }

      uint32_t topoID2x4 = ( vec_topoID2x4[sp] >> ( topo2x4_nbit + 2 * ( fracRow_nbit + fracCol_nbit ) ) );

      retinaCluster.push_back( 1 << isoBit_shift | no_sp_neigh_flag << isoFlag_shift |
                               ( topoID2x4 & topo2x4_mask ) << topo2x4_shift | cX << Row_nbit | cY | sensorBit );
    }

  } // loop over super pixels in raw bank

  // searchRetinaCluster

  for ( auto& m : RetinaMatrixVector0 ) {
    const auto& clusters = m.SearchCluster();
    retinaCluster.insert( end( retinaCluster ), begin( clusters ), end( clusters ) );
  }

  for ( auto& m : RetinaMatrixVector1 ) {
    const auto& clusters = m.SearchCluster();
    retinaCluster.insert( end( retinaCluster ), begin( clusters ), end( clusters ) );
  }

  return retinaCluster;
}
