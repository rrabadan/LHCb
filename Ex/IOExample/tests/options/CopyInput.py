###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from PyConf.Algorithms import CopyInputStream
from PyConf.application import ApplicationOptions, configure, configure_input, create_or_reuse_rootIOAlg
from PyConf.control_flow import CompositeNode

options = ApplicationOptions(_enabled=False)
options.set_input_and_conds_from_testfiledb("upgrade_minbias_2019")
options.evt_max = 10
options.root_ioalg_name = "RootIOAlgExt"
options.output_type = "ROOT"
config = configure_input(options)

ioalg = create_or_reuse_rootIOAlg(options)
CopyAlg = CopyInputStream(
    name="CopyInputStream",
    Output="PFN:LHCbApp_CopyInputStream.dst",
    InputFileLeavesLocation=ioalg.InputLeavesLocation)

node = CompositeNode("Copyinput", children=[CopyAlg])
config.update(configure(options, node))
