###############################################################################
# (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from Gaudi.Configuration import *

from PRConfig.TestFileDB import test_file_db

from Configurables import RootIOAlg, CountBanks, HLTControlFlowMgr, ApplicationMgr, HiveWhiteBoard, HiveDataBrokerSvc

qualifiers = test_file_db['MiniBrunel_2018_MinBias_FTv4_DIGI']
raw_event_location = '/Event/DAQ/RawEvent'

ioalg = RootIOAlg(
    "RootIOAlg",
    EventBufferLocation=raw_event_location + "Banks",
    Input=qualifiers.filenames,
    EventBranches=["/Event/DAQ/RawEvent"],
    BufferNbEvents=20,
    NSkip=20)
countalg = CountBanks("CountBanks", RawEventLocation=raw_event_location)

hiveDataBroker = HiveDataBrokerSvc('HiveDataBrokerSvc')
hiveDataBroker.DataProducers.append(ioalg)
hiveDataBroker.DataProducers.append(countalg)

scheduler = HLTControlFlowMgr(
    'HLTControlFlowMgr',
    CompositeCFNodes=[('testIOAlg', 'LAZY_AND', ["RootIOAlg", "CountBanks"],
                       True)],
    MemoryPoolSize=10 * 1024 * 1024,
    ThreadPoolSize=1,
    EnableLegacyMode=False,
    BarrierAlgNames=[])

app = ApplicationMgr(
    EvtSel="NONE", EvtMax=10, EventLoop=scheduler, TopAlg=[ioalg, countalg])
whiteboard = HiveWhiteBoard('EventDataSvc', EventSlots=1, ForceLeaves=True)
app.ExtSvc.insert(0, whiteboard)
