/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "GaudiAlg/FilterPredicate.h"

#include "Event/HltDecReports.h"

#include "LoKi/FilterAlg.h"
#include "LoKi/HLTTypes.h"
#include "LoKi/IHltFactory.h"

namespace LoKi {
  /** @class HDRFilter
   *  Simple filtering algorithm bases on LoKi/Bender "hybrid" approach
   *  for filtering according to LHCb::HltDecReports
   *  @author Vanya BELYAEV Ivan.BElyaev@nikhef.nl
   *  @date 2008-09-23
   */
  class HDRFilter : public Gaudi::Functional::FilterPredicate<bool( const LHCb::HltDecReports& ),
                                                              Gaudi::Functional::Traits::BaseClass_t<LoKi::FilterAlg>> {
  public:
    bool operator()( const LHCb::HltDecReports& hdr ) const override {
      if ( updateRequired() ) {
        // @TODO/@FIXME: get rid of this const_cast...
        StatusCode sc = const_cast<LoKi::HDRFilter*>( this )->decode();
        Assert( sc.isSuccess(), "Unable to decode the functor!" );
      }

      // use the functor
      const bool result = m_cut( &hdr );

      // some statistics
      m_passed += result;

      return result;
    }

    /** Decode the functor (use the factory)
     *  @see LoKi::FilterAlg
     *  @see LoKi::FilterAlg::decode
     *  @see LoKi::FilterAlg::i_decode
     */
    StatusCode decode() override {
      StatusCode sc = i_decode<LoKi::Hybrid::IHltFactory>( m_cut );
      Assert( sc.isSuccess(), "Unable to decode the functor!" );
      return StatusCode::SUCCESS;
    }

    /** standard constructor
     *  @see LoKi::FilterAlg
     */
    HDRFilter( const std::string& name, ISvcLocator* pSvc ) : FilterPredicate( name, pSvc, KeyValue{"Location", ""} ) {
      auto sc = setProperty( "Factory", "LoKi::Hybrid::HltFactory/HltFactory:PUBLIC" );
      Assert( sc.isSuccess(), "Unable (re)set property 'Factory'" );
    }

    HDRFilter( const HDRFilter& ) = delete;
    HDRFilter& operator=( const HDRFilter& ) = delete;

  private:
    /// the functor itself
    LoKi::Types::HLT_Cut m_cut = {LoKi::BasicFunctors<const LHCb::HltDecReports*>::BooleanConstant( false )};
    mutable Gaudi::Accumulators::BinomialCounter<> m_passed{this, "#passed"};
  };
} // end of namespace LoKi

DECLARE_COMPONENT( LoKi::HDRFilter )
