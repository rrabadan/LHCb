/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef TRACKEVENT_STATEPARAMETERS_H
#define TRACKEVENT_STATEPARAMETERS_H 1

// Include files
#include "GaudiKernel/SystemOfUnits.h"
#include "State.h"

/** @namespace StateParameters
 *
 * This namespace defines some useful variables for the
 * manipulation of States.
 *
 *  @author Eduardo Rodrigues
 *  @date   2006-08-02
 *
 */

namespace LHCb {
  /// Pre-defined z-positions of states for certain State locations
  /// (see Location enum in State.h)
  [[nodiscard]] constexpr std::optional<double> Z( State::Location location ) {
    switch ( location ) {
    case State::Location::EndVelo:
      return 770. * Gaudi::Units::mm;
    case State::Location::BegRich1:
      return 990. * Gaudi::Units::mm;
    case State::Location::EndRich1:
      return 2165. * Gaudi::Units::mm;
    case State::Location::EndUT:
      return 2700. * Gaudi::Units::mm;
    case State::Location::BegT:
      return 7500. * Gaudi::Units::mm;
    case State::Location::EndT:
      return 9410. * Gaudi::Units::mm;
    case State::Location::BegRich2:
      return 9450. * Gaudi::Units::mm;
    case State::Location::EndRich2:
      return 11900. * Gaudi::Units::mm;
    default:
      return std::nullopt;
    }
  };
} // namespace LHCb

namespace /*[[deprecated]]*/ StateParameters {
  constexpr double ZEndVelo  = LHCb::Z( LHCb::State::Location::EndVelo ).value();
  constexpr double ZBegRich1 = LHCb::Z( LHCb::State::Location::BegRich1 ).value();
  constexpr double ZEndRich1 = LHCb::Z( LHCb::State::Location::EndRich1 ).value();
  constexpr double ZMidUT    = 2485. * Gaudi::Units::mm; // no matching State::Location...
  constexpr double ZEndUT    = LHCb::Z( LHCb::State::Location::EndUT ).value();
  constexpr double ZBegT     = LHCb::Z( LHCb::State::Location::BegT ).value();
  constexpr double ZMidT     = 8520. * Gaudi::Units::mm; // no matching State::Location...
  constexpr double ZEndT     = LHCb::Z( LHCb::State::Location::EndT ).value();
  constexpr double ZBegRich2 = LHCb::Z( LHCb::State::Location::BegRich2 ).value();
  constexpr double ZEndRich2 = LHCb::Z( LHCb::State::Location::EndRich2 ).value();
} // namespace StateParameters

// ----------------------------------------------------------------------------
//   end of namespace
// ----------------------------------------------------------------------------

#endif /// TRACKEVENT_STATEPARAMETERS_H
