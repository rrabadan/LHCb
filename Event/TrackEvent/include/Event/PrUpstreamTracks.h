/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#pragma once
#include "Event/SOACollection.h"
#include "Event/State.h"
#include "Event/Track_v3.h"
#include "Kernel/EventLocalAllocator.h"
#include "Kernel/LHCbID.h"
#include "LHCbMath/MatVec.h"
#include "LHCbMath/SIMDWrapper.h"
#include "LHCbMath/bit_cast.h"
#include "PrTracksTag.h"
#include "PrVeloTracks.h"

/**
 *
 * Track data for exchanges between UT and FT
 *
 *@author: Arthur Hennequin
 *Based on https://gitlab.cern.ch/lhcb/Rec/blob/master/Pr/PrKernel/PrKernel/PrVeloUTTrack.h
 * from Michel De Cian
 */

namespace LHCb::Pr::Upstream {

  struct Tag {
    struct Index : Event::int_field {};
    struct LHCbID : Event::lhcbid_field {};
    struct VPHits : Event::vector_field<Event::struct_field<Index, LHCbID>> {};
    struct UTHits : Event::vector_field<Event::struct_field<Index, LHCbID>> {};
    struct trackVP : Event::int_field {};
    struct State : Event::state_field {};
    // we only store a single state in the UT here because the velo track ancestor has CTB
    static_assert( Event::v3::num_states<Event::Enum::Track::Type::Upstream>() == 1 );

    template <typename T>
    using upstream_t = Event::SOACollection<T, trackVP, VPHits, UTHits, State>;
  };

  namespace SP = LHCb::Event::StateParameters;

  struct Tracks : Tag::upstream_t<Tracks> {
    using base_t = typename Tag::upstream_t<Tracks>;
    using tag_t  = Tag; // Needed for tag_type_t helper defined in PrTracksTag.h

    constexpr static auto MaxVPHits  = TracksInfo::MaxVPHits;
    constexpr static auto MaxUTHits  = TracksInfo::MaxUTHits;
    constexpr static auto MaxLHCbIDs = MaxVPHits + MaxUTHits;

    using base_t::allocator_type;

    Tracks( Velo::Tracks const*      velo_ancestors,
            Zipping::ZipFamilyNumber zipIdentifier = Zipping::generateZipIdentifier(), allocator_type alloc = {} )
        : base_t{std::move( zipIdentifier ), std::move( alloc )}, m_velo_ancestors{velo_ancestors} {}

    // Constructor used by zipping machinery when making a copy of a zip
    Tracks( Zipping::ZipFamilyNumber zn, Tracks const& old )
        : base_t{std::move( zn ), old}, m_velo_ancestors{old.m_velo_ancestors} {}

    // Return pointer to ancestor container
    [[nodiscard]] Velo::Tracks const* getVeloAncestors() const { return m_velo_ancestors; };

    // Define an minimal custom proxy for this track
    template <SIMDWrapper::InstructionSet simd, ProxyBehaviour behaviour, typename ContainerType>
    struct UpstreamProxy : Event::Proxy<simd, behaviour, ContainerType> {
      using base_t = typename Event::Proxy<simd, behaviour, ContainerType>;
      using base_t::base_t;
      using base_t::width;
      using simd_t  = SIMDWrapper::type_map_t<simd>;
      using int_v   = typename simd_t::int_v;
      using float_v = typename simd_t::float_v;

      [[nodiscard]] auto qOverP() const { return this->template get<Tag::State>().qOverP(); }
      [[nodiscard]] auto trackVP() const { return this->template get<Tag::trackVP>(); }
      [[nodiscard]] auto p() const { return abs( 1.f / qOverP() ); }
      [[nodiscard]] auto nVPHits() const { return this->template field<Tag::VPHits>().size(); }
      [[nodiscard]] auto nUTHits() const { return this->template field<Tag::UTHits>().size(); }
      [[nodiscard]] auto nHits() const { return nUTHits() + nVPHits(); }

      [[nodiscard]] auto vp_index( std::size_t i ) const {
        return this->template field<Tag::VPHits>()[i].template get<Tag::Index>();
      }
      [[nodiscard]] auto ut_index( std::size_t i ) const {
        return this->template field<Tag::UTHits>()[i].template get<Tag::Index>();
      }
      [[nodiscard]] auto vp_indices() const {
        std::array<int_v, Tracks::MaxVPHits> out = {0};
        for ( auto i = 0; i < nVPHits().hmax( this->loop_mask() ); i++ ) out[i] = vp_index( i );
        return out;
      }
      [[nodiscard]] auto ut_indices() const {
        std::array<int_v, Tracks::MaxUTHits> out = {0};
        for ( auto i = 0; i < nUTHits().hmax( this->loop_mask() ); i++ ) out[i] = ut_index( i );
        return out;
      }

      [[nodiscard]] auto vp_lhcbID( std::size_t i ) const {
        return this->template field<Tag::VPHits>()[i].template get<Tag::LHCbID>();
      }
      [[nodiscard]] auto ut_lhcbID( std::size_t i ) const {
        return this->template field<Tag::UTHits>()[i].template get<Tag::LHCbID>();
      }

      [[nodiscard, gnu::always_inline]] auto StatePosDir( [[maybe_unused]] Event::Enum::State::Location loc ) const {
        assert( stateIndex<Event::Enum::Track::Type::Upstream>( loc ) >= 0 );
        return this->template get<Tag::State>();
      }

      // Retrieve state info
      [[nodiscard]] auto StatePos() const {
        auto const state = this->template get<Tag::State>();
        return LHCb::LinAlg::Vec<float_v, 3>{state.x(), state.y(), state.z()};
      }
      [[nodiscard]] auto StateDir() const {
        auto const state = this->template get<Tag::State>();
        return LHCb::LinAlg::Vec<float_v, 3>{state.tx(), state.ty(), 1.f};
      }

      //  Retrieve the (sorted) set of LHCbIDs
      [[nodiscard]] std::vector<LHCbID> lhcbIDs() const {
        static_assert( width() == 1, "lhcbIDs() method cannot be used on vector proxies" );
        std::vector<LHCbID> ids;
        ids.reserve( Tracks::MaxLHCbIDs );
        for ( auto i = 0; i < nVPHits().cast(); i++ ) { ids.emplace_back( vp_lhcbID( i ).LHCbID() ); }
        for ( auto i = 0; i < nUTHits().cast(); i++ ) { ids.emplace_back( ut_lhcbID( i ).LHCbID() ); }
        std::sort( ids.begin(), ids.end() );
        return ids;
      }
    };
    template <SIMDWrapper::InstructionSet simd, ProxyBehaviour behaviour, typename ContainerType>
    using proxy_type = UpstreamProxy<simd, behaviour, ContainerType>;

  private:
    Velo::Tracks const* m_velo_ancestors{nullptr};
  };
} // namespace LHCb::Pr::Upstream
