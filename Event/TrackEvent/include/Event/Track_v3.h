/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once

#include "Event/SIMDEventTypes.h"
#include "Event/States.h"
#include "Event/UniqueIDGenerator.h"
#include "GaudiKernel/ClassID.h"
#include "Kernel/EventLocalAllocator.h"
#include "Kernel/LHCbID.h"
#include "Kernel/Traits.h"
#include "LHCbMath/MatVec.h"
#include "LHCbMath/SIMDWrapper.h"
#include "TrackEnums.h"

/**
 * Track data after the Kalman fit
 *
 * 2020-08-17 updated to SOACollection by Peilian Li as Forward
 * 2021-27-01 updated to SOACollection by Alex Gilman as Generic
 */

namespace LHCb::Event::v3 {

  constexpr CLID CLID_TracksV3 = 10013;

  namespace detail {
    /** Helper type for fitted track proxies  -- TODO: remove this wrapper, use ADL on top of proxy itself **/
    template <typename TrackProxy>
    struct FittedState {
      FittedState( TrackProxy proxy, StateLocation StateLoc ) : m_proxy{std::move( proxy )}, m_StateLoc{StateLoc} {}
      [[nodiscard, gnu::always_inline]] auto qOverP() const { return m_proxy.qOverP( m_StateLoc ); }
      [[nodiscard, gnu::always_inline]] auto momentum() const { return m_proxy.momentum( m_StateLoc ); }
      [[nodiscard, gnu::always_inline]] auto slopes() const { return m_proxy.direction( m_StateLoc ); }
      [[nodiscard, gnu::always_inline]] auto position() const { return m_proxy.position( m_StateLoc ); }
      [[nodiscard, gnu::always_inline]] auto location() const { return m_StateLoc; }
      [[nodiscard, gnu::always_inline]] auto covariance() const { return m_proxy.covariance( m_StateLoc ); }
      [[nodiscard, gnu::always_inline]] auto x() const { return position().X(); }
      [[nodiscard, gnu::always_inline]] auto y() const { return position().Y(); }
      [[nodiscard, gnu::always_inline]] auto z() const { return position().Z(); }
      [[nodiscard, gnu::always_inline]] auto tx() const { return slopes().X(); }
      [[nodiscard, gnu::always_inline]] auto ty() const { return slopes().Y(); }

      // flag which indicates client code can go for 'threeMomCovMatrix', `momPosCovMatrix` and `posCovMatrix` and not
      // for a track-like stateCov -- possible values: yes (for track-like objects ) no (for neutrals, composites),
      // maybe (particle, check at runtime, calls may return invalid results)
      static constexpr auto canBeExtrapolatedDownstream = CanBeExtrapolatedDownstream::yes;

    private:
      TrackProxy    m_proxy;
      StateLocation m_StateLoc;
    };
  } // namespace detail

  namespace Tag {

    struct trackVP : Event::int_field {};
    struct trackUT : Event::int_field {};
    struct trackSeed : Event::int_field {};
    struct Chi2 : Event::float_field {};
    struct nDoF : Event::int_field {};
    struct history : Event::enum_field<LHCb::Event::Enum::Track::History> {};

    struct UniqueID : Event::int_field {};

    // LHCbID Storage
    struct LHCbID : Event::lhcbid_field {};
    struct VPHits : Event::vector_field<Event::struct_field<LHCbID>> {};
    struct UTHits : Event::vector_field<Event::struct_field<LHCbID>> {};
    struct FTHits : Event::vector_field<Event::struct_field<LHCbID>> {};
    struct MuonHits : Event::vector_field<Event::struct_field<LHCbID>> {};

    struct States : Event::vector_field<Event::state_field> {};
    struct StateCovs : Event::vector_field<Event::state_cov_field> {};

    template <typename T>
    using Track_t = Event::SOACollection<T, trackVP, trackUT, trackSeed, Chi2, nDoF, history, UniqueID, VPHits, UTHits,
                                         FTHits, MuonHits, States, StateCovs>;
  } // namespace Tag

  struct Tracks : Tag::Track_t<Tracks> {
    using base_t        = typename Tag::Track_t<Tracks>;
    using StateLocation = detail::StateLocation;

    using base_t::allocator_type;

    Tracks( TrackType trackType, FitHistory fitHistory, bool backward, LHCb::UniqueIDGenerator const& unique_id_gen,
            Zipping::ZipFamilyNumber zipIdentifier = Zipping::generateZipIdentifier(), allocator_type alloc = {} )
        : base_t{std::move( zipIdentifier ), std::move( alloc )}
        , m_backward{backward}
        , m_trackType{trackType}
        , m_fitHistory{fitHistory}
        , m_unique_id_gen_tag{unique_id_gen.tag()} {

      if ( m_trackType == TrackType::Unknown ) throw std::invalid_argument( "Track type must not be Unknown" );
      reserve( 0 ); // force building of the data tree
      dataTree()->template child<Tag::States>().resize( num_states() );
      dataTree()->template child<Tag::StateCovs>().resize( num_states() );
    }

    Tracks( TrackType trackType, LHCb::UniqueIDGenerator const& unique_id_gen,
            Zipping::ZipFamilyNumber zipIdentifier = Zipping::generateZipIdentifier(), allocator_type alloc = {} )
        : Tracks{trackType,     FitHistory::PrKalmanFilter, false,
                 unique_id_gen, std::move( zipIdentifier ), std::move( alloc )} {}

    // Special constructor for zipping machinery
    Tracks( Zipping::ZipFamilyNumber zipIdentifier, Tracks const& other )
        : base_t{std::move( zipIdentifier ), other}
        , m_backward{other.m_backward}
        , m_trackType{other.m_trackType}
        , m_fitHistory{other.m_fitHistory}
        , m_unique_id_gen_tag{other.m_unique_id_gen_tag} {}

    // Default constructor for unpacking
    Tracks( allocator_type alloc = {} )
        : base_t{Zipping::ZipFamilyNumber{0}, std::move( alloc )}, m_trackType{TrackType::Unknown} {}

    template <typename Buffer>
    void packHeader( Buffer& buffer ) const {
      buffer.write( m_trackType );
      buffer.write( m_fitHistory );
      buffer.write( m_unique_id_gen_tag );
    }

    template <typename Buffer>
    void unpackHeader( Buffer& buffer ) {
      buffer.read( m_trackType );
      buffer.read( m_fitHistory );
      buffer.read( m_unique_id_gen_tag );
      reserve( 0 ); // force building of the data tree
      dataTree()->template child<Tag::States>().resize( num_states() );
      dataTree()->template child<Tag::StateCovs>().resize( num_states() );
    }

    const CLID&        clID() const { return Tracks::classID(); }
    static const CLID& classID() { return CLID_TracksV3; }

    [[nodiscard, gnu::always_inline]] auto type() const { return m_trackType; }
    [[nodiscard]] auto                     backward() const { return m_backward; }
    [[nodiscard]] auto                     fitHistory() const { return m_fitHistory; }
    auto&                                  setFitHistory( FitHistory fitHistory ) {
      m_fitHistory = fitHistory;
      return *this;
    }

    template <SIMDWrapper::InstructionSet simd, LHCb::Pr::ProxyBehaviour behaviour, typename ContainerType>
    struct FittedProxy : Event::Proxy<simd, behaviour, ContainerType> {
      using base_t = typename Event::Proxy<simd, behaviour, ContainerType>;
      using base_t::loop_mask;
      using base_t::Proxy;
      using base_t::width;
      using simd_t  = SIMDWrapper::type_map_t<simd>;
      using int_v   = typename simd_t::int_v;
      using float_v = typename simd_t::float_v;

      [[nodiscard]] auto                     backward() const { return this->container()->backward(); }
      [[nodiscard, gnu::always_inline]] auto trackVP() const { return this->template get<Tag::trackVP>(); }
      [[nodiscard, gnu::always_inline]] auto trackUT() const { return this->template get<Tag::trackUT>(); }
      [[nodiscard, gnu::always_inline]] auto trackSeed() const { return this->template get<Tag::trackSeed>(); }
      [[nodiscard, gnu::always_inline]] auto unique_id() const {
        return LHCb::UniqueIDGenerator::ID<int_v>{this->template get<Tag::UniqueID>(), unique_id_gen_tag()};
      }

      [[nodiscard, gnu::always_inline]] auto unique_id_gen_tag() const {
        return this->container()->unique_id_gen_tag();
      }
      [[nodiscard, gnu::always_inline]] auto vp_lhcbID( std::size_t i ) const {
        return this->template field<Tag::VPHits>()[i].template get<Tag::LHCbID>();
      }
      [[nodiscard, gnu::always_inline]] auto ut_lhcbID( std::size_t i ) const {
        return this->template field<Tag::UTHits>()[i].template get<Tag::LHCbID>();
      }
      [[nodiscard, gnu::always_inline]] auto ft_lhcbID( std::size_t i ) const {
        return this->template field<Tag::FTHits>()[i].template get<Tag::LHCbID>();
      }
      [[nodiscard, gnu::always_inline]] auto muon_lhcbID( std::size_t i ) const {
        return this->template field<Tag::MuonHits>()[i].template get<Tag::LHCbID>();
      }
      [[nodiscard, gnu::always_inline]] auto nVPHits() const { return this->template field<Tag::VPHits>().size(); }
      [[nodiscard, gnu::always_inline]] auto nUTHits() const { return this->template field<Tag::UTHits>().size(); }
      [[nodiscard, gnu::always_inline]] auto nFTHits() const { return this->template field<Tag::FTHits>().size(); }
      [[nodiscard, gnu::always_inline]] auto nMuonHits() const { return this->template field<Tag::MuonHits>().size(); }
      [[nodiscard, gnu::always_inline]] auto nHits() const { return nVPHits() + nUTHits() + nFTHits() + nMuonHits(); }

      [[nodiscard, gnu::always_inline]] auto history() const { return this->template get<Tag::history>(); }

      [[nodiscard, gnu::always_inline]] auto chi2() const { return this->template get<Tag::Chi2>(); }
      [[nodiscard, gnu::always_inline]] auto nDoF() const { return this->template get<Tag::nDoF>(); }
      [[nodiscard, gnu::always_inline]] auto chi2PerDoF() const { return chi2() / nDoF(); }

      [[nodiscard, gnu::always_inline]] auto has_state( StateLocation StateLoc ) const {
        return this->container()->has_state( StateLoc );
      }
      [[nodiscard, gnu::always_inline]] auto state_index( StateLocation StateLoc ) const {
        return this->container()->state_index( StateLoc );
      }
      [[nodiscard, gnu::always_inline]] StateLocation defaultState() const {
        if ( has_state( StateLocation::ClosestToBeam ) ) return StateLocation::ClosestToBeam;
        if ( this->container()->num_states() == 0 ) {
          throw GaudiException( "Track has no states.", "v3::Track::defaultState()", StatusCode::FAILURE );
        }
        const auto state_set = this->container()->state_set();
        // if CTB state is not there, return the first state (typically FirstMeasurement)
        // TODO: c++20 std::countr_zero
        const auto offset_first_state = __builtin_ctz( static_cast<unsigned>( state_set ) );
        return static_cast<StateLocation>( offset_first_state );
      }
      [[nodiscard, gnu::always_inline]] auto qOverP( StateLocation StateLoc ) const {
        if ( !this->has_state( StateLoc ) ) { throw std::invalid_argument( "State not defined for this track type." ); }
        return this->template field<Tag::States>()[state_index( StateLoc )].qOverP();
      }
      [[nodiscard, gnu::always_inline]] auto p( StateLocation StateLoc ) const { return abs( 1 / qOverP( StateLoc ) ); }
      [[nodiscard, gnu::always_inline]] auto pt( StateLocation StateLoc ) const {
        auto const mom    = p( StateLoc );
        auto const tx     = this->template field<Tag::States>()[state_index( StateLoc )].tx();
        auto const ty     = this->template field<Tag::States>()[state_index( StateLoc )].ty();
        auto const tx2ty2 = tx * tx + ty * ty;
        auto const pt2    = mom * mom * tx2ty2 / ( tx2ty2 + 1 );
        using std::sqrt;
        return sqrt( pt2 );
      }
      [[nodiscard, gnu::always_inline]] auto qOverP() const { return qOverP( defaultState() ); }
      [[nodiscard, gnu::always_inline]] auto p() const { return p( defaultState() ); }
      [[nodiscard, gnu::always_inline]] auto pt() const { return pt( defaultState() ); }
      [[nodiscard, gnu::always_inline]] auto charge() const {
        return select( qOverP( defaultState() ) > 0.f, int_v{+1}, int_v{-1} );
      }
      [[nodiscard]] auto covariance( StateLocation StateLoc ) const {
        if ( !this->has_state( StateLoc ) ) { throw std::invalid_argument( "State not defined for this track type." ); }
        auto const&                      stateCov = this->template field<Tag::StateCovs>()[state_index( StateLoc )];
        LHCb::LinAlg::MatSym<float_v, 5> cov; // This is not zero-initialised
        cov( 0, 0 ) = stateCov.x_x();
        cov( 1, 0 ) = stateCov.x_y();
        cov( 2, 0 ) = stateCov.x_tx();
        cov( 3, 0 ) = stateCov.x_ty();
        cov( 4, 0 ) = stateCov.x_QoverP();
        cov( 1, 1 ) = stateCov.y_y();
        cov( 2, 1 ) = stateCov.y_tx();
        cov( 3, 1 ) = stateCov.y_ty();
        cov( 4, 1 ) = stateCov.y_QoverP();
        cov( 2, 2 ) = stateCov.tx_tx();
        cov( 3, 2 ) = stateCov.tx_ty();
        cov( 4, 2 ) = stateCov.tx_QoverP();
        cov( 3, 3 ) = stateCov.ty_ty();
        cov( 4, 3 ) = stateCov.ty_QoverP();
        cov( 4, 4 ) = stateCov.QoverP_QoverP();
        return cov;
      }
      [[nodiscard, gnu::always_inline]] auto position( StateLocation StateLoc ) const {
        if ( !this->has_state( StateLoc ) ) { throw std::invalid_argument( "State not defined for this track type." ); }
        auto const state = this->template field<Tag::States>()[state_index( StateLoc )];
        return LHCb::LinAlg::Vec<float_v, 3>{state.x(), state.y(), state.z()};
      }
      [[nodiscard, gnu::always_inline]] auto direction( StateLocation StateLoc ) const {
        if ( !this->has_state( StateLoc ) ) { throw std::invalid_argument( "State not defined for this track type." ); }
        auto const state = this->template field<Tag::States>()[state_index( StateLoc )];
        return LHCb::LinAlg::Vec<float_v, 3>{state.tx(), state.ty(), 1.f};
      }
      [[nodiscard, gnu::always_inline]] auto state( StateLocation StateLoc ) const {
        if ( !this->has_state( StateLoc ) ) { throw std::invalid_argument( "State not defined for this track type." ); }
        return detail::FittedState{*this, StateLoc};
      }
      [[nodiscard, gnu::always_inline]] auto StatePosDir( StateLocation StateLoc ) const { return state( StateLoc ); }
      [[nodiscard, gnu::always_inline]] auto closestToBeamState() const {
        return state( StateLocation::ClosestToBeam );
      }
      [[nodiscard, gnu::always_inline]] auto positionDirection( StateLocation StateLoc ) const {
        if ( !this->has_state( StateLoc ) ) { throw std::invalid_argument( "State not defined for this track type." ); }
        auto const state = this->template field<Tag::States>()[state_index( StateLoc )];
        return LinAlg::Vec{state.x(), state.y(), state.z(), state.tx(), state.ty()};
      }
      [[nodiscard, gnu::always_inline]] auto pseudoRapidity( StateLocation StateLoc ) const {
        return direction( StateLoc ).eta();
      }
      [[nodiscard, gnu::always_inline]] auto phi( StateLocation StateLoc ) const { return direction( StateLoc ).phi(); }
      [[nodiscard, gnu::always_inline]] auto threeMomentum( StateLocation StateLoc ) const {
        auto const dir = direction( StateLoc );
        return dir * ( p( StateLoc ) / dir.mag() );
      }
      [[nodiscard, gnu::always_inline]] friend auto threeMomentum( FittedProxy const& fp ) {
        return fp.threeMomentum( fp.defaultState() );
      }
      [[nodiscard, gnu::always_inline]] friend auto slopes( FittedProxy const& fp ) {
        return fp.direction( fp.defaultState() );
      }
      [[nodiscard, gnu::always_inline]] friend auto referencePoint( FittedProxy const& fp ) {
        return fp.position( fp.defaultState() );
      }
      static constexpr auto canBeExtrapolatedDownstream = Event::CanBeExtrapolatedDownstream::yes;
      [[nodiscard, gnu::always_inline]] friend auto trackState( FittedProxy const& fp ) {
        return fp.state( StateLocation::ClosestToBeam );
      }

      [[nodiscard, gnu::always_inline]] auto type() const { return this->container()->type(); }
      [[nodiscard]] auto                     fitHistory() const { return this->container()->fitHistory(); }

      // Retrieve the (sorted) set of LHCbIDs
      [[nodiscard]] std::vector<LHCbID> lhcbIDs() const {
        static_assert( width() == 1, "lhcbIDs() method cannot be used on vector proxies" );
        std::vector<LHCbID> ids;
        ids.reserve( nHits().cast() );
        for ( auto i = 0; i < nVPHits().cast(); i++ ) { ids.emplace_back( vp_lhcbID( i ).LHCbID() ); }
        for ( auto i = 0; i < nUTHits().cast(); i++ ) { ids.emplace_back( ut_lhcbID( i ).LHCbID() ); }
        for ( auto i = 0; i < nFTHits().cast(); i++ ) { ids.emplace_back( ft_lhcbID( i ).LHCbID() ); }
        for ( auto i = 0; i < nMuonHits().cast(); i++ ) { ids.emplace_back( muon_lhcbID( i ).LHCbID() ); }
        std::sort( ids.begin(), ids.end() );
        return ids;
      }
    };

    template <SIMDWrapper::InstructionSet simd, LHCb::Pr::ProxyBehaviour behaviour, typename ContainerType>
    using proxy_type = FittedProxy<simd, behaviour, ContainerType>;

    auto const& unique_id_gen_tag() const { return m_unique_id_gen_tag; }

  private:
    bool       m_backward{false};
    TrackType  m_trackType{TrackType::Unknown};
    FitHistory m_fitHistory{FitHistory::Unknown};

    /// Keep the identifier of the generator used to build this container
    boost::uuids::uuid m_unique_id_gen_tag;

    constexpr int state_set() const {
      switch ( m_trackType ) {
      case TrackType::Unknown:
        return available_states_t<TrackType::Unknown>::set;
      case TrackType::Velo:
        switch ( m_fitHistory ) {
        case FitHistory::Unknown:
          return available_states_t<TrackType::Velo>::set;
        case FitHistory::PrKalmanFilter:
        case FitHistory::TrackMasterFitter:
          return available_states_t<TrackType::Velo, FitHistory::PrKalmanFilter>::set;
        case FitHistory::VeloKalman:
        case FitHistory::Last:
          throw GaudiException( "Not a valid FitHistory.", "LHCb::Event::v3::Tracks::state_set()",
                                StatusCode::FAILURE );
        }
        __builtin_unreachable();
      case TrackType::VeloBackward: // is not used for v3 tracks
        switch ( m_fitHistory ) {
        case FitHistory::Unknown:
          return available_states_t<TrackType::VeloBackward>::set;
        case FitHistory::PrKalmanFilter:
        case FitHistory::TrackMasterFitter:
          return available_states_t<TrackType::VeloBackward, FitHistory::PrKalmanFilter>::set;
        case FitHistory::VeloKalman:
        case FitHistory::Last:
          throw GaudiException( "Not a valid FitHistory.", "LHCb::Event::v3::Tracks::state_set()",
                                StatusCode::FAILURE );
        }
        __builtin_unreachable();
      case TrackType::Long:
        switch ( m_fitHistory ) {
        case FitHistory::Unknown:
          return available_states_t<TrackType::Long>::set;
        case FitHistory::PrKalmanFilter:
        case FitHistory::TrackMasterFitter:
          return available_states_t<TrackType::Long, FitHistory::PrKalmanFilter>::set;
        case FitHistory::VeloKalman:
          return available_states_t<TrackType::Long, FitHistory::VeloKalman>::set;
        case FitHistory::Last:
          throw GaudiException( "Last is not a valid FitHistory.", "LHCb::Event::v3::Tracks::state_set()",
                                StatusCode::FAILURE );
        }
        __builtin_unreachable();
      case TrackType::Upstream:
        switch ( m_fitHistory ) {
        case FitHistory::Unknown:
          return available_states_t<TrackType::Upstream>::set;
        case FitHistory::PrKalmanFilter:
        case FitHistory::TrackMasterFitter:
          return available_states_t<TrackType::Upstream, FitHistory::PrKalmanFilter>::set;
        case FitHistory::VeloKalman:
        case FitHistory::Last:
          throw GaudiException( "Not a valid FitHistory.", "LHCb::Event::v3::Tracks::state_set()",
                                StatusCode::FAILURE );
        }
        __builtin_unreachable();
      case TrackType::Downstream:
        switch ( m_fitHistory ) {
        case FitHistory::Unknown:
          return available_states_t<TrackType::Downstream>::set;
        case FitHistory::PrKalmanFilter:
        case FitHistory::TrackMasterFitter:
          return available_states_t<TrackType::Downstream, FitHistory::PrKalmanFilter>::set;
        case FitHistory::VeloKalman:
        case FitHistory::Last:
          throw GaudiException( "Not a valid FitHistory.", "LHCb::Event::v3::Tracks::state_set()",
                                StatusCode::FAILURE );
        }
        __builtin_unreachable();
      case TrackType::Ttrack:
        switch ( m_fitHistory ) {
        case FitHistory::Unknown:
          return available_states_t<TrackType::Ttrack>::set;
        case FitHistory::PrKalmanFilter:
        case FitHistory::TrackMasterFitter:
          return available_states_t<TrackType::Ttrack, FitHistory::PrKalmanFilter>::set;
        case FitHistory::VeloKalman:
        case FitHistory::Last:
          throw GaudiException( "Not a valid FitHistory.", "LHCb::Event::v3::Tracks::state_set()",
                                StatusCode::FAILURE );
        }
        __builtin_unreachable();
      case TrackType::Muon:
        return available_states_t<TrackType::Muon>::set;
      case TrackType::UT:
        return available_states_t<TrackType::UT>::set;
      case TrackType::SeedMuon:
        return available_states_t<TrackType::SeedMuon>::set;
      case TrackType::VeloMuon:
        return available_states_t<TrackType::VeloMuon>::set;
      case TrackType::MuonUT:
        return available_states_t<TrackType::MuonUT>::set;
      case TrackType::LongMuon:
        return available_states_t<TrackType::LongMuon>::set;
      case TrackType::Last:
        throw GaudiException( "Last is not a valid Track Type.", "LHCb::Event::Enum::Track::Type",
                              StatusCode::FAILURE );
      }

      __builtin_unreachable();
    }

    // TODO: c++20 std::popcount
    constexpr int num_states() const { return __builtin_popcount( static_cast<unsigned>( state_set() ) ); }

    /// Check if the tracks have the information of the given state
    constexpr bool has_state( StateLocation StateLoc ) const {
      return state_set() & ( 1 << static_cast<int>( StateLoc ) );
    }
    // TODO: c++20 std::popcount
    constexpr int state_index( StateLocation StateLoc ) const {
      assert( has_state( StateLoc ) );
      return __builtin_popcount( state_set() & ~( 0xFFFFFFFF << ( static_cast<unsigned>( StateLoc ) + 1 ) ) ) - 1;
    }
  };
} // namespace LHCb::Event::v3
