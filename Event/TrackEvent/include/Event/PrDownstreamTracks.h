/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#pragma once

#include "Event/SIMDEventTypes.h"
#include "Event/SOACollection.h"
#include "Kernel/EventLocalAllocator.h"
#include "Kernel/LHCbID.h"
#include "LHCbMath/MatVec.h"
#include "LHCbMath/SIMDWrapper.h"
#include "LHCbMath/bit_cast.h"
#include "PrSeedTracks.h"
#include "PrTracksTag.h"

/**
 * Downstream track made with a FT seed and UT hits
 */
namespace TracksInfo = LHCb::Pr::TracksInfo;

namespace LHCb::Pr::Downstream {

  struct Tag {
    struct trackSeed : Event::int_field {};
    struct Index : Event::int_field {};
    struct LHCbID : Event::lhcbid_field {};
    struct UTHits : Event::vector_field<Event::struct_field<Index, LHCbID>> {};
    struct FTHits : Event::vector_field<Event::struct_field<Index, LHCbID>> {};
    struct State : Event::state_field {};
    // we only store a single state in the UT here because the seed track ancestor has the EndT state
    static_assert( Event::v3::num_states<Event::Enum::Track::Type::Downstream>() == 1 );

    template <typename T>
    using downstream_t = Event::SOACollection<T, trackSeed, FTHits, UTHits, State>;
  };

  namespace SP = LHCb::Event::StateParameters;

  struct Tracks : Tag::downstream_t<Tracks> {
    using base_t                     = typename Tag::downstream_t<Tracks>;
    using tag_t                      = Tag; // Needed for tag_type_t helper defined in PrTracksTag.h
    constexpr static auto MaxLHCbIDs = TracksInfo::MaxFTHits + TracksInfo::MaxUTHits;
    using base_t::allocator_type;
    Tracks( LHCb::Pr::Seeding::Tracks const* ft_ancestors,
            Zipping::ZipFamilyNumber zipIdentifier = Zipping::generateZipIdentifier(), allocator_type alloc = {} )
        : base_t{std::move( zipIdentifier ), std::move( alloc )}, m_ft_ancestors{ft_ancestors} {}

    // Constructor used by zipping machinery when making a copy of a zip
    Tracks( Zipping::ZipFamilyNumber zn, Tracks const& old )
        : base_t{std::move( zn ), old}, m_ft_ancestors{old.m_ft_ancestors} {}

    // Return pointer to ancestor container
    [[nodiscard]] LHCb::Pr::Seeding::Tracks const* getFTAncestors() const { return m_ft_ancestors; };

    // Define an minimal custom proxy for this track
    template <SIMDWrapper::InstructionSet simd, LHCb::Pr::ProxyBehaviour behaviour, typename ContainerType>
    struct DownstreamProxy : Event::Proxy<simd, behaviour, ContainerType> {
      using base_t = typename Event::Proxy<simd, behaviour, ContainerType>;
      using base_t::base_t;
      using base_t::width;

      [[nodiscard]] auto qOverP() const { return this->template get<Tag::State>().qOverP(); }
      [[nodiscard]] auto seed_track_index() const { return this->template get<Tag::trackSeed>(); }
      [[nodiscard]] auto p() const { return abs( 1.0 / qOverP() ); }
      [[nodiscard]] auto nUTHits() const { return this->template field<Tag::UTHits>().size(); }
      [[nodiscard]] auto nFTHits() const { return this->template field<Tag::FTHits>().size(); }
      [[nodiscard]] auto nHits() const { return nUTHits() + nFTHits(); }
      [[nodiscard]] auto ut_index( std::size_t i ) const {
        return this->template field<Tag::UTHits>()[i].template get<Tag::Index>();
      }
      [[nodiscard]] auto ft_index( std::size_t i ) const {
        return this->template field<Tag::FTHits>()[i].template get<Tag::Index>();
      }
      [[nodiscard]] auto ut_lhcbID( std::size_t i ) const {
        return this->template field<Tag::UTHits>()[i].template get<Tag::LHCbID>();
      }
      [[nodiscard]] auto ft_lhcbID( std::size_t i ) const {
        return this->template field<Tag::FTHits>()[i].template get<Tag::LHCbID>();
      }

      [[nodiscard, gnu::always_inline]] auto StatePosDir( [[maybe_unused]] Event::Enum::State::Location loc ) const {
        assert( stateIndex<Event::Enum::Track::Type::Downstream>( loc ) >= 0 );
        return this->template get<Tag::State>();
      }

      //  Retrieve the (sorted) set of LHCbIDs
      [[nodiscard]] std::vector<LHCbID> lhcbIDs() const {
        static_assert( width() == 1, "lhcbIDs() method cannot be used on vector proxies" );
        std::vector<LHCbID> ids;
        ids.reserve( Tracks::MaxLHCbIDs );
        for ( auto i = 0; i < nUTHits().cast(); i++ ) { ids.emplace_back( ut_lhcbID( i ).LHCbID() ); }
        for ( auto i = 0; i < nFTHits().cast(); i++ ) { ids.emplace_back( ft_lhcbID( i ).LHCbID() ); }
        std::sort( ids.begin(), ids.end() );
        return ids;
      }
    };
    template <SIMDWrapper::InstructionSet simd, LHCb::Pr::ProxyBehaviour behaviour, typename ContainerType>
    using proxy_type = DownstreamProxy<simd, behaviour, ContainerType>;

  private:
    LHCb::Pr::Seeding::Tracks const* m_ft_ancestors{nullptr};
  };
} // namespace LHCb::Pr::Downstream
