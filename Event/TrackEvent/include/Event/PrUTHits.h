/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once

#include "PrTracksTag.h"

#include "Event/SOACollection.h"
#include "Kernel/EventLocalAllocator.h"
#include "LHCbMath/SIMDWrapper.h"
#include "UTDAQ/UTInfo.h"

#include "Detector/UT/ChannelID.h"
#include "Kernel/LHCbID.h"

/** @class PrUTHits PrUTHits.h
 *  SoA Implementation of Upstream tracker hit for pattern recognition
 *  @author Michel De Cian, based on Arthur Hennequin's PrVeloHits
 *  @date   2019-11-07
 */

namespace LHCb::Pr::UT {

  namespace UTHitsTag {

    struct channelID : Event::int_field {};
    struct weight : Event::float_field {};
    struct xAtYEq0 : Event::float_field {};
    struct yBegin : Event::float_field {};
    struct yEnd : Event::float_field {};
    struct zAtYEq0 : Event::float_field {};
    struct dxDy : Event::float_field {};
    struct cos : Event::float_field {};
    struct fracStrip : Event::float_field {};
    struct clusterSize : Event::int_field {};
    struct clusterCharge : Event::int_field {};

    template <typename T>
    using uthit_t = Event::SOACollection<T, channelID, weight, xAtYEq0, yBegin, yEnd, zAtYEq0, dxDy, cos, fracStrip,
                                         clusterSize, clusterCharge>;
  } // namespace UTHitsTag

  namespace details {

    struct Hits : UTHitsTag::uthit_t<Hits> {
      using base_t = typename UTHitsTag::uthit_t<Hits>;
      using base_t::base_t;

      // Method to add Hit in the container
      // This should be changed to float, but needs an adaptation of "DeUTSector"
      void emplace_back( const DeUTSector& aSector, unsigned int fullChanIdx, unsigned int strip, double fracStrip,
                         LHCb::Detector::UT::ChannelID chanID, unsigned int clusterSize, bool /*highThreshold*/,
                         unsigned int clusterCharge ) {
        double dxDy{0};
        double dzDy{0};
        double xAtYEq0{0};
        double zAtYEq0{0};
        double yBegin{0};
        double yEnd{0};
        //--- this method allow to set the values
        aSector.trajectory( strip, fracStrip, dxDy, dzDy, xAtYEq0, zAtYEq0, yBegin, yEnd );
        const auto cos = aSector.cosAngle();
        // -- should this not depend on the cluster size?
        const auto pitch          = aSector.pitch();
        const auto hitErrorFactor = aSector.hitErrorFactor( clusterSize );
        // silicon strip sensor has intrinsic resolution = pitch / sqrt(12):
        // https://www.sciencedirect.com/science/article/abs/pii/016890029390255G
        const auto weight = 12.0 / ( pitch * pitch ) / ( hitErrorFactor * hitErrorFactor );

        // NB : The way the indices are setup here assumes all hits for a given
        //      station, layer, region and sector come in order, which appears
        //      to be the case. This must remain so...
        //
        //      Currently, what I am seeing from the MC has this sorting.
        //      But this would also need to be the case for real data.

        // get the indices for this region
        auto& indices = m_indices[fullChanIdx];

        // if first for this range, set the begin and end indices
        if ( &indices != m_last_indices ) {
          // check to see if thi range has been filled previously.
          // If it has, assumed ordering is broken
          assert( indices.first == indices.second );
          // reset indices to current end of container
          indices = {size(), size()};
          // update used last index cache
          m_last_indices = &indices;
        }

        // scalar for the moment
        auto proxy = base_t::emplace_back<SIMDWrapper::InstructionSet::Scalar>();
        proxy.field<UTHitsTag::channelID>().set( (int)chanID );
        proxy.field<UTHitsTag::weight>().set( (float)weight );
        proxy.field<UTHitsTag::xAtYEq0>().set( (float)xAtYEq0 );
        proxy.field<UTHitsTag::zAtYEq0>().set( (float)zAtYEq0 );
        proxy.field<UTHitsTag::yBegin>().set( (float)yBegin );
        proxy.field<UTHitsTag::yEnd>().set( (float)yEnd );
        proxy.field<UTHitsTag::cos>().set( (float)cos );
        proxy.field<UTHitsTag::dxDy>().set( (float)dxDy );
        proxy.field<UTHitsTag::fracStrip>().set( (float)fracStrip );
        proxy.field<UTHitsTag::clusterSize>().set( (int)clusterSize );
        proxy.field<UTHitsTag::clusterCharge>().set( (int)clusterCharge );

        // increment the end index for current range
        ++( indices.second );
      }

      // -- Add correct floating point values to the end
      // -- The just need to be valid floating point values and not meaningful
      // -- in the physics sense. This is to avoid floating point errors
      void addPadding() {
        using simd = SIMDWrapper::best::types;
        // -- size is not increased with "store"
        store<UTHitsTag::channelID>( size(), simd::int_v{1} );
        store<UTHitsTag::weight>( size(), simd::float_v{1e-6f} );
        store<UTHitsTag::xAtYEq0>( size(), simd::float_v{1e6f} );
        store<UTHitsTag::zAtYEq0>( size(), simd::float_v{1e6f} );
        store<UTHitsTag::yBegin>( size(), simd::float_v{1e6f} );
        store<UTHitsTag::yEnd>( size(), simd::float_v{1e6f} );
        store<UTHitsTag::cos>( size(), simd::float_v{1e6f} );
        store<UTHitsTag::dxDy>( size(), simd::float_v{1e6f} );
        store<UTHitsTag::fracStrip>( size(), simd::float_v{1e6f} );
        store<UTHitsTag::clusterSize>( size(), simd::int_v{1} );
        store<UTHitsTag::clusterCharge>( size(), simd::int_v{1} );
      }

      // TODO: isn't there a function for that?
      void copyHit( unsigned int fullChanIdx, int at, const Hits& allhits ) {
        auto& indices = m_indices[fullChanIdx];
        if ( &indices != m_last_indices ) {
          assert( indices.first == indices.second );
          indices        = {size(), size()};
          m_last_indices = &indices;
        }

        // scalar for the moment
        const auto ahits = allhits.scalar();

        auto proxy = base_t::emplace_back<SIMDWrapper::InstructionSet::Scalar>();
        proxy.field<UTHitsTag::channelID>().set( ahits[at].get<UTHitsTag::channelID>() );
        proxy.field<UTHitsTag::weight>().set( ahits[at].get<UTHitsTag::weight>() );
        proxy.field<UTHitsTag::xAtYEq0>().set( ahits[at].get<UTHitsTag::xAtYEq0>() );
        proxy.field<UTHitsTag::zAtYEq0>().set( ahits[at].get<UTHitsTag::zAtYEq0>() );
        proxy.field<UTHitsTag::yBegin>().set( ahits[at].get<UTHitsTag::yBegin>() );
        proxy.field<UTHitsTag::yEnd>().set( ahits[at].get<UTHitsTag::yEnd>() );
        proxy.field<UTHitsTag::cos>().set( ahits[at].get<UTHitsTag::cos>() );
        proxy.field<UTHitsTag::dxDy>().set( ahits[at].get<UTHitsTag::dxDy>() );
        proxy.field<UTHitsTag::fracStrip>().set( ahits[at].get<UTHitsTag::fracStrip>() );
        proxy.field<UTHitsTag::clusterSize>().set( ahits[at].get<UTHitsTag::clusterSize>() );
        proxy.field<UTHitsTag::clusterCharge>().set( ahits[at].get<UTHitsTag::clusterCharge>() );

        ++( indices.second );
      }
      const std::pair<int, int> indices( const int fullChanIdx ) const { return m_indices[fullChanIdx]; }

      int nHits() const { return size(); }

      auto& clear() {
        m_indices.fill( {0, 0} );
        m_last_indices = nullptr;
        return *this;
      }

      LHCb::Detector::UT::ChannelID id( int index ) const {
        assert( index < static_cast<int>( nHits() ) );
        return LHCb::Detector::UT::ChannelID(
            static_cast<unsigned int>( base_t::scalar()[index].get<UTHitsTag::channelID>().cast() ) );
      }
      LHCb::LHCbID lhcbid( int index ) const {
        assert( index < static_cast<int>( nHits() ) );
        return LHCb::LHCbID{id( index )};
      }

    private:
      using HitIndices = std::pair<std::size_t, std::size_t>;
      using HitsInUT   = std::array<HitIndices, UTInfo::MaxNumberOfSectors>;
      // Indices for each range
      HitsInUT m_indices;
      // cache pointer to last indices used
      HitIndices* m_last_indices{nullptr};
    };

  } // namespace details
} // namespace LHCb::Pr::UT
