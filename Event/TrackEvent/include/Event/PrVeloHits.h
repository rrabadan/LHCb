/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once

#include "Event/SIMDEventTypes.h"
#include "Event/SOACollection.h"
#include "Kernel/EventLocalAllocator.h"
#include "LHCbMath/MatVec.h"
#include "LHCbMath/SIMDWrapper.h"

/**
 * Hits in VP
 *
 * @author: Arthur Hennequin
 */
namespace LHCb::Pr::VP {

  namespace VPHitsTag {
    struct pos : Event::Vec_field<3> {};
    struct ChannelId : Event::int_field {};

    template <typename T>
    using velohit_t = Event::SOACollection<T, pos, ChannelId>;
  } // namespace VPHitsTag

  namespace details {

    struct Hits : VPHitsTag::velohit_t<Hits> {
      using base_t = typename VPHitsTag::velohit_t<Hits>;
      using base_t::base_t;
    };

  } // namespace details

} // namespace LHCb::Pr::VP
