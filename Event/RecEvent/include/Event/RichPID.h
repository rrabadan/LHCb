/*****************************************************************************\
* (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once

// Include files
#include "Event/Track.h"
#include "GaudiKernel/KeyedContainer.h"
#include "GaudiKernel/KeyedObject.h"
#include "GaudiKernel/SharedObjectsContainer.h"
#include "GaudiKernel/SmartRef.h"
#include "Kernel/RichParticleIDType.h"
#include "Kernel/RichRadiatorType.h"
#include "gsl/gsl_sf_erf.h"
#include <cmath>
#include <ostream>
#include <utility>
#include <vector>

// Forward declarations

namespace LHCb {

  // Forward declarations

  // Class ID definition
  static const CLID CLID_RichPID = 12402;

  // Namespace for locations in TDS
  namespace RichPIDLocation {
    inline static const std::string Default = "Rec/Rich/PIDs";
    inline static const std::string Offline = Default;
  } // namespace RichPIDLocation

  /** @class RichPID RichPID.h
   *
   * RICH particle identification information for a given track object
   *
   * @author Chris Jones   Christopher.Rob.Jones@cern.ch
   *
   */

  class RichPID : public KeyedObject<int> {
  public:
    // Types for containers of PIDs
    using Vector      = std::vector<RichPID*>;
    using ConstVector = std::vector<const RichPID*>;
    using Container   = KeyedContainer<RichPID, Containers::HashMap>;
    using Selection   = SharedObjectsContainer<RichPID>;
    using Range       = Gaudi::Range_<ConstVector>;
    // Data types
    using FltType = float;
    using DLL     = FltType;
    using DLLs    = std::vector<DLL>;

  public:
    /// Constructor from data fields
    RichPID( const Rich::ParticleIDType pid, const LHCb::Track* track, DLLs&& dllValues )
        : m_particleLLValues( std::forward<DLLs>( dllValues ) ), m_track( track ) {
      setBestParticleID( pid );
    }

    /// Copy constructor
    RichPID( const LHCb::RichPID& pid )
        : KeyedObject<int>()
        , m_pidResultCode( pid.m_pidResultCode )
        , m_particleLLValues( pid.m_particleLLValues )
        , m_track( pid.m_track ) {}

    /// Move constructor
    RichPID( LHCb::RichPID&& pid )
        : KeyedObject<int>()
        , m_pidResultCode( pid.m_pidResultCode )
        , m_particleLLValues( std::move( pid.m_particleLLValues ) )
        , m_track( pid.m_track ) {}

    /// Default Constructor
    RichPID() = default;

  public:
    // Retrieve pointer to class definition structure
    const CLID&        clID() const override { return LHCb::RichPID::classID(); }
    static const CLID& classID() { return CLID_RichPID; }

  public:
    /// Returns the raw gaussian probability value for a given particle species
    FltType particleRawProb( const Rich::ParticleIDType type ) const {
      const auto dll = particleDeltaLL( type );
      return ( dll > 0 ? 1.0F - (FltType)gsl_sf_erf( std::sqrt( dll ) ) : 1.0F );
    }

    /// Returns the normalised gaussian probability value for a given particle species
    FltType particleNormProb( const Rich::ParticleIDType type ) const {
      FltType norm = 0;
      for ( const auto pid : Rich::Particles() ) { norm += particleRawProb( pid ); }
      return ( norm > 0 ? particleRawProb( type ) / norm : 0 );
    }

    /// Returns the delta LL value for a given hypothesis
    DLL particleDeltaLL( const Rich::ParticleIDType type ) const noexcept { return m_particleLLValues[type]; }

    /// Sets the particle delta LL value for the given hypothesis
    void setParticleDeltaLL( const Rich::ParticleIDType type, const DLL deltaLL ) noexcept {
      m_particleLLValues[type] = deltaLL;
    }

    /// Sets the particle delta LL value for the given hypothesis
    void setParticleLLValues( DLLs&& value ) {
      assert( value.size() == m_particleLLValues.size() );
      m_particleLLValues = std::move( value );
    }

    /// Boolean method to check if the given radiator was used to create this PID result (i.e. if the associated track
    /// was found to traverse the radiator in a manner that would have produced detectable Cherenkov photons
    bool traversedRadiator( const Rich::RadiatorType radiator ) const noexcept {
      return ( Rich::Aerogel == radiator ? this->usedAerogel() :       //
                   Rich::Rich1Gas == radiator ? this->usedRich1Gas() : //
                       Rich::Rich2Gas == radiator ? this->usedRich2Gas() : false );
    }

    /// Verify if a given hypothesis was above threshold and the associated track present in any radiator
    bool isAboveThreshold( const Rich::ParticleIDType type ) const;

    /// Set a given hypothesis above threshold and the associated track present in any radiator
    void setAboveThreshold( const Rich::ParticleIDType type, const bool flag );

    /// Returns the signed sigma separation beween 2 particle hypotheses (first relative to last)
    FltType nSigmaSeparation( const Rich::ParticleIDType firstPart, const Rich::ParticleIDType lastPart ) const {
      const auto dLL = m_particleLLValues[lastPart] - m_particleLLValues[firstPart];
      return std::sqrt( std::abs( 2.0F * dLL ) ) * ( dLL > 0 ? 1.0F : -1.0F );
    }

    /// Returns true if the given mass hypothesis is within the given number of sigmas separation from the best PID type
    bool isConsistentNSigma( const Rich::ParticleIDType type, const FltType nsigma ) const {
      return ( nSigmaSeparation( bestParticleID(), type ) > nsigma );
    }

    /// The best Particle ID
    Rich::ParticleIDType bestParticleID() const noexcept {
      // Shift by -1 to convert packed representation to Rich::ParticleIDType
      return ( Rich::ParticleIDType )( ( ( m_pidResultCode & packedBestParticleIDMask ) >> packedBestParticleIDBits ) -
                                       1 );
    }

    /// set the best particle ID
    void setBestParticleID( const Rich::ParticleIDType type ) noexcept {
      // Shift bit-packed representation by +1 to start numbering from 0
      const auto val = (unsigned int)type + 1;
      m_pidResultCode &= ~packedBestParticleIDMask;
      m_pidResultCode |= ( ( ( (unsigned int)val ) << packedBestParticleIDBits ) & packedBestParticleIDMask );
    }

    /// Print this RichPID data object in a human readable way
    std::ostream& fillStream( std::ostream& s ) const override;

    /// Retrieve const  Bit-packed information (Best particle ID and History) for the RichPID result
    unsigned int pidResultCode() const noexcept { return m_pidResultCode; }

    /// Update  Bit-packed information (Best particle ID and History) for the RichPID result
    void setPidResultCode( const unsigned int value ) noexcept { m_pidResultCode = value; }

    /// Retrieve Information from aerogel was used to form this PID result
    bool usedAerogel() const noexcept { return test<usedAerogelMask>(); }

    /// Update Information from aerogel was used to form this PID result
    void setUsedAerogel( const bool value ) noexcept { set<usedAerogelMask>( value ); }

    /// Retrieve Information from Rich1 gas was used to form this PID result
    bool usedRich1Gas() const noexcept { return test<usedRich1GasMask>(); }

    /// Update Information from Rich1 gas was used to form this PID result
    void setUsedRich1Gas( const bool value ) noexcept { set<usedRich1GasMask>( value ); }

    /// Retrieve Information from Rich2 gas was used to form this PID result
    bool usedRich2Gas() const noexcept { return test<usedRich2GasMask>(); }

    /// Update Information from Rich2 gas was used to form this PID result
    void setUsedRich2Gas( const bool value ) noexcept { set<usedRich2GasMask>( value ); }

    /// Retrieve The electron hypothesis is above threshold in at least one active radiator
    bool electronHypoAboveThres() const noexcept { return test<electronHypoAboveThresMask>(); }

    /// Update The electron hypothesis is above threshold in at least one active radiator
    void setElectronHypoAboveThres( const bool value ) noexcept { set<electronHypoAboveThresMask>( value ); }

    /// Retrieve The muon hypothesis is above threshold in at least one active radiator
    bool muonHypoAboveThres() const noexcept { return test<muonHypoAboveThresMask>(); }

    /// Update The muon hypothesis is above threshold in at least one active radiator
    void setMuonHypoAboveThres( const bool value ) noexcept { set<muonHypoAboveThresMask>( value ); }

    /// Retrieve The pion hypothesis is above threshold in at least one active radiator
    bool pionHypoAboveThres() const noexcept { return test<pionHypoAboveThresMask>(); }

    /// Update The pion hypothesis is above threshold in at least one active radiator
    void setPionHypoAboveThres( const bool value ) noexcept { set<pionHypoAboveThresMask>( value ); }

    /// Retrieve The kaon hypothesis is above threshold in at least one active radiator
    bool kaonHypoAboveThres() const noexcept { return test<kaonHypoAboveThresMask>(); }

    /// Update The kaon hypothesis is above threshold in at least one active radiator
    void setKaonHypoAboveThres( const bool value ) noexcept { set<kaonHypoAboveThresMask>( value ); }

    /// Retrieve The proton hypothesis is above threshold in at least one active radiator
    bool protonHypoAboveThres() const noexcept { return test<protonHypoAboveThresMask>(); }

    /// Update The proton hypothesis is above threshold in at least one active radiator
    void setProtonHypoAboveThres( const bool value ) noexcept { set<protonHypoAboveThresMask>( value ); }

    /// Retrieve The deuteron hypothesis is above threshold in at least one active radiator
    bool deuteronHypoAboveThres() const noexcept { return test<deuteronHypoAboveThresMask>(); }

    /// Update The deuteron hypothesis is above threshold in at least one active radiator
    void setDeuteronHypoAboveThres( const bool value ) noexcept { set<deuteronHypoAboveThresMask>( value ); }

    /// Retrieve non-const  Vector of particle hypothesis log likelihood values
    DLLs& particleLLValues() noexcept { return m_particleLLValues; }

    /// Retrieve const  Vector of particle hypothesis log likelihood values
    const DLLs& particleLLValues() const noexcept { return m_particleLLValues; }

    /// Update  Vector of particle hypothesis log likelihood values
    void setParticleLLValues( const DLLs& value ) noexcept { m_particleLLValues = value; }

    /// Retrieve (const)  Associated reconstructed Track
    const LHCb::Track* track() const noexcept { return m_track; }

    /// Update  Associated reconstructed Track
    void setTrack( SmartRef<LHCb::Track> value ) noexcept { m_track = std::move( value ); }

  public:
    /// overload ostream output
    friend std::ostream& operator<<( std::ostream& str, const RichPID& obj ) { return obj.fillStream( str ); }

  public:
    // For Combined DLLs

    /// Apply scaling to given DLL value for given PID type
    static float scaledDLLForCombDLL( const Rich::ParticleIDType type, const float dll ) {
      // Ad-hoc scalings for DLLs when combined with other systems in CombDLLs
      switch ( type ) {
      case Rich::Electron:
        return ( 7.0 * tanh( dll / 40.0 ) );
      case Rich::Muon:
        return ( 7.0 * tanh( dll / 5.0 ) );
      default:
        return dll;
      }
    }

    /// Access the 'scaled for CombDLL' value for a given PID type
    float scaledDLLForCombDLL( const Rich::ParticleIDType type ) const {
      return scaledDLLForCombDLL( type, particleDeltaLL( type ) );
    }

  private:
    template <unsigned int mask>
    void set( bool value ) noexcept {
      m_pidResultCode = ( m_pidResultCode & ~mask ) | ( -value & mask );
    }
    template <unsigned int mask>
    bool test() const noexcept {
      return ( m_pidResultCode & mask ) != 0;
    }

    /// Offsets of bitfield pidResultCode
    enum pidResultCodeBits {
      packedBestParticleIDBits = 0,
    };

    /// Bitmasks for bitfield pidResultCode
    enum pidResultCodeMasks : unsigned int {
      packedBestParticleIDMask   = 0xfU,
      usedAerogelMask            = 0x10U,
      usedRich1GasMask           = 0x20U,
      usedRich2GasMask           = 0x40U,
      electronHypoAboveThresMask = 0x80U,
      muonHypoAboveThresMask     = 0x100U,
      pionHypoAboveThresMask     = 0x200U,
      kaonHypoAboveThresMask     = 0x400U,
      protonHypoAboveThresMask   = 0x800U,
      // Redundant types removed here ...
      deuteronHypoAboveThresMask = 0x20000U
    };

    /// Bit-packed information (Best particle ID and History) for the RichPID result
    unsigned int m_pidResultCode{0};

    /// Vector of particle hypothesis log likelihood values
    DLLs m_particleLLValues = DLLs( Rich::NParticleTypes, 0.0 );

    /// Associated reconstructed Track
    SmartRef<LHCb::Track> m_track;

  }; // class RichPID

  /// Definition of Keyed Container for RichPID
  using RichPIDs = RichPID::Container;

} // namespace LHCb
