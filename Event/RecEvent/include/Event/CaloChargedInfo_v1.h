/*****************************************************************************\
* (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once
#include "Detector/Calo/CaloCellID.h"
#include "Event/Track.h"
#include "GaudiKernel/KeyedContainer.h"
#include "GaudiKernel/KeyedObject.h"
#include "GaudiKernel/SharedObjectsContainer.h"
#include "GaudiKernel/SmartRef.h"
#include <functional>
#include <vector>

namespace LHCb::Event::Calo {

  inline namespace v1 {

    // Class ID definitions
    static const CLID CLID_CaloChargedPID = 2005;
    static const CLID CLID_BremInfo       = 2006;

    // locations (needed for packing)
    namespace CaloChargedPIDLocation {
      inline const std::string Default = "";
    } // namespace CaloChargedPIDLocation
    namespace BremInfoLocation {
      inline const std::string Default = "";
    } // namespace BremInfoLocation

    // class for Calo Charged PID info
    class CaloChargedPID final : public KeyedObject<int> {
      using CellID = LHCb::Detector::Calo::CellID;

    public:
      using Vector      = std::vector<CaloChargedPID*>;
      using ConstVector = std::vector<const CaloChargedPID*>;

      /// typedef for KeyedContainer of CaloChargedPID
      typedef KeyedContainer<CaloChargedPID, Containers::HashMap> Container;

      using Selection = SharedObjectsContainer<CaloChargedPID>;
      using Range     = Gaudi::Range_<ConstVector>;

      // constructors
      CaloChargedPID( CaloChargedPID const& pid )
          : KeyedObject<int>()
          , m_InEcal( pid.m_InEcal )
          , m_ClusterID( pid.m_ClusterID )
          , m_ClusterMatch( pid.m_ClusterMatch )
          , m_ElectronID( pid.m_ElectronID )
          , m_ElectronMatch( pid.m_ElectronMatch )
          , m_ElectronEnergy( pid.m_ElectronEnergy )
          , m_ElectronShowerEoP( pid.m_ElectronShowerEoP )
          , m_ElectronShowerDLL( pid.m_ElectronShowerDLL )
          , m_EcalPIDe( pid.m_EcalPIDe )
          , m_EcalPIDmu( pid.m_EcalPIDmu )
          , m_InHcal( pid.m_InHcal )
          , m_HcalEoP( pid.m_HcalEoP )
          , m_HcalPIDe( pid.m_HcalPIDe )
          , m_HcalPIDmu( pid.m_HcalPIDmu )
          , m_IDTrack( pid.m_IDTrack ) {}

      CaloChargedPID() = default;

      // Retrieve pointer to class definition structure
      const CLID&        clID() const override { return classID(); }
      static const CLID& classID() { return CLID_CaloChargedPID; }

      // related track
      const LHCb::Track* idTrack() const { return m_IDTrack; }
      void               setIDTrack( SmartRef<LHCb::Track> value ) { m_IDTrack = std::move( value ); }

      /// retreive const parameters
      // Ecal
      bool   InEcal() const { return m_InEcal; }
      CellID ClusterID() const { return m_ClusterID; }
      float  ClusterMatch() const { return m_ClusterMatch; }
      CellID ElectronID() const { return m_ElectronID; }
      float  ElectronMatch() const { return m_ElectronMatch; }
      float  ElectronEnergy() const { return m_ElectronEnergy; }
      float  ElectronShowerEoP() const { return m_ElectronShowerEoP; }
      float  ElectronShowerDLL() const { return m_ElectronShowerDLL; }
      float  EcalPIDe() const { return m_EcalPIDe; }
      float  EcalPIDmu() const { return m_EcalPIDmu; }
      // Hcal
      bool  InHcal() const { return m_InHcal; }
      float HcalEoP() const { return m_HcalEoP; }
      float HcalPIDe() const { return m_HcalPIDe; }
      float HcalPIDmu() const { return m_HcalPIDmu; }

      /// setting function for v3 tracks related pid objects
      template <typename ProxyV3>
      void setParametersFromV3( ProxyV3 const& proxy ) {
        // Ecal
        m_InEcal            = proxy.InEcal().cast();
        m_ClusterID         = proxy.ClusterID();
        m_ClusterMatch      = proxy.ClusterMatch().cast();
        m_ElectronID        = proxy.ElectronID();
        m_ElectronMatch     = proxy.ElectronMatch().cast();
        m_ElectronEnergy    = proxy.ElectronEnergy().cast();
        m_ElectronShowerEoP = proxy.ElectronShowerEoP().cast();
        m_ElectronShowerDLL = proxy.ElectronShowerDLL().cast();
        m_EcalPIDe          = proxy.EcalPIDe().cast();
        m_EcalPIDmu         = proxy.EcalPIDmu().cast();
        // Hcal
        m_InHcal    = proxy.InHcal().cast();
        m_HcalEoP   = proxy.HcalEoP().cast();
        m_HcalPIDe  = proxy.HcalPIDe().cast();
        m_HcalPIDmu = proxy.HcalPIDmu().cast();
      }

      // setters for (un)packer
      CaloChargedPID& setInEcal( bool value ) noexcept {
        m_InEcal = value;
        return *this;
      }
      CaloChargedPID& setClusterID( CellID value ) noexcept {
        m_ClusterID = value;
        return *this;
      }
      CaloChargedPID& setClusterMatch( float value ) noexcept {
        m_ClusterMatch = value;
        return *this;
      }
      CaloChargedPID& setElectronID( CellID value ) noexcept {
        m_ElectronID = value;
        return *this;
      }
      CaloChargedPID& setElectronMatch( float value ) noexcept {
        m_ElectronMatch = value;
        return *this;
      }
      CaloChargedPID& setElectronEnergy( float value ) noexcept {
        m_ElectronEnergy = value;
        return *this;
      }
      CaloChargedPID& setElectronShowerEoP( float value ) noexcept {
        m_ElectronShowerEoP = value;
        return *this;
      }
      CaloChargedPID& setElectronShowerDLL( float value ) noexcept {
        m_ElectronShowerDLL = value;
        return *this;
      }
      CaloChargedPID& setEcalPIDe( float value ) noexcept {
        m_EcalPIDe = value;
        return *this;
      }
      CaloChargedPID& setEcalPIDmu( float value ) noexcept {
        m_EcalPIDmu = value;
        return *this;
      }
      CaloChargedPID& setInHcal( bool value ) noexcept {
        m_InHcal = value;
        return *this;
      }
      CaloChargedPID& setHcalEoP( float value ) noexcept {
        m_HcalEoP = value;
        return *this;
      }
      CaloChargedPID& setHcalPIDe( float value ) noexcept {
        m_HcalPIDe = value;
        return *this;
      }
      CaloChargedPID& setHcalPIDmu( float value ) noexcept {
        m_HcalPIDmu = value;
        return *this;
      }

    private:
      // Ecal
      bool   m_InEcal{false};
      CellID m_ClusterID{};
      float  m_ClusterMatch{-1.f};
      CellID m_ElectronID{};
      float  m_ElectronMatch{-1.f};
      float  m_ElectronEnergy{0.f};
      float  m_ElectronShowerEoP{-1.f};
      float  m_ElectronShowerDLL{0.f};
      float  m_EcalPIDe{0.f};
      float  m_EcalPIDmu{0.f};
      // Hcal
      bool  m_InHcal{false};
      float m_HcalEoP{0.f};
      float m_HcalPIDe{0.f};
      float m_HcalPIDmu{0.f};
      // related track
      SmartRef<LHCb::Track> m_IDTrack;

    }; // class CaloChargedPID

    // class bremsstrahlung information
    class BremInfo final : public KeyedObject<int> {
      using CellID = LHCb::Detector::Calo::CellID;

    public:
      using Vector      = std::vector<BremInfo*>;
      using ConstVector = std::vector<const BremInfo*>;

      /// typedef for KeyedContainer of BremInfo
      typedef KeyedContainer<BremInfo, Containers::HashMap> Container;

      using Selection = SharedObjectsContainer<BremInfo>;
      using Range     = Gaudi::Range_<ConstVector>;

      // constructors
      BremInfo( BremInfo const& pid )
          : KeyedObject<int>()
          , m_InBrem( pid.m_InBrem )
          , m_BremHypoID( pid.m_BremHypoID )
          , m_BremHypoMatch( pid.m_BremHypoMatch )
          , m_BremHypoEnergy( pid.m_BremHypoEnergy )
          , m_BremHypoDeltaX( pid.m_BremHypoDeltaX )
          , m_BremTrackBasedEnergy( pid.m_BremTrackBasedEnergy )
          , m_BremBendingCorrection( pid.m_BremBendingCorrection )
          , m_HasBrem( pid.m_HasBrem )
          , m_BremEnergy( pid.m_BremEnergy )
          , m_BremPIDe( pid.m_BremPIDe )
          , m_IDTrack( pid.m_IDTrack ) {}

      BremInfo() = default;

      // Retrieve pointer to class definition structure
      const CLID&        clID() const override { return classID(); }
      static const CLID& classID() { return CLID_BremInfo; }

      // related track
      const LHCb::Track* idTrack() const { return m_IDTrack; }
      BremInfo&          setIDTrack( SmartRef<LHCb::Track> value ) {
        m_IDTrack = std::move( value );
        return *this;
      }

      /// retreive const parameters
      bool   InBrem() const { return m_InBrem; }
      CellID BremHypoID() const { return m_BremHypoID; }
      float  BremHypoMatch() const { return m_BremHypoMatch; }
      float  BremHypoEnergy() const { return m_BremHypoEnergy; }
      float  BremHypoDeltaX() const { return m_BremHypoDeltaX; }
      float  BremTrackBasedEnergy() const { return m_BremTrackBasedEnergy; }
      float  BremBendingCorrection() const { return m_BremBendingCorrection; }
      bool   HasBrem() const { return m_HasBrem; }
      float  BremEnergy() const { return m_BremEnergy; }
      float  BremPIDe() const { return m_BremPIDe; }

      /// setting function for v3 tracks related pid objects
      template <typename ProxyV3>
      BremInfo& operator=( ProxyV3 const& proxy ) {
        m_InBrem                = proxy.InBrem().cast();
        m_BremHypoID            = proxy.BremHypoID();
        m_BremHypoMatch         = proxy.BremHypoMatch().cast();
        m_BremHypoEnergy        = proxy.BremHypoEnergy().cast();
        m_BremHypoDeltaX        = proxy.BremHypoDeltaX().cast();
        m_BremTrackBasedEnergy  = proxy.BremTrackBasedEnergy().cast();
        m_BremBendingCorrection = proxy.BremBendingCorrection().cast();
        m_HasBrem               = proxy.HasBrem().cast();
        m_BremEnergy            = proxy.BremEnergy().cast();
        m_BremPIDe              = proxy.BremPIDe().cast();
        return *this;
      }
      template <typename ProxyV3>
      /* [[deprecated("please use = instead)]] */ BremInfo& setParametersFromV3( ProxyV3 const& proxy ) {
        return *this = proxy;
      }

      // setters for (un)packer
      BremInfo& setInBrem( bool value ) noexcept {
        m_InBrem = value;
        return *this;
      }
      BremInfo& setBremHypoID( CellID value ) noexcept {
        m_BremHypoID = value;
        return *this;
      }
      BremInfo& setBremHypoMatch( float value ) noexcept {
        m_BremHypoMatch = value;
        return *this;
      }
      BremInfo& setBremHypoEnergy( float value ) noexcept {
        m_BremHypoEnergy = value;
        return *this;
      }
      BremInfo& setBremHypoDeltaX( float value ) noexcept {
        m_BremHypoDeltaX = value;
        return *this;
      }
      BremInfo& setBremTrackBasedEnergy( float value ) noexcept {
        m_BremTrackBasedEnergy = value;
        return *this;
      }
      BremInfo& setBremBendingCorrection( float value ) noexcept {
        m_BremBendingCorrection = value;
        return *this;
      }
      BremInfo& setHasBrem( bool value ) noexcept {
        m_HasBrem = value;
        return *this;
      }
      BremInfo& setBremEnergy( float value ) noexcept {
        m_BremEnergy = value;
        return *this;
      }
      BremInfo& setBremPIDe( float value ) noexcept {
        m_BremPIDe = value;
        return *this;
      }

    private:
      bool                  m_InBrem{false};
      CellID                m_BremHypoID{};
      float                 m_BremHypoMatch{-1.f};
      float                 m_BremHypoEnergy{-1.f};
      float                 m_BremHypoDeltaX{-1000.f};
      float                 m_BremTrackBasedEnergy{-1.f};
      float                 m_BremBendingCorrection{1.f};
      bool                  m_HasBrem{false};
      float                 m_BremEnergy{0.f};
      float                 m_BremPIDe{0.f};
      SmartRef<LHCb::Track> m_IDTrack;

    }; // class BremInfo

    /// Definition of Keyed Containers
    using CaloChargedPIDs = CaloChargedPID::Container;
    using BremInfos       = BremInfo::Container;

  } // namespace v1
} // namespace LHCb::Event::Calo

/* hash builders are used to create a unique identifier for storing underlying PID objects in
 * an anonymous but uniquely identifiable way in the TES for different ProtoParticle containers
 */
namespace CaloObjectHash {
  template <auto N>
  std::size_t combine( const std::array<std::size_t, N>& hashes ) {
    return std::accumulate( hashes.begin(), hashes.end(), std::size_t{0}, []( std::size_t i, std::size_t h ) {
      return i ^ ( h + 0x9e3779b9 + ( i << 6 ) + ( i >> 2 ) );
    } );
  }
} // namespace CaloObjectHash

template <>
struct std::hash<LHCb::Event::Calo::v1::CaloChargedPID> {
  std::size_t operator()( const LHCb::Event::Calo::v1::CaloChargedPID& pid ) const noexcept {
    return CaloObjectHash::combine(
        std::array{std::hash<float>{}( pid.InEcal() ), std::hash<float>{}( pid.ClusterID().all() ),
                   std::hash<float>{}( pid.ClusterMatch() ), std::hash<float>{}( pid.ElectronID().all() ),
                   std::hash<float>{}( pid.ElectronMatch() ), std::hash<float>{}( pid.ElectronEnergy() ),
                   std::hash<float>{}( pid.ElectronShowerEoP() ), std::hash<float>{}( pid.ElectronShowerDLL() ),
                   std::hash<float>{}( pid.EcalPIDe() ), std::hash<float>{}( pid.EcalPIDmu() ),
                   std::hash<float>{}( pid.InHcal() ), std::hash<float>{}( pid.HcalEoP() ),
                   std::hash<float>{}( pid.HcalPIDe() ), std::hash<float>{}( pid.HcalPIDmu() )} );
  }
};

template <>
struct std::hash<LHCb::Event::Calo::v1::BremInfo> {
  std::size_t operator()( const LHCb::Event::Calo::v1::BremInfo& pid ) const noexcept {
    return CaloObjectHash::combine(
        std::array{std::hash<float>{}( pid.InBrem() ), std::hash<float>{}( pid.BremHypoID().all() ),
                   std::hash<float>{}( pid.BremHypoMatch() ), std::hash<float>{}( pid.BremHypoEnergy() ),
                   std::hash<float>{}( pid.BremHypoDeltaX() ), std::hash<float>{}( pid.BremTrackBasedEnergy() ),
                   std::hash<float>{}( pid.BremBendingCorrection() ), std::hash<float>{}( pid.HasBrem() ),
                   std::hash<float>{}( pid.BremEnergy() ), std::hash<float>{}( pid.BremPIDe() )} );
  }
};
