/*****************************************************************************\
* (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once

// Include files
#include "GaudiKernel/GenericVectorTypes.h"
#include "GaudiKernel/SymmetricMatrixTypes.h"
#include "GaudiKernel/SystemOfUnits.h"
#include <ostream>

// Forward declarations

namespace LHCb {

  // Forward declarations

  /** @class CaloPosition CaloPosition.h
   *
   * @brief Position of calorimeter cluster * * * The class represents the
   * position of calorimeter cluster, * center of gravity, spread and their
   * covariance matrices * *
   *
   * @author Vanya Belyaev Ivan.Belyaev@itep.ru
   *
   */

  class CaloPosition final {
  public:
    /// 3-vector of parameters (E,X,Y)
    using Parameters = Gaudi::Vector3;
    /// 3x3F Covariance matrix (E,X,Y)
    using Covariance = Gaudi::SymMatrix3x3;
    /// 2-vector of parameters (X,Y)
    using Center = Gaudi::Vector2;
    /// 2x2 Covariance matrix (X,Y)
    using Spread = Gaudi::SymMatrix2x2;

    /// indices to access X,Y and E
    enum Index { X = 0, Y = 1, E = 2 };

    /// Default Constructor
    CaloPosition() : m_z( -1 * Gaudi::Units::km ), m_parameters( 0., 0., 0. ), m_center( 0., 0. ) {}

    /// retrieve the x-position @attention alias method!
    [[nodiscard]] auto x() const { return parameters()( CaloPosition::Index::X ); }

    /// retrieve the y-position @attention alias method!
    [[nodiscard]] auto y() const { return parameters()( CaloPosition::Index::Y ); }

    /// retrieve the energy @attention alias method!
    [[nodiscard]] auto e() const { return parameters()( CaloPosition::Index::E ); }

    /// Print this CaloPosition data object in a human readable way
    std::ostream& fillStream( std::ostream& s ) const;

    /// Retrieve const  Z-position where cluster parameters are estimated
    [[nodiscard]] auto z() const { return m_z; }

    /// Update  Z-position where cluster parameters are estimated
    CaloPosition& setZ( float value ) {
      m_z = value;
      return *this;
    };

    /// Retrieve const  vector of major cluster parameters
    [[nodiscard]] const Parameters& parameters() const { return m_parameters; }

    /// Retrieve  vector of major cluster parameters
    Parameters& parameters() { return m_parameters; }

    /// Update  vector of major cluster parameters
    CaloPosition& setParameters( Parameters value ) {
      m_parameters = std::move( value );
      return *this;
    }

    /// Retrieve const  covariance matrix of major cluster parameters (3x3)
    [[nodiscard]] const Covariance& covariance() const { return m_covariance; }

    /// Retrieve  covariance matrix of major cluster parameters (3x3)
    [[nodiscard]] Covariance& covariance() { return m_covariance; }

    /// Update  covariance matrix of major cluster parameters (3x3)
    CaloPosition& setCovariance( Covariance value ) {
      m_covariance = std::move( value );
      return *this;
    }

    /// Retrieve const  cluster center of gravity position (2D)
    [[nodiscard]] const Center& center() const { return m_center; }

    /// Retrieve  cluster center of gravity position (2D)
    [[nodiscard]] Center& center() { return m_center; }

    /// Update  cluster center of gravity position (2D)
    CaloPosition& setCenter( Center value ) {
      m_center = std::move( value );
      return *this;
    }

    /// Retrieve const  cluster spread matrix (2x2)
    [[nodiscard]] const Spread& spread() const { return m_spread; }

    /// Retrieve  cluster spread matrix (2x2)
    [[nodiscard]] Spread& spread() { return m_spread; }

    /// Update  cluster spread matrix (2x2)
    CaloPosition& setSpread( Spread value ) {
      m_spread = std::move( value );
      return *this;
    }

    friend std::ostream& operator<<( std::ostream& str, const CaloPosition& obj ) { return obj.fillStream( str ); }

  private:
    float      m_z;          ///< Z-position where cluster parameters are estimated
    Parameters m_parameters; ///< vector of major cluster parameters
    Covariance m_covariance; ///< covariance matrix of major cluster parameters (3x3)
    Center     m_center;     ///< cluster center of gravity position (2D)
    Spread     m_spread;     ///< cluster spread matrix (2x2)

  }; // class CaloPosition

  inline std::ostream& operator<<( std::ostream& s, LHCb::CaloPosition::Index e ) {
    switch ( e ) {
    case LHCb::CaloPosition::X:
      return s << "X";
    case LHCb::CaloPosition::Y:
      return s << "Y";
    case LHCb::CaloPosition::E:
      return s << "E";
    default:
      return s << "ERROR wrong value " << int( e ) << " for enum LHCb::CaloPosition::Index";
    }
  }

} // namespace LHCb
