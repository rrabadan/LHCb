/*****************************************************************************\
* (c) Copyright 2000-2023 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once

#include "GaudiKernel/SharedObjectsContainer.h"
#include "LHCbAlgs/MergingTransformer.h"
#include <algorithm>

namespace LHCb {

  template <typename ObjectType, typename ObjectListType,
            typename ObjectLists = Gaudi::Functional::vector_of_const_<ObjectListType>,
            typename OutputData  = SharedObjectsContainer<ObjectType>>
  struct ObjectContainersSharedMerger : LHCb::Algorithm::MergingTransformer<OutputData( const ObjectLists& )> {
    ObjectContainersSharedMerger( const std::string& name, ISvcLocator* pSvcLocator )
        : LHCb::Algorithm::MergingTransformer<OutputData( const ObjectLists& )>(
              name, pSvcLocator, {"InputLocations", {}}, {"OutputLocation", {}} ) {}

    OutputData operator()( const ObjectLists& lists ) const override {

      OutputData out;
      for ( const auto& list : lists ) {
        for ( const auto& obj : list ) {
          // make sure the object is not yet there!
          if ( std::find( out.begin(), out.end(), obj ) == out.end() ) { out.insert( obj ); }
        }
      }
      return out;
    }
  };

} // namespace LHCb
