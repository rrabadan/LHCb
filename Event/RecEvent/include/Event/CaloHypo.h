/*****************************************************************************\
* (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once

// Include files
#include "Detector/Calo/CaloCellID.h"
#include "Event/CaloCluster.h"
#include "Event/CaloDigit.h"
#include "Event/CaloPosition.h"
#include "GaudiKernel/KeyedContainer.h"
#include "GaudiKernel/KeyedObject.h"
#include "GaudiKernel/NamedRange.h"
#include "GaudiKernel/SharedObjectsContainer.h"
#include "GaudiKernel/SmartRefVector.h"
#include "Kernel/RelationObjectTypeTraits.h"
#include "Kernel/STLExtensions.h"
#include <ostream>
#include <vector>

// Forward declarations

namespace LHCb {

  // Forward declarations

  // Class ID definition
  static const CLID CLID_CaloHypo = 2004;

  // Namespace for locations in TDS
  namespace CaloHypoLocation {
    inline const std::string Default         = "Rec/Calo/Hypos";
    inline const std::string Photons         = "Rec/Calo/Photons";
    inline const std::string Electrons       = "Rec/Calo/Electrons";
    inline const std::string MergedPi0s      = "Rec/Calo/MergedPi0s";
    inline const std::string SplitPhotons    = "Rec/Calo/SplitPhotons";
    inline const std::string DefaultHlt      = "Hlt/Calo/Hypos";
    inline const std::string PhotonsHlt      = "Hlt/Calo/Photons";
    inline const std::string PhotonsHlt1     = "Hlt1/Calo/Photons";
    inline const std::string ElectronsHlt    = "Hlt/Calo/Electrons";
    inline const std::string MergedPi0sHlt   = "Hlt/Calo/MergedPi0s";
    inline const std::string SplitPhotonsHlt = "Hlt/Calo/SplitPhotons";
  } // namespace CaloHypoLocation

  /** @class CaloHypo CaloHypo.h
   *
   * @brief class Calorimeter Hypotheses * * * The class represents the
   * calorimeter hypothesis. * *
   *
   * @author Vanya Belyaev Ivan.Belyaev@itep.ru
   *
   */

  class CaloHypo final : public KeyedObject<int> {
  public:
    /// typedef for std::vector of CaloHypo
    using Vector      = std::vector<CaloHypo*>;
    using ConstVector = std::vector<const CaloHypo*>;

    /// typedef for KeyedContainer of CaloHypo
    typedef KeyedContainer<CaloHypo, Containers::HashMap> Container;

    /// For defining SharedObjectContainer
    using Selection = SharedObjectsContainer<LHCb::CaloHypo>;
    /// For accessing a list of CaloHypos which is either a SharedObjectContainer, a KeyedContainer or a ConstVector
    using Range = Gaudi::NamedRange_<ConstVector>;
    /// Shortcut for the type of Likelihood of CaloHypo object
    using Likelihood = double;
    /// Shortcut for the type of CaloPosition object
    using Position = LHCb::CaloPosition;
    /// shortcut for references to Calorimeter Digits
    using Digits = SmartRefVector<LHCb::CaloDigit>;
    /// Shortcut for references to Calorimeter Clusters
    using Clusters = SmartRefVector<LHCb::CaloCluster>;
    /// Shortcut for eferences to Calorimeter Hypothesis
    using Hypos = SmartRefVector<LHCb::CaloHypo>;

    /// Calo hypotheses
    enum Hypothesis {
      Undefined = 0,
      Mip,
      MipPositive,
      MipNegative,
      Photon,
      PhotonFromMergedPi0,
      BremmstrahlungPhoton,
      Pi0Resolved,
      Pi0Overlapped,
      Pi0Merged,
      EmCharged,
      Positron,
      Electron,
      EmChargedSeed,
      PositronSeed,
      ElectronSeed,
      NeutralHadron,
      ChargedHadron,
      PositiveHadron,
      NegativeHadron,
      Jet,
      Other
    };

    /// Copy Constructor
    CaloHypo( const LHCb::CaloHypo& right )
        : Base()
        , m_hypothesis( right.hypothesis() )
        , m_lh( right.lh() )
        , m_position( right.position() ? new CaloPosition( *right.position() ) : nullptr )
        , m_digits( right.digits() )
        , m_clusters( right.clusters() )
        , m_hypos( right.hypos() ) {}

    /// Default Constructor
    CaloHypo() = default;

    /// Destructor
    ~CaloHypo() { delete m_position; }

    // Retrieve pointer to class definition structure
    const CLID&        clID() const override { return LHCb::CaloHypo::classID(); }
    static const CLID& classID() { return CLID_CaloHypo; }

    /// Update the hypothesis's CaloPosition object.
    CaloHypo& setPosition( std::unique_ptr<LHCb::CaloHypo::Position> position ) {
      m_position = position.release();
      return *this;
    }

    /// Retrieve the energy @attention it is just a shortcut!
    float e() const { return m_position ? m_position->e() : 0; }

    /// Print this CaloHypo data object in a human readable way
    std::ostream& fillStream( std::ostream& s ) const override;

    /// Retrieve const  The hypothesis's ID
    const LHCb::CaloHypo::Hypothesis& hypothesis() const { return m_hypothesis; }

    /// Update  The hypothesis's ID
    CaloHypo& setHypothesis( Hypothesis value ) {
      m_hypothesis = value;
      return *this;
    }

    /// Retrieve const  The Hypothesis's likelihood
    const LHCb::CaloHypo::Likelihood& lh() const { return m_lh; }

    /// Update  The Hypothesis's likelihood
    CaloHypo& setLh( Likelihood value ) {
      m_lh = value;
      return *this;
    }

    /// Retrieve const  The hypothesis's CaloPosition object
    const LHCb::CaloHypo::Position* position() const { return m_position; }

    /// Retrieve  The hypothesis's CaloPosition object
    LHCb::CaloHypo::Position* position() { return m_position; }

    /// Retrieve (const)  References to the Calorimeter Digits
    const SmartRefVector<LHCb::CaloDigit>& digits() const { return m_digits; }

    /// Retrieve  References to the Calorimeter Digits
    SmartRefVector<LHCb::CaloDigit>& digits() { return m_digits; }

    /// Update  References to the Calorimeter Digits
    CaloHypo& setDigits( const SmartRefVector<LHCb::CaloDigit>& value ) {
      m_digits = value;
      return *this;
    }

    /// Add to  References to the Calorimeter Digits
    void addToDigits( SmartRef<LHCb::CaloDigit> value ) { m_digits.push_back( std::move( value ) ); }

    /// Att to (pointer)  References to the Calorimeter Digits
    void addToDigits( LHCb::span<LHCb::CaloClusterEntry const> entries );

    /// Remove from  References to the Calorimeter Digits
    void removeFromDigits( const SmartRef<LHCb::CaloDigit>& value );

    /// reserve space for digits
    CaloHypo& reserveDigits( size_t sz ) {
      m_digits.reserve( sz );
      return *this;
    }

    /// Clear  References to the Calorimeter Digits
    void clearDigits() { m_digits.clear(); }

    /// Retrieve (const)  References to the Calorimeter Clusters
    const SmartRefVector<LHCb::CaloCluster>& clusters() const { return m_clusters; }

    /// Update  References to the Calorimeter Clusters
    CaloHypo& setClusters( const SmartRefVector<LHCb::CaloCluster>& value ) {
      m_clusters = value;
      return *this;
    }

    /// Add to  References to the Calorimeter Clusters
    void addToClusters( SmartRef<LHCb::CaloCluster> value ) { m_clusters.push_back( std::move( value ) ); }

    /// Remove from  References to the Calorimeter Clusters
    void removeFromClusters( const SmartRef<LHCb::CaloCluster>& value );

    /// Clear  References to the Calorimeter Clusters
    void clearClusters() { m_clusters.clear(); }

    /// Retrieve (const)  References to the Calorimeter Hypos
    const SmartRefVector<LHCb::CaloHypo>& hypos() const { return m_hypos; }

    /// Update  References to the Calorimeter Hypos
    void setHypos( const SmartRefVector<LHCb::CaloHypo>& value ) { m_hypos = value; }

    /// Add to  References to the Calorimeter Hypos
    CaloHypo& addToHypos( SmartRef<LHCb::CaloHypo> value ) {
      m_hypos.push_back( std::move( value ) );
      return *this;
    }

    /// Remove from  References to the Calorimeter Hypos
    void removeFromHypos( const SmartRef<LHCb::CaloHypo>& value );

    /// Clear  References to the Calorimeter Hypos
    CaloHypo& clearHypos() {
      m_hypos.clear();
      return *this;
    }

    friend std::ostream& operator<<( std::ostream& str, const CaloHypo& obj ) { return obj.fillStream( str ); }

  private:
    /// Shortcut for own base class
    using Base = KeyedObject<int>;

    LHCb::CaloHypo::Hypothesis        m_hypothesis{CaloHypo::Hypothesis::Undefined}; ///< The hypothesis's ID
    LHCb::CaloHypo::Likelihood        m_lh{-1.};                                     ///< The Hypothesis's likelihood
    LHCb::CaloHypo::Position*         m_position{nullptr}; ///< The hypothesis's CaloPosition object
    SmartRefVector<LHCb::CaloDigit>   m_digits;            ///< References to the Calorimeter Digits
    SmartRefVector<LHCb::CaloCluster> m_clusters;          ///< References to the Calorimeter Clusters
    SmartRefVector<LHCb::CaloHypo>    m_hypos;             ///< References to the Calorimeter Hypos

  }; // class CaloHypo

  /// Definition of Keyed Container for CaloHypo
  using CaloHypos = CaloHypo::Container;

  inline std::ostream& operator<<( std::ostream& s, LHCb::CaloHypo::Hypothesis e ) {
    switch ( e ) {
    case LHCb::CaloHypo::Undefined:
      return s << "Undefined";
    case LHCb::CaloHypo::Mip:
      return s << "Mip";
    case LHCb::CaloHypo::MipPositive:
      return s << "MipPositive";
    case LHCb::CaloHypo::MipNegative:
      return s << "MipNegative";
    case LHCb::CaloHypo::Photon:
      return s << "Photon";
    case LHCb::CaloHypo::PhotonFromMergedPi0:
      return s << "PhotonFromMergedPi0";
    case LHCb::CaloHypo::BremmstrahlungPhoton:
      return s << "BremmstrahlungPhoton";
    case LHCb::CaloHypo::Pi0Resolved:
      return s << "Pi0Resolved";
    case LHCb::CaloHypo::Pi0Overlapped:
      return s << "Pi0Overlapped";
    case LHCb::CaloHypo::Pi0Merged:
      return s << "Pi0Merged";
    case LHCb::CaloHypo::EmCharged:
      return s << "EmCharged";
    case LHCb::CaloHypo::Positron:
      return s << "Positron";
    case LHCb::CaloHypo::Electron:
      return s << "Electron";
    case LHCb::CaloHypo::EmChargedSeed:
      return s << "EmChargedSeed";
    case LHCb::CaloHypo::PositronSeed:
      return s << "PositronSeed";
    case LHCb::CaloHypo::ElectronSeed:
      return s << "ElectronSeed";
    case LHCb::CaloHypo::NeutralHadron:
      return s << "NeutralHadron";
    case LHCb::CaloHypo::ChargedHadron:
      return s << "ChargedHadron";
    case LHCb::CaloHypo::PositiveHadron:
      return s << "PositiveHadron";
    case LHCb::CaloHypo::NegativeHadron:
      return s << "NegativeHadron";
    case LHCb::CaloHypo::Jet:
      return s << "Jet";
    case LHCb::CaloHypo::Other:
      return s << "Other";
    default:
      return s << "ERROR wrong value " << int( e ) << " for enum LHCb::CaloHypo::Hypothesis";
    }
  }

} // namespace LHCb

// -----------------------------------------------------------------------------
// end of class
// -----------------------------------------------------------------------------

inline void LHCb::CaloHypo::addToDigits( LHCb::span<LHCb::CaloClusterEntry const> entries ) {
  m_digits.reserve( m_digits.size() + entries.size() );
  std::transform( entries.begin(), entries.end(), std::back_inserter( m_digits ),
                  []( const auto& e ) { return e.digit(); } );
}

inline void LHCb::CaloHypo::removeFromDigits( const SmartRef<LHCb::CaloDigit>& value ) {
  auto i = std::remove( m_digits.begin(), m_digits.end(), value );
  m_digits.erase( i, m_digits.end() );
}

inline void LHCb::CaloHypo::removeFromClusters( const SmartRef<LHCb::CaloCluster>& value ) {
  auto i = std::remove( m_clusters.begin(), m_clusters.end(), value );
  m_clusters.erase( i, m_clusters.end() );
}

inline void LHCb::CaloHypo::removeFromHypos( const SmartRef<LHCb::CaloHypo>& value ) {
  auto i = std::remove( m_hypos.begin(), m_hypos.end(), value );
  m_hypos.erase( i, m_hypos.end() );
}

namespace Relations {
  template <typename>
  struct ObjectTypeTraits;

  template <>
  struct ObjectTypeTraits<LHCb::CaloHypo> : KeyedObjectTypeTraits<LHCb::CaloHypo> {};
} // namespace Relations
