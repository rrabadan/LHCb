/*****************************************************************************\
* (c) Copyright 2000-2023 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once

#include "Event/PrimaryVertex.h"
#include "Event/SOACollection.h"
#include "GaudiKernel/Point3DTypes.h"
#include "LHCbMath/MatVec.h"
#include <limits>

#include "Event/SIMDEventTypes.h"
#include "Event/UniqueIDGenerator.h"
#include "Kernel/EventLocalAllocator.h"
#include "Kernel/LHCbID.h"
#include "Kernel/Traits.h"
#include "Kernel/meta_enum.h"
#include "LHCbMath/BloomFilter.h"
#include "LHCbMath/SIMDWrapper.h"
#include <boost/unordered/unordered_flat_map.hpp>

// Forward declaration
namespace LHCb::Pr::Velo {
  struct Tracks;
}

namespace LHCb::Event::PV {
  constexpr CLID CLID_PrimaryVertexContainer = 10030;

  using PrVeloTracks = LHCb::Pr::Velo::Tracks;

  namespace PVTrackTag {
    // This will just be a copy of the data of the first state in the original track. At some later point we can
    // directly use that data.
    struct z : float_field {};  /// z of track state
    struct x : float_field {};  /// x of track state
    struct y : float_field {};  /// y of track state
    struct tx : float_field {}; /// tx of track state
    struct ty : float_field {}; /// ty of track state
    enum struct XTxCovMatrixElement { xx = 0, xtx, txtx, N };
    struct Vx : floats_field<int( XTxCovMatrixElement::N )> {}; /// Vxx, Vxtx, Vtxtx
    struct Vy : floats_field<int( XTxCovMatrixElement::N )> {}; /// Vyy, Tyty, Vtyty

    struct pvindex : int_field {
      using packer_t = SOADontPack;
    }; /// index to the PV to which this track belongs
    struct veloindex : Event::index2D_field<const PrVeloTracks, std::index_sequence<31>> {
      using packer_t = SOADontPack;
    };                            /// pointer to prvelotrack
    struct veloid : int_field {}; /// velo segment id (hash from velo LHCbIDs)
    struct weight : float_field {
      using packer_t = SOADontPack;
    }; /// Tukey weight
    struct ipchi2 : float_field {
      using packer_t = SOADontPack;
    }; /// ip chi2 of track (which determines the Tukery weight)

    enum struct PosVectorElement { x = 0, y, z, N };
    struct halfDChi2DX : floats_field<int( PosVectorElement::N )> {
      using packer_t = SOADontPack;
    }; /// Contribution the first derivative of the chi2
    enum struct PosCovMatrixElement { v00 = 0, v11, v20, v21, v22, N };
    struct halfD2Chi2DX2 : floats_field<int( PosCovMatrixElement::N )> {
      using packer_t = SOADontPack;
    }; /// Contribution to the second derivative of the chi2, non-zero elements only (0,0),(1,1),(0,2),(1,2),(2,2)

    struct Wx : float_field {
      using packer_t = SOADontPack;
    }; /// Weight of track in x (inverse of Cov(x) after extrapolation to mother vertex z)
    struct Wy : float_field {
      using packer_t = SOADontPack;
    }; /// Weight of track in y (inverse of Cov(y) after extrapolation to mother vertex z)

    template <typename T>
    using pvtrack_t = SOACollection<T, z, x, y, tx, ty, Vx, Vy, pvindex, veloindex, veloid, weight, ipchi2, halfDChi2DX,
                                    halfD2Chi2DX2, Wx, Wy>;
  } // namespace PVTrackTag

  struct PVTracks : PVTrackTag::pvtrack_t<PVTracks> {
    using base_t = typename PVTrackTag::pvtrack_t<PVTracks>;
    using base_t::base_t;

    // Required to get to the velo tracks
    template <typename Tag>
    auto containers() const {
      if constexpr ( std::is_same_v<Tag, PVTrackTag::veloindex> ) { return prvelocontainers; }
    }
    std::array<const PrVeloTracks*, 2> prvelocontainers{}; // forward, backward

    // Define a custom proxy for user friendlyness
    template <SIMDWrapper::InstructionSet simd, LHCb::Pr::ProxyBehaviour behaviour, typename ContainerType>
    struct PVTrackProxy : LHCb::Event::Proxy<simd, behaviour, ContainerType> {
      // use default constructor
      using LHCb::Event::Proxy<simd, behaviour, ContainerType>::Proxy;

      // declare custom functions
      [[nodiscard]] auto z() const { return this->template get<PVTrackTag::z>(); }
      [[nodiscard]] auto x() const { return this->template get<PVTrackTag::x>(); }
      [[nodiscard]] auto y() const { return this->template get<PVTrackTag::y>(); }
      [[nodiscard]] auto tx() const { return this->template get<PVTrackTag::tx>(); }
      [[nodiscard]] auto ty() const { return this->template get<PVTrackTag::ty>(); }
      [[nodiscard]] auto covXX() const {
        return this->template get<PVTrackTag::Vx>( PVTrackTag::XTxCovMatrixElement::xx );
      }
      [[nodiscard]] auto covXTx() const { return this->template get<PVTrackTag::Vx>( 1 ); }
      [[nodiscard]] auto covTxTx() const { return this->template get<PVTrackTag::Vx>( 2 ); }
      [[nodiscard]] auto covYY() const { return this->template get<PVTrackTag::Vy>( 0 ); }
      [[nodiscard]] auto covYTy() const { return this->template get<PVTrackTag::Vy>( 1 ); }
      [[nodiscard]] auto covTyTy() const { return this->template get<PVTrackTag::Vy>( 2 ); }
      [[nodiscard]] auto pvindex() const { return this->template get<PVTrackTag::pvindex>(); }
      [[nodiscard]] auto veloid() const { return this->template get<PVTrackTag::veloid>(); }
      [[nodiscard]] auto weight() const { return this->template get<PVTrackTag::weight>(); }
      [[nodiscard]] auto ipchi2() const { return this->template get<PVTrackTag::ipchi2>(); }
      [[nodiscard]] auto halfDChi2DX( int i ) const { return this->template get<PVTrackTag::halfDChi2DX>( i ); }
      [[nodiscard]] auto halfD2Chi2DX2( PVTrackTag::PosCovMatrixElement i ) const {
        return this->template get<PVTrackTag::halfD2Chi2DX2>( i );
      }
      [[nodiscard]] auto Wx() const { return this->template get<PVTrackTag::Wx>(); }
      [[nodiscard]] auto Wy() const { return this->template get<PVTrackTag::Wy>(); }

      [[nodiscard, gnu::always_inline]] auto veloTrack() const {
        if constexpr ( behaviour != LHCb::Pr::ProxyBehaviour::ScatterGather2D ) {
          auto proxy = this->container()->template simd<simd>().gather2D( this->indices(), this->loop_mask() );
          return proxy.template get<PVTrackTag::veloindex>();
        } else {
          return this->template get<PVTrackTag::veloindex>();
        }
      }
    };

    // Register the proxy:
    template <SIMDWrapper::InstructionSet simd, LHCb::Pr::ProxyBehaviour behaviour, typename ContainerType>
    using proxy_type = PVTrackProxy<simd, behaviour, ContainerType>;
  };

  // The following to function need to go into LHCbMath/MatVec.h
  // Convert between different basic types
  template <typename Out, auto N, typename In>
  auto convert( const LinAlg::MatSym<In, N>& in ) {
    using Utils::unwind;
    LinAlg::MatSym<Out, N> out;
    unwind<0, N>( [&]( auto i ) { unwind<0, i + 1>( [&]( auto j ) { out( i, j ) = in( i, j ); } ); } );
    return out;
  }
  // Convert to SMatrix
  template <typename Out, auto N, typename In>
  auto convertToSMatrix( const LHCb::LinAlg::MatSym<In, N>& in ) {
    using LHCb::Utils::unwind;
    ROOT::Math::SMatrix<Out, N, N, ROOT::Math::MatRepSym<Out, N>> out;
    unwind<0, N>( [&]( auto i ) { unwind<0, i + 1>( [&]( auto j ) { out( i, j ) = in( i, j ); } ); } );
    return out;
  }

  /// Extension of PrimaryVertex class which holds pointers (simple indices) to the first and last corresponding track
  /// in the PVTrack container.
  using PVIndex       = uint8_t;
  using VeloSegmentID = uint32_t;
  enum { PVIndexInvalid = std::numeric_limits<uint8_t>::max() };
  meta_enum_class( PVFitStatus, int, Unknown = 0, NotConverged, Converged, Failed );
  class ExtendedPrimaryVertex final : public PrimaryVertex {
  public:
    using VeloSegmentIDMap = boost::unordered_flat_map<VeloSegmentID, uint16_t>;

  private:
    using SymMatrix3x3 = LHCb::LinAlg::MatSym<double, 3>;
    using Vector3      = LHCb::LinAlg::Vec<double, 3>;
    uint16_t         m_begin{0};                     /// Index of the first track of this PV in the track list
    uint16_t         m_end{0};                       /// Last+1 index of the first track of this PV in the track list
    SymMatrix3x3     m_halfD2Chi2DX2;                /// Cached weight matrix, for unbiasing.
    Vector3          m_halfDChi2DX;                  /// Cached first derivative, for unbiasing.
    double           m_sumOfWeights{0};              /// Cached sum of weights
    VeloSegmentIDMap m_veloidmap;                    /// veloids
    PVFitStatus      m_status{PVFitStatus::Unknown}; /// status of the vertex fit
    uint8_t          m_numFitIter{0};                /// Nummber of iterations of the vertex fit
  public:
    using PrimaryVertex::PrimaryVertex;
    /// Constructor with a selected key (only for testing)
    explicit ExtendedPrimaryVertex( int key ) : PrimaryVertex{key} {}

    // Accessors
    auto        begin() const { return m_begin; }
    auto        end() const { return m_end; }
    auto        size() const { return m_end - m_begin; }
    auto        status() const { return m_status; }
    auto        numFitIter() const { return m_numFitIter; }
    const auto& halfD2Chi2DX2() const { return m_halfD2Chi2DX2; }
    const auto& halfDChi2DX() const { return m_halfDChi2DX; }
    const auto& sumOfWeights() const { return m_sumOfWeights; }
    void        setHalfD2Chi2DX2( const SymMatrix3x3& halfD2Chi2DX2 ) { m_halfD2Chi2DX2 = halfD2Chi2DX2; }
    void        setHalfDChi2DX( const Vector3& halfDChi2DX ) { m_halfDChi2DX = halfDChi2DX; }
    void        setSumOfWeights( double w ) { m_sumOfWeights = w; }
    void        setStatus( PVFitStatus s ) { m_status = s; }
    void        setNumFitIter( unsigned i ) { m_numFitIter = i; }
    template <typename T>
    void setRange( T begin, T end ) {
      m_begin = begin;
      m_end   = end;
    }
    template <typename T>
    void setSize( T size ) {
      m_end = m_begin + size;
    }
    const auto& veloSegmentIDMap() const { return m_veloidmap; }
    bool        contains( VeloSegmentID veloid ) const { return m_veloidmap.find( veloid ) != m_veloidmap.end(); }
    // Interface compatibility with ThOr. This is already in the base class. Do we actually need it?
    [[nodiscard, gnu::always_inline]] friend LinAlg::Vec<SIMDWrapper::scalar::float_v, 3>
    endVertexPos( const ExtendedPrimaryVertex& vtx ) {
      auto pos = vtx.position();
      return {pos.x(), pos.y(), pos.z()};
    }
    [[nodiscard, gnu::always_inline]] friend auto posCovMatrix( const ExtendedPrimaryVertex& vtx ) {
      return LHCb::LinAlg::convert<SIMDWrapper::scalar::float_v>( vtx.covMatrix() );
    }
    friend struct PrimaryVertexContainer;
    bool checkEqual( const ExtendedPrimaryVertex& other ) const;
    void fillVeloIDMap();
  };

  using PrimaryVertices = PrimaryVertex::Vector;
  using ExtendedPrimaryVertices =
      std::vector<ExtendedPrimaryVertex, LHCb::Allocators::EventLocal<ExtendedPrimaryVertex>>;
  using PVTrackIndexList = std::vector<short, LHCb::Allocators::EventLocal<short>>;

  /// Event store object that holds the PVs, the tracks in the PVs, and a table for navigating from the original velo
  /// tracks to the PV tracks, needed for unbiasing.
  struct PrimaryVertexContainer : public ObjectContainerBase {
    ExtendedPrimaryVertices vertices;     /// vertices
    PVTracks                tracks;       /// tracks
    PVTrackIndexList        velotrackmap; /// table to get from velo track index (e.g. v3::Track::trackVP) to pv track
    uint32_t                m_version{0};
    float m_maxDeltaChi2{12}; /// Values for maxDeltaChi2 used to compute Tukey weights. We need this to be persisted.

    template <typename Allocator>
    PrimaryVertexContainer( const Allocator& alloc )
        : vertices{alloc}, tracks{Zipping::generateZipIdentifier(), alloc}, velotrackmap{alloc} {}
    PrimaryVertexContainer() = default;
    /// Copy constructor
    PrimaryVertexContainer( const PrimaryVertexContainer& rhs )
        : vertices{rhs.vertices}, tracks{rhs.tracks.zipIdentifier(), rhs.tracks}, velotrackmap{rhs.velotrackmap} {
      // Note that we copy the zipidentifier: otherwise we cannot check to containers to be equal
      // make sure to copy the tracks. this should be in the SOACollection copy constructor: we'll open an issue for
      // that.
      if ( rhs.tracks.size() > 0 ) {
        tracks.reserve( rhs.tracks.capacity() );
        if ( rhs.tracks.size() > 0 ) {
          // it would be nice if we would learn how to use variadic templates function to just call
          //    std::copy( rhs.m_data, std::next( m_data, rhs.size() ), m_data );
          // for all data fields.
          // for now, we need to use this, which looks rather inefficient:
          for ( auto const& entry : rhs.tracks.simd() )
            tracks.copy_back<SIMDWrapper::best::types>( rhs.tracks, entry.offset() /*, entry.loop_mask()*/ );
          tracks.resize( rhs.tracks.size() );
        }
      }
      setIndices();
    }
    /// Move constructor
    PrimaryVertexContainer( PrimaryVertexContainer&& rhs )
        : vertices{std::move( rhs.vertices )}
        , tracks{std::move( rhs.tracks )}
        , velotrackmap{std::move( rhs.velotrackmap )} {
      setIndices();
    }
    PrimaryVertexContainer& operator=( const PrimaryVertexContainer& ) = delete;
    PrimaryVertexContainer& operator=( PrimaryVertexContainer&& ) = delete;
    ~PrimaryVertexContainer() { setParentPointers( nullptr ); }

    /// Forwarding functions to make this look like a container of vertices.
    auto               begin() const { return vertices.begin(); }
    auto               end() const { return vertices.end(); }
    auto               size() const { return vertices.size(); }
    auto               empty() const { return vertices.empty(); }
    const auto&        front() const { return vertices.front(); }
    const auto&        operator[]( size_t i ) const { return vertices[i]; }
    const CLID&        clID() const override { return PrimaryVertexContainer::classID(); }
    static const CLID& classID() { return CLID_PrimaryVertexContainer; }
    auto               data() { return vertices; }
    using value_type     = ExtendedPrimaryVertices::value_type;
    using contained_type = value_type;
    using allocator_type = ExtendedPrimaryVertices::allocator_type;
    auto get_allocator() const { return tracks.get_allocator(); }

    /// Trivial routine to set the key of the keyed objects
    void setIndices() {
      for ( size_t i = 0; i < size(); ++i ) {
        vertices[i].setParent( this );
        if ( !vertices[i].hasKey() ) vertices[i].setKey( i );
      }
    }
    // Needed for the ObjectContainerBase interface etc
    long index( const ContainedObject* obj ) const final {
      return std::distance( &vertices.front(), reinterpret_cast<const ExtendedPrimaryVertex*>( obj ) );
    }
    const ContainedObject* containedObject( long dist ) const final { return &( vertices[dist] ); }
    ContainedObject*       containedObject( long dist ) final { return &( vertices[dist] ); }
    size_type              numberOfObjects() const final { return size(); }
    long                   add( ContainedObject* ) final {
      throw GaudiException( "add: do not call", __PRETTY_FUNCTION__, StatusCode::FAILURE );
    }
    long remove( ContainedObject* ) final {
      throw GaudiException( "remove: do not call", __PRETTY_FUNCTION__, StatusCode::FAILURE );
    }
    void setParentPointers( ObjectContainerBase* p ) {
      for ( auto& v : vertices ) v.setParent( p );
    }

    /// Packers and unpackers
    template <bool debug = false, typename Buffer>
    void pack( Buffer& buffer ) const;
    template <bool debug = false, typename Buffer>
    void unpack( Buffer& buffer );
    bool checkEqual( const PrimaryVertexContainer& other ) const;
    void updateVeloIDMap();
    void updateDerivatives();
    void updateNavigation();
  };

  /// Default PV location
  inline std::string const DefaultLocation = "Rec/PrimaryVertexContainer";

  /// Helper class to store the configuration of the vertex fit
  struct AdaptiveFitConfig {
    static constexpr float    defaultMaxDeltaChi2          = 12;
    static constexpr float    defaultMaxDeltaZConverged    = 0.001 * Gaudi::Units::mm;
    static constexpr float    defaultMaxDeltaChi2Converged = 0.01;
    static constexpr unsigned defaultMaxFitIter            = 10;
    static constexpr unsigned defaultNumAnnealingSteps     = 0;
    float                     maxDeltaChi2{defaultMaxDeltaChi2};
    float                     maxDeltaZConverged{defaultMaxDeltaZConverged};
    float                     maxDeltaChi2Converged{defaultMaxDeltaChi2Converged};
    unsigned                  maxFitIter{defaultMaxFitIter};
    unsigned                  numAnnealingSteps{defaultNumAnnealingSteps};
  };

  /// Fit a single PV. Warning: due to padding this will invalidate some data in the next vertex. So, make sure to fit
  /// all of them in order if you need more than one vertex.
  PVFitStatus fitAdaptive( ExtendedPrimaryVertex& pv, PVTracks& pvtracks, const AdaptiveFitConfig& fitconfig );

  /// Perform the adaptive vertex fit to all PVs.
  void fitAdaptive( ExtendedPrimaryVertices& vertices, PVTracks& pvtracks, const AdaptiveFitConfig& fitconfig );

  /// Returns the list of unbiased vertices
  PrimaryVertices unbiasedVertices( const PrimaryVertexContainer&   pvdata,
                                    LHCb::span<const VeloSegmentID> vetoedvelotracks );

  /// Returns a single unbiased primary vertex.
  PrimaryVertex unbiasedVertex( const PrimaryVertexContainer& pvdata, int pvindex,
                                LHCb::span<const VeloSegmentID> vetoedvelotracks );

  /// Refits the entire set of vertices updating the track weights
  ExtendedPrimaryVertices refittedVertices( const PrimaryVertexContainer&   pvdata,
                                            LHCb::span<const VeloSegmentID> vetoedvelotracks,
                                            const AdaptiveFitConfig&        fitconfig );

  /// Refits a single unbiased primary vertex
  PrimaryVertex refittedVertex( const PrimaryVertexContainer& pvdata, int pvindex,
                                LHCb::span<const VeloSegmentID> vetoedvelotracks, const AdaptiveFitConfig& fitconfig );

  /// Retrieves the closest PV for this velo track.
  inline auto closestPV( const PrimaryVertexContainer& pvdata, int veloindex ) {
    return pvdata.tracks.scalar()[pvdata.velotrackmap[veloindex]].pvindex();
  }

  // Retrieves a fast estimate for the ip chi2 of this velo track
  inline auto fastIPChi2( const PrimaryVertexContainer& pvdata, int veloindex ) {
    return pvdata.tracks.scalar()[pvdata.velotrackmap[veloindex]].ipchi2();
  }

  // Packers and unpackers
  template <typename PackedType, typename scale = std::ratio<1, 1>, typename UnpackedType, typename Buffer>
  void write( Buffer& buf, const UnpackedType& x ) {
    /// Fixme: This still misses proper rounding and limits, but that requires something with template specialization
    /// for integers/floats
    buf.write( PackedType( ( x * scale::num ) / scale::den ) );
    // buf.write( PackedType(std::min(UnpackedType((x *
    // scale::num)/scale::den),UnpackedType(std::numeric_limits<PackedType>::max()),UnpackedType(std::numeric_limits<PackedType>::min()))
    // ) ) ;
  }
  template <typename PackedType, typename scale = std::ratio<1, 1>, typename UnpackedType, typename Buffer>
  void read( Buffer& buf, UnpackedType& x ) {
    PackedType y;
    buf.read( y );
    x = UnpackedType( ( y * scale::den ) / scale::num );
  }
  template <typename PackedType, typename scale = std::ratio<1, 1>, typename Buffer>
  auto read( Buffer& buf ) {
    PackedType y;
    buf.read( y );
    return ( y * scale::den ) / scale::num;
  }

  /// This is called by the unpacking to recompute columns that were not persisted
  void updateDerivatives( PrimaryVertexContainer& );

  template <bool debug, typename Buffer>
  void PrimaryVertexContainer::pack( Buffer& buffer ) const {
    const auto pos_before = buffer.pos();
    // Pack the version
    write<uint8_t>( buffer, m_version );
    // Pack the tracks (for the unpacking we need those first)
    write<uint16_t>( buffer, tracks.size() );
    if ( tracks.size() > 0 ) tracks.pack<debug>( buffer );
    // Pack the track map
    write<uint16_t>( buffer, velotrackmap.size() );
    for ( const auto& i : velotrackmap ) write<uint16_t>( buffer, i );
    // Pack the vertices
    write<uint8_t>( buffer, vertices.size() );
    for ( const auto& vertex : vertices ) {
      write<uint16_t>( buffer, vertex.m_begin );
      write<uint16_t>( buffer, vertex.m_end );
      write<float>( buffer, vertex.position().x() );
      write<float>( buffer, vertex.position().y() );
      write<float>( buffer, vertex.position().z() );
      std::for_each( vertex.covMatrix().begin(), vertex.covMatrix().end(),
                     [&]( const auto& x ) { write<float>( buffer, x ); } );
      write<float>( buffer, vertex.chi2() );
      write<int16_t>( buffer, vertex.nDoF() );
      write<uint8_t>( buffer, uint8_t( vertex.status() ) );
      write<uint8_t>( buffer, vertex.numFitIter() );
    }
    // Pack the Tukey max chi2
    write<float>( buffer, m_maxDeltaChi2 );

    if constexpr ( debug ) {
      std::cout << "Packed PrimaryVertexContainer from " << pos_before << " to " << buffer.pos() << " ("
                << ( buffer.pos() - pos_before ) << " bytes) " << tracks.size() << " " << velotrackmap.size() << " "
                << vertices.size() << std::endl;
    }
  }

  template <bool debug, typename Buffer>
  void PrimaryVertexContainer::unpack( Buffer& buffer ) {
    const auto pos_before = buffer.pos();
    // Unpack the version
    m_version = read<uint8_t>( buffer );
    // Unpack the tracks
    size_t tracks_size = read<uint16_t>( buffer );
    if ( tracks_size > 0 ) tracks.unpack<debug>( buffer );
    // Unpack the track map
    velotrackmap.resize( read<uint16_t>( buffer ) );
    for ( auto& trk : velotrackmap ) buffer.read( trk );
    // Unpack the vertices
    vertices.resize( read<uint8_t>( buffer ) );
    for ( auto& vertex : vertices ) {
      read<uint16_t>( buffer, vertex.m_begin );
      read<uint16_t>( buffer, vertex.m_end );
      const double vx = read<float>( buffer );
      const double vy = read<float>( buffer );
      const double vz = read<float>( buffer );
      vertex.setPosition( Gaudi::XYZPoint( vx, vy, vz ) );
      Gaudi::SymMatrix3x3 cov;
      std::for_each( cov.begin(), cov.end(), [&]( auto& x ) { x = read<float>( buffer ); } );
      vertex.setCovMatrix( cov );
      vertex.setChi2( read<float>( buffer ) );
      vertex.setNDoF( read<int16_t>( buffer ) );
      vertex.setStatus( PVFitStatus( read<uint8_t>( buffer ) ) );
      vertex.setNumFitIter( read<uint8_t>( buffer ) );
    }
    // Unpack the Tukey max chi2
    m_maxDeltaChi2 = read<float>( buffer );
    if constexpr ( debug ) {
      std::cout << "Unpacked PrimaryVertexContainer from " << pos_before << " to " << buffer.pos() << " ("
                << ( buffer.pos() - pos_before ) << " bytes) " << tracks.size() << " " << velotrackmap.size() << " "
                << vertices.size() << std::endl;
    }

    // Fill the fields that we did not store
    updateNavigation();
    setIndices();
    updateDerivatives();
  }

  // Methods to compute a uniqueVeloSegmentID from a set of LHCbIDs
  template <bool checkIsVelo = true, typename LHCbIDContainer, typename HashType = uint32_t>
  auto uniqueVeloSegmentID( const LHCbIDContainer& lhcbids ) {
    // create a unique ID from the LHCbIDs. we choose the sum because that is independent of the order.
    HashType hash{0};
    for ( const auto id : lhcbids )
      if ( !checkIsVelo || id.isVP() ) hash += id.lhcbID();
    return hash;
  }
  // Unfortunate that we need special version for PrVeloTrack but wrapper was expensive
  template <typename PrVeloTrackProxyScalar, typename HashType = uint32_t>
  auto uniqueVeloSegmentIDFromPrVelo( const PrVeloTrackProxyScalar& trk ) {
    HashType hash{0};
    for ( int ihit = 0; ihit < trk.nHits().cast(); ++ihit ) hash += trk.vp_lhcbID( ihit ).LHCbID().lhcbID();
    return hash;
  }
} // namespace LHCb::Event::PV
