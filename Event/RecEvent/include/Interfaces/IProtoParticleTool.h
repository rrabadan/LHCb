/*****************************************************************************\
* (c) Copyright 2020 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once

#include "DetDesc/IGeometryInfo.h"
#include "Event/ProtoParticle.h"

#include "GaudiKernel/IAlgTool.h"

/**
 *  The generic interface for "ProtoParticle tools" , which deal with
 *  ProtoParticle objects.
 *
 *  @author Gerhard Raven
 *  @date   12/10/2020
 */
namespace LHCb::Rec::Interfaces {
  struct IProtoParticles : extend_interfaces<IAlgTool> {

    DeclareInterfaceID( IProtoParticles, 2, 0 );

    /** The main processing method
     *  @param reference to collection of ProtoParticles to be processed
     *  @return status code
     */
    virtual StatusCode operator()( LHCb::ProtoParticles&, IGeometryInfo const& geometry ) const = 0;
  };
} // namespace LHCb::Rec::Interfaces
