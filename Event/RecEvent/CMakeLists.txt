###############################################################################
# (c) Copyright 2000-2021 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
#[=======================================================================[.rst:
Event/RecEvent
--------------
#]=======================================================================]

gaudi_add_library(RecEvent
    SOURCES
        src/CaloClusterEntry.cpp
        src/CaloHypo.cpp
        src/CaloPosition.cpp
        src/ProcStatus.cpp
        src/ProtoParticle.cpp
        src/RecSummary.cpp
        src/RecVertex.cpp
        src/RecVertex_v2.cpp
        src/RichPID.cpp
        src/TwoProngVertex.cpp
        src/UTSummary.cpp
        src/VertexBase.cpp
	src/PrimaryVertices.cpp
    LINK
        PUBLIC
            Boost::headers
            Gaudi::GaudiKernel
            GSL::gsl
            LHCb::DigiEvent
            LHCb::EventBase
            LHCb::LHCbKernel
            LHCb::LHCbMathLib
            LHCb::PartPropLib
            LHCb::TrackEvent
            Rangev3::rangev3
)

gaudi_add_dictionary(RecEventDict
    HEADERFILES dict/dictionary.h
    SELECTION dict/selection.xml
    LINK LHCb::RecEvent
)

foreach(test IN ITEMS TestMuonZipInfrastructure TestPrimaryVertices)
    gaudi_add_executable(${test}
        SOURCES tests/src/${test}.cpp
        LINK
            Boost::unit_test_framework
            LHCb::RecEvent
        TEST
    )
endforeach()
