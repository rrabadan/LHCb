/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "Event/PrimaryVertices.h"
#include "Event/PrVeloTracks.h"
#include "Event/RecVertex.h"
#include "LHCbMath/MatVec.h"

namespace LHCb::Event::PV {

  using simd    = SIMDWrapper::best::types;
  using float_v = simd::float_v;
  using int_v   = simd::int_v;

  template <typename FTYPE>
  inline auto sqr( FTYPE x ) {
    return x * x;
  }

  void PrimaryVertexContainer::updateNavigation() {
    if ( tracks.size() > 0 ) {
      // 1. set the index from every track to its PV (by starting from the PV). this can be vectorized, but it is not
      // worth it.
      std::fill_n( tracks.data<PVTrackTag::pvindex>(), tracks.size(), -1 );
      size_t ipv = 0;
      for ( auto& pv : vertices ) std::fill_n( tracks.data<PVTrackTag::pvindex>() + pv.begin(), pv.size(), ipv++ );
      // 2. fill the maps that point a uniqueVeloSegmentID to a pv track in every vertex
      updateVeloIDMap();
    }
    // 3. set the keys
    setIndices();
  }

  void PrimaryVertexContainer::updateVeloIDMap() {
    // fill the maps that point a uniqueVeloSegmentID to a pv track in every vertex
    const auto pvtracks_scalar = tracks.scalar();
    for ( auto& pv : vertices ) {
      pv.m_veloidmap.reserve( pv.end() - pv.begin() );
      for ( auto offset = pv.begin(); offset < pv.end(); ++offset ) {
        const auto pvtrack = pvtracks_scalar[offset];
        pv.m_veloidmap.emplace( pvtrack.veloid().cast(), offset );
      }
    }
  }

  template <typename T>
  void initialize_with_zeros( T& mat ) {
    mat = LinAlg::initialize_with_zeros<T>();
  }

  /// Helper class to hold accumulated chi2 and its derivatives
  struct Chi2Accumulator {
    using Vector3      = LHCb::LinAlg::Vec<double, 3>;
    using SymMatrix3x3 = LHCb::LinAlg::MatSym<double, 3>;
    int          entries{0};
    double       chi2{0};
    double       sumw{0};
    Vector3      halfDChi2DX{LinAlg::initialize_with_zeros<Vector3>()};
    SymMatrix3x3 halfD2Chi2DX2{LinAlg::initialize_with_zeros<SymMatrix3x3>()};
    void         reset() {
      entries = 0;
      chi2    = 0;
      initialize_with_zeros( halfDChi2DX );
      initialize_with_zeros( halfD2Chi2DX2 );
    }
  };

  /// Updates the PV with residuals accumulated from the tracks. Returns true if converged.
  PVFitStatus updatePrimaryVertex( ExtendedPrimaryVertex& pv, const Chi2Accumulator& accumulator,
                                   float maxDeltaZConverged, float maxDeltaChi2Converged ) {
    if ( accumulator.entries < 2 ) {
      pv.setNDoF( -1 );
      return PVFitStatus::Failed;
    }
    auto [success, cov] = accumulator.halfD2Chi2DX2.invChol();
    if ( !success ) {
      pv.setNDoF( -1 );
      return PVFitStatus::Failed;
    }
    // compute the delta w.r.t. the reference
    const auto delta = cov * accumulator.halfDChi2DX * -1.0;
    // update the position
    pv.setPosition( pv.position() + Gaudi::XYZVector{delta( 0 ), delta( 1 ), delta( 2 )} );
    // update the chi2
    const auto deltachi2 = delta.dot( accumulator.halfDChi2DX );
    pv.setChi2( accumulator.chi2 + deltachi2 );
    pv.setNDoF( 2 * accumulator.entries - 3 );
    pv.setHalfD2Chi2DX2( accumulator.halfD2Chi2DX2 );
    pv.setHalfDChi2DX( LinAlg::initialize_with_zeros<LHCb::LinAlg::Vec<double, 3>>() );
    pv.setCovMatrix( convertToSMatrix<double>( cov ) );
    pv.setSumOfWeights( accumulator.sumw );
    bool converged = std::abs( delta.z() ) < maxDeltaZConverged && abs( deltachi2 ) < maxDeltaChi2Converged;
    return converged ? PVFitStatus::Converged : PVFitStatus::NotConverged;
  };

  /// Fit all PVs adaptively. They used to be fitted simultaneously but
  /// the time consumption of 'gather' and 'scatter' routines was too
  /// large. Therefore, tracks are now sorted by PV and the PVs just
  /// fitted sequentially.
  void fitAdaptive( ExtendedPrimaryVertices& vertices, PVTracks& pvtracks, const AdaptiveFitConfig& fitconfig ) {
    for ( auto& pv : vertices ) fitAdaptive( pv, pvtracks, fitconfig );
  }

  /// Update derivatives with current veretx position and accumulate the result
  template <bool UpdateWeightMatrix, bool UpdateWeight = true>
  Chi2Accumulator accumulate( ExtendedPrimaryVertex& pv, PVTracks& pvtracks, float chi2max = -1 ) {
    const auto pvtrack_simd = pvtracks.simd();
    const auto start        = pv.begin();
    const auto end          = pv.end();

    float const vtxx = pv.position().x();
    float const vtxy = pv.position().y();
    float const vtxz = pv.position().z();

    Chi2Accumulator accumulator;

    for ( auto offset = start; offset < end; offset += simd::size ) {
      const auto pvtrack = pvtrack_simd[offset];
      const auto dz      = vtxz - pvtrack.z();
      const auto tx      = pvtrack.tx();
      const auto ty      = pvtrack.ty();

      if constexpr ( UpdateWeightMatrix ) {
        auto const Vx = pvtrack.covXX() + 2 * dz * pvtrack.covXTx() + dz * dz * pvtrack.covTxTx();
        auto const Vy = pvtrack.covYY() + 2 * dz * pvtrack.covYTy() + dz * dz * pvtrack.covTyTy();
        assert( none( essentiallyZero( Vx ) || essentiallyZero( Vy ) ) );
        const auto Wx = 1 / Vx;
        const auto Wy = 1 / Vy;

        // ( 1     0 )
        // ( 0     1 )      ( Wx   0  )     ( 1  0  -tx )
        // ( -tx -ty )      (  0   Wy )     ( 0  1  -ty )
        //
        //        (  Wx         0     )
        //   =    (  0          Wy    )      ( 1  0  -tx )
        //        (  -tx Wx    -ty Wy )      ( 0  1  -ty )
        //
        //        (    Wx        0              -tx Wx        )
        //   =    (    0         Wy             -ty Wy        )
        //        ( -tx Wx    -ty Wy    (tx*tx*Wx + ty*ty*Wy) )

        const auto halfD2Chi2DX2_00 = Wx;
        const auto halfD2Chi2DX2_11 = Wy;
        const auto halfD2Chi2DX2_20 = -tx * Wx;
        const auto halfD2Chi2DX2_21 = -ty * Wy;
        const auto halfD2Chi2DX2_22 = tx * Wx * tx + ty * Wy * ty;
        pvtrack.field<PVTrackTag::Wx>().set( Wx );
        pvtrack.field<PVTrackTag::Wy>().set( Wy );
        pvtrack.field<PVTrackTag::halfD2Chi2DX2>( PVTrackTag::PosCovMatrixElement::v00 ).set( halfD2Chi2DX2_00 );
        pvtrack.field<PVTrackTag::halfD2Chi2DX2>( PVTrackTag::PosCovMatrixElement::v11 ).set( halfD2Chi2DX2_11 );
        pvtrack.field<PVTrackTag::halfD2Chi2DX2>( PVTrackTag::PosCovMatrixElement::v20 ).set( halfD2Chi2DX2_20 );
        pvtrack.field<PVTrackTag::halfD2Chi2DX2>( PVTrackTag::PosCovMatrixElement::v21 ).set( halfD2Chi2DX2_21 );
        pvtrack.field<PVTrackTag::halfD2Chi2DX2>( PVTrackTag::PosCovMatrixElement::v22 ).set( halfD2Chi2DX2_22 );
      }

      const auto Wx   = pvtrack.Wx();
      const auto Wy   = pvtrack.Wy();
      const auto dx   = vtxx - ( pvtrack.x() + dz * tx );
      const auto dy   = vtxy - ( pvtrack.y() + dz * ty );
      const auto chi2 = dx * Wx * dx + dy * Wy * dy;

      if constexpr ( UpdateWeight ) {
        const auto chi2_mask = ( 0 <= pvtrack.pvindex() ) && ( chi2 < chi2max );
        const auto weight    = select( chi2_mask, sqr( 1.f - chi2 / chi2max ), 0.f );
        pvtrack.field<PVTrackTag::weight>().set( weight );
      }
      const auto weight = pvtrack.field<PVTrackTag::weight>().get();

      // compute the derivatives
      const auto halfDChi2DX_0 = Wx * dx;
      const auto halfDChi2DX_1 = Wy * dy;
      const auto halfDChi2DX_2 = -tx * Wx * dx - ty * Wy * dy;

      // Store these such that we can use them for unbiasing, etc.
      pvtrack.field<PVTrackTag::ipchi2>().set( chi2 );
      pvtrack.field<PVTrackTag::halfDChi2DX>( 0 ).set( halfDChi2DX_0 );
      pvtrack.field<PVTrackTag::halfDChi2DX>( 1 ).set( halfDChi2DX_1 );
      pvtrack.field<PVTrackTag::halfDChi2DX>( 2 ).set( halfDChi2DX_2 );

      // Now that we have stored everything, add to the chi2 derivatives of the vertex. We do not need the chi2_mask,
      // because the weight is zero.
      accumulator.chi2 += ( weight * chi2 ).hadd( /*chi2_mask*/ );
      accumulator.halfDChi2DX( 0 ) += ( weight * halfDChi2DX_0 ).hadd();
      accumulator.halfDChi2DX( 1 ) += ( weight * halfDChi2DX_1 ).hadd();
      accumulator.halfDChi2DX( 2 ) += ( weight * halfDChi2DX_2 ).hadd();
      accumulator.halfD2Chi2DX2( 0, 0 ) +=
          ( weight * pvtrack.get<PVTrackTag::halfD2Chi2DX2>( PVTrackTag::PosCovMatrixElement::v00 ) ).hadd();
      accumulator.halfD2Chi2DX2( 1, 1 ) +=
          ( weight * pvtrack.get<PVTrackTag::halfD2Chi2DX2>( PVTrackTag::PosCovMatrixElement::v11 ) ).hadd();
      accumulator.halfD2Chi2DX2( 2, 0 ) +=
          ( weight * pvtrack.get<PVTrackTag::halfD2Chi2DX2>( PVTrackTag::PosCovMatrixElement::v20 ) ).hadd();
      accumulator.halfD2Chi2DX2( 2, 1 ) +=
          ( weight * pvtrack.get<PVTrackTag::halfD2Chi2DX2>( PVTrackTag::PosCovMatrixElement::v21 ) ).hadd();
      accumulator.halfD2Chi2DX2( 2, 2 ) +=
          ( weight * pvtrack.get<PVTrackTag::halfD2Chi2DX2>( PVTrackTag::PosCovMatrixElement::v22 ) ).hadd();
      accumulator.entries += popcount( weight > 0 );
      accumulator.sumw += weight.hadd();
#ifdef MYDEBUGGING
      int foundnan{isnan( accumulator.chi2 )};
      for ( int row = 0; row < 3; ++row )
        for ( int col = 0; col <= row; ++col ) foundnan = foundnan || isnan( accumulator.halfD2Chi2DX2( row, col ) );
      if ( foundnan ) {
        std::cout << "Fitting PV found nan: " << iter << " " << offset << " " << pv.begin() << " " << pv.end() << " "
                  << pvtracks.size() << " " << pv.position() << std::endl;
        std::cout << "chi2 " << accumulator.chi2 << " " << offset << std::endl;
        std::cout << accumulator.halfD2Chi2DX2 << std::endl;
        std::cout << "weight: " << weight << std::endl;
        std::cout << "chi2: " << chi2 << std::endl;
        std::cout << "dx=" << dx << "dy=" << dy << "Wx=" << Wx << "Wy=" << Wy << std::endl;
        std::cout << "covXX: " << pvtrack.covXX() << " " << pvtrack.covXTx() << pvtrack.covTxTx() << std::endl;
        std::cout << "covYY: " << pvtrack.covYY() << " " << pvtrack.covYTy() << pvtrack.covTyTy() << std::endl;
        for ( int i = 0; i < 5; ++i )
          std::cout << "ii: " << i << " " << pvtrack.get<PVTrackTag::halfD2Chi2DX2>( i ) << std::endl;
      }
#endif
    }

    return accumulator;
  }

  /// Fill the track fields that we do not read from the Velo tracks or the persistency. This routine is called by the
  /// unpacker. It does not update the weights: these we will store on the DST.
  void updateDerivatives( ExtendedPrimaryVertex& pv, PVTracks& pvtracks, float chi2max ) {
    Chi2Accumulator accumulator = accumulate<true, true>( pv, pvtracks, chi2max );
    pv.setHalfDChi2DX( accumulator.halfDChi2DX );
    pv.setHalfD2Chi2DX2( accumulator.halfD2Chi2DX2 );
    pv.setSumOfWeights( accumulator.sumw );
  }

  /// Fill the track fields that we do not read from the Velo tracks or the persistency. This routine is called by the
  /// fit and by the unpacker.
  void PrimaryVertexContainer::updateDerivatives() {
    for ( auto& pv : vertices ) LHCb::Event::PV::updateDerivatives( pv, tracks, m_maxDeltaChi2 );
  }

  /// Applies one step of the vertex fit. Returns true if converged.
  template <bool UpdateWeightMatrix, bool UpdateWeight = true>
  PVFitStatus fitSingleStep( ExtendedPrimaryVertex& pv, PVTracks& pvtracks, float chi2max, float maxDeltaZConverged,
                             float maxDeltaChi2Converged ) {
    // update anything inside the pv tracks that is PV position dependent and then accumulate the chi2 derivatives
    Chi2Accumulator accumulator = accumulate<UpdateWeightMatrix, UpdateWeight>( pv, pvtracks, chi2max );
    // call the primary vertex update
    return updatePrimaryVertex( pv, accumulator, maxDeltaZConverged, maxDeltaChi2Converged );
  }

  /// Perform vertex fit
  PVFitStatus fitAdaptive( ExtendedPrimaryVertex& pv, PVTracks& pvtracks, const AdaptiveFitConfig& fitconfig ) {
    // std::cout << "In fitAdaptive: begin=" << pv.begin() << " end=" << pv.end() << " size=" << pv.size()
    //           << " ntracks=" << pv.nTracks() << " " << pv.position() << std::endl ;
    auto     status = PVFitStatus::NotConverged;
    auto     zcache = pv.position().z();
    unsigned iter{0};
    for ( ;
          iter < fitconfig.maxFitIter && ( status == PVFitStatus::NotConverged || iter < fitconfig.numAnnealingSteps );
          ++iter ) {
      // dumb 1-step annealing scheme. don't use for refits.
      const auto chi2maxmultiplier = std::max( int( fitconfig.numAnnealingSteps ) - int( iter ) + 1, 1 );
      const auto thischi2max       = chi2maxmultiplier * fitconfig.maxDeltaChi2;

      // Update the 2nd derivative cache only when vtx moved significantly, or on the first iteration
      constexpr auto maxDeltaZForCache  = 0.2 * Gaudi::Units::mm;
      const auto     vtxz               = pv.position().z();
      const auto     updateWeightMatrix = ( iter == 0 ) || ( std::abs( vtxz - zcache ) > maxDeltaZForCache );
      status = updateWeightMatrix ? fitSingleStep<true>( pv, pvtracks, thischi2max, fitconfig.maxDeltaZConverged,
                                                         fitconfig.maxDeltaChi2Converged )
                                  : fitSingleStep<false>( pv, pvtracks, thischi2max, fitconfig.maxDeltaZConverged,
                                                          fitconfig.maxDeltaChi2Converged );
      if ( status == PVFitStatus::Failed ) {
        pv.setStatus( status );
        return status;
      }
      if ( updateWeightMatrix ) zcache = vtxz;
    }
    // To make sure that we can read back the same vertices from the persistency without storing everything,
    // we update once more the derivatives and weights
    updateDerivatives( pv, pvtracks, fitconfig.maxDeltaChi2 );
    pv.setStatus( status );
    pv.setNumFitIter( iter );
    return status;
  }

  namespace {
    auto convertToPVTrackIndices( const ExtendedPrimaryVertex& pv, LHCb::span<const VeloSegmentID> velosegmentids ) {
      boost::container::small_vector<int, 16> pvtrackindices;
      for ( auto veloid : velosegmentids ) {
        auto it = pv.veloSegmentIDMap().find( veloid );
        if ( it != pv.veloSegmentIDMap().end() ) pvtrackindices.emplace_back( it->second );
      }
      return pvtrackindices;
    }
    auto convertToPVTrackIndices( const PrimaryVertexContainer&   pvdata,
                                  LHCb::span<const VeloSegmentID> velosegmentids ) {
      boost::container::small_vector<int, 16> pvtrackindices;
      // for now we need to collect these from the vertices: it would be better to create one map.
      for ( auto veloid : velosegmentids ) {
        for ( const auto& pv : pvdata.vertices ) {
          auto it = pv.veloSegmentIDMap().find( veloid );
          if ( it != pv.veloSegmentIDMap().end() ) pvtrackindices.emplace_back( it->second );
        }
      }
      return pvtrackindices;
    }
  } // namespace

  // Returns a single unbiased primary vertex.
  PrimaryVertex unbiasedVertex( const PrimaryVertexContainer& pvdata, int pvindex,
                                LHCb::span<const VeloSegmentID> velosegmentids ) {

    const auto pvtrackindices = convertToPVTrackIndices( pvdata[pvindex], velosegmentids );
    // Bail out if the index is wrong. Note that this does not throw an error.
    if ( pvindex == PVIndexInvalid || pvindex < 0 || pvindex >= int( pvdata.size() ) ) return {};
    // make a copy, but including the key
    ExtendedPrimaryVertex pv{pvdata.vertices[pvindex]};
    const auto&           tracks_scalar = pvdata.tracks.scalar();
    Chi2Accumulator       accumulator;
    accumulator.halfD2Chi2DX2 = pv.halfD2Chi2DX2();
    accumulator.halfDChi2DX   = pv.halfDChi2DX();
    for ( auto pvtrkindex : pvtrackindices ) {
      const auto& pvtrack = tracks_scalar[pvtrkindex];
      if ( pvtrack.pvindex().cast() == pvindex ) {
        const auto weight = pvtrack.weight().cast();
        if ( weight > 0 ) {
          accumulator.halfDChi2DX( 0 ) -= weight * pvtrack.halfDChi2DX( 0 ).cast();
          accumulator.halfDChi2DX( 1 ) -= weight * pvtrack.halfDChi2DX( 1 ).cast();
          accumulator.halfDChi2DX( 2 ) -= weight * pvtrack.halfDChi2DX( 2 ).cast();
          accumulator.halfD2Chi2DX2( 0, 0 ) -=
              weight * pvtrack.halfD2Chi2DX2( PVTrackTag::PosCovMatrixElement::v00 ).cast();
          accumulator.halfD2Chi2DX2( 1, 1 ) -=
              weight * pvtrack.halfD2Chi2DX2( PVTrackTag::PosCovMatrixElement::v11 ).cast();
          accumulator.halfD2Chi2DX2( 2, 0 ) -=
              weight * pvtrack.halfD2Chi2DX2( PVTrackTag::PosCovMatrixElement::v20 ).cast();
          accumulator.halfD2Chi2DX2( 2, 1 ) -=
              weight * pvtrack.halfD2Chi2DX2( PVTrackTag::PosCovMatrixElement::v21 ).cast();
          accumulator.halfD2Chi2DX2( 2, 2 ) -=
              weight * pvtrack.halfD2Chi2DX2( PVTrackTag::PosCovMatrixElement::v22 ).cast();
          accumulator.chi2 -= weight * pvtrack.ipchi2().cast();
          accumulator.entries -= 1;
          accumulator.sumw -= weight;
        }
      }
    }
    // 3. update the vertex position
    if ( accumulator.entries != 0 ) {
      accumulator.chi2 += pv.chi2();
      accumulator.entries += ( pv.nDoF() + 3 ) / 2; // reverse engineer
      updatePrimaryVertex( pv, accumulator, 0., 0. );
    }
    return pv;
  }

  PrimaryVertices unbiasedVertices( const PrimaryVertexContainer&   pvdata,
                                    LHCb::span<const VeloSegmentID> velosegmentids ) {
    PrimaryVertices vertices{pvdata.get_allocator()};
    vertices.reserve( pvdata.vertices.size() );
    for ( unsigned int pvindex = 0; pvindex < pvdata.vertices.size(); ++pvindex )
      vertices.emplace_back( unbiasedVertex( pvdata, pvindex, velosegmentids ) );
    return vertices;
  }

  // Returns a single unbiased refit primary vertex.
  PrimaryVertex refittedVertex( const PrimaryVertexContainer& pvdata, int pvindex,
                                LHCb::span<const VeloSegmentID> velosegmentids, const AdaptiveFitConfig& fitconfig ) {
    // Bail out if the index is wrong. Note that this does not throw an error.
    if ( pvindex == PVIndexInvalid || pvindex < 0 || pvindex >= int( pvdata.size() ) ) return {};
    // translate segment IDs into indices in the track container
    const auto pvtrackindices = convertToPVTrackIndices( pvdata[pvindex], velosegmentids );
    // If we need to 'refit' the vertex, then we will need to copy the entire container
    PrimaryVertex pv{pvdata.vertices[pvindex]};

    if ( pvtrackindices.size() > 0 ) {
      // mask the tracks. in the most efficient implementation, we run the loop twice: first to figure out if any of the
      // tracks have positive weight to this vertex, and then a second time to actually copy the container and disable
      // the tracks for the refit
      bool                                    pvchanged = false;
      boost::container::small_vector<int, 16> selectedtracks;
      for ( auto pvtrkindex : pvtrackindices ) {
        // we exploit that the vertex fit masks tracks with negative pvindex
        const auto track       = pvdata.tracks.scalar()[pvtrkindex];
        const auto thispvindex = track.field<PVTrackTag::pvindex>().get();
        if ( thispvindex == pvindex ) {
          if ( pvindex >= 0 ) {
            selectedtracks.emplace_back( pvtrkindex );
            const auto weight = track.field<PVTrackTag::weight>().get();
            if ( weight > 0 ) pvchanged = true;
          }
        }
      }
      if ( pvchanged ) {
        // make a copy of the entire container
        auto pvscopy = pvdata;
        // disable the tracks
        for ( const auto pvtrkindex : selectedtracks )
          pvscopy.tracks.scalar()[pvtrkindex].field<PVTrackTag::pvindex>().set( -1 );
        // refit this vertex
        fitAdaptive( pvscopy.vertices[pvindex], pvscopy.tracks, fitconfig );
        pv = PrimaryVertex{pvscopy.vertices[pvindex]};
      }
    }
    return pv;
  }

  ExtendedPrimaryVertices refittedVertices( const PrimaryVertexContainer&   pvdata,
                                            LHCb::span<const VeloSegmentID> velosegmentids,
                                            const AdaptiveFitConfig&        fitconfig ) {
    const auto pvtrackindices = convertToPVTrackIndices( pvdata, velosegmentids );

    /// copy the pvdata
    auto newpvdata{pvdata};
    if ( pvtrackindices.size() > 0 ) {
      // mask the tracks
      const auto&                              tracks_scalar = newpvdata.tracks.scalar();
      boost::container::small_vector<bool, 16> pvchanged( newpvdata.vertices.size(), false );
      for ( auto pvtrkindex : pvtrackindices ) {
        // we exploit that the vertex fit masks tracks with negative pvindex
        const auto pvindex = tracks_scalar[pvtrkindex].get<PVTrackTag::pvindex>().cast();
        if ( pvindex >= 0 ) {
          tracks_scalar[pvtrkindex].field<PVTrackTag::pvindex>().set( -1 );
          const auto weight = tracks_scalar[pvtrkindex].get<PVTrackTag::weight>().cast();
          if ( weight > 0 ) pvchanged[pvindex] = true;
        }
      }
      // refit the vertices that were changed
      for ( size_t pvindex = 0; pvindex < newpvdata.vertices.size(); ++pvindex )
        if ( pvchanged[pvindex] ) fitAdaptive( newpvdata.vertices[pvindex], newpvdata.tracks, fitconfig );
    }
    return newpvdata.vertices;
  }

  bool ExtendedPrimaryVertex::checkEqual( const ExtendedPrimaryVertex& other ) const {
    bool OK = true;

    if ( m_veloidmap != other.m_veloidmap ) {
      std::cout << __PRETTY_FUNCTION__ << "Different velo track map: " << m_veloidmap.size() << " "
                << other.m_veloidmap.size() << std::endl;
      OK &= false;
    }
    if ( key() != other.key() ) {
      std::cout << __PRETTY_FUNCTION__ << "Vertices have different keys: " << key() << " " << other.key() << std::endl;
      OK &= false;
    }
    if ( !( essentiallyEqual( static_cast<float>( position().x() ), static_cast<float>( other.position().x() ) ) &&
            essentiallyEqual( static_cast<float>( position().y() ), static_cast<float>( other.position().y() ) ) &&
            essentiallyEqual( static_cast<float>( position().z() ), static_cast<float>( other.position().z() ) ) ) ) {
      std::cout << __PRETTY_FUNCTION__ << "Vertices have different position: " << position() << " " << other.position()
                << std::endl;
      std::cout << position().x() - other.position().x() << " " << position().y() - other.position().y() << " "
                << position().z() - other.position().z() << " " << std::numeric_limits<float>::epsilon() << std::endl;
      OK &= false;
    }
    // check cov matrix
    for ( unsigned j = 0; j < std::distance( covMatrix().begin(), covMatrix().end() ); ++j ) {
      const auto covlhs = *( covMatrix().begin() + j );
      const auto covrhs = *( other.covMatrix().begin() + j );
      if ( !essentiallyEqual( static_cast<float>( covlhs ), static_cast<float>( covrhs ) ) ) {
        OK &= false;
        std::cout << __PRETTY_FUNCTION__ << "Vertices have different cov matrix element: " << j << " " << covlhs << " "
                  << covrhs << std::endl;
      }
    }
    // check the weight matrix
    for ( unsigned j = 0; j < halfD2Chi2DX2().m.size(); ++j ) {
      const auto covlhs = halfD2Chi2DX2().m[j];
      const auto covrhs = other.halfD2Chi2DX2().m[j];
      if ( !essentiallyEqual( static_cast<float>( covlhs ), static_cast<float>( covrhs ) ) ) {
        OK &= false;
        std::cout << __PRETTY_FUNCTION__ << "Vertices have different weight matrix element: " << j << " " << covlhs
                  << " " << covrhs << std::endl;
      }
    }
    return OK;
  }

  bool PrimaryVertexContainer::checkEqual( const PrimaryVertexContainer& other ) const {
    bool OK = true;

    if ( velotrackmap != other.velotrackmap ) {
      std::cout << __PRETTY_FUNCTION__ << "Different velo track map: " << velotrackmap.size() << " "
                << other.velotrackmap.size() << std::endl;
      OK &= false;
    }
    if ( vertices.size() != other.size() ) {
      std::cout << __PRETTY_FUNCTION__ << "Different number of vertices: " << vertices.size() << " " << other.size()
                << std::endl;
      OK &= false;
    }
    for ( size_t i = 0; i < vertices.size(); ++i ) {
      const auto& vlhs = vertices[i];
      const auto& vrhs = other.vertices[i];
      OK &= vlhs.checkEqual( vrhs );
    }
    if ( tracks.size() != other.tracks.size() ) {
      std::cout << __PRETTY_FUNCTION__ << "Tracks have different size: " << tracks.size() << " " << other.tracks.size()
                << std::endl;
      OK &= false;
    }
    if ( !tracks.checkEqual( other.tracks ) ) {
      std::cout << __PRETTY_FUNCTION__ << "Tracks are different" << std::endl;
      const auto& t1 = tracks.scalar();
      const auto& t2 = other.tracks.scalar();
      for ( unsigned offset = 0; offset < tracks.size(); ++offset ) {
        std::cout << "(" << t1[offset].field<PVTrackTag::x>().get() - t2[offset].field<PVTrackTag::x>().get() << ") "
                  << "(" << t1[offset].field<PVTrackTag::Wy>().get() - t2[offset].field<PVTrackTag::Wy>().get() << ") "
                  << "("
                  << t1[offset].field<PVTrackTag::halfD2Chi2DX2>( PVTrackTag::PosCovMatrixElement::v22 ).get() -
                         t2[offset].field<PVTrackTag::halfD2Chi2DX2>( PVTrackTag::PosCovMatrixElement::v22 ).get()
                  << ") " << std::endl;
      }
      OK &= false;
    }

    return OK;
  }
} // namespace LHCb::Event::PV
