/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

//-----------------------------------------------------------------------------
/** @file RichPID.cpp
 *
 *  Implementation file for class : LHCb::RichPID
 *
 *  @author  Chris Jones  Christopher.Rob.Jones@cern.ch
 *  @date    2002-06-10
 */
//-----------------------------------------------------------------------------

// STL
#include <algorithm>
#include <sstream>
#include <string>

// RichEvent includes
#include "Event/RichPID.h"

// Boost
#include "boost/format.hpp"

bool LHCb::RichPID::isAboveThreshold( const Rich::ParticleIDType type ) const {
  switch ( type ) {
  case Rich::Electron:
    return this->electronHypoAboveThres();
  case Rich::Muon:
    return this->muonHypoAboveThres();
  case Rich::Pion:
    return this->pionHypoAboveThres();
  case Rich::Kaon:
    return this->kaonHypoAboveThres();
  case Rich::Proton:
    return this->protonHypoAboveThres();
  case Rich::Deuteron:
    return this->deuteronHypoAboveThres();
  default:
    return false;
  }
}

void LHCb::RichPID::setAboveThreshold( const Rich::ParticleIDType type, const bool flag ) {
  if ( Rich::Electron == type ) {
    this->setElectronHypoAboveThres( flag );
  } else if ( Rich::Muon == type ) {
    this->setMuonHypoAboveThres( flag );
  } else if ( Rich::Pion == type ) {
    this->setPionHypoAboveThres( flag );
  } else if ( Rich::Kaon == type ) {
    this->setKaonHypoAboveThres( flag );
  } else if ( Rich::Proton == type ) {
    this->setProtonHypoAboveThres( flag );
  } else if ( Rich::Deuteron == type ) {
    this->setDeuteronHypoAboveThres( flag );
  }
}

std::ostream& LHCb::RichPID::fillStream( std::ostream& s ) const {
  s << "[ ";

  // Formatting strings
  const std::string fF = "%6.2f"; // floats
  const std::string iF = "%5i";   // ints

  // PID type
  s << "Key" << boost::format( iF ) % key();

  // Track info
  if ( track() ) {
    // Track type
    std::ostringstream tType;
    tType << track()->type();
    const unsigned int minTkTypeSize = 10;
    auto               S             = tType.str();
    if ( S.size() < minTkTypeSize ) { S.resize( minTkTypeSize, ' ' ); }
    // print
    s << " | Tk " << boost::format( iF ) % track()->key() << " " << S << " "
      << boost::format( fF ) % ( track()->p() / Gaudi::Units::GeV ) << " GeV";
  } else {
    s << " | NO ASSOCIATED TRACK";
  }

  // Active radiators
  std::ostringstream rads;
  if ( usedAerogel() ) { rads << " " << Rich::text( Rich::Aerogel ); }
  if ( usedRich1Gas() ) { rads << " " << Rich::text( Rich::Rich1Gas ); }
  if ( usedRich2Gas() ) { rads << " " << Rich::text( Rich::Rich2Gas ); }
  const unsigned int minRadsSize = 18; // nice for RICH1 and RICH2 gas
  auto               S           = rads.str();
  if ( S.size() < minRadsSize ) { S.resize( minRadsSize, ' ' ); }
  s << " |" << S;

  // Mass thresholds
  s << " | Thres ";
  for ( const auto pid : Rich::particles() ) { s << ( isAboveThreshold( pid ) ? "T" : "F" ); }

  // DLL values
  s << " | DLLs ";
  for ( const auto pid : Rich::particles() ) { s << " " << boost::format( fF ) % particleDeltaLL( pid ); }

  // Best ID
  s << " | " << bestParticleID();

  // return message stream
  return s << " ]";
}
