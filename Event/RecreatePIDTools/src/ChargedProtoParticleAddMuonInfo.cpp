/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
//-----------------------------------------------------------------------------
/** @file ChargedProtoParticleAddMuonInfo.cpp
 *
 * Implementation file for algorithm ChargedProtoParticleAddMuonInfo
 *
 * @author Chris Jones   Christopher.Rob.Jones@cern.ch
 * @date 28/08/2009
 */
//-----------------------------------------------------------------------------
#include "Event/MuonPID.h"
#include "Event/ProtoParticle.h"
#include "GaudiAlg/GaudiTool.h"
#include "Interfaces/IProtoParticleTool.h"
#include <sstream>

/** @class ChargedProtoParticleAddMuonInfo ChargedProtoParticleAddMuonInfo.h
 *
 *  Updates the MuonPID information stored in the ProtoParticles
 *
 *  @author Chris Jones   Christopher.Rob.Jones@cern.ch
 *  @date 28/08/2009
 */

namespace LHCb::Rec::ProtoParticle::Charged {

  class AddMuonInfo final : public extends<GaudiTool, Interfaces::IProtoParticles> {

  public:
    using extends::extends;

    StatusCode operator()( ProtoParticles& protos, IGeometryInfo const& ) const override { ///< Algorithm execution
      auto muonMap = getMuonData();
      for ( auto* proto : protos ) { updateMuon( *proto, muonMap ); }
      return StatusCode::SUCCESS;
    }

  private:
    /// mapping type from Track to MuonPID data objects
    using TrackToMuonPID = std::map<const Track*, const MuonPID*>;

    /// Load the muon data
    TrackToMuonPID getMuonData() const;

    /// Add (or update) the Muon information for the given ProtoParticle
    void updateMuon( LHCb::ProtoParticle& proto, TrackToMuonPID const& muonMap ) const;

  private:
    DataObjectReadHandle<MuonPID::Range> m_muonPath{
        this, "InputMuonPIDLocation", MuonPIDLocation::Offline}; ///< Location of MuonPID data objects in the TES
    Gaudi::Property<bool> m_removeOldInfo{this, "RemoveOldInfo", true,
                                          "Only to be set to false during unpacking old files!"};
  };

  // Declaration of the Algorithm Factory
  DECLARE_COMPONENT_WITH_ID( AddMuonInfo, "ChargedProtoParticleAddMuonInfo" )

  //=============================================================================
  // Replace MUON info to the protoparticle
  //=============================================================================
  void AddMuonInfo::updateMuon( LHCb::ProtoParticle& proto, TrackToMuonPID const& muonMap ) const {
    if ( msgLevel( MSG::VERBOSE ) ) verbose() << "Trying ProtoParticle " << proto.key() << endmsg;

    // Erase current MuonPID information
    if ( m_removeOldInfo ) proto.removeMuonInfo();

    // Does this track have a MUON PID result ?
    auto iM = muonMap.find( proto.track() );
    if ( muonMap.end() == iM ) {
      if ( msgLevel( MSG::VERBOSE ) ) verbose() << " -> NO associated MuonPID object found" << endmsg;
      return;
    }

    // MuonPID object is found
    const MuonPID* muonPID = iM->second;

    // MuonPID for this track is found, so save data
    if ( msgLevel( MSG::VERBOSE ) )
      verbose() << " -> Found MuonPID data : MuLL=" << muonPID->MuonLLMu() << " BkLL=" << muonPID->MuonLLBg()
                << " nSharedHits=" << muonPID->nShared() << " isMuonLoose=" << muonPID->IsMuonLoose()
                << " isMuon=" << muonPID->IsMuon() << endmsg;

    // reference to MuonPID object
    proto.setMuonPID( muonPID );

    // check IsMuon flag - Reject non-muons
    if ( !muonPID->IsMuon() ) {
      if ( msgLevel( MSG::VERBOSE ) ) { verbose() << " -> Rejected by IsMuon cut" << endmsg; }
      return;
    }

    // print full ProtoParticle content
    if ( msgLevel( MSG::VERBOSE ) ) verbose() << proto << endmsg;
  }

  //=============================================================================
  // Loads the Muon data
  //=============================================================================
  AddMuonInfo::TrackToMuonPID AddMuonInfo::getMuonData() const {
    TrackToMuonPID muonMap{};
    // Do we have any MuonPID results
    const auto muonpids = m_muonPath.get();

    // refresh the reverse mapping
    for ( const auto M : muonpids ) {
      if ( M->idTrack() ) {
        muonMap[M->idTrack()] = M;
        if ( msgLevel( MSG::VERBOSE ) )
          verbose() << "MuonPID key=" << M->key() << " has Track key=" << M->idTrack()->key() << " " << M->idTrack()
                    << endmsg;
      }
    }
    return muonMap;
  }
} // namespace LHCb::Rec::ProtoParticle::Charged
