
/*****************************************************************************\
* (c) Copyright 2020 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#undef NDEBUG
#define BOOST_TEST_DYN_LINK
#define BOOST_TEST_MODULE utestSOACollection
#include "Event/SIMDEventTypes.h"
#include "Event/SOACollection.h"
#include "Kernel/meta_enum.h"
#include "LHCbMath/SIMDWrapper.h"

#include <boost/test/unit_test.hpp>

#include <array>
#include <vector>

using namespace LHCb::Event;

struct Float : float_field {};
struct Int : int_field {};
struct Position2D : floats_field<2> {};

struct Test1D : SOACollection<Test1D, Float> {
  using SOACollection::data;
  auto float_( int at ) { return *( data<Float>() + at ); }
};

struct Test2D : SOACollection<Test2D, Float, Int> {
  using SOACollection::data;
  auto float_( int at ) { return *( data<Float>() + at ); }
  auto int_( int at ) { return *( data<Int>() + at ); }
};

struct Test3D : SOACollection<Test3D, Position2D, Int> {
  using SOACollection::data;
  auto posx_( int at ) { return *( data<Position2D>( 0 ) + at ); }
  auto posy_( int at ) { return *( data<Position2D>( 1 ) + at ); }
  auto int_( int at ) { return *( data<Int>() + at ); }
};

BOOST_AUTO_TEST_CASE( CanDefaultConstruct ) {
  auto test_constr = []( auto const& x ) {
    BOOST_CHECK_EQUAL( x.capacity(), 0 );
    BOOST_CHECK_EQUAL( x.size(), 0 );
  };
  test_constr( Test1D{} );
  test_constr( Test2D{} );
  test_constr( Test3D{} );
}

BOOST_AUTO_TEST_CASE( CanMoveConstruct ) {
  // simplest 1d
  auto test_move = []( auto x ) {
    using type = std::decay_t<decltype( x )>;
    auto xzip  = x.zipIdentifier();
    type y{std::move( x )};
    auto yzip = y.zipIdentifier();
    BOOST_CHECK_EQUAL( xzip, yzip );

    type z{};
    z = std::move( y );
    BOOST_CHECK_EQUAL( z.zipIdentifier(), yzip );
  };
  test_move( Test1D{} );
  test_move( Test2D{} );
  test_move( Test3D{} );
}

BOOST_AUTO_TEST_CASE( ChangeCapacity ) {
  Test1D x{};
  x.reserve( 10 );
  BOOST_CHECK_GE( x.capacity(), 10 );
  BOOST_CHECK_EQUAL( x.size(), 0 );
}

BOOST_AUTO_TEST_CASE( ChangeSize ) {
  Test1D x{};
  x.resize( 10 );
  BOOST_CHECK_GE( x.capacity(), 10 );
  BOOST_CHECK_EQUAL( x.size(), 10 );
}

BOOST_AUTO_TEST_CASE( CanScalarEmplaceBack ) {
  Test1D x{};
  for ( int i = 0; i < 2; i++ ) { x.emplace_back<SIMDWrapper::InstructionSet::Scalar>().field<Float>().set( 1.f ); }
  BOOST_CHECK_EQUAL( x.size(), 2 );
  // first row
  BOOST_CHECK_EQUAL( x.float_( 0 ), 1.f );
  // second row
  BOOST_CHECK_EQUAL( x.float_( 1 ), 1.f );

  Test2D y{};
  for ( int i = 0; i < 2; i++ ) {
    auto a = y.emplace_back<SIMDWrapper::InstructionSet::Scalar>();
    a.field<Float>().set( 1.f );
    a.field<Int>().set( 2 );
  }
  BOOST_CHECK_EQUAL( y.size(), 2 );
  // first row
  BOOST_CHECK_EQUAL( y.float_( 0 ), 1.f );
  BOOST_CHECK_EQUAL( y.int_( 0 ), 2 );
  // second row
  BOOST_CHECK_EQUAL( y.float_( 1 ), 1.f );
  BOOST_CHECK_EQUAL( y.int_( 1 ), 2 );

  Test3D z{};
  for ( int i = 0; i < 2; i++ ) {
    auto a = z.emplace_back<SIMDWrapper::InstructionSet::Scalar>();
    a.field<Position2D>( 0 ).set( 1.f );
    a.field<Position2D>( 1 ).set( 2.f );
    a.field<Int>().set( 3 );
  }
  BOOST_CHECK_EQUAL( z.size(), 2 );
  // first row
  BOOST_CHECK_EQUAL( z.posx_( 0 ), 1.f );
  BOOST_CHECK_EQUAL( z.posy_( 0 ), 2.f );
  BOOST_CHECK_EQUAL( z.int_( 0 ), 3 );
  // second row
  BOOST_CHECK_EQUAL( z.posx_( 1 ), 1.f );
  BOOST_CHECK_EQUAL( z.posy_( 1 ), 2.f );
  BOOST_CHECK_EQUAL( z.int_( 1 ), 3 );
}

BOOST_AUTO_TEST_CASE( CanVectorEmplaceBack ) {
  if ( SIMDWrapper::sse::types::size != 4 ) return;

  Test1D x{};
  // create float_v
  std::array<float, 4>      in_f{1, 2, 3, 4};
  SIMDWrapper::sse::float_v f_v{in_f.data()};
  // emplacing
  x.emplace_back<SIMDWrapper::InstructionSet::SSE>().field<Float>().set( f_v );
  x.compress_back<SIMDWrapper::InstructionSet::SSE>( f_v > 2 ).field<Float>().set( f_v );
  BOOST_CHECK_EQUAL( x.size(), 6 );

  std::vector<float> out_f_1D{x.data<Float>(), x.data<Float>() + 6};
  // test unmasked
  for ( auto i = 0; i < 4; ++i ) { BOOST_CHECK_EQUAL( in_f[i], out_f_1D[i] ); }
  // test masked
  BOOST_CHECK_EQUAL( in_f[2], out_f_1D[4] );
  BOOST_CHECK_EQUAL( in_f[3], out_f_1D[5] );

  Test2D                  y{};
  std::array<int, 4>      in_i{5, 6, 7, 8};
  SIMDWrapper::sse::int_v i_v{in_i.data()};
  {
    auto a = y.emplace_back<SIMDWrapper::InstructionSet::SSE>();
    a.field<Float>().set( f_v );
    a.field<Int>().set( i_v );
  }
  {
    auto a = y.compress_back<SIMDWrapper::InstructionSet::SSE>( f_v > 2 );
    a.field<Float>().set( f_v );
    a.field<Int>().set( i_v );
  }
  BOOST_CHECK_EQUAL( y.size(), 6 );

  std::vector<float> out_f_2D{y.data<Float>(), y.data<Float>() + 6};
  std::vector<int>   out_i_2D{y.data<Int>(), y.data<Int>() + 6};
  for ( auto i = 0; i < 4; ++i ) {
    BOOST_CHECK_EQUAL( in_f[i], out_f_2D[i] );
    BOOST_CHECK_EQUAL( in_i[i], out_i_2D[i] );
  }
  BOOST_CHECK_EQUAL( in_f[2], out_f_2D[4] );
  BOOST_CHECK_EQUAL( in_i[2], out_i_2D[4] );
  BOOST_CHECK_EQUAL( in_f[3], out_f_2D[5] );
  BOOST_CHECK_EQUAL( in_i[3], out_i_2D[5] );

  Test3D                    z{};
  std::array<float, 4>      in_f2{9, 10, 11, 12};
  SIMDWrapper::sse::float_v f2_v{in_f2.data()};
  {
    auto a = z.emplace_back<SIMDWrapper::InstructionSet::SSE>();
    a.field<Position2D>( 0 ).set( f_v );
    a.field<Position2D>( 1 ).set( f2_v );
    a.field<Int>().set( i_v );
  }
  {
    auto a = z.compress_back<SIMDWrapper::InstructionSet::SSE>( f2_v < 11 );
    a.field<Position2D>( 0 ).set( f_v );
    a.field<Position2D>( 1 ).set( f2_v );
    a.field<Int>().set( i_v );
  }
  std::vector<float> out_f_3D{z.data<Position2D>( 0 ), z.data<Position2D>( 0 ) + 6};
  std::vector<float> out_f2_3D{z.data<Position2D>( 1 ), z.data<Position2D>( 1 ) + 6};
  std::vector<int>   out_i_3D{z.data<Int>(), z.data<Int>() + 6};
  for ( auto i = 0; i < 4; ++i ) {
    BOOST_CHECK_EQUAL( in_f[i], out_f_3D[i] );
    BOOST_CHECK_EQUAL( in_f2[i], out_f2_3D[i] );
    BOOST_CHECK_EQUAL( in_i[i], out_i_3D[i] );
  }
  BOOST_CHECK_EQUAL( in_f[0], out_f_3D[4] );
  BOOST_CHECK_EQUAL( in_f2[0], out_f2_3D[4] );
  BOOST_CHECK_EQUAL( in_i[0], out_i_3D[4] );
  BOOST_CHECK_EQUAL( in_f[1], out_f_3D[5] );
  BOOST_CHECK_EQUAL( in_f2[1], out_f2_3D[5] );
  BOOST_CHECK_EQUAL( in_i[1], out_i_3D[5] );
}

BOOST_AUTO_TEST_CASE( CanCopyBack ) {
  // 1D
  Test1D x_from{};
  for ( int x = 1; x <= 5; x++ ) { x_from.emplace_back<SIMDWrapper::InstructionSet::Scalar>().field<Float>().set( x ); }

  // checking scalar copy back
  Test1D x_to_scalar{};
  x_to_scalar.copy_back<SIMDWrapper::InstructionSet::Scalar>( x_from.scalar()[1] );
  x_to_scalar.copy_back<SIMDWrapper::InstructionSet::Scalar>( x_from.scalar()[2] );

  BOOST_CHECK_EQUAL( x_to_scalar.float_( 0 ), x_from.float_( 1 ) );
  BOOST_CHECK_EQUAL( x_to_scalar.float_( 1 ), x_from.float_( 2 ) );

  if ( SIMDWrapper::sse::types::size == 4 ) {
    auto x_from_simd = x_from.simd<SIMDWrapper::InstructionSet::SSE>();
    // vector copy back
    Test1D x_to_vector{};
    x_to_vector.copy_back<SIMDWrapper::InstructionSet::SSE>( x_from_simd[1] );

    for ( auto i = 0u; i < SIMDWrapper::sse::types::size; ++i ) {
      BOOST_CHECK_EQUAL( x_to_vector.float_( i ), x_from.float_( i + 1 ) );
    }

    // masked vector copy back
    Test1D x_to_masked{};
    x_to_masked.copy_back<SIMDWrapper::InstructionSet::SSE>( x_from_simd[1], x_from_simd[0].get<Float>() > 2.f );
    // mask = {0,0,1,1}

    for ( int i = 0; i < 2; ++i ) { BOOST_CHECK_EQUAL( x_to_masked.float_( i ), x_from.float_( i + 3 ) ); }
  }

  // 2D
  Test2D y_from{};
  for ( int x = 1; x <= 5; x++ ) {
    auto a = y_from.emplace_back<SIMDWrapper::InstructionSet::Scalar>();
    a.field<Float>().set( x );
    a.field<Int>().set( x );
  }

  // checking scalar copy back
  Test2D y_to_scalar{};
  y_to_scalar.copy_back<SIMDWrapper::InstructionSet::Scalar>( y_from.scalar()[1] );
  y_to_scalar.copy_back<SIMDWrapper::InstructionSet::Scalar>( y_from.scalar()[2] );

  BOOST_CHECK_EQUAL( y_to_scalar.float_( 0 ), y_from.float_( 1 ) );
  BOOST_CHECK_EQUAL( y_to_scalar.float_( 1 ), y_from.float_( 2 ) );
  BOOST_CHECK_EQUAL( y_to_scalar.int_( 0 ), y_from.int_( 1 ) );
  BOOST_CHECK_EQUAL( y_to_scalar.int_( 1 ), y_from.int_( 2 ) );

  if ( SIMDWrapper::sse::types::size == 4 ) {
    auto y_from_simd = y_from.simd<SIMDWrapper::InstructionSet::SSE>();
    // vector copy back
    Test2D y_to_vector{};
    y_to_vector.copy_back<SIMDWrapper::InstructionSet::SSE>( y_from_simd[1] );

    for ( auto i = 0u; i < SIMDWrapper::sse::types::size; ++i ) {
      BOOST_CHECK_EQUAL( y_to_vector.float_( i ), y_from.float_( i + 1 ) );
      BOOST_CHECK_EQUAL( y_to_vector.int_( i ), y_from.int_( i + 1 ) );
    }

    // masked vector copy back
    Test2D y_to_masked{};
    y_to_masked.copy_back<SIMDWrapper::InstructionSet::SSE>( y_from_simd[1], y_from_simd[0].get<Float>() > 2.f );
    // mask = {0,0,1,1}

    for ( int i = 0; i < 2; ++i ) {
      BOOST_CHECK_EQUAL( y_to_masked.float_( i ), y_from.float_( i + 3 ) );
      BOOST_CHECK_EQUAL( y_to_masked.int_( i ), y_from.int_( i + 3 ) );
    }
  }

  // 3D
  Test3D z_from{};
  for ( int x = 1; x <= 5; x++ ) {
    auto a = z_from.emplace_back<SIMDWrapper::InstructionSet::Scalar>();
    a.field<Position2D>( 0 ).set( x );
    a.field<Position2D>( 1 ).set( x );
    a.field<Int>().set( x );
  }

  // checking scalar copy back
  Test3D z_to_scalar{};
  z_to_scalar.copy_back<SIMDWrapper::InstructionSet::Scalar>( z_from.scalar()[1] );
  z_to_scalar.copy_back<SIMDWrapper::InstructionSet::Scalar>( z_from.scalar()[2] );

  BOOST_CHECK_EQUAL( z_to_scalar.posx_( 0 ), z_from.posx_( 1 ) );
  BOOST_CHECK_EQUAL( z_to_scalar.posx_( 1 ), z_from.posx_( 2 ) );
  BOOST_CHECK_EQUAL( z_to_scalar.posy_( 0 ), z_from.posy_( 1 ) );
  BOOST_CHECK_EQUAL( z_to_scalar.posy_( 1 ), z_from.posy_( 2 ) );
  BOOST_CHECK_EQUAL( z_to_scalar.int_( 0 ), z_from.int_( 1 ) );
  BOOST_CHECK_EQUAL( z_to_scalar.int_( 1 ), z_from.int_( 2 ) );

  if ( SIMDWrapper::sse::types::size == 4 ) {
    auto z_from_simd = z_from.simd<SIMDWrapper::InstructionSet::SSE>();
    // vector copy back
    Test3D z_to_vector{};
    z_to_vector.copy_back<SIMDWrapper::InstructionSet::SSE>( z_from_simd[1] );

    for ( auto i = 0u; i < SIMDWrapper::sse::types::size; ++i ) {
      BOOST_CHECK_EQUAL( z_to_vector.posx_( i ), z_from.posx_( i + 1 ) );
      BOOST_CHECK_EQUAL( z_to_vector.posy_( i ), z_from.posy_( i + 1 ) );
      BOOST_CHECK_EQUAL( z_to_vector.int_( i ), z_from.int_( i + 1 ) );
    }

    // masked vector copy back
    Test3D z_to_masked{};
    z_to_masked.copy_back<SIMDWrapper::InstructionSet::SSE>( z_from_simd[1],
                                                             z_from_simd[0].get<Position2D>( 0 ) > 2.f );
    // mask = {0,0,1,1}

    for ( int i = 0; i < 2; ++i ) {
      BOOST_CHECK_EQUAL( z_to_masked.posx_( i ), z_from.posx_( i + 3 ) );
      BOOST_CHECK_EQUAL( z_to_masked.posy_( i ), z_from.posy_( i + 3 ) );
      BOOST_CHECK_EQUAL( z_to_masked.int_( i ), z_from.int_( i + 3 ) );
    }
  }
}

struct RecursiveTest;
struct ChildIndex : index2D_field<RecursiveTest> {};
struct ChildIndexArray : indices2D_field<RecursiveTest, std::index_sequence<30>, 5> {};
struct ChildNumber : int_field {};

struct RecursiveTest : public SOACollection<RecursiveTest, ChildIndex, ChildIndexArray, ChildNumber, Float> {

  template <typename Tag>
  auto containers() {
    if constexpr ( std::is_same_v<Tag, ChildIndex> || std::is_same_v<Tag, ChildIndexArray> ) { return childContainers; }
  }

  std::vector<RecursiveTest*> childContainers;
};

template <typename Tag, typename TagSize, typename TagValue, typename Proxy, typename data_t, typename operator_t>
data_t accumulate( Proxy proxy, data_t start, operator_t op ) {
  using int_v  = typename Proxy::simd_t::int_v;
  using mask_v = typename Proxy::simd_t::mask_v;
  data_t acc   = start;
  int_v  size  = proxy.template get<TagSize>();
  mask_v mask  = ( 0 < size ) && proxy.loop_mask();
  int    i     = 0;
  while ( any( mask ) ) {
    auto child = proxy.template field<Tag>( i ).get( mask );
    acc        = op( acc, child.template get<TagValue>() );
    mask       = ( ++i < size ) && mask;
  }
  return acc;
}

template <typename Tag, typename TagSize, typename TagValue, typename Proxy, typename data_t, typename operator_t>
data_t accumulate_leaves( Proxy proxy, data_t start, operator_t op ) {
  using int_v  = typename Proxy::simd_t::int_v;
  using mask_v = typename Proxy::simd_t::mask_v;

  int_v  size = proxy.template get<TagSize>();
  data_t acc  = select( size == 0, proxy.template get<TagValue>(), start );

  mask_v mask = ( 0 < size ) && proxy.loop_mask();
  int    i    = 0;
  while ( any( mask ) ) {
    auto child = proxy.template field<Tag>( i ).get( mask );
    acc        = op( acc, accumulate_leaves<Tag, TagSize, TagValue>( child, start, op ) );
    mask       = ( ++i < size ) && mask;
  }
  return acc;
}

BOOST_AUTO_TEST_CASE( test_recursive ) {
  RecursiveTest test1{};
  RecursiveTest test2{};
  RecursiveTest test3{};

  test1.childContainers.push_back( &test2 );
  test1.childContainers.push_back( &test3 );
  test2.childContainers.push_back( &test1 );
  test2.childContainers.push_back( &test3 );
  test3.childContainers.push_back( &test1 );
  test3.childContainers.push_back( &test2 );

  for ( int i = 0; i < 16; i++ ) {
    auto a = test1.emplace_back<SIMDWrapper::InstructionSet::Scalar>();
    a.field<ChildIndex>().set( ( i / 4 ) % 2, i % 4 );
    for ( int j = 0; j < 5; j++ ) a.field<ChildIndexArray>( j ).set( ( i / 4 ) % 2, ( i + j + 1 ) % 4 );
    a.field<ChildNumber>().set( 2 );
    a.field<Float>().set( 100.f + i );

    auto b = test2.emplace_back<SIMDWrapper::InstructionSet::Scalar>();
    b.field<ChildIndex>().set( ( i / 2 ) % 2, i % 4 );
    for ( int j = 0; j < 5; j++ ) b.field<ChildIndexArray>( j ).set( ( i / 2 + 1 ) % 2, ( i + j + 1 ) % 4 );
    b.field<ChildNumber>().set( 1 );
    b.field<Float>().set( 200.f + i );

    auto c = test3.emplace_back<SIMDWrapper::InstructionSet::Scalar>();
    c.field<ChildIndex>().set( ( i / 2 ) % 2, i % 4 );
    c.field<ChildNumber>().set( 0 );
    c.field<Float>().set( 300.f + i );
  }

  auto test1_simd = test1.simd<SIMDWrapper::InstructionSet::Best>();
  using simd      = SIMDWrapper::best::types;

  auto proxy = test1_simd.gather2D( simd::indices(), simd::indices() < simd::size );

  std::cout << "test1.float: " << proxy.get<Float>() << std::endl;
  std::cout << "test1.child.float: " << proxy.get<ChildIndex>().get<Float>() << std::endl;
  std::cout << "test1.child.child.float: " << proxy.get<ChildIndex>().get<ChildIndex>().get<Float>() << std::endl;

  std::cout << "test1.child[0].float: " << proxy.get<ChildIndexArray>( 0 ).get<Float>() << std::endl;
  std::cout << "test1.child[1].float: " << proxy.get<ChildIndexArray>( 1 ).get<Float>() << std::endl;

  std::cout << "accumulate: "
            << accumulate<ChildIndexArray, ChildNumber, Float>( proxy, simd::float_v{0.f}, std::plus<>{} ) << std::endl;
  std::cout << "accumulate leaves: "
            << accumulate_leaves<ChildIndexArray, ChildNumber, Float>( proxy, simd::float_v{0.f}, std::plus<>{} )
            << std::endl;
}

meta_enum_class( PID, int, Unknown, Electron, Muon, Pion, Kaon, Proton, Ghost ) struct PIDTag : enum_field<PID> {};
struct SIMDEnumTest : public SOACollection<SIMDEnumTest, PIDTag> {};

BOOST_AUTO_TEST_CASE( test_simd_enum ) {
  SIMDEnumTest test{};
  for ( int i = 0; i < 4; i++ ) {
    test.emplace_back<SIMDWrapper::InstructionSet::Scalar>().field<PIDTag>().set( PID::Electron );
    test.emplace_back<SIMDWrapper::InstructionSet::Scalar>().field<PIDTag>().set( PID::Muon );
    test.emplace_back<SIMDWrapper::InstructionSet::Scalar>().field<PIDTag>().set( PID::Pion );
    test.emplace_back<SIMDWrapper::InstructionSet::Scalar>().field<PIDTag>().set( PID::Kaon );
    test.emplace_back<SIMDWrapper::InstructionSet::Scalar>().field<PIDTag>().set( PID::Proton );
  }
  std::cout << test.simd()[0].get<PIDTag>() << std::endl;

  BOOST_CHECK( any( test.simd()[0].get<PIDTag>() == PID::Pion ) );
  BOOST_CHECK( none( test.simd()[0].get<PIDTag>() == PID::Ghost ) );
}

struct MyStruct : struct_field<Int, Float> {};
struct VectorOfStruct : vector_field<struct_field<Int, Float>> {};

struct VectorStructTest : public SOACollection<VectorStructTest, MyStruct, VectorOfStruct> {};

BOOST_AUTO_TEST_CASE( test_vector_struct ) {
  VectorStructTest test{};
  for ( int i = 0; i < 16; i++ ) {
    auto const proxy = test.emplace_back<SIMDWrapper::InstructionSet::Scalar>();
    proxy.field<MyStruct>().field<Int>().set( i );
    proxy.field<MyStruct>().field<Float>().set( 2.f * i );
    auto const vec = proxy.field<VectorOfStruct>();
    vec.resize( 4 );
    for ( int j = 0; j < 4; j++ ) {
      vec[j].field<Int>().set( j );
      vec[j].field<Float>().set( 2.f * j );
    }
  }

  for ( const auto proxy : test.simd() ) {
    std::cout << proxy.field<MyStruct>().field<Int>().get() << std::endl;
    std::cout << proxy.field<MyStruct>().field<Float>().get() << std::endl;
  }
}

struct TypeA : public SOACollection<TypeA, Int> {};
struct TypeB;
struct TypeABIndexArray : vector_field<index2D_field<std::tuple<TypeA, TypeB>, std::index_sequence<6, 24>>> {};
struct TypeB : public SOACollection<TypeB, Float> {};

// TopContainer contains an array of 2 indices that can point to either TypeA or TypeB
struct TopContainer : public SOACollection<TopContainer, TypeABIndexArray> {

  template <typename Tag>
  auto containers() {
    if constexpr ( std::is_same_v<Tag, TypeABIndexArray> ) { return childContainers; }
  }

  std::tuple<std::vector<TypeA*>, std::vector<TypeB*>> childContainers;
};

BOOST_AUTO_TEST_CASE( test_indices_variant ) {
  TypeA        a1{};
  TypeA        a2{};
  TypeB        b1{};
  TypeB        b2{};
  TopContainer top{};
  std::get<0>( top.childContainers ).push_back( &a1 );
  std::get<0>( top.childContainers ).push_back( &a2 );
  std::get<1>( top.childContainers ).push_back( &b1 );
  std::get<1>( top.childContainers ).push_back( &b2 );
  for ( int i = 0; i < 16; i++ ) {
    auto pa1 = a1.emplace_back<SIMDWrapper::InstructionSet::Scalar>();
    pa1.field<Int>().set( i );
    auto pa2 = a2.emplace_back<SIMDWrapper::InstructionSet::Scalar>();
    pa2.field<Int>().set( 16 + i );
    auto pb1 = b1.emplace_back<SIMDWrapper::InstructionSet::Scalar>();
    pb1.field<Float>().set( i + 0.5f );
    auto pb2 = b2.emplace_back<SIMDWrapper::InstructionSet::Scalar>();
    pb2.field<Float>().set( i + 16.5f );

    auto ptop = top.emplace_back<SIMDWrapper::InstructionSet::Scalar>();
    ptop.field<TypeABIndexArray>().resize( 2 );
    ptop.field<TypeABIndexArray>()[0].set( i % 2, ( i / 2 ) % 2, i );
    ptop.field<TypeABIndexArray>()[1].set( ( i + 1 ) % 2, ( i / 2 ) % 2, i );
  }

  auto top_simd = top.simd<SIMDWrapper::InstructionSet::Best>();
  using simd    = SIMDWrapper::best::types;

  auto proxy = top_simd.gather2D( simd::indices(), simd::indices() < simd::size );

  for ( int i = 0; i < 2; i++ ) { // iterate over the index array
    auto pA = std::get<0>( proxy.field<TypeABIndexArray>()[i].get() );
    std::cout << i << " TypeA: " << pA.loop_mask() << " " << pA.get<Int>() << std::endl;
    auto pB = std::get<1>( proxy.field<TypeABIndexArray>()[i].get() );
    std::cout << i << " TypeB: " << pB.loop_mask() << " " << pB.get<Float>() << std::endl;
  }
}