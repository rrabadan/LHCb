
/*****************************************************************************\
* (c) Copyright 2020 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#undef NDEBUG
#define BOOST_TEST_DYN_LINK
#define BOOST_TEST_MODULE utestSOAPacker
#include "Event/PackedDataBuffer.h"
#include "Event/SIMDEventTypes.h"
#include "Event/SOACollection.h"
#include "LHCbMath/SIMDWrapper.h"

#include <boost/test/unit_test.hpp>

#include <array>
#include <vector>

using namespace LHCb::Event;

struct A : int_field {};
struct B : float_field {};
struct C : int_field {
  using packer_t = SOADontPack; // custom packer
};
struct D : int_field {
  using packer_t = SOAPackIfVersionNewerOrEqual<2, SOAPackNumeric<int>>; // conditional packer (false)
};
struct E : float_field {
  using packer_t =
      SOAPackIfVersionNewerOrEqual<0, SOAPackFloatAs<short, std::ratio<1, 100>>>; // conditional packer (true)
};
struct V : vector_field<int_field> {};
struct LHCbID : lhcbid_field {};
struct Vempty : vector_field<struct_field<LHCbID>> {};
struct TrackLike : public SOACollection<TrackLike, A, B, C, D, E, V, Vempty> {};

BOOST_AUTO_TEST_CASE( test_packer ) {
  std::size_t n = 20;
  TrackLike   test{};
  for ( std::size_t i = 0; i < n; i++ ) {
    auto proxy = test.emplace_back<SIMDWrapper::InstructionSet::Scalar>();
    proxy.field<A>().set( i );
    proxy.field<B>().set( 0.5f * i );
    proxy.field<E>().set( 100.f * i );
    auto v = proxy.field<V>();
    v.resize( i );
    for ( std::size_t j = 0; j < i; j++ ) { v[j].set( j ); }
    // proxy.field<Vempty>().resize( 0 ); // don't resize to test size initialization
  }

  // Pack the collection into writeBuffer
  LHCb::Hlt::PackedData::ByteBuffer writeBuffer;
  test.pack<true>( writeBuffer );

  // Check the buffer's size
  std::size_t expected_byte_size = sizeof( std::size_t ) + sizeof( unsigned ); // size of the collection + version
  expected_byte_size += sizeof( int32_t );                                     // zip familly identifier
  expected_byte_size += n * ( sizeof( int ) + sizeof( float ) );               // fields A and B (C is not packed)
  expected_byte_size += n * ( sizeof( short ) );                               // field E (D is not packed)
  expected_byte_size += n * sizeof( int ) + ( ( n * ( n - 1 ) ) / 2 ) * sizeof( int ); // field V
  expected_byte_size += n * sizeof( int );                                             // field Vempty
  BOOST_CHECK_EQUAL( writeBuffer.pos(), expected_byte_size );

  // Copy the buffer (to reset the head)
  LHCb::Hlt::PackedData::ByteBuffer readBuffer;
  readBuffer.init( writeBuffer.buffer() );

  // Unpack the collection
  TrackLike test_unpacked{};
  test_unpacked.unpack<true>( readBuffer );

  // Check collections are equal
  BOOST_CHECK( test.checkEqual( test_unpacked ) );

  test_unpacked.scalar()[5].field<B>().set( 42.f );

  // Check collections are different
  BOOST_CHECK( !test.checkEqual( test_unpacked ) );
}

BOOST_AUTO_TEST_CASE( test_jsondump ) {
  std::size_t n = 20;
  TrackLike   test{};
  for ( std::size_t i = 0; i < n; i++ ) {
    auto proxy = test.emplace_back<SIMDWrapper::InstructionSet::Scalar>();
    proxy.field<A>().set( i );
    proxy.field<B>().set( 0.5f * i );
    proxy.field<E>().set( 100.f * i );
    auto v = proxy.field<V>();
    v.resize( i );
    for ( std::size_t j = 0; j < i; j++ ) { v[j].set( j ); }
    auto vempty = proxy.field<Vempty>();
    vempty.resize( 19 - i );
    for ( std::size_t j = 0; j < 19 - i; j++ ) {
      vempty[j].template field<LHCbID>().set( SIMDWrapper::scalar::int_v{static_cast<int>( j )} );
    }
  }

  test.jsonDump( std::cout );
}