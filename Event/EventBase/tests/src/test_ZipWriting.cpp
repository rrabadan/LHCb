
/*****************************************************************************\
* (c) Copyright 2020 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#undef NDEBUG
#define BOOST_TEST_DYN_LINK
#define BOOST_TEST_MODULE utestWriteToZips
#include "Event/SOACollection.h"
#include "LHCbMath/MatVec.h"
#include "LHCbMath/SIMDWrapper.h"

#include <boost/test/unit_test.hpp>

#include <array>
#include <vector>

using namespace LHCb::Event;

// Define a trivial SOACollection-based container with a few columns
struct Int : int_field {};
struct Float : float_field {};
struct VecX : float_field {};
struct VecY : float_field {};
struct VecZ : float_field {};
struct State : floats_field<2> {
  enum { XOffset = 0, YOffset, NumColumns };

  template <SIMDWrapper::InstructionSet Simd, LHCb::Pr::ProxyBehaviour Behaviour, typename ContainerTypeBase,
            typename Path>
  struct StateProxy {
    using simd_t        = SIMDWrapper::type_map_t<Simd>;
    using type          = typename simd_t::float_v;
    using OffsetType    = LHCb::Pr::detail::offset_t<Behaviour, Simd>;
    using ContainerType = LHCb::Pr::detail::container_t<Behaviour, Simd, ContainerTypeBase>;
    using proxy_type =
        NumericProxy<Simd, Behaviour, ContainerTypeBase, typename Path::template extend_t<unsigned>, float>;
    StateProxy( ContainerType container, OffsetType offset, Path path )
        : m_container( container ), m_offset( offset ), m_path( path ) {}
    template <unsigned i>
    [[nodiscard]] constexpr auto proxy() const {
      return proxy_type{m_container, m_offset, m_path.append( i )};
    }
    [[nodiscard, gnu::always_inline]] inline constexpr auto x() const { return proxy<XOffset>().get(); }
    [[nodiscard, gnu::always_inline]] inline constexpr auto y() const { return proxy<YOffset>().get(); }
    [[gnu::always_inline]] inline constexpr void setX( type const& x ) const { return proxy<XOffset>().set( x ); }
    [[gnu::always_inline]] inline constexpr void setY( type const& y ) const { return proxy<YOffset>().set( y ); }

  private:
    ContainerType m_container;
    OffsetType    m_offset;
    Path          m_path;
  };

  template <SIMDWrapper::InstructionSet Simd, LHCb::Pr::ProxyBehaviour Behaviour, typename ContainerType, typename Path>
  using proxy_type = StateProxy<Simd, Behaviour, ContainerType, Path>;
};
struct States : vector_field<State> {};

struct Container : SOACollection<Container, Int, Float, VecX, VecY, VecZ, States> {
  template <SIMDWrapper::InstructionSet simd, LHCb::Pr::ProxyBehaviour behaviour, typename ContainerType>
  struct CustomProxy : Proxy<simd, behaviour, ContainerType> {
    using LHCb::Event::Proxy<simd, behaviour, ContainerType>::Proxy;
    using simd_t  = SIMDWrapper::type_map_t<simd>;
    using int_v   = typename simd_t::int_v;
    using float_v = typename simd_t::float_v;

    // Accessors
    [[nodiscard]] auto i() const { return this->template get<Int>(); }
    [[nodiscard]] auto f() const { return this->template get<Float>(); }
    [[nodiscard]] auto vec() const {
      return LHCb::LinAlg::Vec{this->template get<VecX>(), this->template get<VecY>(), this->template get<VecZ>()};
    }
    [[nodiscard]] auto stateX( int i ) const { return state( i ).x(); }
    [[nodiscard]] auto stateY( int i ) const { return state( i ).y(); }

    // Setters
    void setI( int_v const& i ) const { this->template field<Int>().set( i ); }
    void setF( float_v const& f ) const { this->template field<Float>().set( f ); }
    void setVec( LHCb::LinAlg::Vec<float_v, 3> const& v ) const {
      this->template field<VecX>().set( v( 0 ) );
      this->template field<VecY>().set( v( 1 ) );
      this->template field<VecZ>().set( v( 2 ) );
    }
    void setStateX( int i, float_v const& x ) const { state( i ).setX( x ); }
    void setStateY( int i, float_v const& y ) const { state( i ).setY( y ); }

    // Return a proxy to the ith state vector
    [[nodiscard]] auto state( std::size_t i ) const { return states()[i]; }
    [[nodiscard]] auto states() const { return this->template field<States>(); }
  };
  template <SIMDWrapper::InstructionSet simd, LHCb::Pr::ProxyBehaviour behaviour, typename ContainerType>
  using proxy_type = CustomProxy<simd, behaviour, ContainerType>;
};

namespace {
  constexpr auto simd = SIMDWrapper::Best;
  using simd_t        = SIMDWrapper::type_map_t<simd>;
  using int_v         = simd_t::int_v;
  using float_v       = simd_t::float_v;
} // namespace

// Test adding space for simd_t::size elements to a container and using a
// writable proxy object to fill in the content.
BOOST_AUTO_TEST_CASE( TestContiguousStore ) {
  Container      c{};
  constexpr auto epsilon = std::numeric_limits<float>::epsilon();
  auto const     added   = c.emplace_back<simd>();
  // Now `added` refers to `simd_t::size` new elements added to the end of `c`
  BOOST_CHECK_EQUAL( c.size(), simd_t::size );
  // The values returned by `added.i()` and `added.f()` are unspecified. It
  // would be nice if the test could assert that...but that seems like it's
  // probably more trouble than it's worth...
  // TODO: should there be a way of telling `emplace_back` that it *should*
  //       initialise the values? if so, we could

  // Check that we can write a value via `added`
  added.setI( int_v{42} );
  // And that we can read back the same value...
  BOOST_CHECK( all( added.i() == 42 ) );
  // Same for the other column
  added.setF( float_v{-7.f} );
  BOOST_CHECK( all( abs( added.f() - -7.f ) < epsilon ) );
  // Finally for the vector column
  added.setVec( {1.f, 2.f, 3.f} );
  BOOST_CHECK( all( abs( added.vec().x() - 1.f ) < epsilon ) );
  BOOST_CHECK( all( abs( added.vec().y() - 2.f ) < epsilon ) );
  BOOST_CHECK( all( abs( added.vec().z() - 3.f ) < epsilon ) );
  // Now try using the state proxies
  added.states().resize( 2 );
  auto const state0 = added.state( 0 );
  auto const state1 = added.state( 1 );
  state0.setX( float_v{14.f} );
  state0.setY( float_v{-1.f} );
  state1.setX( float_v{50.f} );
  state1.setY( float_v{-4.f} );
  BOOST_CHECK( all( abs( state0.x() - float_v{14.f} ) < epsilon ) );
  BOOST_CHECK( all( abs( state0.y() - float_v{-1.f} ) < epsilon ) );
  BOOST_CHECK( all( abs( state1.x() - float_v{50.f} ) < epsilon ) );
  BOOST_CHECK( all( abs( state1.y() - float_v{-4.f} ) < epsilon ) );
  BOOST_CHECK( all( abs( added.stateX( 0 ) - state0.x() ) < epsilon ) );
  BOOST_CHECK( all( abs( added.stateY( 0 ) - state0.y() ) < epsilon ) );
  BOOST_CHECK( all( abs( added.stateX( 1 ) - state1.x() ) < epsilon ) );
  BOOST_CHECK( all( abs( added.stateY( 1 ) - state1.y() ) < epsilon ) );
}

// Test adding space for `popcount( mask )` elements to a container and using a
// writable proxy object to fill in the content using compress-store operations
BOOST_AUTO_TEST_CASE( TestCompressStore ) {
  Container      c{};
  constexpr auto epsilon = std::numeric_limits<float>::epsilon();
  // Construct a mask that alternates {0101...}
  auto const mask = [] {
    std::array<int, simd_t::size> tmp;
    for ( auto i = 0ul; i < tmp.size(); ++i ) { tmp[i] = i % 2; }
    auto const mask = int_v{tmp.data()} > int_v{0};
    for ( auto i = 0ul; i < tmp.size(); ++i ) { BOOST_CHECK_EQUAL( testbit( mask, i ), tmp[i] ); }
    return mask;
  }();
  BOOST_CHECK_EQUAL( popcount( mask ), simd_t::size / 2 );
  // Construct a writable proxy object that can be used to compress-store
  // values using `mask`
  auto const added = c.compress_back<simd>( mask );
  BOOST_CHECK_EQUAL( c.size(), popcount( mask ) );
  // Store {0, 1, 2, ...} compressed with {010101...},
  // for 256bit this should lead to {1, 3, 5, 7, junk, junk, junk, junk} being
  // stored...note that this is dangerous if the proxy does not refer to the
  // end of the container, where SOACollection guarantees the junk writes are
  // safe. TODO throw/warn in that case
  added.setI( simd_t::indices() );
  added.setF( simd_t::indices() );
  added.setVec( {simd_t::indices(), simd_t::indices(), simd_t::indices()} );
  // Read the values back...compress proxies are write-only [for the moment],
  // so instead let's make a contiguous read proxy into the container and use
  // that for the checking.
  auto const contiguous_added = c.simd<simd>()[0];
  BOOST_CHECK_EQUAL( popcount( contiguous_added.loop_mask() ), popcount( mask ) );
  // Helper to generate {1, 3, 5, ..., junk * simd_t::size / 2}
  auto const compressed = [] {
    std::array<int, simd_t::size> tmp;
    for ( auto i = 0ul; i < tmp.size(); ++i ) { tmp[i] = i < tmp.size() / 2 ? 2 * i + 1 : -42; }
    return int_v{tmp.data()};
  }();
  // Construct a mask {11...0...} (half ones, then half zeroes), these are the
  // entries in `added.i()` that should be valid
  auto const compressed_mask = simd_t::loop_mask( simd_t::size / 2, simd_t::size );
  BOOST_CHECK( none( !compressed_mask && contiguous_added.loop_mask() ) );
  // Check that the stored values are as expected
  BOOST_CHECK( all( contiguous_added.i() == compressed || !compressed_mask ) );
  BOOST_CHECK( all( abs( contiguous_added.f() - compressed ) < epsilon || !compressed_mask ) );
  auto const compressed_vec = contiguous_added.vec();
  BOOST_CHECK( all( abs( compressed_vec.x() - compressed ) < epsilon || !compressed_mask ) );
  BOOST_CHECK( all( abs( compressed_vec.y() - compressed ) < epsilon || !compressed_mask ) );
  BOOST_CHECK( all( abs( compressed_vec.z() - compressed ) < epsilon || !compressed_mask ) );
}
