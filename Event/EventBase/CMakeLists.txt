###############################################################################
# (c) Copyright 2000-2022 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
#[=======================================================================[.rst:
Event/EventBase
---------------
#]=======================================================================]

gaudi_add_header_only_library(EventBase
    LINK
        LHCb::LHCbAlgsLib
        LHCb::LHCbKernel
        LHCb::LHCbMathLib
)

gaudi_add_dictionary(EventBaseDict
    HEADERFILES dict/dictionary.h
    SELECTION dict/selection.xml
    LINK LHCb::EventBase
)

foreach(test IN ITEMS test_SOACollection test_SOAPacker test_UniqueIDGenerator test_ZipSTL test_ZipWriting)
    gaudi_add_executable(${test}
        SOURCES tests/src/${test}.cpp
        LINK 
            Boost::unit_test_framework
            LHCb::EventBase
            LHCb::EventPackerLib
        TEST
    )
endforeach()

if(CMAKE_SYSTEM_PROCESSOR STREQUAL x86_64)
    # FIXME: with lcg-toolchains, x86_64 means "-march=x86_64" while with LbDevTools
    # meant "-march=x86_64 -msse4.2", and test_ZipWriting requires vectors of even size
    set_property(SOURCE tests/src/test_ZipWriting.cpp APPEND PROPERTY COMPILE_OPTIONS "-msse4.2")
endif()
