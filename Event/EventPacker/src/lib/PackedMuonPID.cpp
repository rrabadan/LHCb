/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#include "Event/PackedMuonPID.h"
#include "Event/PackedEventChecks.h"

using namespace LHCb;

void MuonPIDPacker::pack( const Data& pid, PackedData& ppid, PackedDataVector& ppids ) const {
  const auto ver = ppids.packingVersion();
  if ( !isSupportedVer( ver ) ) return;
  if ( ver <= 1 ) throw GaudiException( "Unsupported packing version", __PRETTY_FUNCTION__, StatusCode::FAILURE );
  // save the key
  ppid.key = pid.key();

  ppid.MuonLLMu = StandardPacker::deltaLL( pid.MuonLLMu() );
  ppid.MuonLLBg = StandardPacker::deltaLL( pid.MuonLLBg() );
  if ( ver > 2 ) {
    ppid.chi2Corr = StandardPacker::fltPacked( pid.chi2Corr() );
    ppid.muonMVA1 = StandardPacker::mva( pid.muonMVA1() );
    ppid.muonMVA2 = StandardPacker::mva( pid.muonMVA2() );
    ppid.muonMVA3 = StandardPacker::mva( pid.muonMVA3() );
    ppid.muonMVA4 = StandardPacker::mva( pid.muonMVA4() );
  }
  ppid.nShared = (int)pid.nShared();
  ppid.status  = (int)pid.Status();
  if ( pid.idTrack() ) ppid.idtrack = StandardPacker::reference64( &ppids, pid.idTrack() );
  if ( pid.muonTrack() ) ppid.mutrack = StandardPacker::reference64( &ppids, pid.muonTrack() );
}

StatusCode MuonPIDPacker::unpack( const PackedData& ppid, Data& pid, const PackedDataVector& ppids,
                                  DataVector& pids ) const {
  const auto ver = ppids.packingVersion();
  if ( !isSupportedVer( ver ) ) return StatusCode::FAILURE;
  pid.setMuonLLMu( StandardPacker::deltaLL( ppid.MuonLLMu ) );
  pid.setMuonLLBg( StandardPacker::deltaLL( ppid.MuonLLBg ) );
  if ( ver > 2 ) {
    pid.setChi2Corr( StandardPacker::fltPacked( ppid.chi2Corr ) );
    pid.setMuonMVA1( StandardPacker::mva( ppid.muonMVA1 ) );
    pid.setMuonMVA2( StandardPacker::mva( ppid.muonMVA2 ) );
    pid.setMuonMVA3( StandardPacker::mva( ppid.muonMVA3 ) );
    pid.setMuonMVA4( StandardPacker::mva( ppid.muonMVA4 ) );
  }
  pid.setNShared( ppid.nShared );
  pid.setStatus( ppid.status );
  auto unpack_ref = StandardPacker::UnpackRef{&ppids, &pids, StandardPacker::UnpackRef::Use32{ver <= 1}};
  if ( -1 != ppid.idtrack ) {
    if ( auto t = unpack_ref( ppid.idtrack ); t ) {
      pid.setIDTrack( t );
    } else {
      parent().error() << "Corrupt MuonPID Track SmartRef detected." << endmsg;
    }
  }

  // v2 muon PIDs don't have muonTracks anymore so muonTrack is set to be NULL in converting
  // Is it okay to remove this - needed for adding muon hits temporarily before we move to v2 Muon PIDs

  if ( -1 != ppid.mutrack ) {
    if ( auto t = unpack_ref( ppid.mutrack ); t ) {
      pid.setMuonTrack( t );
    } else {
      parent().error() << "Corrupt MuonPID MuTrack SmartRef detected." << endmsg;
    }
  }
  return StatusCode::SUCCESS;
}

StatusCode MuonPIDPacker::unpack( const PackedDataVector& ppids, DataVector& pids ) const {
  const auto ver = ppids.packingVersion();
  if ( !isSupportedVer( ver ) ) return StatusCode::FAILURE;
  pids.reserve( ppids.data().size() );
  StatusCode sc = StatusCode::SUCCESS;
  for ( const auto& ppid : ppids.data() ) {
    // make and save new pid in container
    auto* pid = new Data();
    if ( 0 == ver ) {
      pids.add( pid );
    } else {
      pids.insert( pid, ppid.key );
    }
    // Fill data from packed object
    auto sc2 = unpack( ppid, *pid, ppids, pids );
    if ( sc.isSuccess() ) sc = sc2;
  }
  return sc;
}

StatusCode MuonPIDPacker::check( const Data* dataA, const Data* dataB ) const {
  // checker
  const DataPacking::DataChecks ch( parent() );

  // assume OK from the start
  bool ok = true;

  // key
  ok &= ch.compareInts( "Key", dataA->key(), dataB->key() );
  // History code
  ok &= ch.compareInts( "Status", dataA->Status(), dataB->Status() );
  // Track references
  ok &= ch.comparePointers( "Track", dataA->idTrack(), dataB->idTrack() );
  ok &= ch.comparePointers( "MuonTrack", dataA->muonTrack(), dataB->muonTrack() );
  // DLLs
  ok &= ch.compareDeltaLLs( "MuonLLMu", dataA->MuonLLMu(), dataB->MuonLLMu() );
  ok &= ch.compareDeltaLLs( "MuonLLBg", dataA->MuonLLBg(), dataB->MuonLLBg() );
  ok &= ch.compareDoubles( "chi2Corr", dataA->chi2Corr(), dataB->chi2Corr() );
  ok &= ch.compareMVAs( "muonMVA1", dataA->muonMVA1(), dataB->muonMVA1() );
  ok &= ch.compareMVAs( "muonMVA2", dataA->muonMVA2(), dataB->muonMVA2() );
  ok &= ch.compareMVAs( "muonMVA3", dataA->muonMVA3(), dataB->muonMVA3() );
  ok &= ch.compareMVAs( "muonMVA4", dataA->muonMVA4(), dataB->muonMVA4() );

  // force printout for tests
  // ok = false;
  // If comparison not OK, print full information
  if ( !ok ) {
    const std::string loc =
        ( dataA->parent() && dataA->parent()->registry() ? dataA->parent()->registry()->identifier() : "Not in TES" );
    parent().warning() << "Problem with MuonPID data packing :-" << endmsg << "  Original PID key=" << dataA->key()
                       << " in '" << loc << "'" << endmsg << dataA << endmsg << "  Unpacked PID" << endmsg << dataB
                       << endmsg;
  }

  return ( ok ? StatusCode::SUCCESS : StatusCode::FAILURE );
}

namespace LHCb {
  StatusCode unpack( Gaudi::Algorithm const* parent, const MuonPIDPacker::PackedDataVector& in,
                     MuonPIDPacker::DataVector& out ) {
    return MuonPIDPacker{parent}.unpack( in, out );
  }
} // namespace LHCb
