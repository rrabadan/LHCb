/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#include "Event/PackedMCCaloHit.h"
#include "Event/PackedEventChecks.h"

using namespace LHCb;

/// Scale factor for energy
constexpr double c_energyScale{1.0e2};

void MCCaloHitPacker::pack( const DataVector& hits, PackedDataVector& phits ) const {
  const auto ver = phits.packingVersion();
  if ( !isSupportedVer( ver ) ) return;
  if ( ver == 0 ) throw GaudiException( "Unsupported packing version", __PRETTY_FUNCTION__, StatusCode::FAILURE );
  phits.data().reserve( hits.size() );
  for ( const auto* hit : hits ) {
    auto& phit     = phits.data().emplace_back();
    phit.activeE   = StandardPacker::energy( hit->activeE() * c_energyScale );
    phit.sensDetID = hit->sensDetID();
    phit.time      = hit->time();
    if ( hit->particle() ) phit.mcParticle = StandardPacker::reference64( &phits, hit->particle() );
  }
}

StatusCode MCCaloHitPacker::unpack( const PackedDataVector& phits, DataVector& hits ) const {
  const auto ver = phits.packingVersion();
  if ( !isSupportedVer( ver ) ) return StatusCode::FAILURE;
  hits.reserve( phits.data().size() );
  auto unpack_ref = StandardPacker::UnpackRef{&phits, &hits, StandardPacker::UnpackRef::Use32{ver == 0}};
  for ( const auto& phit : phits.data() ) {
    // make and save new hit in container
    auto* hit = new Data();
    hits.add( hit );
    // Fill data from packed object
    hit->setActiveE( StandardPacker::energy( phit.activeE ) / c_energyScale );
    hit->setSensDetID( phit.sensDetID );
    hit->setTime( phit.time );
    if ( -1 != phit.mcParticle ) {
      if ( auto p = unpack_ref( phit.mcParticle ); p ) {
        hit->setParticle( p );
      } else {
        parent().error() << "Corrupt MCCaloHit MCParticle SmartRef detected." << endmsg;
      }
    }
  }
  return StatusCode::SUCCESS;
}

StatusCode MCCaloHitPacker::check( const DataVector& dataA, const DataVector& dataB ) const {
  StatusCode sc = StatusCode::SUCCESS;

  // checker
  const DataPacking::DataChecks ch( parent() );

  // Loop over data containers together and compare
  auto iA( dataA.begin() ), iB( dataB.begin() );
  for ( ; iA != dataA.end() && iB != dataB.end(); ++iA, ++iB ) {
    // assume OK from the start
    bool ok = true;
    // Detector ID
    ok &= ch.compareInts( "SensDetID", ( *iA )->sensDetID(), ( *iB )->sensDetID() );
    // energy
    ok &= ch.compareEnergies( "Energy", ( *iA )->activeE(), ( *iB )->activeE() );
    // tof
    ok &= ch.compareDoubles( "TOF", ( *iA )->time(), ( *iB )->time() );
    // MCParticle reference
    ok &= ch.comparePointers( "MCParticle", ( *iA )->particle(), ( *iB )->particle() );

    // force printout for tests
    // ok = false;
    // If comparison not OK, print full information
    if ( !ok ) {
      parent().warning() << "Problem with MCCaloHit data packing :-" << endmsg << "  Original Hit : " << **iA << endmsg
                         << "  Unpacked Hit : " << **iB << endmsg;
      sc = StatusCode::FAILURE;
    }
  }

  // Return final status
  return sc;
}
