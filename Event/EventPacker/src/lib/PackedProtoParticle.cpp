/*****************************************************************************\
* (c) Copyright 2000-2022 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#include "Event/PackedProtoParticle.h"
#include "Event/CaloChargedInfo_v1.h"
#include "Event/GlobalChargedPID.h"
#include "Event/NeutralPID.h"
#include "Event/PackedEventChecks.h"
#include "fmt/format.h"
#include "fmt/ostream.h"
#include <functional>

#if FMT_VERSION >= 90000
template <>
struct fmt::formatter<LHCb::ProtoParticle::additionalInfo> : ostream_formatter {};
#endif

#include <boost/numeric/conversion/bounds.hpp>
#include <memory>
#include <utility>

/* hash builders used to create a unique identifier for storing underlying PID objects in
 * an anonymous but uniquely identifiable way in the TES for different ProtoParticle containers
 */
template <typename T, typename H>
struct std::hash<KeyedContainer<T, H>> {
  std::size_t operator()( const KeyedContainer<T, H>& container ) const noexcept {
    return std::accumulate( container.begin(), container.end(), std::size_t{0}, []( std::size_t h, const T* pid ) {
      return pid ? ( h ^ std::hash<T>{}( *pid ) ) + 0x9e3779b9 + ( h << 6 ) + ( h >> 2 ) : h;
    } );
  }
};

using namespace LHCb;

//-----------------------------------------------------------------------------

void ProtoParticlePacker::pack( const Data& proto, PackedData& pproto, PackedDataVector& pprotos ) const {
  // packing version
  const auto ver = pprotos.packingVersion();
  if ( !isSupportedVer( ver ) ) return;
  if ( ver == 0 ) throw GaudiException( "Unsupported packing version", __PRETTY_FUNCTION__, StatusCode::FAILURE );

  // save the key
  pproto.key = proto.key();

  if ( proto.track() ) {
    pproto.track = StandardPacker::reference64( &pprotos, proto.track() );
    parent().debug() << "Found a track with parent " << identifier( *proto.track() ) << " and key "
                     << proto.track()->key() << endmsg;
    parent().debug() << "Packed a track with key " << pproto.track << endmsg;
  } else {
    pproto.track = -1;
  }

  if ( proto.richPID() ) {
    pproto.richPID = StandardPacker::reference64( &pprotos, proto.richPID() );
    parent().debug() << "Found a richPID with parent " << identifier( *proto.richPID() ) << " and key "
                     << proto.richPID()->key() << endmsg;
    parent().debug() << "Packed a richPID with key " << pproto.richPID << endmsg;
  } else {
    pproto.richPID = -1;
  }

  if ( proto.caloChargedPID() ) {
    pproto.caloChargedPID = ( StandardPacker::reference64( &pprotos, proto.caloChargedPID() ) );
    parent().debug() << "Found a caloChargedPID with parent " << proto.caloChargedPID()->parent()->name() << " and key "
                     << proto.caloChargedPID()->key() << endmsg;
    parent().debug() << "Packed a caloChargedPID with key " << pproto.caloChargedPID << endmsg;
  } else {
    pproto.caloChargedPID = -1;
  }

  if ( proto.bremInfo() ) {
    pproto.bremInfo = ( StandardPacker::reference64( &pprotos, proto.bremInfo() ) );
    parent().debug() << "Found a bremInfo with parent " << proto.bremInfo()->parent()->name() << " and key "
                     << proto.bremInfo()->key() << endmsg;
    parent().debug() << "Packed a bremInfo with key " << pproto.bremInfo << endmsg;
  } else {
    pproto.bremInfo = -1;
  }

  if ( proto.muonPID() ) {
    pproto.muonPID = StandardPacker::reference64( &pprotos, proto.muonPID() );
    parent().debug() << "Found a muonPID with parent " << identifier( *proto.muonPID() ) << " and key "
                     << proto.muonPID()->key() << endmsg;
    parent().debug() << "Packed a muonPID with key " << pproto.muonPID << endmsg;
  } else {
    pproto.muonPID = -1;
  }

  if ( proto.globalChargedPID() ) {
    pproto.globalChargedPID = ( StandardPacker::reference64( &pprotos, proto.globalChargedPID() ) );
    parent().debug() << "Found a globalChargedPID with parent " << proto.globalChargedPID()->parent()->name()
                     << " and key " << proto.globalChargedPID()->key() << endmsg;
    parent().debug() << "Packed a globalChargedPID with key " << pproto.globalChargedPID << endmsg;
  } else {
    pproto.globalChargedPID = -1;
  }

  if ( proto.neutralPID() ) {
    pproto.neutralPID = ( StandardPacker::reference64( &pprotos, proto.neutralPID() ) );
    parent().debug() << "Found a neutralPID with parent " << proto.neutralPID()->parent()->name() << " and key "
                     << proto.neutralPID()->key() << endmsg;
    parent().debug() << "Packed a neutralPID with key " << pproto.neutralPID << endmsg;
  } else {
    pproto.neutralPID = -1;
  }

  //== Store the CaloHypos
  pproto.firstHypo = pprotos.refs().size();
  for ( const auto& caloH : proto.calo() ) {
    pprotos.refs().push_back( StandardPacker::reference64( &pprotos, caloH ) );
    parent().debug() << "Found a caloHYPO with parent " << identifier( *caloH ) << " and key " << caloH->key()
                     << endmsg;
  }
  pproto.lastHypo = pprotos.refs().size();
  parent().debug() << "Packed first caloHypo with key " << pproto.firstHypo << endmsg;
  parent().debug() << "Packed last caloHypo with key " << pproto.lastHypo << endmsg;
  parent().debug() << "Size of references " << pprotos.refs().size() << endmsg;

  //== Handles the ExtraInfo. Nothing is left in ExtraInfo for neutral protoparticles. Should be removed also for
  // charged eventually.
  pproto.firstExtra = pprotos.extras().size();
  const double max  = std::numeric_limits<float>::max();
  for ( const auto& [key, value] : proto.extraInfo() ) {
    if ( value > max || value < -max ) {
      parent().warning() << fmt::format( "ExtraInfo '{}' out of floating point range. Truncating value.",
                                         (LHCb::ProtoParticle::additionalInfo)key )
                         << endmsg;
    }
    pprotos.extras().emplace_back( key, StandardPacker::fltPacked( value ) );
  }
  pproto.lastExtra = pprotos.extras().size();
}

StatusCode ProtoParticlePacker::unpack( const PackedData& pproto, Data& proto, const PackedDataVector& pprotos,
                                        DataVector& protos, LHCb::Packer::Carry& with_carry, NeutralPIDs& npids,
                                        Event::Calo::v1::CaloChargedPIDs& cpids, Event::Calo::v1::BremInfos& binfos,
                                        GlobalChargedPIDs& gpids ) const {

  // packing version
  const auto ver = pprotos.packingVersion();

  if ( !isSupportedVer( ver ) ) return StatusCode::FAILURE;

  auto unpack_ref = StandardPacker::UnpackRef( &pprotos, &protos, StandardPacker::UnpackRef::Use32{ver == 0} );
  if ( -1 != pproto.track ) {
    if ( auto t = unpack_ref( pproto.track ); t ) {
      proto.setTrack( t );
    } else {
      parent().error() << "Corrupt ProtoParticle Track SmartRef detected." << pproto.track << endmsg;
    }
  }

  if ( -1 != pproto.richPID ) {
    if ( auto p = unpack_ref( pproto.richPID ); p ) {
      proto.setRichPID( p );
    } else {
      parent().error() << "Corrupt ProtoParticle RichPID SmartRef detected." << endmsg;
    }
  }

  auto CellIDFromAdditionalInfo = []( auto v ) {
    double ai = StandardPacker::fltPacked( v );
    auto   id = LHCb::Detector::Calo::CellID( ai >= 0. ? (unsigned)ai : 0u );
    if ( !isValid( id ) ) id = LHCb::Detector::Calo::CellID{};
    return id;
  };

  // if the proto is charged-like, unpack CaloChargedPID and BremInfo. If we are in old version without CaloChargedPID
  // and BremInfo objects, create them from AdditionalInfo
  if ( -1 != pproto.track ) {
    if ( ver < 3 ) {
      // only add relevant object if info is there (based on Ecal/Brem acceptance)
      bool add_pid   = false;
      auto pid       = std::make_unique<Event::Calo::v1::CaloChargedPID>();
      bool add_binfo = false;
      auto binfo     = std::make_unique<Event::Calo::v1::BremInfo>();
      for ( const auto& [k, v] : with_carry( pprotos.extras(), pproto.firstExtra, pproto.lastExtra ) ) {
        switch ( k ) {
        case LHCb::ProtoParticle::InAccEcal:
          add_pid = true;
          pid->setInEcal( StandardPacker::fltPacked( v ) );
          break;
        case LHCb::ProtoParticle::CaloChargedID:
          pid->setClusterID( CellIDFromAdditionalInfo( v ) );
          break;
        case LHCb::ProtoParticle::CaloTrMatch:
          pid->setClusterMatch( StandardPacker::fltPacked( v ) );
          break;
        // missing fill for ElectronID - not in AdditionalInfo
        case LHCb::ProtoParticle::CaloElectronMatch:
          pid->setElectronMatch( StandardPacker::fltPacked( v ) );
          break;
        case LHCb::ProtoParticle::CaloChargedEcal:
          pid->setElectronEnergy( StandardPacker::fltPacked( v ) );
          break;
        case LHCb::ProtoParticle::CaloEoverP:
          pid->setElectronShowerEoP( StandardPacker::fltPacked( v ) );
          break;
        case LHCb::ProtoParticle::ElectronShowerDLL:
          pid->setElectronShowerDLL( StandardPacker::fltPacked( v ) );
          break;
        case LHCb::ProtoParticle::EcalPIDe:
          pid->setEcalPIDe( StandardPacker::fltPacked( v ) );
          break;
        case LHCb::ProtoParticle::EcalPIDmu:
          pid->setEcalPIDmu( StandardPacker::fltPacked( v ) );
          break;
        case LHCb::ProtoParticle::InAccHcal:
          pid->setInHcal( StandardPacker::fltPacked( v ) );
          break;
        case LHCb::ProtoParticle::CaloHcalE:
          pid->setHcalEoP( proto.track() ? StandardPacker::fltPacked( v ) / proto.track()->p() : 0 );
          break;
        case LHCb::ProtoParticle::HcalPIDe:
          pid->setHcalPIDe( StandardPacker::fltPacked( v ) );
          break;
        case LHCb::ProtoParticle::HcalPIDmu:
          pid->setHcalPIDmu( StandardPacker::fltPacked( v ) );
          break;
        // breminfo
        case LHCb::ProtoParticle::InAccBrem:
          add_binfo = true;
          binfo->setInBrem( StandardPacker::fltPacked( v ) );
          break;
        case LHCb::ProtoParticle::CaloBremHypoID:
          binfo->setBremHypoID( CellIDFromAdditionalInfo( v ) );
          break;
        case LHCb::ProtoParticle::CaloBremMatch:
          binfo->setBremHypoMatch( StandardPacker::fltPacked( v ) );
          break;
        case LHCb::ProtoParticle::CaloBremHypoEnergy:
          binfo->setBremHypoEnergy( StandardPacker::fltPacked( v ) );
          break;
        case LHCb::ProtoParticle::CaloBremHypoDeltaX:
          binfo->setBremHypoDeltaX( StandardPacker::fltPacked( v ) );
          break;
        case LHCb::ProtoParticle::CaloBremTBEnergy:
          binfo->setBremTrackBasedEnergy( StandardPacker::fltPacked( v ) );
          break;
        case LHCb::ProtoParticle::CaloBremBendingCorr:
          binfo->setBremBendingCorrection( StandardPacker::fltPacked( v ) );
          break;
        case LHCb::ProtoParticle::CaloHasBrem:
          binfo->setHasBrem( StandardPacker::fltPacked( v ) );
          break;
        case LHCb::ProtoParticle::CaloBremEnergy:
          binfo->setBremEnergy( StandardPacker::fltPacked( v ) );
          break;
        case LHCb::ProtoParticle::BremPIDe:
          binfo->setBremPIDe( StandardPacker::fltPacked( v ) );
          break;
        }
      }
      if ( add_pid ) {
        proto.setCaloChargedPID( pid.get() );
        cpids.insert( pid.release() );
      }
      if ( add_binfo ) {
        proto.setBremInfo( binfo.get() );
        binfos.insert( binfo.release() );
      }
    } else {
      if ( -1 != pproto.caloChargedPID ) {
        if ( auto ccp = unpack_ref( pproto.caloChargedPID ); ccp ) {
          proto.setCaloChargedPID( ccp );
        } else {
          parent().error() << "Corrupt ProtoParticle CaloChargedPID detected." << endmsg;
        }
      }
      if ( -1 != pproto.bremInfo ) {
        if ( auto bi = unpack_ref( pproto.bremInfo ); bi ) {
          proto.setBremInfo( bi );
        } else {
          parent().error() << "Corrupt ProtoParticle BremInfo detected." << endmsg;
        }
      }
    }
  }

  if ( -1 != pproto.muonPID ) {
    if ( auto m = unpack_ref( pproto.muonPID ); m ) {
      proto.setMuonPID( m );
    } else {
      parent().error() << "Corrupt ProtoParticle MuonPID SmartRef detected." << endmsg;
    }
  }

  // if the proto is neutral-like and we are in old version without NeutralPID object, create it from AdditionalInfo
  if ( ( ver < 2 ) && ( -1 == pproto.track ) ) {
    // only add if relevent info there
    bool add_pid = false;
    auto pid     = std::make_unique<NeutralPID>();
    for ( const auto& [k, v] : with_carry( pprotos.extras(), pproto.firstExtra, pproto.lastExtra ) ) {
      switch ( k ) {
      case LHCb::ProtoParticle::CaloTrMatch:
        pid->setCaloTrMatch( StandardPacker::fltPacked( v ) );
        break;
      case LHCb::ProtoParticle::ShowerShape:
        pid->setShowerShape( StandardPacker::fltPacked( v ) );
        break;
      case LHCb::ProtoParticle::ClusterMass:
        pid->setClusterMass( StandardPacker::fltPacked( v ) );
        break;
      case LHCb::ProtoParticle::CaloNeutralEcal:
        add_pid |= true;
        pid->setCaloNeutralEcal( StandardPacker::fltPacked( v ) );
        break;
      case LHCb::ProtoParticle::CaloNeutralHcal2Ecal:
        add_pid |= true;
        pid->setCaloNeutralHcal2Ecal( StandardPacker::fltPacked( v ) );
        break;
      case LHCb::ProtoParticle::CaloNeutralE49:
        add_pid |= true;
        pid->setCaloNeutralE49( StandardPacker::fltPacked( v ) );
        break;
      case LHCb::ProtoParticle::CaloNeutralID:
        add_pid |= true;
        pid->setCaloNeutralID( CellIDFromAdditionalInfo( v ) );
        break;
      case LHCb::ProtoParticle::CaloNeutralE19:
        add_pid |= true;
        pid->setCaloNeutralE19( StandardPacker::fltPacked( v ) );
        break;
      case LHCb::ProtoParticle::CaloClusterCode:
        pid->setCaloClusterCode( StandardPacker::fltPacked( v ) );
        break;
      case LHCb::ProtoParticle::CaloClusterFrac:
        pid->setCaloClusterFrac( StandardPacker::fltPacked( v ) );
        break;
      case LHCb::ProtoParticle::IsPhoton:
        pid->setIsPhoton( StandardPacker::fltPacked( v ) );
        break;
      case LHCb::ProtoParticle::IsNotH:
        pid->setIsNotH( StandardPacker::fltPacked( v ) );
        break;
      case LHCb::ProtoParticle::Saturation:
        pid->setSaturation( StandardPacker::fltPacked( v ) );
        break;
      }
    }
    if ( add_pid ) {
      proto.setNeutralPID( pid.get() );
      npids.insert( pid.release() );
    }
  } else if ( -1 != pproto.neutralPID ) {
    assert( ver != 0 );
    if ( auto np = unpack_ref( pproto.neutralPID ); np ) {
      proto.setNeutralPID( np );
    } else {
      parent().error() << "Corrupt ProtoParticle NeutralPID SmartRef detected." << endmsg;
    }
  }

  // if the proto is charged-like and we are in old version without GlobalChargedPID object, create it from
  // AdditionalInfo
  if ( ( ver < 3 ) && ( -1 != pproto.track ) ) {
    bool add_pid = false;
    auto pid     = std::make_unique<GlobalChargedPID>();
    for ( const auto& [k, v] : with_carry( pprotos.extras(), pproto.firstExtra, pproto.lastExtra ) ) {
      switch ( k ) {
      case LHCb::ProtoParticle::CombDLLe:
        add_pid |= true;
        pid->setCombDLLe( StandardPacker::fltPacked( v ) );
        break;
      case LHCb::ProtoParticle::CombDLLmu:
        add_pid |= true;
        pid->setCombDLLmu( StandardPacker::fltPacked( v ) );
        break;
      case LHCb::ProtoParticle::CombDLLk:
        add_pid |= true;
        pid->setCombDLLk( StandardPacker::fltPacked( v ) );
        break;
      case LHCb::ProtoParticle::CombDLLp:
        add_pid |= true;
        pid->setCombDLLp( StandardPacker::fltPacked( v ) );
        break;
      case LHCb::ProtoParticle::CombDLLd:
        add_pid |= true;
        pid->setCombDLLd( StandardPacker::fltPacked( v ) );
        break;
      case LHCb::ProtoParticle::ProbNNe:
        add_pid |= true;
        pid->setProbNNe( StandardPacker::fltPacked( v ) );
        break;
      case LHCb::ProtoParticle::ProbNNmu:
        add_pid |= true;
        pid->setProbNNmu( StandardPacker::fltPacked( v ) );
        break;
      case LHCb::ProtoParticle::ProbNNpi:
        add_pid |= true;
        pid->setProbNNpi( StandardPacker::fltPacked( v ) );
        break;
      case LHCb::ProtoParticle::ProbNNk:
        add_pid |= true;
        pid->setProbNNk( StandardPacker::fltPacked( v ) );
        break;
      case LHCb::ProtoParticle::ProbNNp:
        add_pid |= true;
        pid->setProbNNp( StandardPacker::fltPacked( v ) );
        break;
      case LHCb::ProtoParticle::ProbNNd:
        add_pid |= true;
        pid->setProbNNd( StandardPacker::fltPacked( v ) );
        break;
      case LHCb::ProtoParticle::ProbNNghost:
        add_pid |= true;
        pid->setProbNNghost( StandardPacker::fltPacked( v ) );
        break;
      }
    }
    if ( add_pid ) {
      proto.setGlobalChargedPID( pid.get() );
      gpids.insert( pid.release() );
    }
  } else if ( -1 != pproto.globalChargedPID ) {
    if ( auto gcp = unpack_ref( pproto.globalChargedPID ); gcp ) {
      proto.setGlobalChargedPID( gcp );
    } else {
      parent().error() << "Corrupt ProtoParticle GlobalChargedPID SmartRef detected." << endmsg;
    }
  }

  for ( auto reference : Packer::subrange( pprotos.refs(), pproto.firstHypo, pproto.lastHypo ) ) {
    if ( auto c = unpack_ref( reference ); c ) {
      proto.addToCalo( c );
    } else {
      parent().error() << "Corrupt ProtoParticle CaloHypo SmartRef detected." << endmsg;
    }
  }

  // Nothing left in extra info for neutral protoparticles. Should be removed also for charged eventually.
  if ( -1 != pproto.track ) {
    for ( const auto& [k, v] : with_carry( pprotos.extras(), pproto.firstExtra, pproto.lastExtra ) ) {
      proto.addInfo( k, StandardPacker::fltPacked( v ) );
    }
  }
  return StatusCode::SUCCESS;
}

StatusCode ProtoParticlePacker::unpack( const PackedDataVector& pprotos, DataVector& protos ) const {
  const auto ver = pprotos.packingVersion();
  protos.reserve( pprotos.data().size() );
  parent().debug() << "version " << (int)pprotos.version() << endmsg;
  parent().debug() << "packing version " << ver << endmsg;

  StatusCode sc = StatusCode::SUCCESS;
  // create a new container in TES for neutralPIDs, caloChargedPIDs and BremInfo if in an old version
  auto npids  = std::make_unique<NeutralPIDs>();
  auto cpids  = std::make_unique<Event::Calo::v1::CaloChargedPIDs>();
  auto binfos = std::make_unique<Event::Calo::v1::BremInfos>();
  auto gpids  = std::make_unique<GlobalChargedPIDs>();

  LHCb::Packer::Carry carry{};
  for ( const auto& pproto : pprotos.data() ) {
    auto part = std::make_unique<LHCb::ProtoParticle>();
    auto sc2  = unpack( pproto, *part, pprotos, protos, carry, *npids, *cpids, *binfos, *gpids );
    protos.insert( part.release(), pproto.key );
    if ( sc.isSuccess() ) sc = sc2;
  }

  // if a new container in TES for neutralPIDs, caloChargedPIDs and BremInfo was created, put it in TES
  if ( ( ver < 2 ) && !npids->empty() ) {
    parent()
        .evtSvc()
        ->registerObject( "/Event/Rec/NeutralPIDs", npids.release() )
        .orThrow( "Error in unpacker putting NeutralPIDs container to TES ", "ProtoParticlePacker" );
  }

  if ( ( ver < 3 ) && !cpids->empty() ) {
    std::string cpids_address = std::string( fmt::format(
        "/Event/Anonymous/CaloChargedPIDs_{}",
        std::hash<KeyedContainer<LHCb::Event::Calo::v1::CaloChargedPID, Containers::HashMap>>{}( *cpids ) ) );
    parent()
        .evtSvc()
        ->registerObject( cpids_address, cpids.release() )
        .orThrow( "Error in unpacker putting CaloChargedPIDs container to TES", "ProtoParticlePacker" );
  }

  if ( ( ver < 3 ) && !binfos->empty() ) {
    std::string binfo_address = std::string(
        fmt::format( "/Event/Anonymous/BremInfo_{}",
                     std::hash<KeyedContainer<LHCb::Event::Calo::v1::BremInfo, Containers::HashMap>>{}( *binfos ) ) );
    parent()
        .evtSvc()
        ->registerObject( binfo_address, binfos.release() )
        .orThrow( "Error in unpacker putting BremInfo container to TES ", "ProtoParticlePacker" );
  }

  // if a new container in TES for GlobalChargedPIDs was created, put it in TES
  if ( ( ver < 3 ) && !gpids->empty() ) {
    std::string gpid_address =
        std::string( fmt::format( "/Event/Anonymous/GlobalChargedPIDs_{}",
                                  std::hash<KeyedContainer<GlobalChargedPID, Containers::HashMap>>{}( *gpids ) ) );
    parent()
        .evtSvc()
        ->registerObject( gpid_address, gpids.release() )
        .orThrow( "Error in unpacker putting GlobalChargedPIDs container to TES ", "ProtoParticlePacker" );
  }

  if ( carry ) {
    parent().warning() << "overflow detected while unpacking protoparticle extrainfo -- I _hope_ the correction for "
                          "this worked properly... good luck!!!"
                       << endmsg;
  }
  return sc;
}

StatusCode ProtoParticlePacker::check( const Data* dataA, const Data* dataB ) const {
  // checker
  const DataPacking::DataChecks ch( parent() );

  bool isOK = true;

  // key
  isOK &= ch.compareInts( "Key", dataA->key(), dataB->key() );

  // check referenced objects
  isOK &= ch.comparePointers( "Track", dataA->track(), dataB->track() );
  isOK &= ch.comparePointers( "RichPID", dataA->richPID(), dataB->richPID() );
  isOK &= ch.comparePointers( "MuonPID", dataA->muonPID(), dataB->muonPID() );
  isOK &= ch.comparePointers( "NeutralPID", dataA->neutralPID(), dataB->neutralPID() );
  isOK &= ch.comparePointers( "CaloChargedPID", dataA->caloChargedPID(), dataB->caloChargedPID() );
  isOK &= ch.comparePointers( "BremInfo", dataA->bremInfo(), dataB->bremInfo() );
  isOK &= ch.comparePointers( "GlobalChargedPID", dataA->globalChargedPID(), dataB->globalChargedPID() );

  // calo hypos
  isOK &= ch.compareInts( "#CaloHypos", dataA->calo().size(), dataB->calo().size() );
  if ( isOK ) {
    for ( auto iC = std::make_pair( dataA->calo().begin(), dataB->calo().begin() );
          iC.first != dataA->calo().end() && iC.second != dataB->calo().end(); ++iC.first, ++iC.second ) {
      isOK &= ch.comparePointers( "CaloHypo", iC.first->target(), iC.second->target() );
    }
  }

  // extra info
  isOK &= ch.compareInts( "#ExtraInfo", dataA->extraInfo().size(), dataB->extraInfo().size() );
  if ( isOK ) {
    for ( auto iE = std::make_pair( dataA->extraInfo().begin(), dataB->extraInfo().begin() );
          iE.first != dataA->extraInfo().end() && iE.second != dataB->extraInfo().end(); ++iE.first, ++iE.second ) {
      isOK &= ch.compareInts( "ExtraInfoKey", iE.first->first, iE.second->first );
      if ( isOK ) {
        if ( ( abs( iE.second->second ) < 1.e-7 && abs( iE.first->second ) > 1.e-7 ) || // both 0 ?
             ( abs( iE.second->second ) > 1.e-7 &&
               1.e-7 < std::abs( ( iE.second->second - iE.first->second ) / iE.second->second ) ) )
          isOK = false;
      }
    }
  }

  // isOK = false; // force false for testing

  if ( !isOK || MSG::DEBUG >= parent().msgLevel() ) {
    parent().info() << "===== ProtoParticle key " << dataA->key() << " Check OK = " << isOK << endmsg;
    parent().info() << format( "Old   track %8x  richPID %8X  muonPID%8x  neutralPID%8x caloChargedPID %8X bremInfo%8X "
                               "globalChargedPID%8X nCaloHypo%4d nExtra%4d",
                               dataA->track(), dataA->richPID(), dataA->muonPID(), dataA->neutralPID(),
                               dataA->caloChargedPID(), dataA->bremInfo(), dataA->globalChargedPID(),
                               dataA->calo().size(), dataA->extraInfo().size() )
                    << endmsg;
    parent().info() << format( "Test  track %8x  richPID %8X  muonPID%8x  neutralPID%8x caloChargedPID %8X bremInfo%8X "
                               "globalChargedPID%8X nCaloHypo%4d nExtra%4d",
                               dataB->track(), dataB->richPID(), dataB->muonPID(), dataB->neutralPID(),
                               dataB->caloChargedPID(), dataB->bremInfo(), dataB->globalChargedPID(),
                               dataB->calo().size(), dataB->extraInfo().size() )
                    << endmsg;
    for ( auto iC = std::make_pair( dataA->calo().begin(), dataB->calo().begin() );
          iC.first != dataA->calo().end() && iC.second != dataB->calo().end(); ++iC.first, ++iC.second ) {
      parent().info() << format( "   old CaloHypo %8x   new %8x", iC.first->target(), iC.second->target() ) << endmsg;
    }
    for ( auto iE = std::make_pair( dataA->extraInfo().begin(), dataB->extraInfo().begin() );
          iE.first != dataA->extraInfo().end() && iE.second != dataB->extraInfo().end(); ++iE.first, ++iE.second ) {
      parent().info() << format( "   old Extra %5d %12.4f     new %5d %12.4f", iE.first->first, iE.first->second,
                                 iE.second->first, iE.second->second )
                      << endmsg;
    }
  }

  return ( isOK ? StatusCode::SUCCESS : StatusCode::FAILURE );
}

namespace LHCb {
  StatusCode unpack( Gaudi::Algorithm const* parent, const ProtoParticlePacker::PackedDataVector& in,
                     ProtoParticlePacker::DataVector& out ) {
    return ProtoParticlePacker{parent}.unpack( in, out );
  }
} // namespace LHCb
