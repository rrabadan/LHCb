/*****************************************************************************\
* (c) Copyright 2000-2022 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "Event/PackedRichPID.h"
#include "Event/PackedEventChecks.h"

using namespace LHCb;

void RichPIDPacker::pack( const Data& pid, PackedData& ppid, PackedDataVector& ppids ) const {
  const auto ver = ppids.packingVersion();
  if ( !isSupportedVer( ver ) ) return;
  if ( ver < 4 ) throw GaudiException( "Unsupported packing version", __PRETTY_FUNCTION__, StatusCode::FAILURE );
  // fill the key
  ppid.key           = pid.key();
  ppid.pidResultCode = (int)pid.pidResultCode();
  ppid.dllEl         = StandardPacker::deltaLL( pid.particleDeltaLL( Rich::Electron ) );
  ppid.dllMu         = StandardPacker::deltaLL( pid.particleDeltaLL( Rich::Muon ) );
  ppid.dllPi         = StandardPacker::deltaLL( pid.particleDeltaLL( Rich::Pion ) );
  ppid.dllKa         = StandardPacker::deltaLL( pid.particleDeltaLL( Rich::Kaon ) );
  ppid.dllPr         = StandardPacker::deltaLL( pid.particleDeltaLL( Rich::Proton ) );
  ppid.dllBt         = StandardPacker::deltaLL( pid.particleDeltaLL( Rich::BelowThreshold ) );
  ppid.dllDe         = StandardPacker::deltaLL( pid.particleDeltaLL( Rich::Deuteron ) );
  if ( pid.track() ) ppid.track = StandardPacker::reference64( &ppids, pid.track() );
}

StatusCode RichPIDPacker::unpack( const PackedData& ppid, Data& pid, const PackedDataVector& ppids,
                                  DataVector& pids ) const {
  const auto ver = ppids.packingVersion();
  if ( !isSupportedVer( ver ) ) return StatusCode::FAILURE; // TODO: define dedicated error code
  pid.setPidResultCode( ppid.pidResultCode );
  pid.setParticleDeltaLL( Rich::Electron, (float)StandardPacker::deltaLL( ppid.dllEl ) );
  pid.setParticleDeltaLL( Rich::Muon, (float)StandardPacker::deltaLL( ppid.dllMu ) );
  pid.setParticleDeltaLL( Rich::Pion, (float)StandardPacker::deltaLL( ppid.dllPi ) );
  pid.setParticleDeltaLL( Rich::Kaon, (float)StandardPacker::deltaLL( ppid.dllKa ) );
  pid.setParticleDeltaLL( Rich::Proton, (float)StandardPacker::deltaLL( ppid.dllPr ) );
  // Below threshold information only available in packed version 1 onwards
  if ( ver > 0 ) pid.setParticleDeltaLL( Rich::BelowThreshold, (float)StandardPacker::deltaLL( ppid.dllBt ) );
  // Deuteron information only available in packed version 4 onwards
  if ( ver > 3 ) pid.setParticleDeltaLL( Rich::Deuteron, (float)StandardPacker::deltaLL( ppid.dllDe ) );
  if ( -1 != ppid.track ) {
    // Smart ref packing changed to 64 bit in packing version 3 onwards
    if ( auto t = StandardPacker::UnpackRef{&ppids, &pids, StandardPacker::UnpackRef::Use32{ver < 3}}( ppid.track );
         t ) {
      pid.setTrack( t );
    } else {
      parent().error() << "Corrupt RichPID Track SmartRef detected." << endmsg;
    }
  }
  // If the packing version is pre-Deuteron, check the BestPID field and correct for
  // the fact the numerical value of the Below Threshold enum changed when Deuteron
  // was added.
  if ( ver < 4 && Rich::Deuteron == pid.bestParticleID() ) { pid.setBestParticleID( Rich::BelowThreshold ); }
  return StatusCode::SUCCESS;
}

StatusCode RichPIDPacker::unpack( const PackedDataVector& ppids, DataVector& pids ) const {
  const auto ver = ppids.packingVersion();
  if ( !isSupportedVer( ver ) ) return StatusCode::FAILURE; // TODO: define dedicated status code
  pids.reserve( ppids.data().size() );
  StatusCode sc = StatusCode::SUCCESS;
  for ( const auto& ppid : ppids.data() ) {
    // make and save new pid in container
    auto* pid = new Data();
    // key information only available in version 2 onwards
    if ( ver > 1 ) {
      pids.insert( pid, ppid.key );
    } else {
      pids.add( pid );
    }
    // Fill data from packed object
    auto sc2 = unpack( ppid, *pid, ppids, pids );
    if ( sc.isSuccess() ) sc = sc2;
  }
  // Check for 'RichFuture' bug where original RichPID version was not
  // properly propagated to the packed data objects
  // Easy to detect if packing version > 3 and data version = 0
  if ( ver > 3 && 0 == pids.version() ) {
    // only warn if the packed location is not empty. This is because its
    // possible for the HLT to create a default container with V0 but packing V4.
    if ( !ppids.data().empty() ) {
      parent().warning() << "Incorrect data version 0 for packing version > 3."
                         << " Correcting data to version 2." << endmsg;
    }
    // reset to V2
    pids.setVersion( 2 );
  }
  return sc;
}

StatusCode RichPIDPacker::check( const Data* dataA, const Data* dataB ) const {
  // checker
  const DataPacking::DataChecks ch( parent() );

  // assume OK from the start
  bool ok = true;
  // key
  ok &= ch.compareInts( "Key", dataA->key(), dataB->key() );
  // History code
  ok &= ch.compareInts( "PIDCode", dataA->pidResultCode(), dataB->pidResultCode() );
  // Track reference
  ok &= ch.comparePointers( "Track", dataA->track(), dataB->track() );
  // DLLs
  ok &= dataA->particleLLValues().size() == dataB->particleLLValues().size();
  auto iLLA( dataA->particleLLValues().begin() );
  auto iLLB( dataB->particleLLValues().begin() );
  for ( ; iLLA != dataA->particleLLValues().end() && iLLB != dataB->particleLLValues().end(); ++iLLA, ++iLLB ) {
    ok &= ch.compareDeltaLLs( "Delta(LL)", *iLLA, *iLLB );
  }

  // force printout for tests
  // ok = false;
  // If comparison not OK, print full information
  if ( !ok ) {
    const std::string loc =
        ( dataA->parent() && dataA->parent()->registry() ? dataA->parent()->registry()->identifier() : "Not in TES" );
    parent().warning() << "Problem with RichPID data packing :-" << endmsg << "  Original PID key=" << dataA->key()
                       << " in '" << loc << "'" << endmsg << dataA << endmsg << "  Unpacked PID" << endmsg << dataB
                       << endmsg;
  } else {
    parent().debug() << "Check successful for PID key " << dataA->key() << endmsg;
  }

  return ( ok ? StatusCode::SUCCESS : StatusCode::FAILURE );
}

namespace LHCb {
  StatusCode unpack( Gaudi::Algorithm const* parent, const RichPIDPacker::PackedDataVector& in,
                     RichPIDPacker::DataVector& out ) {
    return RichPIDPacker{parent}.unpack( in, out );
  }
} // namespace LHCb
