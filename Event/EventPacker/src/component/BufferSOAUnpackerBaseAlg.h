/*****************************************************************************\
* (c) Copyright 2022 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once

#include "Event/PackedDataBuffer.h"
#include "Kernel/IIndexedANNSvc.h"
#include "LHCbAlgs/Transformer.h"
#include "RegistryWrapper.h"

/**
 *  Templated base algorithm for all SOA unpacking algorithms
 *  except relations and MC objects
 **/

namespace DataPacking::Buffer::SOA {

  inline const Gaudi::StringKey PackedObjectLocations{"PackedObjectLocations"};

  template <class PACKER>
  class Unpack final
      : public LHCb::Algorithm::Transformer<PACKER( LHCb::Hlt::PackedData::MappedInBuffers const& buffers )> {

    using Buffer = LHCb::Hlt::PackedData::PackedDataInBuffer;

  public:
    Unpack( const std::string& name, ISvcLocator* pSvcLocator )
        : LHCb::Algorithm::Transformer<PACKER( LHCb::Hlt::PackedData::MappedInBuffers const& )>(
              name, pSvcLocator, {"InputName", "/Event/DAQ/MappedDstData"}, {"OutputName", "/Event/SOA"} ) {}

    PACKER operator()( LHCb::Hlt::PackedData::MappedInBuffers const& buffers ) const override {

      PACKER data;

      const auto& s2i = m_annsvc->s2i( buffers.key(), PackedObjectLocations );
      auto        j   = s2i.find( this->outputLocation() );
      if ( j == s2i.end() ) {
        throw GaudiException{"Could not find entry for requested output", __PRETTY_FUNCTION__, StatusCode::FAILURE};
      }

      const auto* buffer = buffers.find( j->second );

      if ( !buffer || !buffer->buffer().size() ) return data;

      Buffer readBuffer{*buffer};
      readBuffer.init( buffer->buffer(),
                       false ); // TODO: allow for emphemeral 'view' for reading without copying just to update 'pos'

      assert( buffers.key() == readBuffer.key() );

      while ( !readBuffer.eof() ) {
        LHCb::Hlt::PackedData::ObjectHeader header{readBuffer};
        auto const                          nBytesRead =
            this->msgLevel( MSG::DEBUG ) ? readBuffer.load<true>( data ) : readBuffer.load<false>( data );

        if ( nBytesRead != header.storedSize ) {
          this->fatal() << "Loading of object (CLID=" << header.classID << " locationID=" << header.locationID << ") "
                        << " consumed " << nBytesRead << " bytes, "
                        << " but " << header.storedSize << " were stored!" << endmsg;
        }
        if ( this->msgLevel( MSG::DEBUG ) ) {
          this->debug() << "Loading of object (CLID=" << header.classID << " locationID=" << header.locationID << ") "
                        << " consumed " << header.storedSize << " bytes, "
                        << " and " << header.linkLocationIDs.size() << " links were stored!" << endmsg;
        }
      }

      return data;
    }

  private:
    ServiceHandle<IIndexedANNSvc> m_annsvc{this, "ANNSvc", "HltANNSvc", "Service to retrieve DecReport IDs"};
  };
} // namespace DataPacking::Buffer::SOA
