/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#include "Event/MCVertex.h"
#include "Event/PackedMCVertex.h"
#include "Event/StandardPacker.h"

#include "Gaudi/Accumulators.h"
#include "LHCbAlgs/Consumer.h"

#include "boost/numeric/conversion/bounds.hpp"

namespace LHCb {

  /**
   *  Pack the MCVertices
   *
   *  Note that the inheritance from Consumer and the void input are misleading.
   *  The algorithm is reading from and writing to TES, just via a Handles so that
   *  it can deal with non existant input and ignore failures on the output.
   *  Failures are ignored so that the algo behave as the previous, non functional
   *  algo. FIXME we should not ignore failures obviously, but here it's an
   *  "expected" one due to the entanglement of MCVertx and MCParticle packers
   *  (see comment below).
   *
   *  @author Olivier Callot
   *  @date   2005-03-18
   */
  struct PackMCVertex : Algorithm::Consumer<void()> {

    using Consumer::Consumer;
    void operator()() const override;

    DataObjectReadHandle<MCVertices>        m_verts{this, "InputName", MCVertexLocation::Default};
    DataObjectWriteHandle<PackedMCVertices> m_pverts{this, "OutputName", PackedMCVertexLocation::Default};

    mutable Gaudi::Accumulators::StatCounter<> m_packed{
        this,
        "#MCVertices",
    };
    mutable Gaudi::Accumulators::MsgCounter<MSG::WARNING> m_underflow{this, "PackedVertex.tof underflow, set to 0.",
                                                                      10};
    mutable Gaudi::Accumulators::MsgCounter<MSG::WARNING> m_overflow{this,
                                                                     "PackedVertex.tof overflow, set to max float", 10};
  };

} // namespace LHCb

DECLARE_COMPONENT_WITH_ID( LHCb::PackMCVertex, "PackMCVertex" )

void LHCb::PackMCVertex::operator()() const {

  if ( !m_verts.exist() ) return;
  auto const* verts = m_verts.get();

  constexpr double smallest = std::numeric_limits<float>::min();
  constexpr double largest  = std::numeric_limits<float>::max();
  constexpr double tiny     = std::numeric_limits<double>::min();

  // NOTE: in case of failure, the failure is considered as "normal" and interpreted
  // as the fact that the MCVertices were already packed as a consequence of the
  // packing of MCParticles and the action of DataOnDemand. Thus the failure is
  // ignored. FIXME !
  try {
    auto* out = m_pverts.put( std::make_unique<PackedMCVertices>() );
    out->mcVerts().reserve( verts->size() );
    for ( const MCVertex* vert : *verts ) {
      auto& newVert = out->mcVerts().emplace_back( PackedMCVertex() );
      newVert.key   = vert->key();
      newVert.x     = StandardPacker::position( vert->position().x() );
      newVert.y     = StandardPacker::position( vert->position().y() );
      newVert.z     = StandardPacker::position( vert->position().z() );

      // Protect crazy vertex times (no need for std::abs, is always positive!)
      if ( vert->time() > 0. && vert->time() < smallest ) {
        ++m_underflow;
        if ( msgLevel( MSG::DEBUG ) ) {
          if ( vert->time() < tiny )
            debug() << "time smaller than " << tiny;
          else
            debug() << "time " << vert->time();
          debug() << " set to zero for vertex " << vert->key() << " of type " << vert->type() << endmsg;
        }
        newVert.tof = 0.;
      } else if ( vert->time() > largest ) {
        ++m_overflow;
        if ( msgLevel( MSG::DEBUG ) )
          debug() << "time " << vert->time() << " set to " << largest << " for vertex " << vert->key() << " of type "
                  << vert->type() << endmsg;
        newVert.tof = (float)largest;
      } else {
        newVert.tof = (float)vert->time(); // What scale ?
      }

      newVert.type   = vert->type();
      newVert.mother = -1;
      if ( vert->mother() ) { newVert.mother = StandardPacker::reference64( out, vert->mother() ); }
      for ( const auto& P : vert->products() ) { newVert.products.push_back( StandardPacker::reference64( out, P ) ); }
    }
    m_packed += out->mcVerts().size();
  } catch ( GaudiException& ) { return; }

  // Clear the registry address of the unpacked container, to prevent reloading
  verts->registry()->setAddress( nullptr );
}
