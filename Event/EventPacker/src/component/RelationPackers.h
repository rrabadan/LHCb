/*****************************************************************************\
* (c) Copyright 2022 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once
#include "ErrorCategory.h"
#include "Event/MCParticle.h"
#include "Event/PackedEventChecks.h"
#include "Event/PackedPartToRelatedInfoRelation.h"
#include "Event/PackedRelations.h"
#include "Event/PackedSharedObjectsContainer.h"
#include "Event/PackerBase.h"
#include "Event/Particle.h"
#include "Event/RecVertex.h"
#include "Event/RelatedInfoMap.h"
#include "Relations/Relation1D.h"
#include "Relations/RelationWeighted1D.h"
#include "expected.h"

#include "GaudiKernel/System.h"

namespace LHCb::Packers {

  namespace details {

    template <typename DataVector>
    using FromContainer = typename std::remove_pointer_t<typename DataVector::From>::Container;
    template <typename DataVector>
    using ToContainer = typename std::remove_pointer_t<typename DataVector::To>::Container;

    template <typename T>
    struct add_const_ {
      using type = std::add_const_t<T>;
    };
    template <typename T>
    struct add_const_<T*> {
      using type = std::add_const_t<T>* const;
    };

    template <typename T>
    using const_value_t = typename add_const_<typename T::value_type>::type;

    class Resolver {
      LinkManager const*                     m_linkMgr = nullptr;
      std::vector<std::pair<size_t, size_t>> m_missing; // for each entry in m_linkMgr, the number of failed lookups...

      auto& missing_key( int i ) {
        if ( m_missing.empty() ) m_missing.resize( m_linkMgr->size() );
        return m_missing.at( i ).first;
      }
      auto& missing_container( int i ) {
        if ( m_missing.empty() ) m_missing.resize( m_linkMgr->size() );
        return m_missing.at( i ).second;
      }

    public:
      Resolver( LinkManager const* p ) : m_linkMgr{p} {
        if ( !m_linkMgr ) throw GaudiException( "no linkmanager???", __func__, StatusCode::FAILURE );
      }

      static int key( std::int64_t i ) { return StandardPacker::indexAndKey64( i ).key; }

      template <typename T>
      tl::expected<typename T::contained_type const*, StatusCode> object( std::int64_t i ) {
        auto [id, key]   = StandardPacker::indexAndKey64( i );
        auto const* link = m_linkMgr->link( id );
        if ( !link ) throw GaudiException( "null link??? ", __func__, StatusCode::FAILURE );
        if ( link->ID() == LinkManager::DirLinkType::INVALID )
          throw GaudiException( "invalid link???", __func__, StatusCode::FAILURE );
        if ( !link->object() ) {
          ++missing_container( id );
          return tl::unexpected{Packers::ErrorCode::CONTAINER_NOT_FOUND};
        }
        auto const* container = dynamic_cast<T const*>( link->object() );
        if ( !container ) return tl::unexpected{Packers::ErrorCode::WRONG_CONTAINER_TYPE}; /// THIS IS WHAT GOES WRONG
        auto const* obj = container->object( key );
        if ( !obj ) {
          ++missing_key( id );
          return tl::unexpected{Packers::ErrorCode::KEY_NOT_FOUND};
        }
        return obj;
      }

      std::string path( std::int64_t i ) const {
        auto [id, key]   = StandardPacker::indexAndKey64( i );
        auto const* link = m_linkMgr->link( id );
        if ( !link ) throw GaudiException( "null link??? ", __func__, StatusCode::FAILURE );
        if ( link->ID() == LinkManager::DirLinkType::INVALID )
          throw GaudiException( "invalid link???", __func__, StatusCode::FAILURE );
        return link->path();
      }

      bool       hasErrors() const { return !m_missing.empty(); }
      MsgStream& dump( MsgStream& msg ) const {
        if ( !hasErrors() ) return msg;
        for ( const auto& [i, m] : LHCb::range::enumerate( m_missing, 0L ) ) {
          if ( m.second )
            msg << "failed to fetch container from " << m_linkMgr->link( i )->path() << " - requested " << m.second
                << " times " << endmsg;
        }
        for ( const auto& [i, m] : LHCb::range::enumerate( m_missing, 0L ) ) {
          if ( m.first )
            msg << "failed to fetch " << m.first << " keys from " << m_linkMgr->link( i )->path() << endmsg;
        }
        return msg;
      }
    };

    // Relations between two containers
    // Allow the caller to specify the target containers -- if not specified,
    // then derive them from RELATION
    template <typename From_ = void, typename To_ = void, typename RELATION, typename PRELATION, typename ALGORITHM>
    StatusCode unpack2d( RELATION& rels, const PRELATION& prels, ALGORITHM const& parent ) {

      using From = std::conditional_t<std::is_void_v<From_>, FromContainer<RELATION>, From_>;
      using To   = std::conditional_t<std::is_void_v<To_>, ToContainer<RELATION>, To_>;

      auto resolver = details::Resolver{prels.linkMgr()};
      for ( const auto& prel : prels.data() ) {
        for ( int kk = prel.start; kk != prel.end; ++kk ) {

          const auto& src = prels.sources()[kk];
          const auto& dst = prels.dests()[kk];

          const auto f = resolver.object<From>( src );
          if ( !f ) {
            if ( f.error() == StatusCode{ErrorCode::WRONG_CONTAINER_TYPE} )
              return f.error(); // abort: target is there, but not as expected -- let layer above retry (if possible)
                                // with different type
            if ( parent.msgLevel( MSG::DEBUG ) )
              parent.debug() << f.error() << " while retrieving From object with key " << resolver.key( src )
                             << " from " << resolver.path( src ) << endmsg;
          }

          const auto t = resolver.object<To>( dst );
          if ( !t ) {
            if ( t.error() == StatusCode{ErrorCode::WRONG_CONTAINER_TYPE} )
              return t.error(); // abort: target is there, but not as expected -- let layer above retry (if possible)
                                // with different type
            if ( parent.msgLevel( MSG::DEBUG ) )
              parent.debug() << t.error() << " while retrieving To object with key " << resolver.key( dst ) << " from "
                             << resolver.path( dst ) << endmsg;
          }

          if ( !f || !t ) continue;

          // only weighted relations have weights
          StatusCode sc;
          if constexpr ( std::is_same_v<PRELATION, LHCb::PackedWeightedRelations> ) {
            sc = rels.relate( f.value(), t.value(), prels.weights()[kk] );
          } else {
            sc = rels.relate( f.value(), t.value() );
          }
          if ( !sc ) {
            parent.warning() << "Something went wrong with relation unpacking "
                             << " sourceKey " << resolver.key( src ) << " sourceLink " << resolver.path( src )
                             << " destKey " << resolver.key( dst ) << " destLink " << resolver.path( dst ) << endmsg;
          }
          if ( parent.msgLevel( MSG::DEBUG ) ) {
            parent.debug() << "Relation build between "
                           << " sourceKey " << resolver.key( src ) << " sourceLink " << resolver.path( src )
                           << " destKey " << resolver.key( dst ) << " destLink " << resolver.path( dst )
                           << " rels size " << rels.relations().size() << endmsg;
          }
        }
      }
      if ( resolver.hasErrors() ) resolver.dump( parent.warning() );
      rels.i_sort();
      return StatusCode::SUCCESS; // TODO: pick up any errors from resolver!!!!
    }

  } // namespace details

  template <typename To,
            typename = std::enable_if_t<std::is_same_v<To, LHCb::RecVertex> || std::is_same_v<To, LHCb::VertexBase> ||
                                        std::is_same_v<To, LHCb::MCParticle>>>
  struct ParticleRelation : public PackerBase {

    using PackerBase::PackerBase;
    using PackedDataVector = LHCb::PackedRelations;
    using DataVector       = LHCb::Relation1D<LHCb::Particle, To>;

    constexpr inline static auto propertyName() {
      if constexpr ( std::is_same_v<To, LHCb::RecVertex> ) return "P2PVRelations";
      if constexpr ( std::is_same_v<To, LHCb::VertexBase> ) return "P2VRelations";
      if constexpr ( std::is_same_v<To, LHCb::MCParticle> ) return "P2MCPRelations";
    }

    StatusCode unpack( const PackedDataVector& pdata, DataVector& data ) const {
      auto sc = details::unpack2d( data, pdata, parent() );
      if constexpr ( std::is_same_v<To, VertexBase> ) {
        // if To == VertexBase, then the target container could be RecVertex::Container
        // instead of VertexBase::Container. So retry in that specific case
        if ( sc == StatusCode{ErrorCode::WRONG_CONTAINER_TYPE} ) {
          sc = details::unpack2d<Particle::Container, RecVertex::Container>( data, pdata, parent() );
        }
      }
      return sc;
    }

    template <typename Range>
    StatusCode check( const Range& dataA, const DataVector& dataB ) const {

      // assume OK from the start
      bool ok = true;

      // checker
      const DataPacking::DataChecks ch( parent() );

      // checks here
      const bool relSizeOK = dataA.relations().size() == dataB.relations().size();
      ok &= relSizeOK;

      if ( relSizeOK ) {
        auto iDA = dataA.relations().begin();
        auto iDB = dataB.relations().begin();
        for ( ; iDA != dataA.relations().end() && iDB != dataB.relations().end(); ++iDA, ++iDB ) {
          bool entryOK = true;
          entryOK &= ch.comparePointers( "Relations From ", iDA->from(), iDB->from() );
          entryOK &= ch.comparePointers( "Relations To ", iDA->to(), iDB->to() );
          ok &= entryOK;
          if ( !entryOK ) {
            const std::string from_loc = ( iDA->from()->parent() && iDA->from()->parent()->registry()
                                               ? iDA->from()->parent()->registry()->identifier()
                                               : "Not in TES" );
            const std::string to_loc =
                ( iDA->to()->parent() && iDA->to()->parent()->registry() ? iDA->to()->parent()->registry()->identifier()
                                                                         : "Not in TES" );

            parent().warning() << "Problem with Relation data packing :-" << endmsg << " from '" << from_loc << "'"
                               << " to '" << to_loc << "'" << endmsg << dataA << endmsg << "  Unpacked Relation"
                               << endmsg << dataB << endmsg;
          }
        }
      } else {
        parent().warning() << "Relations different sizes: " << dataA.relations().size() << " vs. "
                           << dataB.relations().size() << endmsg;
      }

      return ( ok ? StatusCode::SUCCESS : StatusCode::FAILURE );
    }

    void pack( const DataVector& rels, PackedDataVector& prels ) const {

      // Make a new packed data object and save
      auto& prel = prels.data().emplace_back();

      // reference to original container
      prel.container = StandardPacker::reference64( &prels, &rels, 0 );

      // First object
      prel.start = prels.sources().size();

      // reserve size
      const auto newSize = prels.sources().size() + rels.relations().size();
      prels.sources().reserve( newSize );
      prels.dests().reserve( newSize );

      // Loop over relations
      for ( const auto& R : rels.relations() ) {
        prels.sources().push_back( StandardPacker::reference64( &prels, R.from() ) );
        prels.dests().push_back( StandardPacker::reference64( &prels, R.to() ) );
      }

      // last object
      prel.end = prels.sources().size();
    }
  };

  // pack P2IntRELATION
  struct P2IntRelation : public PackerBase {
    using PackerBase::PackerBase;
    using PackedDataVector = LHCb::PackedRelations;
    using DataVector       = LHCb::Relation1D<LHCb::Particle, int>;
    constexpr inline static auto propertyName() { return "P2IntRelations"; }

    StatusCode unpack( const PackedDataVector& prels, DataVector& rels ) const;

    template <typename Range>
    StatusCode check( const Range& dataA, const DataVector& dataB ) const {
      // assume OK from the start
      bool ok = true;

      // checker
      const DataPacking::DataChecks ch( parent() );

      // checks here
      const bool relSizeOK = dataA.relations().size() == dataB.relations().size();
      ok &= relSizeOK;

      if ( relSizeOK ) {
        auto iDA = dataA.relations().begin();
        auto iDB = dataB.relations().begin();
        for ( ; iDA != dataA.relations().end() && iDB != dataB.relations().end(); ++iDA, ++iDB ) {
          bool entryOK = true;
          entryOK &= ch.comparePointers( "Relations From ", iDA->from(), iDB->from() );
          entryOK &= ch.compareInts( "Relations To ", iDA->to(), iDB->to() );
          ok &= entryOK;
          if ( !entryOK ) {
            const std::string from_loc = ( iDA->from()->parent() && iDA->from()->parent()->registry()
                                               ? iDA->from()->parent()->registry()->identifier()
                                               : "Not in TES" );

            parent().warning() << "Problem with Relation data packing :-" << endmsg << " from '" << from_loc << "'"
                               << " to '" << iDA->to() << "'" << endmsg << dataA << endmsg << "  Unpacked Relation"
                               << endmsg << dataB << endmsg;
          }
        }
      } else {
        parent().warning() << "Relations different sizes: " << dataA.relations().size() << " vs. "
                           << dataB.relations().size() << endmsg;
      }

      return ( ok ? StatusCode::SUCCESS : StatusCode::FAILURE );
    }

    void pack( const DataVector& rels, PackedDataVector& prels ) const;
  };

  // Pack Proto particle 2 MC particle Relation
  template <typename From, typename To, typename Weight = double>
  struct WeightedRelation : public PackerBase {
    using PackerBase::PackerBase;
    using DataVector       = LHCb::RelationWeighted1D<From, To, Weight>;
    using PackedDataVector = LHCb::PackedWeightedRelations;
    constexpr inline static auto propertyName() {
      if constexpr ( std::is_same_v<From, LHCb::ProtoParticle> && std::is_same_v<To, LHCb::MCParticle> ) {
        return "PP2MCPRelations";
      }
    }

    StatusCode unpack( const PackedDataVector& pdata, DataVector& data ) const {
      return details::unpack2d( data, pdata, parent() );
    }

    template <typename Range>
    StatusCode check( const Range& dataA, const DataVector& dataB ) const {
      // assume OK from the start
      bool ok = true;

      // checker
      const DataPacking::DataChecks ch( parent() );

      // checks here
      const bool relSizeOK = dataA.relations().size() == dataB.relations().size();
      ok &= relSizeOK;

      if ( relSizeOK ) {
        auto iDA = dataA.relations().begin();
        auto iDB = dataB.relations().begin();
        for ( ; iDA != dataA.relations().end() && iDB != dataB.relations().end(); ++iDA, ++iDB ) {
          bool entryOK = true;
          entryOK &= ch.comparePointers( "Relations From ", iDA->from(), iDB->from() );
          entryOK &= ch.comparePointers( "Relations To ", iDA->to(), iDB->to() );
          entryOK &= ch.compareDoubles( "Relations Weight ", iDA->weight(), iDB->weight() );

          ok &= entryOK;
          if ( !entryOK ) {
            const std::string from_loc = ( iDA->from()->parent() && iDA->from()->parent()->registry()
                                               ? iDA->from()->parent()->registry()->identifier()
                                               : "Not in TES" );

            const std::string to_loc =
                ( iDA->to()->parent() && iDA->to()->parent()->registry() ? iDA->to()->parent()->registry()->identifier()
                                                                         : "Not in TES" );

            parent().warning() << "Problem with Relation data packing :-" << endmsg << " from '" << from_loc << "' to '"
                               << to_loc << "'" << endmsg;
          }
        }
      } else {
        parent().warning() << "Relations different sizes: " << dataA.relations().size() << " vs. "
                           << dataB.relations().size() << endmsg;
      }

      return ( ok ? StatusCode::SUCCESS : StatusCode::FAILURE );
    }

    template <typename RelationRange>
    void pack( const RelationRange& rels, PackedDataVector& prels ) const {

      // Make a new packed data object and save
      auto& prel = prels.data().emplace_back();

      // reference to original container
      prel.container = StandardPacker::reference64( &prels, identifier( rels ), 0 );

      // First object
      prel.start = prels.sources().size();

      // reserve size
      const auto newSize = prels.sources().size() + rels.relations().size();
      prels.sources().reserve( newSize );
      prels.dests().reserve( newSize );
      prels.weights().reserve( newSize );

      // Loop over relations
      for ( const auto& R : rels.relations() ) {
        prels.sources().emplace_back( StandardPacker::reference64( &prels, R.from() ) );
        prels.dests().emplace_back( StandardPacker::reference64( &prels, R.to() ) );
        prels.weights().emplace_back( R.weight() );
      }

      // last object
      prel.end = prels.sources().size();
    }
  };

  using PP2MCPRelation = WeightedRelation<LHCb::ProtoParticle, LHCb::MCParticle, double>;

  // particle 2 info relations
  class P2InfoRelation : public PackerBase {
    const LHCb::RelatedInfoRelationsPacker m_rInfoPacker;

  public:
    /// Related Info Packer
    P2InfoRelation( Gaudi::Algorithm const* p ) : PackerBase( p ), m_rInfoPacker( p ) {}

    using DataVector       = LHCb::Relation1D<LHCb::Particle, LHCb::RelatedInfoMap>;
    using PackedDataVector = LHCb::PackedRelatedInfoRelations;
    constexpr inline static auto propertyName() { return "P2InfoRelations"; }

    StatusCode unpack( const PackedDataVector& prels, DataVector& rels ) const;

    template <typename Range>
    StatusCode check( const Range& dataA, const DataVector& dataB ) const {
      // assume OK from the start
      bool ok = true;

      // checker
      const DataPacking::DataChecks ch( parent() );

      // checks here
      const bool relSizeOK = dataA.relations().size() == dataB.relations().size();
      ok &= relSizeOK;

      if ( relSizeOK ) {
        auto iDA = dataA.relations().begin();
        auto iDB = dataB.relations().begin();
        for ( ; iDA != dataA.relations().end() && iDB != dataB.relations().end(); ++iDA, ++iDB ) {
          bool entryOK = true;
          entryOK &= ch.comparePointers( "Relations From ", iDA->from(), iDB->from() );

          // TODO add a check for related info
          ok &= entryOK;
          if ( !entryOK ) {
            const std::string from_loc = ( iDA->from()->parent() && iDA->from()->parent()->registry()
                                               ? iDA->from()->parent()->registry()->identifier()
                                               : "Not in TES" );

            parent().warning() << "Problem with Relation data packing :-" << endmsg << " from '" << from_loc << "'"
                               << endmsg << dataA << endmsg << "  Unpacked Relation" << endmsg << dataB << endmsg;
          }
        }
      } else {
        parent().warning() << "Relations different sizes: " << dataA.relations().size() << " vs. "
                           << dataB.relations().size() << endmsg;
      }

      return ( ok ? StatusCode::SUCCESS : StatusCode::FAILURE );
    }

    void pack( const DataVector& rels, PackedDataVector& prels ) const;
  };

  template <typename ValueType>
  class SharedObjectsContainer : public PackerBase {
  public:
    using PackedDataVector = PackedSharedObjectsContainer<ValueType>;
    using DataVector       = ::SharedObjectsContainer<ValueType>;

    SharedObjectsContainer( Gaudi::Algorithm const* p ) : PackerBase( p ) {}

    StatusCode unpack( const PackedDataVector& src, DataVector& tgt ) const {
      auto resolver = details::Resolver{src.linkMgr()};
      for ( const auto& i : src ) {
        auto f = resolver.object<typename ValueType::Container>( i );
        if ( !f ) {
          if ( f.error() == StatusCode{ErrorCode::WRONG_CONTAINER_TYPE} )
            return f.error(); // abort: target is there, but not as expected -- let layer above retry (if possible) with
                              // different type
          parent().debug() << f.error() << " while retrieving object with key " << resolver.key( i ) << " from "
                           << resolver.path( i ) << endmsg;
          continue;
        }
        tgt.insert( f.value() );
      }
      if ( resolver.hasErrors() ) resolver.dump( parent().warning() );
      // TODO: Propage resolver error!!!
      return StatusCode::SUCCESS;
    }

    template <typename Range>
    StatusCode check( const Range& dataA, const DataVector& dataB ) const {
      // TODO: check that things point at the same elements
      if ( dataA.size() != dataB.size() ) return StatusCode::FAILURE;
      return std::inner_product(
          dataA.begin(), dataA.end(), dataB.begin(), StatusCode{StatusCode::SUCCESS},
          []( StatusCode sc1, StatusCode sc2 ) { return sc1.isFailure() ? sc1 : sc2; },
          [ch = DataPacking::DataChecks{parent()}]( const auto& lhs, const auto& rhs ) -> StatusCode {
            return ch.comparePointers( "SharedObjectContainer elements", lhs, rhs );
          } );
    }

    void pack( const DataVector& in, PackedDataVector& out ) const {
      out.reserve( out.size() + in.size() );
      StandardPacker::reference64( &out, &in, 0 ); // stupid hack to avoid references which use 'linkID 0' (by adding a
                                                   // harmless/useless first entry), as a linkID of 0 is
                                                   // indistinguishable from an INVALID link...
      for ( const auto& i : in ) out.emplace_back( i );
    }
  };

} // namespace LHCb::Packers

namespace LHCb {

  // known unpacking transformations -- need to return StatusCode here!!!
  StatusCode unpack( Gaudi::Algorithm const*, PackedWeightedRelations const&,
                     RelationWeighted1D<ProtoParticle, MCParticle, double>& );
  StatusCode unpack( Gaudi::Algorithm const*, PackedRelatedInfoRelations const&,
                     Relation1D<Particle, RelatedInfoMap>& );
  StatusCode unpack( Gaudi::Algorithm const*, PackedRelations const&, Relation1D<Particle, VertexBase>& );
  StatusCode unpack( Gaudi::Algorithm const*, PackedRelations const&, Relation1D<Particle, RecVertex>& );
  StatusCode unpack( Gaudi::Algorithm const*, PackedRelations const&, Relation1D<Particle, MCParticle>& );
  StatusCode unpack( Gaudi::Algorithm const*, PackedRelations const&, Relation1D<Particle, int>& );

  template <typename T>
  StatusCode unpack( Gaudi::Algorithm const* parent, PackedSharedObjectsContainer<T> const& in,
                     ::SharedObjectsContainer<T>& out ) {
    return Packers::SharedObjectsContainer<T>{parent}.unpack( in, out );
  }

} // namespace LHCb
