/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#include "Event/PackedRecVertex.h"
#include "Event/RecVertex.h"
#include "Event/StandardPacker.h"

#include "Gaudi/Accumulators.h"
#include "LHCbAlgs/Consumer.h"

namespace LHCb::Obsolete {

  /**
   *  Unpack a PackedRecVertex container to RecVertices.
   *
   *  Note that the inheritance from Consumer is misleading but the algorithm is
   *  writing to TES, just via a Handle so that it can do it at the begining of
   *  the operator(), as cross pointers are used in the TES and requires this.
   *
   *  @author Olivier Callot
   *  @date   2008-11-17
   */
  struct UnpackRecVertex : Algorithm::Consumer<void( PackedRecVertices const& )> {

    UnpackRecVertex( std::string const& name, ISvcLocator* pSvcLocator )
        : Consumer( name, pSvcLocator, KeyValue{"InputName", PackedRecVertexLocation::Primary} ) {}

    void operator()( LHCb::PackedRecVertices const& dst ) const override {
      if ( (int)dst.version() < 2 ) {
        // We can't unpack the weights vector because it lives in a seperate
        // location for < v2 packed vertices; this means fully supporting all
        // vertices would require an 'optional' data dependency which the Gaudi
        // HiveDataBroker doesn't support
        ++m_oldVersion;
      }

      if ( msgLevel( MSG::DEBUG ) ) debug() << "Size of PackedRecVertices = " << dst.data().size() << endmsg;

      auto*                 newRecVertices = m_output.put( std::make_unique<RecVertices>() );
      const RecVertexPacker rvPacker( this );
      for ( const auto& src : dst.data() ) {
        auto* vert = new RecVertex();
        newRecVertices->insert( vert, src.key );
        rvPacker.unpack( src, *vert, dst, *newRecVertices ).ignore();
      }
    }

    DataObjectWriteHandle<RecVertices> m_output{this, "OutputName", RecVertexLocation::Primary};

    mutable Gaudi::Accumulators::MsgCounter<MSG::WARNING> m_oldVersion{
        this, "PackedRecVertex version < 2 not fully supported; associated weights vector will not be unpacked", 10};
  };

  DECLARE_COMPONENT_WITH_ID( UnpackRecVertex, "UnpackRecVertex" )

} // namespace LHCb::Obsolete
