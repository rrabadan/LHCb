/*****************************************************************************\
* (c) Copyright 2024 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once

#include "Event/PackerBase.h"
#include "StandardPacker.h"

namespace LHCb {

  struct PackedFTLiteCluster {
    std::uint32_t id{0};

#ifndef __CLING__
    template <typename Buf>
    void save( Buf& buf ) const {
      Packer::io( buf, *this );
    }

    template <typename Buf>
    void load( Buf& buf, unsigned int /* version */ ) {
      Packer::io( buf, *this );
    }
#endif
  };
} // namespace LHCb