/*****************************************************************************\
* (c) Copyright 2000-2022 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once

#include "Event/PackedEventChecks.h"
#include "Event/PackerBase.h"
#include "Event/StandardPacker.h"
#include "Event/Track.h"

#include "Event/FTLiteCluster.h"
#include "Event/UTHitCluster.h"
#include "Event/VPMicroCluster.h"

#include "PackedFTLiteCluster.h"
#include "PackedUTCluster.h"
#include "PackedVPMicroCluster.h"

#include "GaudiKernel/DataObject.h"
#include "GaudiKernel/StatusCode.h"

#include "fmt/format.h"

#include <array>
#include <cstdint>
#include <limits>
#include <string>
#include <vector>

namespace LHCb {

  /**
   *  Packed description of a track
   *
   *  Version 2: Added new data members to PackedTrack to follow the upgraded Track.
   *
   *  @author Olivier Callot
   *  @date   2009-08-26
   */
  struct PackedTrack {

    std::int64_t  key{0};
    std::int32_t  chi2PerDoF{0};
    std::int32_t  nDoF{0};
    std::uint32_t flags{0};

    std::int32_t firstId{0};
    std::int32_t lastId{0};

    std::int32_t firstState{0};
    std::int32_t lastState{0};

    std::int32_t firstExtra{0};
    std::int32_t lastExtra{0};

    //== Added for version 3, August 2009
    std::int32_t likelihood{0};
    std::int32_t ghostProba{0};

    //== Added for version 7, November 2023
    std::int32_t firstVPCluster{0};
    std::int32_t lastVPCluster{0};

    std::int32_t firstUTCluster{0};
    std::int32_t lastUTCluster{0};

    std::int32_t firstFTCluster{0};
    std::int32_t lastFTCluster{0};

    //== Note that Nodes and Measurements on Track are transient only, and thus never stored.

#ifndef __CLING__
    template <typename T>
    void save( T& buf ) const {
      Packer::io( buf, *this );
    }

    template <typename T>
    void load( T& buf, unsigned int version ) {
      if ( version == 4 ) {
        buf.io( key, chi2PerDoF, nDoF, flags );
        buf.template io<uint16_t>( firstId );
        buf.template io<uint16_t>( lastId );
        buf.template io<uint16_t>( firstState );
        buf.template io<uint16_t>( lastState );
        buf.template io<uint16_t>( firstExtra );
        buf.template io<uint16_t>( lastExtra );
        buf.io( likelihood, ghostProba );
      } else if ( version < 7 ) {
        buf.io( key, chi2PerDoF, nDoF, flags );
        buf.io( firstId, lastId, firstState, lastState );
        buf.io( firstExtra, lastExtra, likelihood, ghostProba );
      } else {
        Packer::io( buf, *this );
      }
    }
#endif
  };

  /**
   *  Describe a packed state
   *
   *  @author Olivier Callot
   *  @date   2008-11-07
   */
  struct PackedState {

    std::int32_t flags{0};

    std::int32_t x{0};
    std::int32_t y{0};
    std::int32_t z{0};
    std::int32_t tx{0};
    std::int32_t ty{0};
    std::int32_t p{0};

    std::int32_t cov_00{0};
    std::int32_t cov_11{0};
    std::int32_t cov_22{0};
    std::int32_t cov_33{0};
    std::int32_t cov_44{0};
    std::int16_t cov_10{0};
    std::int16_t cov_20{0};
    std::int16_t cov_21{0};
    std::int16_t cov_30{0};
    std::int16_t cov_31{0};
    std::int16_t cov_32{0};
    std::int16_t cov_40{0};
    std::int16_t cov_41{0};
    std::int16_t cov_42{0};
    std::int16_t cov_43{0};

#ifndef __CLING__
    template <typename T>
    void save( T& buf ) const {
      Packer::io( buf, *this );
    }

    template <typename T>
    void load( T& buf, unsigned int /*version*/ ) {
      Packer::io( buf, *this ); // identical operation until version is incremented
    }
#endif
  };

  constexpr CLID CLID_PackedTracks = 1550;

  // Namespace for locations in TDS
  namespace PackedTrackLocation {
    inline const std::string Default  = "pRec/Track/Best";
    inline const std::string Muon     = "pRec/Track/Muon";
    inline const std::string InStream = "/pRec/Track/Custom";
  } // namespace PackedTrackLocation

  /**
   *  Container of packed Tracks
   *
   *  @author Olivier Callot
   *  @date   2009-08-26
   */

  class PackedTracks : public DataObject {

  public:
    /// Standard constructor
    PackedTracks() {
      m_vect.reserve( 500 );
      m_state.reserve( 2000 );
      m_ids.reserve( 2000 );
      m_extra.reserve( 5000 );
      m_vpClusters.reserve( 1000 );
      m_utClusters.reserve( 200 );
      m_ftClusters.reserve( 1000 );
    }

    const CLID&        clID() const override { return PackedTracks::classID(); }
    static const CLID& classID() { return CLID_PackedTracks; }

    std::vector<PackedTrack>&       data() { return m_vect; }
    const std::vector<PackedTrack>& data() const { return m_vect; }

    std::vector<std::int32_t>&       ids() { return m_ids; }
    const std::vector<std::int32_t>& ids() const { return m_ids; }

    std::vector<PackedState>&       states() { return m_state; }
    const std::vector<PackedState>& states() const { return m_state; }

    std::vector<std::pair<std::int32_t, std::int32_t>>&       extras() { return m_extra; }
    const std::vector<std::pair<std::int32_t, std::int32_t>>& extras() const { return m_extra; }

    std::vector<PackedVPMicroCluster>&       vpClusters() { return m_vpClusters; }
    const std::vector<PackedVPMicroCluster>& vpClusters() const { return m_vpClusters; }

    std::vector<PackedUTCluster>&       utClusters() { return m_utClusters; }
    const std::vector<PackedUTCluster>& utClusters() const { return m_utClusters; }

    std::vector<PackedFTLiteCluster>&       ftClusters() { return m_ftClusters; }
    const std::vector<PackedFTLiteCluster>& ftClusters() const { return m_ftClusters; }

    /// Default Packing Version
    static std::int8_t defaultPackingVersion() { return 7; }

    /// Access the packing version
    [[nodiscard]] std::int8_t packingVersion() const { return m_packingVersion; }

    /// Describe serialization of object
    template <typename T>
    void save( T& buf ) const {
      buf.save( static_cast<uint8_t>( packingVersion() ) );
      buf.save( static_cast<uint8_t>( version() ) );
      buf.save( m_vect );
      buf.save( m_state );
      buf.save( m_ids );
      buf.save( m_extra );
      if ( packingVersion() > 6 ) {
        buf.save( m_vpClusters );
        buf.save( m_utClusters );
        buf.save( m_ftClusters );
      }
    }

    /// Describe de-serialization of object
    template <typename T>
    void load( T& buf ) {
      m_packingVersion = buf.template load<uint8_t>();
      if ( packingVersion() < 4 || packingVersion() > PackedTracks::defaultPackingVersion() ) {
        throw std::runtime_error( "PackedTracks packing version is not supported: " +
                                  std::to_string( packingVersion() ) );
      }
      // Both packing version and DataObject version only saved to buffer since packing version 6
      const auto ver = ( 6 <= packingVersion() ? buf.template load<uint8_t>() : packingVersion() );
      setVersion( ver );
      buf.load( m_vect, ver );
      buf.load( m_state, ver );
      buf.load( m_ids );
      buf.load( m_extra, ver );

      if ( packingVersion() > 6 ) {
        buf.load( m_vpClusters, ver );
        buf.load( m_utClusters, ver );
        buf.load( m_ftClusters, ver );
      }
    }

    // perform unpacking
    friend StatusCode unpack( Gaudi::Algorithm const*, const PackedTracks&, Tracks& );

  private:
    std::vector<PackedTrack>                           m_vect;
    std::vector<PackedState>                           m_state;
    std::vector<std::int32_t>                          m_ids;
    std::vector<std::pair<std::int32_t, std::int32_t>> m_extra;

    std::vector<PackedVPMicroCluster> m_vpClusters;
    std::vector<PackedUTCluster>      m_utClusters;
    std::vector<PackedFTLiteCluster>  m_ftClusters;

    /// Data packing version
    std::int8_t m_packingVersion{defaultPackingVersion()};
  };

  /**
   *  Utility class to handle the packing and unpacking of Tracks
   *
   *  @author Christopher Rob Jones
   *  @date   05/04/2012
   *
   */
  class TrackPacker : public PackerBase {

  public:
    typedef LHCb::Track          Data;
    typedef LHCb::PackedTrack    PackedData;
    typedef LHCb::Tracks         DataVector;
    typedef LHCb::PackedTracks   PackedDataVector;
    static const std::string&    packedLocation() { return LHCb::PackedTrackLocation::Default; }
    static const std::string&    unpackedLocation() { return LHCb::TrackLocation::Default; }
    constexpr inline static auto propertyName() { return "Tracks"; }

    using PackerBase::PackerBase;

    /// Pack Tracks
    template <typename TrackRange>
    void pack( const TrackRange& tracks, PackedDataVector& ptracks ) const {
      // check version
      const auto ver = ptracks.packingVersion();
      if ( !isSupportedVer( ver ) ) return;
      ptracks.data().reserve( tracks.size() );
      for ( const Data* track : tracks ) {
        if ( track ) pack( *track, ptracks.data().emplace_back(), ptracks );
      }
    }

    /// Unpack a single Track
    StatusCode unpack( const PackedData& ptrack, Data& track, const PackedDataVector& ptracks,
                       DataVector& tracks ) const;

    /// Unpack Tracks
    StatusCode unpack( const PackedDataVector& ptracks, DataVector& tracks ) const;

    /// Compare two Tracks to check the packing -> unpacking performance
    StatusCode check( const Data* dataA, const Data* dataB ) const;

    auto& setStateExceptions(
        const std::map<LHCb::Event::Enum::Track::Type, std::vector<LHCb::State::Location>>& exceptions ) {
      m_persistedStatesExceptions = exceptions;

      return *this;
    }

    auto& setDefaultPersistedStates( std::vector<LHCb::State::Location>& defaultStates ) {
      m_persistedStatesDefault = defaultStates;

      return *this;
    }

  private:
    /// Pack a Track
    void pack( const Data& track, PackedData& ptrack, PackedDataVector& ptracks ) const;

    /// Convert a state to a packed state
    void convertState( const LHCb::State& state, PackedDataVector& ptracks ) const;

    /// Convert a packed state to a state in a track
    void convertState( const LHCb::PackedState& pSta, LHCb::Track& tra ) const;

    void compareStates( const LHCb::State& oSta, const LHCb::State& tSta ) const;

    /// Add a VPCluster into the packed track collection
    void convertVPMicroCluster( const LHCb::VPMicroCluster&, PackedDataVector& ) const;

    /// Convert a packed VPCluster to a cluster on a track
    void convertVPMicroCluster( const LHCb::PackedVPMicroCluster&, LHCb::Track& ) const;

    /// Add a FTCluster into the packed track collection
    void convertFTLiteCluster( const LHCb::FTLiteCluster&, PackedDataVector& ) const;

    /// Convert a packed FTCluster to a cluster on a track
    void convertFTLiteCluster( const LHCb::PackedFTLiteCluster&, LHCb::Track& ) const;

    /// Add a UTCluster into the packed track collection
    void convertUTCluster( const LHCb::UTHitCluster&, PackedDataVector& ) const;

    /// Convert a packed UTCluster to a cluster on a track
    void convertUTCluster( const LHCb::PackedUTCluster&, LHCb::Track& ) const;

    /// Check if the given packing version is supported
    [[nodiscard]] static bool isSupportedVer( const std::int8_t ver ) {
      const bool OK = ( 0 <= ver && ver <= PackedTracks::defaultPackingVersion() );
      if ( !OK ) {
        throw GaudiException( fmt::format( "Unknown packed data version {}", (int)ver ), "TrackPacker",
                              StatusCode::FAILURE );
      }
      return OK;
    }

  public:
    /// Reset wraping bug counts
    void resetWrappingCounts() const {
      m_firstIdHigh     = 0;
      m_lastIdHigh      = 0;
      m_firstStateHigh  = 0;
      m_lastStateHigh   = 0;
      m_firstExtraHigh  = 0;
      m_lastExtraHigh   = 0;
      m_lastPackedDataV = nullptr;
    }

  private:
    /**
    This set of states is persisted for all tracks, if no exception is made.
    In cases of exception, this set is ignored and the other set is taken instead...
    */
    std::vector<LHCb::State::Location> m_persistedStatesDefault = {LHCb::State::Location::ClosestToBeam,
                                                                   LHCb::State::Location::FirstMeasurement,
                                                                   LHCb::State::Location::LastMeasurement};

    std::map<LHCb::Event::Enum::Track::Type, std::vector<LHCb::State::Location>> m_persistedStatesExceptions;

    // cached data to handle wrapped ID numbers ...
    mutable std::int32_t m_firstIdHigh{0};
    mutable std::int32_t m_lastIdHigh{0};
    mutable std::int32_t m_firstStateHigh{0};
    mutable std::int32_t m_lastStateHigh{0};
    mutable std::int32_t m_firstExtraHigh{0};
    mutable std::int32_t m_lastExtraHigh{0};

    // Cache the pointers to the last packed and unpacked containers
    mutable const PackedDataVector* m_lastPackedDataV = nullptr;
  };

} // namespace LHCb
