/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#pragma once
#include "GaudiKernel/MsgStream.h"
#include <boost/crc.hpp>
#include <boost/pfr/core.hpp>
#include <map>
#include <string>
#include <tuple>
#include <type_traits>
#include <vector>

namespace LHCb::Hlt::PackedData {

  /** @class PackedDataChecksum PackedDataChecksum.h
   *  Helper class that calculates packed object checksums
   *
   *  @author Rosen Matev
   *  @date   2016-01-03
   */
  class PackedDataChecksum {

  private:
    constexpr static const std::string_view global_cksum_key = "_global_";

    struct Adaptor {
      PackedDataChecksum* parent = nullptr;
      std::string_view    key;
      template <typename U>
      void save( const U& arg ) {
        if ( parent ) { parent->process( key, arg ); }
      }
    };

  private:
    /// Access a checksum for a given tag
    [[nodiscard]] auto& cksum_for( const std::string_view k ) {
      auto i = m_result.find( k );
      if ( i == m_result.end() ) { i = m_result.emplace( k, boost::crc_32_type{} ).first; }
      return i->second;
    }

    /// Generate a single global checksum
    [[nodiscard]] auto global_checksum() const {
      boost::crc_32_type global_cksum;
      for ( const auto& x : m_result ) {
        const auto cksum = x.second.checksum();
        global_cksum.process_bytes( &cksum, sizeof( cksum ) );
      }
      return global_cksum.checksum();
    }

    template <typename T>
    void process( boost::crc_32_type& cksum, const T& x ) {
      static_assert( std::is_trivially_copyable_v<T> );
      if constexpr ( std::is_aggregate_v<T> ) {
        boost::pfr::for_each_field( x, [&]( const auto& i ) { this->process( cksum, i ); } );
      } else {
        cksum.process_bytes( &x, sizeof( x ) );
      }
    }

    // Specialization for pairs as they might be padded
    template <typename T1, typename T2>
    void process( boost::crc_32_type& cksum, const std::pair<T1, T2>& x ) {
      process( cksum, std::forward_as_tuple( x.first, x.second ) );
    }

    template <typename... T>
    void process( boost::crc_32_type& cksum, const std::tuple<T...>& x ) {
      std::apply( [&]( const auto&... arg ) { ( process( cksum, arg ), ... ); }, x );
    }

    template <typename T>
    void process( const std::string_view key, const T& x ) {
      process( cksum_for( key ), x );
    }

    template <typename T, typename Allocator>
    void process( const std::string_view key, const std::vector<T, Allocator>& v ) {
      auto& cksum = cksum_for( key );
      for ( const auto& x : v ) { process( cksum, x ); }
    }

  public:
    template <typename T>
    void processObject( const T& x, const std::string_view key ) {
      Adaptor processor{this, key};
      x.save( processor );
    }

    /// Obtain the current value of a checksum
    [[nodiscard]] auto checksum( const std::string_view key = "" ) {
      return ( global_cksum_key == key ? global_checksum() : cksum_for( key ).checksum() );
    }

    /// Obtain the current value of all checksums
    [[nodiscard]] auto checksums() const {
      std::map<std::string, int> result;
      for ( const auto& x : m_result ) { result.emplace( x.first, x.second.checksum() ); }
      result.emplace( global_cksum_key, global_checksum() );
      return result;
    }

    /// Reset the checksums
    void reset() {
      for ( auto& x : m_result ) { x.second.reset( 0 ); }
    }

    friend MsgStream& print( MsgStream& msg, PackedDataChecksum const& cs ) {
      auto prnt = [&msg]( const auto& tag, const auto& cksum ) {
        msg << "Packed data checksum for '" << tag << "' = " << std::hex << cksum << endmsg;
      };
      for ( const auto& x : cs.m_result ) { prnt( x.first, x.second.checksum() ); }
      prnt( global_cksum_key, cs.global_checksum() );
      return msg;
    }

  private:
    // Map of all TES checksums
    std::map<std::string, boost::crc_32_type, std::less<>> m_result;
  };

} // namespace LHCb::Hlt::PackedData
