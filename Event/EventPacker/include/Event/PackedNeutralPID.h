/*****************************************************************************\
* (c) Copyright 2023 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once

#include "Event/NeutralPID.h"
#include "Event/PackerBase.h"
#include "Event/StandardPacker.h"

#include "GaudiKernel/DataObject.h"
#include "GaudiKernel/StatusCode.h"

#include "fmt/format.h"

#include <cstdint>

namespace LHCb {

  /**
   *  Packed NeutralPID
   *
   */
  struct PackedNeutralPID {
    std::int32_t CaloNeutralID{0};
    std::int32_t ClusterMass{0};
    std::int32_t CaloNeutralEcal{0};
    std::int32_t CaloTrMatch{0};
    std::int32_t ShowerShape{0};
    std::int32_t CaloNeutralHcal2Ecal{0};
    std::int32_t CaloNeutralE19{0};
    std::int32_t CaloNeutralE49{0};
    std::int32_t CaloClusterCode{0};
    std::int32_t CaloClusterFrac{0};
    std::int32_t Saturation{0};
    std::int32_t IsNotH{0};
    std::int32_t IsPhoton{0};
    std::int64_t key{-1};

#ifndef __CLING__
    template <typename Buf>
    void save( Buf& buf ) const {
      Packer::io( buf, *this );
    }

    template <typename Buf>
    void load( Buf& buf, unsigned int /*version*/ ) {
      Packer::io( buf, *this );
    }
#endif
  };

  constexpr CLID CLID_PackedNeutralPIDs = 1570;

  /// Namespace for locations in TDS
  namespace PackedNeutralPIDLocation {
    inline const std::string Default  = "";
    inline const std::string InStream = "";
  } // namespace PackedNeutralPIDLocation

  /**
   *  Packed NeutralPIDs
   *
   */
  class PackedNeutralPIDs : public DataObject {

  public:
    /// Vector of packed objects
    typedef std::vector<LHCb::PackedNeutralPID> Vector;

    /// Default Packing Version
    static char defaultPackingVersion() { return 0; }

    /// Class ID
    static const CLID& classID() { return CLID_PackedNeutralPIDs; }

    /// Class ID
    const CLID& clID() const override { return PackedNeutralPIDs::classID(); }

    /// Write access to the data vector
    Vector& data() { return m_vect; }

    /// Read access to the data vector
    const Vector& data() const { return m_vect; }

    /// Access the packing version
    [[nodiscard]] char packingVersion() const { return m_packingVersion; }

    /// Describe serialization of object
    template <typename T>
    void save( T& buf ) const {
      buf.save( static_cast<uint8_t>( m_packingVersion ) );
      buf.save( static_cast<uint8_t>( version() ) );
      buf.save( m_vect );
    }

    /// Describe de-serialization of object
    template <typename T>
    void load( T& buf ) {
      m_packingVersion = buf.template load<uint8_t>();
      setVersion( buf.template load<uint8_t>() );
      buf.load( m_vect, m_packingVersion );
    }

    // Perform unpacking
    friend StatusCode unpack( Gaudi::Algorithm const*, const PackedNeutralPIDs&, NeutralPIDs& );

  private:
    /// Data packing version
    char m_packingVersion{defaultPackingVersion()};

    /// The packed data objects
    Vector m_vect;
  };

  /**
   *  Utility class to handle the packing and unpacking of the NeutralPIDs
   *
   */
  class NeutralPIDPacker : public PackerBase {
  public:
    // These are required by the templated algorithms
    typedef LHCb::NeutralPID        Data;
    typedef LHCb::PackedNeutralPID  PackedData;
    typedef LHCb::NeutralPIDs       DataVector;
    typedef LHCb::PackedNeutralPIDs PackedDataVector;
    static const std::string&       packedLocation() { return LHCb::PackedNeutralPIDLocation::Default; }
    static const std::string&       unpackedLocation() { return LHCb::NeutralPIDLocation::Default; }
    constexpr inline static auto    propertyName() { return "NeutralPIDs"; }

    using PackerBase::PackerBase;

    /// Pack NeutralPID
    template <typename NeutralPIDsRange>
    void pack( const NeutralPIDsRange& pids, PackedDataVector& ppids ) const {
      const auto ver = ppids.packingVersion();
      if ( !isSupportedVer( ver ) ) return;
      ppids.data().reserve( pids.size() );
      for ( const auto* pid : pids ) pack( *pid, ppids.data().emplace_back(), ppids );
    }

    /// Unpack a single NeutralPID
    StatusCode unpack( const PackedData& ppid, Data& pid, const PackedDataVector& ppids ) const;

    /// Unpack NeutralPIDs
    StatusCode unpack( const PackedDataVector& ppids, DataVector& pids ) const;

    /// Compare two NeutralPIDs to check the packing -> unpacking performance
    StatusCode check( const Data* dataA, const Data* dataB ) const;

  private:
    /// Pack a NeutralPID
    void pack( const Data& pid, PackedData& ppid, PackedDataVector& ppids ) const;

    /// Check if the given packing version is supported
    [[nodiscard]] static bool isSupportedVer( const char ver ) {
      const bool OK = ( 0 == ver );
      if ( !OK ) {
        throw GaudiException( fmt::format( "Unknown packed data version {}", (int)ver ), "NeutralPIDPacker",
                              StatusCode::FAILURE );
      }
      return OK;
    }
  };

} // namespace LHCb
