/*****************************************************************************\
* (c) Copyright 2024 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "Event/UTHitCluster.h"
#include "Kernel/STLExtensions.h"
#include <algorithm>
#include <numeric>

#ifndef __CLING__
LHCb::UTHitClusters LHCb::hitClustersFromUTHitClusterVector( LHCb::span<LHCb::UTHitCluster const> clusters ) {
  LHCb::UTHitClusters outputClusters;

  if ( clusters.empty() ) return outputClusters;

  outputClusters.reserve( clusters.size() );

  std::vector<size_t> idx( clusters.size() );
  iota( idx.begin(), idx.end(), 0 );

  // prepare a sorted version of the clusters and use that to insert
  std::stable_sort( idx.begin(), idx.end(), [&clusters]( auto hit_a_idx, auto hit_b_idx ) {
    return clusters[hit_a_idx].channelID().sectorFullID() < clusters[hit_b_idx].channelID().sectorFullID();
  } );

  for ( auto cluster_index : idx ) { outputClusters.addHit( clusters[cluster_index] ); }

  outputClusters.setOffsets();

  return outputClusters;
}
#endif