/*****************************************************************************\
* (c) Copyright 2020-2021 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "Event/UTDigit.h"
#include "Kernel/UTNames.h"

std::string LHCb::UTDigit::sectorName() const { return UTNames::UniqueSectorToString( channelID() ); }
std::string LHCb::UTDigit::layerName() const { return UTNames::UniqueLayerToString( channelID() ); }
std::string LHCb::UTDigit::stationName() const { return UTNames::StationToString( channelID() ); }
