/*****************************************************************************\
* (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once
#include <cassert>
#include <cstdint>
#include <ostream>
#include <type_traits>

/** @namespace CaloDigitStatus
 *
 * @brief Predefined constants for the digit status flags
 *
 * @author unknown
 *
 */

namespace LHCb::Calo::DigitStatus {

  namespace details {
    // inspired by https://github.com/foonathan/type_safe/blob/master/include/type_safe/flag_set.hpp
    // TODO: move to some more generic place -- but I would prefer to then re-number the enum such that
    //       its values simply go like 0,1,2,3... and then `mask` does (1u<<...) internally instead of requiring
    //       that the the enum values are powers of 2... i.e. number the bits instead of being the mask
    template <typename Enum, typename StorageType = std::underlying_type_t<Enum>>
    class FlagSet {
      static_assert( std::is_unsigned_v<StorageType> );
      StorageType m_bits;

      [[nodiscard]] static constexpr auto mask( Enum e ) noexcept {
        auto m = static_cast<StorageType>( e );
        assert( __builtin_popcount( m ) <= 1 ); // FIXME: C++20 use std::popcount instead
        return m;
      };

    public:
      // constructors

      constexpr explicit FlagSet( StorageType bits ) noexcept : m_bits{bits} {}

      template <typename... E, typename = std::enable_if_t<std::conjunction_v<std::is_same<E, Enum>...>>>
      constexpr FlagSet( Enum m0, E... mi ) noexcept : m_bits{( mask( m0 ) | ... | mask( mi ) )} {}

      // modifiers

      constexpr FlagSet& operator|=( FlagSet rhs ) noexcept {
        m_bits |= rhs.m_bits;
        return *this;
      }
      constexpr FlagSet& operator&=( FlagSet rhs ) noexcept {
        m_bits &= rhs.m_bits;
        return *this;
      }

      constexpr FlagSet& set( FlagSet fs ) noexcept { return *this |= fs; }
      constexpr FlagSet& reset( FlagSet fs ) noexcept { return *this &= ~fs; }

      // operators

      [[nodiscard]] constexpr friend FlagSet operator&( FlagSet lhs, FlagSet rhs ) noexcept { return lhs &= rhs; }
      [[nodiscard]] constexpr friend FlagSet operator|( FlagSet lhs, FlagSet rhs ) noexcept { return lhs |= rhs; }
      [[nodiscard]] constexpr FlagSet        operator~() const noexcept { return FlagSet{~m_bits}; }
      friend std::ostream&                   operator<<( std::ostream& os, FlagSet fs ) { return os << fs.m_bits; }

      // observers: comparison & tests

      [[nodiscard]] constexpr friend bool operator==( FlagSet lhs, FlagSet rhs ) noexcept {
        return lhs.m_bits == rhs.m_bits;
      }
      [[nodiscard]] constexpr friend bool operator!=( FlagSet lhs, FlagSet rhs ) noexcept { return !( lhs == rhs ); }

      [[nodiscard]] constexpr bool anyOf( FlagSet m ) const noexcept { return ( m_bits & m.m_bits ) != 0; }
      [[nodiscard]] constexpr bool allOf( FlagSet m ) const noexcept { return ( m_bits & m.m_bits ) == m.m_bits; }
      [[nodiscard]] constexpr bool noneOf( FlagSet m ) const noexcept { return !anyOf( m ); }

      [[nodiscard]] constexpr bool test( Enum m ) const noexcept { return anyOf( m ); }

      // observers: misc.

      [[nodiscard]] constexpr StorageType data() const noexcept { return m_bits; }
    };
  } // namespace details

  /// Status mask
  enum class Mask : std::uint32_t {
    Undefined                  = 0,
    SeedCell                   = 1,
    LocalMaximum               = 2,
    CentralCell                = 4,
    OwnedCell                  = 8,
    EdgeCell                   = 16,
    SharedCell                 = 32,
    UseForEnergy               = 64,
    UseForPosition             = 128,
    UseForCovariance           = 256,
    ModifiedBy3x3Tagger        = 512,
    ModifiedByMax2x2Tagger     = 1024,
    ModifiedByNeighbourTagger  = 2048,
    ModifiedBySwissCrossTagger = 4096,
    ModifiedByAllTagger        = 8192
  };

  /// Digit status (presumably 32 bits)
  using Status = details::FlagSet<Mask>;

} // namespace LHCb::Calo::DigitStatus

namespace LHCb::CaloDigitStatus {
  // backwards compatibility
  using Mask   = LHCb::Calo::DigitStatus::Mask;
  using Status = LHCb::Calo::DigitStatus::Status;
} // namespace LHCb::CaloDigitStatus
