/*****************************************************************************\
* (c) Copyright 2022 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once

#include "GaudiKernel/KeyedContainer.h"
#include "GaudiKernel/KeyedObject.h"
#include "Kernel/PlumeChannelIDKeyTraits.h"
#include <map>
#include <ostream>

namespace LHCb {
  // FIXME cabling maps that need to move to conditions
  namespace Plume {
    // EDMS 2100937: PLUME=10 -> (10 << 11 == 0x5000 == 20480)
    static const int lumiAndMonitoringSourceID_1 = 0x5001;
    static const int lumiAndMonitoringSourceID_2 = 0x5002;
    static const int timingSourceID_1            = 0x5003;
    static const int timingSourceID_2            = 0x5004;

    static const int nLumiPMTsPerBank  = 22;
    static const int nTimingPMTs       = 4;
    static const int nMonitoringPMTs   = 4;
    static const int nEmptyPMTsPerBank = 2;
    static const int nPINChannels      = 8;

    static const std::map<unsigned int, unsigned int> lumiFebToLogicalChannel{
        {0, 0},   {1, 24},  {2, 1},   {3, 25},  {4, 2},   {5, 26},  {6, 3},   {7, 27},  {8, 4},   {9, 28},  {10, 12},
        {11, 36}, {12, 13}, {13, 37}, {14, 14}, {15, 38}, {16, 15}, {17, 39}, {18, 16}, {19, 40}, {20, 23}, {21, 47},
        {32, 6},  {33, 30}, {34, 7},  {35, 31}, {36, 8},  {37, 32}, {38, 9},  {39, 33}, {40, 10}, {41, 34}, {42, 17},
        {43, 41}, {44, 18}, {45, 42}, {46, 19}, {47, 43}, {48, 20}, {49, 44}, {50, 21}, {51, 45}, {52, 22}, {53, 46}};
    using ChannelType = LHCb::Detector::Plume::ChannelID::ChannelType;
    static const std::map<unsigned int, std::pair<ChannelType, unsigned int>> monitoringFebToLogicalChannel{
        {0, {ChannelType::PIN, 1}}, {1, {ChannelType::PIN, 2}},  {2, {ChannelType::PIN, 3}},
        {3, {ChannelType::PIN, 4}}, {4, {ChannelType::PIN, 8}},  {5, {ChannelType::PIN, 9}},
        {6, {ChannelType::PIN, 6}}, {7, {ChannelType::PIN, 7}},  {8, {ChannelType::MON, 1}},
        {9, {ChannelType::MON, 2}}, {10, {ChannelType::MON, 3}}, {11, {ChannelType::MON, 4}}};

    static const std::array<std::map<unsigned int, std::pair<unsigned int, unsigned int>>, 2> timingFebToLogicalChannel{
        {{{0, {5, 0}},   {1, {5, 8}},    {2, {29, 0}},   {3, {29, 8}},  {4, {5, 1}},    {5, {5, 9}},    {6, {29, 1}},
          {7, {29, 9}},  {8, {5, 2}},    {9, {5, 10}},   {10, {29, 2}}, {11, {29, 10}}, {12, {5, 3}},   {13, {5, 11}},
          {14, {29, 3}}, {15, {29, 11}}, {16, {5, 4}},   {17, {5, 12}}, {18, {29, 4}},  {19, {29, 12}}, {20, {5, 5}},
          {21, {5, 13}}, {22, {29, 5}},  {23, {29, 13}}, {24, {5, 6}},  {25, {5, 14}},  {26, {29, 6}},  {27, {29, 14}},
          {28, {5, 7}},  {29, {5, 15}},  {30, {29, 7}},  {31, {29, 15}}},
         {{0, {11, 0}},  {1, {11, 8}},   {2, {35, 0}},  {3, {35, 8}},   {4, {11, 1}},  {5, {11, 9}},
          {6, {35, 1}},  {7, {35, 9}},   {8, {11, 2}},  {9, {11, 10}},  {10, {35, 2}}, {11, {35, 10}},
          {12, {11, 3}}, {13, {11, 11}}, {14, {35, 3}}, {15, {35, 11}}, {16, {11, 4}}, {17, {11, 12}},
          {18, {35, 4}}, {19, {35, 12}}, {20, {11, 5}}, {21, {11, 13}}, {22, {35, 5}}, {23, {35, 13}},
          {24, {11, 6}}, {25, {11, 14}}, {26, {35, 6}}, {27, {35, 14}}, {28, {11, 7}}, {29, {11, 15}},
          {30, {35, 7}}, {31, {35, 15}}}}};
    static const std::array<std::array<unsigned int, 2>, 2> timingValueLogicalChannel{{{5, 29}, {11, 35}}};
  } // namespace Plume

  // Class ID definition
  static const CLID CLID_PlumeAdc = 14100;

  // Namespace for locations in TDS
  namespace PlumeAdcLocation {
    inline const std::string Default = "Raw/Plume/Adcs";
  } // namespace PlumeAdcLocation

  /** @class PlumeAdc PlumeAdc.h
   *
   * @brief The ADC content for given PMT * * * The class represents the
   * digitised value in a Plume PMT
   *
   * @author Vladyslav Orlov
   *
   */

  class PlumeAdc final : public KeyedObject<LHCb::Detector::Plume::ChannelID> {
  public:
    /// typedef for KeyedContainer of PlumeAdc
    using Container = KeyedContainer<PlumeAdc, Containers::HashMap>;

    /// Non-default constructor
    PlumeAdc( const LHCb::Detector::Plume::ChannelID& id, int adc, int pedestal, bool overThreshold )
        : KeyedObject<LHCb::Detector::Plume::ChannelID>( id )
        , m_adc( adc )
        , m_pedestal( pedestal )
        , m_overThreshold( overThreshold ) {}

    /// Copy Constructor
    PlumeAdc( const PlumeAdc& src )
        : KeyedObject<LHCb::Detector::Plume::ChannelID>( src.channelID() )
        , m_adc( src.adc() )
        , m_pedestal( src.pedestal() )
        , m_overThreshold( src.overThreshold() ) {}

    /// Default Constructor
    PlumeAdc() = default;

    /// Default copy asignment
    PlumeAdc& operator=( const PlumeAdc& ) = default;

    // Retrieve pointer to class definition structure
    [[nodiscard]] const CLID& clID() const override { return LHCb::PlumeAdc::classID(); };
    static const CLID&        classID() { return CLID_PlumeAdc; }

    /// Fill the ASCII output stream
    std::ostream& fillStream( std::ostream& s ) const override;

    /// Retrieve pmt identifier/key @attention alias to Base::key() method!
    [[nodiscard]] const LHCb::Detector::Plume::ChannelID& channelID() const { return key(); }

    /// Retrieve const  ADC value for the given PMT
    [[nodiscard]] int adc() const { return m_adc; }

    /// Retrieve const  pedestal value for the given PMT
    [[nodiscard]] int pedestal() const { return m_pedestal; }

    /// Retrieve overThreshold value for the given PMT
    [[nodiscard]] bool overThreshold() const { return m_overThreshold; }

    friend std::ostream& operator<<( std::ostream& str, const PlumeAdc& obj ) { return obj.fillStream( str ); }

  private:
    int  m_adc{0}; ///< ADC value for the given pmt
    int  m_pedestal{0};
    bool m_overThreshold{false};
  };

  /// Definition of Keyed Container for PlumeAdc
  using PlumeAdcs = KeyedContainer<PlumeAdc, Containers::HashMap>;
} // namespace LHCb
