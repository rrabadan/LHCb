/*****************************************************************************\
* (c) Copyright 2024 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once

// Include files
#include "Detector/UT/ChannelID.h"
#include "Detector/UT/UTConstants.h"

#include "LHCbMath/LHCbMath.h"
#include "UTDet/DeUTSector.h"
#include <iostream>

#include "GaudiKernel/ClassID.h"
#include "GaudiKernel/DataObject.h"

#include "Event/ZipUtils.h"
#include "Kernel/EventLocalAllocator.h"
#include "Kernel/STLExtensions.h"

// This compile guard is present for gcc12 + lcg 104/105
// support for ROOT.
// See https://gitlab.cern.ch/lhcb/LHCb/-/issues/342
#ifndef __CLING__
#  include "Kernel/MultiIndexedContainer.h"
#endif

namespace LHCb {

  static const CLID CLID_UTHitClusters = 9021;

  /**
   * The UTHitCluster is a minimal implementation of the
   * UT cluster information. It is implemented along the
   * same lines as the FTLiteCluster, where the rudimentary
   * cluster information, including the channelID and
   * and the fractional strip information, are packed together
   * in a "bit field". As bit fields are not portable, instead
   * two integers are used which are filled and interpreted
   * in a consitent manner using bit shifts and masks.
   *
   * The reserved number of bits are the result of a very generous
   * allocation at the time of Run-3 commissioning (2024), but ensure
   * that no changes to this class are needed.
   *
   * @author Laurent Dufour
   **/
  class UTHitCluster final {
    // FIXME: It would be good if we could use a 'native' 48-bit type here,
    // which would be a combination of 32-bit + 8-bit.
    // or just 5 8-bits.
    // Unfortunately the cluster charge is 9 bits, not 8.
    unsigned int m_cluster;
    uint16_t     m_clusterEx;

  private:
    /// Offsets of bitfield with 21 bits for channelID
    enum clusterBits {
      channelIDBits = 0,
      fracStripBits = 21, // channelID is 21 bits, so start at 21
      sizeBits      = 30, // 9 bits for fracStrip
    };
    enum clusterMasks { channelIDMask = 0x001FFFFFL, fractStripMask = 0x3FE00000L, sizeMask = 0xC0000000L };

    enum clusterExMasks {
      sizeExMask          = 0x3FL,  // first 6 bits
      clusterChargeExMask = 0xFFC0L // last 10 bits
    };
    enum clusterExBits {
      sizeExBits          = 0,
      clusterChargeExBits = 6, // size is 6 bits, start at 6 (counting starts at 0)
    };

  public:
    UTHitCluster( Detector::UT::ChannelID chan, float _fracStrip, unsigned int size, unsigned int clusterCharge ) {
      m_cluster = ( ( chan << clusterBits::channelIDBits ) & clusterMasks::channelIDMask );

      uint8_t fracStripUnits = ( _fracStrip + 0.5 ) * 255;
      m_cluster += ( fracStripUnits << clusterBits::fracStripBits );

      // the size is split up between two data fields
      uint8_t cropped_size = std::min( size, (unsigned int)128 ) - 1;
      m_cluster += ( ( cropped_size << clusterBits::sizeBits ) & clusterMasks::sizeMask );

      // now add the other 6 bits from the size in the other variable
      // (removing 2 bits from the cropped_size)
      m_clusterEx                         = ( ( cropped_size >> 2 ) & clusterExMasks::sizeExMask );
      unsigned int cropped_cluster_charge = std::min( clusterCharge, (unsigned int)1023 );

      m_clusterEx += ( cropped_cluster_charge << clusterExBits::clusterChargeExBits );
    }

    UTHitCluster( unsigned int cluster, uint16_t clusterEx ) : m_cluster( cluster ), m_clusterEx( clusterEx ) {}

    inline unsigned int size() const {
      unsigned int size = ( m_cluster & clusterMasks::sizeMask ) >> clusterBits::sizeBits;
      size += ( ( m_clusterEx & clusterExMasks::sizeExMask ) >> clusterExBits::sizeExBits ) << 2;
      size += 1;

      return size;
    }

    inline uint8_t fractionUnits() const {
      return ( m_cluster & clusterMasks::fractStripMask ) >> clusterBits::fracStripBits;
    }

    inline float fracStrip() const {
      float fUnits = fractionUnits();
      if ( fUnits == ( uint8_t )( 0.5 * 255 ) ) return 0.0;
      if ( fUnits == ( uint8_t )( 0 ) ) return -0.5;

      return ( fUnits / 255 ) - 0.5 + 0.5 / 255;
    }

    inline unsigned int clusterCharge() const {
      return ( ( m_clusterEx & clusterExMasks::clusterChargeExMask ) >> clusterExBits::clusterChargeExBits );
    }

    inline bool isUT() const { return channelID().isUT(); }

    inline unsigned int station() const { return channelID().station(); }

    inline unsigned int layer() const { return channelID().layer(); }

    inline unsigned int detRegion() const { return channelID().detRegion(); }

    inline unsigned int sector() const { return channelID().sector(); }

    inline unsigned int strip() const { return channelID().strip(); }

    // Explicitly set the mask for the 'type' to UT
    inline Detector::UT::ChannelID channelID() const {
      return Detector::UT::ChannelID(
          ( ( ( m_cluster & clusterMasks::channelIDMask ) >> clusterBits::channelIDBits ) + ( 2 << 21 ) ) );
    }

    inline std::pair<unsigned int, uint16_t> getCluster() const { return {m_cluster, m_clusterEx}; }

    /// Print the lite cluster in a human readable way
    inline std::ostream& fillStream( std::ostream& s ) const {

      s << "{ UTHitCluster: " << (int)channelID() << " strip " << strip() << " Size " << size()
        << " interStripFraction " << fracStrip() << " clusterCharge " << clusterCharge() << std::endl
        << " }";
      return s;
    }

    friend std::ostream& operator<<( std::ostream& str, const UTHitCluster& obj ) { return obj.fillStream( str ); }

    bool operator==( const UTHitCluster& other ) const {
      return m_cluster == other.m_cluster && m_clusterEx == other.m_clusterEx;
    }

    /** Ordering required by the IndexedHitContainer (UTHitClusters) **/
    struct Order {
      bool operator()( const UTHitCluster& lhs, const UTHitCluster& rhs ) const {
        // Primary sorting done by the indexing paramter
        if ( lhs.channelID().sectorFullID() != rhs.channelID().sectorFullID() )
          return lhs.channelID().sectorFullID() < rhs.channelID().sectorFullID();

        if ( lhs.channelID() != rhs.channelID() ) { return lhs.channelID() < rhs.channelID(); }

        // This is needed to deal with the corrupted data,
        // in which the first one is kept (over the others)
        // single-size clusters now preferred, giving a more
        // deterministic behaviour.
        return lhs.size() < rhs.size();
      }
    };
  };

// This compile guard is present for gcc12 + lcg 104/105
// support for ROOT.
// See https://gitlab.cern.ch/lhcb/LHCb/-/issues/342
#ifndef __CLING__
  /**
   * Container for UTHitClusters, with an indexing based on the
   * sectorFullID() (same as the UTHitHandler)
   *
   * Most of the work is provided by the IndexedContainer,
   * but an additional API is provided to help with
   * (1) the possible persistence of this class
   *     in the far future
   *
   * (2) the direct clustering into these objects
   *     via the PrStoreUTHits
   *
   * The indexing is currently done on the unique sector ID (sectorFullID)
   * This is an index with a size over 2000, while there are O(1k)
   * clusters per event. The reason it was chosen is just
   * because it's the same as what is used in the UTHitHandler.
   * If it turns out to be as fast to use a less-stringent
   * index, such as uniqueLayer, that could be changed as it would
   * probably be more practical in its use.
   **/
  class UTHitClusters final : public DataObject {
  private:
    LHCb::Container::MultiIndexedContainer<UTHitCluster, LHCb::Detector::UT::MaxNumberOfSectors> m_clusters; // n layers

  public:
    /// Default Constructor
    UTHitClusters()      = default;
    using allocator_type = LHCb::Allocators::EventLocal<UTHitCluster>;

    UTHitClusters( Zipping::ZipFamilyNumber, allocator_type ) {}
    UTHitClusters( allocator_type ) {}

    // Retrieve pointer to class definition structure
    static const CLID&        classID() { return CLID_UTHitClusters; }
    [[nodiscard]] const CLID& clID() const override { return UTHitClusters::classID(); }

    void reserve( size_t r ) { m_clusters.reserve( r ); }

    /// return iterator for beginning / end of nonmutable sequence
    auto begin() const { return m_clusters.range().begin(); }
    auto end() const { return m_clusters.range().end(); }

    // These are the only non-const iterators that you can use...
    auto begin( unsigned int hitGroup ) { return m_clusters.range_( hitGroup ).first; }
    auto end( unsigned int hitGroup ) { return m_clusters.range_( hitGroup ).second; }

    auto begin( unsigned int hitGroup ) const { return m_clusters.range( hitGroup ).begin(); }
    auto end( unsigned int hitGroup ) const { return m_clusters.range( hitGroup ).end(); }

    auto range( unsigned int hitGroup ) const { return m_clusters.range( hitGroup ); }

    auto range() const { return m_clusters.range(); }

    /**
     * Note: when using this function, call the forceSort() explicitly after
     * inserting all your hits!
     */
    void addHit( LHCb::Detector::UT::ChannelID chanID, float fracStrip, unsigned int size,
                 unsigned int clusterCharge ) {
      m_clusters.addHit( std::forward_as_tuple( chanID, fracStrip, size, clusterCharge ), chanID.sectorFullID() );
    }

    void forceSort() { m_clusters.forceSort(); }

    /**
     * Removes duplicates from the container based on their channelID.
     * The first one in the list (as determined by the Sort) is kept.
     *
     * Returns true when the container was already unique.
     */
    bool forceUnique() {
      auto old_size = m_clusters.size();

      m_clusters.forceUnique( [&]( const UTHitCluster& clusterA, const UTHitCluster& clusterB ) {
        return clusterA.channelID() == clusterB.channelID();
      } );

      return ( old_size == m_clusters.size() );
    }

    /**
     * Note: This function is rather slow - avoid when possible.
     * However, does allow to insert clusters in an unordered manner.
     */
    void addHit( const UTHitCluster& cluster ) {
      m_clusters.insert( &cluster, &cluster + 1, cluster.channelID().sectorFullID() );
    }

    void setOffsets() { m_clusters.setOffsets(); }

    // This API is chosen for compatibility with the
    // PrStoreUTHits class
    void emplace_back( const DeUTSector&, unsigned int, unsigned int, double fracStrip,
                       LHCb::Detector::UT::ChannelID chanID, unsigned int size, bool, unsigned int clusterCharge ) {
      addHit( chanID, fracStrip, size, clusterCharge );
    }

    void emplace_back( LHCb::Detector::UT::ChannelID chanID, float fracStrip, unsigned int size,
                       unsigned int clusterCharge ) {
      addHit( chanID, fracStrip, size, clusterCharge );
    }

    void push_back( const UTHitCluster& cluster ) {
      m_clusters.insert( &cluster, &cluster + 1, cluster.channelID().sectorFullID() );
      m_clusters.setOffsets();
    }

    auto size() const { return m_clusters.size(); }
    void clear() { return m_clusters.clear(); }
  };

  inline auto sortClusterContainer( LHCb::UTHitClusters& clusters ) { return clusters.forceSort(); }

  inline std::optional<LHCb::UTHitCluster> findCluster( LHCb::Detector::UT::ChannelID channelID,
                                                        const LHCb::UTHitClusters&    clusters ) {
    for ( const auto& cluster : clusters.range( channelID.sectorFullID() ) ) {
      if ( cluster.channelID() == channelID ) { return cluster; }
    }

    return std::nullopt;
  }

  LHCb::UTHitClusters hitClustersFromUTHitClusterVector( LHCb::span<LHCb::UTHitCluster const> );
#endif
} // namespace LHCb