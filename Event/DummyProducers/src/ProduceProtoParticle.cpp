/*****************************************************************************\
* (c) Copyright 2022 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#include "Event/ProtoParticle.h"
#include "GaudiKernel/RndmGenerators.h"
#include "LHCbAlgs/Producer.h"

//-----------------------------------------------------------------------------
// A class to produce dummy protoparticles with many extraInfo for testing purposes
//
// 2022-05-19 : Samuel Belin (adapting Sevda Esen's similar code)
//-----------------------------------------------------------------------------

namespace LHCb {

  struct ProduceProtoParticles : public Algorithm::Producer<ProtoParticles()> {
    ProduceProtoParticles( const std::string& name, ISvcLocator* svcLoc )
        : Producer( name, svcLoc, KeyValue{"Output", "Event/Fake/ProtoParticles"} ) {}
    ProtoParticles operator()() const override {
      Rndm::Numbers  rndmgauss( randSvc(), Rndm::Gauss( 10., 1. ) );
      ProtoParticles datavector;
      for ( int i = 0; i < int( m_npart ); i++ ) {
        auto* ppart = new ProtoParticle();
        ppart->addInfo( 0, i );
        for ( int j = 1; j < int( m_nExtraInfo ); j++ ) {
          // dummy extrainfo
          double a = rndmgauss();
          ppart->addInfo( 10000 + j, a );
        }
        datavector.insert( ppart, i );
      }
      return datavector;
    }

  private:
    Gaudi::Property<std::size_t> m_npart{this, "NumberPart", 70000, "Number of protoparticle to generate"};
    Gaudi::Property<std::size_t> m_nExtraInfo{this, "nExtraInfo", 100, "Number of extrainfo for each protoparticle"};
  };
  DECLARE_COMPONENT_WITH_ID( ProduceProtoParticles, "ProduceProtoParticles" )

} // namespace LHCb
