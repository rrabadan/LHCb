/*****************************************************************************\
* (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once
#include "GaudiKernel/DataObject.h"
#include "GaudiKernel/VectorMap.h"
#include <ostream>

namespace LHCb {

  // Forward declarations

  // Class ID definition
  static const CLID CLID_HltLumiSummary = 7541;

  // Namespace for locations in TDS
  namespace HltLumiSummaryLocation {
    inline const std::string Default = "Hlt/LumiSummary";
  }

  /** @class HltLumiSummary HltLumiSummary.h
   *
   * Summary class for Lumi in Hlt
   *
   * @author Jaap Panman
   *
   */

  class HltLumiSummary final : public DataObject {
  public:
    /// User information
    typedef GaudiUtils::VectorMap<std::string, double> ExtraInfo;

    /// Default Constructor
    HltLumiSummary() = default;

    // Retrieve pointer to class definition structure
    const CLID&        clID() const override { return LHCb::HltLumiSummary::classID(); }
    static const CLID& classID() { return CLID_HltLumiSummary; }

    /// Fill the ASCII output stream
    std::ostream& fillStream( std::ostream& s ) const override {
      return s << "{ "
               << "extraInfo :	" << m_extraInfo << "\n }";
    }

    /// ExtraInformation. Don't use directly, use hasInfo, info, addInfo...
    const ExtraInfo& extraInfo() const { return m_extraInfo; }

    /// has information for specified key
    bool hasInfo( std::string const& key ) const { return m_extraInfo.end() != m_extraInfo.find( key ); }

    ///  Add new information associated with the specified key. This method cannot be used to modify information for a
    ///  pre-existing key.
    bool addInfo( std::string const& key, double info ) { return m_extraInfo.insert( key, info ).second; }

    /// extract the information associated with the given key. If there is no such infomration the default value will be
    /// returned.
    double info( const std::string& key, const double def ) const {
      auto i = m_extraInfo.find( key );
      return m_extraInfo.end() == i ? def : i->second;
    }

    /// erase the information associated with the given key
    unsigned long eraseInfo( std::string const& key ) { return m_extraInfo.erase( key ); }

    /// Update  Some addtional user information. Don't use directly. Use *Info() methods.
    HltLumiSummary& setExtraInfo( ExtraInfo value ) {
      m_extraInfo = std::move( value );
      return *this;
    }

    friend std::ostream& operator<<( std::ostream& str, const HltLumiSummary& obj ) { return obj.fillStream( str ); }

  private:
    ExtraInfo m_extraInfo; ///< Some addtional user information. Don't use directly. Use *Info() methods.

  }; // class HltLumiSummary

} // namespace LHCb
