###############################################################################
# (c) Copyright 2000-2021 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
#[=======================================================================[.rst:
Event/PhysEvent
---------------
#]=======================================================================]

gaudi_add_library(PhysEvent
    SOURCES
        src/Particle.cpp
        src/Vertex.cpp
        src/Tagger.cpp
    LINK
        PUBLIC
            Gaudi::GaudiKernel
            LHCb::EventBase
            LHCb::LHCbKernel
            LHCb::LHCbMathLib
            LHCb::RecEvent
)

gaudi_add_dictionary(PhysEventDict
    HEADERFILES dict/dictionary.h
    SELECTION dict/selection.xml
    LINK LHCb::PhysEvent
)

gaudi_add_executable(test_Particle_v2
    SOURCES
        tests/src/test_Particle_v2.cpp
    LINK
        Boost::unit_test_framework
        LHCb::PhysEvent
    TEST
)
