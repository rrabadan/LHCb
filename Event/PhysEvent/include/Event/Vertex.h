/*****************************************************************************\
* (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once

// Include files
#include "Event/VertexBase.h"
#include "GaudiKernel/KeyedContainer.h"
#include "GaudiKernel/NamedRange.h"
#include "GaudiKernel/SharedObjectsContainer.h"
#include "GaudiKernel/SmartRefVector.h"
#include <ostream>
#include <vector>

// Forward declarations

namespace LHCb {

  // Forward declarations
  class Particle;

  // Class ID definition
  static const CLID CLID_Vertex = 802;

  // Namespace for locations in TDS
  namespace VertexLocation {
    inline const std::string User       = "Phys/User/Vertices";
    inline const std::string Production = "Phys/Prod/Vertices";
  } // namespace VertexLocation

  /** @class Vertex Vertex.h
   *
   * Physics analysis Vertex. It holds geometrical information and is part of the
   * tree structure
   *
   * @author Patrick Koppenburg
   *
   */

  class Vertex : public VertexBase {
  public:
    /// typedef for std::vector of Vertex
    typedef std::vector<Vertex*>       Vector;
    typedef std::vector<const Vertex*> ConstVector;

    /// typedef for KeyedContainer of Vertex
    typedef KeyedContainer<Vertex, Containers::HashMap> Container;

    /// The container type for shared vertices (without ownership)
    typedef SharedObjectsContainer<LHCb::Vertex> Selection;
    /// For uniform access to containers in TES (KeyedContainer,SharedContainer)
    typedef Gaudi::NamedRange_<ConstVector> Range;

    /// Describe how the vertex was made (NEED MORE)
    enum CreationMethod {
      Unknown = 0,
      Primary = LHCb::VertexBase::VertexType::Primary,
      Unconstrained,
      MassConstrained,
      VertexFitter,
      MomentumAdder,
      LastGlobal = 10000
    };
    friend std::ostream& operator<<( std::ostream& s, LHCb::Vertex::CreationMethod e ) {
      switch ( e ) {
      case LHCb::Vertex::Unknown:
        return s << "Unknown";
      case LHCb::Vertex::Primary:
        return s << "Primary";
      case LHCb::Vertex::Unconstrained:
        return s << "Unconstrained";
      case LHCb::Vertex::MassConstrained:
        return s << "MassConstrained";
      case LHCb::Vertex::VertexFitter:
        return s << "VertexFitter";
      case LHCb::Vertex::MomentumAdder:
        return s << "MomentumAdder";
      case LHCb::Vertex::LastGlobal:
        return s << "LastGlobal";
      default:
        return s << "ERROR wrong value " << int( e ) << " for enum LHCb::Vertex::CreationMethod";
      }
    }

    /// (Default) Constructor(s)
    using VertexBase::VertexBase;

    // Retrieve pointer to class definition structure
    const CLID&        clID() const override { return LHCb::Vertex::classID(); }
    static const CLID& classID() { return CLID_Vertex; }

    /// Clone vertex
    [[nodiscard]] Vertex* clone() const override { return new Vertex( *this ); }

    /// Is the vertex a primary?
    [[nodiscard]] bool isPrimary() const override { return Vertex::CreationMethod::Primary == m_technique; }

    /// Get outgoing particles as a LHCb::Particle::ConstVector
    [[nodiscard]] std::vector<const LHCb::Particle*> outgoingParticlesVector() const {
      std::vector<const LHCb::Particle*> out;
      out.reserve( m_outgoingParticles.size() );
      std::copy( m_outgoingParticles.begin(), m_outgoingParticles.end(), std::back_inserter( out ) );
      return out;
    }

    /// Print this Particle in a human readable way
    std::ostream& fillStream( std::ostream& s ) const override;

    /// Retrieve const  How the vertex was made
    [[nodiscard]] const CreationMethod& technique() const { return m_technique; }

    /// Update  How the vertex was made
    Vertex& setTechnique( const CreationMethod& value ) {
      m_technique = value;
      return *this;
    }

    /// Retrieve (const)  Reference to outgoing particles
    [[nodiscard]] const SmartRefVector<LHCb::Particle>& outgoingParticles() const { return m_outgoingParticles; }

    /// Update  Reference to outgoing particles
    Vertex& setOutgoingParticles( SmartRefVector<LHCb::Particle> value ) {
      m_outgoingParticles = std::move( value );
      return *this;
    }

    /// Add to  Reference to outgoing particles
    Vertex& addToOutgoingParticles( SmartRef<LHCb::Particle> value ) {
      m_outgoingParticles.emplace_back( std::move( value ) );
      return *this;
    }

    /// Remove from  Reference to outgoing particles
    Vertex& removeFromOutgoingParticles( const SmartRef<LHCb::Particle>& value ) {
      m_outgoingParticles.erase( std::remove( m_outgoingParticles.begin(), m_outgoingParticles.end(), value ),
                                 m_outgoingParticles.end() );
      return *this;
    }

    /// Clear  Reference to outgoing particles
    Vertex& clearOutgoingParticles() {
      m_outgoingParticles.clear();
      return *this;
    }

    friend std::ostream& operator<<( std::ostream& str, const Vertex& obj ) { return obj.fillStream( str ); }

    // Interface compatibility with ThOr
    [[nodiscard, gnu::always_inline]] friend LinAlg::Vec<SIMDWrapper::scalar::float_v, 3>
    endVertexPos( const Vertex& vtx ) {
      auto pos = vtx.position();
      return {pos.x(), pos.y(), pos.z()};
    }
    [[nodiscard, gnu::always_inline]] friend auto posCovMatrix( const Vertex& vtx ) {
      return LHCb::LinAlg::convert<SIMDWrapper::scalar::float_v>( vtx.covMatrix() );
    }

  private:
    CreationMethod                 m_technique = Vertex::CreationMethod::Unknown; ///< How the vertex was made
    SmartRefVector<LHCb::Particle> m_outgoingParticles;                           ///< Reference to outgoing particles

  }; // class Vertex

  /// Definition of Keyed Container for Vertex
  using Vertices = Vertex::Container;

} // namespace LHCb

// TODO: should be able to remove the following, but it is kept for backwards compatibility...
#include "Particle.h"
