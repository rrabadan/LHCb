/*****************************************************************************\
* (c) Copyright 2019-2021 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once
#include "GaudiKernel/GaudiException.h"
#include "GaudiKernel/Vector4DTypes.h"
#include "LHCbMath/MatVec.h"
#include "boost/container/static_vector.hpp"
#include <array>
#include <cmath>
#include <functional>

namespace LHCb {

  namespace Combination::details {
    using fp = float;
  }

  /** @brief A list of particle objects.
   *
   * A 'combination' is simply an ordered list of particle objects. This
   * combination object holds such a list and has several member functions for
   * computing kinematic 'properties' of the list.
   *
   * The 'four momentum' of a combination is defined as the sum of the four
   * momentum of the particles held by it. Most other intrinsic properties can be
   * derived from this, such as the 'invariant mass' of a combination.
   *
   * Some other methods exist to make this object compatible with the API expected
   * by the ThOr functor framework, allowing an instance of ParticleCombination to
   * be passed in as an argument to a functor.
   */

  template <typename... InputTypes>
  struct ParticleCombination;

  template <typename Particle_t>
  struct ParticleCombination<Particle_t> {

    /// Pointers to each of the N child objects in this combination
    /// 8 children should be more than enough...
    static_assert( !std::is_pointer_v<Particle_t> );
    boost::container::static_vector<Particle_t const*, 8> m_children{};

  public:
#ifndef __clang__
#  pragma GCC diagnostic push
// hide gcc 12 warning (false positive?), see lhcb/DaVinci#127
#  pragma GCC diagnostic ignored "-Wmaybe-uninitialized"
#endif
    template <typename... Args,
              typename = std::enable_if_t<( sizeof...( Args ) > 1 ) &&
                                          std::conjunction_v<std::is_convertible<Particle_t const*, Args>...>>>
    ParticleCombination( Args&&... args ) : m_children{std::forward<Args>( args )...} {
      assert( std::none_of( m_children.begin(), m_children.end(), []( auto* i ) { return i == nullptr; } ) );
    }
#ifndef __clang__
#  pragma GCC diagnostic pop
#endif

    template <typename Predicate>
    bool pairwise_none_of( Predicate predicate ) const {
      auto e = m_children.end();
      for ( auto i1 = m_children.begin(); i1 != e; ++i1 ) {
        for ( auto i2 = std::next( i1 ); i2 != e; ++i2 ) {
          if ( predicate( **i1, **i2 ) ) return false;
        }
      }
      return true;
    }

    template <typename Transform, typename Reduce = std::plus<>>
    auto transform_reduce( Transform transform, Reduce reduce = {} ) const {
      auto i1  = m_children.begin();
      auto end = m_children.end();
      if ( i1 == end ) {
        throw GaudiException{"Empty collection of children -- this should never happen", "transform_reduce",
                             StatusCode::FAILURE};
      }

      using value_t   = std::invoke_result_t<Transform, decltype( **i1 )>;
      using reduced_t = std::invoke_result_t<Reduce, value_t, value_t>;

      reduced_t current = transform( **i1 );
      for ( ; i1 != m_children.end(); ++i1 ) { current = reduce( std::move( current ), transform( **i1 ) ); }
      return current;
    }

    template <typename Transform, typename Reduce = std::plus<>>
    auto pairwise_transform_reduce( Transform transform, Reduce reduce = {} ) const {
      auto i1  = m_children.begin();
      auto end = m_children.end();
      if ( i1 == end ) {
        throw GaudiException{"Empty collection -- this should never happen", "pairwise_transform_reduce",
                             StatusCode::FAILURE};
      }
      auto i2 = std::next( i1 );
      if ( i2 == end ) {
        throw GaudiException{"single particle collection -- this should never happen", "pairwise_transform_reduce",
                             StatusCode::FAILURE};
      }
      // Calculate the first value explicitly to avoid having to pass in an initial value, and thus having to specify
      // its type
      using value_t     = std::invoke_result_t<Transform, decltype( **i1 ), decltype( **i1 )>;
      using reduced_t   = std::invoke_result_t<Reduce, value_t, value_t>;
      reduced_t current = transform( **i1, **i2 );
      do {
        ++i2;
        for ( ; i2 != end; ++i2 ) { current = reduce( std::move( current ), transform( **i1, **i2 ) ); }
        ++i1;
        i2 = i1;
      } while ( i1 != end );
      return current;
    }

    /** @brief Number of children.
     */
    auto size() const { return m_children.size(); }
    auto numChildren() const { return m_children.size(); }

    Particle_t const& operator[]( size_t i ) const {
      assert( i < m_children.size() );
#pragma GCC diagnostic push
#ifndef __clang__
// hide clang 12 / gcc 10 warnings (false positive?)
#  pragma GCC diagnostic ignored "-Wuninitialized"
#  pragma GCC diagnostic ignored "-Wmaybe-uninitialized"
#endif
      assert( m_children[i] != nullptr );
      return *m_children[i];
#pragma GCC diagnostic pop
    }

    template <size_t N>
    Particle_t const& get() const {
      return ( *this )[N];
    }

    /** @brief the decayProducts of a combination _is_ the combination
     */
    friend ParticleCombination const& decayProducts( ParticleCombination const& pc ) { return pc; }

    auto begin() const { return m_children.begin(); }
    auto end() const { return m_children.end(); }

    /** @brief three momentum sum of the children.
     */
    auto threeMomentum() const {
      Combination::details::fp px = 0;
      Combination::details::fp py = 0;
      Combination::details::fp pz = 0;
      for ( auto* i : m_children ) {
        px += i->momentum().px();
        py += i->momentum().py();
        pz += i->momentum().pz();
      }
      return LHCb::LinAlg::Vec{px, py, pz};
    }

    auto slopes() const {
      auto mom = threeMomentum();
      return mom / Z( mom );
    }

    /** @brief Squared-magnitude of the three momentum sum of the children.
     */
    Combination::details::fp mom2() const { return threeMomentum().mag2(); }

    template <std::size_t... idxs>
    ParticleCombination subCombination() const {
      assert( ( ( 0 <= idxs && idxs < m_children.size() ) && ... ) );
      return ParticleCombination{m_children[idxs]...};
    }

    // necessary specialisation for subCombination<idx...>( decayProducts( <LHCb::Particle> )) to work
    template <std::size_t... idx>
    friend auto subCombination( ParticleCombination item ) {
      return item.subCombination<idx...>();
    }

    /** @brief Magnitude of the four momentum sum of the children.
     */
    Combination::details::fp p() const { return std::sqrt( mom2() ); }

    /** @brief Transverse momentum component of the four momentum sum of the children.
     */
    Combination::details::fp pt() const {
      Combination::details::fp px = 0.;
      Combination::details::fp py = 0.;
      for ( auto* i : m_children ) {
        px += i->momentum().px();
        py += i->momentum().py();
      }
      return std::sqrt( px * px + py * py );
    }

    /** @brief Invariant mass of the four momentum sum of the children.
     */
    Combination::details::fp mass2() const {
      // FIXME/TODO: use stable computation from https://gitlab.cern.ch/lhcb/Analysis/-/issues/23
      Gaudi::LorentzVector sum{};
      for ( auto* i : m_children ) sum += i->momentum();
      return sum.mag2();
    }

    Combination::details::fp mass() const {
      using std::sqrt;
      return sqrt( mass2() );
    }
  };

  template <typename Particle_t, typename... InputTypes>
  struct ParticleCombination<Particle_t, InputTypes...> : ParticleCombination<InputTypes...> {
    static_assert( sizeof...( InputTypes ) > 0 );
    static_assert( std::conjunction_v<std::is_same<Particle_t, InputTypes>...> );
    using ParticleCombination<InputTypes...>::ParticleCombination;
  };

} // namespace LHCb
