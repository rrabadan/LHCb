/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "Event/Particle.h"
#include "GaudiKernel/IRegistry.h"
#include "LHCbMath/MatrixManip.h"
#include <iostream>

//=============================================================================

Gaudi::SymMatrix7x7 LHCb::Particle::covMatrix() const {
  Gaudi::Matrix7x7 full;

  full.Place_at( m_posCovMatrix, 0, 0 );
  full.Place_at( m_momCovMatrix, 3, 3 );
  full.Place_at( m_posMomCovMatrix, 3, 0 );
  full.Place_at( ROOT::Math::Transpose( m_posMomCovMatrix ), 0, 3 );

  return Gaudi::SymMatrix7x7( full.LowerBlock() );
}

//=============================================================================

std::ostream& LHCb::Particle::fillStream( std::ostream& s ) const {
  s << "{ particleID      : " << m_particleID << "\nmeasuredMass    : " << m_measuredMass
    << "\nmeasuredMassErr : " << m_measuredMassErr << "\nmomentum        : " << m_momentum
    << "\nreferencePoint  : " << m_referencePoint << "\nmomCovMatrix    : \n"
    << m_momCovMatrix << "\nposCovMatrix    : \n"
    << m_posCovMatrix << "\nposMomCovMatrix : \n"
    << m_posMomCovMatrix << "\nextraInfo       : [";
  for ( const auto& i : extraInfo() ) {
    if ( i.first < (int)LHCb::Particle::additionalInfo::LastGlobal ) {
      const auto info = static_cast<LHCb::Particle::additionalInfo>( i.first );
      s << " " << info << "=" << i.second;
    } else {
      s << " " << i.first << "=" << i.second;
    }
  }
  s << " ]";
  const std::string testLocation = ( parent() && parent()->registry() ? parent()->registry()->identifier() : "" );
  if ( !testLocation.empty() ) { s << "\nTES=" << testLocation; }
  return s << " }";
}

//=============================================================================
namespace {
  static const GaudiUtils::VectorMap<std::string, LHCb::Particle::additionalInfo> s_additionalInfoTypMap_m = {
      {"Unknown", LHCb::Particle::additionalInfo::Unknown},
      {"ConfLevel", LHCb::Particle::additionalInfo::ConfLevel},
      {"Weight", LHCb::Particle::additionalInfo::Weight},
      {"Chi2OfMassConstrainedFit", LHCb::Particle::additionalInfo::Chi2OfMassConstrainedFit},
      {"Chi2OfDirectionConstrainedFit", LHCb::Particle::additionalInfo::Chi2OfDirectionConstrainedFit},
      {"Chi2OfVertexConstrainedFit", LHCb::Particle::additionalInfo::Chi2OfVertexConstrainedFit},
      {"Chi2OfParticleReFitter", LHCb::Particle::additionalInfo::Chi2OfParticleReFitter},
      {"HasBremAdded", LHCb::Particle::additionalInfo::HasBremAdded},
      {"NumVtxWithinChi2WindowOneTrack", LHCb::Particle::additionalInfo::NumVtxWithinChi2WindowOneTrack},
      {"SmallestDeltaChi2OneTrack", LHCb::Particle::additionalInfo::SmallestDeltaChi2OneTrack},
      {"SmallestDeltaChi2MassOneTrack", LHCb::Particle::additionalInfo::SmallestDeltaChi2MassOneTrack},
      {"SmallestDeltaChi2TwoTracks", LHCb::Particle::additionalInfo::SmallestDeltaChi2TwoTracks},
      {"SmallestDeltaChi2MassTwoTracks", LHCb::Particle::additionalInfo::SmallestDeltaChi2MassTwoTracks},
      {"FlavourTaggingIPPUs", LHCb::Particle::additionalInfo::FlavourTaggingIPPUs},
      {"FlavourTaggingTaggerID", LHCb::Particle::additionalInfo::FlavourTaggingTaggerID},
      {"Cone1Angle", LHCb::Particle::additionalInfo::Cone1Angle},
      {"Cone1Mult", LHCb::Particle::additionalInfo::Cone1Mult},
      {"Cone1PX", LHCb::Particle::additionalInfo::Cone1PX},
      {"Cone1PY", LHCb::Particle::additionalInfo::Cone1PY},
      {"Cone1PZ", LHCb::Particle::additionalInfo::Cone1PZ},
      {"Cone1P", LHCb::Particle::additionalInfo::Cone1P},
      {"Cone1PT", LHCb::Particle::additionalInfo::Cone1PT},
      {"Cone1PXAsym", LHCb::Particle::additionalInfo::Cone1PXAsym},
      {"Cone1PYAsym", LHCb::Particle::additionalInfo::Cone1PYAsym},
      {"Cone1PZAsym", LHCb::Particle::additionalInfo::Cone1PZAsym},
      {"Cone1PAsym", LHCb::Particle::additionalInfo::Cone1PAsym},
      {"Cone1PTAsym", LHCb::Particle::additionalInfo::Cone1PTAsym},
      {"Cone1DeltaEta", LHCb::Particle::additionalInfo::Cone1DeltaEta},
      {"Cone1DeltaPhi", LHCb::Particle::additionalInfo::Cone1DeltaPhi},
      {"Cone2Angle", LHCb::Particle::additionalInfo::Cone2Angle},
      {"Cone2Mult", LHCb::Particle::additionalInfo::Cone2Mult},
      {"Cone2PX", LHCb::Particle::additionalInfo::Cone2PX},
      {"Cone2PY", LHCb::Particle::additionalInfo::Cone2PY},
      {"Cone2PZ", LHCb::Particle::additionalInfo::Cone2PZ},
      {"Cone2P", LHCb::Particle::additionalInfo::Cone2P},
      {"Cone2PT", LHCb::Particle::additionalInfo::Cone2PT},
      {"Cone2PXAsym", LHCb::Particle::additionalInfo::Cone2PXAsym},
      {"Cone2PYAsym", LHCb::Particle::additionalInfo::Cone2PYAsym},
      {"Cone2PZAsym", LHCb::Particle::additionalInfo::Cone2PZAsym},
      {"Cone2PAsym", LHCb::Particle::additionalInfo::Cone2PAsym},
      {"Cone2PTAsym", LHCb::Particle::additionalInfo::Cone2PTAsym},
      {"Cone2DeltaEta", LHCb::Particle::additionalInfo::Cone2DeltaEta},
      {"Cone2DeltaPhi", LHCb::Particle::additionalInfo::Cone2DeltaPhi},
      {"Cone3Angle", LHCb::Particle::additionalInfo::Cone3Angle},
      {"Cone3Mult", LHCb::Particle::additionalInfo::Cone3Mult},
      {"Cone3PX", LHCb::Particle::additionalInfo::Cone3PX},
      {"Cone3PY", LHCb::Particle::additionalInfo::Cone3PY},
      {"Cone3PZ", LHCb::Particle::additionalInfo::Cone3PZ},
      {"Cone3P", LHCb::Particle::additionalInfo::Cone3P},
      {"Cone3PT", LHCb::Particle::additionalInfo::Cone3PT},
      {"Cone3PXAsym", LHCb::Particle::additionalInfo::Cone3PXAsym},
      {"Cone3PYAsym", LHCb::Particle::additionalInfo::Cone3PYAsym},
      {"Cone3PZAsym", LHCb::Particle::additionalInfo::Cone3PZAsym},
      {"Cone3PAsym", LHCb::Particle::additionalInfo::Cone3PAsym},
      {"Cone3PTAsym", LHCb::Particle::additionalInfo::Cone3PTAsym},
      {"Cone3DeltaEta", LHCb::Particle::additionalInfo::Cone3DeltaEta},
      {"Cone3DeltaPhi", LHCb::Particle::additionalInfo::Cone3DeltaPhi},
      {"Cone4Angle", LHCb::Particle::additionalInfo::Cone4Angle},
      {"Cone4Mult", LHCb::Particle::additionalInfo::Cone4Mult},
      {"Cone4PX", LHCb::Particle::additionalInfo::Cone4PX},
      {"Cone4PY", LHCb::Particle::additionalInfo::Cone4PY},
      {"Cone4PZ", LHCb::Particle::additionalInfo::Cone4PZ},
      {"Cone4P", LHCb::Particle::additionalInfo::Cone4P},
      {"Cone4PT", LHCb::Particle::additionalInfo::Cone4PT},
      {"Cone4PXAsym", LHCb::Particle::additionalInfo::Cone4PXAsym},
      {"Cone4PYAsym", LHCb::Particle::additionalInfo::Cone4PYAsym},
      {"Cone4PZAsym", LHCb::Particle::additionalInfo::Cone4PZAsym},
      {"Cone4PAsym", LHCb::Particle::additionalInfo::Cone4PAsym},
      {"Cone4PTAsym", LHCb::Particle::additionalInfo::Cone4PTAsym},
      {"Cone4DeltaEta", LHCb::Particle::additionalInfo::Cone4DeltaEta},
      {"Cone4DeltaPhi", LHCb::Particle::additionalInfo::Cone4DeltaPhi},
      {"EWCone1Index", LHCb::Particle::additionalInfo::EWCone1Index},
      {"EWCone2Index", LHCb::Particle::additionalInfo::EWCone2Index},
      {"EWCone3Index", LHCb::Particle::additionalInfo::EWCone3Index},
      {"EWCone4Index", LHCb::Particle::additionalInfo::EWCone4Index},
      {"FirstJetIndex", LHCb::Particle::additionalInfo::FirstJetIndex},
      {"JetActiveArea", LHCb::Particle::additionalInfo::JetActiveArea},
      {"JetActiveAreaError", LHCb::Particle::additionalInfo::JetActiveAreaError},
      {"JetActiveAreaPx", LHCb::Particle::additionalInfo::JetActiveAreaPx},
      {"JetActiveAreaPy", LHCb::Particle::additionalInfo::JetActiveAreaPy},
      {"JetActiveAreaPz", LHCb::Particle::additionalInfo::JetActiveAreaPz},
      {"JetActiveAreaE", LHCb::Particle::additionalInfo::JetActiveAreaE},
      {"JetPtPerUnitArea", LHCb::Particle::additionalInfo::JetPtPerUnitArea},
      {"LastJetIndex", LHCb::Particle::additionalInfo::LastJetIndex},
      {"LastGlobal", LHCb::Particle::additionalInfo::LastGlobal}};
}

const GaudiUtils::VectorMap<std::string, LHCb::Particle::additionalInfo>& LHCb::Particle::s_additionalInfoTypMap() {
  return s_additionalInfoTypMap_m;
}
