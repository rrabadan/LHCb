/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

// Event
#include "Event/MCVertex.h"

//-----------------------------------------------------------------------------
// Implementation file for class : MCVertex
//
// 2012-11-20 : Chris Jones
//-----------------------------------------------------------------------------

std::ostream& LHCb::MCVertex::fillStream( std::ostream& s ) const {
  s << "{ Position = " << position() << " Time = " << time() << " Type = " << type() << " Mother = " << mother()
    << " Products : #=" << products().size() << " [";
  for ( const auto& p : products() ) { s << " " << p.target(); }
  s << " ] }";
  return s;
}

namespace LHCb {
  std::ostream& operator<<( std::ostream& s, MCVertex::MCVertexType e ) {
    switch ( e ) {
    case MCVertex::Unknown:
      return s << "Unknown";
    case MCVertex::ppCollision:
      return s << "ppCollision";
    case MCVertex::DecayVertex:
      return s << "DecayVertex";
    case MCVertex::OscillatedAndDecay:
      return s << "OscillatedAndDecay";
    case MCVertex::StringFragmentation:
      return s << "StringFragmentation";
    case MCVertex::HadronicInteraction:
      return s << "HadronicInteraction";
    case MCVertex::Bremsstrahlung:
      return s << "Bremsstrahlung";
    case MCVertex::PairProduction:
      return s << "PairProduction";
    case MCVertex::Compton:
      return s << "Compton";
    case MCVertex::DeltaRay:
      return s << "DeltaRay";
    case MCVertex::PhotoElectric:
      return s << "PhotoElectric";
    case MCVertex::Annihilation:
      return s << "Annihilation";
    case MCVertex::RICHPhotoElectric:
      return s << "RICHPhotoElectric";
    case MCVertex::Cerenkov:
      return s << "Cerenkov";
    case MCVertex::RichHpdBackScat:
      return s << "RichHpdBackScat";
    case MCVertex::GenericInteraction:
      return s << "GenericInteraction";
    case MCVertex::LHCHalo:
      return s << "LHCHalo";
    case MCVertex::MuonBackground:
      return s << "MuonBackground";
    case MCVertex::MuonBackgroundFlat:
      return s << "MuonBackgroundFlat";
    case MCVertex::MuonBackgroundSpillover:
      return s << "MuonBackgroundSpillover";
    case MCVertex::WorldLimit:
      return s << "WorldLimit";
    case MCVertex::KinematicLimit:
      return s << "KinematicLimit";
    default:
      return s << "ERROR wrong value " << int( e ) << " for enum LHCb::MCVertex::MCVertexType";
    }
  }
} // namespace LHCb
