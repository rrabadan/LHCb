/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef LINKER_LINKEDTO_H
#define LINKER_LINKEDTO_H 1

#include "Event/LinksByKey.h"
#include "GaudiKernel/ContainedObject.h"
#include "GaudiKernel/GaudiException.h"
#include "GaudiKernel/IRegistry.h"
#include "GaudiKernel/LinkManager.h"
#include "GaudiKernel/ObjectContainerBase.h"
#include "GaudiKernel/SmartDataPtr.h"
#include "fmt/format.h"
#include <iterator>

/** @class LinkedTo LinkedTo.h Linker/LinkedTo.h
 *  Templated interface to the LinksByKey, direct version
 *
 *  @author Olivier Callot
 *  @date   2004-01-06
 */

template <class TARGET>
class LinkedTo final {

  static const TARGET* target( LHCb::LinksByKey const* links, LHCb::LinkReference const& ref ) {
    const LinkManager::Link* link = links->linkMgr()->link( ref.linkID() );
    if ( !link->object() ) {
      SmartDataPtr<DataObject> tmp( links->registry()->dataSvc(), link->path() );
      const_cast<LinkManager::Link*>( link )->setObject( tmp );
      if ( !tmp ) return nullptr;
    }
    const ObjectContainerBase* parent = dynamic_cast<const ObjectContainerBase*>( link->object() );
    return parent ? static_cast<TARGET const*>( parent->containedObject( ref.objectKey() ) ) : nullptr;
  }

  enum struct Weighted { yes, no };

  template <Weighted weighted>
  class ToRange {
    friend LinkedTo;
    class Iterator {
      TARGET const*           m_current = nullptr;
      LHCb::LinkReference     m_curReference{0, 0, 0, -1, 0};
      LHCb::LinksByKey const* m_links = nullptr;

      friend ToRange;
      Iterator() = default; // only used for `end()`; C++20: replace by dedicated Sentinel type...
      Iterator( LHCb::LinksByKey const* links, LHCb::LinkReference ref, TARGET const* current )
          : m_current{current}, m_curReference{std::move( ref )}, m_links{links} {}

    public:
      using difference_type = int;
      using value_type      = void;
      using pointer         = void;
      using reference = std::conditional_t<weighted == Weighted::yes, std::tuple<TARGET const&, float>, TARGET const&>;
      using iterator_category = std::forward_iterator_tag;

      bool      operator==( Iterator const& other ) const { return m_current == other.m_current; }
      bool      operator!=( Iterator const& other ) const { return !( *this == other ); }
      Iterator& operator++() {
        if ( m_current ) {
          bool status = m_links->nextReference( m_curReference );
          if ( status )
            m_current = target( m_links, m_curReference );
          else {
            m_current = nullptr;
            m_curReference.setNextIndex( -1 );
          }
        }
        return *this;
      }
      reference operator*() const {
        if constexpr ( weighted == Weighted::yes ) {
          return {*m_current, m_curReference.weight()};
        } else {
          return *m_current;
        }
      }
    };

    Iterator m_begin;

    ToRange( LHCb::LinksByKey const* links, ContainedObject const* source ) {
      TARGET const*       current = nullptr;
      LHCb::LinkReference curReference{0, 0, 0, -1, 0};
      bool status = links && source && links->firstReference( source->index(), source->parent(), curReference );
      if ( status ) {
        current = target( links, curReference );
      } else {
        curReference.setNextIndex( -1 );
      }
      m_begin = Iterator{links, curReference, current};
    }
    ToRange( LHCb::LinksByKey const* links, int key ) {
      TARGET const*       current = nullptr;
      LHCb::LinkReference curReference{0, 0, 0, -1, 0};
      bool                status = links && links->firstReference( key, nullptr, curReference );
      if ( status ) {
        current = target( links, curReference );
      } else {
        curReference.setNextIndex( -1 );
      }
      m_begin = Iterator{links, curReference, current};
    }

  public:
    Iterator      begin() const { return m_begin; }
    Iterator      end() const { return Iterator{}; }
    int           size() const { return std::distance( begin(), end() ); }
    bool          empty() const { return !m_begin.m_current; }
    TARGET const& front() const { return *m_begin.m_current; }
    TARGET const* try_front() const { return m_begin.m_current; }
  };

public:
  LinkedTo( LHCb::LinksByKey const* links ) : m_links{links} { check_requirements(); }

  operator bool() const { return m_links != nullptr; }

  auto size() const { return m_links->keyIndex().size(); }

  /** returns a range of targets
   */
  template <typename SOURCE>
  auto range( const SOURCE* source ) const {
    check_source( SOURCE::classID() );
    return ToRange<Weighted::no>{m_links, source};
  }
  template <typename SOURCE>
  auto weightedRange( const SOURCE* source ) const {
    check_source( SOURCE::classID() );
    return ToRange<Weighted::yes>{m_links, source};
  }

  /** returns a range of targets
   */
  auto range( int key ) const { return ToRange<Weighted::no>{m_links, key}; }
  auto weightedRange( int key ) const { return ToRange<Weighted::yes>{m_links, key}; }

private:
  void check_requirements() const {
    if ( !m_links ) return; // FIXME: this ought to be an error... but it is not for 'backwards compatibility'...
    if ( !m_links->registry() ) {
      throw GaudiException( "LinksByKey not registered in event store", "LinkedTo", StatusCode::FAILURE );
    }
    if ( !m_links->registry()->dataSvc() ) {
      throw GaudiException( "LinksByKey could not obtain dataSvc", "LinkedTo", StatusCode::FAILURE );
    }
    //== Check proper template, only if specified.
    if ( m_links->targetClassID() != TARGET::classID() ) {
      throw GaudiException(
          fmt::format( "Incompatible TARGET type for location {} : Template classID is {} expected {}",
                       m_links->registry()->identifier(), TARGET::classID(), m_links->targetClassID() ),
          "LinkedTo", StatusCode::FAILURE );
    }
  }
  void check_source( const CLID& source_classid ) const {
    if ( !m_links ) return; // FIXME: this ought to be an error... but it is not for 'backwards compatibility'...
    if ( m_links->sourceClassID() != source_classid && CLID_ContainedObject != source_classid ) {
      throw GaudiException( fmt::format( "Incompatible SOURCE type for location {}: Template classID is {} expected {}",
                                         m_links->registry()->identifier(), source_classid, m_links->sourceClassID() ),
                            "LinkedTo", StatusCode::FAILURE );
    }
  }

  LHCb::LinksByKey const* m_links = nullptr;
};

#endif // LINKER_LINKEDTO_H
