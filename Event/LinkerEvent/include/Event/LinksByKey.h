/*****************************************************************************\
* (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once
#include "Event/LinkReference.h"
#include "GaudiKernel/DataObject.h"
#include "GaudiKernel/GaudiException.h"
#include "GaudiKernel/IRegistry.h"
#include "GaudiKernel/KeyedObject.h"
#include "Kernel/STLExtensions.h"
#include <fmt/format.h>
#include <functional>
#include <ostream>
#include <utility>
#include <vector>

// Forward declarations
class IDataProviderSvc;

namespace LHCb {

  // Forward declarations
  using GaudiUtils::operator<<;

  // Class ID definition
  static const CLID CLID_LinksByKey = 400;

  /** @class LinksByKey LinksByKey.h
   *
   * Link of an object to Keyed objects
   *
   * @author OlivierCallot
   * created Fri Mar 22 11:58:15 2019
   *
   */

  class LinksByKey final : public DataObject {
  public:
    enum struct Order { increasingWeight, decreasingWeight };

    static std::string linkerName( const std::string& containerName ) {
      return "Link/" + ( "/Event/" == containerName.substr( 0, 7 ) ? containerName.substr( 7 ) : containerName );
    }
    static std::string inverseLinkerName( std::string const& containerName ) {
      return linkerName( containerName ) + "Inv";
    }

    // [[deprecated]] // TODO: deprecate...
    template <typename SOURCE, typename TARGET>
    static LinksByKey* fetchFromOrCreateOnTES( IDataProviderSvc* evtSvc, std::string const& name,
                                               Order order = Order::decreasingWeight ) {
      return createOnTES( evtSvc, name, SOURCE::classID(), TARGET::classID(), order );
    }

    /// Default constructor, reserve space (needed for ROOT I/O streaming unfortunately)
    /* [[deprecated]] */ LinksByKey() {
      m_keyIndex.reserve( 1000 );
      m_linkReference.reserve( 1000 );
    }

    template <typename SOURCE, typename TARGET, typename = std::enable_if_t<!std::is_void_v<SOURCE>>>
    LinksByKey( std::in_place_type_t<SOURCE>, std::in_place_type_t<TARGET>, Order order = Order::decreasingWeight )
        : LinksByKey{SOURCE::classID(), TARGET::classID(), order} {}

    template <typename TARGET>
    LinksByKey( std::in_place_type_t<void>, std::in_place_type_t<TARGET>, Order order = Order::decreasingWeight )
        : LinksByKey{0, TARGET::classID(), order} {}

    // Retrieve pointer to class definition structure
    const CLID&        clID() const override { return LHCb::LinksByKey::classID(); }
    static const CLID& classID() { return CLID_LinksByKey; }

    /// Fill the ASCII output stream
    std::ostream& fillStream( std::ostream& s ) const override;

    /// Return the order of the references
    Order order() const { return m_increasing ? Order::increasingWeight : Order::decreasingWeight; }

    /// Get the pointers to the linked containers
    void resolveLinks( IDataProviderSvc* eventSvc );

    template <typename SOURCE, typename TARGET>
    void link( const SOURCE* source, const TARGET* dest, double weight = 1. ) {
      requireTargetID( TARGET::classID() );
      requireSourceID( SOURCE::classID() );
      if ( !source || !dest ) return;
      addReference( source->index(), linkID( source->parent() ), dest->index(), linkID( dest->parent() ), weight );
    }

    template <typename SOURCE, typename TARGETRANGE>
    auto link( const SOURCE* source, TARGETRANGE&& range ) -> std::void_t<decltype( *begin( range ) )> {
      using std::begin;
      using TARGET = std::remove_pointer_t<std::tuple_element_t<0, std::decay_t<decltype( *begin( range ) )>>>;
      requireSourceID( SOURCE::classID() );
      requireTargetID( TARGET::classID() );
      if ( !source ) return;
      auto idx = source->index();
      auto id  = linkID( source->parent() );
      for ( auto const& [target, weight] : range )
        addReference( idx, id, target->index(), linkID( target->parent() ), weight );
    }

    template <typename TARGET>
    void link( int key, const TARGET* dest, double weight = 1. ) {
      requireTargetID( TARGET::classID() );
      if ( !dest ) return;
      addReference( key, -1, dest->index(), linkID( dest->parent() ), weight );
    }

    template <typename TARGETRANGE>
    auto link( int key, TARGETRANGE&& range ) -> std::void_t<decltype( *begin( range ) )> {
      using std::begin;
      using TARGET = std::remove_pointer_t<std::tuple_element_t<0, std::decay_t<decltype( *begin( range ) )>>>;
      requireTargetID( TARGET::classID() );
      for ( auto const& [target, weight] : range )
        addReference( key, -1, target->index(), linkID( target->parent() ), weight );
    }

    /// Retrieve the first LinkReference for this key
    bool firstReference( int key, const DataObject* container, LHCb::LinkReference& reference ) const;

    /// Retrieve the next LinkReference for this key
    bool nextReference( LHCb::LinkReference& reference ) const;

    /// Checks whether a given object has any link
    bool hasEntry( const KeyedObject<int>& obj ) const;

    /// Retrieve the first key for this LinkReference
    int firstSource( LHCb::LinkReference& reference, std::vector<std::pair<int, int>>::const_iterator& iter ) const;

    /// Retrieve the next key for this LinkReference
    int nextSource( LHCb::LinkReference& reference, std::vector<std::pair<int, int>>::const_iterator& iter ) const;

    /// Reset the content of the objects
    void reset() {
      m_keyIndex.clear();
      m_linkReference.clear();
    }

    /// Retrieve const  List of linked objects
    const std::vector<std::pair<int, int>>& keyIndex() const { return m_keyIndex; }

    /// Retrieve const  List of references
    const std::vector<LHCb::LinkReference>& linkReference() const { return m_linkReference; }

    /// Retrieve const  Class ID of the source of the Link
    unsigned int sourceClassID() const { return m_sourceClassID; }

    void requireSourceID( const CLID& sourceID ) const {
      if ( sourceClassID() != sourceID ) {
        throw GaudiException(
            fmt::format( "Incompatible SOURCE type for location {} : requested classID is : {} expected {}", location(),
                         sourceID, sourceClassID() ),
            "LinksByKey", StatusCode::FAILURE );
      }
    }

    /// Retrieve const  Class ID of the target of the Link
    unsigned int targetClassID() const { return m_targetClassID; }

    void requireTargetID( const CLID& targetID ) const {
      if ( targetClassID() != targetID ) {
        throw GaudiException(
            fmt::format( "Incompatible TARGET type for location {} : Request classID is {} expected {}", location(),
                         targetID, targetClassID() ),
            "LinksByKey", StatusCode::FAILURE );
      }
    }

    friend std::ostream& operator<<( std::ostream& str, const LinksByKey& obj ) { return obj.fillStream( str ); }

    /**
     * applies a given function to all links of the LinksByKey object
     * Function must have the following signature :
     *    (unsigned int srcIndex, unsigned int tgtIndex, float weight)
     * where
     *   - srcindex is the index of the source item in the source container
     *   - tgtindex is the index of the target item in the target container
     *   - weight is the weight of the current link
     */
    template <typename Function>
    void applyToAllLinks( Function&& func ) const {
      for ( auto [srcIndex, refIndex] : m_keyIndex ) { internalApply( srcIndex, refIndex, func ); }
    }

    /**
     * applies a given function to all links related to given source
     * Function must have the following signature :
     *    (unsigned int srcIndex, unsigned int tgtIndex, float weight)
     * where
     *   - srcindex is the index of the source item in the source container
     *   - tgtindex is the index of the target item in the target container
     *   - weight is the weight of the current link
     */
    template <typename Function>
    void applyToLinks( unsigned int srcIndex, Function&& func ) const {
      if ( int key; findIndex( srcIndex, key ) ) { internalApply( srcIndex, m_keyIndex[key].second, func ); }
    }

  private:
    static LinksByKey* createOnTES( IDataProviderSvc*, std::string const&, const CLID&, const CLID&, Order );

    LinksByKey( const CLID& sourceID, const CLID& targetID, Order order )
        : m_increasing{order == Order::increasingWeight}, m_sourceClassID{sourceID}, m_targetClassID{targetID} {
      m_keyIndex.reserve( 1000 );
      m_linkReference.reserve( 1000 );
    }

    /// Returns the LinkManager linkID for this data object (Container)
    int linkID( const DataObject* obj );

  public:
    /// Add a reference to the specified key
    void addReference( int srcKey, int srcLinkID, int destKey, int destLinkID, double weight = 1. );

  private:
    /// Returns the index of the key in m_keyIndex. True if key exist, else inserting position
    bool findIndex( int key, int& index ) const;

    /**
     * Internal helper function applying the given function to all links of the
     * chain given by refIndex
     */
    template <typename Function>
    void internalApply( unsigned int srcIndex, int refIndex, Function&& func ) const;

    std::string location() const { return registry() ? registry()->identifier() : "UnRegistered"; }

  private:
    bool                             m_increasing = false; ///< Type of ordering
    std::vector<std::pair<int, int>> m_keyIndex;           ///< List of linked objects
    std::vector<LHCb::LinkReference> m_linkReference;      ///< List of references
    unsigned int                     m_sourceClassID = 0;  ///< Class ID of the source of the Link
    unsigned int                     m_targetClassID = 0;  ///< Class ID of the target of the Link

  }; // class LinksByKey

} // namespace LHCb

// -----------------------------------------------------------------------------
// end of class
// -----------------------------------------------------------------------------

inline std::ostream& LHCb::LinksByKey::fillStream( std::ostream& s ) const {
  return s << "{ "
           << "increasing :	" << ( m_increasing ? 'T' : 'F' ) << "\nkeyIndex :	" << m_keyIndex
           << "\nlinkReference :	" << m_linkReference << "\nsourceClassID :	" << m_sourceClassID
           << "\ntargetClassID :	" << m_targetClassID << "\n }";
}

template <typename Function>
void LHCb::LinksByKey::internalApply( unsigned int srcIndex, int refIndex, Function&& func ) const {
  while ( refIndex >= 0 ) {
    auto& reference = m_linkReference[refIndex];
    std::invoke( func, srcIndex, (unsigned int)reference.objectKey(), reference.weight() );
    refIndex = reference.nextIndex();
  }
}
