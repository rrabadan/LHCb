/*****************************************************************************\
* (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once

#include "Event/SOACollection.h"

#include <map>
#include <tuple>

namespace LHCb::Event {

  namespace RelationTag {
    struct Index1 : int_field {};
    struct Index2 : int_field {};
    struct IndicesRel : vector_field<int_field> {};
    struct NumRelations : int_field {};
  } // namespace RelationTag

  template <typename C1, typename... ExtraTags>
  struct RelationTable1D : SOACollection<RelationTable1D<C1, ExtraTags...>, RelationTag::Index1, ExtraTags...> {
    using base_t = SOACollection<RelationTable1D<C1, ExtraTags...>, RelationTag::Index1, ExtraTags...>;

    RelationTable1D( const C1* c1 ) : m_from( c1 ) {}

    // Special constructor for zipping machinery
    RelationTable1D( Zipping::ZipFamilyNumber zipIdentifier, const RelationTable1D& other )
        : base_t{std::move( zipIdentifier ), other}, m_from{other.m_from} {}

    template <SIMDWrapper::InstructionSet simd, LHCb::Pr::ProxyBehaviour behaviour, typename ContainerType>
    struct RelationTable1DProxy : Proxy<simd, behaviour, ContainerType> {
      using base_t = typename Event::Proxy<simd, behaviour, ContainerType>;
      using base_t::base_t;
      using base_t::loop_mask;
      using simd_t = SIMDWrapper::type_map_t<simd>;
      using int_v  = typename simd_t::int_v;

      template <typename... Ts>
      void set( int_v idxFrom, Ts... extras ) const {
        this->template field<RelationTag::Index1>().set( idxFrom );
        static_assert( sizeof...( ExtraTags ) == sizeof...( Ts ),
                       "extras arguments size in set should match ExtraTags" );
        ( this->template field<ExtraTags>().set( extras ), ... );
      }

      [[nodiscard]] auto fromIndex() const { return this->template get<RelationTag::Index1>(); }
      [[nodiscard]] auto from() const {
        return this->container()->from()->template simd<simd>().gather( this->fromIndex(), this->loop_mask() );
      }
    };

    template <SIMDWrapper::InstructionSet simd, LHCb::Pr::ProxyBehaviour behaviour, typename ContainerType>
    using proxy_type = RelationTable1DProxy<simd, behaviour, ContainerType>;

    const C1* from() const { return m_from; }

    // SOACollection to represent a view of the relation that is zippable with the 'from' container
    template <typename C1v, typename R>
    struct RelationTable1DView
        : SOACollection<RelationTable1DView<C1v, R>, RelationTag::Index1, RelationTag::NumRelations> {
      using base_t = SOACollection<RelationTable1DView<C1v, R>, RelationTag::Index1, RelationTag::NumRelations>;

      RelationTable1DView( const C1v* c1, const R* r ) : base_t( c1->zipIdentifier() ), m_from( c1 ), m_rel( r ) {}

      // Special constructor for zipping machinery
      RelationTable1DView( Zipping::ZipFamilyNumber zipIdentifier, const RelationTable1DView& other )
          : base_t{std::move( zipIdentifier ), other}, m_from{other.m_from}, m_rel{other.m_rel} {}

      template <SIMDWrapper::InstructionSet simd, LHCb::Pr::ProxyBehaviour behaviour, typename ContainerType>
      struct RelationTable1DViewProxy : Proxy<simd, behaviour, ContainerType> {
        using base_t = typename Event::Proxy<simd, behaviour, ContainerType>;
        using base_t::base_t;
        [[nodiscard]] auto hasRelation() const { return this->template get<RelationTag::NumRelations>() > 0; }
        [[nodiscard]] auto relationIndex() const { return this->template get<RelationTag::Index1>(); }
        [[nodiscard]] auto relation() const {
          return this->container()->relations()->template simd<simd>().gather(
              this->relationIndex(), this->loop_mask() && this->hasRelation() );
        }
        /**
         * @brief Get values of ExtraTags("weight") for each relation.
         *
         * @return std::tuple containing the relation for each ExtraTag. If there's no relation, the default
         * initialised value of the ExtraTag type is returned, typically 0.
         */
        [[nodiscard]] auto weight() const {
          return std::make_tuple( [this] {
            using ret_t = decltype( relation().template get<ExtraTags>() );
            return select( this->loop_mask() && hasRelation(), relation().template get<ExtraTags>(), ret_t{} );
          }()... );
        }
      };

      template <SIMDWrapper::InstructionSet simd, LHCb::Pr::ProxyBehaviour behaviour, typename ContainerType>
      using proxy_type = RelationTable1DViewProxy<simd, behaviour, ContainerType>;

      const C1v* from() const { return m_from; }
      const R*   relations() const { return m_rel; }

    private:
      const C1v* m_from;
      const R*   m_rel;
    };

    template <typename Rel>
    using view_t = RelationTable1DView<C1, Rel>;

    [[nodiscard]] auto buildView() const {
      using rel_t = const RelationTable1D<C1, ExtraTags...>;
      RelationTable1DView<C1, rel_t> view{m_from, this};
      view.resize( m_from->size() );
      for ( auto const& v : view.simd() ) { v.template field<RelationTag::NumRelations>().set( 0 ); }
      for ( auto const& rel : this->scalar() ) {
        const int i       = rel.template get<RelationTag::Index1>().cast();
        auto      viewRel = view.scalar()[i];
        viewRel.template field<RelationTag::Index1>().set( rel.indices() );
        viewRel.template field<RelationTag::NumRelations>().set( 1 );
      }
      return view;
    }

    template <SIMDWrapper::InstructionSet simd, LHCb::Pr::ProxyBehaviour behaviour, typename... Ts>
    void add( const typename C1::template proxy_type<simd, behaviour, const C1>& from_proxy, Ts... extras ) {
      this->template emplace_back<simd>().set( from_proxy.indices(), extras... );
    }
    template <SIMDWrapper::InstructionSet simd, LHCb::Pr::ProxyBehaviour behaviour, typename... Ts>

    void addUniquely( const typename C1::template proxy_type<simd, behaviour, const C1>& from_proxy, Ts... extras ) {
      for ( const auto& existing_entry : this->scalar() ) {
        // Check if the existing entry matches the data you want to add
        if ( existing_entry.template get<RelationTag::Index1>() == from_proxy.indices() ) {
          // Entry with the same index already exists. This function could be updated to consider information in extra
          // tags
          return; // Skip adding since it already exists
        }
      }

      // If no matching entry is found, add the new entry
      this->template emplace_back<simd>().set( from_proxy.indices(), extras... );
    }

  private:
    const C1* m_from;
  };

  template <typename C1, typename C2, typename... ExtraTags>
  struct RelationTable2D
      : SOACollection<RelationTable2D<C1, C2, ExtraTags...>, RelationTag::Index1, RelationTag::Index2, ExtraTags...> {
    using base_t =
        SOACollection<RelationTable2D<C1, C2, ExtraTags...>, RelationTag::Index1, RelationTag::Index2, ExtraTags...>;

    RelationTable2D( const C1* c1, const C2* c2 ) : m_from( c1 ), m_to( c2 ) {}

    // Special constructor for zipping machinery
    RelationTable2D( Zipping::ZipFamilyNumber zipIdentifier, const RelationTable2D& other )
        : base_t{std::move( zipIdentifier ), other}, m_from{other.m_from}, m_to{other.m_to} {}

    template <SIMDWrapper::InstructionSet simd, LHCb::Pr::ProxyBehaviour behaviour, typename ContainerType>
    struct RelationTable2DProxy : Proxy<simd, behaviour, ContainerType> {
      using base_t = typename Event::Proxy<simd, behaviour, ContainerType>;
      using base_t::base_t;
      using base_t::loop_mask;
      using simd_t = SIMDWrapper::type_map_t<simd>;
      using int_v  = typename simd_t::int_v;

      template <typename... Ts>
      void set( int_v idxFrom, int_v idxTo, Ts... extras ) const {
        this->template field<RelationTag::Index1>().set( idxFrom );
        this->template field<RelationTag::Index2>().set( idxTo );
        static_assert( sizeof...( ExtraTags ) == sizeof...( Ts ),
                       "extras arguments size in set should match ExtraTags" );
        ( this->template field<ExtraTags>().set( extras ), ... );
      }

      [[nodiscard]] auto fromIndex() const { return this->template get<RelationTag::Index1>(); }
      [[nodiscard]] auto toIndex() const { return this->template get<RelationTag::Index2>(); }
      [[nodiscard]] auto from() const {
        return this->container()->from()->template simd<simd>().gather( this->fromIndex(), this->loop_mask() );
      }
      [[nodiscard]] auto to() const {
        return this->container()->to()->template simd<simd>().gather( this->toIndex(), this->loop_mask() );
      }
    };

    template <SIMDWrapper::InstructionSet simd, LHCb::Pr::ProxyBehaviour behaviour, typename ContainerType>
    using proxy_type = RelationTable2DProxy<simd, behaviour, ContainerType>;

    const C1* from() const { return m_from; }
    const C2* to() const { return m_to; }

    // SOACollection to represent an oriented relation, ie. zippable with the 'from' container
    template <typename C1v, typename C2v, typename R>
    struct RelationTable2DView : SOACollection<RelationTable2DView<C1v, C2v, R>, RelationTag::IndicesRel> {
      using base_t = SOACollection<RelationTable2DView<C1v, C2v, R>, RelationTag::IndicesRel>;

      RelationTable2DView( const C1v* c1, const C2v* c2, const R* r )
          : base_t( c1->zipIdentifier() ), m_from( c1 ), m_to( c2 ), m_rel( r ) {}

      // Special constructor for zipping machinery
      RelationTable2DView( Zipping::ZipFamilyNumber zipIdentifier, const RelationTable2DView& other )
          : base_t{std::move( zipIdentifier ), other}, m_from{other.m_from}, m_to{other.m_to}, m_rel{other.m_rel} {}

      template <SIMDWrapper::InstructionSet simd, LHCb::Pr::ProxyBehaviour behaviour, typename ContainerType>
      struct RelationTable2DViewProxy : Proxy<simd, behaviour, ContainerType> {
        using base_t = typename Event::Proxy<simd, behaviour, ContainerType>;
        using base_t::base_t;
        [[nodiscard]] auto relations() const { return this->template field<RelationTag::IndicesRel>(); }
        [[nodiscard]] auto numRelations() const { return relations().size(); }
        [[nodiscard]] auto hasRelation( std::size_t i ) const { return numRelations() > i; }
        [[nodiscard]] auto relationIndex( std::size_t i ) const { return relations()[i].get(); }
        [[nodiscard]] auto relation( std::size_t i ) const {
          return this->container()->relations()->template simd<simd>().gather(
              this->relationIndex( i ), this->loop_mask() && this->hasRelation( i ) );
        }
        /**
         * @brief Get values of ExtraTags("weight") for each relation.
         *
         * @return std::tuple containing a vector of relations for each ExtraTag. If there's no relation,
         * the default initialised value of the ExtraTag type is returned, typically 0.
         */
        [[nodiscard]] auto weight() const {
          return std::make_tuple( [this] {
            using ret_t = decltype( relation( 0 ).template get<ExtraTags>() );
            std::vector<ret_t> rels{};
            const auto         nRels = numRelations().hmax( this->loop_mask() );
            rels.reserve( nRels );
            for ( auto i = 0; i < nRels; ++i ) {
              rels.emplace_back(
                  select( this->loop_mask() && hasRelation( i ), relation( i ).template get<ExtraTags>(), ret_t{} ) );
            }
            return rels;
          }()... );
        }
      };

      template <SIMDWrapper::InstructionSet simd, LHCb::Pr::ProxyBehaviour behaviour, typename ContainerType>
      using proxy_type = RelationTable2DViewProxy<simd, behaviour, ContainerType>;

      const C1v* from() const { return m_from; }
      const C2v* to() const { return m_to; }
      const R*   relations() const { return m_rel; }

    private:
      const C1v* m_from;
      const C2v* m_to;
      const R*   m_rel;
    };

    template <typename Rel>
    using view_t = RelationTable2DView<C1, C2, Rel>;

    [[nodiscard]] auto buildFromView() const { return buildViewHelper<RelationTag::Index1>( m_from, m_to ); }
    [[nodiscard]] auto buildToView() const { return buildViewHelper<RelationTag::Index2>( m_to, m_from ); }

    template <SIMDWrapper::InstructionSet simd, LHCb::Pr::ProxyBehaviour bhv1, LHCb::Pr::ProxyBehaviour bhv2,
              typename... Ts>
    void add( const typename C1::template proxy_type<simd, bhv1, const C1>& from_proxy,
              const typename C2::template proxy_type<simd, bhv2, const C2>& to_proxy, Ts... extras ) {
      this->template emplace_back<simd>().set( from_proxy.indices(), to_proxy.indices(), extras... );
    }

  private:
    template <typename Tag1, typename C1v, typename C2v>
    [[nodiscard]] auto buildViewHelper( const C1v* from, const C2v* to ) const {
      using rel_t = const RelationTable2D<C1, C2, ExtraTags...>;
      RelationTable2DView<C1v, C2v, rel_t> view{from, to, this};
      view.resize( from->size() );
      for ( auto const& v : view.simd() ) { v.relations().clear(); }
      // Histogram-like algorithms don't play nice with simd,
      // scatter operations adds an other level of complexity
      // because conflicts need to be resolved (if multiple
      // elements vote in the same array, they must be serialized)
      // So this is implemented as scalar:
      for ( auto const& rel : this->scalar() ) {
        const int i         = rel.template get<Tag1>().cast();
        auto      viewRel   = view.scalar()[i];
        auto      relations = viewRel.relations();
        auto      nRel      = relations.size();

        relations.resize( nRel + 1 ); // resize if needed
        relations[nRel.cast()].set( rel.indices() );
      }
      return view;
    }

    const C1* m_from;
    const C2* m_to;
  };

  template <typename ContainerV1>
  struct V3ToV1Mapping {
    V3ToV1Mapping( const ContainerV1* c2 ) : m_to( c2 ) {}

    void reserve( std::size_t size ) { m_indices.reserve( size ); }

    void add( int index_v3, int index_v1 ) { m_indices[index_v3] = index_v1; }

    auto get( int index_v3 ) const { return ( *m_to )( m_indices.at( index_v3 ) ); }

  private:
    const ContainerV1*           m_to;
    std::unordered_map<int, int> m_indices;
  };

} // namespace LHCb::Event
