###############################################################################
# (c) Copyright 2000-2021 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
#[=======================================================================[.rst:
Event/LinkerEvent
-----------------
#]=======================================================================]

gaudi_add_library(LinkerEvent
    SOURCES
        src/LinksByKey.cpp
    LINK
        PUBLIC
            LHCb::EventBase
            LHCb::LHCbKernel
            LHCb::PhysEvent
)

gaudi_add_dictionary(LinkerEventDict
    HEADERFILES dict/lcgDict.h
    SELECTION dict/lcg_selection.xml
    LINK LHCb::LinkerEvent
)

foreach(test IN ITEMS test_SOARelations)
    gaudi_add_executable(${test}
        SOURCES tests/src/${test}.cpp
        LINK 
            Boost::unit_test_framework
            LHCb::EventBase
            LHCb::LinkerEvent
        TEST
    )
endforeach()
