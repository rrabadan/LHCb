/*****************************************************************************\
* (c) Copyright 2000-2021 CERN for the benefit of the LHCb Collaboration      *
\*****************************************************************************/
#include <Event/ODIN.h>

#ifndef ODIN_WITHOUT_GAUDI
namespace LHCb::ODINImplementation::v7 {
#else
namespace LHCb::ODINImplementation::v7_standalone {
#endif
  template <>
  ODIN ODIN::from_version<6>( LHCb::span<const std::uint32_t> buffer ) {
    using LHCb::ODINImplementation::details::get_bits;

    // Check size of the input buffer
    if ( buffer.size() != 10 ) {
      std::string msg =
          "Invalid buffer size for ODIN bank version 6: got " + std::to_string( buffer.size() ) + ", expected 10";
#ifndef ODIN_WITHOUT_GAUDI
      throw GaudiException( msg, "LHCb::ODIN::from_version", StatusCode::FAILURE );
#else
      throw std::runtime_error{msg};
#endif
    }

    ODIN odin;

    // Map ODIN bank v6 layout to v7
    odin.data[0] = buffer[0]; // run number
    odin.data[1] = buffer[1]; // event type + calibration step
    odin.data[7] = buffer[2]; // orbit number
    odin.data[9] = buffer[3]; // event number (hi)
    odin.data[8] = buffer[4]; // event number (lo)
    odin.data[3] = buffer[5]; // gps time (hi)
    odin.data[2] = buffer[6]; // gps time (lo)
    odin.data[4] = buffer[9]; // TCK
    // error bits + detector status (dropped): buffer[7]
    // misc fields (buffer[8], ignoring bits 15, 21, 24-31)
    odin.setBunchId( get_bits<12, 8 * 32 + 0>( buffer ) );
    odin.setTimeAlignmentEventIndex( get_bits<3, 8 * 32 + 12>( buffer ) );
    odin.setTriggerType( get_bits<3, 8 * 32 + 16>( buffer ) );
    // calibration type changed from value (x) to bitmask (1 << x)
    odin.setCalibrationType( 1 << get_bits<2, 8 * 32 + 19>( buffer ) );
    odin.setBunchCrossingType( static_cast<ODIN::BXTypes>( get_bits<2, 8 * 32 + 22>( buffer ) ) );

    return odin;
  }

  template <>
  void ODIN::to_version<6>( LHCb::span<std::uint32_t> buffer ) const {
    using LHCb::ODINImplementation::details::set_bits;
    assert( buffer.size() == 10 );
    std::memset( buffer.data(), 0, 10 * sizeof( std::uint32_t ) );

    // Map ODIN bank v7 layout to v6
    buffer[0] = data[0]; // run number
    buffer[1] = data[1]; // event type + calibration step
    buffer[2] = data[7]; // orbit number
    buffer[3] = data[9]; // event number (hi)
    buffer[4] = data[8]; // event number (lo)
    buffer[5] = data[3]; // gps time (hi)
    buffer[6] = data[2]; // gps time (lo)
    buffer[9] = data[4]; // TCK

    set_bits<12, 8 * 32 + 0>( buffer, bunchId() );
    set_bits<3, 8 * 32 + 12>( buffer, timeAlignmentEventIndex() );
    assert( triggerType() <= 7 ); // change of width between v6 and v7
    set_bits<3, 8 * 32 + 16>( buffer, triggerType() );
    // change of width between v6 and v7
    std::uint8_t calibrationTypeV6 = 0;
    if ( calibrationTypeBit( CalibrationTypes::A ) ) {
      calibrationTypeV6 = 0;
    } else if ( calibrationTypeBit( CalibrationTypes::B ) ) {
      calibrationTypeV6 = 1;
    } else if ( calibrationTypeBit( CalibrationTypes::C ) ) {
      calibrationTypeV6 = 2;
    } else if ( calibrationTypeBit( CalibrationTypes::D ) ) {
      calibrationTypeV6 = 3;
    }
    set_bits<2, 8 * 32 + 19>( buffer, calibrationTypeV6 );
    set_bits<2, 8 * 32 + 22>( buffer, static_cast<std::uint16_t>( bunchCrossingType() ) );
  }

  std::ostream& operator<<( std::ostream& s, ODIN::CalibrationTypes e ) { return s << static_cast<int>( e ); }

  std::ostream& operator<<( std::ostream& s, ODIN::BXTypes e ) {
    switch ( e ) {
    case ODIN::BXTypes::NoBeam:
      return s << "NoBeam";
    case ODIN::BXTypes::Beam1:
      return s << "Beam1";
    case ODIN::BXTypes::Beam2:
      return s << "Beam2";
    case ODIN::BXTypes::BeamCrossing:
      return s << "BeamCrossing";
    default:
      return s << "ERROR unknown value " << static_cast<int>( e ) << " for enum LHCb::ODIN::BXTypes";
    }
  }

  std::ostream& operator<<( std::ostream& s, ODIN::TriggerTypes e ) {
    s << "TriggerType=" << static_cast<int>( e );
    switch ( e ) {
    case LHCb::ODIN::TriggerTypes::LumiTrigger:
      return s << "(LumiTrigger)";
    case LHCb::ODIN::TriggerTypes::CalibrationTrigger:
      return s << "(CalibrationTrigger)";
    default:
      return s;
    }
  }
} // namespace LHCb::ODINImplementation::v7
