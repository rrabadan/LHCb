/*****************************************************************************\
* (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once

// Include files
#include "Detector/FT/FTChannelID.h"
#include "Kernel/SiDataFunctor.h"

// This compile guard is present for gcc12 + lcg 104/105
// support for ROOT.
// See https://gitlab.cern.ch/lhcb/LHCb/-/issues/342
#ifndef __CLING__
#  include "Kernel/MultiIndexedContainer.h"
#endif

namespace LHCb {

  // Forward declarations
  // Namespace for locations in TDS
  namespace FTLiteClusterLocation {
    inline const std::string Default = "Raw/FT/LiteClusters";
  }

  /** @class FTLiteCluster FTLiteCluster.h
   *
   * Lite cluster, all information on 32 bits for fast container
   *
   * @author Olivier Callot
   *
   */

  class FTLiteCluster final {
  public:
    /// channelID type
    typedef Detector::FTChannelID chan_type;
    /// finding policy
    typedef SiDataFunctor::CompareByChannel<FTLiteCluster> findPolicy;

// This compile guard is present for gcc12 + lcg 104/105
// support for ROOT.
// See https://gitlab.cern.ch/lhcb/LHCb/-/issues/342
#ifndef __CLING__
    /// fast container of Lite Cluster
    typedef Container::MultiIndexedContainer<FTLiteCluster, 48> FTLiteClusters;
#endif

    /// Constructor
    FTLiteCluster( Detector::FTChannelID chan, int fraction, int pseudoSize );

    /// Default Constructor
    FTLiteCluster() : m_liteCluster( 0 ) {}

    /// Comparison equality
    friend bool operator==( const FTLiteCluster& lhs, const FTLiteCluster& rhs ) {
      return lhs.channelID() == rhs.channelID();
    }
    /// Comparison <
    friend bool operator<( const FTLiteCluster& lhs, const FTLiteCluster& rhs ) {
      return lhs.channelID() < rhs.channelID();
    }

    /// Comparison >
    friend bool operator>( const FTLiteCluster& lhs, const FTLiteCluster& rhs ) { return rhs < lhs; }

    /// Returns the FT channel ID
    Detector::FTChannelID channelID() const;

    /// returns the fraction of channel
    float fraction() const;

    /// returns the fraction of channel as the integer value (0 or 1)
    int fractionBit() const;

    /// Retrieve number of cells in the cluster
    unsigned int pseudoSize() const;

    // Overwrite the contents of the cluster, used for packing/unpacking
    void setLiteCluster( unsigned int liteCluster ) { m_liteCluster = liteCluster; }

    [[nodiscard]] unsigned int getLiteCluster() const { return m_liteCluster; }

    /// Print the lite cluster in a human readable way
    std::ostream& fillStream( std::ostream& s ) const;

    friend std::ostream& operator<<( std::ostream& str, const FTLiteCluster& obj ) { return obj.fillStream( str ); }

  protected:
  private:
    /// Offsets of bitfield liteCluster
    enum liteClusterBits { channelIDBits = 0, fractionBits = 20, pseudoSizeBits = 21 };

    /// Bitmasks for bitfield liteCluster
    enum liteClusterMasks { channelIDMask = 0xfffffL, fractionMask = 0x100000L, pseudoSizeMask = 0x1e00000L };

    unsigned int m_liteCluster; ///< Lite Cluster

  public:
    struct Order {
      bool operator()( const LHCb::FTLiteCluster& lhs, const LHCb::FTLiteCluster& rhs ) const {
        if ( lhs.channelID().globalQuarterIdx() != rhs.channelID().globalQuarterIdx() )
          return lhs.channelID().globalQuarterIdx() < rhs.channelID().globalQuarterIdx();

        if ( lhs.channelID().globalModuleID() != rhs.channelID().globalModuleID() ) {
          const auto quarterIdx = lhs.channelID().globalQuarterIdx();
          // DESC ASC DESC ASC etc regardless of layer
          return ( quarterIdx % 2 ) ? lhs.channelID().globalModuleID() < rhs.channelID().globalModuleID()
                                    : lhs.channelID().globalModuleID() > rhs.channelID().globalModuleID();
        }

        bool ascending_rest = true;

        if ( ( ( lhs.channelID().globalLayerIdx() % 2 ) == 0 ) )
          ascending_rest = lhs.channelID().isBottom();
        else
          ascending_rest = lhs.channelID().isTop();

        // mat ordering
        if ( lhs.channelID().globalMatID() != rhs.channelID().globalMatID() ) {
          if ( ascending_rest )
            return lhs.channelID().globalMatID() < rhs.channelID().globalMatID();
          else
            return lhs.channelID().globalMatID() > rhs.channelID().globalMatID();
        }

        if ( ascending_rest )
          return lhs.channelID() < rhs.channelID();
        else
          return lhs.channelID() > rhs.channelID();
      }
    };
  }; // class FTLiteCluster

#ifndef __CLING__
  inline std::optional<FTLiteCluster> findCluster( LHCb::Detector::FTChannelID          channelID,
                                                   const FTLiteCluster::FTLiteClusters& clusters ) {
    unsigned int quarterIdx = channelID.globalQuarterIdx();

    for ( const auto& cluster : clusters.range( quarterIdx ) ) {
      if ( cluster.channelID() == channelID ) { return cluster; }
    }

    return std::nullopt;
  };

  inline auto sortClusterContainer( FTLiteCluster::FTLiteClusters& clusters ) { return clusters.forceSort(); }
#endif

} // namespace LHCb

// -----------------------------------------------------------------------------
// end of class
// -----------------------------------------------------------------------------

// Including forward declarations

inline LHCb::FTLiteCluster::FTLiteCluster( LHCb::Detector::FTChannelID chan, int fraction, int pseudoSize ) {

  m_liteCluster = ( chan << channelIDBits ) + ( fraction << fractionBits ) + ( pseudoSize << pseudoSizeBits );
}

inline unsigned int LHCb::FTLiteCluster::pseudoSize() const {
  return (unsigned int)( ( m_liteCluster & pseudoSizeMask ) >> pseudoSizeBits );
}

inline LHCb::Detector::FTChannelID LHCb::FTLiteCluster::channelID() const {
  return LHCb::Detector::FTChannelID( ( m_liteCluster & channelIDMask ) >> channelIDBits );
}

inline float LHCb::FTLiteCluster::fraction() const {
  return 0.5 * ( ( m_liteCluster & fractionMask ) >> fractionBits );
}

inline int LHCb::FTLiteCluster::fractionBit() const { return ( m_liteCluster & fractionMask ) >> fractionBits; }

inline std::ostream& LHCb::FTLiteCluster::fillStream( std::ostream& s ) const {
  s << "{ FTLiteCluster: " << (int)channelID() << " fraction: " << fractionBit() << " pseudosize " << pseudoSize()
    << "}";

  return s;
}