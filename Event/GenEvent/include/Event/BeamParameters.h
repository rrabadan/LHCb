/*****************************************************************************\
* (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once

#include "Core/FloatComparison.h"

#include "GaudiKernel/DataObject.h"
#include "GaudiKernel/Point3DTypes.h"
#include "GaudiKernel/SystemOfUnits.h"

#include <ostream>

namespace LHCb {

  // Forward declarations

  // Class ID definition
  static const CLID CLID_BeamParameters = 200;

  // Namespace for locations in TDS
  namespace BeamParametersLocation {
    inline const std::string Default = "Gen/BeamParameters";
  }

  /** @class BeamParameters BeamParameters.h
   *
   * Parameters of the beam
   *
   * @author P. Robbe
   *
   */

  class BeamParameters final : public DataObject {
  public:
    /// Default Constructor
    BeamParameters() = default;

    // Retrieve pointer to class definition structure
    [[nodiscard]] const CLID& clID() const override;
    static const CLID&        classID();

    /// Fill the ASCII output stream
    std::ostream& fillStream( std::ostream& s ) const override;

    /// Mean number of interactions per event (nu)
    [[nodiscard]] double nu() const;

    /// emittance
    [[nodiscard]] double emittance() const;

    /// sigma X of the luminous region
    [[nodiscard]] double sigmaX() const;

    /// sigma Y of the luminous region
    [[nodiscard]] double sigmaY() const;

    /// sigma Z of the luminous region
    [[nodiscard]] double sigmaZ() const;

    /// Smearing of the angle between the beams
    [[nodiscard]] double angleSmear() const;

    /// Retrieve const  Beam energy
    [[nodiscard]] double energy() const;

    /// Update  Beam energy
    void setEnergy( double value );

    /// Retrieve const  RMS of the bunch length
    [[nodiscard]] double sigmaS() const;

    /// Update  RMS of the bunch length
    void setSigmaS( double value );

    /// Retrieve const  Normalized emittance
    [[nodiscard]] double epsilonN() const;

    /// Update  Normalized emittance
    void setEpsilonN( double value );

    /// Retrieve const  Revolution frequency
    [[nodiscard]] double revolutionFrequency() const;

    /// Retrieve const  Total cross-section
    [[nodiscard]] double totalXSec() const;

    /// Update  Total cross-section
    void setTotalXSec( double value );

    /// Retrieve const  Horizontal crossing angle
    [[nodiscard]] double horizontalCrossingAngle() const;

    /// Update  Horizontal crossing angle
    void setHorizontalCrossingAngle( double value );

    /// Retrieve const  Vertical crossing angle
    [[nodiscard]] double verticalCrossingAngle() const;

    /// Update  Vertical crossing angle
    void setVerticalCrossingAngle( double value );

    /// Retrieve const  Horizontal beamline angle
    [[nodiscard]] double horizontalBeamlineAngle() const;

    /// Update  Horizontal beamline angle
    void setHorizontalBeamlineAngle( double value );

    /// Retrieve const  Vertical beamline angle
    [[nodiscard]] double verticalBeamlineAngle() const;

    /// Update  Vertical beamline angle
    void setVerticalBeamlineAngle( double value );

    /// Retrieve const  Beta star
    [[nodiscard]] double betaStar() const;

    /// Update  Beta star
    void setBetaStar( double value );

    /// Retrieve const  Bunch spacing
    [[nodiscard]] double bunchSpacing() const;

    /// Update  Bunch spacing
    void setBunchSpacing( double value );

    /// Retrieve const  Luminous region mean position
    [[nodiscard]] const Gaudi::XYZPoint& beamSpot() const;

    /// Update  Luminous region mean position
    void setBeamSpot( const Gaudi::XYZPoint& value );

    /// Retrieve const  Luminosity
    [[nodiscard]] double luminosity() const;

    /// Update  Luminosity
    void setLuminosity( double value );

    friend std::ostream& operator<<( std::ostream& str, const BeamParameters& obj ) { return obj.fillStream( str ); }

  private:
    double          m_energy{3.5 * Gaudi::Units::TeV};                              ///< Beam energy
    double          m_sigmaS{0.0};                                                  ///< RMS of the bunch length
    double          m_epsilonN{0.0};                                                ///< Normalized emittance
    double          m_revolutionFrequency{11.245 * Gaudi::Units::kilohertz};        ///< Revolution frequency
    double          m_totalXSec{91.1 * Gaudi::Units::millibarn};                    ///< Total cross-section
    double          m_horizontalCrossingAngle{0.130 * Gaudi::Units::mrad};          ///< Horizontal crossing angle
    double          m_verticalCrossingAngle{0.130 * Gaudi::Units::mrad};            ///< Vertical crossing angle
    double          m_horizontalBeamlineAngle{0.0};                                 ///< Horizontal beamline angle
    double          m_verticalBeamlineAngle{0.0};                                   ///< Vertical beamline angle
    double          m_betaStar{3.0 * Gaudi::Units::m};                              ///< Beta star
    double          m_bunchSpacing{50. * Gaudi::Units::ns};                         ///< Bunch spacing
    Gaudi::XYZPoint m_beamSpot{0.0, 0.0, 0.0};                                      ///< Luminous region mean position
    double          m_luminosity{0.2e30 / ( Gaudi::Units::cm2 * Gaudi::Units::s )}; ///< Luminosity

  }; // class BeamParameters

} // namespace LHCb

// -----------------------------------------------------------------------------
// end of class
// -----------------------------------------------------------------------------

// Including forward declarations

inline const CLID& LHCb::BeamParameters::clID() const { return LHCb::BeamParameters::classID(); }

inline const CLID& LHCb::BeamParameters::classID() { return CLID_BeamParameters; }

inline std::ostream& LHCb::BeamParameters::fillStream( std::ostream& s ) const {
  s << "{ "
    << "energy :	" << (float)m_energy << std::endl
    << "sigmaS :	" << (float)m_sigmaS << std::endl
    << "epsilonN :	" << (float)m_epsilonN << std::endl
    << "revolutionFrequency :	" << (float)m_revolutionFrequency << std::endl
    << "totalXSec :	" << (float)m_totalXSec << std::endl
    << "horizontalCrossingAngle :	" << (float)m_horizontalCrossingAngle << std::endl
    << "verticalCrossingAngle :	" << (float)m_verticalCrossingAngle << std::endl
    << "horizontalBeamlineAngle :	" << (float)m_horizontalBeamlineAngle << std::endl
    << "verticalBeamlineAngle :	" << (float)m_verticalBeamlineAngle << std::endl
    << "betaStar :	" << (float)m_betaStar << std::endl
    << "bunchSpacing :	" << (float)m_bunchSpacing << std::endl
    << "beamSpot :	" << m_beamSpot << std::endl
    << "luminosity :	" << (float)m_luminosity << std::endl
    << " }";
  return s;
}

inline double LHCb::BeamParameters::energy() const { return m_energy; }

inline void LHCb::BeamParameters::setEnergy( double value ) { m_energy = value; }

inline double LHCb::BeamParameters::sigmaS() const { return m_sigmaS; }

inline void LHCb::BeamParameters::setSigmaS( double value ) { m_sigmaS = value; }

inline double LHCb::BeamParameters::epsilonN() const { return m_epsilonN; }

inline void LHCb::BeamParameters::setEpsilonN( double value ) { m_epsilonN = value; }

inline double LHCb::BeamParameters::revolutionFrequency() const { return m_revolutionFrequency; }

inline double LHCb::BeamParameters::totalXSec() const { return m_totalXSec; }

inline void LHCb::BeamParameters::setTotalXSec( double value ) { m_totalXSec = value; }

inline double LHCb::BeamParameters::horizontalCrossingAngle() const { return m_horizontalCrossingAngle; }

inline void LHCb::BeamParameters::setHorizontalCrossingAngle( double value ) { m_horizontalCrossingAngle = value; }

inline double LHCb::BeamParameters::verticalCrossingAngle() const { return m_verticalCrossingAngle; }

inline void LHCb::BeamParameters::setVerticalCrossingAngle( double value ) { m_verticalCrossingAngle = value; }

inline double LHCb::BeamParameters::horizontalBeamlineAngle() const { return m_horizontalBeamlineAngle; }

inline void LHCb::BeamParameters::setHorizontalBeamlineAngle( double value ) { m_horizontalBeamlineAngle = value; }

inline double LHCb::BeamParameters::verticalBeamlineAngle() const { return m_verticalBeamlineAngle; }

inline void LHCb::BeamParameters::setVerticalBeamlineAngle( double value ) { m_verticalBeamlineAngle = value; }

inline double LHCb::BeamParameters::betaStar() const { return m_betaStar; }

inline void LHCb::BeamParameters::setBetaStar( double value ) { m_betaStar = value; }

inline double LHCb::BeamParameters::bunchSpacing() const { return m_bunchSpacing; }

inline void LHCb::BeamParameters::setBunchSpacing( double value ) { m_bunchSpacing = value; }

inline const Gaudi::XYZPoint& LHCb::BeamParameters::beamSpot() const { return m_beamSpot; }

inline void LHCb::BeamParameters::setBeamSpot( const Gaudi::XYZPoint& value ) { m_beamSpot = value; }

inline double LHCb::BeamParameters::luminosity() const { return m_luminosity; }

inline void LHCb::BeamParameters::setLuminosity( double value ) { m_luminosity = value; }

inline double LHCb::BeamParameters::nu() const { return luminosity() * totalXSec() / revolutionFrequency(); }

inline double LHCb::BeamParameters::emittance() const {

  double beta =
      sqrt( energy() * energy() - 938.272013 * Gaudi::Units::MeV * 938.272013 * Gaudi::Units::MeV ) / energy();
  double gamma = 1. / sqrt( 1. - beta * beta );
  return epsilonN() / gamma / beta;
}

inline double LHCb::BeamParameters::sigmaX() const { return sqrt( betaStar() * emittance() / 2. ); }

inline double LHCb::BeamParameters::sigmaY() const { return sqrt( betaStar() * emittance() / 2. ); }

inline double LHCb::BeamParameters::sigmaZ() const { return sigmaS() / sqrt( 2. ); }

inline double LHCb::BeamParameters::angleSmear() const {
  if ( !essentiallyZero( betaStar() ) )
    return ( emittance() / betaStar() );
  else
    return 0.0;
}
