/*****************************************************************************\
* (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once
#include "GaudiKernel/VectorMap.h"
#include <algorithm>
#include <map>
#include <ostream>

namespace LHCb {

  /** @class CrossSectionsFSR CrossSectionsFSR.h
   *
   * Enum class for Generator fractions and cross-sections
   *
   * @author Davide Fazzini
   *
   */

  class CrossSectionsFSR final {
  public:
    /// lookup table for fraction and cross-section keys
    enum CrossSectionKey {
      MBCrossSection      = 0,  // Total cross-section of Minimum Bias events
      MeanNoZeroPUInt     = 2,  // Mean number of non-empty generated Pile-Up interactions
      MeanPUInt           = 3,  // Mean number of generated Pile-Up interactions
      MeanPUIntAcc        = 5,  // Mean number of accepted Pile-Up interactions
      FullEvtCut          = 7,  // Full event cut efficiency
      bCrossGen           = 10, // Generated b cross-section
      ThreebCrossGen      = 11, // Double generated b cross-section
      PromptBCrossGen     = 12, // Generated prompt B cross-section
      cCrossGen           = 13, // Generated c cross-section
      ThreecCrossGen      = 14, // Double generated c cross-section
      PromptCCrossGen     = 15, // Generated prompt C cross-section
      bAndcCrossGen       = 16, // Generated b and c cross-section
      bCrossAcc           = 17, // Accepted b cross-section
      ThreebCrossAcc      = 18, // Double accepted b cross-section
      PromptBCrossAcc     = 19, // Accepted prompt B cross-section
      cCrossAcc           = 20, // Accepted c cross-section
      ThreecCrossAcc      = 21, // Double accepted c cross-section
      PromptCCrossAcc     = 22, // Accepted prompt C cross-section
      bAndcCrossAcc       = 23, // Accepted b and c cross-section
      GenLevelCut         = 27, // Generator level cut efficiency
      FractionB0Gen       = 30, // Fraction of generated B0
      FractionantiB0Gen   = 31, // Fraction of generated B0bar
      FractionBplusGen    = 32, // Fraction of generated B+
      FractionBminusGen   = 33, // Fraction of generated B-
      FractionBs0Gen      = 34, // Fraction of generated Bs0
      FractionantiBs0Gen  = 35, // Fraction of generated anti-Bs0
      FractionbBarGen     = 36, // Fraction of generated b-Baryon
      FractionantibBarGen = 37, // Fraction of generated anti-b-Baryon
      FractionBcplusGen   = 38, // Fraction of generated Bc+
      FractionBcminusGen  = 39, // Fraction of generated Bc-
      FractionB0Acc       = 41, // Fraction of accepted B0
      FractionantiB0Acc   = 42, // Fraction of accepted B0bar
      FractionBplusAcc    = 43, // Fraction of accepted B+
      FractionBminusAcc   = 44, // Fraction of accepted B-
      FractionBs0Acc      = 45, // Fraction of accepted Bs0
      FractionantiBs0Acc  = 46, // Fraction of accepted anti-Bs0
      FractionbBaryonAcc  = 47, // Fraction of accepted b-Baryon
      FractionantibBarAcc = 48, // Fraction of accepted anti-b-Baryon
      FractionBcplusAcc   = 49, // Fraction of accepted Bc+
      FractionBcminusAcc  = 50, // Fraction of accepted Bc-
      FractionD0Gen       = 55, // Fraction of generated D0
      FractionantiD0Gen   = 56, // Fraction of generated D0bar
      FractionDplusGen    = 57, // Fraction of generated D+
      FractionDminusGen   = 58, // Fraction of generated D-
      FractionDsplusGen   = 59, // Fraction of generated Ds+
      FractionDsminusGen  = 60, // Fraction of generated Ds-
      FractioncBarGen     = 61, // Fraction of generated c-Baryon
      FractionanticBarGen = 62, // Fraction of generated anti-c-Baryon
      FractionD0Acc       = 64, // Fraction of accepted D0
      FractionantiD0Acc   = 65, // Fraction of accepted D0bar
      FractionDplusAcc    = 66, // Fraction of accepted D+
      FractionDminusAcc   = 67, // Fraction of accepted D-
      FractionDsplusAcc   = 68, // Fraction of accepted Ds+
      FractionDsminusAcc  = 69, // Fraction of accepted Ds-
      FractioncBaryonAcc  = 70, // Fraction of accepted c-Baryon
      FractionanticBarAcc = 71, // Fraction of accepted anti-c-Baryon
      FractionBGen        = 75, // Fraction of generated B
      FractionBstarGen    = 76, // Fraction of generated B*
      FractionB2starGen   = 77, // Fraction of generated B**
      FractionBAcc        = 78, // Fraction of accepted B
      FractionBstarAcc    = 79, // Fraction of accepted B*
      FractionB2starAcc   = 80, // Fraction of accepted B**
      FractionDGen        = 85, // Fraction of generated D
      FractionDstarGen    = 86, // Fraction of generated D*
      FractionD2starGen   = 87, // Fraction of generated D**
      FractionDAcc        = 88, // Fraction of accepted D
      FractionDstarAcc    = 89, // Fraction of accepted D*
      FractionD2starAcc   = 90, // Fraction of accepted D**
      FractionSignal      = 91, // Fraction of generated signal events
      FractionantiSignal  = 92, // Fraction of generated anti-signal events
      PartCutEff          = 94, // Particle cut efficiency
      antiPartCutEff      = 96, // Anti-particle cut efficiency
      Unknown             = 98  // Unknown value
    };

    friend std::ostream& operator<<( std::ostream& s, CrossSectionKey e ) { return s << CrossSectionKeyToString( e ); }

    CrossSectionsFSR() = delete;

    /// conversion of string to enum for type CrossSectionKey
    static CrossSectionKey CrossSectionKeyToType( const std::string& aName ) {
      auto iter = s2csk.find( aName );
      return iter != s2csk.end() ? iter->second : CrossSectionKey::Unknown;
    }

    /// conversion to string for enum type CrossSectionKey
    static const std::string& CrossSectionKeyToString( CrossSectionKey aEnum ) {
      static const std::string s_Unknown = "Unknown";
      auto iter = std::find_if( s2csk.begin(), s2csk.end(), [&]( const auto& i ) { return i.second == aEnum; } );
      if ( iter == s2csk.end() ) throw std::out_of_range( "unknown CrossSectionKey" );
      return iter->first;
    }

    /// return the list with the complete names of cross-sections
    static const std::map<CrossSectionKey, std::string>& getFullNames() { return csk2s; }

  private:
    static const GaudiUtils::VectorMap<std::string, CrossSectionKey> s2csk;
    static const std::map<CrossSectionKey, std::string>              csk2s;

  }; // class CrossSectionsFSR

} // namespace LHCb
