/*****************************************************************************\
* (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once
#include "Event/CrossSectionsFSR.h"
#include "Event/GenCountersFSR.h"
#include "GaudiKernel/DataObject.h"
#include "Kernel/STLExtensions.h"
#include <cmath>
#include <cstdio>
#include <fstream>
#include <map>
#include <numeric>
#include <ostream>
#include <string>
#include <vector>

namespace LHCb {

  // Forward declarations

  // Class ID definition
  static const CLID CLID_GenFSR = 13600;

  // Namespace for locations in TDS
  namespace GenFSRLocation {
    inline const std::string Default = "/FileRecords/GenFSR";
  }

  /** @class GenFSR GenFSR.h
   *
   * Accounting class for Generator in FSR
   *
   * @author Davide Fazzini
   *
   */

  class GenFSR final : public DataObject {
  public:
    /// Counter informations
    typedef std::pair<int, longlong> CounterValues;
    /// Counter informations
    typedef std::pair<std::string, double> CrossValues;
    /// Simulation string information container
    typedef std::map<std::string, std::string, std::less<>> StringInfo;
    /// Simulation int information container
    typedef std::map<std::string, int, std::less<>> IntInfo;

    // Retrieve pointer to class definition structure
    [[nodiscard]] const CLID& clID() const override { return GenFSR::classID(); }

    static const CLID& classID() { return CLID_GenFSR; }

    /// Initialize the FSR information
    GenFSR& initializeInfos() {
      for ( auto const& i : {"evtType", "nJobs"} ) m_intInfos.emplace( i, 0 );
      for ( auto const& i : {"hardGenerator", "generationMethod", "decFiles"} ) m_stringInfos.emplace( i, "" );
      return *this;
    }

    /// Get the int value of a FSR information
    [[nodiscard]] int getSimulationInfo( std::string_view name, int def = 0 ) const {
      auto i = m_intInfos.find( name );
      return m_intInfos.end() == i ? def : i->second;
    }

    /// Get the string value of a FSR information
    [[nodiscard]] std::string getSimulationInfo( std::string_view name, const std::string& def = "" ) const {
      auto i = m_stringInfos.find( name );
      return m_stringInfos.end() == i ? def : i->second;
    }

    /// Set the int value of a FSR information
    GenFSR& addSimulationInfo( const std::string& name, int value ) {
      m_intInfos[name] = value;
      return *this;
    }

    /// Set the string value of a FSR information
    GenFSR& addSimulationInfo( const std::string& name, const std::string& value ) {
      m_stringInfos[name] = value;
      return *this;
    }

    /// Increment the number of jobs used to create the FSR
    GenFSR& incrementNJobs( const int nJobs = 1 ) {
      int info = getSimulationInfo( "nJobs", 0 );
      return addSimulationInfo( "nJobs", info + nJobs );
    }

    /// add a Generator Counter to the set
    GenFSR& addGenCounter( GenCountersFSR::CounterKey key, longlong value, const int incr = 1 ) {
      m_genCounters.insert( {key, CounterValues{incr, value}} );
      return *this;
    }

    /// has information for specified Generator counter
    [[nodiscard]] bool hasGenCounter( GenCountersFSR::CounterKey key ) const {
      return m_genCounters.end() != m_genCounters.find( key );
    }

    /// erase Generator counter from the set
    GenFSR& eraseGenCounter( GenCountersFSR::CounterKey key ) {
      m_genCounters.erase( key );
      return *this;
    }

    /// extract the informations associated with the given key.
    [[nodiscard]] CounterValues getGenCounterInfo( GenCountersFSR::CounterKey key ) const {
      auto i = m_genCounters.find( key );
      return m_genCounters.end() == i ? CounterValues{0, 0} : i->second;
    }

    /// increments the values for existing keys and inserts a new key if needed
    GenFSR& incrementGenCounter( GenCountersFSR::CounterKey key, longlong value );

    /// sum a set of counters
    [[nodiscard]] longlong sumGenCounters( std::vector<std::string> const& counters ) const {
      return std::accumulate( counters.begin(), counters.end(), longlong{0}, [&]( longlong sum, const auto& c ) {
        return sum + getGenCounterInfo( GenCountersFSR::CounterKeyToType( c ) ).second;
      } );
    }
    /// sum a set of counters
    [[nodiscard]] longlong sumGenCounters( LHCb::span<const GenCountersFSR::CounterKey> counters ) const {
      return std::accumulate( counters.begin(), counters.end(), longlong{0},
                              [&]( longlong sum, const auto& c ) { return sum + getGenCounterInfo( c ).second; } );
    }

    /// Select the right counter to use as denominator to evaluate the fractions
    [[nodiscard]] longlong getDenominator( GenCountersFSR::CounterKey key ) const;

    /// Calculate the efficiency or the fraction relative to a counter
    [[nodiscard]] double getEfficiency( const longlong num, const longlong den, const double C = 1 ) const {
      return ( num != 0 && den != 0 ) ? C * ( (double)num / (double)den ) : 0.;
    }

    /// Calculate the efficiency error
    [[nodiscard]] double getEfficiencyError( longlong num, longlong den, double C = 1., bool flag = true ) const {
      if ( den == 0 ) return 0.;
      double eps = double( num ) / double( den );
      return C * sqrt( num * ( flag ? ( 1 - eps ) : ( 1 + eps ) ) ) / double( den );
    }

    /// add a Cross-Section to the set
    GenFSR& addCrossSection( CrossSectionsFSR::CrossSectionKey key, const CrossValues& value ) {
      m_crossSections.insert( {static_cast<int>( key ), value} );
      return *this;
    }

    /// has information for specified Cross-Section
    [[nodiscard]] bool hasCrossSection( CrossSectionsFSR::CrossSectionKey key ) const {
      return m_crossSections.end() != m_crossSections.find( static_cast<int>( key ) );
    }

    /// erase a Cross-Section from the set
    GenFSR& eraseCrossSection( CrossSectionsFSR::CrossSectionKey key ) {
      m_crossSections.erase( static_cast<int>( key ) );
      return *this;
    }

    /// extract the informations associated with the given key.
    [[nodiscard]] CrossValues getCrossSectionInfo( CrossSectionsFSR::CrossSectionKey key ) const {
      auto i = m_crossSections.find( static_cast<int>( key ) );
      return m_crossSections.end() == i ? CrossValues{"", 0} : i->second;
    }

    /// Addition operator
    GenFSR& operator+=( const GenFSR& rhs ) {
      if ( this != &rhs ) {
        // merge the cross sections
        mergeCrossSections( rhs.m_crossSections, rhs.m_genCounters );

        // merge the generator level counters
        mergeGenCounters( rhs.m_genCounters );

        // increment the number of jobs
        incrementNJobs( rhs.getSimulationInfo( "nJobs", 0 ) );
      }
      return *this;
    }

    /// printout
    std::ostream& fillStream( std::ostream& text ) const override;

    /// Retrieve const  Informations related to the simulation conditions
    [[nodiscard]] const StringInfo& stringInfos() const { return m_stringInfos; }

    /// Update  Informations related to the simulation conditions
    GenFSR& setStringInfos( const StringInfo& value ) {
      m_stringInfos = value;
      return *this;
    }

    /// Retrieve const  Numerical information related to the simulation conditions
    [[nodiscard]] const IntInfo& intInfos() const { return m_intInfos; }

    /// Update  Numerical information related to the simulation conditions
    GenFSR& setIntInfos( const IntInfo& value ) {
      m_intInfos = value;
      return *this;
    }

    /// Retrieve const  Set of Generator Level counter used in this job
    [[nodiscard]] auto genCounters() const {
      struct RangeAdaptor {
        std::map<int, CounterValues> const*          m_parent;
        std::map<int, CounterValues>::const_iterator m_current = m_parent->begin();
        struct Sentinel {};
        RangeAdaptor& operator++() {
          ++m_current;
          return *this;
        }
        auto operator*() const { return std::pair{GenCountersFSR::CounterKey( m_current->first ), m_current->second}; }
        auto begin() const { return *this; }
        auto end() const { return Sentinel{}; }
        bool operator!=( Sentinel ) const { return m_current != m_parent->end(); }
      };
      return RangeAdaptor{&m_genCounters};
    }

    /// Retrieve const  Set of Cross-Sections
    [[nodiscard]] auto crossSections() const {
      struct RangeAdaptor {
        std::map<int, CrossValues> const*          m_parent;
        std::map<int, CrossValues>::const_iterator m_current = m_parent->begin();
        struct Sentinel {};
        RangeAdaptor& operator++() {
          ++m_current;
          return *this;
        }
        auto operator*() const {
          return std::pair{CrossSectionsFSR::CrossSectionKey( m_current->first ), m_current->second};
        }
        auto begin() const { return *this; }
        auto end() const { return Sentinel{}; }
        bool operator!=( Sentinel ) const { return m_current != m_parent->end(); }
      };
      return RangeAdaptor{&m_crossSections};
    }

    friend std::ostream& operator<<( std::ostream& str, const GenFSR& obj ) { return obj.fillStream( str ); }

  private:
    /// sum the cross-sections for existing keys and inserts a new key if needed
    GenFSR& mergeCrossSections( const std::map<int, CrossValues>&   rhs_cross,
                                const std::map<int, CounterValues>& rhs_counter );
    /// sums the counter values for existing keys and inserts a new key if needed
    GenFSR& mergeGenCounters( const std::map<int, CounterValues>& rhs_counter );

    StringInfo                   m_stringInfos{};   ///< Informations related to the simulation conditions
    IntInfo                      m_intInfos{};      ///< Numerical information related to the simulation conditions
    std::map<int, CounterValues> m_genCounters{};   ///< Set of Generator Level counter used in this job
    std::map<int, CrossValues>   m_crossSections{}; ///< Set of Cross-Sections
  };                                                // class GenFSR

} // namespace LHCb

// -----------------------------------------------------------------------------
// end of class
// -----------------------------------------------------------------------------

inline LHCb::GenFSR& LHCb::GenFSR::mergeGenCounters( const std::map<int, CounterValues>& rhs_counter ) {
  // sum counter values or add new key
  for ( auto [key_, counterB] : rhs_counter ) {
    auto key      = GenCountersFSR::CounterKey( key_ );
    auto counterA = getGenCounterInfo( key );
    if ( hasGenCounter( key ) ) eraseGenCounter( key );
    addGenCounter( key, counterA.second + counterB.second, counterA.first + counterB.first );
  }
  return *this;
}

inline LHCb::GenFSR& LHCb::GenFSR::incrementGenCounter( LHCb::GenCountersFSR::CounterKey key, longlong value ) {
  // increment counter or add new key, only if data exists
  if ( value != -1 ) {
    CounterValues pair( 0, 0 );
    if ( hasGenCounter( key ) ) {
      pair = getGenCounterInfo( key );
      eraseGenCounter( key );
    }
    addGenCounter( key, pair.second + value, pair.first + 1 );
  }
  return *this;
}

inline longlong LHCb::GenFSR::getDenominator( LHCb::GenCountersFSR::CounterKey key ) const {
  longlong den = 0;

  if ( hasGenCounter( key ) ) {
    auto BcounterKey = LHCb::GenCountersFSR::CounterKey::Unknown;

    // For these counters the fraction is calculated using the sum of a sub-set of
    // counters
    bool sum_flag = ( ( key >= 30 && key <= 50 ) || // B Hadron counters
                      ( key >= 55 && key <= 71 ) || // D Hadron counters
                      ( key >= 75 && key <= 80 ) || // B** Hadron counters
                      ( key >= 85 && key <= 90 ) || // D** Hadron counters
                      ( key == 91 || key == 92 ) ); // signal/anti-signal counters

    if ( !sum_flag ) {
      if ( key == LHCb::GenCountersFSR::EvtGenerated ) {
        BcounterKey = LHCb::GenCountersFSR::CounterKey::EvtGenerated;
      } else if ( key == LHCb::GenCountersFSR::IntGenerated ) {
        BcounterKey = LHCb::GenCountersFSR::CounterKey::AllEvt;
      } else if ( key == LHCb::GenCountersFSR::IntAccepted ) {
        BcounterKey = LHCb::GenCountersFSR::CounterKey::EvtAccepted;
      } else if ( key == LHCb::GenCountersFSR::AfterFullEvt ) {
        BcounterKey = LHCb::GenCountersFSR::CounterKey::BeforeFullEvt;
      } else if ( key >= 10 && key <= 16 ) { // counters of generated interactions
        BcounterKey = LHCb::GenCountersFSR::CounterKey::IntGenerated;
      } else if ( key >= 17 && key <= 23 ) { // counters of accepted interactions
        BcounterKey = LHCb::GenCountersFSR::CounterKey::IntAccepted;
      } else if ( key == LHCb::GenCountersFSR::AfterLevelCut ) {
        BcounterKey = LHCb::GenCountersFSR::CounterKey::BeforeLevelCut;
      } else if ( key == LHCb::GenCountersFSR::AfterPCut ) {
        BcounterKey = LHCb::GenCountersFSR::CounterKey::BeforePCut;
      } else if ( key == LHCb::GenCountersFSR::AfterantiPCut ) {
        BcounterKey = LHCb::GenCountersFSR::CounterKey::BeforeantiPCut;
      } else {
        BcounterKey = LHCb::GenCountersFSR::CounterKey::Unknown;
      }

      if ( BcounterKey != LHCb::GenCountersFSR::CounterKey::Unknown ) { den = getGenCounterInfo( BcounterKey ).second; }
    } else {

      constexpr auto s1 =
          std::array{LHCb::GenCountersFSR::CounterKey::B0Gen, LHCb::GenCountersFSR::CounterKey::BplusGen,
                     LHCb::GenCountersFSR::CounterKey::Bs0Gen, LHCb::GenCountersFSR::CounterKey::bBaryonGen,
                     LHCb::GenCountersFSR::CounterKey::BcplusGen};
      constexpr auto s2 =
          std::array{LHCb::GenCountersFSR::CounterKey::antiB0Gen, LHCb::GenCountersFSR::CounterKey::BminusGen,
                     LHCb::GenCountersFSR::CounterKey::antiBs0Gen, LHCb::GenCountersFSR::CounterKey::antibBaryonGen,
                     LHCb::GenCountersFSR::CounterKey::BcminusGen};
      constexpr auto s3 =
          std::array{LHCb::GenCountersFSR::CounterKey::B0Acc, LHCb::GenCountersFSR::CounterKey::BplusAcc,
                     LHCb::GenCountersFSR::CounterKey::Bs0Acc, LHCb::GenCountersFSR::CounterKey::bBaryonAcc,
                     LHCb::GenCountersFSR::CounterKey::BcplusAcc};
      constexpr auto s4 =
          std::array{LHCb::GenCountersFSR::CounterKey::antiB0Acc, LHCb::GenCountersFSR::CounterKey::BminusAcc,
                     LHCb::GenCountersFSR::CounterKey::antiBs0Acc, LHCb::GenCountersFSR::CounterKey::antibBaryonAcc,
                     LHCb::GenCountersFSR::CounterKey::BcminusAcc};
      constexpr auto s5 =
          std::array{LHCb::GenCountersFSR::CounterKey::D0Gen, LHCb::GenCountersFSR::CounterKey::DplusGen,
                     LHCb::GenCountersFSR::CounterKey::DsplusGen, LHCb::GenCountersFSR::CounterKey::cBaryonGen};
      constexpr auto s6 =
          std::array{LHCb::GenCountersFSR::CounterKey::antiD0Gen, LHCb::GenCountersFSR::CounterKey::DminusGen,
                     LHCb::GenCountersFSR::CounterKey::DsminusGen, LHCb::GenCountersFSR::CounterKey::anticBaryonGen};
      constexpr auto s7 =
          std::array{LHCb::GenCountersFSR::CounterKey::D0Acc, LHCb::GenCountersFSR::CounterKey::DplusAcc,
                     LHCb::GenCountersFSR::CounterKey::DsplusAcc, LHCb::GenCountersFSR::CounterKey::cBaryonAcc};
      constexpr auto s8 =
          std::array{LHCb::GenCountersFSR::CounterKey::antiD0Acc, LHCb::GenCountersFSR::CounterKey::DminusAcc,
                     LHCb::GenCountersFSR::CounterKey::DsminusAcc, LHCb::GenCountersFSR::CounterKey::anticBaryonAcc};
      constexpr auto s9 = std::array{LHCb::GenCountersFSR::CounterKey::BGen, LHCb::GenCountersFSR::CounterKey::BstarGen,
                                     LHCb::GenCountersFSR::CounterKey::B2starGen};
      constexpr auto s10 =
          std::array{LHCb::GenCountersFSR::CounterKey::DGen, LHCb::GenCountersFSR::CounterKey::DstarGen,
                     LHCb::GenCountersFSR::CounterKey::D2starGen};
      constexpr auto s11 =
          std::array{LHCb::GenCountersFSR::CounterKey::BAcc, LHCb::GenCountersFSR::CounterKey::BstarAcc,
                     LHCb::GenCountersFSR::CounterKey::B2starAcc};
      constexpr auto s12 =
          std::array{LHCb::GenCountersFSR::CounterKey::DAcc, LHCb::GenCountersFSR::CounterKey::DstarAcc,
                     LHCb::GenCountersFSR::CounterKey::D2starAcc};
      constexpr auto s13 =
          std::array{LHCb::GenCountersFSR::CounterKey::EvtSignal, LHCb::GenCountersFSR::CounterKey::EvtantiSignal};

      auto s = std::array<LHCb::span<const LHCb::GenCountersFSR::CounterKey>, 13>{s1, s2, s3,  s4,  s5,  s6, s7,
                                                                                  s8, s9, s10, s11, s12, s13};

      auto i = std::find_if( s.begin(), s.end(),
                             [key]( auto ks ) { return std::find( ks.begin(), ks.end(), key ) != ks.end(); } );
      if ( i != s.end() ) den = sumGenCounters( *i );
    }
  }

  return den;
}

inline LHCb::GenFSR& LHCb::GenFSR::mergeCrossSections( const std::map<int, CrossValues>&   rhs_cross,
                                                       const std::map<int, CounterValues>& rhs_counter ) {
  // sum cross-sections or add new key
  for ( const auto& [keys_, vals] : rhs_cross ) {
    auto keys                 = CrossSectionsFSR::CrossSectionKey( keys_ );
    const auto [name, crossB] = vals; // copy on purpose, eraseCrossSection(keys) below will reset vals...
    const double crossA       = getCrossSectionInfo( keys ).second;

    int      keyc   = 0;
    longlong countA = 0, countB = 0;

    auto i = std::find_if( rhs_counter.begin(), rhs_counter.end(),
                           [k = keys + 100]( const auto& p ) { return p.first == k; } );
    if ( i != rhs_counter.end() ) {
      keyc   = i->first;
      countB = i->second.second;
    } else if ( !rhs_counter.empty() ) { // backwards bug compatibility...
      keyc = rhs_counter.rbegin()->first;
    }
    countA = getGenCounterInfo( static_cast<GenCountersFSR::CounterKey>( keyc ) ).second; // TODO/FIXME: is this really
                                                                                          // correct???
    double meanCross = ( ( countA + countB ) != 0 ? ( crossA * countA + crossB * countB ) / ( countA + countB ) : 0 );

    if ( hasCrossSection( keys ) ) eraseCrossSection( keys );
    addCrossSection( keys, {name, meanCross} );
  }
  return *this;
}

inline std::ostream& LHCb::GenFSR::fillStream( std::ostream& s ) const {
  s << "{ "
    << "Event type: " << getSimulationInfo( "evtType", 0 )
    << "  Hard Generator: " << getSimulationInfo( "hardGenerator", "" )
    << "  Generation method: " << getSimulationInfo( "generationMethod", "" )
    << "  DecFiles version: " << getSimulationInfo( "decFiles", "" ) << "  njobs:" << getSimulationInfo( "nJobs", 0 )
    << "\n\n"
    << " generator counters: ";
  for ( const auto& [key_, v] : m_genCounters ) {
    auto key  = GenCountersFSR::CounterKey( key_ );
    auto name = GenCountersFSR::CounterKeyToString( key );
    if ( name == "Unknown" ) {
      auto i = m_crossSections.find( static_cast<CrossSectionsFSR::CrossSectionKey>( key - 100 ) );
      if ( i != m_crossSections.end() ) name = i->second.first;
    }
    s << name << "  key: " << static_cast<int>( key ) << "  value: " << v.second << " / ";
  }
  s << "\n\ncross-sections: ";
  for ( const auto& [key, v] : m_crossSections ) {
    std::string_view name = v.first;
    name.remove_suffix( 2 );
    s << name << "  key: " << static_cast<int>( key ) << "  value: " << v.second << " / ";
  }
  return s << " }";
}
