###############################################################################
# (c) Copyright 2000-2021 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
#[=======================================================================[.rst:
Event/Phoenix
-----------
This is used for getting event data from DumpEvent Algs and 
output in a json file to be used in LHCbEventDisplay

https://lhcb-web-display.app.cern.ch/
#]=======================================================================]

gaudi_add_library(PhoenixLib
    SOURCES
        src/Store.cpp
    LINK
        PUBLIC
            Gaudi::GaudiKernel
            nlohmann_json::nlohmann_json
)

gaudi_add_module(Phoenix
    SOURCES
        src/PhoenixSink.cpp
    LINK
        Gaudi::GaudiKernel
        nlohmann_json::nlohmann_json
        LHCb::PhoenixLib
)