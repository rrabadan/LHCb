/***************************************************************************** \
 * (c) Copyright 2000-2021 CERN for the benefit of the LHCb Collaboration      *
 *                                                                             *
 * This software is distributed under the terms of the GNU General Public      *
 * Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
 *                                                                             *
 * In applying this licence, CERN does not waive the privileges and immunities *
 * granted to it by virtue of its status as an Intergovernmental Organization  *
 * or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#include "DetDesc/Condition.h"
#include "DetDesc/DetectorElement.h"
#include "DetDesc/GenericConditionAccessorHolder.h"
#include "Event/PrHits.h"
#include "Event/RawBank.h"
#include "Event/RawEvent.h"
#include "MuonDAQ/MuonDAQDefinitions.h"
#include "MuonDAQ/MuonHitsBankDecoder.h"
#include "MuonDet/DeMuonDetector.h"
#include "MuonDet/MuonNamespace.h"
#include "MuonDet/MuonTell40LinksMap.h"
#include "MuonDet/MuonTilePositionUpgrade.h"

//#include "LHCb/DeLHCb.h"

#include "LHCbAlgs/Transformer.h"

#include "GaudiKernel/ToolHandle.h"

#include <boost/numeric/conversion/cast.hpp>

#include <array>
#include <bitset>
#include <functional>
#include <optional>
#include <string>
#include <vector>

/**
 *  This is the muon reconstruction algorithm
 *  This just crosses the logical strips back into pads
 */
using namespace Muon::DAQ;
using namespace LHCb::Detector;
namespace LHCb::MuonUpgrade::DAQ {

  namespace {
    struct Digit {
      Detector::Muon::TileID tile;
      unsigned int           tdc;
      Digit( Detector::Muon::TileID tile, unsigned int tdc ) : tile{tile}, tdc{tdc} {} // C++20: please remove this line
    };
  } // namespace

} // namespace LHCb::MuonUpgrade::DAQ

namespace LHCb::MuonUpgrade::DAQ {
  namespace {
    template <typename Iterator>
    Iterator addCoordsCrossingMap( Iterator first, Iterator last, CommonMuonHits& commonHits,
                                   const ComputeTilePosition& compute, size_t /* nStations */ ) {
      // need to calculate the shape of the horizontal and vertical logical strips

      // used flags

      std::bitset<500> used; // (to be updated with new readout) the maximum # of channels per quadrant is currently 384
                             // (from M2R2) the pads readout region have been ignored otherwise larger number is
                             // possible

      // find if region is already at pads
      bool alreadyPads = false;

      if ( first != last ) {
        auto s = first->tile.station();
        auto r = first->tile.region();
        if ( s == 0 && r > 1 ) alreadyPads = true;
        if ( s == 2 && r == 0 ) alreadyPads = true;
        if ( s == 3 && r == 0 ) alreadyPads = true;
        if ( s == 3 && r == 3 ) alreadyPads = true;
      }

      // partition into the two directions of digits
      // vertical and horizontal stripes
      Iterator mid;

      if ( !alreadyPads ) {
        assert( std::distance( first, last ) < 500 );
        mid = std::partition( first, last, []( const Digit& digit ) { return digit.tile.isHorizontal(); } );
      } else {
        // if already a pad region skip the crossing
        mid = last;
      }

      auto digitsOne = make_span( first, mid );
      auto digitsTwo = make_span( mid, last );

      // check how many cross
      if ( first != mid && mid != last ) {
        auto thisGridX = first->tile.layout().xGrid();
        auto thisGridY = first->tile.layout().yGrid();

        auto     otherGridX = mid->tile.layout().xGrid();
        auto     otherGridY = mid->tile.layout().yGrid();
        unsigned i          = 0;
        for ( const Digit& one : digitsOne ) {
          unsigned int calcX = one.tile.nX() * otherGridX / thisGridX;
          unsigned     j     = mid - first;
          for ( const Digit& two : digitsTwo ) {
            unsigned int calcY = two.tile.nY() * thisGridY / otherGridY;
            if ( calcX == two.tile.nX() && calcY == one.tile.nY() ) {
              Detector::Muon::TileID pad( one.tile );
              pad.setY( two.tile.nY() );
              pad.setLayout( {thisGridX, otherGridY} );
              auto&& [pos, dx, dy] = compute.tilePosition( pad );
              commonHits.emplace_back( std::move( pad ), one.tile, two.tile, pos.X(), dx, pos.Y(), dy, pos.Z(), 0,
                                       one.tdc, one.tdc - two.tdc, 0 );
              used[i] = used[j] = true;
            }
            ++j;
          }
          ++i;
        }
      }

      if ( alreadyPads ) {
        for ( const Digit& digit : digitsOne ) {
          auto pos = compute.tilePosition( digit.tile );

          commonHits.emplace_back( digit.tile, pos.p.X(), pos.dX, pos.p.Y(), pos.dY, pos.p.Z(), 0., 1, digit.tdc,
                                   digit.tdc );
        }
      } else {
        unsigned m = 0;
        for ( const Digit& digit : digitsOne ) {

          if ( !used[m++] ) {

            auto pos = compute.stripXPosition( digit.tile );
            commonHits.emplace_back( digit.tile, pos.p.X(), pos.dX, pos.p.Y(), pos.dY, pos.p.Z(), 0., 1, digit.tdc,
                                     digit.tdc );
          }
        }
        for ( const Digit& digit : digitsTwo ) {
          if ( !used[m++] ) {

            auto pos = compute.stripYPosition( digit.tile );
            commonHits.emplace_back( digit.tile, pos.p.X(), pos.dX, pos.p.Y(), pos.dY, pos.p.Z(), 0., 1, digit.tdc,
                                     digit.tdc );
          }
        }
      }

      return last;
    }
  } // namespace
  //-----------------------------------------------------------------------------
  // Implementation file for class : RawToHits
  //-----------------------------------------------------------------------------
  class RawToHits final
      : public Algorithm::Transformer<MuonHitContainer( const EventContext&, const RawBank::View&, const RawBank::View&,
                                                        const DeMuonDetector&, const ComputeTilePosition&,
                                                        const Tell40LinksMap& ),
                                      DetDesc::usesConditions<DeMuonDetector, ComputeTilePosition, Tell40LinksMap>> {
  public:
    RawToHits( const std::string& name, ISvcLocator* pSvcLocator );

    StatusCode       initialize() override;
    StatusCode       finalize() override;
    MuonHitContainer operator()( const EventContext&, const RawBank::View&, const RawBank::View&, const DeMuonDetector&,
                                 const ComputeTilePosition&, const Tell40LinksMap& ) const override;

  private:
    std::array<unsigned int, 24> ReadMapFromData( unsigned int active_links, ByteType data_fiber[3],
                                                  unsigned int& number_of_readout_fibers ) const;
    Gaudi::Property<bool> m_useErrorBank{this, "UseErrorBank", true}; // set to false if no special bank data is desired
    /* commented code below need revision
    mutable unsigned int  local_hits_counters[44][16]               = {};
    mutable unsigned int  local_wrong_hits_counters[44][16]         = {};
    mutable unsigned int  local_acquired_frame[44][16]              = {};
    mutable unsigned int  local_desynch_acquired_frame[44][16]      = {};
    mutable unsigned int  local_desynch_hits_counters[44][16]       = {};
    mutable unsigned int  local_desynch_wrong_hits_counters[44][16] = {};
    */
    mutable Gaudi::Accumulators::MsgCounter<MSG::ERROR> m_invalid_add{this, "invalid add"};
    mutable Gaudi::Accumulators::MsgCounter<MSG::ERROR> m_bad_magic{this, "Incorrect Magic pattern in raw bank"};
    mutable Gaudi::Accumulators::MsgCounter<MSG::ERROR> m_bank_too_short{this, "Muon bank is too short"};
    mutable Gaudi::Accumulators::MsgCounter<MSG::ERROR> m_too_many_hits{this,
                                                                        "Muon bank has too many hits for its size"};
    mutable Gaudi::Accumulators::MsgCounter<MSG::ERROR> m_bad_size_of_bank{
        this, "Size of link is bank is not compatible with single links size "};
    mutable Gaudi::Accumulators::MsgCounter<MSG::ERROR> m_bad_size_of_link{this, "Size of link is bad (<= 6)"};
    mutable Gaudi::Accumulators::MsgCounter<MSG::ERROR> m_missing_links_in_map{this, "Link info missing in readou map"};

    // counters that must undergo to a revision
    mutable Gaudi::Accumulators::Counter<> m_Tell40Size[44]{
        {this, "Tell_001_0 size"}, {this, "Tell_001_1 size"}, {this, "Tell_002_0 size"}, {this, "Tell_002_1 size"},
        {this, "Tell_003_0 size"}, {this, "Tell_003_1 size"}, {this, "Tell_004_0 size"}, {this, "Tell_004_1 size"},
        {this, "Tell_005_0 size"}, {this, "Tell_005_1 size"}, {this, "Tell_006_0 size"}, {this, "Tell_006_1 size"},
        {this, "Tell_007_0 size"}, {this, "Tell_007_1 size"}, {this, "Tell_008_0 size"}, {this, "Tell_008_1 size"},
        {this, "Tell_009_0 size"}, {this, "Tell_009_1 size"}, {this, "Tell_010_0 size"}, {this, "Tell_010_1 size"},
        {this, "Tell_011_0 size"}, {this, "Tell_011_1 size"}, {this, "Tell_012_0 size"}, {this, "Tell_012_1 size"},
        {this, "Tell_013_0 size"}, {this, "Tell_013_1 size"}, {this, "Tell_014_0 size"}, {this, "Tell_014_1 size"},
        {this, "Tell_015_0 size"}, {this, "Tell_015_1 size"}, {this, "Tell_016_0 size"}, {this, "Tell_016_1 size"},
        {this, "Tell_017_0 size"}, {this, "Tell_017_1 size"}, {this, "Tell_018_0 size"}, {this, "Tell_018_1 size"},
        {this, "Tell_019_0 size"}, {this, "Tell_019_1 size"}, {this, "Tell_020_0 size"}, {this, "Tell_020_1 size"},
        {this, "Tell_021_0 size"}, {this, "Tell_021_1 size"}, {this, "Tell_022_0 size"}, {this, "Tell_022_1 size"}};
    Gaudi::Property<bool> m_print_stat{this, "PrintStat", false};
  };

  DECLARE_COMPONENT_WITH_ID( RawToHits, "MuonRawInUpgradeToHits" )

  //=============================================================================
  // Standard constructor
  //=============================================================================
  RawToHits::RawToHits( const std::string& name, ISvcLocator* pSvcLocator )
      : Transformer( name, pSvcLocator,
                     {KeyValue{"RawBanks", "DAQ/RawBanks/Muon"}, KeyValue{"ErrorRawBanks", "DAQ/RawBanks/MuonError"},
                      KeyValue{"MuonDetectorLocation", DeMuonLocation::Default},
                      KeyValue{"TilePositionCalculator", "AlgorithmSpecific-" + name + "-TilePositionCalculator"},
                      KeyValue{"Tell40LinksMapByRun", "AlgorithmSpecific-" + name + "-Tell40LinksMapByRun"}},
                     KeyValue{"HitContainer", MuonHitContainerLocation::Default} ) {}

  //=============================================================================
  // Initialisation
  //=============================================================================
  StatusCode RawToHits::initialize() {

    auto sc = Transformer::initialize();
    if ( !sc ) return sc;

    addConditionDerivation<Tell40LinksMap( const DeMuonDetector&, const DeLHCb& )>(
        {DeMuonLocation::Default, LHCb::standard_geometry_top}, inputLocation<Tell40LinksMap>() );

    addConditionDerivation<ComputeTilePosition( const DeMuonDetector& )>( {DeMuonLocation::Default},
                                                                          inputLocation<ComputeTilePosition>() );

    return sc;
  }

  StatusCode RawToHits::finalize() {

    return Transformer::finalize().andThen( [&]() {
      if ( m_print_stat ) {
        for ( int i = 0; i < 44; i++ ) {
          info() << "Tell40 size " << i / 2 + 1 << " " << i % 2 << " " << m_Tell40Size[i] << endmsg;
        }
        info() << "---------------------------------------------------------------------------------"
                  "-------------------"
                  "--------------------------------------------------"
               << endmsg;
        /*
        info() << " Number of hits in each link " << endmsg;
        for ( int i = 0; i < 44; i++ ) {
          info() << "Tell40 " << std::setw( 2 ) << i / 2 + 1 << " PCI number " << i % 2 << "-------"; // endmsg;
          for ( int j = 0; j < 16; j++ ) {
            if ( local_acquired_frame[i][j] > 0 ) {
              info() << "  " << std::setw( 10 ) << local_hits_counters[i][j] << "  ";
            } else {
              info() << "  " << std::setw( 10 ) << "n/c"
                     << "  ";
            }

            if ( j == 7 ) {
              info() << endmsg;
              info() << std::setw( 29 ) << " ";
            }
          }
          info() << endmsg;
        }
        info() << " Number of bits in hits map not connected to FE channels in each link " << endmsg;
        for ( int i = 0; i < 44; i++ ) {
          info() << "Tell40 " << std::setw( 2 ) << i / 2 + 1 << " PCI number " << i % 2 << "-------"; // endmsg;
          for ( int j = 0; j < 16; j++ ) {
            if ( local_acquired_frame[i][j] > 0 ) {
              info() << "  " << std::setw( 10 ) << local_wrong_hits_counters[i][j] << "  ";
            } else {
              info() << "  " << std::setw( 10 ) << "n/c"
                     << "  ";
            }
            if ( j == 7 ) {
              info() << endmsg;
              info() << std::setw( 29 ) << " ";
            }
          }
          info() << endmsg;
        }
        info() << " Number of acquired events  in each link " << endmsg;
        for ( int i = 0; i < 44; i++ ) {
          info() << "Tell40 " << std::setw( 2 ) << i / 2 + 1 << " PCI number " << i % 2 << "-------"; // endmsg;
          for ( int j = 0; j < 16; j++ ) {
            info() << "  " << std::setw( 8 ) << local_acquired_frame[i][j] << "  ";
            if ( j == 7 ) {
              info() << endmsg;
              info() << std::setw( 29 ) << " ";
            }
          }
          info() << endmsg;
        }
        info() << " Number of hits in each link  when desyncronized" << endmsg;
        for ( int i = 0; i < 44; i++ ) {
          info() << "Tell40 " << std::setw( 2 ) << i / 2 + 1 << " PCI number " << i % 2 << "-------"; // endmsg;
          for ( int j = 0; j < 16; j++ ) {
            if ( local_desynch_acquired_frame[i][j] > 0 ) {
              info() << "  " << std::setw( 10 ) << local_desynch_hits_counters[i][j] << "  ";
            } else {
              info() << "  " << std::setw( 10 ) << "n/c"
                     << "  ";
            }

            if ( j == 7 ) {
              info() << endmsg;
              info() << std::setw( 29 ) << " ";
            }
          }
          info() << endmsg;
        }
        info() << " Number of bits in hits map not connected to FE channels in each link  when desyncronized" << endmsg;
        for ( int i = 0; i < 44; i++ ) {
          info() << "Tell40 " << std::setw( 2 ) << i / 2 + 1 << " PCI number " << i % 2 << "-------"; // endmsg;
          for ( int j = 0; j < 16; j++ ) {
            if ( local_desynch_acquired_frame[i][j] > 0 ) {
              info() << "  " << std::setw( 10 ) << local_desynch_wrong_hits_counters[i][j] << "  ";
            } else {
              info() << "  " << std::setw( 10 ) << "n/c"
                     << "  ";
            }
            if ( j == 7 ) {
              info() << endmsg;
              info() << std::setw( 29 ) << " ";
            }
          }
          info() << endmsg;
        }
        info() << " Number of acquired events  in each link when desyncronized" << endmsg;
        for ( int i = 0; i < 44; i++ ) {
          info() << "Tell40 " << std::setw( 2 ) << i / 2 + 1 << " PCI number " << i % 2 << "-------"; // endmsg;
          for ( int j = 0; j < 16; j++ ) {
            info() << "  " << std::setw( 8 ) << local_desynch_acquired_frame[i][j] << "  ";
            if ( j == 7 ) {
              info() << endmsg;
              info() << std::setw( 29 ) << " ";
            }
          }
          info() << endmsg;
        }
        */
      }
    } );
  }

  //=============================================================================
  // Main execution
  //=============================================================================
  MuonHitContainer RawToHits::operator()( const EventContext& evtCtx, const RawBank::View& muon_banks,
                                          const RawBank::View& muon_error_banks, const DeMuonDetector& det,
                                          const ComputeTilePosition& compute, const Tell40LinksMap& linksMap ) const {

    if ( msgLevel( MSG::DEBUG ) ) { debug() << "==> Execute the decoding" << endmsg; }
    size_t nStations = boost::numeric_cast<size_t>( det.stations() );
    assert( nStations <= 4 );

    auto memResource = LHCb::getMemResource( evtCtx );
    // Maybe not actually important to set up the memory resource properly here..?
    auto stations = LHCb::make_object_array<CommonMuonStation, 4>( memResource );
    if ( muon_banks.empty() ) return {std::move( stations )};
    if ( msgLevel( MSG::DEBUG ) ) {
      debug() << "==> Detected " << muon_banks.size() << " # normal banks and " << muon_error_banks.size()
              << " erro banks" << endmsg;
    }

    // array of vectors of hits
    // each element of the array correspond to hits from a single station
    // this will ease the sorting after
    auto decoding = LHCb::make_object_array<std::vector<Digit, LHCb::Allocators::EventLocal<Digit>>, 64>( memResource );

    std::array<std::array<bool, MuonUpgradeDAQHelper_linkNumber>,
               MuonUpgradeDAQHelper_maxTell40PCINumber * MuonUpgradeDAQHelper_maxTell40Number>
        link_in_error{{}};

    std::vector<Digit> digits_inTell40;
    digits_inTell40.reserve( 48 ); // 48 is the max number of hits in each Tell40 link
    std::map<int, LHCb::span<RawBank const* const>> allBanks;
    // add to the used banks first the stadard muon banks
    allBanks[0] = muon_banks;
    // then the special banks created when a desyncronization has been detected in FW
    allBanks[1] = muon_error_banks;
    if ( msgLevel( MSG::DEBUG ) ) {
      for ( int bt = 0; bt < 2; bt++ ) {
        for ( const auto& raw_bank : allBanks[bt] ) {
          unsigned int tell40PCI = raw_bank->sourceID() & 0x00FF;
          unsigned int TNumber   = tell40PCI / 2 + 1;
          unsigned int PCINumber = tell40PCI % 2;
          debug() << "source ID " << bt << " " << tell40PCI << " " << TNumber << " " << PCINumber << endmsg;
        }
      }
    }

    for ( int bt = 0; bt < 2; bt++ ) {
      if ( !m_useErrorBank && bt == 1 ) continue;

      for ( const auto& raw_bank : allBanks[bt] ) {
        // check magic pattern
        if ( RawBank::MagicPattern != raw_bank->magic() ) {
          ++m_bad_magic;
          continue;
        }

        // the Tell40 number is hidden in source ID lower part let's retrieve it
        bool         bank_corrupted  = false;
        unsigned int tell40PCI       = raw_bank->sourceID() & 0x00FF;
        unsigned int TNumber         = tell40PCI / 2 + 1;
        unsigned int PCINumber       = tell40PCI % 2;
        unsigned int stationOfTell40 = det.getUpgradeDAQInfo()->whichstationTell40( TNumber - 1 );
        unsigned int active_links    = det.getUpgradeDAQInfo()->getNumberOfActiveLinkInTell40PCI( TNumber, PCINumber );
        if ( msgLevel( MSG::DEBUG ) )
          debug() << " reading tell40 num " << TNumber << " PCI n " << PCINumber << " "
                  << " station " << stationOfTell40 << " active link " << active_links << endmsg;
        if ( msgLevel( MSG::DEBUG ) )
          debug() << " reading tell40 num " << TNumber << " PCI n " << PCINumber << " "
                  << " bank size" << (unsigned int)raw_bank->size() << endmsg;

        if ( (unsigned int)raw_bank->size() < 1 ) {
          ++m_bank_too_short;
          continue;
        }
        auto         range8             = raw_bank->range<ByteType>();
        auto         range_data         = range8.subspan( 1 );
        unsigned int link_start_pointer = 0;
        unsigned int regionOfLink       = 99;
        unsigned int quarterOfLink      = 99;
        unsigned int ZS_applied         = ( range8[0] & 0x05 );
        unsigned int EDAC_applied       = ( range8[0] & 0x08 ) >> 3;
        unsigned int synch_evt          = ( range8[0] & 0x10 ) >> 4;
        unsigned int align_info         = ( range8[0] & 0x20 ) >> 5;
        // keep to implement EDAC info for testing purposes        unsigned int EDAC_info_stored   = ( range8[0] & 0x40
        // ) >> 6;

        if ( msgLevel( MSG::DEBUG ) ) {
          debug() << "header of bank reports :" << endmsg;
          debug() << " ZS type " << ZS_applied << " "
                  << " EDAC enabled " << EDAC_applied << " " << endmsg;
          debug() << " Synch event " << synch_evt << " aligned fiber info stored " << align_info << endmsg;
        }
        // synch events do not contains hits  so let's skip
        if ( synch_evt ) continue;
        if ( (unsigned int)raw_bank->size() < 1 + 3 * align_info ) {
          ++m_bank_too_short;
          continue;
        }
        unsigned int                 number_of_readout_fibers = 0;
        std::array<unsigned int, 24> map_connected_fibers     = {0};

        // need to determine the true enabled links, possible only or using online condition or data itself if
        // align_info is true

        bool skipmap = false;

        if ( !skipmap && linksMap.getInitStatus( TNumber, PCINumber ) == Tell40LinksMap::InitStatus::OK ) {
          connectionMap test       = linksMap.getAllConnection( TNumber, PCINumber );
          number_of_readout_fibers = test.connectedLinks;
          map_connected_fibers     = test.map;

        } else if ( align_info ) {

          auto     range_fiber   = range8.subspan( 1, 3 );
          ByteType align_data[3] = {range_fiber[0], range_fiber[1], range_fiber[2]};
          map_connected_fibers   = ReadMapFromData( active_links, align_data, number_of_readout_fibers );
          verbose() << "tot fibers " << number_of_readout_fibers << " " << endmsg;
          for ( int io = 0; io < 24; io++ ) { verbose() << "map from raw bank " << map_connected_fibers[io] << endmsg; }
        } else if ( linksMap.getInitStatus( TNumber, PCINumber ) == Tell40LinksMap::InitStatus::FAKE ) {
          connectionMap test       = linksMap.getAllConnection( TNumber, PCINumber );
          number_of_readout_fibers = test.connectedLinks;
          map_connected_fibers     = test.map;
          debug() << "fake map " << number_of_readout_fibers << " " << endmsg;
          for ( int io = 0; io < 24; io++ ) {
            verbose() << "map from FAKE initialization bank " << map_connected_fibers[io] << endmsg;
          }
        } else {
          // use  Tell40LinksMap
          debug() << "link map status initialized now, why not initialized as FAKE? "
                  << linksMap.getInitStatus( TNumber, PCINumber ) << endmsg;
          for ( unsigned int i = 0; i < active_links; i++ ) { map_connected_fibers[i] = i; }
          number_of_readout_fibers = active_links;
        }

        link_start_pointer = ( align_info == 1 ) ? link_start_pointer + 3 : link_start_pointer;

        if ( msgLevel( MSG::VERBOSE ) ) {
          if ( (unsigned int)raw_bank->size() > number_of_readout_fibers + 1 + 3 * align_info ) {
            verbose() << " link " << TNumber << " " << PCINumber << " " << number_of_readout_fibers << " "
                      << raw_bank->size() - number_of_readout_fibers + 1 + 3 * align_info << endmsg;
          }
        }
        if ( m_print_stat ) {
          m_Tell40Size[( TNumber - 1 ) * 2 + PCINumber] +=
              raw_bank->size() - number_of_readout_fibers - 1 - 3 * align_info - 3 * bt;
        }
        // if error bank need to skip 3 bytes
        // checking there is no data corruption in the Tell40
        //        link_start_pointer= (bt==1) ? link_start_pointer+3 : link_start_pointer;

        unsigned current_pointer = link_start_pointer;
        if ( bt == 1 ) {
          // let decode the additional info present in special banks... 3 bytes
          if ( (unsigned int)raw_bank->size() < 1 + 3 * align_info + 3 * bt ) {
            ++m_bank_too_short;
            continue;
          }

          auto range_errorLink = range_data.subspan( current_pointer, 3 );
          if ( msgLevel( MSG::VERBOSE ) )
            verbose() << "link in error  " << current_pointer << " " << std::bitset<8>( range_errorLink[0] ) << " "
                      << std::bitset<8>( range_errorLink[1] ) << " " << std::bitset<8>( range_errorLink[2] ) << endmsg;
          for ( int i = 0; i < 8; i++ ) {
            if ( ( range_errorLink[0] >> i ) & 0x1 ) {
              link_in_error[( TNumber - 1 ) * 2 + PCINumber][16 + i] = true;
              if ( msgLevel( MSG::DEBUG ) )
                debug() << "found desync link " << ( TNumber - 1 ) * 2 + PCINumber << " " << 16 + i << endmsg;
            }
            if ( ( range_errorLink[1] >> i ) & 0x1 ) {
              link_in_error[( TNumber - 1 ) * 2 + PCINumber][8 + i] = true;
              if ( msgLevel( MSG::DEBUG ) )
                debug() << "found desync link " << ( TNumber - 1 ) * 2 + PCINumber << " " << 8 + i << endmsg;
            }
            if ( ( range_errorLink[2] >> i ) & 0x1 ) {
              link_in_error[( TNumber - 1 ) * 2 + PCINumber][i] = true;
              if ( msgLevel( MSG::DEBUG ) )
                debug() << "found desync link " << ( TNumber - 1 ) * 2 + PCINumber << " " << i << endmsg;
            }
          }
        }
        // move pointer to where real hits start
        link_start_pointer = ( bt == 1 ) ? link_start_pointer + 3 : link_start_pointer;
        current_pointer    = link_start_pointer;
        // check that the rest of bank is valid at least from the size point of view
        for ( unsigned int link = 0; link < number_of_readout_fibers; link++ ) {
          if ( current_pointer >= range_data.size() ) {
            ++m_bad_size_of_bank;
            bank_corrupted = true;
            break;
          }
          unsigned int size_of_link = ( ( range_data[current_pointer] & 0xF0 ) >> 4 ) + 1;
          if ( ( size_of_link > 1 and size_of_link <= 6 ) or ( current_pointer + size_of_link ) > range_data.size() ) {
            bank_corrupted = true;
            ++m_bad_size_of_link;
            break;
          }
          current_pointer += size_of_link;
        }
        if ( bank_corrupted ) continue;

        // now the bank can be decoded
        ByteType control_register = range8[0];
        for ( unsigned int link = 0; link < number_of_readout_fibers; link++ ) {
          unsigned int hits_in_frame   = 0;
          unsigned int reroutered_link = map_connected_fibers[link];

          if ( linksMap.linkNumberWithNoHole( TNumber, PCINumber, link ) ) {
            verbose() << " compare " << map_connected_fibers[link] << " "
                      << ( linksMap.linkNumberWithNoHole( TNumber, PCINumber, link ) ).value() << endmsg;
            verbose() << " " << reroutered_link << " "
                      << ( linksMap.linkNumberWithNoHole( TNumber, PCINumber, link ) ).value() << endmsg;
          } else {
            ++m_missing_links_in_map;
            // info() << "missing in map why????" << endmsg;
          }
          if ( msgLevel( MSG::VERBOSE ) ) {
            verbose() << " number of active links " << number_of_readout_fibers << " " << link << " " << reroutered_link
                      << endmsg;
          }

          regionOfLink  = det.getUpgradeDAQInfo()->regionOfLink( TNumber, PCINumber, reroutered_link );
          quarterOfLink = det.getUpgradeDAQInfo()->quarterOfLink( TNumber, PCINumber, reroutered_link );
          if ( msgLevel( MSG::DEBUG ) ) {
            debug() << "region and quarter " << regionOfLink << " " << quarterOfLink << endmsg;
          }
          digits_inTell40.clear();
          digits_inTell40.reserve( 48 );

          ByteType     curr_byte    = range_data[link_start_pointer];
          unsigned int size_of_link = ( ( curr_byte & 0xF0 ) >> 4 ) + 1;
          // single link data
          if ( msgLevel( MSG::DEBUG ) )
            debug() << " new link " << link << " " << size_of_link << " " << link_start_pointer << endmsg;
          if ( msgLevel( MSG::DEBUG ) ) debug() << " new link " << std::bitset<8>( curr_byte ) << endmsg;

          if ( size_of_link > 1 ) {
            bool                              TDCPadded = true;
            auto                              allFrame  = range_data.subspan( link_start_pointer, size_of_link );
            std::array<ByteType, MaxHitBytes> HitsMap   = {};
            std::array<ByteType, MaxTDCBytes> TDCMap    = {};
            unsigned int                      TDCMap_ByteLength = 0;
            // it return byte aligned hitsmap and TDC. The returned value spot if edac counters are in data, it affects
            // TDC length
            auto link_info = splitMuonLinkFrame( control_register, allFrame, HitsMap, TDCMap, TDCMap_ByteLength );
            if ( link_info ) {
              debug() << "Link decoded with EDAC info stored" << endmsg;
              if ( ( link_info )->EDAC_stored ) TDCPadded = false;
            }
            if ( msgLevel( MSG::DEBUG ) ) {
              debug() << "print after new frame access mode  " << size_of_link << " " << TDCPadded << " ";
              for ( unsigned int rt = 0; rt < size_of_link; rt++ ) {
                if ( rt == 6 ) debug() << " ";
                debug() << std::bitset<8>( allFrame[rt] );
              }
              debug() << endmsg;
              debug() << "print HitsMap ";
              for ( unsigned int rt = 0; rt < 6; rt++ ) { debug() << std::bitset<8>( HitsMap[rt] ); }
              debug() << endmsg;
              debug() << "print TDCsMap  ";
              for ( unsigned int rt = 0; rt < std::min( TDCMap_ByteLength, MaxTDCBytes ); rt++ ) {
                debug() << std::bitset<8>( TDCMap[rt] );
              }
              debug() << endmsg;
            }
            // now true decoding of hits map and TDC
            unsigned int pos_in_link        = 0;
            unsigned int nSynch_hits_number = 0;
            unsigned int TDDD               = ( TDCPadded ) ? TDCMap_ByteLength * 2 - 1 : TDCMap_ByteLength * 2;
            unsigned int TDC_value          = 0;
            for ( auto byteIndex = 5; byteIndex >= 0; byteIndex-- ) {
              ByteType data_copy = HitsMap[byteIndex];
              if ( msgLevel( MSG::DEBUG ) )
                debug() << "byteIndex " << byteIndex << " " << std::bitset<8>( data_copy ) << endmsg;
              for ( auto b_it = single_bits<ByteType>{}.begin(); b_it != single_bits<ByteType>{}.end(); ++b_it ) {
                const auto bit = *b_it;
                if ( data_copy & bit ) {
                  if ( msgLevel( MSG::DEBUG ) ) debug() << "found a bit in pos " << b_it.i << pos_in_link << endmsg;
                  hits_in_frame++;
                  LHCb::Detector::Muon::TileID hitTile =
                      ( det.getUpgradeDAQInfo() )
                          ->TileInTell40FrameNoHole( TNumber, PCINumber, reroutered_link, pos_in_link );
                  if ( msgLevel( MSG::DEBUG ) )
                    debug() << " hit " << b_it.i << " " << pos_in_link << " " << reroutered_link << " " << hitTile
                            << " " << nSynch_hits_number << endmsg;
                  if ( hitTile.isDefined() ) {
                    if ( nSynch_hits_number < TDDD ) {
                      if ( nSynch_hits_number > 11 ) {
                        TDC_value = 0;
                      } else {
                        unsigned int odd    = nSynch_hits_number % 2;
                        unsigned int TDCby2 = nSynch_hits_number / 2;
                        if ( odd ) {
                          TDC_value = ( TDCMap[TDCby2] & 0x0F );
                        } else {
                          TDC_value = ( TDCMap[TDCby2] & 0xF0 ) >> 4;
                        }
                      }

                      struct Digit d = {hitTile, TDC_value};
                      if ( msgLevel( MSG::DEBUG ) )
                        debug() << "found a new hit " << hitTile << " time " << TDC_value << endmsg;
                      digits_inTell40.emplace_back( d );

                    } else {

                      // there are too much hits in this nSync, so some of them have not the associated
                      // TDC stored in data
                      struct Digit d = {hitTile, 0};

                      if ( msgLevel( MSG::DEBUG ) )
                        debug() << "found a new hit " << hitTile << " no time associated " << endmsg;
                      // digits_inTell40.emplace_back( d );
                      digits_inTell40.emplace_back( d );
                    }

                    nSynch_hits_number++;
                  } else {
                    debug() << TNumber << " " << PCINumber << " " << reroutered_link << " " << pos_in_link << " "
                            << stationOfTell40 << " " << regionOfLink << endmsg;
                    ++m_invalid_add;
                  }
                }
                // end here
                pos_in_link++;
              }
            }
          }

          // fill stats counters;
          debug() << "info added " << TNumber << " " << PCINumber << " " << reroutered_link << " "
                  << link_in_error[( TNumber - 1 ) * 2 + PCINumber][reroutered_link] << " " << hits_in_frame << endmsg;
          /* commented code below need revsion
          if ( !link_in_error[( TNumber - 1 ) * 2 + PCINumber][reroutered_link] ) {
            local_hits_counters[( TNumber - 1 ) * 2 + PCINumber][reroutered_link] += hits_in_frame;
          } else {
            local_desynch_hits_counters[( TNumber - 1 ) * 2 + PCINumber][reroutered_link] += hits_in_frame;
          }
          if ( !link_in_error[( TNumber - 1 ) * 2 + PCINumber][reroutered_link] ) {
            local_wrong_hits_counters[( TNumber - 1 ) * 2 + PCINumber][reroutered_link] += wrong_hits_in_frame;
          } else {
            local_desynch_wrong_hits_counters[( TNumber - 1 ) * 2 + PCINumber][reroutered_link] += wrong_hits_in_frame;
          }
          if ( !link_in_error[( TNumber - 1 ) * 2 + PCINumber][reroutered_link] ) {
            local_acquired_frame[( TNumber - 1 ) * 2 + PCINumber][reroutered_link]++;
          } else {
            local_desynch_acquired_frame[( TNumber - 1 ) * 2 + PCINumber][reroutered_link]++;
          }
          */
          // move the raw bank pointer to next link
          link_start_pointer = link_start_pointer + size_of_link;

          if ( stationOfTell40 * 16 + regionOfLink * 4 + quarterOfLink < 64 ) {

            decoding[stationOfTell40 * 16 + regionOfLink * 4 + quarterOfLink].insert(
                decoding[stationOfTell40 * 16 + regionOfLink * 4 + quarterOfLink].end(),
                std::make_move_iterator( digits_inTell40.begin() ), std::make_move_iterator( digits_inTell40.end() ) );
          }
        }
      }
    }

    auto addCrossings = [&]( auto f, auto l, auto& dest ) {
      return addCoordsCrossingMap( f, l, dest, compute, nStations );
    };

    unsigned int where = 0;

    unsigned station = 0;

    CommonMuonHits commonHits_sta[4];
    CommonMuonHits commonHits_12{memResource};

    int count_sta = 0;

    for ( auto& decode : decoding ) {
      if ( where % 16 == 15 ) {
        commonHits_sta[station].reserve( count_sta );
        count_sta = 0;
        station++;
      }
      count_sta = count_sta + decode.size();

      where++;
    }
    where   = 0;
    station = 0;

    for ( auto& decode : decoding ) {

      for ( auto i = decode.begin(); i != decode.end(); i = addCrossings( i, decode.end(), commonHits_sta[station] ) )
        ; /* empty on purpose */

      if ( where % 16 == 15 ) {
        stations[station] = CommonMuonStation{det, station, std::move( commonHits_sta[station] )};
        station++;
      }

      where++;
    }

    return MuonHitContainer{std::move( stations )};
  }

  std::array<unsigned int, 24> RawToHits::ReadMapFromData( unsigned int active_links, ByteType data_fiber[3],
                                                           unsigned int& number_of_readout_fibers ) const {

    std::array<unsigned int, 24> map_connected_fibers = {0};
    if ( msgLevel( MSG::VERBOSE ) )
      verbose() << "connected fibers " << std::bitset<8>( data_fiber[0] ) << " " << std::bitset<8>( data_fiber[1] )
                << " " << std::bitset<8>( data_fiber[2] ) << endmsg;
    bool         align_vector[24] = {};
    unsigned int readout_fibers   = 0;
    for ( int i = 0; i < 8; i++ ) {
      if ( ( data_fiber[0] >> i ) & 0x1 ) {
        align_vector[16 + i] = true;
        readout_fibers++;
      }
      if ( ( data_fiber[1] >> i ) & 0x1 ) {
        align_vector[8 + i] = true;
        readout_fibers++;
      }
      if ( ( data_fiber[2] >> i ) & 0x1 ) {
        align_vector[i] = true;
        readout_fibers++;
      }
    }
    if ( msgLevel( MSG::VERBOSE ) ) {
      for ( int i = 0; i < 24; i++ ) { verbose() << i << " " << align_vector[i] << endmsg; }
    }
    unsigned int fib_counter = 0;
    if ( msgLevel( MSG::VERBOSE ) ) verbose() << "how many active link " << active_links << endmsg;
    for ( unsigned int i = 0; i < active_links; i++ ) {
      if ( align_vector[i] ) {
        map_connected_fibers[fib_counter] = i;
        if ( msgLevel( MSG::VERBOSE ) )
          verbose() << "filling " << fib_counter << " " << map_connected_fibers[fib_counter] << endmsg;
        fib_counter++;
      }
    }
    if ( msgLevel( MSG::VERBOSE ) ) {
      for ( int i = 0; i < 24; i++ ) { verbose() << i << " " << align_vector[i] << endmsg; }
    }
    number_of_readout_fibers = readout_fibers;
    return map_connected_fibers;
  }

} // namespace LHCb::MuonUpgrade::DAQ
