/*****************************************************************************\
* (c) Copyright 2000-2021 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#include "MuonSynchFrame.h"

#include "Detector/Muon/TileID.h"
#include "Event/MuonBankVersion.h"
#include "Event/MuonDigit.h"
#include "Event/RawBank.h"
#include "Event/RawEvent.h"
#include "Gaudi/Accumulators.h"
#include "MuonDAQ/MuonDAQDefinitions.h"
#include "MuonDet/DeMuonDetector.h"
#include "MuonDet/MuonNODEBoard.h"
#include "MuonDet/MuonNamespace.h"
#include "MuonDet/MuonTell40Board.h"
#include "MuonDet/MuonTell40PCI.h"
#include "MuonDet/MuonUpgradeStationCabling.h"
#include "MuonDet/MuonUpgradeTSMap.h"

#include "LHCbAlgs/Consumer.h"

#include "Gaudi/Accumulators.h"

#include <bitset>
#include <string>

//-----------------------------------------------------------------------------
// Implementation file for class : MuonTell40Decoding
//
// 2020-05-11 : Alessia Satta
//-----------------------------------------------------------------------------

/**
 *  @author Alessia Satta
 *  @date   2020-05-11
 */
using namespace LHCb;
using namespace Muon::DAQ;

namespace {
  struct Digit {
    Detector::Muon::TileID tile;
    unsigned int           tdc;
    Digit( Detector::Muon::TileID tile, unsigned int tdc ) : tile{tile}, tdc{tdc} {} // C++20: please remove this line
    inline bool operator==( const Digit& a ) { return a.tile == tile && a.tdc == tdc; }
  };
} // namespace

class MuonTell40Decoding
    : public LHCb::Algorithm::Consumer<void( RawEvent const&, MuonDigits const&, DeMuonDetector const& ),
                                       LHCb::DetDesc::usesConditions<DeMuonDetector>> {
public:
  MuonTell40Decoding( const std::string& name, ISvcLocator* pSvcLocator )
      : Consumer( name, pSvcLocator,
                  {{"RawEvent", RawEventLocation::Default},
                   {"Digits", MuonDigitLocation::MuonDigit},
                   {"DeMuonLocation", DeMuonLocation::Default}} ) {}

  void operator()( RawEvent const&, MuonDigits const&, DeMuonDetector const& ) const override;

private:
  Gaudi::Property<unsigned int>                   m_vtype{this, "VType", 2};
  mutable Gaudi::Accumulators::AveragingCounter<> m_not_matched_mc_muon_digit{this, "missing MC digits"};
  mutable Gaudi::Accumulators::Counter<>          m_wrongchannels{this, "total wrong channels"};
};

DECLARE_COMPONENT( MuonTell40Decoding )

void MuonTell40Decoding::operator()( RawEvent const& raw, MuonDigits const& muonDigits,
                                     DeMuonDetector const& muonDet ) const {
  unsigned int       TDC_tmp_vect[12];
  std::vector<Digit> digits;

  // retrieve muon banks
  const auto& mb = raw.banks( RawBank::Muon );

  for ( const auto& rb : mb ) {

    if ( msgLevel( MSG::DEBUG ) )
      debug() << "source ID of the bank "
              << " " << rb->sourceID() << std::hex << rb->sourceID() << std::dec << endmsg;
    // to match source ID nd Tell40 numbering keep the lower part, the higher part is a constant that address A or C
    // side of the muon detector
    unsigned int tell40PCI = rb->sourceID() & 0x00FF;
    if ( msgLevel( MSG::DEBUG ) ) debug() << " tell40 PCI number " << tell40PCI << " and size " << rb->size() << endmsg;

    unsigned int TNumber      = tell40PCI / 2 + 1;
    unsigned int PCINumber    = tell40PCI % 2;
    unsigned int active_links = muonDet.getUpgradeDAQInfo()->getNumberOfActiveLinkInTell40PCI( TNumber, PCINumber );
    if ( msgLevel( MSG::DEBUG ) )
      debug() << "Tell40 and PCI  number " << TNumber << " " << PCINumber << " "
              << "active links " << active_links << endmsg;
    if ( rb->size() < 1 ) {
      error() << "error: Muon Bank is toot short " << endmsg;
      break;
    }
    auto range8 = rb->range<ByteType>();
    if ( msgLevel( MSG::DEBUG ) )
      debug() << range8.size() << " first byte of bank " << std::bitset<8>( range8[0] )
              << endmsg; // first byte is a control word
    // let's decode
    unsigned int ZS_applied   = ( range8[0] & 0x05 );
    unsigned int EDAC_applied = ( range8[0] & 0x08 ) >> 3;
    unsigned int synch_evt    = ( range8[0] & 0x10 ) >> 4;
    unsigned int align_info   = ( range8[0] & 0x20 ) >> 5;
    if ( synch_evt ) break;
    if ( msgLevel( MSG::DEBUG ) )
      debug() << " ZS_applied -  EDAC - aligned_info " << ZS_applied << " " << EDAC_applied << " " << align_info
              << endmsg;
    if ( align_info && rb->size() < 4 ) {
      error() << "error: Muon Bank is toot short " << endmsg;
      break;
    }
    unsigned int number_of_readout_fibers = 0;
    unsigned int map_connected_fibers[24] = {99};
    if ( !align_info ) {
      for ( unsigned int i = 0; i < active_links; i++ ) { map_connected_fibers[i] = i; }
      number_of_readout_fibers = active_links;
      if ( (unsigned int)rb->size() < active_links + 1 ) {
        error() << "error muon bak too short " << endmsg;
        break;
      }
    } else {
      auto range_fiber = range8.subspan( 1, 3 );
      if ( msgLevel( MSG::DEBUG ) )
        debug() << "connected fibers " << std::bitset<8>( range_fiber[0] ) << " " << std::bitset<8>( range_fiber[1] )
                << " " << std::bitset<8>( range_fiber[2] ) << endmsg;
      bool         align_vector[24] = {false};
      unsigned int readout_fibers   = 0;
      for ( int i = 0; i < 8; i++ ) {
        if ( ( range_fiber[0] >> i ) & 0x1 ) {
          align_vector[16 + i] = true;
          readout_fibers++;
        }
        if ( ( range_fiber[1] >> i ) & 0x1 ) {
          align_vector[8 + i] = true;
          readout_fibers++;
        }
        if ( ( range_fiber[2] >> i ) & 0x1 ) {
          align_vector[i] = true;
          readout_fibers++;
        }
      }
      if ( msgLevel( MSG::VERBOSE ) ) {
        for ( int i = 0; i < 24; i++ ) { verbose() << align_vector[i] << endmsg; }
      }

      unsigned int fib_counter = 0;
      for ( unsigned int i = 0; i < active_links; i++ ) {
        if ( align_vector[i] ) {
          map_connected_fibers[fib_counter] = i;
          fib_counter++;
        }
      }
      number_of_readout_fibers = readout_fibers;
      if ( (unsigned int)rb->size() < readout_fibers + 1 + 3 ) {
        error() << "error muon bak too short " << endmsg;
        break;
      }
    }

    if ( msgLevel( MSG::DEBUG ) )
      debug() << " number_of_readout_fibers connected is " << number_of_readout_fibers << endmsg;
    if ( msgLevel( MSG::VERBOSE ) ) {
      for ( int i = 0; i < 24; i++ ) verbose() << i << " " << map_connected_fibers[i] << endmsg;
    }
    // get the data part removing the header part (all links data are concatenated here
    auto range_data = range8.subspan( 1 + 3 * align_info );

    unsigned int link_start_pointer = 0;
    // loop on links
    for ( unsigned int link = 0; link < number_of_readout_fibers; link++ ) {
      // get the link number
      unsigned int reroutered_link = map_connected_fibers[link];
      ByteType     curr_byte       = range_data[link_start_pointer];
      // calculate the length for this link
      unsigned int size_of_link = ( ( curr_byte & 0xF0 ) >> 4 ) + 1;
      if ( msgLevel( MSG::DEBUG ) )
        debug() << " first byte for link " << link << " " << std::bitset<8>( range_data[link_start_pointer] ) << endmsg;
      if ( msgLevel( MSG::DEBUG ) )
        debug() << " link " << link << " size " << size_of_link << " link_start_pointer " << link_start_pointer
                << endmsg;
      // get all link data  (not other lnk here)
      // if size_link==1 no hits have been transimitted only header
      if ( size_of_link > 1 ) {
        // first part is hits map
        auto range_link_HitsMap = range_data.subspan( link_start_pointer, 7 );
        std::fill( &( TDC_tmp_vect[0] ), &( TDC_tmp_vect[12] ), 0 );
        // the max number of TDC is 12
        // TDC start at byte number 6 second half
        auto range_link_TDC = range_data.subspan( link_start_pointer + 6, size_of_link - 6 );
        if ( msgLevel( MSG::DEBUG ) ) debug() << "TDC range size " << range_link_TDC.size() << endmsg;
        // now read TDC: TDC uses 4 bits and only fill second half of firts byte
        bool         first_TDC_byte = true;
        bool         last_TDC_byte  = false;
        unsigned int TDC_counter    = 0;
        unsigned int count_byte     = 0;
        for ( ByteType data_TDC : range_link_TDC ) {
          count_byte++;
          // ByteType data_TDC_copy=data_TDC;
          unsigned int TDC1 = 0;
          unsigned int TDC2 = 0;
          if ( count_byte == ( size_of_link - 6 ) ) last_TDC_byte = true;
          if ( msgLevel( MSG::DEBUG ) ) debug() << "Byte with TDC INFO " << std::bitset<8>( data_TDC ) << endmsg;
          if ( !first_TDC_byte ) {
            TDC1                      = ( data_TDC & 0xF0 ) >> 4;
            TDC_tmp_vect[TDC_counter] = TDC1;
            TDC_counter++;
          }
          TDC2 = ( data_TDC & 0x0F );
          if ( !last_TDC_byte ) {
            TDC_tmp_vect[TDC_counter] = TDC2;
            TDC_counter++;

          } else if ( last_TDC_byte && TDC_counter < 11 ) {
            TDC_tmp_vect[TDC_counter] = TDC2;
            TDC_counter++;
          }

          if ( msgLevel( MSG::DEBUG ) ) {
            if ( first_TDC_byte ) {
              debug() << TDC2 << endmsg;
            } else {
              debug() << range_link_TDC.size() << " TDC ocunters " << TDC1 << " " << TDC2 << " " << TDC_counter
                      << endmsg;
            }
          }
          // after first byte the  first_TDC_byte must be set to false
          first_TDC_byte = false;
        }
        if ( msgLevel( MSG::DEBUG ) ) {
          debug() << "Stored TDC ";
          for ( unsigned int ii = 0; ii < TDC_counter; ii++ ) { debug() << TDC_tmp_vect[ii] << " "; }
          debug() << endmsg;
        }
        // now read hits map
        bool first_hitmap_byte          = false;
        bool last_hitmap_byte           = true;
        count_byte                      = 0;
        unsigned int pos_in_link        = 0;
        unsigned int nSynch_hits_number = 0;
        // navigate the map bytes  in reversed direction : this is how the nSynch writes it
        for ( auto r = range_link_HitsMap.rbegin(); r < range_link_HitsMap.rend(); r++ ) {
          ByteType data = *r;
          count_byte++;
          if ( count_byte == 7 ) first_hitmap_byte = true;
          if ( count_byte > 7 ) break;
          ByteType data_copy = data;
          if ( msgLevel( MSG::DEBUG ) )
            debug() << " new byte " << count_byte << " " << last_hitmap_byte << " " << std::bitset<8>( data ) << endmsg;
          // navigate the single byte of the map in reversed direction : this is how the nSynch writes it
          for ( auto b_it = single_bits<ByteType>{}.begin(); b_it != single_bits<ByteType>{}.end(); ++b_it ) {
            const auto bit = *b_it;

            if ( first_hitmap_byte && b_it.i < 4 ) continue;

            if ( last_hitmap_byte && b_it.i > 3 ) continue;

            if ( data_copy & bit ) {
              if ( msgLevel( MSG::DEBUG ) )
                debug() << "found a bit in pos " << b_it.i << " " << reroutered_link << " " << pos_in_link << " "
                        << nSynch_hits_number << endmsg;
              Detector::Muon::TileID hitTile =
                  muonDet.getUpgradeDAQInfo()->TileInTell40Frame( TNumber, PCINumber, reroutered_link, pos_in_link );
              if ( hitTile.isValid() ) {
                if ( msgLevel( MSG::DEBUG ) )
                  debug() << " hitTile " << TNumber << " " << PCINumber << " " << link << " " << pos_in_link << " "
                          << hitTile.toString() << endmsg;
                if ( msgLevel( MSG::DEBUG ) ) debug() << " hitTile " << hitTile << endmsg;
              } else {
                ++m_wrongchannels;
                int ode   = -1;
                int synch = -1;
                muonDet.getUpgradeDAQInfo()->getODENumberAndSynch( TNumber, PCINumber, reroutered_link, ode, synch );
                error() << "a bit set 1 where not possible need to investigate " << count_byte << " "
                        << std::bitset<8>( data ) << " " << b_it.i << endmsg;
                error() << TNumber << " " << PCINumber << " " << link << " " << reroutered_link << " " << pos_in_link
                        << endmsg;
                error() << "ODE " << ode << " " << muonDet.getUpgradeDAQInfo()->getODENameInECS( ode ) << " synch "
                        << synch << "channel " << pos_in_link << endmsg;
              }
              if ( nSynch_hits_number < TDC_counter ) {
                if ( msgLevel( MSG::DEBUG ) ) debug() << "  time " << TDC_tmp_vect[nSynch_hits_number] << endmsg;
                struct Digit d = {hitTile, TDC_tmp_vect[nSynch_hits_number]};
                digits.emplace_back( d );
              } else {
                struct Digit d = {hitTile, 0};
                digits.emplace_back( d );
                if ( msgLevel( MSG::DEBUG ) ) debug() << "No Time fot hit " << endmsg;
              }

              nSynch_hits_number++;
            }
            pos_in_link++;
          }
          last_hitmap_byte = false;
        }
      }
      link_start_pointer = link_start_pointer + size_of_link;
    }
  }
  if ( msgLevel( MSG::DEBUG ) ) debug() << " the number of reconstructed  digits is " << digits.size() << endmsg;
  if ( msgLevel( MSG::DEBUG ) )
    debug() << " The number of MC  hits in input container is " << muonDigits.size() << endmsg;

  // add the simplest check
  if ( digits.size() != muonDigits.size() ) error() << " discrepancy in simulated and reconstructed digits" << endmsg;
  for ( const auto& idigit : ( muonDigits ) ) {
    Detector::Muon::TileID digitTile   = idigit->key();
    unsigned int           time        = idigit->TimeStamp();
    const struct Digit     d_full      = {digitTile, time};
    const struct Digit     d_truncated = {digitTile, 0};
    if ( std::find( digits.begin(), digits.end(), d_full ) != digits.end() ) {
      if ( msgLevel( MSG::VERBOSE ) ) verbose() << "find digits " << endmsg;
    } else if ( std::find( digits.begin(), digits.end(), d_truncated ) != digits.end() ) {
      if ( msgLevel( MSG::VERBOSE ) )
        verbose() << "find digits with no TDC associated " << digitTile.toString() << " time " << time << endmsg;
    } else {
      error() << "not found " << digitTile << " " << time << endmsg;
      ++m_not_matched_mc_muon_digit;
    }
  }
}
