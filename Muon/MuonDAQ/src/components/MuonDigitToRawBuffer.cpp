/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#include "Detector/Muon/TileID.h"

#include "MuonDAQ/MuonHLTDigitFormat.h"
#include "MuonDAQ/SortDigitInL1.h"
#include "MuonDAQ/SortDigitInODE.h"

#include "Event/BankWriter.h"
#include "Event/MuonBankVersion.h"
#include "Event/MuonDigit.h"
#include "Event/RawBank.h"
#include "Event/RawEvent.h"
#include "MuonDet/DeMuonDetector.h"
#include "MuonDet/MuonDAQHelper.h"
#include "MuonDet/MuonL1Board.h"
#include "MuonDet/MuonNamespace.h"
#include "MuonDet/MuonODEBoard.h"
#include "MuonDet/MuonStationCabling.h"
#include "MuonDet/MuonTSMap.h"

#include "LHCbAlgs/Consumer.h"

#include "fmt/format.h"

#include <array>
#include <string>
#include <vector>

namespace {
  struct DigitsHelper {
    std::array<std::vector<unsigned int>, MuonDAQHelper::maxODENumber>   digitsInODE;
    std::array<unsigned int, MuonDAQHelper::maxODENumber>                firedInODE;
    std::array<std::vector<unsigned int>, MuonDAQHelper::maxTell1Number> digitsInL1;
    std::array<std::vector<unsigned int>, MuonDAQHelper::maxTell1Number> padInL1;
    std::array<unsigned int, MuonDAQHelper::maxTell1Number * 4>          firedInPP;
    DigitsHelper( DeMuonDetector const& muonDet ) {
      auto& daqInfo = *muonDet.getDAQInfo();
      for ( unsigned int i = 0; i < daqInfo.TotTellNumber(); i++ ) {
        for ( unsigned int iODE = 0; iODE < daqInfo.ODEInTell1( i ); iODE++ ) {
          unsigned int odenumber = daqInfo.getODENumberInTell1( i, iODE ) - 1;
          digitsInODE[odenumber].resize( 0 );
          firedInODE[odenumber] = 0;
        }
        for ( int j = 0; j < 4; j++ ) firedInPP[i * 4 + j] = 0;
      }
    }
  };
} // namespace

namespace LHCb {

  /**
   *  @author Alessia Satta
   *  @date   2004-01-19
   */
  class MuonDigitToRawBuffer : public Algorithm::Consumer<void( MuonDigits const&, DeMuonDetector const& ),
                                                          DetDesc::usesConditions<DeMuonDetector>> {
  public:
    MuonDigitToRawBuffer( std::string const& name, ISvcLocator* pSvcLocator )
        : Consumer{name,
                   pSvcLocator,
                   {{"Digits", MuonDigitLocation::MuonDigit}, {"MuonDetectorLocation", DeMuonLocation::Default}}} {}

    void operator()( MuonDigits const&, DeMuonDetector const& ) const override;

  private:
    void         ProcessDC06( MuonDigits const&, RawEvent&, DeMuonDetector const& ) const;
    void         ProcessV1( MuonDigits const&, RawEvent&, DeMuonDetector const& ) const;
    DigitsHelper ProcessDigitDC06( MuonDigits const&, DeMuonDetector const& ) const;
    DigitsHelper ProcessDigitV1( MuonDigits const&, DeMuonDetector const& ) const;
    void         ProcessPads( MuonDAQHelper const&, DigitsHelper& ) const;

    Gaudi::Property<unsigned int> m_vtype{this, "VType", 2};

    // This is actually an update handle, in the sense that the RawEvent will be modified
    DataObjectReadHandle<RawEvent> m_raw{this, "RawEvent", RawEventLocation::Default};
  };

  DECLARE_COMPONENT_WITH_ID( MuonDigitToRawBuffer, "MuonDigitToRawBuffer" )

} // namespace LHCb

void LHCb::MuonDigitToRawBuffer::operator()( MuonDigits const& digits, DeMuonDetector const& muonDet ) const {
  switch ( m_vtype ) {
  case MuonBankVersion::DC06:
    ProcessDC06( digits, *m_raw.get(), muonDet );
    break;
  case MuonBankVersion::v1:
    ProcessV1( digits, *m_raw.get(), muonDet );
    break;
  }
}

void LHCb::MuonDigitToRawBuffer::ProcessDC06( const LHCb::MuonDigits& digits, LHCb::RawEvent& raw,
                                              DeMuonDetector const& muonDet ) const {

  auto  dh      = ProcessDigitDC06( digits, muonDet );
  auto& daqInfo = *muonDet.getDAQInfo();
  ProcessPads( daqInfo, dh );

  for ( unsigned int i = 0; i < daqInfo.TotTellNumber(); i++ ) {
    unsigned int ODE_in_L1   = daqInfo.ODEInTell1( i );
    unsigned int totalChInL1 = 0;
    unsigned int odeInTell1  = 0;

    for ( unsigned int iODE = 0; iODE < ODE_in_L1; iODE++ ) {
      unsigned int odenumber = daqInfo.getODENumberInTell1( i, iODE );
      // build header == channels fired for each ode
      unsigned int firedChannels = dh.firedInODE[odenumber - 1];
      // fill in bank.....
      totalChInL1 = totalChInL1 + firedChannels;
      odeInTell1++;
    }
    // now the dimension of the bank can be fixed....
    // the algo if (odeInTell1+2*totalChInL1+0.5*totalChInL1)/4
    unsigned int bankLenght = 0;
    unsigned int bankLenPad = 0;
    if ( i >= daqInfo.M1TellNumber() ) bankLenPad = ( dh.padInL1[i].size() + 1 ) * 16;

    unsigned int bankLenAdd  = odeInTell1 * 8 + 8 * totalChInL1;
    unsigned int bankLenTime = 4 * totalChInL1;

    // info()<<"ode in Tell1 "<<odeInTell1<<" tot ch "<<totalChInL1<<endmsg  ;
    if ( bankLenPad % 32 != 0 ) {
      bankLenght = bankLenght + bankLenPad / 32 + 1;
    } else {
      bankLenght = bankLenght + bankLenPad / 32;
    }
    if ( bankLenAdd % 32 != 0 ) {
      bankLenght = bankLenght + bankLenAdd / 32 + 1;
    } else {
      bankLenght = bankLenght + bankLenAdd / 32;
    }
    if ( bankLenTime % 32 != 0 ) {
      bankLenght = bankLenght + bankLenTime / 32 + 1;
    } else {
      bankLenght = bankLenght + bankLenTime / 32;
    }

    //    info()<<"Tell 1 "<<i<<" "<<bankLenPad<<" "
    //<<bankLenAdd<<" "<<bankLenTime<<" "<<bankLenght<<endmsg;
    BankWriter bank{bankLenght};
    if ( i >= daqInfo.M1TellNumber() ) {
      short padSize = dh.padInL1[i].size();
      bank << padSize;
      for ( const auto& padADD : dh.padInL1[i] ) {
        // fill the pad address
        bank << padADD;
      }
    }
    // padding at the end
    short nullWord = 0;
    if ( bankLenPad % 32 != 0 ) bank << nullWord;
    //  info()<<"after creation "<<sizeof(int)<<" "<<sizeof(char)<<endmsg;
    for ( unsigned int iODE = 0; iODE < ODE_in_L1; iODE++ ) {
      unsigned int odenumber = daqInfo.getODENumberInTell1( i, iODE );

      // build header == channels fired for each ode
      unsigned int firedChannels = dh.firedInODE[odenumber - 1];
      // //fill in bank.....
      unsigned char fired = (unsigned char)firedChannels;
      bank << fired;
    }

    for ( unsigned int iODE = 0; iODE < ODE_in_L1; iODE++ ) {
      unsigned int odenumber = daqInfo.getODENumberInTell1( i, iODE ) - 1;

      for ( const auto& digit : dh.digitsInODE[odenumber] ) {
        MuonHLTDigitFormat temp( digit, MuonBankVersion::DC06 );
        unsigned int       adress = temp.getAddress();
        unsigned char      chad   = (unsigned char)adress;
        bank << chad;
      }
    }
    // padding if needed
    if ( bankLenAdd % 32 != 0 ) {
      unsigned int padding = ( bankLenAdd % 32 ) / 8;
      for ( unsigned int addpad = 0; addpad < 4 - padding; addpad++ ) {
        // the padding is set to zero
        unsigned char chad = (unsigned char)0;
        bank << chad;
      }
    }

    bool          even     = false;
    unsigned char time     = 0;
    unsigned int  timetemp = 0;

    for ( unsigned int iODE = 0; iODE < ODE_in_L1; iODE++ ) {
      unsigned int odenumber = daqInfo.getODENumberInTell1( i, iODE ) - 1;

      for ( const auto& digit : dh.digitsInODE[odenumber] ) {
        MuonHLTDigitFormat temp1( digit, MuonBankVersion::DC06 );
        unsigned int       time1 = temp1.getTime();
        if ( even ) {
          timetemp = timetemp + ( time1 << 4 );
        } else {
          timetemp = time1;
        }
        time = (unsigned char)timetemp;

        if ( even ) bank << time;
        even = !even;
      }
    }
    if ( even ) bank << time;
    // fill with zeros the padding
    if ( bankLenTime % 32 != 0 ) {
      unsigned int padding = ( bankLenTime % 32 ) / 8;
      for ( unsigned int addpad = 0; addpad < padding; addpad++ ) {
        // the padding is set to zero
        unsigned char chad = (unsigned char)0;
        bank << chad;
      }
    }

    raw.addBank( i, RawBank::Muon, 1, bank.dataBank() );
  }
}

void LHCb::MuonDigitToRawBuffer::ProcessV1( const LHCb::MuonDigits& digits, LHCb::RawEvent& raw,
                                            DeMuonDetector const& muonDet ) const {
  auto  dh      = ProcessDigitV1( digits, muonDet );
  auto& daqInfo = *muonDet.getDAQInfo();
  ProcessPads( daqInfo, dh );

  unsigned int pp_counter[4];

  for ( unsigned int i = 0; i < daqInfo.TotTellNumber(); i++ ) {
    for ( int pp = 0; pp < 4; pp++ ) pp_counter[pp] = dh.firedInPP[i * 4 + pp];
    unsigned int totHit = 0;
    for ( int pp = 0; pp < 4; pp++ ) totHit = totHit + pp_counter[pp];
    unsigned int totalChInL1 = 0;
    // now the dimension of the bank can be fixed....
    // the algo if (odeInTell1+2*totalChInL1+0.5*totalChInL1)/4
    unsigned int bankLenght = 0;
    unsigned int bankLenPad = 0;

    bankLenPad              = ( dh.padInL1[i].size() + 1 ) * 16 + 16;
    totalChInL1             = ( dh.digitsInL1[i].size() );
    unsigned int bankLenHit = 16 * totalChInL1 + 16 * 4;

    if ( totHit != totalChInL1 ) {
      throw GaudiException( fmt::format( " wrong # hit in L1: cross check failed {} {}", totHit, totalChInL1 ),
                            "MuonDigitToRawBuffer", StatusCode::FAILURE );
    }

    // info()<<"ode in Tell1 "<<odeInTell1<<" tot ch "<<totalChInL1<<endmsg  ;
    if ( bankLenPad % 32 != 0 ) {
      bankLenght = bankLenght + bankLenPad / 32 + 1;
    } else {
      bankLenght = bankLenght + bankLenPad / 32;
    }
    if ( bankLenHit % 32 != 0 ) {
      bankLenght = bankLenght + bankLenHit / 32 + 1;
    } else {
      bankLenght = bankLenght + bankLenHit / 32;
    }

    BankWriter bank( bankLenght );
    short      padSize = dh.padInL1[i].size();
    short      evt     = 0;

    bank << padSize;
    bank << evt;
    if ( msgLevel( MSG::DEBUG ) ) debug() << i << " pad size " << padSize << " " << bankLenPad << endmsg;

    for ( std::vector<unsigned int>::iterator itPad = dh.padInL1[i].begin(); itPad < dh.padInL1[i].end(); itPad++ ) {
      // fill the pad address
      short padADD = *itPad;
      bank << padADD;
    }

    // padding at the end
    short nullWord = 0;
    if ( bankLenPad % 32 != 0 ) bank << nullWord;

    std::vector<unsigned int>::iterator itHit;
    for ( unsigned int pp_data = 0; pp_data < 4; pp_data++ ) {
      bank << (short)pp_counter[pp_data];
      unsigned int hit_counter = 0;
      if ( pp_data == 0 ) itHit = dh.digitsInL1[i].begin();
      for ( hit_counter = 0; hit_counter < pp_counter[pp_data]; hit_counter++ ) {
        short hit = (short)( *itHit );
        if ( msgLevel( MSG::DEBUG ) ) debug() << "'digit written " << ( hit & 0x0FFF ) << endmsg;
        bank << hit;
        itHit++;
      }
    }
    if ( bankLenHit % 32 != 0 ) bank << nullWord;
    raw.addBank( i, RawBank::Muon, MuonBankVersion::v1, bank.dataBank() );
  }
}

DigitsHelper LHCb::MuonDigitToRawBuffer::ProcessDigitDC06( const LHCb::MuonDigits& digit,
                                                           DeMuonDetector const&   muonDet ) const {
  DigitsHelper dh{muonDet};
  for ( const auto& idigit : digit ) {
    Detector::Muon::TileID digitTile = idigit->key();
    unsigned int           time      = idigit->TimeStamp();
    auto                   address   = muonDet.getDAQInfo()->DAQaddressInODE( digitTile, false );
    if ( address.position >= 192 ) info() << " che diavolo succede " << endmsg;

    if ( msgLevel( MSG::DEBUG ) ) {
      debug() << "ODENumber " << address.ODENumber << endmsg;
      debug() << " digitOutputPosition " << address.position << endmsg;
    }
    MuonHLTDigitFormat temp( MuonBankVersion::DC06 );
    temp.setAddress( address.position );
    temp.setTime( time );
    unsigned int digitInDAQ = temp.getWord();
    dh.digitsInODE[address.ODENumber - 1].push_back( digitInDAQ );
    dh.firedInODE[address.ODENumber - 1]++;
  }

  for ( unsigned int i = 0; i < MuonDAQHelper::maxODENumber; i++ ) {
    std::stable_sort( dh.digitsInODE[i].begin(), dh.digitsInODE[i].end(), SortDigitInODE() );
  }
  return dh;
}

void LHCb::MuonDigitToRawBuffer::ProcessPads( MuonDAQHelper const& daqInfo, DigitsHelper& dh ) const {
  for ( unsigned int i = daqInfo.M1TellNumber(); i < daqInfo.TotTellNumber(); i++ ) {
    // get Tell1 information
    std::string L1path = daqInfo.Tell1Name( i );

    SmartDataPtr<MuonL1Board> l1( detSvc(), L1path );
    // this method implies 5 stations ! Don't use it !
    //    int station=l1->getStation();
    // patch with this for the moment
    std::string::size_type findLastSeparator = L1path.rfind( "/" );
    std::string            stname            = L1path.substr( findLastSeparator + 1, 2 );

    std::string cablingBasePath = daqInfo.getBasePath( stname );

    unsigned int padsInTell1 = 0;
    unsigned int ODE_in_L1   = daqInfo.ODEInTell1( i );
    unsigned int maxPads     = 0;
    unsigned int TSInODE     = 0;

    // loop on ODE
    unsigned int iODE = 0;
    for ( iODE = 0; iODE < ODE_in_L1; iODE++ ) {

      if ( msgLevel( MSG::DEBUG ) ) debug() << " start a new ODE " << endmsg;

      unsigned int odenumber = daqInfo.getODENumberInTell1( i, iODE ) - 1;

      std::string                ODEpath = cablingBasePath + l1->getODEName( iODE );
      SmartDataPtr<MuonODEBoard> ode( detSvc(), ODEpath );
      // how many digit in the TS?
      TSInODE = ode->getTSNumber();
      if ( msgLevel( MSG::DEBUG ) ) debug() << "TS " << TSInODE << endmsg;

      SmartDataPtr<MuonTSMap> TS( detSvc(), cablingBasePath + ode->getTSName( 0 ) );
      long                    channelInTS = TS->numberOfOutputSignal();
      if ( TS->numberOfLayout() == 2 ) {
        maxPads = TS->gridXLayout( 0 ) * TS->gridYLayout( 1 );
      } else {
        maxPads = channelInTS;
      }
      if ( msgLevel( MSG::DEBUG ) ) debug() << " max pad " << maxPads << " " << channelInTS << endmsg;

      std::vector<unsigned int> list_input( channelInTS, 0 );
      // debug()<<" channelInTS "<<channelInTS<<endmsg;
      // build header == channels fired for each ode
      unsigned int TSnumberInODE = 0;
      for ( auto itDigit = dh.digitsInODE[odenumber].begin(); itDigit < dh.digitsInODE[odenumber].end(); itDigit++ ) {
        MuonHLTDigitFormat temp( *itDigit, MuonBankVersion::DC06 );
        unsigned int       address = temp.getAddress();
        if ( msgLevel( MSG::DEBUG ) )
          debug() << address << " " << channelInTS << " " << TSInODE << " " << TSnumberInODE << endmsg;

        if ( itDigit == dh.digitsInODE[odenumber].begin() ) { TSnumberInODE = address / channelInTS; }
        // debug()<<" address "<<address<<" "<<TSnumberInODE<<endmsg;
        bool swap = true;

        if ( channelInTS * ( TSnumberInODE + 1 ) <= address || itDigit == dh.digitsInODE[odenumber].end() - 1 ) {
          // info()<<" entra qui "<<TSnumberInODE<<endmsg;
          if ( itDigit == ( dh.digitsInODE[odenumber].end() - 1 ) && address < channelInTS * ( TSnumberInODE + 1 ) ) {
            list_input[address - channelInTS * TSnumberInODE] = 1;
            swap                                              = false;
          }
          std::string TSPath = cablingBasePath + ode->getTSName( TSnumberInODE );
          // debug()<<" TSPath "<<  TSPath <<endmsg;
          std::vector<unsigned int> resultsAddress = daqInfo.padsinTS( list_input, TSPath );
          // store the output
          if ( msgLevel( MSG::DEBUG ) ) debug() << "size " << resultsAddress.size() << endmsg;

          std::transform( resultsAddress.begin(), resultsAddress.end(), std::back_inserter( dh.padInL1[i] ),
                          [&]( const auto& pad ) { return padsInTell1 + maxPads * TSnumberInODE + pad; } );

          // add te offset padsInTell1;
          // padsInTell1=padsInTell1+maxPads;
          // clear the area
          std::fill( list_input.begin(), list_input.end(), 0 );

          TSnumberInODE = address / channelInTS;
          if ( swap ) {
            if ( msgLevel( MSG::DEBUG ) ) {
              debug() << "adding in position " << address - channelInTS * TSnumberInODE << " " << address << endmsg;
              debug() << list_input.size() << endmsg;
            }

            list_input[address - channelInTS * TSnumberInODE] = 1;
            // add the pads mechanism....
            if ( itDigit == dh.digitsInODE[odenumber].end() - 1 ) {
              if ( msgLevel( MSG::DEBUG ) ) debug() << " before " << endmsg;
              const auto& resultsAddress = daqInfo.padsinTS( list_input, TSPath );
              if ( msgLevel( MSG::DEBUG ) ) debug() << " after " << endmsg;
              std::transform( resultsAddress.begin(), resultsAddress.end(), std::back_inserter( dh.padInL1[i] ),
                              [&]( const auto& pad ) { return padsInTell1 + maxPads * TSnumberInODE + pad; } );
            }
          }
        } else {
          list_input[address - channelInTS * TSnumberInODE] = 1;
        }
      }
      padsInTell1 = padsInTell1 + maxPads * TSInODE;

    } // end of loop on ODEs
  }
}

DigitsHelper LHCb::MuonDigitToRawBuffer::ProcessDigitV1( const LHCb::MuonDigits& digit,
                                                         DeMuonDetector const&   muonDet ) const {
  DigitsHelper dh{muonDet};
  auto&        daqInfo = *muonDet.getDAQInfo();
  for ( const auto& idigit : digit ) {
    Detector::Muon::TileID digitTile = idigit->key();
    unsigned int           time      = idigit->TimeStamp();

    if ( msgLevel( MSG::DEBUG ) )
      debug() << "Processing V1 digit: " << digitTile.toString() << " time " << time << endmsg;

    auto addressInL1 = daqInfo.DAQaddressInL1( digitTile );
    if ( msgLevel( MSG::DEBUG ) ) debug() << " L1 add " << addressInL1.ODENumber << endmsg;

    auto addressInODE = daqInfo.DAQaddressInODE( digitTile, false );
    if ( msgLevel( MSG::DEBUG ) ) {
      debug() << "ODENumber " << addressInODE.ODENumber << endmsg;
      debug() << " digitOutputPosition " << addressInL1.position << endmsg;
    }

    std::string l1name  = daqInfo.findL1( digitTile );
    int         L1Index = daqInfo.findL1Index( l1name );
    if ( msgLevel( MSG::DEBUG ) ) debug() << "Found index for L1board: " << l1name << " " << L1Index << endmsg;

    unsigned int pp_num = daqInfo.getPPNumber( L1Index, addressInODE.ODENumber );

    MuonHLTDigitFormat temp( MuonBankVersion::v1 );
    temp.setAddress( addressInL1.position );
    temp.setTime( time );
    unsigned int digitInDAQ = temp.getWord();

    dh.digitsInL1[L1Index].push_back( digitInDAQ );
    dh.firedInPP[L1Index * 4 + pp_num]++;

    MuonHLTDigitFormat tempode( MuonBankVersion::v1 );
    tempode.setAddress( addressInODE.position );
    tempode.setTime( time );
    digitInDAQ = tempode.getWord();

    dh.digitsInODE[addressInODE.ODENumber - 1].push_back( digitInDAQ );
    dh.firedInODE[addressInODE.ODENumber - 1]++;
  }

  for ( unsigned int i = 0; i < MuonDAQHelper::maxTell1Number; i++ ) {
    if ( msgLevel( MSG::DEBUG ) ) debug() << " start sorting " << dh.digitsInL1[i].size() << endmsg;
    std::stable_sort( dh.digitsInL1[i].begin(), dh.digitsInL1[i].end(), SortDigitInL1() );
  }
  for ( unsigned int i = 0; i < MuonDAQHelper::maxODENumber; i++ ) {
    if ( msgLevel( MSG::DEBUG ) ) debug() << " start sorting " << dh.digitsInODE[i].size() << endmsg;
    std::stable_sort( dh.digitsInODE[i].begin(), dh.digitsInODE[i].end(), SortDigitInODE() );
  }
  return dh;
}
