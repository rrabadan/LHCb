/*****************************************************************************\
 * (c) Copyright 2000-2021 CERN for the benefit of the LHCb Collaboration      *
 *                                                                             *
 * This software is distributed under the terms of the GNU General Public      *
 * Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
 *                                                                             *
 * In applying this licence, CERN does not waive the privileges and immunities *
 * granted to it by virtue of its status as an Intergovernmental Organization  *
 * or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once

#include "Event/RawBank.h"
#include "Event/RawEvent.h"
#include "Kernel/STLExtensions.h"
#include "MuonDAQ/MuonDAQDefinitions.h"
#include "MuonDet/DeMuonDetector.h"
#include "MuonDet/MuonNamespace.h"
#include "MuonDet/MuonTilePositionUpgrade.h"

#include "GaudiKernel/ToolHandle.h"

#include <boost/numeric/conversion/cast.hpp>

#include <array>
#include <bitset>
#include <functional>
#include <iostream>
#include <optional>
#include <string>
#include <vector>
/**
 *  This is the muon reconstruction algorithm
 *  This just crosses the logical strips back into pads
 */

using namespace Muon::DAQ;

namespace LHCb::MuonUpgrade::DAQ {

  struct LinkFrameInfo {
    bool wrong_size   = false;
    bool EDAC_stored  = false;
    bool single_error = false;
    bool double_error = false;
  };

  std::optional<LinkFrameInfo> splitMuonLinkFrame( ByteType control_register, LHCb::span<const ByteType> allFrame,
                                                   LHCb::span<ByteType, MaxHitBytes> HitsMap,
                                                   LHCb::span<ByteType, MaxTDCBytes> TDCMap,
                                                   unsigned int&                     TDCMap_ByteLength ) {
    // allframe is single fiber data the length is written inside but it cannot be longer than 13
    // first interpret the setting used in Tell40
    LinkFrameInfo out_info;
    // leave commented
    // unsigned int ZS_applied         = ( control_register & 0x05 );
    // unsigned int EDAC_applied       = ( control_register & 0x08 ) >> 3;
    // unsigned int synch_evt          = ( control_register & 0x10 ) >> 4;
    // unsigned int align_info         = ( control_register & 0x20 ) >> 5;
    unsigned int EDAC_info_stored = ( control_register & 0x40 ) >> 6;

    ByteType     curr_byte    = allFrame[0];
    unsigned int size_of_link = ( ( curr_byte & 0xF0 ) >> 4 ) + 1;
    // first be sure the size_of_link is in the allowed range
    if ( size_of_link == 1 ) return {};
    if ( EDAC_info_stored > 0 ) {
      out_info.EDAC_stored  = true;
      out_info.single_error = ( curr_byte & 0x80 ) >> 7;
      out_info.single_error = ( curr_byte & 0x40 ) >> 6;
    }
    if ( size_of_link > 1 && size_of_link < 7 ) {
      out_info.wrong_size = true;
      return out_info;
    }
    // now checkif EDAC_info_stored is 0/1
    // maybe usefull unsigned int map_offset_in_bits = ( EDAC_info_stored > 0 ) ? 8 : 4;
    unsigned int map_offset_in_byte = ( EDAC_info_stored > 0 ) ? 1 : 0;

    // maybe usefull unsigned int TDC_offset_in_bits = map_offset_in_bits + 48;
    unsigned int HitsMap_length = ( EDAC_info_stored > 0 ) ? 6 : 7;

    unsigned int TDCMap_length = ( EDAC_info_stored > 0 ) ? size_of_link - 7 : size_of_link - 6;

    auto range_link_HitsMap = allFrame.subspan( map_offset_in_byte, HitsMap_length );
    auto range_link_TDC     = allFrame.subspan( map_offset_in_byte + 6, TDCMap_length );

    // now copy the in in output   --- byte aligned
    // TDCMap[6];
    if ( EDAC_info_stored == 0 ) {
      for ( unsigned int i = 0; i < MaxHitBytes; i++ ) {
        HitsMap[i] = ( ( range_link_HitsMap[i] << 4 ) & 0xF0 ) | ( ( range_link_HitsMap[i + 1] >> 4 ) & 0x0F );
      }
    } else {
      for ( unsigned int i = 0; i < MaxHitBytes; i++ ) { HitsMap[i] = range_link_HitsMap[i]; }
    }
    // now TDC part
    if ( EDAC_info_stored == 0 ) {
      for ( unsigned int i = 0; i < TDCMap_length - 1; i++ ) {
        TDCMap[i] = ( ( range_link_TDC[i] & 0x0F ) << 4 ) | ( ( range_link_TDC[i + 1] & 0xF0 ) >> 4 );
      }
      // in the [TDCMap_length==7 only first half of the last byte can contain a TDC so skip following line in that case
      if ( TDCMap_length <= MaxTDCBytes )
        TDCMap[TDCMap_length - 1] = ( ( range_link_TDC[TDCMap_length - 1] & 0x0F ) << 4 );
    } else {
      for ( unsigned int i = 0; i < TDCMap_length; i++ ) { TDCMap[i] = range_link_TDC[i]; }
    }
    TDCMap_ByteLength = TDCMap_length;
    if ( EDAC_info_stored > 0 ) return out_info;
    return {};
  }

} // namespace LHCb::MuonUpgrade::DAQ
