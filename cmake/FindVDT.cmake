###############################################################################
# (c) Copyright 2021-2023 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
# - Locate VDT math library
# Defines:
#
#  VDT_FOUND
#  VDT_INCLUDE_DIR
#  VDT_INCLUDE_DIRS (not cached)
#  VDT_LIBRARIES
#
# Imports:
#
#  VDT::vdt
#

# try ROOT FindVdt.cmake if possible
find_package(Vdt QUIET)
if(NOT VDT_FOUND)
    find_path(VDT_INCLUDE_DIR vdt/vdtcore_common.h
              HINTS $ENV{VDT_ROOT_DIR}/include ${VDT_ROOT_DIR}/include)
    find_library(VDT_LIBRARY NAMES vdt
              HINTS $ENV{VDT_ROOT_DIR}/lib ${VDT_ROOT_DIR}/lib)

    # handle the QUIETLY and REQUIRED arguments and set VDT_FOUND to TRUE if
    # all listed variables are TRUE
    INCLUDE(FindPackageHandleStandardArgs)
    FIND_PACKAGE_HANDLE_STANDARD_ARGS(VDT DEFAULT_MSG VDT_INCLUDE_DIR VDT_LIBRARY)

    mark_as_advanced(VDT_FOUND VDT_INCLUDE_DIR VDT_LIBRARY)

endif()
set(VDT_INCLUDE_DIRS ${VDT_INCLUDE_DIR})
set(VDT_LIBRARIES ${VDT_LIBRARY})

if(TARGET VDT::vdt)
    return()
endif()
if(VDT_FOUND)
    if(TARGET VDT::VDT)
        # we found VDT via ROOT FindVdt.cmake
        add_library(VDT::vdt ALIAS VDT::VDT)
    else()
        add_library(VDT::vdt INTERFACE IMPORTED)
        target_include_directories(VDT::vdt SYSTEM INTERFACE "${VDT_INCLUDE_DIR}")
        target_link_libraries(VDT::vdt INTERFACE "${VDT_LIBRARIES}")
        # Display the imported target for the user to know
        if(NOT ${CMAKE_FIND_PACKAGE_NAME}_FIND_QUIETLY)
            message(STATUS "  Import target: VDT::vdt")
        endif()
    endif()
endif()
