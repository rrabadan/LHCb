###############################################################################
# (c) Copyright 2021-2022 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
#[========================================================================[.rst:
LHCbConfigUtils
===============

Utilities for the configuration of projects based on LHCb.

#]========================================================================]

include_guard(GLOBAL) # Protect from multiple include (global scope, because
                      # everything defined in this file is globally visible)

set(LHCbConfigUtils_DIR ${CMAKE_CURRENT_LIST_DIR} CACHE INTERNAL
    "location of LHCbConfigUtils.cmake, to find related files")
list(PREPEND CMAKE_MODULE_PATH ${LHCbConfigUtils_DIR})

# load common modules
include(FindDataPackage)

set(LHCB_UNUSED_SUBDIR_MESSAGE_TYPE "WARNING"
    CACHE STRING "Message type for detected unused subdirs")
set(LHCB_UNUSED_SOURCE_MESSAGE_TYPE "WARNING"
    CACHE STRING "Message type for detected unused source files")

#[========================================================================[.rst:
.. cmake:command:: lhcb_env

  .. code-block:: cmake

    lhcb_env([PRIVATE]
             [SET <variable> <value>
             |APPEND <variable> <value>
             |PREPEND <variable> <value>
             |DEFAULT <variable> <value>] ...)

  Modify the runtime environment.

  If the flag ``PRIVATE`` is specified, the changes to the environment are
  only valid for build and test (not propagated to downstream projects).
#]========================================================================]
function(lhcb_env)
    message(DEBUG "lhcb_env(${ARGV})")
    if(ARGC GREATER 0 AND ARGV0 STREQUAL "PRIVATE")
        # drop the flag and do not propagate the change downstream
        list(REMOVE_AT ARGV 0)
    else()
        # not PRIVATE, so propagate the change downstream
        set_property(GLOBAL APPEND PROPERTY ${PROJECT_NAME}_ENVIRONMENT ${ARGV})
    endif()
    if(NOT TARGET target_runtime_paths)
        # this usually means we are not using GaudiToolbox.cmake, like in old
        # style projects
        return()
    endif()
    while(ARGV)
        list(POP_FRONT ARGV action variable value)
        if(action STREQUAL "SET")
            set_property(TARGET target_runtime_paths APPEND_STRING
                PROPERTY extra_commands "export ${variable}=${value}\n")
        elseif(action STREQUAL "PREPEND")
            set_property(TARGET target_runtime_paths APPEND_STRING
                PROPERTY extra_commands "export ${variable}=${value}:\$${variable}\n")
        elseif(action STREQUAL "APPEND")
            set_property(TARGET target_runtime_paths APPEND_STRING
                PROPERTY extra_commands "export ${variable}=\$${variable}:${value}\n")
        elseif(action STREQUAL "DEFAULT")
            set_property(TARGET target_runtime_paths APPEND_STRING
                PROPERTY extra_commands "export ${variable}=\${${variable}:-${value}}\n")
        else()
            message(FATAL_ERROR "invalid environment action ${action}")
        endif()
    endwhile()
endfunction()

#[========================================================================[.rst:
.. cmake:command:: lhcb_initialize_configuration

  .. code-block:: cmake

    lhcb_initialize_configuration()

  Implement some standard configuration tasks that have to be run before
  adding the subdirectories.

  The invocation of :cmake:command:`lhcb_initialize_configuration` is
  optional.
#]========================================================================]
macro(lhcb_initialize_configuration)
    # Optionally enable compatibility with old-style CMake configurations, via helper module
    option(GAUDI_LEGACY_CMAKE_SUPPORT "Enable compatibility with old-style CMake builds" "$ENV{GAUDI_LEGACY_CMAKE_SUPPORT}")

    # default install prefix when building in legacy mode
    # note: this doplicates a bit of code in LegacyGaudiCMakeSupport.cmake, but we need to set CMAKE_INSTALL_PREFIX
    #       before we use it later in this macro (and before we can include LegacyGaudiCMakeSupport.cmake)
    if(GAUDI_LEGACY_CMAKE_SUPPORT)
        # make sure we have a BINARY_TAG CMake variable set
        if(NOT BINARY_TAG)
            if(NOT "$ENV{BINARY_TAG}" STREQUAL "")
                set(BINARY_TAG $ENV{BINARY_TAG})
            elseif(LHCB_PLATFORM)
                set(BINARY_TAG ${LHCB_PLATFORM})
            else()
                message(AUTHOR_WARNING "BINARY_TAG not set")
            endif()
        endif()

        if(CMAKE_INSTALL_PREFIX_INITIALIZED_TO_DEFAULT)
            set(CMAKE_INSTALL_PREFIX "${CMAKE_SOURCE_DIR}/InstallArea/${BINARY_TAG}"
                CACHE PATH "Install prefix" FORCE)
        endif()
    endif()

    # environment for the project
    lhcb_env(PREPEND PATH "${CMAKE_INSTALL_PREFIX}/${CMAKE_INSTALL_BINDIR}")
    lhcb_env(PREPEND LD_LIBRARY_PATH "${CMAKE_INSTALL_PREFIX}/${CMAKE_INSTALL_LIBDIR}")
    if(NOT CMAKE_INSTALL_LIBDIR STREQUAL GAUDI_INSTALL_PLUGINDIR)
        lhcb_env(PREPEND LD_LIBRARY_PATH "${CMAKE_INSTALL_PREFIX}/${GAUDI_INSTALL_PLUGINDIR}")
    endif()
    lhcb_env(PREPEND PYTHONPATH "${CMAKE_INSTALL_PREFIX}/${GAUDI_INSTALL_PYTHONDIR}")
    lhcb_env(PREPEND ROOT_INCLUDE_PATH "${CMAKE_INSTALL_PREFIX}/${CMAKE_INSTALL_INCLUDEDIR}")

    # override GaudiToolbox internal macro to use lhcb_env
    macro(_gaudi_runtime_prepend runtime value)
        string(TOUPPER "${runtime}" _runtime_var)
        lhcb_env(PRIVATE PREPEND "${_runtime_var}" "${value}")
    endmacro()

    set_property(DIRECTORY PROPERTY LHCB_CONFIGURATION_INITIALIZED TRUE)
endmacro()

#[========================================================================[.rst:
.. cmake:command:: lhcb_add_subdirectories

  .. code-block:: cmake

    lhcb_add_subdirectories(subdir1 [subdir2 ...])

  Simple helper to add multiple subdirectories in one call.
#]========================================================================]
macro(lhcb_add_subdirectories)
    get_property(_init_called DIRECTORY PROPERTY LHCB_CONFIGURATION_INITIALIZED)
    if(NOT _init_called)
        lhcb_initialize_configuration()
    endif()
    message(STATUS "Adding ${ARGC} subdirectories:")
    set(lhcb_add_subdirectories_index 0)
    foreach(subdir IN ITEMS ${ARGN})
        math(EXPR lhcb_add_subdirectories_index "${lhcb_add_subdirectories_index} + 1")
        math(EXPR lhcb_add_subdirectories_progress "${lhcb_add_subdirectories_index} * 100 / ${ARGC}")
        if(lhcb_add_subdirectories_progress LESS 10)
            set(lhcb_add_subdirectories_progress "  ${lhcb_add_subdirectories_progress}")
        elseif(lhcb_add_subdirectories_progress LESS 100)
            set(lhcb_add_subdirectories_progress " ${lhcb_add_subdirectories_progress}")
        endif()
        message(STATUS "    (${lhcb_add_subdirectories_progress}%) ${subdir}")
        add_subdirectory(${subdir})
        string(TOUPPER "${subdir}ROOT" root_var)
        string(REGEX REPLACE ".*/" "" root_var "${root_var}")
        lhcb_env(SET "${root_var}" "${CMAKE_CURRENT_SOURCE_DIR}/${subdir}")
    endforeach()
endmacro()


macro(_lhcb_finalize_extra_commands)
    # environment for data packages
    get_property(data_packages_found GLOBAL PROPERTY DATA_PACKAGES_FOUND)
    foreach(data_package IN LISTS data_packages_found)
        if(data_package MATCHES "^([^:]*):(.*)\$")
            set(data_package ${CMAKE_MATCH_1})
        endif()
        string(TOUPPER "${data_package}" DP_NAME)
        string(REGEX REPLACE ".*/" "" DP_NAME "${DP_NAME}")
        lhcb_env(PRIVATE DEFAULT "${DP_NAME}ROOT" "${${data_package}_ROOT_DIR}")
        if(IS_DIRECTORY "${${data_package}_ROOT_DIR}/options")
            lhcb_env(PRIVATE SET "${DP_NAME}OPTS" "\${${DP_NAME}ROOT}/options")
        endif()
        if(IS_DIRECTORY "${${data_package}_ROOT_DIR}/python")
            lhcb_env(PRIVATE PREPEND "PYTHONPATH" "\${${DP_NAME}ROOT}/python")
        endif()
    endforeach()

    set_property(TARGET target_runtime_paths APPEND_STRING
        PROPERTY extra_commands "# remove duplicates
remove_dups() {
    # modified from https://unix.stackexchange.com/a/124525/149705
    local var=\${1}
    export \${var}=\$(
        IFS=:; set -f; declare -A a; NR=0
        for i in \${!var}; do
            NR=\$((NR+1))
            if [ \! \${a[\$i]+_} ]; then
                if [ \$NR -gt 1 ]; then printf \":\$i\"; else printf \"\$i\"; fi
                a[\$i]=1
            fi
        done
        )
}
remove_dups PATH
remove_dups PYTHONPATH
remove_dups LD_LIBRARY_PATH
remove_dups ROOT_INCLUDE_PATH")
endmacro()


#[========================================================================[.rst:
.. cmake:command:: lhcb_finalize_configuration

  .. code-block:: cmake

    lhcb_finalize_configuration([NO_EXPORT])

  Implement some standard configuration tasks

    - generate config files (``<Project>Config.cmake`` and ``<Project>>ConfigVersion.cmake``)
    - install target export information
    - generate project version header (``<PROJECT>_VERSION.h``)
    - install standard CMake modules
    - add optional old-style CMake builds compatibility
    - check that all subdirectories with a ``CMakeLists.txt`` are actually used
      unless listed in ``LHCB_IGNORE_SUBDIRS``
      (by default print a warnings but the message can be tuned with the variable
      ``LHCB_UNUSED_SUBDIR_MESSAGE_TYPE``, e.g. setting it to ``FATAL_ERROR``)
    - check that all ``*.cpp`` and ``*.cxx`` files in source directories are
      effectively used (them message level can be tuned with the variable
      ``LHCB_UNUSED_SOURCE_MESSAGE_TYPE``, where ``IGNORE`` disables the check)
#]========================================================================]
macro(lhcb_finalize_configuration)
    get_property(_init_called DIRECTORY PROPERTY LHCB_CONFIGURATION_INITIALIZED)
    if(NOT _init_called)
        lhcb_initialize_configuration()
    endif()

    # environment used by lbexec
    lhcb_env(PRIVATE DEFAULT "GAUDIAPPNAME" "${CMAKE_PROJECT_NAME}")
    lhcb_env(PRIVATE DEFAULT "GAUDIAPPVERSION" "${CMAKE_PROJECT_VERSION}")

    get_property(_lhcb_finalize_scheduled GLOBAL PROPERTY LHCB_FINALIZE_SCHEDULED)
    if(NOT _lhcb_finalize_scheduled)
        # Only run _lhcb_finalize_extra_commands once at the end of the top CMakeLists.txt.
        # This is important when building multiple projects in a super-project build.
        if(CMAKE_VERSION VERSION_LESS 3.19)
            _lhcb_finalize_extra_commands()
        else()
            cmake_language(DEFER DIRECTORY ${CMAKE_SOURCE_DIR} CALL _lhcb_finalize_extra_commands)
        endif()
        set_property(GLOBAL PROPERTY LHCB_FINALIZE_SCHEDULED TRUE)
    endif()

    include(CMakePackageConfigHelpers)
    # special variables needed in the config file
    string(TOUPPER "${PROJECT_NAME}" PROJECT_NAME_UPCASE)
    get_property(packages_found GLOBAL PROPERTY PACKAGES_FOUND)
    get_property(ENVIRONMENT GLOBAL PROPERTY ${PROJECT_NAME}_ENVIRONMENT)
    string(REPLACE "${PROJECT_SOURCE_DIR}/" "\${${PROJECT_NAME_UPCASE}_PROJECT_ROOT}/"
        ENVIRONMENT "${ENVIRONMENT}")
    foreach(pack IN LISTS packages_found)
        string(TOUPPER "${pack}" PROJ)
        if(DEFINED ${PROJ}_PROJECT_ROOT)
            string(REPLACE "${${PROJ}_PROJECT_ROOT}/" "\${${PROJ}_PROJECT_ROOT}/"
                ENVIRONMENT "${ENVIRONMENT}")
        endif()
    endforeach()
    #  record "persistent options" for downstream projects
    set(CONFIG_OPTIONS "")
    foreach(option IN LISTS ${PROJECT_NAME}_PERSISTENT_OPTIONS)
        if(DEFINED ${option})
            string(APPEND CONFIG_OPTIONS "set(${option} ${${option}} CACHE BOOL \"value used at compile time for ${option}\" FORCE)\n")
        endif()
    endforeach()
    if(CONFIG_OPTIONS) # this is just to make the generated file nicer
        string(PREPEND CONFIG_OPTIONS "\n# Options used when compiling\n")
    endif()
    #  record versions of upstream LHCb projects
    set(DEPS_VERSIONS "")
    foreach(pack IN LISTS packages_found)
        # we want to record the versions of projects with /InstallArea/
        # as we usually have multiple versions available in the search path
        # and we want to pick up the same version that was used during the build
        if("${${pack}_DIR}${${pack}_ROOT_DIR}" MATCHES "/InstallArea/" AND "${${pack}_VERSION}" MATCHES "^[0-9.]+$")
            string(APPEND DEPS_VERSIONS
                "if(NOT DEFINED ${pack}_EXACT_VERSION)\n"
                "    set(${pack}_EXACT_VERSION ${${pack}_VERSION} CACHE STRING \"Version of ${pack} used in upstream builds\")\n"
                "    mark_as_advanced(${pack}_EXACT_VERSION)\n"
                "elseif(NOT ${pack}_EXACT_VERSION STREQUAL \"${${pack}_VERSION}\")\n"
                "    message(WARNING \"Requested version of ${pack} (\${${pack}_EXACT_VERSION}) differs from that used for build (${${pack}_VERSION})\")\n"
                "endif()\n")
        endif()
    endforeach()
    if(DEPS_VERSIONS)
        string(APPEND CONFIG_OPTIONS "\n# Versions of upstream projects used for the build\n${DEPS_VERSIONS}")
    endif()

    if(CMAKE_SOURCE_DIR STREQUAL CMAKE_CURRENT_SOURCE_DIR AND NOT "${ARGV0}" STREQUAL "NO_EXPORT")
        # install exports only if we are a master project and we were not given the "NO_EXPORT" option
        install(EXPORT ${PROJECT_NAME} NAMESPACE ${PROJECT_NAME}::
            FILE "${PROJECT_NAME}Targets.cmake"
            DESTINATION "${GAUDI_INSTALL_CONFIGDIR}")
        set(CONFIG_IMPORT_TARGETS "include(\${CMAKE_CURRENT_LIST_DIR}/${PROJECT_NAME}Targets.cmake)\n")
    endif()

    # generate config files
    configure_package_config_file(
        ${LHCbConfigUtils_DIR}/ProjectConfig.cmake.in ${PROJECT_NAME}Config.cmake
        INSTALL_DESTINATION "${GAUDI_INSTALL_CONFIGDIR}"
        PATH_VARS
            CMAKE_INSTALL_BINDIR
            CMAKE_INSTALL_LIBDIR
            CMAKE_INSTALL_INCLUDEDIR
            GAUDI_INSTALL_PLUGINDIR
            GAUDI_INSTALL_PYTHONDIR
        NO_CHECK_REQUIRED_COMPONENTS_MACRO
    )
    write_basic_package_version_file(${PROJECT_NAME}ConfigVersion.cmake
        COMPATIBILITY AnyNewerVersion)

    gaudi_generate_version_header_file()

    gaudi_install(CMAKE
        cmake/${PROJECT_NAME}Dependencies.cmake
        "${CMAKE_CURRENT_BINARY_DIR}/${PROJECT_NAME}Config.cmake"
        "${CMAKE_CURRENT_BINARY_DIR}/${PROJECT_NAME}ConfigVersion.cmake"
    )
    if(EXISTS lhcbproject.yml)
        gaudi_install(CMAKE lhcbproject.yml)
    endif()

    # check that we actually build everything in the project
    # - check all subdirs are included (except for those listed in LHCB_IGNORE_SUBDIRS)
    get_property(added_subdirs DIRECTORY PROPERTY SUBDIRECTORIES)
    file(GLOB_RECURSE detected_subdirs "*/CMakeLists.txt")
    if(detected_subdirs)
        set(missed_subdirs)
        list(TRANSFORM detected_subdirs REPLACE "/CMakeLists.txt" "")
        list(SORT detected_subdirs)
        foreach(subdir IN LISTS detected_subdirs)
            list(FIND added_subdirs ${subdir} idx)
            if(idx STREQUAL "-1" AND NOT subdir MATCHES "^${CMAKE_BINARY_DIR}/.*")
                file(GLOB subdir RELATIVE "${PROJECT_SOURCE_DIR}" ${subdir})
                list(APPEND missed_subdirs ${subdir})
            endif()
        endforeach()
        if(missed_subdirs AND LHCB_IGNORE_SUBDIRS)
            list(REMOVE_ITEM missed_subdirs ${LHCB_IGNORE_SUBDIRS})
        endif()
        if(missed_subdirs)
            list(JOIN missed_subdirs "\n - " missed_subdirs)
            message(${LHCB_UNUSED_SUBDIR_MESSAGE_TYPE}
                "Project ${PROJECT_NAME} contains subdirectories that are not used:\n - ${missed_subdirs}\n")
        endif()
    endif()
    # - check for all files (*.cpp and *.cxx in source directories)
    if(NOT LHCB_UNUSED_SOURCE_MESSAGE_TYPE STREQUAL "IGNORE")
        # for each directory (including the top one) we get the targets and for
        # each target we get the source files excluding absolute paths
        # (which means also those in the build directory), then we prefix the subdir
        # path
        set(used_sources)
        list(PREPEND added_subdirs ${PROJECT_SOURCE_DIR})
        foreach(subdir IN LISTS added_subdirs)
            get_property(targets DIRECTORY ${subdir} PROPERTY BUILDSYSTEM_TARGETS)
            foreach(target IN LISTS targets)
                get_target_property(target_type ${target} TYPE)
                if(target_type STREQUAL "INTERFACE_LIBRARY" AND CMAKE_VERSION VERSION_LESS "3.19")
                    continue()  # sources for interface libraries were introduced only in cmake 3.19
                endif()
                get_target_property(sources ${target} SOURCES)
                list(FILTER sources EXCLUDE REGEX "^/")
                list(TRANSFORM sources PREPEND "${subdir}/")
                list(APPEND used_sources ${sources})
            endforeach()
        endforeach()
        list(REMOVE_DUPLICATES used_sources)
        # from the list of of source files we guess the directories meant to contain
        # source files
        list(TRANSFORM used_sources REPLACE "/[^/]*$" ""
            OUTPUT_VARIABLE source_dirs)
        list(REMOVE_DUPLICATES source_dirs)
        # and we look for all *.cpp and *.cxx files in these directories
        list(TRANSFORM source_dirs APPEND "/*.cpp" OUTPUT_VARIABLE source_cpp_globs)
        list(TRANSFORM source_dirs APPEND "/*.cxx" OUTPUT_VARIABLE source_cxx_globs)
        file(GLOB all_sources ${source_cpp_globs} ${source_cxx_globs})
        # check if they are all used
        set(missed_sources)
        foreach(src IN LISTS all_sources)
            list(FIND used_sources ${src} idx)
            if(idx STREQUAL "-1")
                file(GLOB src RELATIVE "${PROJECT_SOURCE_DIR}" ${src})
                list(APPEND missed_sources ${src})
            endif()
        endforeach()
        # and report any missing file
        if(missed_sources)
            list(JOIN missed_sources "\n - " missed_sources)
            message(${LHCB_UNUSED_SOURCE_MESSAGE_TYPE}
                "Project ${PROJECT_NAME} contains source files which are not used in any target:\n - ${missed_sources}\n")
        endif()
    endif()


    # Set the version of the project as a cache variable to be seen by other
    # projects in the same super-project.
    set(${PROJECT_NAME}_VERSION "${PROJECT_VERSION}" CACHE STRING "Version of ${PROJECT_NAME}" FORCE)

    if(CMAKE_SOURCE_DIR STREQUAL CMAKE_CURRENT_SOURCE_DIR AND GAUDI_LEGACY_CMAKE_SUPPORT)
        find_file(legacy_cmake_config_support NAMES LegacyGaudiCMakeSupport.cmake)
        if(legacy_cmake_config_support)
            include(${legacy_cmake_config_support})
        else()
            message(FATAL_ERROR "GAUDI_LEGACY_CMAKE_SUPPORT set to TRUE, but cannot find LegacyGaudiCMakeSupport.cmake")
        endif()
    endif()
endmacro()

#[========================================================================[.rst:
.. cmake:command:: lhcb_add_confuser_dependencies

  .. code-block:: cmake

    lhcb_add_confuser_dependencies(<directory>:<target> [...])

  Declare dependency of ConfigurableUser modules on targets from other
  subdirectories.

  The dependencies have to be declared as ``<directory>[:<target>]`` where

    - ``<directory>`` is the directory defining the target we need (if empty
      it means the target is define in the current directory)
    - ``<target>`` is the target we need, if not specified it means we need
      the ConfigurableUser from the directory
#]========================================================================]
function(lhcb_add_confuser_dependencies)
    get_filename_component(package_name ${CMAKE_CURRENT_SOURCE_DIR} NAME)
    foreach(dep IN LISTS ARGV)
        if(dep MATCHES "^([^:]*)(:(.+))?\$")
            if(CMAKE_MATCH_1)
                # we are asked for an explicit directory, so we check for the requested targets or
                # for the directory itself (to support out-of-order add_subdirectory calls in satellite
                # projects)
                get_filename_component(other_package_name "${CMAKE_MATCH_1}" NAME)
                if(CMAKE_MATCH_3 AND (TARGET "${CMAKE_MATCH_3}" OR EXISTS "${PROJECT_SOURCE_DIR}/${CMAKE_MATCH_1}/CMakeLists.txt"))
                    message(DEBUG "add_dependencies(${package_name}_confuserdb ${CMAKE_MATCH_3})")
                    add_dependencies(${package_name}_confuserdb ${CMAKE_MATCH_3})
                elseif(TARGET "${other_package_name}_confuserdb" OR EXISTS "${PROJECT_SOURCE_DIR}/${CMAKE_MATCH_1}/CMakeLists.txt")
                    message(DEBUG "add_dependencies(${package_name}_confuserdb ${other_package_name}_confuserdb)")
                    add_dependencies(${package_name}_confuserdb ${other_package_name}_confuserdb)
                endif()
            elseif(CMAKE_MATCH_3)
                # local target
                message(DEBUG "add_dependencies(${package_name}_confuserdb ${CMAKE_MATCH_3})")
                add_dependencies(${package_name}_confuserdb ${CMAKE_MATCH_3})
            endif()
        else()
            message(FATAL_ERROR "invalid confuser dependency '${dep}', it must be '<directory>:<target>'")
        endif()
    endforeach()
endfunction()
