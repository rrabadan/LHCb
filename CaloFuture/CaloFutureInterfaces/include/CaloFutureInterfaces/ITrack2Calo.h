/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once

#include "CaloDet/DeCalorimeter.h"
#include "Detector/Calo/CaloCellID.h"
#include "Event/CaloPosition.h"
#include "Event/State.h"
#include "Event/Track.h"
#include "Kernel/TrackDefaultParticles.h"

#include "GaudiKernel/IAlgTool.h"

#include <string>

#include "DetDesc/IGeometryInfo.h"
namespace LHCb {
  class CaloHypo;
  class CaloCluster;
} // namespace LHCb

/**
 *  @author Olivier Deschamps
 *  @date   2007-06-25
 */
struct ITrack2Calo : extend_interfaces<IAlgTool> {
  // Return the interface ID
  DeclareInterfaceID( ITrack2Calo, 4, 0 );

  virtual bool match( const LHCb::Track* track, IGeometryInfo const& geometry,
                      std::string det = DeCalorimeterLocation::Ecal, CaloPlane::Plane plane = CaloPlane::ShowerMax,
                      double delta = 0., LHCb::Tr::PID pid = LHCb::Tr::PID::Pion() ) = 0;

  virtual LHCb::State                  caloState()  = 0;
  virtual LHCb::Detector::Calo::CellID caloCellID() = 0;
  virtual bool                         isValid()    = 0;
  //
  virtual LHCb::State        closestState( LHCb::CaloHypo* hypo, IGeometryInfo const& geometry,
                                           LHCb::Tr::PID pid = LHCb::Tr::PID::Pion() ) = 0;
  virtual LHCb::State        closestState( LHCb::CaloCluster* cluster, IGeometryInfo const& geometry,
                                           LHCb::Tr::PID pid = LHCb::Tr::PID::Pion() ) = 0;
  virtual LHCb::State        closestState( LHCb::CaloPosition calopos, IGeometryInfo const& geometry,
                                           LHCb::Tr::PID pid = LHCb::Tr::PID::Pion() ) = 0;
  virtual LHCb::State        closestState( LHCb::Detector::Calo::CellID cellID, IGeometryInfo const& geometry,
                                           LHCb::Tr::PID pid = LHCb::Tr::PID::Pion() ) = 0;
  virtual const LHCb::Track* track()                                                   = 0;
};
