/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "CaloDet/DeCalorimeter.h"
#include "CaloFutureUtils/CaloFutureAlgUtils.h"
#include "Event/CaloAdc.h"
#include "Event/RawEvent.h"
#include "Kernel/STLExtensions.h"
#include "LHCbAlgs/Consumer.h"

//-----------------------------------------------------------------------------
// Implementation file for class : FillRawBuffer
//
// 2004-12-17 : Olivier Callot
//-----------------------------------------------------------------------------

/** @class FillRawBuffer
 *  Fills the Raw Buffer banks for the calorimeter
 *
 *  @author Olivier Callot
 *  @date   2004-12-17
 */

namespace LHCb::Calo {

  class FillRawBuffer : public LHCb::Algorithm::Consumer<void( const LHCb::CaloAdcs&, const DeCalorimeter& ),
                                                         LHCb::DetDesc::usesConditions<DeCalorimeter>> {

  public:
    // Standard constructor
    FillRawBuffer( const std::string& name, ISvcLocator* pSvcLocator );

    // Algorithm execution
    void operator()( const LHCb::CaloAdcs&, const DeCalorimeter& ) const override;

  private:
    void fillPackedBank( const LHCb::CaloAdcs& digs, const DeCalorimeter& calo, LHCb::RawEvent& rawEvent ) const;
    void fillBank( const LHCb::CaloAdcs& digs, const DeCalorimeter& calo, LHCb::RawEvent& rawEvent ) const;

    std::string          m_triggerBank;
    Gaudi::Property<int> m_dataCodingType{this, "DataCodingType", 1, [this]( auto& ) {
                                            if ( 0 >= m_dataCodingType )
                                              throw std::invalid_argument( "Invalid Data Coding Type" );
                                          }};

    // This is actually an update handle, in the sense that the RawEvent will be modified
    DataObjectReadHandle<RawEvent> m_raw{this, "RawEvent", LHCb::RawEventLocation::Default};

    Gaudi::Property<bool> m_pin{this, "FillWithPin", false};

    // Statistics
    mutable Gaudi::Accumulators::StatCounter<> m_totDataSize{this, "# totDataSize"};
    mutable Gaudi::Accumulators::StatCounter<> m_totTrigSize{this, "# totTrigSize"};
  };

  DECLARE_COMPONENT_WITH_ID( FillRawBuffer, "CaloFutureFillRawBuffer" )

  // Standard constructor, initializes variables
  FillRawBuffer::FillRawBuffer( const std::string& name, ISvcLocator* pSvcLocator )
      : Consumer( name, pSvcLocator,
                  {KeyValue{"InputBank", LHCb::CaloAdcLocation::FullEcal},
                   KeyValue{"DetectorLocation", DeCalorimeterLocation::Ecal}} ) {}

  // Main execution
  void FillRawBuffer::operator()( const LHCb::CaloAdcs& digs, const DeCalorimeter& calo ) const {

    if ( !m_raw.exist() ) m_raw.put( std::make_unique<RawEvent>() );

    if ( m_dataCodingType < 3 ) // Old run 2 encoding
      fillPackedBank( digs, calo, *m_raw.get() );
    else if ( m_dataCodingType == 4 || m_dataCodingType == 5 )
      fillBank( digs, calo, *m_raw.get() );
    else
      error() << "CodingType " << m_dataCodingType << " not supported" << endmsg;
  }

  //  Packed data format, trigger and data in the same bank. Process ALL digits
  void FillRawBuffer::fillPackedBank( const LHCb::CaloAdcs& digs, const DeCalorimeter& calo,
                                      LHCb::RawEvent& rawEvent ) const {

    LHCb::RawBank::BankType bankType        = LHCb::RawBank::LastType;
    LHCb::RawBank::BankType triggerBankType = LHCb::RawBank::LastType;
    int                     numberOfBanks   = 1;
    if ( m_dataCodingType == 2 ) {
      //== TELL1 coding format: packed data, starting from Full banks
      numberOfBanks = calo.nTell1s();
      bankType      = ( calo.index() == Detector::Calo::CellCode::Index::EcalCalo ) ? LHCb::RawBank::EcalPacked
                                                                               : LHCb::RawBank::HcalPacked;
    } else {
      bankType =
          ( calo.index() == Detector::Calo::CellCode::Index::EcalCalo ) ? LHCb::RawBank::EcalE : LHCb::RawBank::HcalE;
    }
    triggerBankType = ( calo.index() == Detector::Calo::CellCode::Index::EcalCalo ) ? LHCb::RawBank::EcalTrig
                                                                                    : LHCb::RawBank::HcalTrig;

    if ( m_dataCodingType == 1 ) {

      std::vector<unsigned int> trigBank;
      std::vector<unsigned int> bank;
      bank.reserve( 500 );
      trigBank.reserve( 500 );

      //  Fill the calorimeter data bank, simple structure: ID (upper 16 bits) + ADC
      std::transform( digs.begin(), digs.end(), std::back_inserter( bank ),
                      []( const auto& dig ) { return ( dig->cellID().all() << 16 ) | ( dig->adc() & 0xFFFF ); } );
      rawEvent.addBank( 0, bankType, m_dataCodingType, bank );
      m_totDataSize += bank.size();
      rawEvent.addBank( 0, triggerBankType, 0, trigBank );
      m_totTrigSize += trigBank.size();

    } else {

      int m_numberOfBanks = calo.nTell1s();

      std::vector<std::vector<unsigned int>> banks;
      for ( int kk = 0; numberOfBanks > kk; kk++ ) { banks.emplace_back().reserve( 500 ); }

      for ( int kTell1 = 0; m_numberOfBanks > kTell1; kTell1++ ) {
        for ( auto const cardNum : calo.tell1ToCards( kTell1 ) ) {
          if ( calo.isPinCard( cardNum ) && !m_pin ) continue; // No sub-bank for PIN-FEB if not explicitely requested
          int sizeIndex = banks[kTell1].size();
          banks[kTell1].push_back( calo.cardCode( cardNum ) << 14 );

          //=== The trigger part is first
          int patternIndex = banks[kTell1].size();
          int patTrig      = 0;
          banks[kTell1].push_back( patTrig );
          int word   = 0;
          int offset = 0;
          int bNum   = 0;

          int sizeTrig = 4 * ( banks[kTell1].size() - patternIndex ); // in byte
          //== If no trigger at all, remove even the pattern...
          if ( 4 == sizeTrig ) {
            banks[kTell1].pop_back();
            sizeTrig = 0;
          } else {
            banks[kTell1][patternIndex] = patTrig;
          }

          //=== Now the ADCs
          patternIndex = banks[kTell1].size();
          int pattern  = 0;
          banks[kTell1].push_back( pattern );
          word   = 0;
          offset = 0;
          bNum   = 0;

          for ( LHCb::Detector::Calo::CellID id : calo.cardChannels( cardNum ) ) {
            LHCb::CaloAdc* dig = digs.object( id );
            int adc = ( dig ? std::clamp( dig->adc() + 256, 0, 4095 ) : 256 ); //== Default if non existing cell.
            if ( 248 <= adc && adc < 264 ) {                                   //... store short
              adc -= 248;
              word |= adc << offset;
              offset += 4;
            } else { //... store long
              word |= adc << offset;
              pattern += ( 1 << bNum );
              offset += 12;
            }
            if ( 32 <= offset ) { //== Have we a full word or more ? Store it
              banks[kTell1].push_back( word );
              offset -= 32;
              word = adc >> ( 12 - offset ); //== upper bits if needed
            }
            bNum++;
          }
          if ( 0 != offset ) banks[kTell1].push_back( word );

          int sizeAdc                 = 4 * ( banks[kTell1].size() - patternIndex );
          banks[kTell1][patternIndex] = pattern;

          banks[kTell1][sizeIndex] |= ( sizeAdc << 7 ) + sizeTrig;
          m_totTrigSize += sizeTrig;

          if ( msgLevel( MSG::DEBUG ) )
            debug() << format( "Tell1 %2d card %3d pattern %8x patTrig %8x size Adc %2d Trig %2d", kTell1, cardNum,
                               pattern, patTrig, sizeAdc, sizeTrig )
                    << endmsg;
        }
      }

      for ( const auto& [kk, bank] : range::enumerate( banks ) ) {
        rawEvent.addBank( kk, bankType, m_dataCodingType, bank );
        m_totDataSize += bank.size();
      }
    }
  }

  //  New (run 3)  data format. Process ALL digits
  void FillRawBuffer::fillBank( const LHCb::CaloAdcs& digs, const DeCalorimeter& calo,
                                LHCb::RawEvent& rawEvent ) const {

    //== TELL40 coding format: packed data, starting from Full banks
    if ( msgLevel( MSG::DEBUG ) )
      debug() << "DataCodingType==" << m_dataCodingType << "  -  Processing " << calo.nCards() << " FE-Cards and "
              << calo.nTell1s() << " TELL40  -  BankType: LHCb::RawBank::Calo" << endmsg;

    std::vector<unsigned int> bank;
    bank.reserve( 500 );
    auto append = [isLittleEndian = ( m_dataCodingType == 5 )]( std::vector<unsigned int>& b, unsigned int w ) {
      if ( !isLittleEndian )
        w = ( ( w >> 24 ) & 0x000000FF ) | ( ( w >> 8 ) & 0x0000FF00 ) | ( ( w << 8 ) & 0x00FF0000 ) |
            ( ( w << 24 ) & 0xFF000000 );
      b.push_back( w );
    };

    for ( auto const& [sourceID, cardNums] : calo.getSourceIDsMap() ) {

      // clear for current iteration by initializing the LLT part first
      unsigned int LLT1 = 0, LLT2 = 0, LLT3 = 0;
      bank.assign( {LLT1, LLT2, LLT3} );

      for ( auto const& cardNum : cardNums ) {
        if ( cardNum == 0 ) {
          if ( msgLevel( MSG::DEBUG ) )
            debug() << "cardnum == 0 fill 12 empty words of 32 bits (32 cells  of 12 bits)" << endmsg;
          bank.insert( bank.end(), 12, 0 );
          continue;
        }
        if ( calo.isPinCard( cardNum ) && !m_pin ) {
          bank.insert( bank.end(), 12, 0 );
          continue; // No sub-bank for PIN-FEB if not explicitely requested
        }

        unsigned int word   = 0;
        int          offset = 0;

        //=== Now the ADCs
        for ( auto cell_id : calo.cardChannels( cardNum ) ) {
          LHCb::CaloAdc* dig = digs.object( cell_id );
          unsigned int adc = ( dig ? std::clamp( dig->adc() + 256, 0, 4095 ) : 256 ); //== Default if non existing cell.

          offset += 12;
          if ( offset < 32 ) {
            word |= adc << ( 32 - offset );
          } else {
            offset -= 32;
            word |= adc >> offset;
            append( bank, word );
            word = ( offset == 0 ? 0 : ( adc << ( 32 - offset ) ) ); // pick up the overflow upper bits
          }
        }
        if ( offset != 0 ) append( bank, word );

        if ( msgLevel( MSG::DEBUG ) ) debug() << format( "source_id %2d card %3d", sourceID, cardNum ) << endmsg;
      }

      rawEvent.addBank( sourceID, LHCb::RawBank::Calo, m_dataCodingType, bank );
      m_totDataSize += bank.size();
    }
  }

} // namespace LHCb::Calo
