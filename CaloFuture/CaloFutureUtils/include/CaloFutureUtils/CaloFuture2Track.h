/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef CALOFUTURE2TRACK_H
#define CALOFUTURE2TRACK_H 1

#include <string>

#include "Event/CaloCluster.h"
#include "Event/CaloHypo.h"
#include "Event/Track.h"

#include "Relations/IRelation.h"
#include "Relations/IRelationWeighted.h"
#include "Relations/IRelationWeighted2D.h"

/** @class CaloFuture2Track CaloFuture2Track.h Event/CaloFuture2Track.h
 *
 *
 *  @author Olivier Deschamps
 *  @date   2006-02-21
 */
namespace LHCb {
  namespace CaloFutureIdLocation {
    // CaloFuture-Track matching tables   : IRelationWeighted< CaloFutureCluster/Hypo , Track , float >
    inline const std::string ClusterMatch  = "Rec/Calo/ClusterMatch";
    inline const std::string PhotonMatch   = "Rec/Calo/PhotonMatch";
    inline const std::string BremMatch     = "Rec/Calo/BremMatch";
    inline const std::string ElectronMatch = "Rec/Calo/ElectronMatch";
    // Acceptance tables       : IRelation< Track , bool  >
    inline const std::string InSpd  = "Rec/Calo/InAccSpd";
    inline const std::string InPrs  = "Rec/Calo/InAccPrs";
    inline const std::string InBrem = "Rec/Calo/InAccBrem";
    inline const std::string InEcal = "Rec/Calo/InAccEcal";
    inline const std::string InHcal = "Rec/Calo/InAccHcal";
    // Intermediate estimators : IRelation< Track , float >
    inline const std::string SpdE     = "Rec/Calo/SpdE";
    inline const std::string PrsE     = "Rec/Calo/PrsE";
    inline const std::string EcalE    = "Rec/Calo/EcalE";
    inline const std::string HcalE    = "Rec/Calo/HcalE";
    inline const std::string EcalChi2 = "Rec/Calo/EcalChi2";
    inline const std::string BremChi2 = "Rec/Calo/BremChi2";
    inline const std::string ClusChi2 = "Rec/Calo/ClusChi2";
    // CaloFuturePID DLLs            : IRelation< Track , float >
    inline const std::string PrsPIDe   = "Rec/Calo/PrsPIDe";
    inline const std::string EcalPIDe  = "Rec/Calo/EcalPIDe";
    inline const std::string BremPIDe  = "Rec/Calo/BremPIDe";
    inline const std::string HcalPIDe  = "Rec/Calo/HcalPIDe";
    inline const std::string EcalPIDmu = "Rec/Calo/EcalPIDmu";
    inline const std::string HcalPIDmu = "Rec/Calo/HcalPIDmu";
    // Neutral ID DLLs
    inline const std::string PhotonID           = "Rec/Calo/PhotonID";
    inline const std::string MergedID           = "Rec/Calo/MergedID";
    inline const std::string PhotonFromMergedID = "Rec/Calo/PhotonFromMergedID";

  } // namespace CaloFutureIdLocation
  namespace CaloFuture2Track {
    typedef IRelationWeighted<LHCb::CaloCluster, LHCb::Track, float>            IClusTrTable;
    typedef IRelationWeighted<LHCb::Detector::Calo::CellID, LHCb::Track, float> ICluster2TrackTable;
    typedef IRelationWeighted<LHCb::Track, LHCb::CaloCluster, float>            ITrClusTable;
    typedef IRelationWeighted<LHCb::CaloHypo, LHCb::Track, float>               IHypoTrTable;
    typedef IRelationWeighted<LHCb::Track, LHCb::CaloHypo, float>               ITrHypoTable;

    typedef IRelationWeighted2D<LHCb::CaloCluster, LHCb::Track, float>            IClusTrTable2D;
    typedef IRelationWeighted2D<LHCb::Detector::Calo::CellID, LHCb::Track, float> ICluster2TrackTable2D;
    typedef IRelationWeighted2D<LHCb::Track, LHCb::CaloCluster, float>            ITrClusTable2D;
    typedef IRelationWeighted2D<LHCb::CaloHypo, LHCb::Track, float>               IHypoTrTable2D;
    typedef IRelationWeighted2D<LHCb::Detector::Calo::CellID, LHCb::Track, float> IHypothesis2TrackTable2D;
    typedef IRelationWeighted2D<LHCb::Track, LHCb::CaloHypo, float>               ITrHypoTable2D;
    typedef IRelation<LHCb::CaloHypo, float>                                      IHypoEvalTable;

    typedef IRelation<LHCb::Track, float> ITrEvalTable;
    typedef IRelation<LHCb::Track, bool>  ITrAccTable;
  } // namespace CaloFuture2Track
} // namespace LHCb

#endif // CALOFUTURE2TRACK_H
