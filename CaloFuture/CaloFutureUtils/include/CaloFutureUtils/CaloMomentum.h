/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once
#include "Event/CaloClusters_v2.h"
#include "Event/CaloPosition.h"
#include "GaudiKernel/GenericMatrixTypes.h"
#include "GaudiKernel/Point3DTypes.h"
#include "GaudiKernel/SymmetricMatrixTypes.h"
#include "GaudiKernel/Vector4DTypes.h"
#include "LHCbMath/Kinematics.h"
#include "boost/container/small_vector.hpp"
#include <vector>
// ============================================================================
// Forward declarations
// ============================================================================
namespace LHCb {
  class CaloCluster;
  class CaloHypo;
  class ProtoParticle;
  class VertexBase;
} // namespace LHCb
// ============================================================================
namespace LHCb::Calo {
  // ==========================================================================
  /** @class CaloMomentum CaloFutureUtils/CaloMomentum.h
   *  Helper class to evaluate the parameters of "Calo"-particles
   *  @author Olivier Deschamps
   *  @date 2006-08-08
   */
  class Momentum {
    // ========================================================================
  public:
    // ========================================================================
    /// 4-momentum (px,py,pz,e)
    using Vector = Gaudi::LorentzVector;
    ///  3D point (x0,y0,z0)
    using Point = Gaudi::XYZPoint;
    /// 4x4 Covariance matrix (px,py,pz,e)
    using MomCovariance = Gaudi::SymMatrix4x4;
    /// 3x3 Covariance matrix (x0,y0,z0)
    using PointCovariance = Gaudi::SymMatrix3x3;
    /// 4x3 pos-mom  covariance matrix off-diagonal block
    using MomPointCovariance = Gaudi::Matrix4x3;
    /// vector of CaloPositions
    using CaloPositions = boost::container::small_vector<LHCb::CaloPosition, 4>;
    /// vector of CaloHypos
    using CaloHypos = boost::container::small_vector<const LHCb::CaloHypo*, 8>;
    // ========================================================================
  public:
    // ========================================================================
    /// indices to access Px,Py,Pz,E
    enum Index { Px = 0, Py = 1, Pz = 2, E = 3 };
    // ========================================================================
    enum class Parameter { Momentum = 1, Covariance = 2 };
    // ========================================================================
    enum Status {
      OK                  = 0,
      NullCaloCluster     = 1,
      NullCaloPos         = 2,
      NullCaloHypo        = 4,
      NullProtoPart       = 8,
      ChargedProtoPart    = 16,
      EmptyHypoRef        = 32,
      MultipleHypoRef     = 64,
      NullPart            = 128,
      ChargedParticle     = 256,
      PartIsNotCaloObject = 512
    };
    // ========================================================================
    enum Flag {
      Empty               = 0,
      MomentumEvaluated   = 1,
      CovarianceEvaluated = 2,
      FromCaloCluster     = 4,
      FromCaloPos         = 8,
      FromCaloHypoRef     = 16,
      FromCaloHypo        = 32,
      FromProtoPart       = 64,
      FromPartDaughters   = 128,
      FromPart            = 256,
      ParticleUpdated     = 512,
      ParticleTreeUpdated = 1024,
      NewPointCovariance  = 2048,
      NewReferencePoint   = 4096
    };
    // ========================================================================
  public: // constructors
    // ========================================================================
    /// Default Constructor
    Momentum() = default;

    // T can be:
    // LHCb::CaloPosition*
    // LHCb::ProtoParticle*
    // LHCb::CaloHypo*
    // LHCb::CaloCluster*
    // LHCb::Event::Calo::Clusters::reference<simd, behaviour>
    // LHCb::Event::Calo::Clusters::const_reference<simd, behaviour>
    template <typename T>
    Momentum( const T calo ) {
      addCaloPosition( calo );
    }
    // ========================================================================
    /// From CaloPosition + reference point (null covariance)
    Momentum( const LHCb::CaloPosition* calo, Point point );
    /// From CaloPosition + reference point + covariance
    Momentum( const LHCb::CaloPosition* calo, Point point, const PointCovariance& cov );
    // ========================================================================
    /// Special constructors from ProtoParticle
    Momentum( const LHCb::ProtoParticle* proto, Point point );
    Momentum( const LHCb::ProtoParticle* proto, Point point, const PointCovariance& cov );
    // ========================================================================
    /// Special constructors from CaloHypo
    Momentum( const std::vector<const LHCb::CaloHypo*> hypos );
    Momentum( const LHCb::CaloHypo* hypo, Point point );
    Momentum( const LHCb::CaloHypo* hypo, Point point, const PointCovariance& cov );
    // ========================================================================
    /// Special constrctor from the CaloCluster
    template <SIMDWrapper::InstructionSet simd, LHCb::Pr::ProxyBehaviour behaviour, typename container, typename path>
    Momentum( LHCb::Event::vector_field<LHCb::Event::index_field<LHCb::Event::Calo::v2::Clusters>>::VectorProxy<
              simd, behaviour, container, path>
                  clusters ) {
      for ( auto c : clusters ) addCaloPosition( c );
    }
    // ========================================================================
    /// Destructor
    virtual ~Momentum() = default;
    // ========================================================================
  public:
    // ========================================================================
    void addCaloPosition( const LHCb::CaloPosition* pos );
    void addCaloPosition( const LHCb::CaloCluster* clus );
    template <SIMDWrapper::InstructionSet simd, LHCb::Pr::ProxyBehaviour behaviour>
    void addCaloPosition(
        LHCb::Event::Calo::v2::Clusters::ClusterProxy<simd, behaviour, LHCb::Event::Calo::v2::Clusters> cluster ) {
      m_flag |= LHCb::Calo::Momentum::FromCaloCluster;
      auto& pos = m_caloPositions.emplace_back();
      pos.setZ( cluster.position().z() );
      pos.setParameters( {cluster.position().x(), cluster.position().y(), cluster.e()} );
      pos.setCovariance( cluster.covariance() );
      pos.setSpread( cluster.spread() );
    }

    template <SIMDWrapper::InstructionSet simd, LHCb::Pr::ProxyBehaviour behaviour>
    void addCaloPosition( LHCb::Event::Calo::Clusters::reference<simd, behaviour> cluster ) {
      m_flag |= LHCb::Calo::Momentum::FromCaloCluster;
      auto& pos = m_caloPositions.emplace_back();
      pos.setZ( cluster.position().z() );
      pos.setParameters( {cluster.position().x(), cluster.position().y(), cluster.e()} );
      pos.setCovariance( cluster.covariance() );
      pos.setSpread( cluster.spread() );
    }

    template <SIMDWrapper::InstructionSet simd, LHCb::Pr::ProxyBehaviour behaviour>
    void addCaloPosition( LHCb::Event::Calo::Clusters::const_reference<simd, behaviour> cluster ) {
      m_flag |= LHCb::Calo::Momentum::FromCaloCluster;
      auto& pos = m_caloPositions.emplace_back();
      pos.setZ( cluster.position().z() );
      pos.setParameters( {cluster.position().x(), cluster.position().y(), cluster.e()} );
      pos.setCovariance( cluster.covariance() );
      pos.setSpread( cluster.spread() );
    }
    void addCaloPosition( const LHCb::CaloHypo* hypo );
    void addCaloPosition( const LHCb::ProtoParticle* proto );
    // ========================================================================
  public:
    // ========================================================================
    // Getters (const)
    // ========================================================================
    [[nodiscard]] const CaloPositions&   caloPositions() const { return m_caloPositions; }
    [[nodiscard]] const Point&           referencePoint() const { return m_point; }
    [[nodiscard]] const PointCovariance& pointCovMatrix() const { return m_pointCovMatrix; }
    [[nodiscard]] unsigned int           multiplicity() const { return m_caloPositions.size(); }
    [[nodiscard]] int                    status() const { return m_status; }
    [[nodiscard]] int                    flag() const { return m_flag; }
    // ========================================================================
  public:
    // ========================================================================
    // Getters (non-const)
    // ========================================================================
    const Vector&             momentum();
    const MomCovariance&      momCovMatrix();
    const MomPointCovariance& momPointCovMatrix();
    // ========================================================================
  public:
    // ========================================================================
    // Setters
    // ========================================================================
    void setCaloPositions( const CaloPositions& value ) { m_caloPositions = value; }
    void resetCaloPositions() { m_caloPositions.clear(); }
    //

    CaloHypos caloHypos();
    void      setReferencePoint( const Point& point );
    void      setPosCovMatrix( const PointCovariance& cov );
    void      setReferencePoint( const Point& point, const PointCovariance& cov );
    void      setReferencePoint( const LHCb::VertexBase* vertex );
    //
    void addToStatus( Status status ) { m_status |= status; }
    void addToFlag( Flag flag ) { m_flag |= flag; }
    // ========================================================================
  public:
    // ========================================================================
    // Shortcuts
    // ========================================================================
    auto e() { return momentum().E(); }
    auto px() { return momentum().Px(); }
    auto py() { return momentum().Py(); }
    auto pz() { return momentum().Pz(); }
    auto pt() { return momentum().Pt(); }
    auto mass() { return momentum().M(); }
    auto emass() {
      evaluate();
      return Gaudi::Math::sigmamass( m_momentum, m_momCovMatrix );
    }
    // ========================================================================
  public:
    // ========================================================================
    /// Evaluators
    bool evaluate( int param );
    bool evaluate() {
      return evaluate( static_cast<unsigned>( Parameter::Momentum ) & static_cast<unsigned>( Parameter::Covariance ) );
    }
    // ========================================================================
  private:
    // ========================================================================
    /// vector of Calo-positions
    CaloPositions m_caloPositions; //     vector of Calo-positions
    /// vector of CaloHypos
    CaloHypos m_caloHypos; //     vector of CaloHypos
    /// origin point
    Point m_point; //                 origin point
    /// origin point covariance
    PointCovariance m_pointCovMatrix; //      origin point covariance
    /// 4-momentum
    Vector m_momentum; //                   4-momentum
    /// momentum covariance matrix
    MomCovariance m_momCovMatrix; //   momentum covariance matrix
    /// position-momentum correlation matrix
    MomPointCovariance m_momPointCovMatrix; //       the correlation matrix
    // ========================================================================
  private: // auxillary
    // ========================================================================
    int m_status = LHCb::Calo::Momentum::OK;    //                   the status
    int m_flag   = LHCb::Calo::Momentum::Empty; //                         flag
    // ========================================================================
  }; // class Momentum
  // ==========================================================================
} // namespace LHCb::Calo
// ============================================================================

inline LHCb::Calo::Momentum::CaloHypos LHCb::Calo::Momentum::caloHypos() { return m_caloHypos; }

inline void LHCb::Calo::Momentum::setReferencePoint( const LHCb::Calo::Momentum::Point& point ) {
  m_point = point;
  this->addToFlag( LHCb::Calo::Momentum::NewReferencePoint );
}
// ============================================================================
inline void LHCb::Calo::Momentum::setPosCovMatrix( const LHCb::Calo::Momentum::PointCovariance& cov ) {
  m_pointCovMatrix = cov;
  this->addToFlag( LHCb::Calo::Momentum::NewPointCovariance );
}
// ============================================================================
inline void LHCb::Calo::Momentum::setReferencePoint( const LHCb::Calo::Momentum::Point&           point,
                                                     const LHCb::Calo::Momentum::PointCovariance& cov ) {
  m_point          = point;
  m_pointCovMatrix = cov;
  this->addToFlag( LHCb::Calo::Momentum::NewReferencePoint );
  this->addToFlag( LHCb::Calo::Momentum::NewPointCovariance );
}
// ============================================================================
