/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// Include files

// from Gaudi
#include "AIDA/IHistogram1D.h"
#include "AIDA/IHistogram2D.h"
#include "AIDA/IProfile1D.h"
#include "AIDA/IProfile2D.h"
#include "GaudiUtils/Aida2ROOT.h"
// from LHCb
#include "Detector/Calo/CaloCellCode.h"
// local
#include "CaloFutureUtils/CaloFuture2Dview.h"

//------------------------------------------------------------------------------
// Implementation file for class : CaloFuture2Dview
//
// 2008-01-17 : Olivier Deschamps
//------------------------------------------------------------------------------

// Declaration of the Algorithm Factory
// DECLARE_COMPONENT( CaloFuture2Dview );

//==============================================================================
// Standard constructor, initializes variables
//==============================================================================

CaloFuture2Dview::CaloFuture2Dview( const std::string& name, ISvcLocator* pSvcLocator )
    : GaudiHistoAlg( name, pSvcLocator ) {
  setHistoDir( name );
}

//==============================================================================

StatusCode CaloFuture2Dview::initialize() {
  return GaudiHistoAlg::initialize().andThen( [&] {
    if ( msgLevel( MSG::DEBUG ) ) debug() << "==> Initialize" << endmsg;

    // protection against splitting when non-goemetrical view or 1D is requested.
    if ( !m_geo ) m_split = false;
    if ( m_1d ) m_split = false;

    // Set up the parameters
    // 0 and 1 are SPD and PRS, which don't exist anymore
    m_caloParams[2] = {
        32,
        32,
        6,
        128,
        351,
        {
            LHCb::Detector::Calo::CellID( LHCb::Detector::Calo::CellCode::Index::EcalCalo, 0, 6, 0 ),  // outer
            LHCb::Detector::Calo::CellID( LHCb::Detector::Calo::CellCode::Index::EcalCalo, 1, 12, 0 ), // middle
            LHCb::Detector::Calo::CellID( LHCb::Detector::Calo::CellCode::Index::EcalCalo, 2, 14, 8 )  // inner
        },
        {
            // FIXME: READ FROM GEOMETRY - but needed at initialization for histo booking
            121.9,       // outer [mm]
            121.9 / 2.f, // middle [mm]
            121.9 / 3.f, // inner [mm]
        }};

    m_caloParams[3] = {32,
                       16,
                       2,
                       352,
                       415,
                       {LHCb::Detector::Calo::CellID( LHCb::Detector::Calo::CellCode::Index::HcalCalo, 0, 3, 0 ),
                        LHCb::Detector::Calo::CellID( LHCb::Detector::Calo::CellCode::Index::HcalCalo, 1, 2, 0 )},
                       {
                           // FIXME: READ FROM GEOMETRY - but needed at initialization for histo booking
                           131.3 * 2, // outer [mm]
                           131.3,     // inner [mm]
                       }};
  } );
}

//==============================================================================

void CaloFuture2Dview::bookCaloFuture2D( const HistoID& unit, const std::string title, const std::string name,
                                         int area ) const {
  const auto calo = LHCb::Detector::Calo::CellCode::CaloNumFromName( name );
  if ( calo < 0 ) {
    error() << "Calo name : " << name << "is unknown " << endmsg;
    return;
  }
  bookCaloFuture2D( unit, title, calo, area );
  return;
}

//==============================================================================

void CaloFuture2Dview::bookCaloFuture2D( const HistoID& unit, const std::string title,
                                         LHCb::Detector::Calo::CellCode::Index calo, int area ) const {
  // Get params
  const auto& cp = m_caloParams[calo];

  // Pre-booking
  m_caloViewMap[unit] = calo;

  // 1D view : caloCellID as xAxis
  if ( m_1d ) {
    if ( m_profile )
      GaudiHistoAlg::bookProfile1D( unit, title, 0., (double)m_bin1d, m_bin1d, m_prof );
    else
      GaudiHistoAlg::book1D( unit, title, 0., (double)m_bin1d, m_bin1d );
    return;
  }

  // electronics 2D view : FEB vs Channel (include PIN-diode channels)
  if ( !m_geo ) {
    // Book histo
    TH2* th2 = nullptr;
    if ( m_profile ) {
      const auto p2d = GaudiHistoAlg::bookProfile2D( unit, title, (double)cp.fCard, (double)cp.lCard,
                                                     cp.lCard - cp.fCard, 0., (double)cp.nChan, cp.nChan );
      th2            = Gaudi::Utils::Aida2ROOT::aida2root( p2d );
    } else {
      const auto h2d = GaudiHistoAlg::book2D( unit, title, (double)cp.fCard, (double)cp.lCard, cp.lCard - cp.fCard, 0.,
                                              (double)cp.nChan, cp.nChan );
      th2            = Gaudi::Utils::Aida2ROOT::aida2root( h2d );
    }
    // set Xaxis bin labels
    th2->GetXaxis()->LabelsOption( m_lab.value().c_str() );
    for ( int bin = 1; bin <= cp.lCard - cp.fCard; ++bin ) {
      if ( ( bin - 1 ) % 8 != 0 ) continue;
      int               code  = cp.fCard + bin - 1;
      int               crate = code / 16;
      int               feb   = code - crate * 16;
      std::stringstream loc( "" );
      loc << "c" << format( "%02i", crate ) << "f" << format( "%02i", feb );
      th2->GetXaxis()->SetBinLabel( bin, loc.str().c_str() );
    }
    return;
  }

  // 2D geometrical view with small bining to simulate variable bin size (separated PMT/PIN histo)
  int fArea = 0;
  int lArea = m_split ? ( 6 + cp.reg ) / 4 : 1;
  if ( m_split && area >= 0 ) {
    fArea = area;
    lArea = area + 1;
  }
  int ppcm = m_split ? 1 : cp.reg;
  for ( int i = fArea; i < lArea; ++i ) {
    double xmin = cp.refCell[i].col();
    double xmax = (double)cp.centre * 2. - xmin;
    double ymin = cp.refCell[i].row();
    double ymax = (double)cp.centre * 2. - ymin;
    int    xbin = ( cp.centre - cp.refCell[i].col() ) * 2 * ppcm;
    int    ybin = ( cp.centre - cp.refCell[i].row() ) * 2 * ppcm;
    if ( m_dim ) {
      xmax = cp.cellSizes[i] * ( xmax - xmin ) / 2.;
      xmin = -xmax;
      ymax = cp.cellSizes[i] * ( ymax - ymin ) / 2.;
      ymin = -ymax;
    }
    const auto lun = getUnit( unit, calo, i );
    const auto tl  = getTitle( title, calo, i );
    if ( !m_profile )
      GaudiHistoAlg::book2D( lun, tl, xmin, xmax, xbin, ymin, ymax, ybin );
    else
      GaudiHistoAlg::bookProfile2D( lun, tl, xmin, xmax, xbin, ymin, ymax, ybin );
  }
  return;
}

//==============================================================================

const GaudiHistoAlg::HistoID CaloFuture2Dview::getUnit( const HistoID& unit, int calo, int area ) const {
  if ( !m_split ) return unit;
  const std::string& nArea = LHCb::Detector::Calo::CellCode::caloArea( calo, area );
  std::string        sunit = unit.literalID();
  if ( sunit == "" ) sunit = std::string( unit );
  int index = sunit.find_last_of( "/" ) + 1;
  sunit.insert( index, nArea + "/" );
  return (HistoID)sunit;
}

//==============================================================================

std::string CaloFuture2Dview::getTitle( std::string title, int calo, int area ) const {
  if ( !m_split ) return title;
  const std::string& nArea = LHCb::Detector::Calo::CellCode::caloArea( calo, area );
  std::string        tit   = title + " (" + nArea + " area)";
  return tit;
}

//==============================================================================
// FILL CALOFUTURE 2D
//==============================================================================

// The main implementation of fillCaloFuture2D
void CaloFuture2Dview::fillCaloFuture2D( const HistoID& unit, const LHCb::Detector::Calo::CellID& id, double value,
                                         const DeCalorimeter& calo, const std::string title ) const {

  // get calo number
  const auto calo_id = id.calo();
  if ( calo_id != calo.index() ) {
    ++m_id_error;
    return;
  }
  const unsigned int area = id.area();

  const auto& cp = m_caloParams[calo_id];

  // threshold
  if ( value < m_threshold ) return;

  // book histo if not found
  const HistoID& lun = getUnit( unit, calo_id, area );
  if ( !histoExists( lun ) ) bookCaloFuture2D( unit, title, calo_id, area );

  // check the cellID is consistent with the calo
  if ( m_caloViewMap[unit] != calo_id ) {
    if ( msgLevel( MSG::DEBUG ) )
      debug() << "Cannot put the  CaloCellID " << id << " in the " << caloName( m_caloViewMap[unit] ) << " view '"
              << unit << "'" << endmsg;
    return;
  }

  // -------------- 1D view
  if ( m_1d ) {
    if ( m_profile )
      profile1D( lun )->fill( (double)id.index(), value, 1. );
    else
      histo1D( lun )->fill( (double)id.index(), value );
    return;
  }

  // -------------- Electronics 2D view (crate/feb .vs. channel)
  if ( !m_geo ) {
    const int    feb    = calo.cardNumber( id );
    const int    code   = calo.cardCode( feb );
    const int    chan   = calo.cardColumn( id ) + nColCaloCard * calo.cardRow( id );
    const double weight = ( value + m_offset ) / ( m_flux ? cp.cellSizes[area] * cp.cellSizes[area] : 1. );
    if ( m_profile )
      profile2D( lun )->fill( (double)code, (double)chan, weight, 1. );
    else
      histo2D( lun )->fill( (double)code, (double)chan, weight );
    return;
  }

  // -------------- 2D geometrical view with small bining to simulate variable cell sizes
  const double       xs      = cp.cellSizes[area];
  const double       ys      = cp.cellSizes[area];
  const double       xs0     = cp.cellSizes[0];
  const double       ys0     = cp.cellSizes[0];
  const double       xResize = m_dim ? ( m_split ? xs : xs0 ) : 1.;
  const double       yResize = m_dim ? ( m_split ? ys : ys0 ) : 1.;
  const unsigned int ibox    = m_split ? 1 : cp.reg / ( area + 1 );
  const int          ppcm    = m_split ? 1 : cp.reg;
  const double       weight  = ( value + m_offset ) / ( m_flux ? xs * ys : 1. );

  // PMT channels
  const unsigned int row = id.row();
  const unsigned int col = id.col();

  // L0-cluster mode (2x2 cells area are filled)
  const unsigned int icl = m_l0 ? 2 : 1;

  // loop over cluster area (1x1 or 2x2 cells depending on L0 mode)
  for ( unsigned int kr = 0; kr < icl; kr++ ) {
    for ( unsigned int kc = 0; kc < icl; kc++ ) {
      LHCb::Detector::Calo::CellID temp = id;
      if ( m_l0 ) {
        temp = LHCb::Detector::Calo::CellID( calo_id, area, row + kr, col + kc );
        if ( !calo.valid( temp ) ) continue;
      }
      unsigned int theRow = row + kr;
      unsigned int theCol = col + kc;
      // Fill the standard view  for PMT
      // loop over cell area ( ibox * ibox bins depending on CaloFuture/Area)
      for ( unsigned int ir = 0; ir < ibox; ir++ ) {
        for ( unsigned int ic = 0; ic < ibox; ic++ ) {
          int iic = ( theCol - cp.centre ) * ibox + ic;
          int iir = ( theRow - cp.centre ) * ibox + ir;
          if ( !m_dim ) {
            iic += ppcm * cp.centre;
            iir += ppcm * cp.centre;
          }
          double x = xResize * ( (double)iic + 0.5 ) / (double)ppcm;
          double y = yResize * ( (double)iir + 0.5 ) / (double)ppcm;
          if ( m_profile )
            profile2D( lun )->fill( x, y, weight, 1 );
          else
            histo2D( lun )->fill( x, y, weight );
        }
      }
    }
  }
  return;
}
