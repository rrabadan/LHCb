/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// ============================================================================
// Include files
// ============================================================================
// Event
// ============================================================================
#include "Event/Particle.h"
#include "Event/Vertex.h"
// ============================================================================
// CaloFutureUtils
// ============================================================================
#include "CaloFutureUtils/CaloFutureParticle.h"
// ============================================================================
/** @file
 *  Implementation file for class LHCb::CaloFutureParticle
 *  @author Olivier Deschamps
 */
// ============================================================================
// constructor from the particle
// ============================================================================
LHCb::Calo::CaloParticle::CaloParticle( LHCb::Particle* part ) : m_isCaloFuture( bool( part ) ) {
  addCaloFuturePosition( part );
}
// ============================================================================
// constructor from particle & origin point
// ============================================================================
LHCb::Calo::CaloParticle::CaloParticle( LHCb::Particle* part, const LHCb::Calo::Momentum::Point& point )
    : m_isCaloFuture( bool( part ) ) {
  setReferencePoint( point );
  addCaloFuturePosition( part );
  addToFlag( LHCb::Calo::Momentum::NewReferencePoint );
}
// ============================================================================
// constructor from particle & origin point & covarinace
// ============================================================================
LHCb::Calo::CaloParticle::CaloParticle( LHCb::Particle* part, const LHCb::Calo::Momentum::Point& point,
                                        const LHCb::Calo::Momentum::PointCovariance& cov )
    : m_isCaloFuture( bool( part ) ) {
  setReferencePoint( point, cov );
  addCaloFuturePosition( part );
  addToFlag( LHCb::Calo::Momentum::NewReferencePoint );
  addToFlag( LHCb::Calo::Momentum::NewPointCovariance );
}
// ============================================================================
// Particle
// ============================================================================
void LHCb::Calo::CaloParticle::addCaloFuturePosition( LHCb::Particle* part ) {
  // --- Fill CaloFuturePosition vector from particle
  //  2 configuration :
  //    - the particle is basic (e.g. photon, mergedPi0)  : use proto->caloggggg
  //    - the particle is composite (e.g. pi0/eta->gg, Ks/B->pi0pi0->gggg, ...) : run along the decay tree

  // Some checks
  addToFlag( LHCb::Calo::Momentum::FromPart );
  if ( !part ) {
    addToStatus( LHCb::Calo::Momentum::NullPart );
    m_neutral = false;
    return;
  }
  if ( 0 != part->charge() ) {
    addToStatus( LHCb::Calo::Momentum::ChargedParticle );
    m_neutral = false;
    return;
  }

  // Check the particle origin (protoParticle)
  if ( part->proto() ) {
    if ( part->proto()->calo().empty() ) {
      m_isCaloFuture = false;
      addToStatus( LHCb::Calo::Momentum::PartIsNotCaloObject );
      return;
    }
    // particle derive from proto->caloHypo (-> photon, mergedPi0 )
    m_parts.push_back( part );
    m_caloEndTree.push_back( part );
    LHCb::Calo::Momentum::addCaloPosition( part->proto() );
  } else if ( !part->isBasicParticle() ) {
    // particle is composite
    addToFlag( LHCb::Calo::Momentum::FromPartDaughters );
    CaloFutureParticleTree( part );
    if ( m_isCaloFuture ) {
      m_parts.push_back( part );
      // the end-tree particles are pure calo objects
      for ( const auto& icalo : m_caloEndTree ) {
        if ( icalo->charge() != 0 ) m_neutral = false;
        LHCb::Calo::Momentum::addCaloPosition( icalo->proto() );
      }
    } else {
      //      m_caloEndTree.clear();
      addToStatus( LHCb::Calo::Momentum::PartIsNotCaloObject );
    }
  } else {
    //    m_caloEndTree.clear();
    addToStatus( LHCb::Calo::Momentum::PartIsNotCaloObject );
  }
}
// ============================================================================
// update the tree
// ============================================================================
void LHCb::Calo::CaloParticle::CaloFutureParticleTree( const LHCb::Particle* part ) {

  if ( !m_isCaloFuture ) {
    m_caloEndTree.clear();
    return;
  }
  if ( part->isBasicParticle() ) {
    if ( nullptr == part->proto() ) {
      m_isCaloFuture = false;
    } else if ( part->proto()->calo().empty() ) {
      m_isCaloFuture = false;
    } else {
      m_caloEndTree.push_back( part );
    }
  } else {
    for ( const auto& dau : part->daughters() ) CaloFutureParticleTree( const_cast<LHCb::Particle*>( dau.target() ) );
  }
}
// ============================================================================
// update particle
// ============================================================================
void LHCb::Calo::CaloParticle::updateParticle() {
  if ( LHCb::Calo::Momentum::OK == status() ) {
    for ( LHCb::Particle* part : m_parts ) {
      part->setReferencePoint( referencePoint() );
      part->setPosCovMatrix( pointCovMatrix() );
      part->setMomentum( momentum() );
      part->setMomCovMatrix( momCovMatrix() );
      part->setPosMomCovMatrix( momPointCovMatrix() );
      addToFlag( LHCb::Calo::Momentum::ParticleUpdated );
    }
  }
}
// ============================================================================
// update tree
// ============================================================================
void LHCb::Calo::CaloParticle::updateTree() {
  updateParticle();

  for ( LHCb::Particle* part : m_parts ) {
    if ( !part->isBasicParticle() && LHCb::Calo::Momentum::OK == status() ) {
      for ( const auto& daughter : part->daughters() ) {
        LHCb::Calo::CaloParticle caloDau( const_cast<LHCb::Particle*>( daughter.target() ), referencePoint(),
                                          pointCovMatrix() );
        caloDau.updateTree();
      }
      addToFlag( LHCb::Calo::Momentum::ParticleTreeUpdated );
    }
  }
}
// ============================================================================
// add vertex
// ============================================================================
void LHCb::Calo::CaloParticle::addToVertex( LHCb::Vertex* vertex ) {
  m_vert = vertex;
  for ( const auto& part : m_parts ) vertex->addToOutgoingParticles( part );
  setReferencePoint( vertex );
  addToFlag( LHCb::Calo::Momentum::NewReferencePoint );
  addToFlag( LHCb::Calo::Momentum::NewPointCovariance );
}
// ============================================================================
// get the particle
// ============================================================================
LHCb::Particle* LHCb::Calo::CaloParticle::particle() const { return !m_parts.empty() ? m_parts.front() : nullptr; }
