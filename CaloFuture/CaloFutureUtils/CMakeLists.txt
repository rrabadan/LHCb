###############################################################################
# (c) Copyright 2000-2021 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
#[=======================================================================[.rst:
CaloFuture/CaloFutureUtils
--------------------------
#]=======================================================================]

gaudi_add_library(CaloFutureUtils
    SOURCES
        src/CaloFuture2Dview.cpp
        src/CaloFutureAlgUtils.cpp
        src/CaloFutureNeighbours.cpp
        src/CaloFutureParticle.cpp
        src/CaloFutureUtils.cpp
        src/CaloMomentum.cpp
        src/CellMatrix.cpp
        src/CellNeighbour.cpp
        src/ClusterFunctors.cpp
        src/Kinematics.cpp
    LINK
        PUBLIC
            Boost::headers
            fmt::fmt
            Gaudi::GaudiAlgLib
            Gaudi::GaudiKernel
            LHCb::CaloDetLib
            LHCb::CaloFutureInterfaces
            LHCb::DigiEvent
            LHCb::LHCbKernel
            LHCb::LHCbMathLib
            LHCb::LinkerEvent
            LHCb::MCEvent
            LHCb::PhysEvent
            LHCb::RecEvent
            LHCb::RelationsLib
            ROOT::Hist
        PRIVATE
            AIDA::aida
            Gaudi::GaudiUtilsLib
            LHCb::CaloKernel
            LHCb::DAQEventLib

)

gaudi_add_dictionary(CaloFutureUtilsDict
    HEADERFILES dict/CaloFutureUtilsDict.h
    SELECTION dict/CaloFutureUtils.xml
    LINK
        LHCb::CaloFutureUtils
        LHCb::CaloKernel
        LHCb::DigiEvent
)

gaudi_install(PYTHON)
