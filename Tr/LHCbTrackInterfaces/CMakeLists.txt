###############################################################################
# (c) Copyright 2000-2021 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
#[=======================================================================[.rst:
Tr/LHCbTrackInterfaces
----------------------
#]=======================================================================]

gaudi_add_header_only_library(LHCbTrackInterfaces
    LINK
        Gaudi::GaudiKernel
        LHCb::LHCbKernel
        LHCb::UTDetLib
        LHCb::DetDescLib
        LHCb::MagnetLib
)

gaudi_add_dictionary(LHCbTrackInterfacesDict
    HEADERFILES dict/LHCbTrackInterfacesDict.h
    SELECTION dict/LHCbTrackInterfacesDict.xml
    LINK LHCb::LHCbTrackInterfaces
)
