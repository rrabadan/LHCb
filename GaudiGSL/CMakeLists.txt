###############################################################################
# (c) Copyright 2000-2021 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
#[=======================================================================[.rst:
GaudiGSL
--------
#]=======================================================================]

gaudi_add_library(GaudiGSLLib
    SOURCES
        src/Lib/Adapter.cpp
        src/Lib/Adapters.cpp
        src/Lib/Constant.cpp
        src/Lib/GSLFunAdapters.cpp
        src/Lib/GaudiGSL.cpp
        src/Lib/GslErrorHandlers.cpp
        src/Lib/Helpers.cpp
        src/Lib/Integral.cpp
        src/Lib/Interpolation.cpp
        src/Lib/NumericalDefiniteIntegral.cpp
        src/Lib/NumericalDerivative.cpp
        src/Lib/NumericalIndefiniteIntegral.cpp
        src/Lib/Splines.cpp
    LINK
        PUBLIC
            Gaudi::GaudiKernel
            GSL::gsl
            CLHEP::CLHEP
            Detector::DetectorLib
        PRIVATE
            AIDA::aida
)

gaudi_add_module(GaudiGSL
    SOURCES
        src/Components/EqSolver.cpp
        src/Components/FuncMinimum.cpp
        src/Components/GslErrorCount.cpp
        src/Components/GslErrorException.cpp
        src/Components/GslErrorPrint.cpp
        src/Components/GslSvc.cpp
    LINK
        Gaudi::GaudiAlgLib
        LHCb::GaudiGSLLib
)

gaudi_add_module(GaudiGSLExamples
    SOURCES
        examples/src/EqSolverGenAlg.cpp
        examples/src/EqSolverIAlg.cpp
        examples/src/EqSolverPAlg.cpp
        examples/src/FuncMinimumGenAlg.cpp
        examples/src/FuncMinimumIAlg.cpp
        examples/src/FuncMinimumPAlg.cpp
    LINK
        AIDA::aida
        LHCb::GaudiGSLLib
)

gaudi_add_dictionary(GaudiGSLMathDict
    HEADERFILES dict/GaudiGSLMath.h
    SELECTION dict/GaudiGSLMath.xml
    LINK LHCb::GaudiGSLLib
)

foreach(test IN ITEMS
        IntegralInTest DerivativeTest 2DoubleFuncTest GSLAdaptersTest
        PFuncTest ExceptionsTest SimpleFuncTest 3DoubleFuncTest InterpTest   
        Integral1Test)
    gaudi_add_executable(${test}
        SOURCES src/Tests/${test}.cpp
        LINK LHCb::GaudiGSLLib
        TEST)      
endforeach()
